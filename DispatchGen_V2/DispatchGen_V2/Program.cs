﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLog;
using System.Collections.Specialized;
using System.Configuration;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Data;

namespace DispatchGen
{
    class Program
    {
        private static string ConnectionString;
        public static string DispatchUrl;
        public static Logger logger = NLog.LogManager.GetCurrentClassLogger();
        private static DispatchGenFunction func;

        static void Main(string[] args)
        {

            NameValueCollection nc = ConfigurationManager.AppSettings;
            if (nc["Server"] == "Production")
            {
                ConnectionString = "Server=10.0.2.23;Port=3306;Database=carpoollab;Uid=ishareco;Pwd=fu0!j0$su0^vup#;Allow User Variables=True";
                //DispatchUrl = @"http://10.0.2.16:3000/api/match";
                DispatchUrl = @"http://10.0.2.22:3000/api/match";
            }
            else if (nc["Server"] == "Lab")
            {
                ConnectionString = "Server=10.0.2.21;Port=3306;Database=carpoollab;Uid=apiuser;Pwd=7oPOh88I;Allow User Variables=True";
                DispatchUrl = @"http://10.0.2.22:3000/api/match";
            }
            func = new DispatchGen.DispatchGenFunction(ConnectionString);
            DateTime Today = DateTime.Now;
            /*
             * 加入連續假日提前媒合 20171229
             * 檢查當日是否為連續假日截止預約日 
             * 如果是 先媒合連續假期的訂單
             * 再依序媒合 D+7(試搓)~D+2(正式)
            */
            try
            {
                DateTime MatchDate;

                //Step 檢查是否有連續假日

                MatchHolidayInfo info = func.GetHoliday(Today.ToString("yyyyMMdd"));
                if (info != null)
                {
                    DateTime HolidayBeginDate = DateTime.Parse(info.BeginDate.Substring(0, 4) + "/" + info.BeginDate.Substring(4, 2) + "/" + info.BeginDate.Substring(6, 2));
                    DateTime HolidayEndDate = DateTime.Parse(info.EndDate.Substring(0, 4) + "/" + info.EndDate.Substring(4, 2) + "/" + info.EndDate.Substring(6, 2));
                    MatchDate = HolidayEndDate;

                    while (MatchDate >= HolidayBeginDate.Date)
                    {
                        string Result = string.Empty, Json = string.Empty;
                        Result = func.Match(MatchDate.ToString("yyyyMMdd"));

                        if (Result == "")
                        {
                            try
                            {
                                func.SaveMatchRecord(MatchDate.ToString("yyyyMMdd"), 0);
                            }
                            catch (Exception ex)
                            {
                                logger.Error(DateTime.Now.ToString("yyyyMMdd HH:mm:ss") + "   " + MatchDate.ToString("yyyyMMdd") + "媒合結果檔寫入失敗：" + ex.Message + Environment.NewLine + ex.StackTrace);
                                MatchDate = MatchDate.AddDays(-1).Date;
                                continue;
                            }

                            MatchDate = MatchDate.AddDays(-1).Date;
                            continue;

                        }
                        JObject jo = JsonConvert.DeserializeObject<JObject>(Result);
                        Json = jo["data"].ToString();
                        MatchResultJsonStruct MatchResult = JsonConvert.DeserializeObject<MatchResultJsonStruct>(Json);

                        //試搓 寫入價格檔
                        func.PreMatch(MatchDate.ToString("yyyyMMdd"), MatchResult);

                        //假日固定要寫入
                        func.WriteResult2DB(MatchDate.ToString("yyyyMMdd"), Json);

                        MatchDate = MatchDate.AddDays(-1).Date;
                    }
                }

                //D+7開始試搓  D+2 正式媒合需寫入資料庫
                DateTime BeginDate = DateTime.Now.AddDays(7).Date;
                DateTime OfficialDate = DateTime.Now.AddDays(2).Date;

                MatchDate = BeginDate;
                while (MatchDate >= OfficialDate.Date)
                {

                    if (func.isAlreadyMatched(MatchDate.ToString("yyyyMMdd")))
                    {
                        MatchDate = MatchDate.AddDays(-1).Date;
                        continue;
                    }
                    string Result = string.Empty, Json = string.Empty;
                    Result = func.Match(MatchDate.ToString("yyyyMMdd"));

                    if (Result == "")
                    {
                        try
                        {
                            if (MatchDate.Date == OfficialDate.Date)
                                func.SaveMatchRecord(MatchDate.ToString("yyyyMMdd"), 0);
                        }
                        catch (Exception ex)
                        {
                            logger.Error(DateTime.Now.ToString("yyyyMMdd HH:mm:ss") + "   " + MatchDate.ToString("yyyyMMdd") + "媒合結果檔寫入失敗：" + ex.Message + Environment.NewLine + ex.StackTrace);
                            MatchDate = MatchDate.AddDays(-1).Date;
                            continue;
                        }
                        MatchDate = MatchDate.AddDays(-1).Date;
                        continue;
                    }
                    JObject jo = JsonConvert.DeserializeObject<JObject>(Result);
                    Json = jo["data"].ToString();
                    MatchResultJsonStruct MatchResult = JsonConvert.DeserializeObject<MatchResultJsonStruct>(Json);

                    //試搓 寫入價格檔
                    func.PreMatch(MatchDate.ToString("yyyyMMdd"), MatchResult);


                    //D+2日 將媒合寫入資料庫
                    if (MatchDate.Date == OfficialDate.Date)
                        func.WriteResult2DB(MatchDate.ToString("yyyyMMdd"), Json);

                    MatchDate = MatchDate.AddDays(-1).Date;
                }
            }
            catch (Exception ex)
            {
                logger.Error(DateTime.Now.ToString("yyyyMMdd HH:mm:ss") + "媒合失敗：  " + ex.Message + Environment.NewLine + ex.StackTrace);
            }
            logger.Info(DateTime.Now.ToString("yyyyMMdd HH:mm:ss") + "媒合完成 ");
            try
            {
                string FilePathDispatch = System.Environment.CurrentDirectory;
                string FilePathReservation = System.Environment.CurrentDirectory;
                FilePathDispatch += @"\csv\" + DateTime.Now.ToString("yyyy-MM-dd") + "派車資料_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".txt";
                FilePathReservation += @"\csv\" + DateTime.Now.ToString("yyyy-MM-dd") + "訂單資料_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".txt";
                DataTable dtDispatch = new DataTable();
                DataTable dtReservation = new DataTable();
                dtDispatch = func.GetAllDispatchData();
                dtReservation = func.GetAllReservationData();
                System.IO.File.WriteAllText(FilePathDispatch, JsonConvert.SerializeObject(dtDispatch));
                System.IO.File.WriteAllText(FilePathReservation, JsonConvert.SerializeObject(dtReservation));
                List<string> Mailaddress = new List<string>();
                Mailaddress.Add("bryan_yang@ishareco.com");
                List<System.Net.Mail.Attachment> Attach = new List<System.Net.Mail.Attachment>();
                Attach.Add(new System.Net.Mail.Attachment(FilePathDispatch));
                Attach.Add(new System.Net.Mail.Attachment(FilePathReservation));
                GmailSMTP smtp = new GmailSMTP("info@ishareco.com", "share52439256i");
                smtp.Send(Mailaddress, "艾雪科技通知", "每日(" + Today.ToString("yyyy-MM-dd") + "派遣&預約通知信", "", Attach);

            }
            catch (Exception ex)
            {
                logger.Error(DateTime.Now.ToString("yyyyMMdd") + "寄送趟次資料失敗" + Environment.NewLine + ex.Message + Environment.NewLine + ex.StackTrace);
                Environment.Exit(0);
            }

            logger.Info(DateTime.Now.ToString("yyyyMMdd") + "媒合+寄送趟次資料完成");
            Environment.Exit(0);
        }
    }

    public class ReservationList
    {
        public string ReservationNo { get; set; }
        public string ServiceTime { get; set; }
    }

    public class DispatchDetail
    {
        public DispatchDetail() { this.ServiceList = new List<ReservationList>(); }
        public int BaggageCnt { get; set; }
        public string MaxFlag { get; set; }
        public int PassengerCnt { get; set; }
        public int ServiceCnt { get; set; }
        public string ServiceDate { get; set; }
        public List<ReservationList> ServiceList { get; set; }
        public string ServiceType { get; set; }
    }

    public class DispatchInfo
    {
        public DispatchDetail Detail { get; set; }
        public int Index { get; set; }
    }

    public class MatchResultJsonStruct
    {
        public MatchResultJsonStruct()
        {
            this.DispatchInfo = new List<DispatchInfo>();
        }
        public List<DispatchInfo> DispatchInfo { get; set; }
    }

    public class MatchHolidayInfo
    {
        public string LockDate { get; set; }
        public string BeginDate { get; set; }
        public string EndDate { get; set; }
    }
}
