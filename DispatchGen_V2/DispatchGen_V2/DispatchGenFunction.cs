﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DbConn;
using MySql.Data.MySqlClient;
using System.Data;
using System.Net;
using System.IO;
using Newtonsoft.Json;
using Callcar.CObject;
using Callcar.BAL;
using Callcar.DAL;
using Newtonsoft.Json.Linq;

namespace DispatchGen
{
    public class DispatchGenFunction
    {
        private string _connectionstring;
        public DispatchGenFunction(string ConnectionString) { this._connectionstring = ConnectionString; }

        public MatchHolidayInfo GetHoliday(string QueryDate)
        {
            MatchHolidayInfo info = new MatchHolidayInfo();
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            DataTable dt;

            sb.Append("SELECT LockDate, BeginDate, EndDate 	FROM match_holiday_data 	where LockDate = @date");
            parms.Add(new MySqlParameter("@date", QueryDate));

            using (dbConnectionMySQL conn = new dbConnectionMySQL(this._connectionstring))
            {
                dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
            }

            if (dt.Rows.Count == 1)
            {
                info.LockDate = QueryDate;
                info.BeginDate = Convert.ToString(dt.Rows[0]["BeginDate"]);
                info.EndDate = Convert.ToString(dt.Rows[0]["EndDate"]);
                return info;
            }
            else
                return null;
        }

        public bool isAlreadyMatched(string MatchDate)
        {
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            sb.Append("select count(1) FROM  match_record  WHERE MatchDate  = @MatchDate");
            parms.Add(new MySqlParameter("@MatchDate", MatchDate));

            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._connectionstring))
                {
                    DataTable dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());

                    if (Convert.ToInt32(dt.Rows[0][0]) == 0)
                        return false;
                    else
                        return true;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public OMachSourceData GetMatchSourceByDate(string date)
        {
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            sb.Append("SELECT ReservationNo, UserID, ServiceType, TakeDate, TimeSegment, SelectionType, PickCartype, FlightNo, ScheduleFlightTime, PickupCity, ");
            sb.Append("PickupDistinct, PickupVillage, PickupAddress, X(PickupGPS) as plng, Y(PickupGPS) as plat, TakeoffCity, TakeoffDistinct, TakeoffVillage, TakeoffAddress, X(TakeoffGPS) as tlng ,Y(TakeoffGPS) as tlat , PassengerCnt, ");
            sb.Append("BaggageCnt, MaxFlag, Price, Coupon, InvoiceData, CreditData, ProcessStage, InvoiceNum, CreateTime, UpdTime ");
            sb.Append("FROM reservation_sheet where TakeDate = @sdate  AND ProcessStage = '0' order by TakeDate,TimeSegment asc ");

            parms.Add(new MySqlParameter("@sdate", date));
            DataTable dt = new DataTable();
            List<OReservation> List = new List<OReservation>();
            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._connectionstring))
                {
                    dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                }
            }
            catch (MySqlException ex)
            {
                throw ex;
            }

            OMachSourceData Match = new OMachSourceData();
            Match.MatchDate = date;
            if (dt.Rows.Count > 0)
            {

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    OMatchRData m = new OMatchRData();
                    m.ReservationNo = Convert.ToString(dt.Rows[i]["ReservationNo"]);
                    m.ServiceType = Convert.ToString(dt.Rows[i]["ServiceType"]);
                    m.TimeSegment = Convert.ToString(dt.Rows[i]["TimeSegment"]);
                    m.BaggageCnt = Convert.ToInt32(dt.Rows[i]["BaggageCnt"]);
                    m.PassengerCnt = Convert.ToInt32(dt.Rows[i]["PassengerCnt"]);
                    m.PickupLat = Convert.ToDouble(dt.Rows[i]["plng"]);
                    m.PickupLng = Convert.ToDouble(dt.Rows[i]["plat"]);
                    m.TakeoffLat = Convert.ToDouble(dt.Rows[i]["tlat"]);
                    m.TakeoffLng = Convert.ToDouble(dt.Rows[i]["tlng"]);
                    m.MaxFlag = Convert.ToString(dt.Rows[i]["MaxFlag"]);
                    if (dt.Rows[i]["FlightNo"] != null && dt.Rows[i]["FlightNo"] != DBNull.Value)
                    {
                        m.FlightNo = Convert.ToString(dt.Rows[i]["FlightNo"]);

                        if (m.FlightNo != "")
                            m.FlightTime = Convert.ToDateTime(dt.Rows[i]["ScheduleFlightTime"]);
                    }
                    else
                    {
                        m.FlightNo = "";
                    }

                    DateTime MatchStartTime, MatchEndTime, PreferTime;
                    PreferTime = Callcar.Common.General.ConvertToDateTime(date + m.TimeSegment, 12);

                    if (m.ServiceType == "O")
                    {
                        if (m.FlightNo != "" && m.FlightTime != null)
                        {
                            if (PreferTime.AddMinutes(15).CompareTo(m.FlightTime.AddMinutes(-150)) >= 0)
                                MatchEndTime = m.FlightTime.AddMinutes(-150);
                            else
                                MatchEndTime = PreferTime.AddMinutes(15);

                            MatchStartTime = PreferTime.AddMinutes(-30);
                        }
                        else
                        {
                            MatchEndTime = PreferTime.AddMinutes(15);
                            MatchStartTime = PreferTime.AddMinutes(-30);
                        }
                    }
                    else
                    {
                        MatchEndTime = PreferTime.AddMinutes(15);
                        MatchStartTime = PreferTime;
                    }

                    if (MatchStartTime.Date < PreferTime.Date)
                        m.mStartTime = "0000";
                    else
                        m.mStartTime = MatchStartTime.ToString("HHmm");

                    if (MatchEndTime.Date > PreferTime.Date)
                        m.mEndTime = "2359";
                    else
                        m.mEndTime = MatchEndTime.ToString("HHmm");

                    Match.ReservationList.Add(m);
                }
            }
            return Match;
        }

        private DataTable GetReservationForMatch(string sdate)
        {

            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            sb.Append("SELECT ReservationNo, UserID, ServiceType, TakeDate, TimeSegment, SelectionType, PickCartype, FlightNo, ScheduleFlightTime, PickupCity, ");
            sb.Append("PickupDistinct, PickupVillage, PickupAddress, X(PickupGPS) as plng, Y(PickupGPS) as plat, TakeoffCity, TakeoffDistinct, TakeoffVillage, TakeoffAddress, X(TakeoffGPS) as tlng ,Y(TakeoffGPS) as tlat , PassengerCnt, ");
            sb.Append("BaggageCnt, MaxFlag, Price, Coupon, InvoiceData, CreditData, ProcessStage, InvoiceNum, CreateTime, UpdTime ");
            sb.Append("FROM reservation_sheet where TakeDate = @sdate  AND ProcessStage = '0' AND  CarpoolFlag = 'S'  and PassengerCnt < 5 order by TakeDate,TimeSegment asc ");

            parms.Add(new MySqlParameter("@sdate", sdate));
            DataTable dt = new DataTable();
            //List<OReservation> List = new List<OReservation>();
            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._connectionstring))
                {
                    return dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                }
            }
            catch (MySqlException ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 將媒合日期數量寫入紀錄檔
        /// </summary>
        /// <returns></returns>
        public bool SaveMatchRecord(string MatchDate, int Count)
        {
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            sb.Append("INSERT INTO match_record 	(MatchDate, DispatchCnt) 	VALUES (@MatchDate,@DispatchCnt)");
            parms.Add(new MySqlParameter("@MatchDate", MatchDate));
            parms.Add(new MySqlParameter("@DispatchCnt", Count));

            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._connectionstring))
                {
                    if (conn.executeInsertQuery(sb.ToString(), parms.ToArray()) > 0)
                        return true;
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// 丟到allen的媒合程式
        /// </summary>
        /// <param name="DispatchDate"></param>
        /// <returns></returns>
        public string Match(string DispatchDate)
        {
            try
            {
                OMachSourceData source = GetMatchSourceByDate(DispatchDate);
                if (source.ReservationList.Count == 0)
                    return "";

                string MatchJson = JsonConvert.SerializeObject(source);

                HttpWebRequest request = HttpWebRequest.Create(Program.DispatchUrl) as HttpWebRequest;
                string result = null;
                request.Method = "POST";    // 方法
                request.KeepAlive = true; //是否保持連線
                request.ContentType = "application/json";

                byte[] bs = Encoding.ASCII.GetBytes(MatchJson);

                using (Stream Stream = request.GetRequestStream())
                {
                    Stream.Write(bs, 0, bs.Length);
                }

                using (WebResponse response = request.GetResponse())
                {
                    StreamReader sr = new StreamReader(response.GetResponseStream());
                    result = sr.ReadToEnd();
                    sr.Close();
                }
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void PreMatch(string DispatchDate, MatchResultJsonStruct MatchResult)
        {
            //int CarFee = 0, NightFee = 0, BagFee = 0;
            //int PromotFee = 0, TotalPrice = 0, 
            int CarCnt = 0;
            BALReservation balr = new Callcar.BAL.BALReservation(this._connectionstring);
            BALGeneral balg = new Callcar.BAL.BALGeneral(this._connectionstring);
            DALGeneral dalg = new Callcar.DAL.DALGeneral(this._connectionstring);

            try
            {
                foreach (DispatchInfo di in MatchResult.DispatchInfo)
                {
                    foreach (ReservationList sl in di.Detail.ServiceList)
                    {
                        OReservation order = balr.GetReservationByNo(sl.ReservationNo);

                        OPrice op = new OPrice();
                        CarCnt = di.Detail.PassengerCnt - order.PassengerCnt;
                        try
                        {
                            //string FeeString = balg.CalculateTrailFee(order.OrderTime.ToString("yyyyMMdd"), order.ServeDate, order.PreferTime, order.PassengerCnt, order.BaggageCnt, order.Coupon, CarCnt, order.AddBagCnt, false, "", order.UserID);
                            op = balg.CalculateTrailFee(order.OrderTime.ToString("yyyyMMdd"), order.ServeDate, order.PreferTime, order.PassengerCnt, order.BaggageCnt, order.Coupon, CarCnt, order.AddBagCnt, false, "", order.UserID);
                            //string[] Fee = FeeString.Split('_');
                            //Int32.TryParse(Fee[0], out CarFee);
                            //Int32.TryParse(Fee[1], out NightFee);
                            //Int32.TryParse(Fee[2], out BagFee);
                            //Int32.TryParse(Fee[3], out PromotFee);
                            //TotalPrice = CarFee + NightFee + BagFee - PromotFee;


                            dalg.InsertTrailFee(order.ReservationNo, op);// TotalPrice);
                        }
                        catch (Exception ex)
                        {
                            Program.logger.Error("Prematch 試算金額 Order：" + order.ReservationNo + "發生錯誤：" + ex.Message);
                            continue;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Program.logger.Error("Prematch Date：" + DispatchDate + "發生錯誤：" + ex.Message);
                return;
            }
        }

        /// <summary>
        /// 將媒合結果寫入派車資料
        /// </summary>
        /// <param name="DispatchDate"></param>
        /// <param name="result"></param>
        /// <returns></returns>
        public bool WriteResult2DB(string DispatchDate, string result)
        {
            try
            {
                int sheetCnt = 0;
                sheetCnt = BatchAddDispatchSheet(DispatchDate, result);
                try
                {
                    SaveMatchRecord(DispatchDate, sheetCnt);
                }
                catch (Exception)
                {
                    return true;
                }

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 批次新增媒合結果
        /// </summary>
        /// <param name="data">媒合結果的JSON</param>
        /// <returns></returns>
        private int BatchAddDispatchSheet(string DispatchDate, string data)
        {
            MatchResultJsonStruct MatchResult = JsonConvert.DeserializeObject<MatchResultJsonStruct>(data);
            int sheetCnt = 0;


            DALDispatch dald = new DALDispatch(this._connectionstring);
            DALEmployee dale = new DALEmployee(this._connectionstring);
            DALReservation dalr = new DALReservation(this._connectionstring);
            DALSerialNum dals = new DALSerialNum(this._connectionstring);
            //將Json資料設定至派遣Object中
            foreach (DispatchInfo info in MatchResult.DispatchInfo)
            {
                ODispatchSheet od = new ODispatchSheet();

                string DispatchNo = string.Empty;
                try
                {
                    OSerialNum serial = dals.GetSerialNumObject("Dispatch");
                    DispatchNo = DateTime.Now.ToString(serial.Prefix) + dals.GetSeqNoString(serial);
                }
                catch (Exception ex)
                {
                    throw new Exception("取派車單號失敗" + ex.Message + Environment.NewLine + JsonConvert.SerializeObject(info));
                }

                sheetCnt++;
                od.DispatchNo = DispatchNo;

                int PCnt = 0, BCnt = 0;
                string TakeDate = string.Empty;
                List<DateTime> ServiceTime = new List<DateTime>();

                foreach (ReservationList s in info.Detail.ServiceList)
                {
                    ODpServiceUnit su = new ODpServiceUnit();
                    OReservation r;
                    try
                    {
                        r = dalr.GetReservation(s.ReservationNo);
                    }
                    catch (Exception ex)
                    {
                        //寫入媒合紀錄檔
                        JObject jo = JsonConvert.DeserializeObject<JObject>(data);
                        JArray arryDispatch = (JArray)jo["DispatchInfo"];
                        for (int i = 0; i < arryDispatch.Count; i++)
                        {
                            if (Convert.ToInt32(arryDispatch[i]["Index"].ToString()) == info.Index)
                            {
                                SaveMatchError(od.TakeDate, "E", info.Index, arryDispatch[i]["Detail"].ToString(), ex.Message);
                                break;
                            }
                        }
                        continue;
                    }

                    su.DispatchNo = DispatchNo;
                    su.ScheduleTime = Convert.ToDateTime(DispatchDate.Substring(0, 4) + "-" + DispatchDate.Substring(4, 2) + "-" + DispatchDate.Substring(6, 2) + " " + s.ServiceTime.Substring(0, 2) + ":" + s.ServiceTime.Substring(2, 2));
                    ServiceTime.Add(su.ScheduleTime);
                    ServiceTime.Sort((a, b) => a.CompareTo(b));
                    su.ReservationNo = s.ReservationNo;
                    su.ServiceRemark = "0";
                    if (r.ServiceType == "I")
                        su.ServiceGPS = r.TakeoffGPS;
                    else
                        su.ServiceGPS = r.PickupGPS;
                    PCnt += r.PassengerCnt;
                    BCnt += r.BaggageCnt;
                    TakeDate = r.ServeDate;

                    od.ServiceList.Add(su);
                }

                ServiceTime.Sort();
                od.BaggageCnt = BCnt;
                od.PassengerCnt = PCnt;


                if (od.PassengerCnt > 3)
                    od.CarType = "七人座";
                else
                    od.CarType = "四人座";

                od.DriverID = 0;
                od.DriverName = "";
                od.FleetID = 0;
                od.CarNo = "";

                od.TimeSegment = info.Detail.ServiceList[0].ServiceTime.Substring(0, 2) + "00";


                od.FisrtServiceTime = ServiceTime[0].ToString("HHmm");
                od.ServiceCnt = info.Detail.ServiceCnt;
                od.ServiceType = info.Detail.ServiceType;
                od.TakeDate = TakeDate;// info.Detail.ServiceDate.Replace("/", "");


                //儲存媒合資料
                try
                {
                    dald.addDispatchSheet(od);
                }
                catch (Exception ex)
                {
                    //寫入媒合紀錄檔
                    JObject jo = JsonConvert.DeserializeObject<JObject>(data);
                    JArray arryDispatch = (JArray)jo["DispatchInfo"];
                    for (int i = 0; i < arryDispatch.Count; i++)
                    {
                        if (Convert.ToInt32(arryDispatch[i]["Index"].ToString()) == info.Index)
                        {
                            SaveMatchError(od.TakeDate, "E", info.Index, arryDispatch[i]["Detail"].ToString(), ex.Message);
                            break;
                        }
                    }
                    continue;
                }

            }

            return sheetCnt;
        }


        public bool SaveMatchError(string date, string Status, int ID, string MatchJson, string ErrMsg)
        {
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            sb.Append("INSERT INTO dispatch_record 	(TakeDate, ExeStatus, MatchupID, Json,ErrorMsg) 	VALUES (@TakeDate,@ExeStatus,@MatchupID,@Json,@ErrorMsg)");
            parms.Add(new MySqlParameter("@TakeDate", date));
            parms.Add(new MySqlParameter("@ExeStatus", Status));
            parms.Add(new MySqlParameter("@MatchupID", ID));
            parms.Add(new MySqlParameter("@Json", MatchJson));
            parms.Add(new MySqlParameter("@ErrorMsg", ErrMsg));
            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._connectionstring))
                {
                    if (conn.executeInsertQuery(sb.ToString(), parms.ToArray()) <= 0)
                        return false;
                    else
                        return true;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }

        public DataTable GetAllDispatchData()
        {
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();

            sb.Append("select s.DispatchNo as '派車單號',  ");
            sb.Append("case s.ServiceType when 'O' then '出國' else '回國' end as '服務類別' ,  ");
            sb.Append(" s.ServeDate as '乘車日', s.DispatchTime as '服務時間',  ");
            sb.Append(" case s.CarpoolFlag when 'S' then '共乘' else '專車' end as '乘車類別',  ");
            sb.Append(" case s.CanPick when '1' then '可揀' else '不可揀' end as '揀車類別', ");
            sb.Append("  s.PassengerCnt as '乘客人數', s.BaggageCnt as '行李數量', s.StopCnt as '服務組數',  ");
            sb.Append("  s.CarType as '車型', s.CarNo as '車號',u.UserName as '司機名稱', ");
            sb.Append("d.ScheduleServeTime as '訂單服務時間',  ");
            sb.Append("d.ReservationNo as '訂單編號', concat(a.LastName, a.FirstName) as '訂車人名稱',  ");
            sb.Append("r.TimeSegment as '預約時間',  ");
            sb.Append("case r.CarpoolFlag when 'S' then '共乘' else '專車' end as '預約類別',  ");
            sb.Append("r.FlightNo as '航班編號', r.PickupAddress as '上車地址', r.TakeoffAddress as '下車地址',  ");
            sb.Append("r.PassengerCnt as '訂單人數', r.BaggageCnt as '訂單行李數', r.AddBaggageCnt as '臨加行李數', ");
            sb.Append("r.Price as '金額', r.Coupon as '優惠碼', r.InvoiceData as '發票', r.PassengerName as '乘車人姓名', r.PassengerPhone as '乘車人手機' ");
            sb.Append("from dispatch_sheet s, dispatch_reservation d, reservation_sheet r, user_info u, account a ");
            sb.Append("where s.DispatchNo = d.DispatchNo and d.ReservationNo = r.ReservationNo ");
            sb.Append("and s.DriverID = u.UserID and r.UserID = a.UserID and s.ServeDate >= date_format(now(), '%Y%m%d') ");
            sb.Append("order by s.ServeDate, s.DispatchTime, d.ScheduleServeTime asc ");
            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._connectionstring))
                {
                    return conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                }
            }
            catch (MySqlException e)
            {
                throw e;
            }
        }

        public DataTable GetAllReservationData()
        {
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();

            sb.Append("SELECT ReservationNo as '訂單邊號', case ServiceType when 'O' then '出國' else '回國' end as '服務類別' , ");
            sb.Append("TakeDate as '乘車日期', TimeSegment as '預約時間', FlightNo as '航班編號', ");
            sb.Append("PickupAddress as '上車地址', TakeoffAddress as '下車地址', PassengerCnt as '乘車人數', ");
            sb.Append("BaggageCnt as '行李數', AddBaggageCnt as '臨加行李', Price as '金額', Coupon as '優惠碼', InvoiceData as '發票', ");
            sb.Append("case ProcessStage  when '0' then '一般' when 'A' then '媒合' when 'Z' then '結帳' else '取消' end as '狀態', PassengerName as '乘客姓名', PassengerPhone as '乘客電話', CreateTime as '新增時間' ");
            sb.Append("FROM reservation_sheet ");
            sb.Append("where takedate >= date_format(now(), '%Y%m%d') ");
            sb.Append("order by takedate, timesegment  asc ");
            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._connectionstring))
                {
                    return conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                }
            }
            catch (MySqlException e)
            {
                throw e;
            }
        }

        //Datatable轉存CSV
        public void SaveToCSV(DataTable oTable, string FilePath)
        {
            string data = "";
            StreamWriter wr = new StreamWriter(FilePath, false, System.Text.Encoding.Default);
            foreach (DataColumn column in oTable.Columns)
            {
                data += column.ColumnName + ",";
            }
            data += "\n";
            wr.Write(data);
            data = "";

            foreach (DataRow row in oTable.Rows)
            {
                foreach (DataColumn column in oTable.Columns)
                {
                    if (row[column] == DBNull.Value)
                        data += " " + ",";
                    else
                        data += row[column].ToString().Trim() + ",";
                }
                data += "\n";
                wr.Write(data);
                data = "";
            }
            data += "\n";

            wr.Dispose();
            wr.Close();
        }

        public class OMachSourceData
        {
            public OMachSourceData()
            {

                this.ReservationList = new List<OMatchRData>();
            }

            public string MatchDate { get; set; }
            public List<OMatchRData> ReservationList { get; set; }
        }

        public class OMatchRData
        {
            public string ReservationNo { get; set; }
            public string ServiceType { get; set; }
            public string TimeSegment { get; set; }
            public string MatchRegion { get; set; }
            public double PickupLat { get; set; }
            public double PickupLng { get; set; }
            public double TakeoffLat { get; set; }
            public double TakeoffLng { get; set; }
            public int PassengerCnt { get; set; }
            public int BaggageCnt { get; set; }
            public string MaxFlag { get; set; }
            public string FlightNo { get; set; }
            public DateTime FlightTime { get; set; }
            public string mStartTime { get; set; }
            public string mEndTime { get; set; }
        }
    }

    /// <summary>
    /// 發送EMAIL
    /// </summary>
    public class GmailSMTP
    {
        private System.Net.Mail.SmtpClient MySmtp;
        private string Account;
        private string Password;

        public GmailSMTP(string Account, string Password)
        {
            this.Account = Account;
            this.Password = Password;
            /*
              *  outlook.com smtp.live.com port:25
              *  yahoo smtp.mail.yahoo.com.tw port:465
             */
            //建立 SmtpClient 物件 並設定 Gmail的smtp主機及Port 
            MySmtp = new System.Net.Mail.SmtpClient("smtp.gmail.com", 587);
            //設定你的帳號密碼            
            MySmtp.Credentials = new System.Net.NetworkCredential(this.Account, this.Password);
            //Gmial 的 smtp 使用 SSL
            MySmtp.EnableSsl = true;
        }

        public string Send(List<string> MailList, string DisplayName, string Subject, string Body, List<System.Net.Mail.Attachment> Attach)
        {
            try
            {
                System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();
                //收件者，以逗號分隔不同收件者 ex "test@gmail.com,test2@gmail.com"
                msg.To.Add(string.Join(",", MailList.ToArray()));
                msg.From = new System.Net.Mail.MailAddress(this.Account, DisplayName, System.Text.Encoding.UTF8);
                //郵件標題 
                msg.Subject = Subject;
                //郵件標題編碼  
                msg.SubjectEncoding = System.Text.Encoding.UTF8;
                //郵件內容
                msg.Body = Body;
                msg.IsBodyHtml = true;
                msg.BodyEncoding = System.Text.Encoding.UTF8;//郵件內容編碼 
                msg.Priority = System.Net.Mail.MailPriority.Normal;//郵件優先級 
                if (Attach != null && Attach.Count > 0)
                {
                    foreach (System.Net.Mail.Attachment a in Attach)
                        msg.Attachments.Add(a);
                }

                MySmtp.Send(msg);
                return "Success";
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }

}
