﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using DbConn;
using CallCar.Reservation;
using CallCar.Account;
using NLog;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.IO;
using CallCar.Data;
using System.Windows.Forms;
using System.Net;
using System.Web;

namespace CustomerNotification
{
    class Program
    {
        private static string ConnectionString;
        private static string VIPConnectionString;
        //private static string ResponseURL;
        private static Logger logger = LogManager.GetCurrentClassLogger();


        static void Main(string[] args)
        {
            logger.Info("===============" + DateTime.Now.ToString("yyyy-MM-dd HH:mm") + "開始執行=============");
            NameValueCollection nc = ConfigurationManager.AppSettings;

            if (nc["Server"] == "Production")
            {
                //ResponseURL = @"https://api.ishareco.com/BAPI/SMSReport";
                ConnectionString = "Server=10.0.2.23;Port=3306;Database=carpoollab;Uid=ishareco;Pwd=fu0!j0$su0^vup#;Allow User Variables=True";
                VIPConnectionString = "Data Source=112.121.69.1;Initial Catalog=CRMS_v2;Persist Security Info=True;User ID=sa;password=2Axijoll"; ;


            }
            else if (nc["Server"] == "Lab")
            {
                //ResponseURL = @"http://apid.ishareco.com/BAPI/SMSReport";
                ConnectionString = "Server=61.63.55.200;Port=3306;Database=carpoollab;Uid=apiuser;Pwd=7oPOh88I;Allow User Variables=True";
                VIPConnectionString = "";
            }

            DataTable dtRecord = GetNoticeRecord();
            for (int i = 0; i < dtRecord.Rows.Count; i++)
            {
                int SeqNo = Convert.ToInt32(dtRecord.Rows[i]["SeqNo"]);
                string Code = Convert.ToString(dtRecord.Rows[i]["ProgramCode"]);
                int Freq = Convert.ToInt32(dtRecord.Rows[i]["Frequency"]);
                DateTime LastTime = Convert.ToDateTime(dtRecord.Rows[i]["LastExeTime"]);
                string unit = Convert.ToString(dtRecord.Rows[i]["Unit"]);
                DateTime NowTime = DateTime.Now;
                bool NeedExec = false;

                switch (unit)
                {
                    case "D":
                        if (DateTime.Compare(NowTime.AddDays(-1 * Freq), LastTime) > 0)
                            NeedExec = true;
                        break;
                    case "M":
                        if (DateTime.Compare(NowTime.AddMinutes(-1 * Freq), LastTime) > 0)
                            NeedExec = true;
                        break;
                    case "H":
                        if (DateTime.Compare(NowTime.AddHours(-1 * Freq), LastTime) > 0)
                            NeedExec = true;
                        break;
                    default:
                        NeedExec = false;
                        break;
                }

                if (NeedExec)
                {
                    switch (Code)
                    {
                        case "AddBagNotice":
                            try
                            {
                                AddBagNotice();
                            }
                            catch (Exception)
                            {

                            }
                            break;
                        case "SIMPromote":


                            break;
                        default:
                            break;
                    }
                }
            }





            logger.Info("===============執行結束=============");
        }

        #region 臨加行李通知
        private static void AddBagNotice()
        {
            DataTable dt = new DataTable();
            BALReservation br = new BALReservation(ConnectionString);
            BALAccount ba = new BALAccount(ConnectionString);
            CallCar.Tool.GmailSMTP smtp = new CallCar.Tool.GmailSMTP("info@ishareco.com", "share52439256i");
            MiTakeSMS MiTak = new MiTakeSMS();
            try
            {
                dt = GetAddBagList();
            }
            catch (MySqlException ex)
            {
                logger.Error("臨加行李執行錯誤(" + DateTime.Now.ToString("yyyy-MM-dd HH:mm") + ")：讀取清單失敗：" + ex.Message);
                return;
            }
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {

                    //發送簡訊
                    string MessageTemplate = "{0}共乘服務{1}，通知您超額行李{2}件，需加收{3}元，APP預約為主動扣款，若透過經銷商請循原付費管道加購，謝謝您！艾雪敬上";
                    //{0} 日期 MM-dd {1}時間 HH:mm
                    string ReservationNo = Convert.ToString(dt.Rows[i]["ReservationNo"]);
                    int SeqNo = Convert.ToInt32(dt.Rows[i]["SeqNo"]);
                    OReservation or;
                    OAccount oa;
                    try
                    {
                        or = br.GetReservation(ReservationNo);
                        oa = ba.GetUserAccount(or.UserID);
                    }
                    catch (Exception ex)
                    {
                        logger.Error("臨加行李執行錯誤(" + DateTime.Now.ToString("yyyy-MM-dd HH:mm") + ")：取訂單會員資料失敗(" + ReservationNo + ")：" + ex.Message);
                        continue;
                    }

                    string Msg = string.Format(MessageTemplate, or.ServeDate.Substring(4, 2) + "-" + or.ServeDate.Substring(6, 2), or.PreferTime.Substring(0, 2) + ":" + or.PreferTime.Substring(2, 2), or.AddBagCnt.ToString(), (or.AddBagCnt * 100).ToString());
                    MiTak.SMSBody = new List<MiTakeSMS.SMSBodyStruct>();
                    MiTakeSMS.SMSBodyStruct smsBody = new MiTakeSMS.SMSBodyStruct();
                    smsBody.Body = Msg;
                    smsBody.Name = "";
                    smsBody.Number = or.PassengerPhone;
                    smsBody.Reference = or.ReservationNo;

                    MiTak.SMSBody.Add(smsBody);

                    //發送失敗則先繼續 下次再發一次
                    if (!MiTak.SendSingleSMS())
                    {
                        logger.Error("臨加行李執行錯誤(" + DateTime.Now.ToString("yyyy-MM-dd HH:mm") + ")：單號：" + ReservationNo + "寄送SMS失敗(" + or.PassengerPhone + ")");
                        continue;
                    }
                    if (oa.Email == "www@www")
                        return;
                    string HtmlBody = "";
                    string filepath = "";
                    string filename = "";
                    string Subject;

                    filename = "callcar_addbag_email_zhtw.html";

                    filepath = System.IO.Path.GetDirectoryName(Application.ExecutablePath) + @"\template\" + filename;

                    try
                    {
                        if (File.Exists(filepath))
                        {
                            //讀取檔案
                            using (StreamReader streamReader = new StreamReader(filepath, Encoding.GetEncoding("utf-8")))
                            {
                                //轉字串給輸出
                                HtmlBody = streamReader.ReadToEnd();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        logger.Error("臨加行李執行錯誤(" + DateTime.Now.ToString("yyyy-MM-dd HH:mm") + ")讀取EMAIL Template失敗：" + ex.Message);
                        continue;
                    }

                    //發送Email
                    try
                    {
                        HtmlBody = HtmlBody.Replace("{0}", ReservationNo);
                        HtmlBody = HtmlBody.Replace("{1}", or.ServeDate.Substring(0, 4) + "-" + or.ServeDate.Substring(4, 2) + "-" + or.ServeDate.Substring(6, 2));
                        HtmlBody = HtmlBody.Replace("{2}", or.PreferTime.Substring(0, 2) + "：" + or.PreferTime.Substring(2, 2));
                        HtmlBody = HtmlBody.Replace("{3}", or.PassengerCnt.ToString());
                        HtmlBody = HtmlBody.Replace("{4}", or.BaggageCnt.ToString());
                        HtmlBody = HtmlBody.Replace("{5}", or.AddBagCnt.ToString());
                        HtmlBody = HtmlBody.Replace("{6}", or.AddBagCnt.ToString());
                        HtmlBody = HtmlBody.Replace("{7}", (or.AddBagCnt * 100).ToString());
                        Subject = "CallCar機場共乘 臨加行李通知信";

                        List<string> MailAddress = new List<string>();
                        MailAddress.Add(oa.Email);

                        //發送失敗則先繼續 下次再發一次
                        if (smtp.Send(MailAddress, "艾雪科技客服通知", Subject, HtmlBody, null) == "Success")
                            SendEmailComplete(SeqNo);
                        else
                        {
                            logger.Error("臨加行李執行錯誤(" + DateTime.Now.ToString("yyyy-MM-dd HH:mm") + ")：單號：" + ReservationNo + "寄送Email失敗(" + oa.Email + ")");
                            continue;
                        }
                    }
                    catch (Exception ex)
                    {
                        logger.Error("臨加行李執行錯誤(" + DateTime.Now.ToString("yyyy-MM-dd HH:mm") + ")：" + ex.Message);
                        continue;
                    }
                }
            }
        }
        #endregion

        #region SIM卡通知
        private void SIMPromote(DateTime NowTime)
        {
            DataTable dtVIP, dtShare;
            dtVIP = GetCallcarOutboundList(NowTime.AddDays(1).ToString("yyyyMMdd"));
            dtShare = GetiSharecoOutboundList(NowTime.AddDays(1).ToString("yyyyMMdd"));
            List<SIMList> List = new List<SIMList>();
            string Name, Tel;
            string Msg = "您好，要出國了，來不及準備網卡嗎？即日起CallCar車上有機會買到多國上網SIM卡，熱門國家大都有涵蓋且可跨國使用！價格也低於電信商喔！";
            if (dtVIP != null && dtVIP.Rows.Count > 0)
            {
                foreach (DataRow dr in dtVIP.Rows)
                {
                    if (Convert.ToString(dr["DriverType"]).Trim() == "自有")
                    {
                        Name = Convert.ToString(dr["MbrName"]);
                        Tel = Convert.ToString(dr["Tel1"]);
                        if (Tel.Length == 10)
                            List.Add(new SIMList(Name, Tel));
                    }
                }
            }
            if (dtShare != null && dtShare.Rows.Count > 0)
            {
                foreach (DataRow dr in dtShare.Rows)
                {
                    Name = Convert.ToString(dr["PassengerName"]);
                    Tel = Convert.ToString(dr["PassengerPhone"]);
                    if (Tel.Length == 10)
                        List.Add(new SIMList(Name, Tel));
                }
            }

            if (List.Count > 0)
            {
                foreach(SIMList sim in List)
                {
                    MiTakeSMS sms = new MiTakeSMS();
                    sms.SMSBody = new List<MiTakeSMS.SMSBodyStruct>();
                    MiTakeSMS.SMSBodyStruct smsBody = new MiTakeSMS.SMSBodyStruct();
                    smsBody.Body = Msg;
                    smsBody.Name = "";
                    smsBody.Number = sim.MobilePhone;
                    smsBody.Reference = "";

                    sms.SMSBody.Add(smsBody);

                    //發送失敗則先繼續 下次再發一次
                    if (!sms.SendSingleSMS())
                    {
                        logger.Error("臨加行李執行錯誤(" + DateTime.Now.ToString("yyyy-MM-dd HH:mm") + ")：手機號碼：" + sim.MobilePhone + "寄送SMS失敗");
                        continue;
                    }
                }
            }
        }
        #endregion

        #region 取資料
        private static DataTable GetNoticeRecord()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("SELECT SeqNo, ProgramCode,  Frequency, Unit, LastExeTime, ActiveFlag	FROM callcar_program_exec_record where ActiveFlag = '1' ");
            List<MySqlParameter> parms = new List<MySqlParameter>();

            DataTable dt;
            using (dbConnectionMySQL conn = new dbConnectionMySQL(ConnectionString))
            {
                dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
            }

            return dt;
        }
        private static DataTable GetAddBagList()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("SELECT SeqNo, ReservationNo 	FROM reservation_email_addbag Where SendFlag = '0'");
            List<MySqlParameter> parms = new List<MySqlParameter>();

            DataTable dt;
            using (dbConnectionMySQL conn = new dbConnectionMySQL(ConnectionString))
            {
                dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
            }

            return dt;
        }

        private static DataTable GetCallcarOutboundList(string date)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("SELECT r.Sno,r.ServiceName,p.MbrName,p.Tel1,r.ServiceTime1,r.ServiceTime1_Hour,u.EmpName ,convert(CHAR(8),r.ServiceTime1,112) AS servicedate,u.DriverType  ");
            sb.Append("FROM CRM013 r, CRM011 p, Users u ");
            sb.Append("WHERE r.MbrId = p.MbrId and r.DriverEmpId = u.EmpId AND u.tCarTeams_No = '54657828' AND r.ServiceName = '出國' ");
            sb.Append("AND convert(CHAR(8), r.ServiceTime1, 112) = @date ");
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("@date", date));
            DataTable dt;
            using (dbConnectionMySQL conn = new dbConnectionMySQL(ConnectionString))
            {
                dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
            }

            return dt;
        }

        private static DataTable GetiSharecoOutboundList(string date)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("select PassengerName, PassengerPhone from reservation_sheet where  ProcessStage in ('0','A') and ServiceType = 'O' and TakeDate = @date ");
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("@date", date));
            DataTable dt;
            using (dbConnectionMySQL conn = new dbConnectionMySQL(ConnectionString))
            {
                dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
            }

            return dt;
        }
        private static void SendEmailComplete(int SeqNo)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("Update reservation_email_addbag Set SendFlag = '1' ,SendTime = Now()  Where  SeqNo = @no");
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("@no", SeqNo));
            using (dbConnectionMySQL conn = new dbConnectionMySQL(ConnectionString))
            {
                conn.executeUpdateQuery(sb.ToString(), parms.ToArray());
            }
        }
        #endregion

        public class MiTakeSMS
        {
            public MiTakeSMS()
            {
                this.UserName = "52439256";
                this.Password = "ishareco.com";
                this.SMSBody = new List<SMSBodyStruct>();
            }

            public bool SendSingleSMS()
            {
                if (this.UserName == "" || this.Password == "" || this.SMSBody.Count == 0)
                    return false;

                string URL = @"http://smexpress.mitake.com.tw:9600/SmSendGet.asp?username={0}&password={1}&dstaddr={2}&encoding=UTF8&smbody={3}&response={4}";

                foreach (SMSBodyStruct body in SMSBody)
                {
                    string RequestURL = string.Format(URL, this.UserName, this.Password, body.Number, HttpUtility.UrlEncode(body.Body), "");
                    HttpWebRequest req = (HttpWebRequest)HttpWebRequest.Create(RequestURL);
                    req.Method = "GET";
                    string msgid = "", statuscode = "";

                    using (WebResponse wr = req.GetResponse())
                    {
                        using (var responseStream = wr.GetResponseStream())
                        {
                            using (var responseReader = new StreamReader(responseStream))
                            {
                                string str = responseReader.ReadLine();

                                while (str != null)
                                {
                                    if (str.IndexOf("msgid") >= 0)
                                    {
                                        msgid = str.Split('=')[1];
                                    }
                                    else if (str.IndexOf("statuscode") >= 0)
                                    {
                                        statuscode = str.Split('=')[1];
                                    }
                                    str = responseReader.ReadLine();
                                }
                            }
                        }
                    }

                    if (msgid != "")
                        return true;
                    else
                        return false;

                }
                return true;
            }

            public string UserName { get; set; }
            public string Password { get; set; }
            public List<SMSBodyStruct> SMSBody { get; set; }

            public class SMSBodyStruct
            {
                public string Name { get; set; }
                public string Number { get; set; }
                public string Body { get; set; }
                public string Reference { get; set; }

            }

        }

        public class SIMList
        {
            public SIMList(string name, string tel)
            {
                this.MbrName = name;
                this.MobilePhone = tel;
            }
            public string MbrName { get; set; }
            public string MobilePhone { get; set; }
        }
    }
}
