﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using MySql.Data.MySqlClient;
using MySql.Data.Types;
using System.Data;
using Newtonsoft.Json;
using Callcar;
using NLog;

namespace PreTransCallcarConsole
{
    class Program
    {
        private const string ConnectionString = "Data Source=112.121.69.1;Initial Catalog=CRMS_v2;Persist Security Info=True;User ID=sa;password=2Axijoll";
        private const string MySqlConnectionString = "Server=10.0.2.21;Port=3306;Database=carpoollab;Uid=apiuser;Pwd=7oPOh88I;Allow User Variables=True";
        //private const string MySqlConnectionStringProduct = "Server=10.0.2.20;Port=3306;Database=carpoollab;Uid=ishareco;Pwd=fu0!j0$su0^vup#;Allow User Variables=True";
        private static Logger logger = NLog.LogManager.GetCurrentClassLogger();

        static void Main(string[] args)
        {
            logger.Info("轉檔開始");
            DataTable dt = new DataTable();
            StringBuilder sb = new StringBuilder();
            List<Reservation> List = new List<Reservation>();
            using (SqlConnection conn = new SqlConnection(ConnectionString))
            {
                try
                {
                    conn.Open();
                }
                catch (SqlException ex)
                {
                    logger.Error("和相資料庫連線失敗" + ex.Message);
                }
                sb.Append("SELECT Sno,ServiceName, ");
                sb.Append("convert(CHAR(8),ServiceTime1,112) AS ServiceDate,ServiceTime1_Hour,  ");
                sb.Append("(SELECT AreaName2 FROM CD003 WHERE AreaId = AreaNameA1) AS A1,  ");
                sb.Append("(SELECT AreaName2 FROM CD003 WHERE AreaId = AreaNameA2) AS A2,  ");
                sb.Append("AddressA,  ");
                sb.Append("FlightNo,FlightTime,Terminal,CarTypeName,BaggageCnt,PassengerCnt,  ");
                sb.Append("SpecifiedTime  ");
                sb.Append("FROM CRM013 ");
                sb.Append("WHERE convert(CHAR(8),ServiceTime1,112) >= convert(CHAR(8),getdate(),112) ");
                sb.Append("AND AreaNameA1 IN ('25000','04000','06000') ");
                sb.Append("AND Employer <> 'JCB高鐵升等' AND Terminal <> '尚無選擇'  AND Terminal <> '松山' ");
                sb.Append("AND FlightNo <> ''  AND ReturnFlag = 0  AND Cancel = 0 AND CloseFlag NOT IN ( '4','10') ");
                sb.Append("Order by ServiceTime1,ServiceTime1_Hour ASC ");
                using (SqlCommand cmd = new SqlCommand(sb.ToString(), conn))
                {
                    using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                    {
                        da.Fill(dt);
                    }
                }
            }

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                Reservation r = new Reservation();
                r.uid = 100;
              //  string TimeSegment = "", TakeDate = "";
                DateTime FlightTime = Convert.ToDateTime(dt.Rows[i]["FlightTime"]);

                if (Convert.ToString(dt.Rows[i]["ServiceName"]) == "出國")
                {
                    r.stype = "O";
                    r.sdate = FlightTime.AddMinutes(-150).Date.ToString("yyyyMMdd");
                    r.ptime = FlightTime.AddMinutes(-150).ToString("HHmm");
                }
                else
                {
                    r.stype = "I";
                    r.sdate = FlightTime.AddMinutes(90).Date.ToString("yyyyMMdd");
                    r.ptime = FlightTime.AddMinutes(90).ToString("HHmm");
                }
             //   r.sdate = Convert.ToString(dt.Rows[i]["ServiceDate"]);
//                r.ptime = Convert.ToString(dt.Rows[i]["ServiceTime1_Hour"]).Substring(0, 2) + "00";
                
                if (Convert.ToString(dt.Rows[i]["SpecifiedTime"]) == "True")
                {
                    r.ctype = "T";
                    r.fno = null;
                    r.ftime = null;
                    r.fdate = null;
                }
                else
                {
                    r.ctype = "F";
                    r.fno = Convert.ToString(dt.Rows[i]["FlightNo"]);
                    r.ftime = FlightTime.ToString("HHmm"); //Convert.ToString(dt.Rows[i]["FlightTime"]).Replace(":", "");
                    r.fdate = FlightTime.ToString("yyyyMMdd");
                }
                r.pcartype = "0";
                string Outputstring;
                if (r.stype == "O")
                {
                    r.pcity = Convert.ToString(dt.Rows[i]["A1"]);
                    r.pdistinct = Convert.ToString(dt.Rows[i]["A2"]);
                    r.paddress = Convert.ToString(dt.Rows[i]["AddressA"]);
                    if (r.paddress.Contains("("))
                    {
                        int index = r.paddress.IndexOf("(");
                        r.paddress = r.paddress.Substring(0, index).Trim();
                    }
                    Outputstring = Query(r.pcity + r.pdistinct + r.paddress);
                    TGOSJson t = JsonConvert.DeserializeObject<TGOSJson>(Outputstring);
                    try
                    {
                        if (t.AddressList == null)
                            continue;
                        r.plat = t.AddressList[0].Y;
                        r.plng = t.AddressList[0].X;
                    }
                    catch (Exception)
                    {
                        continue;
                    }
                    r.tcity = "桃園市";
                    r.tdistinct = "大園區";
                    r.taddress = "桃園國際機場";
                    if (Convert.ToString(dt.Rows[i]["Terminal"]) == "桃T2")
                    {
                        r.taddress += " 第二航廈";
                        r.tlat = 25.076821;
                        r.tlng = 121.231683;
                    }
                    else
                    {
                        r.taddress += " 第一航廈";
                        r.tlat = 25.076821;
                        r.tlng = 121.231683;
                    }
                }
                else
                {
                    r.tcity = Convert.ToString(dt.Rows[i]["A1"]);
                    r.tdistinct = Convert.ToString(dt.Rows[i]["A2"]);
                    r.taddress = Convert.ToString(dt.Rows[i]["AddressA"]);
                    if (r.taddress.Contains("("))
                    {
                        int index = r.taddress.IndexOf("(");
                        r.taddress = r.taddress.Substring(0, index).Trim();
                    }
                    Outputstring = Query(r.tcity + r.tdistinct + r.taddress);
                    TGOSJson t = JsonConvert.DeserializeObject<TGOSJson>(Outputstring);
                    try
                    {
                        if (t.AddressList == null)
                            continue;
                        r.tlat = t.AddressList[0].Y;
                        r.tlng = t.AddressList[0].X;
                    }
                    catch (Exception)
                    {
                        continue;
                    }
                    r.pcity = "桃園市";
                    r.pdistinct = "大園區";
                    r.paddress = "桃園國際機場";
                    if (Convert.ToString(dt.Rows[i]["Terminal"]) == "桃T2")
                    {
                        r.paddress += " 第二航廈";
                        r.plat = 25.076821;
                        r.plng = 121.231683;
                    }
                    else
                    {
                        r.paddress += " 第一航廈";
                        r.plat = 25.076821;
                        r.plng = 121.231683;
                    }
                }


                string CarType = Convert.ToString(dt.Rows[i]["CarTypeName"]).Trim();
                if (CarType == "四人座" || CarType == "高級車")
                    r.pcnt = 1;
                else
                    r.pcnt = 2;

                r.bcnt = r.pcnt;
                r.Sno = Convert.ToString(dt.Rows[i]["Sno"]);

                List.Add(r);
            }
            string OrderNo = string.Empty;

            foreach (Reservation r in List)
            {
                Order o = new Order();
                o.OrderNo = r.Sno;
                OrderNo = o.OrderNo;
                o.BaggageCnt = r.bcnt;
                o.ChooseType = r.ctype;
                o.Coupon = "";

                o.PickCartype = r.pcartype;
                if (o.ChooseType == "F" && r.fno != null)
                {
                    o.FlightNo = r.fno.Replace("-", "");
                    o.FlightTime = ConvertToDateTime(r.fdate + r.ftime, 12);
                }
                o.Invoice = "";
                o.Credit = "";
                o.PassengerCnt = r.pcnt;
                o.PickupCity = r.pcity;
                o.PickupDistinct = r.pdistinct;
                o.PickupAddress = r.paddress;
                o.PickupVillage = null;
                if (r.plat == 0)
                    o.PickupGPS = null;
                else
                    o.PickupGPS = new SpatialGPS(r.plat, r.plng);

                o.PreferTime = r.ptime;
                o.ProcessStage = "0";
                o.ServeDate = r.sdate;
                //o.ServiceResult = "0";
                o.ServiceType = r.stype;
                o.TakeoffAddress = r.taddress;
                o.TakeoffCity = r.tcity;
                o.TakeoffDistinct = r.tdistinct;
                o.TakeoffVillage = null;
                if (r.tlat == 0)
                    o.TakeoffGPS = null;
                else
                    o.TakeoffGPS = new SpatialGPS(r.tlat, r.tlng);
                o.UserID = r.uid;
                Callcar.BAL.BALGeneral bal = new Callcar.BAL.BALGeneral(MySqlConnectionString);
                //string[] Fee = bal.CalculateTrailFee(DateTime.Now.ToString("yyyyMMdd"), o.ServeDate, o.PreferTime, o.PassengerCnt, o.BaggageCnt, "", 0, 0).Split('_');
                //int CarFee, NightFee, BagFee, PromotFee;
                //CarFee = Convert.ToInt32(Fee[0]);
                //NightFee = Convert.ToInt32(Fee[1]);
                //BagFee = Convert.ToInt32(Fee[2]);
                //PromotFee = Convert.ToInt32(Fee[3]);

                o.Price = 0;


                using (MySqlConnection conn = new MySqlConnection(MySqlConnectionString))
                {
                    try
                    {
                        conn.Open();
                    }
                    catch (MySqlException ex)
                    {
                        logger.Error("LAB MySQL資料庫連線失敗" + ex.Message);
                        Environment.Exit(0);
                    }

                    sb.Length = 0;
                    sb.Append("INSERT INTO reservation_sheet  ");
                    sb.Append("(ReservationNo,UserID, ServiceType, TakeDate, TimeSegment, SelectionType,PickCartype, FlightNo, ScheduleFlightTime,  ");
                    sb.Append("PickupCity, PickupDistinct,PickupVillage, PickupAddress,PickupGPS, TakeoffCity, TakeoffDistinct,TakeoffVillage, TakeoffAddress, TakeoffGPS, PassengerCnt,   ");
                    sb.Append("BaggageCnt, Coupon, InvoiceData,CreditData, ProcessStage,Price)  ");
                    sb.Append("VALUES (@ReservationNo,@UserID, @ServiceType, @TakeDate, @TimeSegment, @SelectionType, @PickCartype,@FlightNo, @ScheduleFlightTime,  ");
                    sb.Append("@PickupCity, @PickupDistinct,@PickupVillage, @PickupAddress,GeomFromText(@PickupGPS), @TakeoffCity, @TakeoffDistinct, @TakeoffVillage, @TakeoffAddress, GeomFromText(@TakeoffGPS), @PassengerCnt,   ");
                    sb.Append("@BaggageCnt, @Coupon, @InvoiceData,@CreditData, @ProcessStage,@Price)  ");

                    using (MySqlCommand cmd = new MySqlCommand(sb.ToString(), conn))
                    {

                        cmd.Parameters.AddWithValue("@ReservationNo", o.OrderNo);
                        cmd.Parameters.AddWithValue("@UserID", o.UserID);
                        cmd.Parameters.AddWithValue("@ServiceType", o.ServiceType);
                        cmd.Parameters.AddWithValue("@TakeDate", o.ServeDate);
                        cmd.Parameters.AddWithValue("@TimeSegment", o.PreferTime);
                        cmd.Parameters.AddWithValue("@SelectionType", o.ChooseType);
                        cmd.Parameters.AddWithValue("@PickCartype", o.PickCartype);
                        cmd.Parameters.AddWithValue("@FlightNo", o.FlightNo);
                        cmd.Parameters.AddWithValue("@ScheduleFlightTime", o.FlightTime);
                        cmd.Parameters.AddWithValue("@PickupCity", o.PickupCity);
                        cmd.Parameters.AddWithValue("@PickupDistinct", o.PickupDistinct);
                        cmd.Parameters.AddWithValue("@PickupVillage", o.PickupVillage);
                        cmd.Parameters.AddWithValue("@PickupAddress", o.PickupAddress);
                        if (o.PickupGPS != null)
                            cmd.Parameters.AddWithValue("@PickupGPS", "Point(" + o.PickupGPS.GetLng().ToString() + " " + o.PickupGPS.GetLat().ToString() + ")");
                        else
                            cmd.Parameters.AddWithValue("@PickupGPS", "Point(0 0)");
                        cmd.Parameters.AddWithValue("@TakeoffCity", o.TakeoffCity);
                        cmd.Parameters.AddWithValue("@TakeoffDistinct", o.TakeoffDistinct);
                        cmd.Parameters.AddWithValue("@TakeoffVillage", o.TakeoffVillage);
                        cmd.Parameters.AddWithValue("@TakeoffAddress", o.TakeoffAddress);
                        if (o.TakeoffGPS != null)
                            cmd.Parameters.AddWithValue("@TakeoffGPS", "Point(" + o.TakeoffGPS.GetLng().ToString() + " " + o.TakeoffGPS.GetLat().ToString() + ")");
                        else
                            cmd.Parameters.AddWithValue("@TakeoffGPS", "Point(0 0)");
                        cmd.Parameters.AddWithValue("@PassengerCnt", o.PassengerCnt);
                        cmd.Parameters.AddWithValue("@BaggageCnt", o.BaggageCnt);
                        cmd.Parameters.AddWithValue("@Coupon", o.Coupon);
                        cmd.Parameters.AddWithValue("@InvoiceData", o.Invoice);
                        cmd.Parameters.AddWithValue("@CreditData", o.Credit);
                        cmd.Parameters.AddWithValue("@ProcessStage", o.ProcessStage);
                        cmd.Parameters.AddWithValue("@Price", o.Price);
                        try
                        {
                            cmd.ExecuteNonQuery();
                            UpdCRMS(OrderNo);
                        }
                        catch (MySqlException ex)
                        {
                            if (ex.Number == 1062)
                            {
                                UpdCRMS(OrderNo);
                                continue;
                            }
                            else
                            {
                                logger.Error("LAB 新增訂單失敗" + ex.Number + " " + ex.Message);
                                UpdCRMS(OrderNo);
                                continue;
                            }
                        }

                        ////先同步新增到正式區
                        //using (MySqlConnection connP = new MySqlConnection(MySqlConnectionStringProduct))
                        //{
                        //    try
                        //    {
                        //        connP.Open();
                        //        cmd.Connection = connP;
                        //        cmd.ExecuteNonQuery();

                        //    }
                        //    catch (MySqlException ex)
                        //    {
                        //        logger.Error("Production 新增訂單失敗" + ex.Number + " " + ex.Message);

                        //    }

                        //}
                    }


                    //

                    // sb.Length = 0;


                    //將資料寫入待追蹤航班表中 
                    //寫失敗(duplicate key)就算了

                    //if (o.FlightNo != null && o.FlightNo != "")
                    //{
                    //    string AirlineID, FlightNumber, FT;

                    //    if (o.FlightNo.Length <= 2)
                    //        continue;
                    //    AirlineID = o.FlightNo.Substring(0, 2);
                    //    FlightNumber = o.FlightNo.Substring(2, o.FlightNo.Length - 2);
                    //    if (o.ServiceType == "I")
                    //        FT = "PA";
                    //    else
                    //        FT = "PD";

                    //    sb.Append("INSERT INTO flight_track (FlightDate, AirlineID,FlightNo,FlightTime,FlightType)VALUES (@FlightDate,@AirlineID,@FlightNo,@FlightTime,@FlightType )");
                    //    using (MySqlCommand cmd = new MySqlCommand(sb.ToString(), conn))
                    //    {
                    //        cmd.Parameters.AddWithValue("@FlightDate", o.FlightTime.ToString("yyyyMMdd"));
                    //        cmd.Parameters.AddWithValue("@AirlineID", AirlineID);
                    //        cmd.Parameters.AddWithValue("@FlightNo", FlightNumber);
                    //        cmd.Parameters.AddWithValue("@FlightTime", o.FlightTime);
                    //        cmd.Parameters.AddWithValue("@FlightType", FT);

                    //        try
                    //        {
                    //            cmd.ExecuteNonQuery();
                    //        }
                    //        catch (MySqlException ex)
                    //        {
                    //            if (ex.Number == 1062)
                    //            {

                    //            }
                    //            else
                    //            {
                    //                logger.Info("訂單：" + o.OrderNo + "新增航班追蹤失敗" + ex.Number + " " + ex.Message);
                    //            }
                    // }

                    //先同步新增到正式區
                    //using (MySqlConnection connP = new MySqlConnection(MySqlConnectionStringProduct))
                    //{
                    //    try
                    //    {
                    //        connP.Open();
                    //        cmd.Connection = connP;
                    //        cmd.ExecuteNonQuery();

                    //    }
                    //    catch (MySqlException ex)
                    //    {
                    //        logger.Error("Production 新增航班失敗" + ex.Number + " " + ex.Message);

                    //    }
                    //}
                    //    }
                    //}
                    //logger.Info("訂單：" + o.OrderNo + "完成轉檔");
                }
            }
            logger.Info(DateTime.Now.ToString("yyyy-MM-dd") + "完成轉檔");
        }

        private static string Query(string aAddress)
        {
            //所要查詢的門牌地址
            //string aAddress = "台北市內湖區瑞光路76巷43號";
            //坐標系統(SRS)EPSG:4326(WGS84)國際通用, EPSG:3825 (TWD97TM119) 澎湖及金馬適用,EPSG:3826 (TWD97TM121) 台灣地區適用,EPSG:3827 (TWD67TM119) 澎湖及金馬適用,EPSG:3828 (TWD67TM121) 台灣地區適用
            string aSRS = "EPSG:4326";
            //0:最近門牌號機制,1:單雙號機制,2:[最近門牌號機制]+[單雙號機制]
            int aFuzzyType = 0;
            //回傳的資料格式，允許傳入的代碼為：JSON、XML
            string aResultDataType = "JSON";
            //模糊比對回傳門牌號的許可誤差範圍，輸入格式為正整數，如輸入 0 則代表不限制誤差範圍
            int aFuzzyBuffer = 0;
            //是否只進行完全比對，允許傳入的值為：true、false，如輸入 true ，模糊比對機制將不被使用
            string aIsOnlyFullMatch = "false";
            //是否鎖定縣市，允許傳入的值為：true、false，如輸入 true ，則代表查詢結果中的 [縣市] 要與所輸入的門牌地址中的 [縣市] 完全相同
            string aIsLockCounty = "false";
            //是否鎖定鄉鎮市區，允許傳入的值為：true、false，如輸入 true ，則代表查詢結果中的 [鄉鎮市區] 要與所輸入的門牌地址中的 [鄉鎮市區] 完全相同
            string aIsLockTown = "false";
            //是否鎖定村里，允許傳入的值為：true、false，如輸入 true ，則代表查詢結果中的 [村里] 要與所輸入的門牌地址中的 [村里] 完全相同
            string aIsLockVillage = "false";
            //是否鎖定路段，允許傳入的值為：true、false，如輸入 true ，則代表查詢結果中的 [路段] 要與所輸入的門牌地址中的 [路段] 完全相同
            string aIsLockRoadSection = "false";
            //是否鎖定巷，允許傳入的值為：true、false，如輸入 true ，則代表查詢結果中的 [巷] 要與所輸入的門牌地址中的 [巷] 完全相同
            string aIsLockLane = "false";
            //是否鎖定弄，允許傳入的值為：true、false，如輸入 true ，則代表查詢結果中的 [弄] 要與所輸入的門牌地址中的 [弄] 完全相同
            string aIsLockAlley = "false";
            //是否鎖定地區，允許傳入的值為：true、fals，如輸入 true ，則代表查詢結果中的 [地區] 要與所輸入的門牌地址中的 [地區] 完全相同
            string aIsLockArea = "false";
            //號之、之號是否視為相同，允許傳入的值為：true、false
            string aIsSameNumber_SubNumber = "false";
            //TGOS_Query_Addr_Url WebService網址
            string aCanIgnoreVillage = "true";
            //找不時是否可忽略村里，允許傳入的值為：true、false
            string aCanIgnoreNeighborhood = "true";
            //找不時是否可忽略鄰，允許傳入的值為：true、false
            int aReturnMaxCount = 0;
            //如為多筆時，限制回傳最大筆數，輸入格式為正整數，如輸入 0 則代表不限制回傳筆數





            //使用string.Format給予參數設定
            string param = @"oAPPId={0}&oAPIKey={1}&oAddress={2}&oSRS={3}&oFuzzyType={4}
                         &oResultDataType={5}&oFuzzyBuffer={6}&oIsOnlyFullMatch={7}&oIsLockCounty={8}
                         &oIsLockTown={9}&oIsLockVillage={10}&oIsLockRoadSection={11}&oIsLockLane={12}
                         &oIsLockAlley={13}&oIsLockArea={14}&oIsSameNumber_SubNumber={15}&oCanIgnoreVillage={16}&oCanIgnoreNeighborhood={17}&oReturnMaxCount={18}";

            //給予需要的參數
            param = string.Format(param,

                  //應用程式識別碼(APPId) 實由使用者向TGOS申請
                  QueryAddr.QueryAddraAppId,
                  //應用程式介接驗證碼(APIKey)由使用者向TGOS申請
                  QueryAddr.QueryAddrAPIKey,
                  aAddress,
                  aSRS,
                  aFuzzyType,
                  aResultDataType,
                  aFuzzyBuffer,
                  aIsOnlyFullMatch,
                  aIsLockCounty,
                  aIsLockTown,
                  aIsLockVillage,
                  aIsLockRoadSection,
                  aIsLockLane,
                  aIsLockAlley,
                  aIsLockArea,
                  aIsSameNumber_SubNumber,
                  aCanIgnoreVillage,
                  aCanIgnoreNeighborhood,
                  aReturnMaxCount
              );
            string Outputstring = string.Empty;
            Outputstring = QueryAddr.QueryAddrResult(param, QueryAddr.Query_Addr_Url + "/QueryAddr?", Outputstring);
            return Outputstring;
        }
        private static DateTime ConvertToDateTime(string input, int length)
        {
            if (length == 8)
                return DateTime.Parse(input.Substring(0, 4) + "/" + input.Substring(4, 2) + "/" + input.Substring(6, 2));
            else if (length == 12)
                return DateTime.Parse(input.Substring(0, 4) + "/" + input.Substring(4, 2) + "/" + input.Substring(6, 2) + " " + input.Substring(8, 2) + ":" + input.Substring(10, 2));
            else
                throw new Exception("Convert Length Error");

        }

        private static void UpdCRMS(string OrderNo)
        {
            StringBuilder sb = new StringBuilder();
            sb.Length = 0;

            using (SqlConnection conn = new SqlConnection(ConnectionString))
            {
                sb.Append("Update CRM013 Set ReturnFlag = 1 Where Sno = @no");
                using (SqlCommand cmd = new SqlCommand(sb.ToString(), conn))
                {
                    cmd.Parameters.AddWithValue("@no", OrderNo);
                    conn.Open();
                    try
                    {
                        cmd.ExecuteNonQuery();
                    }
                    catch (SqlException)
                    {

                    }
                }
            }
        }
        public class Reservation
        {
            public string Sno { get; set; } //callcar訂單號
            public int uid { get; set; }  //用戶序號
            public string stype { get; set; } //出國或回國
            public string sdate { get; set; } //乘車日期
            public string ptime { get; set; } //預約時段
            public string ctype { get; set; } //航班或航廈
            public string pcartype { get; set; } //預約或撿車
            public string fno { get; set; }  //航班編號
            public string fdate { get; set; } //航班日期
            public string ftime { get; set; } //航班時間
            public string pcity { get; set; } //上車地點
            public string pdistinct { get; set; } //上車地點
            public string pvillage { get; set; }
            public string paddress { get; set; } //上車地點
            public double plat { get; set; }
            public double plng { get; set; }
            public string tcity { get; set; } //上車地點
            public string tdistinct { get; set; } //上車地點
            public string tvillage { get; set; }
            public string taddress { get; set; } //上車地點
            public double tlat { get; set; }
            public double tlng { get; set; }
            public int pcnt { get; set; } //人數
            public int bcnt { get; set; } //行李數量
        }

        public class Order
        {
            public string OrderNo { get; set; } //訂單編號
            public int UserID { get; set; }  //用戶序號
            public string ServiceType { get; set; } //出國或回國
            public string ServeDate { get; set; } //乘車日期
            public string PreferTime { get; set; } //預約時段
            public string ChooseType { get; set; } //航班或航廈
            public string PickCartype { get; set; } //預約或撿趟
            public string FlightNo { get; set; }  //航班編號
            public string FlightDate { get; set; } //航班日期
            public DateTime FlightTime { get; set; } //航班時間
            public string PickupCity { get; set; } //上車地點
            public string PickupDistinct { get; set; } //上車地點
            public string PickupVillage { get; set; }
            public string PickupAddress { get; set; } //上車地點
            public SpatialGPS PickupGPS { get; set; }
            public string TakeoffCity { get; set; } //上車地點
            public string TakeoffDistinct { get; set; } //上車地點
            public string TakeoffVillage { get; set; }
            public string TakeoffAddress { get; set; } //上車地點
            public SpatialGPS TakeoffGPS { get; set; }
            public int PassengerCnt { get; set; } //人數
            public int BaggageCnt { get; set; } //行李數量
            public string Coupon { get; set; } //優惠
            public string Invoice { get; set; } //發票資料
            public string Credit { get; set; } //信用卡資料
            public DateTime OrderTime { get; set; } //下訂時間
            public string ProcessStage { get; set; } //處理階段
            public string ServiceResult { get; set; } //服務狀況
            public int Price { get; set; } //金額

        }

        public class SpatialGPS
        {
            public SpatialGPS(double lat, double lng)
            {
                this.lat = lat;
                this.lng = lng;
            }
            public double GetLat()
            {
                return this.lat;
            }

            public double GetLng()
            {
                return this.lng;
            }
            private double lat { get; set; }
            private double lng { get; set; }


        }
        public class TGOSJson
        {
            public List<Info> Info { get; set; }
            public List<AddressList> AddressList { get; set; }
        }


        public class Info
        {
            public string IsSuccess { get; set; }
            public string InAddress { get; set; }
            public string InSRS { get; set; }
            public string InFuzzyType { get; set; }
            public string InFuzzyBuffer { get; set; }
            public string InIsOnlyFullMatch { get; set; }
            public string InIsLockCounty { get; set; }
            public string InIsLockTown { get; set; }
            public string InIsLockVillage { get; set; }
            public string InIsLockRoadSection { get; set; }
            public string InIsLockLane { get; set; }
            public string InIsLockAlley { get; set; }
            public string InIsLockArea { get; set; }
            public string InIsSameNumber_SubNumber { get; set; }
            public string InCanIgnoreVillage { get; set; }
            public string InCanIgnoreNeighborhood { get; set; }
            public string InReturnMaxCount { get; set; }
            public string OutTotal { get; set; }
            public string OutMatchType { get; set; }
            public string OutMatchCode { get; set; }
            public string OutTraceInfo { get; set; }
        }

        public class AddressList
        {
            public string FULL_ADDR { get; set; }
            public string COUNTY { get; set; }
            public string TOWN { get; set; }
            public string VILLAGE { get; set; }
            public string NEIGHBORHOOD { get; set; }
            public string ROAD { get; set; }
            public string SECTION { get; set; }
            public string LANE { get; set; }
            public string ALLEY { get; set; }
            public string SUB_ALLEY { get; set; }
            public string TONG { get; set; }
            public string NUMBER { get; set; }
            public double X { get; set; }
            public double Y { get; set; }
        }
    }
}
