﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Text;
using System.ComponentModel;
using System.Configuration;
using System.IO;
using System.Net;
using System.Collections.Specialized;
//using HttpUtils;

using CallCar.Driver;
//using Callcar.BAL;
//using Callcar.Struct;
//using Callcar.CObject;
//using Callcar.Common;
using CallCar.Data;
using CallCar.Account;
using CallCar.Dispatch;
using CallCar.Reservation;
using CallCar.Tool;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Runtime.Serialization;

namespace Airport_Carpool_DAPI
{

    [ServiceContract]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]

    public class DAPI
    {
        public static string ConnectionString = ConfigurationManager.ConnectionStrings["CarpoolLab"].ConnectionString;
        public const string _iv = "icallcar84177282";
        public const string _key = "ishareco52439256";

        #region OLD Android
        //[Description("帳密驗證")]
        //[OperationContract, WebInvoke(UriTemplate = "chkAcc", Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        //public string VarifyAccountPW(Stream input)
        //{
        //    RtnStruct r = new RtnStruct();
        //    string data = string.Empty;
        //    string token = string.Empty;

        //    data = MultiParse(input);
        //    if (data == "Error")
        //    {
        //        r.RtnObject = "Parse資料錯誤";
        //        r.Status = "Error";
        //        return JsonConvert.SerializeObject(r);
        //    }
        //    // token = Callcar.Common.AESCryptography.TokenAnalysis(ref data);


        //    try
        //    {
        //        data = DeCrypt(data, out token);
        //        BALEmployee bal = new BALEmployee(ConnectionString);
        //        AccountPWStruct ps = JsonConvert.DeserializeObject<AccountPWStruct>(data);
        //        if (bal.ChkAccPWAPP(ps.acc, ps.pw))
        //            r.RtnObject = "True";
        //        else
        //            r.RtnObject = "False";

        //        r.Status = "Success";

        //        return Callcar.Common.General.OutputCrypt(JsonConvert.SerializeObject(r), token);
        //    }
        //    catch (Exception ex)
        //    {
        //        r.Status = "Error";
        //        r.RtnObject = ex.Message;
        //        return Callcar.Common.General.OutputCrypt(JsonConvert.SerializeObject(r), token);
        //    }
        //}

        //[Description("取得司機資料")]
        //[OperationContract, WebInvoke(UriTemplate = "getAcc", Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        //public string GetAccountByID(Stream input)
        //{
        //    RtnStruct r = new RtnStruct();
        //    string data = string.Empty;
        //    string token = string.Empty;

        //    data = MultiParse(input);
        //    if (data == "Error")
        //    {
        //        r.RtnObject = "Parse資料錯誤";
        //        r.Status = "Error";
        //        return JsonConvert.SerializeObject(r);
        //    }

        //    try
        //    {
        //        data = DeCrypt(data, out token);

        //        APIParms json = JsonConvert.DeserializeObject<APIParms>(data);

        //        int UserID = 0;
        //        //if (!Int32.TryParse(, out UserID))
        //        //{
        //        //    r.Status = "Error";
        //        //    r.RtnObject = "資料錯誤";
        //        //    return JsonConvert.SerializeObject(r);
        //        //}
        //        string CitizenID = json.param1;
        //        Callcar.DAL.DALEmployee dal = new Callcar.DAL.DALEmployee(ConnectionString);
        //        UserID = dal.GetEmployeeIDByCitizenID(CitizenID);
        //        if (UserID != 0)
        //        {
        //            BALEmployee bal = new BALEmployee(ConnectionString);
        //            DriverInfoStruct ds = bal.GetDriverInfo(UserID);
        //            if (ds != null)
        //                r.RtnObject = ds;
        //            else
        //                r.RtnObject = null;

        //            r.Status = "Success";
        //        }
        //        else
        //        {
        //            r.Status = "Error";
        //            r.RtnObject = "Wrong Account Password";
        //        }

        //        return Callcar.Common.General.OutputCrypt(JsonConvert.SerializeObject(r), token);
        //    }
        //    catch (Exception ex)
        //    {
        //        r.Status = "Error";
        //        r.RtnObject = ex.Message;
        //        return Callcar.Common.General.OutputCrypt(JsonConvert.SerializeObject(r), token);
        //    }
        //}

        //[Description("取得公告資料  data:傳入資料 (尚無)")]
        //[OperationContract, WebInvoke(UriTemplate = "getAnnDAPP", Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        //public string GetAnnounce(Stream input)
        //{
        //    RtnStruct r = new RtnStruct();
        //    //string token = string.Empty;
        //    string data = string.Empty;

        //    try
        //    {
        //        data = MultiParse(input);
        //        if (data == "Error")
        //        {
        //            r.RtnObject = "Parse資料錯誤";
        //            r.Status = "Error";
        //            return JsonConvert.SerializeObject(r);
        //        }

        //    }
        //    catch (Exception)
        //    {
        //        r.RtnObject = "資料錯誤";
        //        r.Status = "Error";
        //        return JsonConvert.SerializeObject(r);
        //    }

        //    try
        //    {
        //        //data = DeCrypt(data, out token);
        //        //if (data == "keyError")
        //        //{
        //        //    r.RtnObject = "Key資料錯誤";
        //        //    r.Status = "Error";
        //        //    return JsonConvert.SerializeObject(r);
        //        //}
        //        APIParms json = JsonConvert.DeserializeObject<APIParms>(data);
        //        int FleetNo = Convert.ToInt32(json.param1);
        //        string CitizenID = json.param2;
        //        Callcar.DAL.DALEmployee dal = new Callcar.DAL.DALEmployee(ConnectionString);
        //        int EmployeeID = dal.GetEmployeeIDByCitizenID(CitizenID);

        //        BALGeneral bal = new BALGeneral(ConnectionString);
        //        List<OBullletin> ob = bal.GetShowBulletin("D", FleetNo, EmployeeID);
        //        List<Bulletin> rb = new List<Bulletin>();
        //        int serial = 0;
        //        foreach (OBullletin b in ob)
        //        {
        //            serial++;
        //            Bulletin bulletin = new Bulletin();
        //            bulletin.Serial = serial;
        //            bulletin.PublishTime = b.CreateTime;
        //            string CreateTime = b.CreateTime.ToString("yyyy/MM/dd HH:mm");
        //            string Content = b.Content;
        //            int TargetID = b.EmployeeID;
        //            string ToTarget = "";

        //            if (TargetID == -1)
        //                ToTarget = "[全體]";
        //            else if (TargetID == -100)
        //                ToTarget = "[車隊]";
        //            else
        //                ToTarget = "[個人]";

        //            Content = CreateTime + " " + ToTarget + "<br />" + Content;

        //            if (Content.Contains("https") || Content.Contains("http"))
        //            {
        //                int endIndex = 0;
        //                int index = Content.IndexOf("https");

        //                if (index == -1)
        //                    index = Content.IndexOf("http");

        //                if (index == -1)
        //                    continue;
        //                string Hyperlink;
        //                string CompleteHyperlink;
        //                endIndex = Content.IndexOf(" ", index + 1);
        //                if (endIndex == -1)
        //                {
        //                    endIndex = Content.Length - 1;

        //                    Hyperlink = Content.Substring(index, endIndex - index + 1);
        //                    CompleteHyperlink = "<a href='" + Hyperlink + "'>點此連結</a>";
        //                    bulletin.Content = Content.Substring(0, index) + CompleteHyperlink;
        //                }
        //                else
        //                {
        //                    Hyperlink = Content.Substring(index, endIndex - index);
        //                    CompleteHyperlink = "<a href='" + Hyperlink + "'>點此連結</a>";
        //                    bulletin.Content = Content.Substring(0, index) + CompleteHyperlink + Content.Substring(endIndex, Content.Length - endIndex);
        //                }

        //            }
        //            else
        //                bulletin.Content = Content;

        //            rb.Add(bulletin);
        //        }

        //        r.RtnObject = rb;

        //        r.Status = "Success";

        //        //return Callcar.Common.General.OutputCrypt(JsonConvert.SerializeObject(r), token);
        //        return JsonConvert.SerializeObject(r);
        //    }
        //    catch (Exception ex)
        //    {
        //        r.Status = "Error";
        //        r.RtnObject = ex.Message;
        //        //return Callcar.Common.General.OutputCrypt(JsonConvert.SerializeObject(r), token);
        //        return JsonConvert.SerializeObject(r);
        //    }
        //}

        //[Description("取得時間內待服務清單")]
        //[OperationContract, WebInvoke(UriTemplate = "getUSDAPP", Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        //public string GetUnServeSheets(Stream input)
        //{
        //    RtnStruct r = new RtnStruct();
        //    string token = string.Empty;
        //    string data = string.Empty;

        //    try
        //    {
        //        data = MultiParse(input);
        //        if (data == "Error")
        //        {
        //            r.RtnObject = "Parse資料錯誤";
        //            r.Status = "Error";
        //            return JsonConvert.SerializeObject(r);
        //        }

        //    }
        //    catch (Exception)
        //    {
        //        r.RtnObject = "資料錯誤";
        //        r.Status = "Error";
        //        return JsonConvert.SerializeObject(r);
        //    }

        //    try
        //    {
        //        data = DeCrypt(data, out token);

        //        APIParms json = JsonConvert.DeserializeObject<APIParms>(data);

        //        Callcar.BAL.BALDispatch bal = new BALDispatch(ConnectionString);
        //        Callcar.BAL.BALUserAccount balu = new BALUserAccount(ConnectionString);
        //        Callcar.BAL.BALReservation balr = new BALReservation(ConnectionString);
        //        Callcar.DAL.DALEmployee dal = new Callcar.DAL.DALEmployee(ConnectionString);
        //        int DriverID = dal.GetEmployeeIDByCitizenID(json.param1);

        //        if (DriverID == 0)
        //        {
        //            r.RtnObject = "司機資料錯誤";
        //            r.Status = "Error";
        //            return JsonConvert.SerializeObject(r);
        //        }
        //        List<ODispatchSheet> Lists = bal.GetDispatchSheetByDriver(DriverID);

        //        if (Lists != null && Lists.Count > 0)
        //        {
        //            List<APIDispatchStruct> ListA = new List<APIDispatchStruct>();
        //            //OAccount oa;
        //            foreach (ODispatchSheet s in Lists)
        //            {
        //                APIDispatchStruct a = new APIDispatchStruct();
        //                a.bc = s.BaggageCnt;
        //                a.dno = s.DispatchNo;
        //                a.pc = s.PassengerCnt;
        //                a.sc = s.ServiceCnt;
        //                List<DispatchService> _asList = new List<DispatchService>();
        //                foreach (ODpServiceUnit u in s.ServiceList)
        //                {
        //                    DispatchService _as = new DispatchService();
        //                    _as.aa = u.AirportAddress;
        //                    _as.at = u.ActualTime;
        //                    _as.bc = u.BaggageCnt;
        //                    _as.fno = u.FlightNo;
        //                    _as.lat = u.ServiceGPS.GetLat();
        //                    _as.lng = u.ServiceGPS.GetLng();
        //                    _as.ma = u.MainAddress;
        //                    _as.pc = u.PassengerCnt;
        //                    _as.rno = u.ReservationNo;
        //                    _as.so = u.ServiceOrder;
        //                    _as.sr = u.ServiceRemark;
        //                    _as.st = u.ScheduleTime;
        //                    _as.bag = u.AddBaggageCnt;
        //                    OReservation or = balr.GetReservationByNo(u.ReservationNo);
        //                    //oa = balu.GetAccountInfo(UID);
        //                    _as.gn = or.PassengerName;
        //                    _as.mb = or.PassengerPhone;
        //                    _asList.Add(_as);
        //                }
        //                a.sl = _asList;
        //                a.st = s.ServiceType;
        //                a.td = Callcar.Common.General.ConvertToDateTime(s.TakeDate, 8).ToString("yyyy/MM/dd");
        //                a.re = s.ServiceRemark;
        //                a.uf = s.UpdFlag;

        //                ListA.Add(a);
        //            }
        //            r.RtnObject = ListA;
        //        }
        //        else
        //            r.RtnObject = null;

        //        r.Status = "Success";

        //        return Callcar.Common.General.OutputCrypt(JsonConvert.SerializeObject(r), token);
        //    }
        //    catch (Exception ex)
        //    {
        //        r.Status = "Error";
        //        r.RtnObject = ex.Message;
        //        return Callcar.Common.General.OutputCrypt(JsonConvert.SerializeObject(r), token);
        //    }
        //}

        //[Description("取得時間內完成清單")]
        //[OperationContract, WebInvoke(UriTemplate = "getSRDDAPP", Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        //public string GetServedSheets(Stream input)
        //{
        //    RtnStruct r = new RtnStruct();
        //    string token = string.Empty;
        //    string data = string.Empty;

        //    try
        //    {
        //        data = MultiParse(input);
        //        if (data == "Error")
        //        {
        //            r.RtnObject = "Parse資料錯誤";
        //            r.Status = "Error";
        //            return JsonConvert.SerializeObject(r);
        //        }

        //    }
        //    catch (Exception)
        //    {
        //        r.RtnObject = "資料錯誤";
        //        r.Status = "Error";
        //        return JsonConvert.SerializeObject(r);
        //    }

        //    try
        //    {
        //        data = DeCrypt(data, out token);

        //        APIParms json = JsonConvert.DeserializeObject<APIParms>(data);

        //        Callcar.BAL.BALDispatch bal = new BALDispatch(ConnectionString);
        //        Callcar.BAL.BALUserAccount balu = new BALUserAccount(ConnectionString);
        //        Callcar.BAL.BALReservation balr = new BALReservation(ConnectionString);
        //        Callcar.DAL.DALEmployee dal = new Callcar.DAL.DALEmployee(ConnectionString);
        //        int DriverID = dal.GetEmployeeIDByCitizenID(json.param1);

        //        if (DriverID == 0)
        //        {
        //            r.RtnObject = "司機資料錯誤";
        //            r.Status = "Error";
        //            return JsonConvert.SerializeObject(r);
        //        }
        //        List<ODispatchSheet> Lists = bal.GetServedDispatchSheetByDriver(DriverID);

        //        if (Lists != null && Lists.Count > 0)
        //        {
        //            List<APIDispatchStruct> ListA = new List<APIDispatchStruct>();
        //            //int UID = 0;
        //            //OAccount oa;
        //            foreach (ODispatchSheet s in Lists)
        //            {
        //                APIDispatchStruct a = new APIDispatchStruct();
        //                a.bc = s.BaggageCnt;
        //                a.dno = s.DispatchNo;
        //                a.pc = s.PassengerCnt;
        //                a.sc = s.ServiceCnt;
        //                List<DispatchService> _asList = new List<DispatchService>();
        //                foreach (ODpServiceUnit u in s.ServiceList)
        //                {
        //                    DispatchService _as = new DispatchService();
        //                    _as.aa = u.AirportAddress;
        //                    _as.at = u.ActualTime;
        //                    _as.bc = u.BaggageCnt;
        //                    _as.fno = u.FlightNo;
        //                    _as.lat = u.ServiceGPS.GetLat();
        //                    _as.lng = u.ServiceGPS.GetLng();
        //                    _as.ma = u.MainAddress;
        //                    _as.pc = u.PassengerCnt;
        //                    _as.rno = u.ReservationNo;
        //                    _as.so = u.ServiceOrder;
        //                    _as.sr = u.ServiceRemark;
        //                    _as.st = u.ScheduleTime;
        //                    _as.bag = u.AddBaggageCnt;
        //                    OReservation or = balr.GetReservationByNo(u.ReservationNo);
        //                    //oa = balu.GetAccountInfo(UID);
        //                    _as.gn = or.PassengerName;
        //                    _as.mb = or.PassengerPhone;
        //                    _asList.Add(_as);
        //                }
        //                a.sl = _asList;
        //                a.st = s.ServiceType;
        //                a.td = Callcar.Common.General.ConvertToDateTime(s.TakeDate, 8).ToString("yyyy/MM/dd");
        //                a.re = s.ServiceRemark;
        //                a.uf = s.UpdFlag;

        //                ListA.Add(a);
        //            }
        //            r.RtnObject = ListA;
        //        }
        //        else
        //            r.RtnObject = null;

        //        r.Status = "Success";

        //        return Callcar.Common.General.OutputCrypt(JsonConvert.SerializeObject(r), token);
        //    }
        //    catch (Exception ex)
        //    {
        //        r.Status = "Error";
        //        r.RtnObject = ex.Message;
        //        return Callcar.Common.General.OutputCrypt(JsonConvert.SerializeObject(r), token);
        //    }
        //}

        //[Description("取得單一筆服務單")]
        //[OperationContract, WebInvoke(UriTemplate = "getSDAPP", Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        //public string GetSingleSheet(Stream input)
        //{
        //    RtnStruct r = new RtnStruct();
        //    string data = string.Empty;
        //    string token = string.Empty;
        //    try
        //    {
        //        data = MultiParse(input);
        //        if (data == "Error")
        //        {
        //            r.RtnObject = "Parse資料錯誤";
        //            r.Status = "Error";
        //            return JsonConvert.SerializeObject(r);
        //        }
        //    }
        //    catch (Exception)
        //    {
        //        r.RtnObject = "資料錯誤";
        //        r.Status = "Error";
        //        return JsonConvert.SerializeObject(r);
        //    }

        //    try
        //    {
        //        string Dno = string.Empty;
        //        Dno = DeCrypt(data, out token);

        //        Callcar.BAL.BALDispatch bal = new BALDispatch(ConnectionString);
        //        Callcar.BAL.BALUserAccount balu = new BALUserAccount(ConnectionString);
        //        Callcar.BAL.BALReservation balr = new BALReservation(ConnectionString);

        //        ODispatchSheet sheet = bal.GetDispatchSheetByNo(Dno);

        //        if (sheet != null)
        //        {
        //            // int UID = 0;
        //            //OAccount oa;
        //            APIDispatchStruct a = new APIDispatchStruct();
        //            a.bc = sheet.BaggageCnt;
        //            a.dno = sheet.DispatchNo;
        //            a.pc = sheet.PassengerCnt;
        //            a.sc = sheet.ServiceCnt;
        //            List<DispatchService> _asList = new List<DispatchService>();
        //            foreach (ODpServiceUnit u in sheet.ServiceList)
        //            {
        //                DispatchService _as = new DispatchService();
        //                _as.aa = u.AirportAddress;
        //                _as.at = u.ActualTime;
        //                _as.bc = u.BaggageCnt;
        //                _as.fno = u.FlightNo;
        //                _as.lat = u.ServiceGPS.GetLat();
        //                _as.lng = u.ServiceGPS.GetLng();
        //                _as.ma = u.MainAddress;
        //                _as.pc = u.PassengerCnt;
        //                _as.rno = u.ReservationNo;
        //                _as.so = u.ServiceOrder;
        //                _as.sr = u.ServiceRemark;
        //                _as.st = u.ScheduleTime;
        //                _as.bag = u.AddBaggageCnt;
        //                OReservation or = balr.GetReservationByNo(u.ReservationNo);
        //                //oa = balu.GetAccountInfo(UID);
        //                _as.gn = or.PassengerName;
        //                _as.mb = or.PassengerPhone;
        //                _asList.Add(_as);
        //            }
        //            a.sl = _asList;
        //            a.st = sheet.ServiceType;
        //            a.td = Callcar.Common.General.ConvertToDateTime(sheet.TakeDate, 8).ToString("yyyy/MM/dd");
        //            a.re = sheet.ServiceRemark;
        //            a.uf = sheet.UpdFlag;

        //            r.RtnObject = a;
        //        }
        //        else
        //            r.RtnObject = null;

        //        r.Status = "Success";

        //        return Callcar.Common.General.OutputCrypt(JsonConvert.SerializeObject(r), token);
        //    }
        //    catch (Exception ex)
        //    {
        //        r.RtnObject = ex.Message;
        //        r.Status = "Error";
        //        return Callcar.Common.General.OutputCrypt(JsonConvert.SerializeObject(r), token);
        //    }
        //}

        //[Description("更新派車單的服務結果(單一)")]
        //[OperationContract, WebInvoke(UriTemplate = "updURSDAPP", Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        //public string UpdateServiceResult(Stream input)
        //{
        //    RtnStruct r = new RtnStruct();
        //    string token = string.Empty;
        //    string data = string.Empty;

        //    try
        //    {
        //        data = MultiParse(input);
        //        if (data == "Error")
        //        {
        //            r.RtnObject = "Parse資料錯誤";
        //            r.Status = "Error";
        //            return JsonConvert.SerializeObject(r);
        //        }

        //    }
        //    catch (Exception)
        //    {
        //        r.RtnObject = "資料錯誤";
        //        r.Status = "Error";
        //        return JsonConvert.SerializeObject(r);
        //    }

        //    try
        //    {

        //        data = DeCrypt(data, out token);
        //        APIParms json = JsonConvert.DeserializeObject<APIParms>(data);
        //        Callcar.DAL.DALEmployee dal = new Callcar.DAL.DALEmployee(ConnectionString);
        //        int DriverID = dal.GetEmployeeIDByCitizenID(json.param2);

        //        if (DriverID == 0)
        //        {
        //            r.RtnObject = "司機資料錯誤";
        //            r.Status = "Error";
        //            return JsonConvert.SerializeObject(r);
        //        }


        //        ODriverServiceRecord odr = new ODriverServiceRecord();

        //        odr.Dno = json.param1;
        //        odr.DriverID = DriverID;
        //        odr.SheetServiceRemark = json.param3;
        //        odr.UpdFlag = json.param4;
        //        odr.Reservation1 = json.param5;
        //        if (!string.IsNullOrWhiteSpace(odr.Reservation1))
        //        {
        //            odr.ServiceRemark1 = json.param6;
        //            if (odr.ServiceRemark1 != "0")
        //            {
        //                odr.ServiceTime1 = DateTime.Parse(json.param7);
        //                odr.addBag1 = Convert.ToInt32(json.param8);
        //            }
        //        }

        //        odr.Reservation2 = json.param9;
        //        if (!string.IsNullOrWhiteSpace(odr.Reservation2))
        //        {
        //            odr.ServiceRemark2 = json.param10;
        //            if (odr.ServiceRemark2 != "0")
        //            {
        //                odr.ServiceTime2 = DateTime.Parse(json.param11);
        //                odr.addBag2 = Convert.ToInt32(json.param12);
        //            }
        //        }

        //        odr.Reservation3 = json.param13;
        //        if (!string.IsNullOrWhiteSpace(odr.Reservation3))
        //        {
        //            odr.ServiceRemark3 = json.param14;
        //            if (odr.ServiceRemark3 != "0")
        //            {
        //                odr.ServiceTime3 = DateTime.Parse(json.param15);
        //                odr.addBag3 = Convert.ToInt32(json.param16);
        //            }
        //        }


        //        BALDispatch bal = new BALDispatch(ConnectionString);
        //        Callcar.BAL.BALUserAccount balu = new BALUserAccount(ConnectionString);
        //        Callcar.BAL.BALReservation balr = new BALReservation(ConnectionString);

        //        if (bal.UpdateServiceStatusByDriver(odr))
        //        {
        //            r.Status = "Success";
        //            ODispatchSheet ods = bal.GetDispatchSheetByNo(odr.Dno);
        //            //int UID = 0;
        //            //OAccount oa;
        //            APIDispatchStruct a = new APIDispatchStruct();
        //            a.bc = ods.BaggageCnt;
        //            a.dno = ods.DispatchNo;
        //            a.pc = ods.PassengerCnt;
        //            a.sc = ods.ServiceCnt;
        //            List<DispatchService> _asList = new List<DispatchService>();
        //            foreach (ODpServiceUnit u in ods.ServiceList)
        //            {
        //                DispatchService _as = new DispatchService();
        //                _as.aa = u.AirportAddress;
        //                _as.at = u.ActualTime;
        //                _as.bc = u.BaggageCnt;
        //                _as.fno = u.FlightNo;
        //                _as.lat = u.ServiceGPS.GetLat();
        //                _as.lng = u.ServiceGPS.GetLng();
        //                _as.ma = u.MainAddress;
        //                _as.pc = u.PassengerCnt;
        //                _as.rno = u.ReservationNo;
        //                _as.so = u.ServiceOrder;
        //                _as.sr = u.ServiceRemark;
        //                _as.st = u.ScheduleTime;
        //                _as.bag = u.AddBaggageCnt;
        //                OReservation or = balr.GetReservationByNo(u.ReservationNo);
        //                //oa = balu.GetAccountInfo(UID);
        //                _as.gn = or.PassengerName;
        //                _as.mb = or.PassengerPhone;
        //                _asList.Add(_as);
        //            }
        //            a.sl = _asList;
        //            a.st = ods.ServiceType;
        //            a.td = Callcar.Common.General.ConvertToDateTime(ods.TakeDate, 8).ToString("yyyy/MM/dd");
        //            a.re = ods.ServiceRemark;
        //            a.uf = ods.UpdFlag;

        //            r.RtnObject = a;
        //        }
        //        else
        //        {
        //            r.Status = "Failure";

        //            r.RtnObject = null;
        //        }
        //        return Callcar.Common.General.OutputCrypt(JsonConvert.SerializeObject(r), token);
        //    }
        //    catch (Exception ex)
        //    {
        //        r.Status = "Error";
        //        r.RtnObject = ex.Message;
        //        return Callcar.Common.General.OutputCrypt(JsonConvert.SerializeObject(r), token);
        //    }
        //}
        #endregion

        #region New雙系統
        [Description("帳密驗證")]
        [OperationContract, WebInvoke(UriTemplate = "IchkAcc", Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public RtnStruct iOSVarifyAccountPW(Stream input)
        {
            RtnStruct r = new RtnStruct();
            string data = string.Empty;
            string token = string.Empty;

            data = new StreamReader(input).ReadToEnd();

            try
            {
                DALDriverDispatch dal = new DALDriverDispatch(ConnectionString);
                string Acc, PW;
                JObject jo = JsonConvert.DeserializeObject<JObject>(data);
                Acc = Convert.ToString(jo["acc"]);
                PW = Convert.ToString(jo["pw"]);
                AESCryptography aes = new AESCryptography(_key, _iv);

                if (Acc == "APPLEDRIVE" && PW == "IPHONE")
                {
                    r.RtnObject = aes.Encrypt("True");
                    r.Status = "Success";
                    return r;
                }


                if (dal.ChkAccPWAPP(Acc, PW))
                    r.RtnObject = aes.Encrypt("True");
                else
                    r.RtnObject = aes.Encrypt("False");

                r.Status = "Success";

                return r;
            }
            catch (Exception ex)
            {
                r.Status = "Error";
                r.RtnObject = ex.Message;
                return r;
            }
        }

        [Description("取得司機資料")]
        [OperationContract, WebInvoke(UriTemplate = "IgetAcc", Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public RtnStruct iOSGetAccountByID(Stream input)
        {
            RtnStruct r = new RtnStruct();
            string data = string.Empty;
            string token = string.Empty;

            data = new StreamReader(input).ReadToEnd();

            try
            {
                APIParms json = JsonConvert.DeserializeObject<APIParms>(data);

                string CitizenID = json.param1;

                BALAccount bala = new BALAccount(ConnectionString);
                OEmployee oe = bala.GetEmployeeInfo(CitizenID);
                if (oe.ID != 0)
                {
                    DriverInfoStruct ds = new DriverInfoStruct();
                    ds.fname = "";
                    ds.fno = oe.FleetID;
                    ds.id = oe.ID;
                    ds.name = oe.Name;
                    AESCryptography aes = new AESCryptography(_key, _iv);
                    if (ds != null)
                        r.RtnObject = aes.Encrypt(JsonConvert.SerializeObject(ds));
                    else
                        r.RtnObject = null;

                    r.Status = "Success";
                }
                else
                {
                    r.Status = "Error";
                    r.RtnObject = "Wrong Driver Data";
                }

                return r;
            }
            catch (Exception ex)
            {
                r.Status = "Error";
                r.RtnObject = ex.Message;
                return r;
            }
        }

        [Description("取得待服務清單")]
        [OperationContract, WebInvoke(UriTemplate = "IgetUSDAPP", Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        public RtnStruct iOSGetUnServeSheets(Stream input)
        {
            RtnStruct r = new RtnStruct();
            string token = string.Empty;
            string data = string.Empty;

            data = new StreamReader(input).ReadToEnd();

            try
            {
                //data = DeCrypt(data, out token);

                APIParms json = JsonConvert.DeserializeObject<APIParms>(data);

                BALDispatch baldp = new BALDispatch(ConnectionString);
                BALAccount bala = new BALAccount(ConnectionString);
                BALReservation balr = new BALReservation(ConnectionString);
                DALDriverDispatch dald = new DALDriverDispatch(ConnectionString);
                OEmployee oe = bala.GetEmployeeInfo(json.param1);

                if (oe.ID == 0)
                {
                    r.RtnObject = "司機資料錯誤";
                    r.Status = "Error";
                    return r;
                }
                List<ODriverDispatchSheet> Lists = dald.GetDispatchSheetByDriver(oe.ID);

                if (Lists != null && Lists.Count > 0)
                {
                    List<APIDispatchStruct> ListA = new List<APIDispatchStruct>();

                    foreach (ODriverDispatchSheet s in Lists)
                    {
                        APIDispatchStruct a = new APIDispatchStruct();
                        a.bc = s.BaggageCnt;
                        a.dno = s.DispatchNo;
                        a.pc = s.PassengerCnt;
                        a.sc = s.ServiceCnt;
                        List<DispatchService> _asList = new List<DispatchService>();
                        foreach (ODriverServiceUnit u in s.ServiceList)
                        {
                            DispatchService _as = new DispatchService();
                            _as.aa = u.AirportAddress;
                            _as.at = u.ActualTime;
                            _as.bc = u.BaggageCnt;
                            _as.fno = u.FlightNo;
                            _as.lat = u.ServiceGPS.GetLat();
                            _as.lng = u.ServiceGPS.GetLng();
                            _as.ma = u.MainAddress;
                            _as.pc = u.PassengerCnt;
                            _as.rno = u.ReservationNo;
                            _as.so = u.ServiceOrder;
                            _as.sr = u.ServiceRemark;
                            _as.st = u.ScheduleTime;
                            _as.bag = u.AddBaggageCnt;
                            OReservation or = balr.GetReservation(u.ReservationNo);
                            if (or != null)
                            {
                                _as.gn = or.PassengerName;
                                _as.mb = or.PassengerPhone;
                            }
                            _asList.Add(_as);
                        }
                        a.sl = _asList;
                        a.st = s.ServiceType;
                        a.td = General.ConvertToDateTime(s.TakeDate, 8).ToString("yyyy/MM/dd");
                        a.re = s.ServiceRemark;
                        a.uf = s.UpdFlag;
                        a.cartype = s.CarpoolFlag;

                        ListA.Add(a);
                    }
                    AESCryptography aes = new AESCryptography(_key, _iv);
                    r.RtnObject = aes.Encrypt(JsonConvert.SerializeObject(ListA));

                }
                else
                    r.RtnObject = null;

                r.Status = "Success";

                return r;
            }
            catch (Exception ex)
            {
                r.Status = "Error";
                r.RtnObject = ex.Message;

                return r;
            }
        }

        [Description("取得時間內已上傳服務完成清單")]
        [OperationContract, WebInvoke(UriTemplate = "IgetSRDDAPP", Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public RtnStruct iOSGetServedSheets(Stream input)
        {
            RtnStruct r = new RtnStruct();
            string token = string.Empty;
            string data = string.Empty;

            data = new StreamReader(input).ReadToEnd();

            try
            {
                APIParms json = JsonConvert.DeserializeObject<APIParms>(data);

                //Callcar.BAL.BALDispatch bal = new BALDispatch(ConnectionString);
                BALDispatch baldp = new BALDispatch(ConnectionString);
                //Callcar.BAL.BALUserAccount balu = new BALUserAccount(ConnectionString);
                BALAccount bala = new BALAccount(ConnectionString);
                //Callcar.BAL.BALReservation balr = new BALReservation(ConnectionString);
                BALReservation balr = new BALReservation(ConnectionString);
                //Callcar.DAL.DALEmployee dal = new Callcar.DAL.DALEmployee(ConnectionString);
                DALDriverDispatch dald = new DALDriverDispatch(ConnectionString);
                OEmployee oe = bala.GetEmployeeInfo(json.param1);

                if (oe.ID == 0)
                {
                    r.RtnObject = "司機資料錯誤";
                    r.Status = "Error";
                    return r;
                }
                List<ODriverDispatchSheet> Lists = dald.GetServedDispatchSheetByDriver(oe.ID);

                if (Lists != null && Lists.Count > 0)
                {
                    List<APIDispatchStruct> ListA = new List<APIDispatchStruct>();

                    foreach (ODriverDispatchSheet s in Lists)
                    {
                        APIDispatchStruct a = new APIDispatchStruct();
                        a.bc = s.BaggageCnt;
                        a.dno = s.DispatchNo;
                        a.pc = s.PassengerCnt;
                        a.sc = s.ServiceCnt;
                        List<DispatchService> _asList = new List<DispatchService>();
                        foreach (ODriverServiceUnit u in s.ServiceList)
                        {
                            DispatchService _as = new DispatchService();
                            _as.aa = u.AirportAddress;
                            _as.at = u.ActualTime;
                            _as.bc = u.BaggageCnt;
                            _as.fno = u.FlightNo;
                            _as.lat = u.ServiceGPS.GetLat();
                            _as.lng = u.ServiceGPS.GetLng();
                            _as.ma = u.MainAddress;
                            _as.pc = u.PassengerCnt;
                            _as.rno = u.ReservationNo;
                            _as.so = u.ServiceOrder;
                            _as.sr = u.ServiceRemark;
                            _as.st = u.ScheduleTime;
                            _as.bag = u.AddBaggageCnt;
                            OReservation or = balr.GetReservation(u.ReservationNo);
                            _as.gn = or.PassengerName;
                            _as.mb = or.PassengerPhone;
                            _asList.Add(_as);
                        }
                        a.sl = _asList;
                        a.st = s.ServiceType;
                        a.td = General.ConvertToDateTime(s.TakeDate, 8).ToString("yyyy/MM/dd");
                        a.re = s.ServiceRemark;
                        a.uf = s.UpdFlag;
                        a.cartype = s.CarpoolFlag;

                        ListA.Add(a);
                    }
                    AESCryptography aes = new AESCryptography(_key, _iv);
                    r.RtnObject = aes.Encrypt(JsonConvert.SerializeObject(ListA));
                }
                else
                    r.RtnObject = null;

                r.Status = "Success";

                return r;
            }
            catch (Exception ex)
            {
                r.Status = "Error";
                r.RtnObject = ex.Message;
                return r;
            }
        }

        [Description("更新派車單的服務結果(單一)")]
        [OperationContract, WebInvoke(UriTemplate = "IupdURSDAPP", Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public RtnStruct iOSUpdateServiceResult(Stream input)
        {
            RtnStruct r = new RtnStruct();
            string token = string.Empty;
            string data = string.Empty;

            data = new StreamReader(input).ReadToEnd();


            try
            {

                APIParms json = JsonConvert.DeserializeObject<APIParms>(data);
                BALAccount bala = new BALAccount(ConnectionString);
                OEmployee oe = bala.GetEmployeeInfo(json.param2);

                if (oe.ID == 0)
                {
                    r.RtnObject = "司機資料錯誤";
                    r.Status = "Error";
                    return r;
                }


                ODriverServiceRecord odr = new ODriverServiceRecord();

                odr.Dno = json.param1;
                odr.DriverID = oe.ID;
                odr.SheetServiceRemark = json.param3;
                odr.UpdFlag = json.param4;
                odr.Reservation1 = json.param5;
                if (!string.IsNullOrWhiteSpace(odr.Reservation1))
                {
                    odr.ServiceRemark1 = json.param6;
                    if (odr.ServiceRemark1 != "0")
                    {
                        odr.ServiceTime1 = DateTime.Parse(json.param7);
                        odr.addBag1 = Convert.ToInt32(json.param8);
                    }
                }

                odr.Reservation2 = json.param9;
                if (!string.IsNullOrWhiteSpace(odr.Reservation2))
                {
                    odr.ServiceRemark2 = json.param10;
                    if (odr.ServiceRemark2 != "0")
                    {
                        odr.ServiceTime2 = DateTime.Parse(json.param11);
                        odr.addBag2 = Convert.ToInt32(json.param12);
                    }
                }

                odr.Reservation3 = json.param13;
                if (!string.IsNullOrWhiteSpace(odr.Reservation3))
                {
                    odr.ServiceRemark3 = json.param14;
                    if (odr.ServiceRemark3 != "0")
                    {
                        odr.ServiceTime3 = DateTime.Parse(json.param15);
                        odr.addBag3 = Convert.ToInt32(json.param16);
                    }
                }


                DALDriverDispatch dal = new DALDriverDispatch(ConnectionString);
                BALReservation balr = new BALReservation(ConnectionString);

                if (dal.UpdateServiceStatusByDriver(odr))
                {
                    r.Status = "Success";
                    ODriverDispatchSheet ods = dal.GetDriverDispatchSheet(odr.Dno);
                    APIDispatchStruct a = new APIDispatchStruct();
                    a.bc = ods.BaggageCnt;
                    a.dno = ods.DispatchNo;
                    a.pc = ods.PassengerCnt;
                    a.sc = ods.ServiceCnt;
                    List<DispatchService> _asList = new List<DispatchService>();
                    foreach (ODriverServiceUnit u in ods.ServiceList)
                    {
                        DispatchService _as = new DispatchService();
                        _as.aa = u.AirportAddress;
                        _as.at = u.ActualTime;
                        _as.bc = u.BaggageCnt;
                        _as.fno = u.FlightNo;
                        _as.lat = u.ServiceGPS.GetLat();
                        _as.lng = u.ServiceGPS.GetLng();
                        _as.ma = u.MainAddress;
                        _as.pc = u.PassengerCnt;
                        _as.rno = u.ReservationNo;
                        _as.so = u.ServiceOrder;
                        _as.sr = u.ServiceRemark;
                        _as.st = u.ScheduleTime;
                        _as.bag = u.AddBaggageCnt;
                        OReservation or = balr.GetReservation(u.ReservationNo);
                        _as.gn = or.PassengerName;
                        _as.mb = or.PassengerPhone;
                        _asList.Add(_as);
                    }
                    a.sl = _asList;
                    a.st = ods.ServiceType;
                    a.td = General.ConvertToDateTime(ods.TakeDate, 8).ToString("yyyy/MM/dd");
                    a.re = ods.ServiceRemark;
                    a.uf = ods.UpdFlag;
                    a.cartype = ods.CarpoolFlag;

                    AESCryptography aes = new AESCryptography(_key, _iv);
                    r.RtnObject = aes.Encrypt(JsonConvert.SerializeObject(a));
                }
                else
                {
                    r.Status = "Error";

                    r.RtnObject = null;
                }
                return r;
            }
            catch (Exception ex)
            {
                r.Status = "Error";
                r.RtnObject = ex.Message;
                return r;
            }
        }
        #endregion


        //private string MultiParse(Stream Input)
        //{
        //    string data;
        //    try
        //    {
        //        HttpMultipartParser parser = new HttpMultipartParser(Input, "form");
        //        if (!parser.Success)
        //            return "Error";

        //        data = parser.Parameters["data"];
        //        return data;
        //    }
        //    catch (Exception)
        //    {
        //        return "Error";
        //    }
        //}

        private string DeCrypt(string CryptedText, out string token)
        {
            string data = CryptedText;
            try
            {
                token = AESCryptography.TokenAnalysis(ref data);
                AESCryptography aes = new AESCryptography(token, _iv);
                return aes.Decrypt(data);
            }
            catch (Exception)
            {
                throw new Exception("密文錯誤");
            }
        }

        #region 加解密測試
        //[Description("測試加密")]
        //[OperationContract, WebInvoke(UriTemplate = "cryp", Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        //public string TestCry(Stream input)
        //{
        //    string data = new StreamReader(input).ReadToEnd();
        //    RtnStruct r = new RtnStruct();
        //    OCrypto oc = JsonConvert.DeserializeObject<OCrypto>(data);
        //    try
        //    {
        //        Callcar.Common.AESCryptography cry = new Callcar.Common.AESCryptography(oc.Key, _iv);
        //        return cry.Encrypt(oc.text);
        //    }
        //    catch (Exception ex)
        //    {
        //        return ex.StackTrace;
        //    }
        //}
        //[Description("測試解密")]
        //[OperationContract, WebInvoke(UriTemplate = "decryp", Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        //public string TestDecry(Stream input)
        //{
        //    string data = new StreamReader(input).ReadToEnd();
        //    RtnStruct r = new RtnStruct();
        //    OCrypto oc = JsonConvert.DeserializeObject<OCrypto>(data);
        //    try
        //    {
        //        Callcar.Common.AESCryptography cry = new Callcar.Common.AESCryptography(oc.Key, _iv);
        //        return cry.Decrypt(oc.text);
        //    }
        //    catch (Exception ex)
        //    {
        //        return ex.StackTrace;
        //    }
        //}

        public class OCrypto
        {
            public string text { get; set; }
            public string Key { get; set; }
        }

        //[Description("測試Multipart/Form-data")]
        //[OperationContract, WebInvoke(UriTemplate = "TestPost", Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        //public string TestFormData(Stream input)
        //{
        //    HttpMultipartParser parser = new HttpMultipartParser(input, "form");
        //    if (parser.Success)
        //    {
        //        return parser.Parameters["data"] + " " + parser.Parameters["skip"] + " " + parser.Parameters["take"];
        //    }
        //    else
        //        return "Error";
        //}

        [Description("測試")]
        [OperationContract, WebInvoke(UriTemplate = "Test", Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public void Test(Stream input)
        {
          
        }

        #endregion
    }

    public class APIParms
    {
        public string param1 { get; set; }
        public string param2 { get; set; }
        public string param3 { get; set; }
        public string param4 { get; set; }
        public string param5 { get; set; }
        public string param6 { get; set; }
        public string param7 { get; set; }
        public string param8 { get; set; }
        public string param9 { get; set; }
        public string param10 { get; set; }
        public string param11 { get; set; }
        public string param12 { get; set; }
        public string param13 { get; set; }
        public string param14 { get; set; }
        public string param15 { get; set; }
        public string param16 { get; set; }
    }


    public class APIDispatchStruct
    {
        public APIDispatchStruct()
        {
            this.sl = new List<DispatchService>();
        }

        public string dno { get; set; }  //派遣單號
        public string td { get; set; }  //搭乘日期
        public string st { get; set; }  //出回國 (I/O)
        public List<DispatchService> sl { get; set; } //訂單
        public int pc { get; set; } //全車人數
        public int bc { get; set; }  //全車行李數
        public int sc { get; set; }  //全車訂單數
        public string re { get; set; } //執行狀況
        public string uf { get; set; } //更新註紀
        public string cartype { get; set; } //車趟種類 P專接 S共乘
    }

    public class DispatchService
    {
        public string rno { get; set; } //訂單編號
        public int so { get; set; }  //接送順序
        public int pc { get; set; } //訂單人數
        public int bc { get; set; }  //訂單行李數
        public string ma { get; set; }  //接送地址
        public string aa { get; set; }  //接送機場航廈
        public string fno { get; set; } //航班編號
        public double lat { get; set; }  //緯度
        public double lng { get; set; } //經度
        public DateTime st { get; set; }  //預約時間
        public DateTime at { get; set; } //實際接送時間
        public string sr { get; set; } //服務結果
        public int bag { get; set; } //增加行李數
        public string gn { get; set; }//乘客姓名
        public string mb { get; set; } //手機號碼
    }


    public class Bulletin
    {
        public int Serial { get; set; }
        public string Content { get; set; }
        public DateTime PublishTime { get; set; }
    }

    public class RtnStruct
    {
        public string Status { get; set; }
        public object RtnObject { get; set; }
    }
    public class DriverInfoStruct
    {
        public int id { get; set; }
        public string name { get; set; }
        public int fno { get; set; }
        public string fname { get; set; }
    }
}