﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.ComponentModel;
using System.Configuration;
using System.IO;
using HttpUtils;

namespace CallCarAPI
{

    [ServiceContract]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]

    public class CallcarAPI
    {
        private string ConnectionString = ConfigurationManager.ConnectionStrings["CRMS_v2"].ConnectionString;
        private const string RemoteFolder = @"C:\inetpub\wwwroot\CRMS_v2\Signature\";
        private const string SignatureFolder = @"C:\inetpub\wwwroot\MobileAPI\Signature\";
        private const string ConditionFolder = @"C:\inetpub\wwwroot\MobileAPI\Condtition\";

        [Description("驗證傳入之帳號密碼是否正確 id：司機帳號 pw：司機密碼")]
        [OperationContract, WebInvoke(UriTemplate = "AccChk?id={id}&pw={pw}", Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        public string VarifyAccount(string id, string pw)
        {
            ReturnValue r = new ReturnValue();
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionString))
                {
                    conn.Open();
                    StringBuilder sb = new StringBuilder();
                    sb.Append("SELECT EmpId,EmpName,tCarTeams_No ");
                    sb.Append("FROM Users where IdNo = @id and substring(IdNo,6,5) = @pw ");
                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandText = sb.ToString();
                    cmd.Connection = conn;
                    cmd.Parameters.AddWithValue("@id", id);
                    cmd.Parameters.AddWithValue("@pw", pw);

                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        if (dr.HasRows)
                        {
                            dr.Read();
                            DriverInfo i = new DriverInfo();
                            i.ID = Convert.ToString(dr["EmpId"]);
                            i.Name = Convert.ToString(dr["EmpName"]);
                            i.FleetNo = Convert.ToString(dr["tCarTeams_No"]);
                            r.Status = "Success";
                            r.RtnObject = i;
                        }
                        else
                        {
                            r.Status = "None";
                            r.RtnObject = "{}";
                        }
                    }
                    return JsonConvert.SerializeObject(r);
                }
            }
            catch (Exception ex)
            {
                r.Status = "Error";
                r.RtnObject = ex.Message;
                return JsonConvert.SerializeObject(r);
            }
        }


        [Description("取得公告資料 id：司機id no：車隊編號")]
        [OperationContract, WebInvoke(UriTemplate = "GetAnnounce?id={id}&no={no}", Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        public string GetBulletin(string id, string no)
        {
            ReturnValue r = new ReturnValue();
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionString))
                {
                    conn.Open();
                    StringBuilder sb = new StringBuilder();
                    sb.Append("SELECT ROW_NUMBER() OVER(ORDER BY CreateTime DESC) AS row_num ,  Content,User_EmpId,Createtime   ");
                    sb.Append("FROM ServiceBulletin ");
                    sb.Append("WHERE (CarTeam_No = @no OR User_EmpId = @id OR User_EmpId = -1)AND Show = 'True'  ");
                    sb.Append("ORDER BY CreateTime DESC ");
                    SqlCommand cmd = new SqlCommand(sb.ToString(), conn);
                    cmd.Parameters.AddWithValue("@id", id);
                    cmd.Parameters.AddWithValue("@no", no);
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    DataTable dt = ds.Tables[0];

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        string CreateTime = Convert.ToDateTime(dt.Rows[i]["Createtime"]).ToString("yyyy/MM/dd HH:mm");
                        string Content = Convert.ToString(dt.Rows[i]["Content"]);
                        int TargetID = Convert.ToInt32(dt.Rows[i]["User_EmpId"]);
                        string ToTarget = "";

                        if (TargetID == -1)
                            ToTarget = "[全體]";
                        else if (TargetID == -100)
                            ToTarget = "[車隊]";
                        else
                            ToTarget = "[個人]";

                        Content = CreateTime + " " + ToTarget + "<br />" + Content;

                        if (Content.Contains("https") || Content.Contains("http"))
                        {
                            int endIndex = 0;
                            int index = Content.IndexOf("https");

                            if (index == -1)
                                index = Content.IndexOf("http");

                            if (index == -1)
                                continue;
                            string Hyperlink;
                            string CompleteHyperlink;
                            endIndex = Content.IndexOf(" ", index + 1);
                            if (endIndex == -1)
                            {
                                endIndex = Content.Length - 1;

                                Hyperlink = Content.Substring(index, endIndex - index + 1);
                                CompleteHyperlink = "<a href='" + Hyperlink + "'>點此連結</a>";
                                dt.Rows[i]["Content"] = Content.Substring(0, index) + CompleteHyperlink;
                            }
                            else
                            {
                                Hyperlink = Content.Substring(index, endIndex - index);
                                CompleteHyperlink = "<a href='" + Hyperlink + "'>點此連結</a>";
                                dt.Rows[i]["Content"] = Content.Substring(0, index) + CompleteHyperlink + Content.Substring(endIndex, Content.Length - endIndex);
                            }

                        }
                        else
                            dt.Rows[i]["Content"] = Content;

                    }


                    if (dt.Rows.Count == 0)
                    {
                        r.Status = "None";
                        r.RtnObject = "{}";
                    }
                    else
                    {
                        r.Status = "Scuccess";
                        r.RtnObject = dt;
                    }
                }
                return JsonConvert.SerializeObject(r);
            }
            catch (Exception ex)
            {
                r.Status = "Error";
                r.RtnObject = ex.Message;
                return JsonConvert.SerializeObject(r);
            }
        }


        //我幫你整理缺的了  --  stopCount - 停靠數量 /  package_size -行李尺寸 / memberCardID - 卡號 /  memberCarId - 車號 / flightPickupWay - 指定接機方式 / help_content -  克服備註 /  other_address - 其他地址


        [Description("(舊)取得司機的將服務的簽單資料 id：司機的系統編號")]
        [OperationContract, WebInvoke(UriTemplate = "GetSignSheet?id={id}", Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        public string GetReservationByEmp(string id)
        {
            /*
             * 系統代收
                        SELECT initForCus,CompanyCostAmount,CompanyCostAmountForCus,CashCostAmount FROM CRM013

                        if (sqlDataReader["initForCus"].ToString() == "False")
                        {
                            result = sqlDataReader["CompanyCostAmountForCus"].ToString();
                        }
                        else
                        {
                            result = sqlDataReader["CashCostAmount"].ToString();
                        }
             登錄代收 
                if (sqlDataReader["initForCus"].ToString() == "False")
                {
                    result = sqlDataReader["CashAgencyCostForCus"].ToString();
                }
                else
                {
                    result = sqlDataReader["CashAgencyCost"].ToString();
                }
             */
            ReturnValue r = new ReturnValue();
            try
            {
                EmpReservData data = new EmpReservData();
                data.ReservList = new List<ReservationData>();
                data.EmpID = id;
                data.ReservationCnt = 0;
                data.QryTime = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");
                using (SqlConnection conn = new SqlConnection(ConnectionString))
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = conn;
                    conn.Open();
                    StringBuilder sb = new StringBuilder();
                    sb.Append(OutputQueryString());
                    sb.Append("and convert(char(8), r.ServiceTime1,112) >= convert(char(8), dateadd(day,-1,getdate()),112) ");
                    sb.Append("Order by servicedate,r.ServiceTime1_Hour ASC");
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.AddWithValue("@id", id);
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        if (dr.HasRows)
                        {
                            while (dr.Read())
                            {
                                ReservationData d = new ReservationData();
                                d.ServiceType = Convert.ToString(dr["ServiceType"]);
                                d.ServiceName = Convert.ToString(dr["ServiceName"]);
                                d.ServeDate = Convert.ToDateTime(dr["ServiceTime1"]).ToString("yyyy/MM/dd");
                                d.ServeTime = Convert.ToString(dr["ServiceTime1_Hour"]);

                                //未完成簽名：24小時後不再顯示
                                //已完成簽名：12小時後不再顯示
                                DateTime ServeTime, Now;
                                ServeTime = Convert.ToDateTime(d.ServeDate + " " + d.ServeTime);
                                Now = DateTime.Now;
                                TimeSpan t = Now - ServeTime;
                                if (dr["Signature_AutoID"] != null && dr["Signature_AutoID"] != DBNull.Value)
                                {
                                    if (Convert.ToString(dr["Signature_AutoID"]).Trim() != "")
                                    {
                                        if (Convert.ToInt32(dr["Signature_AutoID"]) > 0)
                                            d.SignFlag = "1";
                                        else
                                            d.SignFlag = "0";
                                    }
                                    else
                                        d.SignFlag = "0";
                                }
                                else
                                    d.SignFlag = "0";

                                if ((d.SignFlag == "1" && t.Hours >= 12) || (d.SignFlag == "0" && t.Hours >= 24))
                                    continue;

                                d.ReservNo = Convert.ToString(dr["Sno"]);
                                d.FlightNo = Convert.ToString(dr["FlightNo"]);
                                d.FightTime = Convert.ToString(dr["FlightTime"]);
                                d.CarType = Convert.ToString(dr["CarTypeName"]);

                                d.PassengerName = Convert.ToString(dr["MbrName"]);
                                d.PassengerMobile = Convert.ToString(dr["Tel1"]);

                                d.CustomerName = Convert.ToString(dr["Employer"]);
                                d.CustomerType = Convert.ToString(dr["CusType"]);

                                d.OrderName = Convert.ToString(dr["CallCarUser_Name"]);
                                d.OrderTel = Convert.ToString(dr["CallCarUser_CellPhone"]);

                                if (d.ServiceName == "商務" || d.ServiceName == "包車")
                                {
                                    d.Pickup = Convert.ToString(dr["A1"]) + Convert.ToString(dr["A2"]) + Convert.ToString(dr["AddressA"]);
                                    d.Getoff = Convert.ToString(dr["B1"]) + Convert.ToString(dr["B2"]) + Convert.ToString(dr["AddressB"]);
                                }
                                else if (d.ServiceName == "回國")
                                {
                                    d.Pickup = Convert.ToString(dr["Terminal"]);
                                    if (d.Pickup == "桃T2")
                                        d.Pickup = "桃園機場 第二航廈";
                                    else if (d.Pickup == "桃T1")
                                        d.Pickup = "桃園機場 第一航廈";
                                    else if (d.Pickup == "小港")
                                        d.Pickup = "小港機場";
                                    else if (d.Pickup == "松山")
                                        d.Pickup = "松山機場";

                                    d.Getoff = Convert.ToString(dr["A1"]) + Convert.ToString(dr["A2"]) + Convert.ToString(dr["AddressA"]);
                                }
                                else
                                {
                                    d.Pickup = Convert.ToString(dr["A1"]) + Convert.ToString(dr["A2"]) + Convert.ToString(dr["AddressA"]);
                                    d.Getoff = Convert.ToString(dr["Terminal"]);
                                    if (d.Getoff == "桃T2")
                                        d.Getoff = "桃園機場 第二航廈";
                                    else if (d.Getoff == "桃T1")
                                        d.Getoff = "桃園機場 第一航廈";
                                    else if (d.Getoff == "小港")
                                        d.Getoff = "小港機場";
                                    else if (d.Getoff == "松山")
                                        d.Getoff = "松山機場";
                                }


                                d.GetCash = Convert.ToString(dr["CashAgencyCost"]);
                                if (d.GetCash == null || d.GetCash == "")
                                    d.GetCash = "0";
                                d.RecordCash = Convert.ToString(dr["CashAgencyCostForCus"]);
                                if (d.RecordCash == null || d.RecordCash == "")
                                    d.RecordCash = "0";
                                d.PassengerCnt = Convert.ToString(dr["PassengerCnt"]);
                                d.BaggageCnt = Convert.ToString(dr["BaggageCnt"]);

                                d.StopAddress1 = Convert.ToString(dr["ContactAddressA"]);
                                d.ContactName1 = Convert.ToString(dr["ContactNameA"]);
                                d.ContactTel1 = Convert.ToString(dr["ContactTelA1"]);

                                d.StopAddress2 = Convert.ToString(dr["ContactAddressB"]);
                                d.ContactName2 = Convert.ToString(dr["ContactNameB"]);
                                d.ContactTel2 = Convert.ToString(dr["ContactTelB1"]);

                                d.StopAddress3 = Convert.ToString(dr["ContactAddressC"]);
                                d.ContactName3 = Convert.ToString(dr["ContactNameC"]);
                                d.ContactTel3 = Convert.ToString(dr["ContactTelC1"]);

                                d.StopAddress4 = Convert.ToString(dr["ContactAddressD"]);
                                d.ContactName4 = Convert.ToString(dr["ContactNameD"]);
                                d.ContactTel4 = Convert.ToString(dr["ContactTelD1"]);

                                d.BoardMemo = Convert.ToString(dr["BoardMemo"]);
                                d.SaftySeat = Convert.ToString(dr["BabyAmount"]);

                                //d.SignFlag = Convert.ToString(dr["SignFlag"]);
                                d.BagNote = Convert.ToString(dr["BaggageNote"]);
                                d.StopCnt = Convert.ToString(dr["StopCnt"]);
                                d.CardNo = Convert.ToString(dr["CreditCardNo"]);
                                if (d.CardNo != "-    -" && d.CardNo != "" && d.CardNo.Length == 14)
                                {
                                    d.CardNo = d.CardNo.Split('-')[2];
                                }
                                else
                                    d.CardNo = "";

                                d.CarNo = Convert.ToString(dr["CarNo"]);
                                d.Memo = Convert.ToString(dr["Memo2"]);
                                if (Convert.ToString(dr["SpecifiedTime"]) == "True")
                                    d.SpecifiedTime = "指定時間";
                                else
                                    d.SpecifiedTime = "";

                                d.DriverName = Convert.ToString(dr["EmpName"]);


                                MetaData md = GetSignMetaData(d.ReservNo, id, d.DriverName);
                                d.SignturePath = JsonConvert.SerializeObject(md);

                                data.ReservList.Add(d);
                                data.ReservationCnt += 1;
                            }
                            r.Status = "Success";
                            r.RtnObject = data;
                        }
                        else
                        {
                            r.Status = "None";
                            r.RtnObject = "{}";
                        }
                    }
                }

                return JsonConvert.SerializeObject(r);
            }
            catch (Exception ex)
            {
                r.Status = "Error";
                r.RtnObject = ex.Message;
                return JsonConvert.SerializeObject(r);
            }
        }

        [Description("司機查詢歷史簽單的資料(三個月內) id:司機的系統編號 stDate：查詢開始日期(yyyy/MM/dd) endDate：查詢結束日期(yyyy/MM/dd)")]
        [OperationContract, WebInvoke(UriTemplate = "QryEmpHistory?id={id}&date1={stDate}&date2={endDate}", Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        public string GetReservationByEmpDate(string id, string stDate, string endDate)
        {
            ReturnValue r = new ReturnValue();
            try
            {
                EmpReservData data = new EmpReservData();
                data.ReservList = new List<ReservationData>();
                data.EmpID = id;
                data.QryTime = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");
                DateTime sdate = Convert.ToDateTime(stDate);
                DateTime edate = Convert.ToDateTime(endDate);

                using (SqlConnection conn = new SqlConnection(ConnectionString))
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = conn;
                    conn.Open();
                    StringBuilder sb = new StringBuilder();
                    sb.Append(OutputQueryString());
                    sb.Append(" and convert(char(8), r.ServiceTime1,112) >= convert(char(8), dateadd(month, -3,getdate()),112)  and  convert(char(8), r.ServiceTime1,112) between @sdate and @edate ");
                    sb.Append("Order by servicedate,r.ServiceTime1_Hour ASC");
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.AddWithValue("@id", id);
                    cmd.Parameters.AddWithValue("@sdate", sdate);
                    cmd.Parameters.AddWithValue("@edate", edate);
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        if (dr.HasRows)
                        {
                            while (dr.Read())
                            {
                                ReservationData d = new ReservationData();
                                d.ServiceType = Convert.ToString(dr["ServiceType"]);
                                d.ServiceName = Convert.ToString(dr["ServiceName"]);
                                d.ServeDate = Convert.ToDateTime(dr["ServiceTime1"]).ToString("yyyy/MM/dd");
                                d.ServeTime = Convert.ToString(dr["ServiceTime1_Hour"]);
                                d.ReservNo = Convert.ToString(dr["Sno"]);
                                d.FlightNo = Convert.ToString(dr["FlightNo"]);
                                d.FightTime = Convert.ToString(dr["FlightTime"]);
                                d.CarType = Convert.ToString(dr["CarTypeName"]);

                                d.PassengerName = Convert.ToString(dr["MbrName"]);
                                d.PassengerMobile = Convert.ToString(dr["Tel1"]);

                                d.CustomerName = Convert.ToString(dr["Employer"]);
                                d.CustomerType = Convert.ToString(dr["CusType"]);

                                d.OrderName = Convert.ToString(dr["CallCarUser_Name"]);
                                d.OrderTel = Convert.ToString(dr["CallCarUser_CellPhone"]);

                                if (d.ServiceName == "商務" || d.ServiceName == "包車")
                                {
                                    d.Pickup = Convert.ToString(dr["A1"]) + Convert.ToString(dr["A2"]) + Convert.ToString(dr["AddressA"]);
                                    d.Getoff = Convert.ToString(dr["B1"]) + Convert.ToString(dr["B2"]) + Convert.ToString(dr["AddressB"]);
                                }
                                else if (d.ServiceName == "回國")
                                {
                                    d.Pickup = Convert.ToString(dr["Terminal"]);
                                    d.Getoff = Convert.ToString(dr["A1"]) + Convert.ToString(dr["A2"]) + Convert.ToString(dr["AddressA"]);
                                }
                                else
                                {
                                    d.Pickup = Convert.ToString(dr["A1"]) + Convert.ToString(dr["A2"]) + Convert.ToString(dr["AddressA"]);
                                    d.Getoff = Convert.ToString(dr["Terminal"]);
                                }
                                d.GetCash = Convert.ToString(dr["CashAgencyCost"]);
                                if (d.GetCash == null || d.GetCash == "")
                                    d.GetCash = "0";
                                d.RecordCash = Convert.ToString(dr["CashAgencyCostForCus"]);
                                if (d.RecordCash == null || d.RecordCash == "")
                                    d.RecordCash = "0";
                                d.PassengerCnt = Convert.ToString(dr["PassengerCnt"]);
                                d.BaggageCnt = Convert.ToString(dr["BaggageCnt"]);
                                d.StopAddress1 = Convert.ToString(dr["ContactAddressA"]);
                                d.ContactName1 = Convert.ToString(dr["ContactNameA"]);
                                d.ContactTel1 = Convert.ToString(dr["ContactTelA1"]);

                                d.StopAddress2 = Convert.ToString(dr["ContactAddressB"]);
                                d.ContactName2 = Convert.ToString(dr["ContactNameB"]);
                                d.ContactTel2 = Convert.ToString(dr["ContactTelB1"]);

                                d.StopAddress3 = Convert.ToString(dr["ContactAddressC"]);
                                d.ContactName3 = Convert.ToString(dr["ContactNameC"]);
                                d.ContactTel3 = Convert.ToString(dr["ContactTelC1"]);

                                d.StopAddress4 = Convert.ToString(dr["ContactAddressD"]);
                                d.ContactName4 = Convert.ToString(dr["ContactNameD"]);
                                d.ContactTel4 = Convert.ToString(dr["ContactTelD1"]);

                                d.BoardMemo = Convert.ToString(dr["BoardMemo"]);
                                d.SaftySeat = Convert.ToString(dr["BabyAmount"]);

                                if (dr["Signature_AutoID"] != null && dr["Signature_AutoID"] != DBNull.Value)
                                {
                                    if (Convert.ToInt32(dr["Signature_AutoID"]) > 0)
                                        d.SignFlag = "1";
                                    else
                                        d.SignFlag = "0";
                                }
                                else
                                    d.SignFlag = "0";
                                d.BagNote = Convert.ToString(dr["BaggageNote"]);
                                d.StopCnt = Convert.ToString(dr["StopCnt"]);
                                d.CardNo = Convert.ToString(dr["CreditCardNo"]);
                                if (d.CardNo != "-    -" && d.CardNo != "" && d.CardNo.Length == 14)
                                {
                                    d.CardNo = d.CardNo.Split('-')[2];
                                }
                                else
                                    d.CardNo = "";
                                d.CarNo = Convert.ToString(dr["CarNo"]);
                                d.Memo = Convert.ToString(dr["Memo2"]);
                                if (Convert.ToString(dr["SpecifiedTime"]) == "True")
                                    d.SpecifiedTime = "指定時間";
                                else
                                    d.SpecifiedTime = "";

                                data.ReservList.Add(d);
                                data.ReservationCnt += 1;
                            }
                            r.Status = "Success";
                            r.RtnObject = data;
                        }
                        else
                        {
                            r.Status = "None";
                            r.RtnObject = "{}";
                        }
                    }
                }

                return JsonConvert.SerializeObject(r);
            }
            catch (Exception ex)
            {
                r.Status = "Error";
                r.RtnObject = ex.Message;
                return JsonConvert.SerializeObject(r);
            }
        }




        //[Description("**舊**更新簽單並取得上傳資料之Meta Data no:簽單編號 id：司機的系統編號 name：司機姓名")]
        //[OperationContract, WebInvoke(UriTemplate = "UpdateSheet?no={no}&id={id}&name={name}", Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        //public string UpdateSheet(string no, string id, string name)
        //{
        //    ReturnValue r = new ReturnValue();
        //    try
        //    {
        //        MetaData md = GetSignMetaData(no, id, name);
        //        if (md != null)
        //        {
        //            r.Status = "Success";
        //            r.RtnObject = md;
        //            return JsonConvert.SerializeObject(r);
        //        }
        //        else
        //        {
        //            r.Status = "None";
        //            r.RtnObject = "{}";
        //            return JsonConvert.SerializeObject(r);
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        r.Status = "Error";
        //        r.RtnObject = ex.Message;
        //        return JsonConvert.SerializeObject(r);
        //    }
        //}

        [Description("(舊)取得上傳的MetaData no:簽單編號 id：司機的系統編號 name：司機姓名 ")]
        [OperationContract, WebInvoke(UriTemplate = "GetFolder?no={no}&id={id}&name={name}", Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        public string GetUploadFolder(string no, string id, string name)
        {
            ReturnValue r = new ReturnValue();
            try
            {
                MetaData md = GetSignMetaData(no, id, name);
                if (md != null)
                {
                    r.Status = "Success";
                    r.RtnObject = md;
                    return JsonConvert.SerializeObject(r);
                }
                else
                {
                    r.Status = "None";
                    r.RtnObject = "{}";
                    return JsonConvert.SerializeObject(r);
                }

            }
            catch (Exception ex)
            {
                r.Status = "Error";
                r.RtnObject = ex.Message;
                return JsonConvert.SerializeObject(r);
            }
        }

        private MetaData GetSignMetaData(string no, string id, string name)
        {
            try
            {
                string Uri = string.Empty;
                int SignatureID = 0;
                DateTime Now = DateTime.Now;
                string NowTime = Now.ToString("yyyyMMddHHmmss");
                string ImageName = no + "_" + NowTime.Substring(0, 8) + "_" + NowTime.Substring(8, 6) + ".jpeg";
                string TargetPath = SignatureFolder + NowTime.Substring(0, 4) + @"\" + NowTime.Substring(4, 2) + @"\" + NowTime.Substring(6, 2);
                string FtpPath = NowTime.Substring(0, 4) + @"/" + NowTime.Substring(4, 2) + @"/" + NowTime.Substring(6, 2);
                DirectoryInfo di = new DirectoryInfo(TargetPath);
                if (!di.Exists)
                    di.Create();

                Uri = @"https://www.callcar.com.tw/MobileAPI/Signature/" + NowTime.Substring(0, 4) + @"/" + NowTime.Substring(4, 2) + @"/" + NowTime.Substring(6, 2) + "/" + ImageName;
                using (SqlConnection conn = new SqlConnection(ConnectionString))
                {
                    SqlCommand cmd = new SqlCommand();
                    StringBuilder sb = new StringBuilder();
                    sb.Append("INSERT INTO Signature(Sno,SignaturePath,SignatureFileName,SignatureTime,Type,del,CreateTime,AcceptEmpId,AcceptEmpName,UpdTime,UpdEmpId,UpdEmpName)");
                    sb.Append("VALUES (@sno,@signaturepath,@signaturefilename,@signaturetime,@type,@del,@createtime,@acceptempid,@acceptempname,@updtime,@updempid,@updempname)");
                    cmd.CommandText = sb.ToString();
                    cmd.Connection = conn;
                    cmd.Parameters.AddWithValue("@sno", no);
                    cmd.Parameters.AddWithValue("@signaturepath", Uri);
                    cmd.Parameters.AddWithValue("@signaturefilename", ImageName);
                    cmd.Parameters.AddWithValue("@signaturetime", Now);
                    cmd.Parameters.AddWithValue("@type", "2");
                    cmd.Parameters.AddWithValue("@del", "0");
                    cmd.Parameters.AddWithValue("@createtime", Now);
                    cmd.Parameters.AddWithValue("@acceptempid", id);
                    cmd.Parameters.AddWithValue("@acceptempname", name);
                    cmd.Parameters.AddWithValue("@updtime", Now);
                    cmd.Parameters.AddWithValue("@updempid", id);
                    cmd.Parameters.AddWithValue("@updempname", name);

                    conn.Open();
                    if (cmd.ExecuteNonQuery() < 1)
                        return null;

                    cmd.Parameters.Clear();
                    cmd.CommandText = "Select @@Identity";
                    object obj = cmd.ExecuteScalar();
                    SignatureID = Convert.ToInt32(obj);
                }

                MetaData md = new MetaData();
                md.Sno = no;
                md.Uri = Uri;
                md.FtpPath = FtpPath;
                md.ImageName = new List<string>();
                md.ImageName.Add(ImageName);
                md.SignID = SignatureID;


                return md;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        [Description("(舊)上傳簽名檔完成更新簽單  no:簽單編號 id：MetaData的SigniD ")]
        [OperationContract, WebInvoke(UriTemplate = "UpdSheetSignID?no={SheetNo}&id={SignID}", Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        public string UpdateCRM(string SheetNo, string SignID)
        {
            ReturnValue r = new ReturnValue();
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionString))
                {
                    SqlCommand cmd = new SqlCommand();
                    StringBuilder sb = new StringBuilder();
                    sb.Append("Update CRM013 Set Signature_AutoID = @id,CloseFlag = '9'  Where Sno = @no");
                    cmd.CommandText = sb.ToString();
                    cmd.Connection = conn;
                    cmd.Parameters.AddWithValue("@no", SheetNo);
                    cmd.Parameters.AddWithValue("@id", Int32.Parse(SignID));

                    conn.Open();
                    if (cmd.ExecuteNonQuery() <= 0)
                    {
                        throw new Exception("請確認簽單號碼 - 無更新的簽單");
                    }
                    else
                    {
                        r.Status = "Success";
                        r.RtnObject = null;
                    }
                    return JsonConvert.SerializeObject(r);
                }
            }
            catch (Exception ex)
            {
                r.Status = "Error";
                r.RtnObject = ex.Message;
                return JsonConvert.SerializeObject(r);
            }
        }


        [Description("更新司機代收費用 no:簽單編號 id：司機的系統編號 name：司機姓名 amt：代收金額 type：車趟類別 (1出國2回國3商務4包車)")]
        [OperationContract, WebInvoke(UriTemplate = "UpdMoney1?no={no}&id={id}&name={name}&amt={amt}&type={type}", Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        public string UpdDriverTakeMoney(string no, string id, string name, string amt, string type)
        {
            ReturnValue r = new ReturnValue();
            try
            {

                int amount;
                StringBuilder sb = new StringBuilder();
                Int32.TryParse(amt, out amount);

                if (amount == 0)
                {
                    r.Status = "Error";
                    r.RtnObject = "amt cannot be zero or any un-numeric value";
                    return JsonConvert.SerializeObject(r);
                }
                using (SqlConnection conn = new SqlConnection(ConnectionString))
                {

                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Connection = conn;
                        conn.Open();
                        sb.Append("Update CRM013 ");
                        sb.Append("Set CashAgencyCostForCus = @amt ");
                        sb.Append("Where Sno = @no ");

                        cmd.CommandText = sb.ToString();
                        cmd.Parameters.AddWithValue("@amt", Int32.Parse(amt));
                        cmd.Parameters.AddWithValue("@no", no);
                        int rtn = cmd.ExecuteNonQuery();

                        if (rtn == 1)
                        {
                            r.Status = "Success";
                            r.RtnObject = "Update Success";
                        }
                        else if (rtn == 0)
                        {
                            r.Status = "Error";
                            r.RtnObject = "No data can be updated";
                        }
                        else
                        {
                            r.Status = "Error";
                            r.RtnObject = "Unexcepted Error";
                        }
                    }
                    return JsonConvert.SerializeObject(r);
                }
            }
            catch (Exception ex)
            {
                r.Status = "Error";
                r.RtnObject = ex.Message;
                return JsonConvert.SerializeObject(r);
            }
        }

        [Description("取得該單之異況資料 no:簽單編號")]
        [OperationContract, WebInvoke(UriTemplate = "GetCondition?no={no}", Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        public string GetReportCondition(string no)
        {
            ReportCon rc = new ReportCon();
            ReturnValue r = new ReturnValue();
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = conn;
                    StringBuilder sb = new StringBuilder();
                    sb.Append("SELECT Sno,ReportContent,Status FROM DCR WHERE Sno = @no");
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.AddWithValue("@no", no);

                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        if (dr.HasRows)
                        {
                            dr.Read();
                            rc.ReservNo = no;
                            rc.Content = "";// Convert.ToString(dr["ReportContent"]);
                            rc.Status = Convert.ToString(dr["Status"]);
                            r.Status = "Success";
                            r.RtnObject = rc;
                        }
                        else
                        {
                            r.Status = "None";
                            r.RtnObject = "{}";
                        }
                    }
                }
                return JsonConvert.SerializeObject(r);
            }
            catch (Exception ex)
            {
                r.Status = "Error";
                r.RtnObject = ex.Message;
                return JsonConvert.SerializeObject(r);
            }
        }

        [Description("(舊)回傳異況並取得上傳之Meta Data no:簽單編號 id：司機的系統編號 name：司機姓名 cnt：上傳照片數量 Con：異況內容")]
        [OperationContract, WebInvoke(UriTemplate = "ReportCondition?no={no}&id={id}&name={name}&cnt={cnt}&Con={Con}", Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        public string ReportCondition(string no, string id, string name, string cnt, string Con)
        {
            ReturnValue r = new ReturnValue();
            try
            {
                string Uri = string.Empty;
                DateTime Now = DateTime.Now;
                string NowTime = Now.ToString("yyyyMMddHHmmss");
                //string TargetPath = ConditionFolder;
                int imgcnt;
                MetaData md = new MetaData();

                if (!Int32.TryParse(cnt, out imgcnt))
                {
                    r.Status = "Error";
                    r.RtnObject = "cnt must be integer";
                }

                if (imgcnt > 0)
                {
                    //先刪除網站上的圖檔                   
                    //DirectoryInfo di = new DirectoryInfo(TargetPath);

                    //if (!di.Exists)
                    //    di.Create();
                    //else
                    //{
                    //    FileInfo[] fi = di.GetFiles();
                    //    foreach (FileInfo f in fi)
                    //        f.Delete();
                    //}




                    md.FtpPath = "";
                    md.ImageName = new List<string>();
                    for (int i = 1; i <= imgcnt; i++)
                        md.ImageName.Add(no + "_" + i.ToString() + ".jpeg");
                    md.Sno = no;
                    md.Uri = Uri = @"https://www.callcar.com.tw/MobileAPI/Condition/";

                }
                else if (imgcnt < 0)
                {
                    r.Status = "Error";
                    r.RtnObject = "cnt must be positive integer";
                }

                //更新資料
                Con = System.Text.Encoding.GetEncoding("utf-8").GetString(Convert.FromBase64String(Con));

                using (SqlConnection conn = new SqlConnection(ConnectionString))
                {
                    conn.Open();
                    StringBuilder sb = new StringBuilder();
                    sb.Append("Select reportcontent from DCR Where Sno = @no");
                    object rtn;

                    using (SqlCommand cmd = new SqlCommand(sb.ToString(), conn))
                    {
                        cmd.Parameters.AddWithValue("@no", no);
                        rtn = cmd.ExecuteScalar();
                    }

                    sb.Length = 0;
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        if (rtn == null || rtn == DBNull.Value)
                        {
                            //新增
                            sb.Append("INSERT INTO DCR(Sno,ReportSource,ReportType,ReportContent,ReportCashAgencyCost,Status, ");
                            sb.Append("AcceptEmpId,AcceptEmpName,CreateTime,UpdEmpId,UpdEmpName,UpdateTime,image_uri,image_name) ");
                            sb.Append("VALUES (@sno,@reportsource,@reporttype,@reportcontent,@reportcashagencycost,@status, ");
                            sb.Append("@acceptempid,@acceptempname,@createtime,@updempid,@updempname,@updatetime,@image_uri,@image_name) ");

                            cmd.Parameters.AddWithValue("@sno", no);
                            cmd.Parameters.AddWithValue("@reportsource", "數位");
                            cmd.Parameters.AddWithValue("@reporttype", "異況");
                            cmd.Parameters.AddWithValue("@reportcontent", Con);
                            cmd.Parameters.AddWithValue("@reportcashagencycost", 0);
                            cmd.Parameters.AddWithValue("@status", "未結案");
                            cmd.Parameters.AddWithValue("@acceptempid", id);
                            cmd.Parameters.AddWithValue("@acceptempname", name);
                            cmd.Parameters.AddWithValue("@createtime", Now);
                            cmd.Parameters.AddWithValue("@updempid", id);
                            cmd.Parameters.AddWithValue("@updempname", name);
                            cmd.Parameters.AddWithValue("@updatetime", Now);
                            if (imgcnt == 0)
                            {
                                cmd.Parameters.AddWithValue("@image_uri", "");
                                cmd.Parameters.AddWithValue("@image_name", "");
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@image_uri", md.Uri);
                                cmd.Parameters.AddWithValue("@image_name", JsonConvert.SerializeObject(md.ImageName));
                            }

                        }
                        else
                        {
                            Con = Convert.ToString(rtn) + Environment.NewLine + Con;
                            sb.Length = 0;
                            cmd.Parameters.Clear();
                            sb.Append("UPDATE DCR ");
                            sb.Append("SET ReportContent = @reportcontent,UpdEmpId = @updempid,UpdEmpName = @updempname,UpdateTime = @updatetime, ");
                            sb.Append("image_uri = @image_uri,image_name = @image_name, Status=@status WHERE  Sno = @sno");

                            cmd.Parameters.AddWithValue("@sno", no);
                            cmd.Parameters.AddWithValue("@reportcontent", Con);
                            cmd.Parameters.AddWithValue("@updempid", id);
                            cmd.Parameters.AddWithValue("@updempname", name);
                            cmd.Parameters.AddWithValue("@updatetime", Now);
                            cmd.Parameters.AddWithValue("@status", "未結案");


                            if (imgcnt > 0)
                            {
                                cmd.Parameters.AddWithValue("@image_uri", md.Uri);
                                cmd.Parameters.AddWithValue("@image_name", JsonConvert.SerializeObject(md.ImageName));
                            }
                        }
                        cmd.Connection = conn;
                        cmd.CommandText = sb.ToString();
                        cmd.ExecuteNonQuery();
                    }
                    r.RtnObject = md;
                    r.Status = "Success";
                    return JsonConvert.SerializeObject(r);
                }

            }
            catch (Exception ex)
            {
                r.Status = "Error";
                r.RtnObject = ex.Message;
                return JsonConvert.SerializeObject(r);
            }
        }

        [Description("回傳乘客評價 no:簽單編號 grade：分數")]
        [OperationContract, WebInvoke(UriTemplate = "UpdGrade?no={no}&grade={grade}", Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        public string UpdEvaluation(string no, string grade)
        {
            ReturnValue r = new ReturnValue();
            try
            {
                int rtn;
                using (SqlConnection conn = new SqlConnection(ConnectionString))
                {
                    conn.Open();
                    StringBuilder sb = new StringBuilder();
                    sb.Append("update CRM013 Set Favorability = @grade Where Sno = @no");
                    SqlCommand cmd = new SqlCommand(sb.ToString(), conn);
                    cmd.Parameters.AddWithValue("@grade", Int32.Parse(grade));
                    cmd.Parameters.AddWithValue("@no", no);
                    rtn = cmd.ExecuteNonQuery();
                }
                if (rtn > 0)
                {
                    r.Status = "Success";
                    r.RtnObject = null;
                    return JsonConvert.SerializeObject(r);
                }
                else
                {
                    r.Status = "None";
                    r.RtnObject = "無更新之資料";
                    return JsonConvert.SerializeObject(r);
                }

            }
            catch (Exception ex)
            {
                r.Status = "Error";
                r.RtnObject = ex.Message;
                return JsonConvert.SerializeObject(r);
            }
        }

        private string OutputQueryString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("SELECT r.Sno,r.ServiceType,r.ServiceName,r.CallCarUser_Name,r.CallCarUser_CellPhone, ");
            sb.Append("p.MbrName,p.Tel1, p.Type as CusType, ");
            sb.Append("r.ServiceTime1,r.ServiceTime1_Hour, ");
            sb.Append("(SELECT AreaName2 FROM CD003 WHERE AreaId = r.AreaNameA1) AS A1, ");
            sb.Append("(SELECT AreaName2 FROM CD003 WHERE AreaId = r.AreaNameA2) AS A2, ");
            sb.Append("r.AddressA, ");
            sb.Append("(SELECT AreaName2 FROM CD003 WHERE AreaId = r.AreaNameB1) AS B1, ");
            sb.Append("(SELECT AreaName2 FROM CD003 WHERE AreaId = r.AreaNameB2) AS B2, ");
            sb.Append("r.AddressB, ");
            sb.Append("r.ContactNameA,r.ContactTelA1,r.ContactAddressA,  ");
            sb.Append("r.ContactNameB,r.ContactTelB1,r.ContactAddressB,  ");
            sb.Append("r.ContactNameC,r.ContactTelC1,r.ContactAddressC,  ");
            sb.Append("r.ContactNameD,r.ContactTelD1,r.ContactAddressD,  ");
            sb.Append("r.FlightNo,r.FlightTime,r.Terminal,r.CarTypeName,r.CompName,r.Employer,r.BabyAmount,r.BaggageCnt,r.PassengerCnt,r.BoardMemo,r.CashCostAmount, ");
            sb.Append("r.BoardMemo,r.BoardPath,r.BoardFileName, ");
            sb.Append("r.Signature_AutoID, ");
            sb.Append("r.CashAgencyCostForCus,r.CashAgencyCost, ");
            sb.Append("r.StopCnt,r.BaggageNote,r.CreditCardNo,r.CarNo,r.Memo2,r.SpecifiedTime,u.EmpName ,convert(CHAR(8),r.ServiceTime1,112) AS servicedate ");
            sb.Append("FROM CRM013 r, CRM011 p,Users u ");
            sb.Append("WHERE r.MbrId = p.MbrId and r.DriverEmpId = u.EmpId and r.DriverEmpId = @id  ");
            return sb.ToString();
        }

        #region 新版簽單上傳
        [Description("(新) 取得司機的將服務的簽單資料(取消使用上傳路徑)  id：司機的系統編號")]
        [OperationContract, WebInvoke(UriTemplate = "GetSignSheetV2?id={id}", Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        public string GetReservationByEmpV2(string id)
        {

            ReturnValue r = new ReturnValue();
            try
            {
                EmpReservData data = new EmpReservData();
                data.ReservList = new List<ReservationData>();
                data.EmpID = id;
                data.ReservationCnt = 0;
                data.QryTime = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");
                using (SqlConnection conn = new SqlConnection(ConnectionString))
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = conn;
                    conn.Open();
                    StringBuilder sb = new StringBuilder();
                    sb.Append(OutputQueryString());
                    sb.Append("and convert(char(8), r.ServiceTime1,112) >= convert(char(8), dateadd(day,-1,getdate()),112) ");
                    sb.Append("Order by servicedate,r.ServiceTime1_Hour ASC");
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.AddWithValue("@id", id);
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        if (dr.HasRows)
                        {
                            while (dr.Read())
                            {
                                ReservationData d = new ReservationData();
                                d.ServiceType = Convert.ToString(dr["ServiceType"]);
                                d.ServiceName = Convert.ToString(dr["ServiceName"]);
                                d.ServeDate = Convert.ToDateTime(dr["ServiceTime1"]).ToString("yyyy/MM/dd");
                                d.ServeTime = Convert.ToString(dr["ServiceTime1_Hour"]);

                                //未完成簽名：24小時後不再顯示
                                //已完成簽名：12小時後不再顯示
                                DateTime ServeTime, Now;
                                ServeTime = Convert.ToDateTime(d.ServeDate + " " + d.ServeTime);
                                Now = DateTime.Now;
                                TimeSpan t = Now - ServeTime;
                                if (dr["Signature_AutoID"] != null && dr["Signature_AutoID"] != DBNull.Value)
                                {
                                    if (Convert.ToString(dr["Signature_AutoID"]).Trim() != "")
                                    {
                                        if (Convert.ToInt32(dr["Signature_AutoID"]) > 0)
                                            d.SignFlag = "1";
                                        else
                                            d.SignFlag = "0";
                                    }
                                    else
                                        d.SignFlag = "0";
                                }
                                else
                                    d.SignFlag = "0";

                                if ((d.SignFlag == "1" && t.Hours >= 12) || (d.SignFlag == "0" && t.Hours >= 24))
                                    continue;

                                d.ReservNo = Convert.ToString(dr["Sno"]);
                                d.FlightNo = Convert.ToString(dr["FlightNo"]);
                                d.FightTime = Convert.ToString(dr["FlightTime"]);
                                d.CarType = Convert.ToString(dr["CarTypeName"]);

                                d.PassengerName = Convert.ToString(dr["MbrName"]);
                                d.PassengerMobile = Convert.ToString(dr["Tel1"]);

                                d.CustomerName = Convert.ToString(dr["Employer"]);
                                d.CustomerType = Convert.ToString(dr["CusType"]);

                                d.OrderName = Convert.ToString(dr["CallCarUser_Name"]);
                                d.OrderTel = Convert.ToString(dr["CallCarUser_CellPhone"]);

                                if (d.ServiceName == "商務" || d.ServiceName == "包車")
                                {
                                    d.Pickup = Convert.ToString(dr["A1"]) + Convert.ToString(dr["A2"]) + Convert.ToString(dr["AddressA"]);
                                    d.Getoff = Convert.ToString(dr["B1"]) + Convert.ToString(dr["B2"]) + Convert.ToString(dr["AddressB"]);
                                }
                                else if (d.ServiceName == "回國")
                                {
                                    d.Pickup = Convert.ToString(dr["Terminal"]);
                                    if (d.Pickup == "桃T2")
                                        d.Pickup = "桃園機場 第二航廈";
                                    else if (d.Pickup == "桃T1")
                                        d.Pickup = "桃園機場 第一航廈";
                                    else if (d.Pickup == "小港")
                                        d.Pickup = "小港機場";
                                    else if (d.Pickup == "松山")
                                        d.Pickup = "松山機場";

                                    d.Getoff = Convert.ToString(dr["A1"]) + Convert.ToString(dr["A2"]) + Convert.ToString(dr["AddressA"]);
                                }
                                else
                                {
                                    d.Pickup = Convert.ToString(dr["A1"]) + Convert.ToString(dr["A2"]) + Convert.ToString(dr["AddressA"]);
                                    d.Getoff = Convert.ToString(dr["Terminal"]);
                                    if (d.Getoff == "桃T2")
                                        d.Getoff = "桃園機場 第二航廈";
                                    else if (d.Getoff == "桃T1")
                                        d.Getoff = "桃園機場 第一航廈";
                                    else if (d.Getoff == "小港")
                                        d.Getoff = "小港機場";
                                    else if (d.Getoff == "松山")
                                        d.Getoff = "松山機場";
                                }


                                d.GetCash = Convert.ToString(dr["CashAgencyCost"]);
                                if (d.GetCash == null || d.GetCash == "")
                                    d.GetCash = "0";
                                d.RecordCash = Convert.ToString(dr["CashAgencyCostForCus"]);
                                if (d.RecordCash == null || d.RecordCash == "")
                                    d.RecordCash = "0";
                                d.PassengerCnt = Convert.ToString(dr["PassengerCnt"]);
                                d.BaggageCnt = Convert.ToString(dr["BaggageCnt"]);

                                d.StopAddress1 = Convert.ToString(dr["ContactAddressA"]);
                                d.ContactName1 = Convert.ToString(dr["ContactNameA"]);
                                d.ContactTel1 = Convert.ToString(dr["ContactTelA1"]);

                                d.StopAddress2 = Convert.ToString(dr["ContactAddressB"]);
                                d.ContactName2 = Convert.ToString(dr["ContactNameB"]);
                                d.ContactTel2 = Convert.ToString(dr["ContactTelB1"]);

                                d.StopAddress3 = Convert.ToString(dr["ContactAddressC"]);
                                d.ContactName3 = Convert.ToString(dr["ContactNameC"]);
                                d.ContactTel3 = Convert.ToString(dr["ContactTelC1"]);

                                d.StopAddress4 = Convert.ToString(dr["ContactAddressD"]);
                                d.ContactName4 = Convert.ToString(dr["ContactNameD"]);
                                d.ContactTel4 = Convert.ToString(dr["ContactTelD1"]);

                                d.BoardMemo = Convert.ToString(dr["BoardMemo"]);
                                d.SaftySeat = Convert.ToString(dr["BabyAmount"]);

                                //d.SignFlag = Convert.ToString(dr["SignFlag"]);
                                d.BagNote = Convert.ToString(dr["BaggageNote"]);
                                d.StopCnt = Convert.ToString(dr["StopCnt"]);
                                d.CardNo = Convert.ToString(dr["CreditCardNo"]);
                                if (d.CardNo != "-    -" && d.CardNo != "" && d.CardNo.Length == 14)
                                {
                                    d.CardNo = d.CardNo.Split('-')[2];
                                }
                                else
                                    d.CardNo = "";

                                d.CarNo = Convert.ToString(dr["CarNo"]);
                                d.Memo = Convert.ToString(dr["Memo2"]);
                                if (Convert.ToString(dr["SpecifiedTime"]) == "True")
                                    d.SpecifiedTime = "指定時間";
                                else
                                    d.SpecifiedTime = "";

                                d.DriverName = Convert.ToString(dr["EmpName"]);


                                //MetaData md = GetSignMetaData(d.ReservNo, id, d.DriverName);
                                //d.SignturePath = JsonConvert.SerializeObject(md);

                                data.ReservList.Add(d);
                                data.ReservationCnt += 1;
                            }
                            r.Status = "Success";
                            r.RtnObject = data;
                        }
                        else
                        {
                            r.Status = "None";
                            r.RtnObject = "{}";
                        }
                    }
                }

                return JsonConvert.SerializeObject(r);
            }
            catch (Exception ex)
            {
                r.Status = "Error";
                r.RtnObject = ex.Message;
                return JsonConvert.SerializeObject(r);
            }
        }

        [Description("(新)上傳簽名檔完成更新簽單(Ftp上傳完成呼叫 固定將圖檔上傳至user1的根目錄)  no:簽單編號 id：司機的iD ")]
        [OperationContract, WebInvoke(UriTemplate = "UpdSheetSignIDV2?no={SheetNo}&id={DriverID}", Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        public string UpdateCRMV2(string SheetNo, string DriverID)
        {
            ReturnValue r = new ReturnValue();
            try
            {
                int DID = Int32.Parse(DriverID);
                DriverInfo Driver = GetDriver(DID);
                if (Driver == null)
                {
                    r.Status = "Error";
                    r.RtnObject = "司機編號異常";
                    return JsonConvert.SerializeObject(r);
                }
                string Uri = string.Empty;
                Uri = @"https://www.callcar.com.tw/MobileAPI/Signature/" + SheetNo + ".jpeg";
                using (SqlConnection conn = new SqlConnection(ConnectionString))
                {
                    SqlCommand cmd = new SqlCommand();
                    StringBuilder sb = new StringBuilder();
                    cmd.Connection = conn;


                    sb.Append("INSERT INTO Signature(Sno,SignaturePath,SignatureFileName,SignatureTime,Type,del,CreateTime,AcceptEmpId,AcceptEmpName,UpdTime,UpdEmpId,UpdEmpName)");
                    sb.Append("VALUES (@sno,@signaturepath,@signaturefilename,@signaturetime,@type,@del,@createtime,@acceptempid,@acceptempname,@updtime,@updempid,@updempname)");
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.AddWithValue("@sno", SheetNo);
                    cmd.Parameters.AddWithValue("@signaturepath", Uri);
                    cmd.Parameters.AddWithValue("@signaturefilename", SheetNo + ".jpeg");
                    cmd.Parameters.AddWithValue("@signaturetime", DateTime.Now);
                    cmd.Parameters.AddWithValue("@type", "2");
                    cmd.Parameters.AddWithValue("@del", "0");
                    cmd.Parameters.AddWithValue("@createtime", DateTime.Now);
                    cmd.Parameters.AddWithValue("@acceptempid", Driver.ID);
                    cmd.Parameters.AddWithValue("@acceptempname", Driver.Name);
                    cmd.Parameters.AddWithValue("@updtime", DateTime.Now);
                    cmd.Parameters.AddWithValue("@updempid", Driver.ID);
                    cmd.Parameters.AddWithValue("@updempname", Driver.Name);

                    conn.Open();
                    if (cmd.ExecuteNonQuery() < 1)
                    {
                        r.Status = "Error";
                        r.RtnObject = "新增簽名記錄失敗";
                        return JsonConvert.SerializeObject(r);
                    }

                    cmd.Parameters.Clear();
                    cmd.CommandText = "Select @@Identity";
                    object obj = cmd.ExecuteScalar();
                    int SignatureID;
                    SignatureID = Convert.ToInt32(obj);



                    sb.Length = 0;
                    cmd.Parameters.Clear();
                    sb.Append("Update CRM013 Set Signature_AutoID = @id,CloseFlag = '9'  Where Sno = @no");
                    cmd.CommandText = sb.ToString();

                    cmd.Parameters.AddWithValue("@no", SheetNo);
                    cmd.Parameters.AddWithValue("@id", SignatureID);

                    if (cmd.ExecuteNonQuery() <= 0)
                    {
                        r.Status = "Error";
                        r.RtnObject = "請確認簽單號碼 - 無更新的簽單";
                    }
                    else
                    {
                        r.Status = "Success";
                        r.RtnObject = null;
                    }
                    return JsonConvert.SerializeObject(r);
                }
            }
            catch (Exception ex)
            {
                r.Status = "Error";
                r.RtnObject = ex.Message;
                return JsonConvert.SerializeObject(r);
            }
        }

        [Description("(新)回傳異況並取得上傳之Meta Data  form-data key name：no:簽單編號 id：司機的系統編號 name：司機姓名 cnt：上傳照片數量 Con：異況內容")]
        [OperationContract, WebInvoke(UriTemplate = "ReportConditionV2", Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public string ReportConditionV2(Stream Input)
        {
            ReturnValue r = new ReturnValue();
            string data;
            ReportJson report;

            try
            {
                HttpMultipartParser parser = new HttpMultipartParser(Input, "form");
                if (!parser.Success)
                {
                    r.Status = "Error";
                    r.RtnObject = "Form-data Parse 失敗";
                    return JsonConvert.SerializeObject(r);
                }
                data = parser.Parameters["data"];
                report = JsonConvert.DeserializeObject<ReportJson>(data);
            }
            catch (Exception)
            {
                r.Status = "Error";
                r.RtnObject = "參數格式錯誤";
                return JsonConvert.SerializeObject(r);
            }
            try
            {
                string Uri = string.Empty;
                DateTime Now = DateTime.Now;
                string NowTime = Now.ToString("yyyyMMddHHmmss");
                int imgcnt;
                MetaData md = new MetaData();

                if (!Int32.TryParse(report.cnt, out imgcnt))
                {
                    r.Status = "Error";
                    r.RtnObject = "cnt must be integer";
                }

                if (imgcnt > 0)
                {
                    md.FtpPath = "";
                    md.ImageName = new List<string>();
                    for (int i = 1; i <= imgcnt; i++)
                        md.ImageName.Add(report.no + "_" + i.ToString() + ".jpeg");
                    md.Sno = report.no;
                    md.Uri = Uri = @"https://www.callcar.com.tw/MobileAPI/Condition/";

                }
                else if (imgcnt < 0)
                {
                    r.Status = "Error";
                    r.RtnObject = "cnt must be positive integer";
                }

                //更新資料
                using (SqlConnection conn = new SqlConnection(ConnectionString))
                {
                    conn.Open();
                    StringBuilder sb = new StringBuilder();
                    sb.Append("Select reportcontent from DCR Where Sno = @no");
                    object rtn;

                    using (SqlCommand cmd = new SqlCommand(sb.ToString(), conn))
                    {
                        cmd.Parameters.AddWithValue("@no", report.no);
                        rtn = cmd.ExecuteScalar();
                    }

                    sb.Length = 0;
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        if (rtn == null || rtn == DBNull.Value)
                        {
                            //新增
                            sb.Append("INSERT INTO DCR(Sno,ReportSource,ReportType,ReportContent,ReportCashAgencyCost,Status, ");
                            sb.Append("AcceptEmpId,AcceptEmpName,CreateTime,UpdEmpId,UpdEmpName,UpdateTime,image_uri,image_name) ");
                            sb.Append("VALUES (@sno,@reportsource,@reporttype,@reportcontent,@reportcashagencycost,@status, ");
                            sb.Append("@acceptempid,@acceptempname,@createtime,@updempid,@updempname,@updatetime,@image_uri,@image_name) ");

                            cmd.Parameters.AddWithValue("@sno", report.no);
                            cmd.Parameters.AddWithValue("@reportsource", "數位");
                            cmd.Parameters.AddWithValue("@reporttype", "異況");
                            cmd.Parameters.AddWithValue("@reportcontent", report.Con);
                            cmd.Parameters.AddWithValue("@reportcashagencycost", 0);
                            cmd.Parameters.AddWithValue("@status", "未結案");
                            cmd.Parameters.AddWithValue("@acceptempid", Int32.Parse(report.id));
                            cmd.Parameters.AddWithValue("@acceptempname", report.name);
                            cmd.Parameters.AddWithValue("@createtime", Now);
                            cmd.Parameters.AddWithValue("@updempid", Int32.Parse(report.id));
                            cmd.Parameters.AddWithValue("@updempname", report.name);
                            cmd.Parameters.AddWithValue("@updatetime", Now);
                            if (imgcnt == 0)
                            {
                                cmd.Parameters.AddWithValue("@image_uri", "");
                                cmd.Parameters.AddWithValue("@image_name", "");
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@image_uri", md.Uri);
                                cmd.Parameters.AddWithValue("@image_name", JsonConvert.SerializeObject(md.ImageName));
                            }

                        }
                        else
                        {
                            report.Con = Convert.ToString(rtn) + Environment.NewLine + report.Con;
                            sb.Length = 0;
                            cmd.Parameters.Clear();
                            sb.Append("UPDATE DCR ");
                            sb.Append("SET ReportContent = @reportcontent,UpdEmpId = @updempid,UpdEmpName = @updempname,UpdateTime = @updatetime, ");
                            sb.Append("image_uri = @image_uri,image_name = @image_name, Status=@status WHERE  Sno = @sno");

                            cmd.Parameters.AddWithValue("@sno", report.no);
                            cmd.Parameters.AddWithValue("@reportcontent", report.Con);
                            cmd.Parameters.AddWithValue("@updempid", Int32.Parse(report.id));
                            cmd.Parameters.AddWithValue("@updempname", report.name);
                            cmd.Parameters.AddWithValue("@updatetime", Now);
                            cmd.Parameters.AddWithValue("@status", "未結案");


                            if (imgcnt > 0)
                            {
                                cmd.Parameters.AddWithValue("@image_uri", md.Uri);
                                cmd.Parameters.AddWithValue("@image_name", JsonConvert.SerializeObject(md.ImageName));
                            }
                        }
                        cmd.Connection = conn;
                        cmd.CommandText = sb.ToString();
                        cmd.ExecuteNonQuery();
                    }
                    r.RtnObject = md;
                    r.Status = "Success";
                    return JsonConvert.SerializeObject(r);
                }

            }
            catch (Exception ex)
            {
                r.Status = "Error";
                r.RtnObject = ex.Message;
                return JsonConvert.SerializeObject(r);
            }
        }

        #endregion

        private DriverInfo GetDriver(int DriverID)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionString))
                {
                    conn.Open();
                    StringBuilder sb = new StringBuilder();
                    sb.Append("SELECT EmpId,EmpName,tCarTeams_No ");
                    sb.Append("FROM Users where EmpId = @id  ");
                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandText = sb.ToString();
                    cmd.Connection = conn;
                    cmd.Parameters.AddWithValue("@id", DriverID);

                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        if (dr.HasRows)
                        {
                            dr.Read();
                            DriverInfo i = new DriverInfo();
                            i.ID = Convert.ToString(dr["EmpId"]);
                            i.Name = Convert.ToString(dr["EmpName"]);
                            i.FleetNo = Convert.ToString(dr["tCarTeams_No"]);
                            return i;
                        }
                        else
                            return null;
                    }
                }
            }
            catch (Exception)
            {
                return null;
            }
        }
        #region 類別
        public class MetaData
        {
            public string Sno { get; set; }
            public string Uri { get; set; }
            public string FtpPath { get; set; }
            public List<string> ImageName { get; set; }
            public int SignID { get; set; } //回傳新增path的資料庫ID  寫入簽單用

        }
        //司機/Mobile登入資料
        public class DriverInfo
        {
            public string ID { get; set; }
            public string Name { get; set; }
            public string FleetNo { get; set; } //車隊編號
        }
        //回傳資料結構
        public class ReturnValue
        {
            public string Status { get; set; }
            public object RtnObject { get; set; }
        }
        //司機簽單資料
        public class EmpReservData
        {
            public string QryTime { get; set; }
            public string EmpID { get; set; }
            public int ReservationCnt { get; set; }
            public List<ReservationData> ReservList { get; set; }
        }
        //簽單資料
        public class ReservationData
        {
            public string ServiceType { get; set; } //客戶種類
            public string ServiceName { get; set; } //服務種類
            public string ServeDate { get; set; } //服務日期
            public string ServeTime { get; set; } //服務時間
            public string ReservNo { get; set; } //預約單號
            public string FlightNo { get; set; }//航班編號
            public string FightTime { get; set; }//航班時間
            public string CarType { get; set; }//服務車款
            public string PassengerName { get; set; }//乘客姓名
            public string PassengerMobile { get; set; }//乘客電　　話
            public string CustomerName { get; set; }//客戶名稱
            //public string CustomerTel { get; set; }//訂車人電話
            public string CustomerType { get; set; } //客戶屬性
            public string OrderName { get; set; } //訂車人名稱
            public string OrderTel { get; set; } //訂車人電話
            public string Pickup { get; set; }//上　　車：地址、航站、航廈
            public string Getoff { get; set; }//下　　車：地址 航站、航廈
            public string GetCash { get; set; }//系統指定司機該收的RecordCash
            public string RecordCash { get; set; }//司機登錄收到的
            public string PassengerCnt { get; set; }//人數
            public string BaggageCnt { get; set; }//行李
            public string BoardMemo { get; set; }//舉牌
            public string SaftySeat { get; set; }//幼椅
            public string StopAddress1 { get; set; }//停靠1
            public string ContactName1 { get; set; }//同行者1
            public string ContactTel1 { get; set; } //同行者電話1
            public string StopAddress2 { get; set; }//停靠2
            public string ContactName2 { get; set; }//同行者2
            public string ContactTel2 { get; set; } //同行者電話2
            public string StopAddress3 { get; set; }//停靠3
            public string ContactName3 { get; set; }//同行者3
            public string ContactTel3 { get; set; } //同行者電話3
            public string StopAddress4 { get; set; }//停靠4
            public string ContactName4 { get; set; }//同行者4
            public string ContactTel4 { get; set; } //同行者電話4
            public string SignFlag { get; set; } //是否已數位簽收
            public string StopCnt { get; set; } //停靠點數量
            public string BagNote { get; set; } //行李尺寸
            public string CardNo { get; set; } //卡號
            public string CarNo { get; set; } //車號
            public string Memo { get; set; } //客服備註            
            public string SpecifiedTime { get; set; }//指定時間接送
            public string DriverName { get; set; } //司機姓名
            public string SignturePath { get; set; } //簽單路徑上傳Json
        }

        public class ReportCon
        {
            public string ReservNo { get; set; } //預約單號
            public string Content { get; set; } //回報內容
            public string Status { get; set; } //狀態
        }

        public class ReportJson
        {
            public string id { get; set; }
            public string no { get; set; }
            public string Con { get; set; }
            public string cnt { get; set; }
            public string name { get; set; }
        }

        #endregion
    }
}
