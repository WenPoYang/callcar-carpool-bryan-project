// APIs
public void SendCRM013Email(string Sno)
{
	try
	{
		DB_CRMS dB_CRMS = new DB_CRMS();
		string text = "";
		string address = "service@callcar.com.tw";
		string subject = "";
		string text2 = "";
		DataTable dataTable = dB_CRMS.Get_CRM013_For_Email_SMS("SELECT * FROM CRM013 WHERE Sno = '" + Sno + "'");
		Common.LogWithSysTime(Parameters.Debu_LogPath + "\\Email.txt", "Sno：" + Sno + "，dt.Rows.Count：" + dataTable.Rows.Count.ToString());
		int i = 0;
		while (i < dataTable.Rows.Count)
		{
			string a = dataTable.Rows[i]["客戶類別"].ToString().Trim();
			if (a == "JCB晶緻 (2016)" || a == "JCB晶緻 (2016網路)" || a == "JCB晶緻(2016)" || a == "JCB晶緻(2016網路)" || a == "JCB晶緻（2016）" || a == "JCB晶緻（2016網路）" || a == "JCB自費 (2016)" || a == "JCB自費 (2016網路)" || a == "JCB自費(2016)" || a == "JCB自費(2016網路)" || a == "JCB自費（2016）" || a == "JCB自費（2016網路）")
			{
				text = "J07";
			}
			else if (a == "JCB極緻卡" || a == "JCB尊緻卡")
			{
				text = "AS07";
			}
			else
			{
				text = "C07";
			}
			text = text.ToUpper();
			string text3 = HttpContext.Current.Server.MapPath("../DriverApp/Email/" + text + ".htm");
			Common.LogWithSysTime(Parameters.Debu_LogPath + "\\Email.txt", "SendCRM013Email  客戶類別：" + text + "，FilePath：" + text3.ToString());
			if (File.Exists(text3))
			{
				using (StreamReader streamReader = new StreamReader(text3))
				{
					string str;
					while ((str = streamReader.ReadLine()) != null)
					{
						text2 += str;
					}
					goto IL_216;
				}
				break;
				IL_216:
				if (text == "J05" || text == "C05" || text == "AS05" || text == "HSR05" || text == "OM05")
				{
					text2 = text2.Replace("$D", dataTable.Rows[i]["退件原因"].ToString());
				}
				else
				{
					text2 = text2.Replace("$D", "");
				}
				if (text == "J06" || text == "C06" || text == "AS06" || text == "HSR06" || text == "OM06")
				{
					text2 = text2.Replace("$E", dataTable.Rows[i]["取消原因"].ToString());
				}
				else
				{
					text2 = text2.Replace("$E", "");
				}
				if (text == "C01" || text == "C02" || text == "C03" || text == "C05" || text == "C06" || text == "T01")
				{
					text2 = text2.Replace("$F10", dataTable.Rows[i]["包車時數"].ToString());
					text2 = text2.Replace("$F9", dataTable.Rows[i]["其他地址"].ToString());
				}
				else
				{
					text2 = text2.Replace("$F10", "");
					text2 = text2.Replace("$F9", "");
				}
				text2 = text2.Replace("$F11", dB_CRMS.GetAirportAllName(dataTable.Rows[i]["機場航廈"].ToString()));
				text2 = text2.Replace("$F12", dataTable.Rows[i]["航班編號"].ToString());
				string newValue = (dataTable.Rows[i]["航班時間"].ToString().Length > 4) ? dataTable.Rows[i]["航班時間"].ToString().Substring(0, 5) : dataTable.Rows[i]["航班時間"].ToString();
				text2 = text2.Replace("$F13", newValue);
				text2 = text2.Replace("$F14", dataTable.Rows[i]["自費項目"].ToString());
				text2 = text2.Replace("$F15", dataTable.Rows[i]["搭乘人數"].ToString());
				text2 = text2.Replace("$F16", dataTable.Rows[i]["行李件數"].ToString());
				text2 = text2.Replace("$F17", dataTable.Rows[i]["同行人姓名1"].ToString());
				text2 = text2.Replace("$F18", dataTable.Rows[i]["同行人電話1"].ToString());
				if (text == "J02" || text == "J02_2016" || text == "J03" || text == "J03_2016" || text == "AS01" || text == "AS02" || text == "AS03" || text == "C02" || text == "C03")
				{
					text2 = text2.Replace("$G4", dataTable.Rows[i]["停靠地址4"].ToString());
					text2 = text2.Replace("$G3", dataTable.Rows[i]["停靠地址3"].ToString());
					text2 = text2.Replace("$G2", dataTable.Rows[i]["停靠地址2"].ToString());
					text2 = text2.Replace("$G1", dataTable.Rows[i]["停靠地址1"].ToString());
				}
				else
				{
					text2 = text2.Replace("$G4", "");
					text2 = text2.Replace("$G3", "");
					text2 = text2.Replace("$G2", "");
					text2 = text2.Replace("$G1", "");
				}
				text2 = text2.Replace("$F19", dataTable.Rows[i]["停靠點數"].ToString());
				text2 = text2.Replace("$F20", dataTable.Rows[i]["顯示備註"].ToString());
				text2 = text2.Replace("$F8", dataTable.Rows[i]["服務地址"].ToString());
				string newValue2 = (dataTable.Rows[i]["乘車時間"].ToString().Length > 4) ? dataTable.Rows[i]["乘車時間"].ToString().Substring(0, 5) : dataTable.Rows[i]["乘車時間"].ToString();
				text2 = text2.Replace("$F7", newValue2);
				text2 = text2.Replace("$F6", Convert.ToDateTime(dataTable.Rows[i]["服務日期"]).ToString("yyyy-MM-dd"));
				text2 = text2.Replace("$F5", dataTable.Rows[i]["服務車款"].ToString());
				text2 = text2.Replace("$F4", dataTable.Rows[i]["預約車款"].ToString());
				text2 = text2.Replace("$F3", dataTable.Rows[i]["服務類型"].ToString());
				text2 = text2.Replace("$F2", dataTable.Rows[i]["專案名稱"].ToString());
				text2 = text2.Replace("$F1", dataTable.Rows[i]["服務單號"].ToString());
				if (text == "J01" || text == "J02" || text == "J02_2016" || text == "AS01" || text == "AS02" || text == "AS03" || text == "OM01" || text == "HSR02" || text == "HSR03" || text == "OM01" || text == "OM02")
				{
					text2 = text2.Replace("$A3", dataTable.Rows[i]["刷卡金額"].ToString());
					text2 = text2.Replace("$A4", dataTable.Rows[i]["信用卡前2_12碼"].ToString());
				}
				else
				{
					text2 = text2.Replace("$A3", "");
					text2 = text2.Replace("$A4", "");
				}
				text2 = text2.Replace("$A2", dataTable.Rows[i]["貴賓電話"].ToString());
				text2 = text2.Replace("$A1", dataTable.Rows[i]["貴賓姓名"].ToString());
				if (text == "C01" || text == "C02" || text == "C04" || text == "C05" || text == "C06" || text == "T01")
				{
					text2 = text2.Replace("$B2", dataTable.Rows[i]["訂車人電話"].ToString());
					text2 = text2.Replace("$B1", dataTable.Rows[i]["訂車人姓名"].ToString());
				}
				else
				{
					text2 = text2.Replace("$B2", "");
					text2 = text2.Replace("$B1", "");
				}
				if (text == "C03" || text == "J03" || text == "J03_2016" || text == "AS03")
				{
					text2 = text2.Replace("$C3", dataTable.Rows[i]["司機行動"].ToString());
					text2 = text2.Replace("$C2", dataTable.Rows[i]["車號"].ToString());
					text2 = text2.Replace("$C1", dataTable.Rows[i]["服務司機"].ToString());
				}
				else
				{
					text2 = text2.Replace("$C3", "");
					text2 = text2.Replace("$C2", "");
					text2 = text2.Replace("$C1", "");
				}
				string key;
				switch (key = text)
				{
				case "J01":
				case "C01":
				case "AS01":
				case "HSR01":
				case "OM01":
					subject = "CallCar " + dataTable.Rows[i]["貴賓姓名"].ToString() + " 網路預約登錄受理";
					break;
				case "T01":
					subject = string.Concat(new string[]
					{
						"CallCar ",
						DateTime.Now.ToString("yyyy-MM-dd"),
						" ",
						dataTable.Rows[i]["貴賓姓名"].ToString(),
						" 服務取消通知"
					});
					break;
				case "J02":
				case "J02_2016":
				case "C02":
				case "AS02":
				case "HSR02":
				case "OM02":
					subject = string.Concat(new string[]
					{
						"CallCar ",
						Convert.ToDateTime(dataTable.Rows[i]["服務日期"]).ToString("yyyy-MM-dd"),
						" ",
						dataTable.Rows[i]["貴賓姓名"].ToString(),
						" 預約完成通知"
					});
					break;
				case "J03":
				case "J03_2016":
				case "C03":
				case "AS03":
				case "OM03":
					subject = string.Concat(new string[]
					{
						"CallCar ",
						Convert.ToDateTime(dataTable.Rows[i]["服務日期"]).ToString("yyyy-MM-dd"),
						" ",
						dataTable.Rows[i]["貴賓姓名"].ToString(),
						" 派遣完成通知"
					});
					break;
				case "HSR03":
					subject = string.Concat(new string[]
					{
						"CallCar ",
						Convert.ToDateTime(dataTable.Rows[i]["服務日期"]).ToString("yyyy-MM-dd"),
						" ",
						dataTable.Rows[i]["貴賓姓名"].ToString(),
						" 高鐵升等劵寄送完成通知"
					});
					break;
				case "J04":
				case "C04":
				case "AS04":
				case "HSR04":
				case "OM04":
					subject = string.Concat(new string[]
					{
						"CallCar ",
						Convert.ToDateTime(dataTable.Rows[i]["服務日期"]).ToString("yyyy-MM-dd"),
						" ",
						dataTable.Rows[i]["貴賓姓名"].ToString(),
						" 服務完成通知"
					});
					break;
				case "J05":
				case "C05":
				case "AS05":
				case "OM05":
					subject = string.Concat(new string[]
					{
						"CallCar ",
						Convert.ToDateTime(dataTable.Rows[i]["服務日期"]).ToString("yyyy-MM-dd"),
						" ",
						dataTable.Rows[i]["貴賓姓名"].ToString(),
						" 預約未成功通知"
					});
					break;
				case "HSR05":
					subject = string.Concat(new string[]
					{
						"CallCar ",
						Convert.ToDateTime(dataTable.Rows[i]["服務日期"]).ToString("yyyy-MM-dd"),
						" ",
						dataTable.Rows[i]["貴賓姓名"].ToString(),
						" 高鐵升等劵申請未成功通知"
					});
					break;
				case "J06":
				case "C06":
				case "AS06":
				case "HSR06":
				case "OM06":
					subject = string.Concat(new string[]
					{
						"CallCar ",
						Convert.ToDateTime(dataTable.Rows[i]["服務日期"]).ToString("yyyy-MM-dd"),
						" ",
						dataTable.Rows[i]["貴賓姓名"].ToString(),
						" 服務取消通知"
					});
					break;
				case "J07":
				case "C07":
				case "AS07":
					subject = string.Concat(new string[]
					{
						"CallCar ",
						Convert.ToDateTime(dataTable.Rows[i]["服務日期"]).ToString("yyyy-MM-dd"),
						" ",
						dataTable.Rows[i]["貴賓姓名"].ToString(),
						" 服務完成通知"
					});
					break;
				}
				string text4 = dataTable.Rows[i]["電子郵件"].ToString();
				string text5 = "";
				SmtpClient smtpClient = new SmtpClient("127.0.0.1", 25);
				smtpClient.Credentials = new NetworkCredential("service", "service1");
				using (MailMessage mailMessage = new MailMessage())
				{
					mailMessage.From = new MailAddress(address);
					if (text4.EndsWith(";"))
					{
						text4 = text4.Substring(0, text4.Length - 1);
					}
					string[] array = text4.Split(";".ToCharArray());
					string[] array2 = array;
					for (int j = 0; j < array2.Length; j++)
					{
						string address2 = array2[j];
						mailMessage.To.Add(new MailAddress(address2));
					}
					if (text5.EndsWith(";"))
					{
						text5 = text5.Substring(0, text5.Length - 1);
					}
					string[] array3 = text5.Split(";".ToCharArray());
					string[] array4 = array3;
					for (int k = 0; k < array4.Length; k++)
					{
						string address3 = array4[k];
						mailMessage.Bcc.Add(new MailAddress(address3));
					}
					Common.LogWithSysTime(Parameters.Debu_LogPath + "\\Email.txt", "msg.Body：" + text2);
					mailMessage.Subject = subject;
					mailMessage.Body = text2;
					mailMessage.IsBodyHtml = true;
					mailMessage.SubjectEncoding = Encoding.UTF8;
					mailMessage.BodyEncoding = Encoding.UTF8;
					smtpClient.Send(mailMessage);
				}
				i++;
				continue;
			}
			break;
		}
	}
	catch (Exception ex)
	{
		Common.LogWithSysTime(Parameters.Debu_LogPath + "\\Email.txt", "SendCRM013Email  發送E-mail的時候，出現例外情況：" + ex.ToString());
	}
}
