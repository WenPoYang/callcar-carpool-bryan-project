﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Callcar.CObject;
using MySql.Data.MySqlClient;
using DbConn;
using System.Data;
using Newtonsoft.Json;
using NLog;
using NLog.Config;
using System.Collections.Specialized;
using System.Configuration;

namespace FlightTrackApplication
{
    class Program
    {
        private static string ConnectionString;
        private static DateTime NowTime;
        private static Logger logger = NLog.LogManager.GetCurrentClassLogger();

        static void Main(string[] args)
        {
            logger.Info("執行紀錄：" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            NameValueCollection nc = ConfigurationManager.AppSettings;
            if (nc["Server"] == "Production")
                ConnectionString = "Server=10.0.2.20;Port=3306;Database=carpoollab;Uid=ishareco;Pwd=fu0!j0$su0^vup#;Allow User Variables=True";
            else if (nc["Server"] == "Lab")
                ConnectionString = "Server=10.0.2.21;Port=3306;Database=carpoollab;Uid=apiuser;Pwd=7oPOh88I;Allow User Variables=True";

            NowTime = DateTime.Now;
            StringBuilder sb = new StringBuilder();
            DataTable dt = new DataTable();

            sb.Append("select Distinct FlightDate,FlightNo,ServiceType ");
            sb.Append("from reservation_charge_list ");
            sb.Append("where  FlightTime between DATE_SUB(now() ,INTERVAL 4 HOUR) and  DATE_ADD(now() ,INTERVAL 4 HOUR) and SelectionType = 'F'     ");
            List<MySqlParameter> parms = new List<MySqlParameter>();

            using (dbConnectionMySQL conn = new dbConnectionMySQL(ConnectionString))
            {
                try
                {
                    dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                }
                catch (Exception ex)
                {
                    logger.Error("查詢航班追蹤資料失敗  " + ex.StackTrace);
                    return;
                }
                //int SeqNo;
                string FlightDate, AirlineID, FlightNo, FlightType, Json = string.Empty;
                DateTime FlightTime = DateTime.Now;
                DateTime ActualTime = DateTime.Now;
                string FlightNoFull = string.Empty;
                string FlightRemark = string.Empty;
                string ServiceType = string.Empty;

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    //SeqNo = Convert.ToInt32(dt.Rows[i]["SeqNo"]);
                    FlightDate = Convert.ToString(dt.Rows[i]["FlightDate"]);
                    FlightNoFull = Convert.ToString(dt.Rows[i]["FlightNo"]);
                    AirlineID = FlightNoFull.Substring(0, 2); // Convert.ToString(dt.Rows[i]["AirlineID"]);
                    FlightNo = FlightNoFull.Substring(2, FlightNoFull.Length - 2); //Convert.ToString(dt.Rows[i]["FlightNo"]);
                    ServiceType = Convert.ToString(dt.Rows[i]["ServiceType"]);
                    if (ServiceType == "I")
                        FlightType = "PA";
                    else
                        FlightType = "PD";

                    //FlightTime = Convert.ToDateTime(dt.Rows[i]["FlightTime"]);

                    sb.Length = 0;
                    parms.Clear();
                    if (FlightType == "PA")
                    {

                        List<OArrivalFlightInfo> Lafi = new List<OArrivalFlightInfo>();
                        Callcar.Air.AirInfo air = new Callcar.Air.AirInfo();

                        Json = air.GetInstantFlightInfo(AirlineID, FlightNo);
                        if (Json == null || Json == "null" || Json == "Error")
                        {
                            logger.Error("PTX查無飛航資料 FlightDate: " + FlightDate + "  AirlineiD: " + AirlineID + " FlightNo:" + FlightNo);
                            continue;
                        }
                        try
                        {
                            Lafi = JsonConvert.DeserializeObject<List<OArrivalFlightInfo>>(Json);
                        }
                        catch (Exception ex)
                        {
                            logger.Error("解析Json失敗 FlightDate: " + FlightDate + "  AirlineiD: " + AirlineID + " FlightNo:" + FlightNo + Environment.NewLine + ex.StackTrace);
                            continue;
                        }
                        int index = 0;
                        if (Lafi.Count > 1)
                            index = Lafi.FindIndex(x => x.AirlineID == AirlineID);
                        else
                            index = 0;

                        if (index == -1)
                        {
                            logger.Error("PTX查無飛航資料 FlightDate: " + FlightDate + "  AirlineiD: " + AirlineID + " FlightNo:" + FlightNo);
                            continue;
                        }

                        if (Lafi[index].AirlineID == AirlineID && Lafi[index].FlightNumber == FlightNo && Lafi[index].FlightDate.Replace("-", "") == FlightDate)
                        {
                            if (Lafi[index].ScheduleArrivalTime == null)
                            {
                                logger.Error("資料為降落航班，可是PTX無降落資料  FlightDate: " + FlightDate + "  AirlineiD: " + AirlineID + " FlightNo:" + FlightNo);
                                continue;
                            }
                            //sb.Append("UPDATE flight_track ");
                            //sb.Append("SET DepartureAirportID=@DepartureAirportID,ArrivalAirportID=@ArrivalAirportID,ScheduleArrivalTime=@ScheduleArrivalTime,ActulArrivalTime=@ActulArrivalTime,  ");
                            //sb.Append("Ternimal=@Ternimal,	Gate=@Gate,	Remark=@Remark  ");
                            //sb.Append("WHERE SeqNo = @no  ");

                            //parms.Add(new MySqlParameter("@DepartureAirportID", Lafi[index].DepartureAirportID));
                            //parms.Add(new MySqlParameter("@ArrivalAirportID", Lafi[index].ArrivalAirportID));
                            //parms.Add(new MySqlParameter("@ScheduleArrivalTime", Convert.ToDateTime(Lafi[index].ScheduleArrivalTime.Replace("T", " "))));
                            //parms.Add(new MySqlParameter("@ActulArrivalTime", Convert.ToDateTime(Lafi[index].ActualArrivalTime.Replace("T", " "))));
                            //parms.Add(new MySqlParameter("@Ternimal", Lafi[index].ArrivalTerminal));
                            //parms.Add(new MySqlParameter("@Gate", Lafi[index].ArrivalGate));
                            //parms.Add(new MySqlParameter("@Remark", Lafi[index].ArrivalRemark));
                            //parms.Add(new MySqlParameter("@no", SeqNo));

                            //try
                            //{
                            //    conn.executeUpdateQuery(sb.ToString(), parms.ToArray());
                            //}
                            //catch (Exception ex)
                            //{
                            //    logger.Error("更新航班追蹤資料失敗 FlightDate: " + FlightDate + "  AirlineiD: " + AirlineID + " FlightNo:" + FlightNo + Environment.NewLine + ex.StackTrace);
                            //    continue;
                            //}

                            FlightTime = Convert.ToDateTime(Lafi[index].ScheduleArrivalTime.Replace("T", " "));
                            ActualTime = Convert.ToDateTime(Lafi[index].ActualArrivalTime.Replace("T", " "));
                            FlightRemark = Lafi[index].ArrivalRemark;
                        }
                    }
                    else
                    {
                        List<ODepartureFlightInfo> Ldfi = new List<ODepartureFlightInfo>();
                        Callcar.Air.AirInfo air = new Callcar.Air.AirInfo();
                        Json = air.GetInstantFlightInfo(AirlineID, FlightNo);
                        if (Json == null || Json == "null")
                        {
                            logger.Error("PTX查無飛航資料 FlightDate: " + FlightDate + "  AirlineiD: " + AirlineID + " FlightNo:" + FlightNo);
                            continue;
                        }

                        try
                        {
                            Ldfi = JsonConvert.DeserializeObject<List<ODepartureFlightInfo>>(Json);
                        }
                        catch (Exception ex)
                        {
                            logger.Error("解析Json失敗 FlightDate: " + FlightDate + "  AirlineiD: " + AirlineID + " FlightNo:" + FlightNo + Environment.NewLine + ex.StackTrace);
                            continue;
                        }

                        int index = 0;
                        if (Ldfi.Count > 1)
                            index = Ldfi.FindIndex(x => x.AirlineID == AirlineID);
                        else
                            index = 0;

                        if (index == -1)
                        {
                            logger.Error("PTX查無飛航資料 FlightDate: " + FlightDate + "  AirlineiD: " + AirlineID + " FlightNo:" + FlightNo);
                            continue;
                        }

                        if (Ldfi[index].AirlineID == AirlineID && Ldfi[index].FlightNumber == FlightNo && Ldfi[index].FlightDate.Replace("-", "") == FlightDate)
                        {
                            if (Ldfi[index].ScheduleDepartureTime == null)
                            {
                                logger.Error("資料為起飛航班，可是PTX無起飛資料 FlightDate: " + FlightDate + "  AirlineiD: " + AirlineID + " FlightNo:" + FlightNo);
                                continue;
                            }
                            //sb.Append("UPDATE flight_track ");
                            //sb.Append("SET DepartureAirportID=@DepartureAirportID,ArrivalAirportID=@ArrivalAirportID,ScheduleDepartureTime=@ScheduleDepartureTime,ActulDepartureTime=@ActulDepartureTime,  ");
                            //sb.Append("Ternimal=@Ternimal,	Gate=@Gate,	Remark=@Remark  ");
                            //sb.Append("WHERE SeqNo = @no  ");

                            //parms.Add(new MySqlParameter("@DepartureAirportID", Ldfi[index].DepartureAirportID));
                            //parms.Add(new MySqlParameter("@ArrivalAirportID", Ldfi[index].ArrivalAirportID));
                            //parms.Add(new MySqlParameter("@ScheduleDepartureTime", Convert.ToDateTime(Ldfi[index].ScheduleDepartureTime.Replace("T", " "))));
                            //parms.Add(new MySqlParameter("@ActulDepartureTime", Convert.ToDateTime(Ldfi[index].ActualDepartureTime.Replace("T", " "))));
                            //parms.Add(new MySqlParameter("@Ternimal", Ldfi[index].DepartureTerminal));
                            //parms.Add(new MySqlParameter("@Gate", Ldfi[index].DepartureGate));
                            //parms.Add(new MySqlParameter("@Remark", Ldfi[index].DepartureRemark));
                            //parms.Add(new MySqlParameter("@no", SeqNo));

                            //try
                            //{
                            //    conn.executeUpdateQuery(sb.ToString(), parms.ToArray());
                            //}
                            //catch (Exception ex)
                            //{
                            //    logger.Error("更新航班追蹤資料失敗 FlightDate: " + FlightDate + "  AirlineiD: " + AirlineID + " FlightNo:" + FlightNo + Environment.NewLine + ex.StackTrace);
                            //    continue;
                            //}
                            FlightTime = Convert.ToDateTime(Ldfi[index].ScheduleDepartureTime.Replace("T", " "));
                            ActualTime = Convert.ToDateTime(Ldfi[index].ActualDepartureTime.Replace("T", " "));
                            FlightRemark = Ldfi[index].DepartureRemark;
                        }
                    }

                    sb.Length = 0;
                    parms.Clear();

                    sb.Append("update reservation_charge_list set FlightTime = @FlightTime, FlightRemark =@FlightRemark, ActualFlightTime = @ActualFlightTime  where FlightNo = @FlightNo and  FlightDate = @FlightDate and SelectionType = 'F'");
                    parms.Add(new MySqlParameter("@FlightTime", FlightTime));
                    parms.Add(new MySqlParameter("@FlightRemark", FlightRemark));
                    parms.Add(new MySqlParameter("@ActualFlightTime", ActualTime));
                    parms.Add(new MySqlParameter("@FlightNo", FlightNoFull));
                    parms.Add(new MySqlParameter("@FlightDate", FlightDate));
                    try
                    {
                        conn.executeUpdateQuery(sb.ToString(), parms.ToArray());
                    }
                    catch (MySqlException ex)
                    {
                        logger.Error("更新帳務追蹤資料失敗 FlightDate: " + FlightDate + "  AirlineiD: " + AirlineID + " FlightNo:" + FlightNo + Environment.NewLine + ex.Number + Environment.NewLine + ex.StackTrace);
                        continue;
                    }
                    catch (Exception ex)
                    {
                        logger.Error("更新帳務追蹤資料失敗 FlightDate: " + FlightDate + "  AirlineiD: " + AirlineID + " FlightNo:" + FlightNo + Environment.NewLine + Environment.NewLine + ex.StackTrace);
                        continue;
                    }

                }
            }
        }
    }
}
