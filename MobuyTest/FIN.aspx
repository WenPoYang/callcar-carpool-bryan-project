﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="FIN.aspx.cs" Inherits="FIN" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="js/prefixfree.min.js">
</head>
<body>
<div class="wrapper2">
  <div class="center">
  <form id="form1" runat="server">
        <div class="form-group">
            <label class="control-label">輸入統編:</label>
            <div>
            <asp:TextBox class="form-control" size="30" ID="SNO" runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label">產生電子發票 ，如不需要統編請直接點選確認</label>
        </div>
        <div class="form-group">
            <asp:Button  class="btn-yellow" ID="check" runat="server" Text="確定" onclick="check_Click" />
        </div>
        </form>
    </div>    
</div>
<script type="text/javascript"src="js/jquery-1.11.3.min.js"></script>

<script type="text/javascript"src="js/components/core-min.js"></script>
<script type="text/javascript"src="js/components/enc-utf16-min.js"></script>
<script type="text/javascript"src="js/components/enc-base64-min.js"></script>
<script type="text/javascript"src="js/rollups/aes.js"></script>
<script type="text/javascript"src="js/rollups/sha256.js"></script>

</body>
</html>
