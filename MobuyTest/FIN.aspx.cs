﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Net;
using System.Text;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.SessionState;
using System.IO;
using System.Xml;
using Newtonsoft.Json;
public partial class FIN : System.Web.UI.Page
{
    public void Page_Load(object sender, EventArgs e)
    {
        
        //this.Session["Data"] = Request.Form["JSONData"];
        //Request.Form["JSONData"];
	//Response.Write("Url:  " + Request.Url.AbsoluteUri);
        if (Request.Form["JSONData"] != null)
        {
            this.Session["Data"] = Request.Form["JSONData"];
            OriginalData OD = JsonConvert.DeserializeObject<OriginalData>(this.Session["Data"].ToString());
            if (OD.Status != "SUCCESS")
            {
                Response.Redirect("http://mobuytest.cablesoft.com.tw/testallpai/find.html");
            }
            else
            {
                if (Request.QueryString["Utoken"] != null)
                {
                    string targetUrl = "http://app.mobuy.tw/new/api2/getTransactionData?md5="+Request.QueryString["Utoken"];
                    HttpWebRequest request = HttpWebRequest.Create(targetUrl) as HttpWebRequest;
                    request.Method = "GET";
                    request.ContentType = "application/json";

                    string result = "";
                    // 取得回應資料
                    using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
                    {
                        using (StreamReader sr = new StreamReader(response.GetResponseStream()))
                        {
                            result = sr.ReadToEnd();
                        }
                    }

                    //Response.Write(result);
                    datafortoken temp = JsonConvert.DeserializeObject<datafortoken>(result);
                    this.Session["Utoken"] = temp.token;
                    this.Session["CarNum"] = temp.placeID;
                    this.Session["place"] = temp.carLicense;
                    this.Session["email"] = temp.email;
                    //Response.Write("Data:  " + this.Session["Data"].ToString());
                    //Response.Write("CarNum:  " + this.Session["CarNum"].ToString());
                    //Response.Write("place:  "+this.Session["place"].ToString());
                    
                }
                else
                {
                    Response.Write("<script language=javascript>alert('error code:SN0002!!');</" + "script>");
                }
            }
        }
	if (Request.QueryString["Utoken"] == null)
        {
		Response.Redirect("http://mobuytest.cablesoft.com.tw/testallpai/index.html");
                  
        }

        //this.Label1.Text = this.Session["Card_No2"].ToString();
    }
    private int overTen(int intVal) //超過10則十位數與個位數相加，直到相加後小於10
    {
        if (intVal >= 10)
            intVal = overTen((intVal / 10) + (intVal % 10));
        return intVal;
    }
    public void check_Click(object sender, EventArgs e)
    {
        if (SNO.Text != "")
        {
            if (SNO.Text.Trim().Length == 8)
            {
                int[] intTmpVal = new int[6];
                int intTmpSum = 0;
                for (int i = 0; i < 6; i++)
                {
                    //位置在奇數位置的*2，偶數位置*1，位置計算從0開始
                    if (i % 2 == 1)
                        intTmpVal[i] = overTen(int.Parse(SNO.Text[i].ToString()) * 2);
                    else
                        intTmpVal[i] = overTen(int.Parse(SNO.Text[i].ToString()));

                    intTmpSum += intTmpVal[i];
                }
                intTmpSum += overTen(int.Parse(SNO.Text[6].ToString()) * 4); //第6碼*4
                intTmpSum += overTen(int.Parse(SNO.Text[7].ToString())); //第7碼*1

                if (intTmpSum % 10 != 0) //除以10後餘0表示正確，反之則錯誤
                {
                    Response.Write("<script language=javascript>alert('統一編號錯誤');</" + "script>");
                    return;
                }
                this.Session["SNO"] = SNO.Text;
            }
            else
            {
                Response.Write("<script language=javascript>alert('統一編號錯誤');</" + "script>");
                return;
            }
        }
        else
        {
            this.Session["SNO"] = "";
        }
        toTicketOK();
        string ivn = getIVNumber();
        Response.Write("1: " + ivn);
        string number;
        XmlDocument ivnumberxml = new XmlDocument();
        ivnumberxml.LoadXml(ivn);
        XmlNodeList ivnumberList = ivnumberxml.GetElementsByTagName("GetInvoiceNumberResponse");
        foreach (XmlNode parentNode in ivnumberList)
        {
            if (parentNode is XmlElement)
            {
                XmlElement element = (XmlElement)parentNode;
                XmlNodeList ConnectionsCurrent = element.GetElementsByTagName("GetInvoiceNumberResult");

                foreach (XmlNode childNode in ConnectionsCurrent)
                {
                    number = ((XmlElement)childNode).ChildNodes.Item(0).Value;
                    //Response.Write("1: " + this.Session["Data"]);
                    createIVB(number);
		    string doesDone = TEST(number);
                    if(doesDone =="S")
			Response.Redirect("http://mobuytest.cablesoft.com.tw/testallpai/finish.html");
                }
            }
        }

    }
    public string toTicketOK()
    {
        string vvv = this.Session["Data"].ToString();
        OriginalData OD = JsonConvert.DeserializeObject<OriginalData>(vvv);
        bussinessData BD = JsonConvert.DeserializeObject<bussinessData>(OD.Result);
        WebRequest tRequest;
        tRequest = WebRequest.Create("http://app.mobuy.tw/new/api2/informPaymentDone");
        tRequest.Method = "post";
        tRequest.ContentType = " application/x-www-form-urlencoded;charset=UTF-8";
        NameValueCollection outgoingQueryString = HttpUtility.ParseQueryString(String.Empty);
        outgoingQueryString.Add("placeID", this.Session["place"].ToString());
        outgoingQueryString.Add("carLicense", this.Session["CarNum"].ToString());
        outgoingQueryString.Add("email", this.Session["email"].ToString());
        outgoingQueryString.Add("price", BD.Amt);
        outgoingQueryString.Add("paymentStatus", "true");
        outgoingQueryString.Add("token", this.Session["Utoken"].ToString());
        string postdata = outgoingQueryString.ToString();

        Byte[] byteArray = Encoding.UTF8.GetBytes(postdata);
        tRequest.ContentLength = byteArray.Length;

        Stream dataStream = tRequest.GetRequestStream();
        dataStream.Write(byteArray, 0, byteArray.Length);
        dataStream.Close();

        WebResponse tResponse = tRequest.GetResponse();

        dataStream = tResponse.GetResponseStream();

        StreamReader tReader = new StreamReader(dataStream);

        String sResponseFromServer = tReader.ReadToEnd();


        tReader.Close();
        dataStream.Close();
        tResponse.Close();

        return sResponseFromServer;
    }

    public string TEST(string ivnumber)
    {

        string BBB = this.Session["Data"].ToString();
        OriginalData OD = JsonConvert.DeserializeObject<OriginalData>(BBB);
        bussinessData BD = JsonConvert.DeserializeObject<bussinessData>(OD.Result);
        String endpoint = "http://invoice.paynow.com.tw/PayNowEInvoice.asmx?op=UploadInvoice";
        // 讀取 request_message
        XmlDocument soapEnvelopeXml = new XmlDocument();
        soapEnvelopeXml.LoadXml(@"<?xml version=""1.0"" encoding=""utf-8""?>
<soap:Envelope xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">
<soap:Body>
<UploadInvoice xmlns=""http://invoice.PayNow.com.tw/"">
      <mem_cid>42304785</mem_cid>
      <mem_pw>tk6ycr9u7w4=</mem_pw>
      <InvNo>SA41448692</InvNo>
      <BuyerId>1</BuyerId>
      <BuyerName>消費者姓名</BuyerName>
      <BuyerAdd></BuyerAdd>
      <BuyerPhoneNo></BuyerPhoneNo>
      <BuyerEmail>jjj@hhh.com</BuyerEmail>
      <TotalAmount>690</TotalAmount>
      <OrderNo>1485135050</OrderNo>
      <Send>0</Send>
      <CarrierType></CarrierType>
      <CarrierId1></CarrierId1>
      <CarrierId2></CarrierId2>
      <NPOBAN></NPOBAN>
    </UploadInvoice>
</soap:Body>
</soap:Envelope>");
        XmlNodeList nodeList = soapEnvelopeXml.GetElementsByTagName("UploadInvoice");
        foreach (XmlNode parentNode in nodeList)
        {
            if (parentNode is XmlElement)
            {
                XmlElement element = (XmlElement)parentNode;
                XmlNodeList ConnectionsCurrent = element.GetElementsByTagName("InvNo");

                foreach (XmlNode childNode in ConnectionsCurrent)
                {
                    //Console.Write("<" + ((XmlElement)childNode).Name + "> ");
                    ((XmlElement)childNode).ChildNodes.Item(0).Value = ivnumber;
                    //Response.Write(((XmlElement)childNode).ChildNodes.Item(0).Value);
                    //Console.Write(((XmlElement)childNode).ChildNodes.Item(0).Value);
                }
                XmlElement element2 = (XmlElement)parentNode;
                XmlNodeList ConnectionsCurrent2 = element2.GetElementsByTagName("mem_pw");
                foreach (XmlNode childNode in ConnectionsCurrent2)
                {
                    ((XmlElement)childNode).ChildNodes.Item(0).Value = TripleDESEncode();
                }

                XmlElement element3 = (XmlElement)parentNode;
                XmlNodeList ConnectionsCurrent3 = element3.GetElementsByTagName("BuyerId");
                foreach (XmlNode childNode in ConnectionsCurrent3)
                {
		   if(this.Session["SNO"]!="")
			((XmlElement)childNode).ChildNodes.Item(0).Value = this.Session["SNO"].ToString();
		    else
                        ((XmlElement)childNode).ChildNodes.Item(0).Value = "";
                }
                XmlElement element4 = (XmlElement)parentNode;
                XmlNodeList ConnectionsCurrent4 = element4.GetElementsByTagName("OrderNo");
                foreach (XmlNode childNode in ConnectionsCurrent4)
                {
                    ((XmlElement)childNode).ChildNodes.Item(0).Value = BD.MerchantOrderNo;
                }
                XmlElement element5 = (XmlElement)parentNode;
                XmlNodeList ConnectionsCurrent5 = element5.GetElementsByTagName("TotalAmount");
                foreach (XmlNode childNode in ConnectionsCurrent5)
                {
                    ((XmlElement)childNode).ChildNodes.Item(0).Value = BD.Amt;
                }
                XmlElement element6 = (XmlElement)parentNode;
                XmlNodeList ConnectionsCurrent6 = element6.GetElementsByTagName("BuyerEmail");
                foreach (XmlNode childNode in ConnectionsCurrent6)
                {
                    ((XmlElement)childNode).ChildNodes.Item(0).Value = this.Session["email"].ToString();
                }
            }
        }
        string postData = soapEnvelopeXml.OuterXml;
        //Response.Write("postData:  " + postData);
        byte[] data = Encoding.UTF8.GetBytes(postData);

        // HTTP POST 請求物件
        HttpWebRequest httpWReq = (HttpWebRequest)WebRequest.Create(endpoint);
        httpWReq.Headers.Add(@"SOAP:Action");
        httpWReq.Method = "POST";
        httpWReq.ContentType = "text/xml;charset=\"utf-8\"";
        httpWReq.ContentLength = data.Length;
        using (Stream stream = httpWReq.GetRequestStream())
        {
            stream.Write(data, 0, data.Length);
        }
        HttpWebResponse response = (HttpWebResponse)httpWReq.GetResponse();
        string responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();
	XmlDocument finishXML = new XmlDocument();
         finishXML.LoadXml(responseString);
         XmlNodeList finishList = finishXML.GetElementsByTagName("UploadInvoiceResult");
         string status = string.Empty;
        foreach (XmlNode parentNode in finishList )
        {
            if (parentNode is XmlElement)
            {
                XmlElement element = (XmlElement)parentNode;
                XmlNodeList ConnectionsCurrent = element.GetElementsByTagName("ReturnStr");

                foreach (XmlNode childNode in ConnectionsCurrent)
                {
                    status = ((XmlElement)childNode).ChildNodes.Item(0).Value;
                }
            }
        }
        return status ;

        //Console.Write("Finish!!! " + responseString);
    }

    public string TripleDESEncode()
    {
        string ReturnStr;
        string Password = "25420001";
        byte[] data = Encoding.UTF8.GetBytes(Password);
        TripleDES tdes = TripleDES.Create();
        tdes.IV = Encoding.UTF8.GetBytes("12345678");
        tdes.Key = Encoding.UTF8.GetBytes("1234567890" + Password + "123456");
        tdes.Mode = CipherMode.ECB;
        tdes.Padding = PaddingMode.Zeros;
        ICryptoTransform ict = tdes.CreateEncryptor();
        byte[] enc = ict.TransformFinalBlock(data, 0, data.Length);
        ReturnStr = Convert.ToBase64String(enc);

        return ReturnStr;
    }
    public string createIVB(string ivnumber)
    {
        string result;
        //Response.Write(this.Session["Data"].ToString());
        string vvv = this.Session["Data"].ToString();
        OriginalData OD = JsonConvert.DeserializeObject<OriginalData>(vvv);
        bussinessData BD = JsonConvert.DeserializeObject<bussinessData>(OD.Result);
        HttpWebRequest request = CreateIVBWebRequest();
        XmlDocument soapEnvelopeXml = new XmlDocument();
        soapEnvelopeXml.LoadXml(@"<?xml version=""1.0"" encoding=""utf-8""?>
<soap:Envelope xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">
<soap:Body>
<UploadInvoice_Body xmlns=""http://invoice.PayNow.com.tw/"">
      <kMapingCode>ivnumber</kMapingCode>
      <Description>測試商品2</Description>
      <Amount>200</Amount>
      <Quantity>1</Quantity>
      <UnitPrice>200</UnitPrice>
      <mem_cid>42304785</mem_cid>
      <orderno>test0010</orderno>
      <Remark>停車費</Remark>
    </UploadInvoice_Body>
</soap:Body>
</soap:Envelope>");
        XmlNodeList nodeList = soapEnvelopeXml.GetElementsByTagName("UploadInvoice_Body");
        foreach (XmlNode parentNode in nodeList)
        {
            if (parentNode is XmlElement)
            {
                XmlElement element = (XmlElement)parentNode;
                XmlNodeList ConnectionsCurrent = element.GetElementsByTagName("kMapingCode");

                foreach (XmlNode childNode in ConnectionsCurrent)
                {
                    //Console.Write("<" + ((XmlElement)childNode).Name + "> ");
                    ((XmlElement)childNode).ChildNodes.Item(0).Value = ivnumber;
                    //Response.Write(((XmlElement)childNode).ChildNodes.Item(0).Value);
                    //Console.Write(((XmlElement)childNode).ChildNodes.Item(0).Value);
                }
                XmlElement element2 = (XmlElement)parentNode;
                XmlNodeList ConnectionsCurrent2 = element.GetElementsByTagName("Description");

                foreach (XmlNode childNode in ConnectionsCurrent2)
                {
                    //Console.Write("<" + ((XmlElement)childNode).Name + "> ");
                    ((XmlElement)childNode).ChildNodes.Item(0).Value = BD.ItemDesc;
                    //Response.Write(((XmlElement)childNode).ChildNodes.Item(0).Value);
                    //Console.Write(((XmlElement)childNode).ChildNodes.Item(0).Value);
                }
                XmlElement element3 = (XmlElement)parentNode;
                XmlNodeList ConnectionsCurrent3 = element.GetElementsByTagName("Amount");

                foreach (XmlNode childNode in ConnectionsCurrent3)
                {
                    //Console.Write("<" + ((XmlElement)childNode).Name + "> ");
                    ((XmlElement)childNode).ChildNodes.Item(0).Value = BD.Amt;
                    //Response.Write(((XmlElement)childNode).ChildNodes.Item(0).Value);
                    //Console.Write(((XmlElement)childNode).ChildNodes.Item(0).Value);
                }
                XmlElement element4 = (XmlElement)parentNode;
                XmlNodeList ConnectionsCurrent4 = element.GetElementsByTagName("UnitPrice");

                foreach (XmlNode childNode in ConnectionsCurrent4)
                {
                    //Console.Write("<" + ((XmlElement)childNode).Name + "> ");
                    ((XmlElement)childNode).ChildNodes.Item(0).Value = BD.Amt;
                    //Response.Write(((XmlElement)childNode).ChildNodes.Item(0).Value);
                    //Console.Write(((XmlElement)childNode).ChildNodes.Item(0).Value);
                }

                XmlElement element5 = (XmlElement)parentNode;
                XmlNodeList ConnectionsCurrent5 = element5.GetElementsByTagName("orderno");
                foreach (XmlNode childNode in ConnectionsCurrent5)
                {
                    ((XmlElement)childNode).ChildNodes.Item(0).Value = BD.MerchantOrderNo;
                }
            }
        }
        using (Stream stream = request.GetRequestStream())
        {
            soapEnvelopeXml.Save(stream);
        }
        using (WebResponse response = request.GetResponse())
        {
            using (StreamReader rd = new StreamReader(response.GetResponseStream()))
            {
                string soapResult = rd.ReadToEnd();
                result = soapResult;
                //Response.Write(soapResult);
                //Console.WriteLine(soapResult);
            }
        }
        return result;
    }
    public HttpWebRequest CreateIVBWebRequest()
    {
        HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(@"http://invoice.paynow.com.tw/PayNowEInvoice.asmx?op=UploadInvoice_Body");
        webRequest.Headers.Add(@"SOAP:Action");
        webRequest.ContentType = "text/xml;charset=\"utf-8\"";
        webRequest.Accept = "text/xml";
        webRequest.Method = "POST";
        return webRequest;
    }

    public string getIVNumber()
    {
        string ivnumber;
        HttpWebRequest request = CreateWebRequest();
        XmlDocument soapEnvelopeXml = new XmlDocument();
        soapEnvelopeXml.LoadXml(@"<?xml version=""1.0"" encoding=""utf-8""?>
<soap:Envelope xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">
<soap:Body>
<GetInvoiceNumber xmlns=""http://invoice.PayNow.com.tw/"">
      <mem_cid>42304785</mem_cid>
      <Year>2017</Year>
      <period>0</period>
    </GetInvoiceNumber>
</soap:Body>
</soap:Envelope>");
        string Month = DateTime.Now.Month.ToString();
        string period = "";
        if (Month == "1" || Month == "2")
            period = "0";
        else if (Month == "3" || Month == "4")
            period = "1";
        else if (Month == "5" || Month == "6")
            period = "2";
        else if (Month == "7" || Month == "8")
            period = "3";
        else if (Month == "9" || Month == "10")
            period = "4";
        else if (Month == "11" || Month == "12")
            period = "5";
        XmlNodeList finishList = soapEnvelopeXml.GetElementsByTagName("GetInvoiceNumber");
        foreach (XmlNode parentNode in finishList)
        {
            if (parentNode is XmlElement)
            {
                XmlElement element = (XmlElement)parentNode;
                XmlNodeList ConnectionsCurrent = element.GetElementsByTagName("Year");

                foreach (XmlNode childNode in ConnectionsCurrent)
                {
                    ((XmlElement)childNode).ChildNodes.Item(0).Value= DateTime.Now.Year.ToString();
                }
                XmlElement element2 = (XmlElement)parentNode;
                XmlNodeList ConnectionsCurrent2 = element2.GetElementsByTagName("period");

                foreach (XmlNode childNode in ConnectionsCurrent2)
                {
                    ((XmlElement)childNode).ChildNodes.Item(0).Value = period;
                }
            }
        }
        string postData = soapEnvelopeXml.OuterXml;
        //Response.Write(postData);
        using (Stream stream = request.GetRequestStream())
        {
            soapEnvelopeXml.Save(stream);
        }
        using (WebResponse response = request.GetResponse())
        {
            using (StreamReader rd = new StreamReader(response.GetResponseStream()))
            {
                string soapResult = rd.ReadToEnd();
                ivnumber = soapResult;
                //Response.Write(soapResult);
                //Console.WriteLine(soapResult);
            }
        }
        return ivnumber;
    }
    public HttpWebRequest CreateWebRequest()
    {
        HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(@"http://invoice.paynow.com.tw/PayNowEInvoice.asmx?op=GetInvoiceNumber");
        webRequest.Headers.Add(@"SOAP:Action");
        webRequest.ContentType = "text/xml;charset=\"utf-8\"";
        webRequest.Accept = "text/xml";
        webRequest.Method = "POST";
        return webRequest;
    }

    public class OriginalData
    {
        public string Status
        {
            get;
            set;
        }

        public string Message
        {
            get;
            set;
        }
        public string Result
        {
            get;
            set;
        }
    }
    public class bussinessData
    {
        public string Amt
        {
            get;
            set;
        }

        public string MerchantOrderNo
        {
            get;
            set;
        }
        public string ItemDesc
        {
            get;
            set;
        }
    }
    public class datafortoken
    {
        public string placeID
        {
            get;
            set;
        }

        public string carLicense
        {
            get;
            set;
        }
        public string email
        {
            get;
            set;
        }
        public string token
        {
            get;
            set;
        }
    }
}