﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Security.Cryptography;
using System.Text;
public partial class ivcreate : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string RespondType = "JSON";
        string Version = "1.3";
        string TimeStamp = "1475115323";
        string MerchantOrderNo = "1475115323";
        string Status = "1";
        string Category = "B2C";
        string BuyerName = "jonathan";
        string BuyerEmail = "jonathan@YYY.com.tw";
        string PrintFlag = "N";
        string TaxType = "3";
        string TaxRate = "0";
        string Amt = "58";
        string TaxAmt = "0";
        string TotalAmt = "58";
        string ItemName = "car";
        string ItemCount = "1";
        string ItemUnit = "a";
        string ItemPrice = "58";
        string ItemAmt = "58";
        
        this.PostData_.Value = EncryptAES256("RespondType=" + RespondType + "&Version=" + Version + "&TimeStamp=" + TimeStamp + "&MerchantOrderNo=" + MerchantOrderNo + "&Status=" + Status + "&Category=" + Category + "&BuyerName=" + BuyerName + "&BuyerEmail=" + BuyerEmail + "&PrintFlag=" + PrintFlag + "&TaxType=" + TaxType + "&Amt=" + Amt + "&TaxRate=" + TaxRate + "&TaxAmt=" + TaxAmt + "&TotalAmt=" + TotalAmt + "&ItemName=" + ItemName + "&ItemCount=" + ItemCount + "&ItemUnit=" + ItemUnit + "&ItemPrice=" + ItemPrice + "&ItemAmt=" + ItemAmt);
    }
    public string EncryptAES256(string source)//加密
    {
        string sSecretKey = "ax9hhIc6W3oOCEn0GzxzMW7zwKKczmNJ";
        string iv = "UW1bFeiChGrTlOU1";
        byte[] sourceBytes = AddPKCS7Padding(Encoding.UTF8.GetBytes(source),
        32);
        var aes = new RijndaelManaged();
        aes.Key = Encoding.UTF8.GetBytes(sSecretKey);
        aes.IV = Encoding.UTF8.GetBytes(iv);
        aes.Mode = CipherMode.CBC;
        aes.Padding = PaddingMode.None;
        ICryptoTransform transform = aes.CreateEncryptor();
        return ByteArrayToHex(transform.TransformFinalBlock(sourceBytes, 0,
        sourceBytes.Length)).ToLower();
    }
    public string DecryptAES256(string encryptData)//解密
    {
        string sSecretKey = "ax9hhIc6W3oOCEn0GzxzMW7zwKKczmNJ";
        string iv = "UW1bFeiChGrTlOU1";
        var encryptBytes = HexStringToByteArray(encryptData.ToUpper());
        var aes = new RijndaelManaged();
        aes.Key = Encoding.UTF8.GetBytes(sSecretKey);
        aes.IV = Encoding.UTF8.GetBytes(iv);
        aes.Mode = CipherMode.CBC;
        aes.Padding = PaddingMode.None;
        ICryptoTransform transform = aes.CreateDecryptor();
        return Encoding.UTF8.GetString(RemovePKCS7Padding(transform.TransformFinalBlock(encryptBytes, 0, encryptBytes.Length)));
    }
    private static byte[] AddPKCS7Padding(byte[] data, int iBlockSize)
    {
        int iLength = data.Length;
        byte cPadding = (byte)(iBlockSize - (iLength % iBlockSize));
        var output = new byte[iLength + cPadding];
        Array.Copy(data, 0, output, 0, iLength);
        for (var i = iLength; i < output.Length; i++)
            output[i] = (byte)cPadding;
        return output;
    }
    private static byte[] RemovePKCS7Padding(byte[] data)
    {
        int iLength = data[data.Length - 1];
        var output = new byte[data.Length - iLength];
        Array.Copy(data, 0, output, 0, output.Length);
        return output;
    }
    private static string ByteArrayToHex(byte[] barray)
    {
        char[] c = new char[barray.Length * 2];
        byte b;
        for (int i = 0; i < barray.Length; ++i)
        {
            b = ((byte)(barray[i] >> 4));
            c[i * 2] = (char)(b > 9 ? b + 0x37 : b + 0x30);
            b = ((byte)(barray[i] & 0xF));
            c[i * 2 + 1] = (char)(b > 9 ? b + 0x37 : b + 0x30);
        }
        return new string(c);
    }
    private static byte[] HexStringToByteArray(string hexString)
    {
        int hexStringLength = hexString.Length;
        byte[] b = new byte[hexStringLength / 2];
        for (int i = 0; i < hexStringLength; i += 2)
        {
            int topChar = (hexString[i] > 0x40 ? hexString[i] - 0x37 : hexString[i] - 0x30)
            << 4;
            int bottomChar = hexString[i + 1] > 0x40 ? hexString[i + 1] - 0x37 :
            hexString[i + 1] - 0x30;
            b[i / 2] = Convert.ToByte(topChar + bottomChar);
        }
        return b;
    }
}