﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Net;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
public partial class PayNowGetIV : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //Response.Write(getIVNumber());
        string SNO = Request.Form["SNO"];
        string ivn = getIVNumber();
        string number;
        XmlDocument ivnumberxml = new XmlDocument();
        ivnumberxml.LoadXml(ivn);
        XmlNodeList ivnumberList = ivnumberxml.GetElementsByTagName("GetInvoiceNumberResponse");
        foreach (XmlNode parentNode in ivnumberList)
        {
            if (parentNode is XmlElement)
            {
                XmlElement element = (XmlElement)parentNode;
                XmlNodeList ConnectionsCurrent = element.GetElementsByTagName("GetInvoiceNumberResult");

                foreach (XmlNode childNode in ConnectionsCurrent)
                {
                    //Console.Write("<" + ((XmlElement)childNode).Name + "> ");
                    number = ((XmlElement)childNode).ChildNodes.Item(0).Value;
                    //Response.Write(((XmlElement)childNode).ChildNodes.Item(0).Value);
                    //Console.Write(((XmlElement)childNode).ChildNodes.Item(0).Value);
                    createIVB(number);
                    Response.Write(TEST(number));
                    //Response.Write("createIV: " + createIV(number));
                }
            }
        }
        //Response.Write(TripleDESEncode());
        
    }

    public string TEST(string ivnumber)
    {
        String endpoint = "http://testinvoice.paynow.com.tw/PayNowEInvoice.asmx?op=UploadInvoice";
        // 讀取 request_message
        XmlDocument soapEnvelopeXml = new XmlDocument();
        soapEnvelopeXml.LoadXml(@"<?xml version=""1.0"" encoding=""utf-8""?>
<soap:Envelope xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">
<soap:Body>
<UploadInvoice xmlns=""http://testinvoice.PayNow.com.tw/"">
      <mem_cid>70828783</mem_cid>
      <mem_pw>70828783</mem_pw>
      <InvNo>SA41448568</InvNo>
      <BuyerId></BuyerId>
      <BuyerName>消費者姓名</BuyerName>
      <BuyerAdd></BuyerAdd>
      <BuyerPhoneNo></BuyerPhoneNo>
      <BuyerEmail>jonathan_chiu@cablesoft.com.tw</BuyerEmail>
      <TotalAmount>200</TotalAmount>
      <OrderNo>test0010</OrderNo>
      <Send>0</Send>
      <CarrierType></CarrierType>
      <CarrierId1></CarrierId1>
      <CarrierId2></CarrierId2>
      <NPOBAN/>
    </UploadInvoice>
</soap:Body>
</soap:Envelope>");
        XmlNodeList nodeList = soapEnvelopeXml.GetElementsByTagName("UploadInvoice");
        foreach (XmlNode parentNode in nodeList)
        {
            if (parentNode is XmlElement)
            {
                XmlElement element = (XmlElement)parentNode;
                XmlNodeList ConnectionsCurrent = element.GetElementsByTagName("InvNo");

                foreach (XmlNode childNode in ConnectionsCurrent)
                {
                    //Console.Write("<" + ((XmlElement)childNode).Name + "> ");
                    ((XmlElement)childNode).ChildNodes.Item(0).Value = ivnumber;
                    //Response.Write(((XmlElement)childNode).ChildNodes.Item(0).Value);
                    //Console.Write(((XmlElement)childNode).ChildNodes.Item(0).Value);
                }
                XmlElement element2 = (XmlElement)parentNode;
                XmlNodeList ConnectionsCurrent2 = element2.GetElementsByTagName("mem_pw");
                foreach (XmlNode childNode in ConnectionsCurrent2)
                {
                    ((XmlElement)childNode).ChildNodes.Item(0).Value = TripleDESEncode();
                }
            }
        }
        string postData = soapEnvelopeXml.OuterXml;
        byte[] data = Encoding.UTF8.GetBytes(postData);

        // HTTP POST 請求物件
        HttpWebRequest httpWReq = (HttpWebRequest)WebRequest.Create(endpoint);
        httpWReq.Headers.Add(@"SOAP:Action");
        httpWReq.Method = "POST";
        httpWReq.ContentType = "text/xml;charset=\"utf-8\"";
        httpWReq.ContentLength = data.Length;
        using (Stream stream = httpWReq.GetRequestStream())
        {
            stream.Write(data, 0, data.Length);
        }
        HttpWebResponse response = (HttpWebResponse)httpWReq.GetResponse();
        string responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();
        return responseString;
        Console.Write(responseString);
    }

    public string TripleDESEncode()
    {
        string ReturnStr;
        string Password = "70828783";
        byte[] data = Encoding.UTF8.GetBytes(Password);
        TripleDES tdes = TripleDES.Create();
        tdes.IV = Encoding.UTF8.GetBytes("12345678");
        tdes.Key = Encoding.UTF8.GetBytes("1234567890" + Password + "123456");
        tdes.Mode = CipherMode.ECB;
        tdes.Padding = PaddingMode.Zeros;
        ICryptoTransform ict = tdes.CreateEncryptor();
        byte[] enc = ict.TransformFinalBlock(data, 0, data.Length);
        ReturnStr = Convert.ToBase64String(enc);

        return ReturnStr;
    }
    public string createIV(string ivnumber)
    {
        string result;
        HttpWebRequest request = CreateIVWebRequest();
        XmlDocument soapEnvelopeXml = new XmlDocument();
        soapEnvelopeXml.LoadXml(@"<?xml version=""1.0"" encoding=""utf-8""?>
<soap:Envelope xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">
<soap:Body>
<UploadInvoice xmlns=""http://testinvoice.PayNow.com.tw/"">
      <mem_cid>70828783</mem_cid>
      <mem_pw>70828783</mem_pw>
      <InvNo>SA41448568</InvNo>
      <BuyerId></BuyerId>
      <BuyerName>消費者姓名</BuyerName>
      <BuyerAdd></BuyerAdd>
      <BuyerPhoneNo></BuyerPhoneNo>
      <BuyerEmail>jonathan_chiu@cablesoft.com.tw</BuyerEmail>
      <TotalAmount>100</TotalAmount>
      <OrderNo>test0007</OrderNo>
      <Send>0</Send>
      <CarrierType></CarrierType>
      <CarrierId1></CarrierId1>
      <CarrierId2></CarrierId2>
      <NPOBAN/>
    </UploadInvoice>
</soap:Body>
</soap:Envelope>");//空字串帶2個  " <---
        XmlNodeList nodeList = soapEnvelopeXml.GetElementsByTagName("UploadInvoice");
        foreach (XmlNode parentNode in nodeList)
        {
            if (parentNode is XmlElement)
            {
                XmlElement element = (XmlElement)parentNode;
                XmlNodeList ConnectionsCurrent = element.GetElementsByTagName("InvNo");

                foreach (XmlNode childNode in ConnectionsCurrent)
                {
                    //Console.Write("<" + ((XmlElement)childNode).Name + "> ");
                    ((XmlElement)childNode).ChildNodes.Item(0).Value = ivnumber;
                    //Response.Write(((XmlElement)childNode).ChildNodes.Item(0).Value);
                    //Console.Write(((XmlElement)childNode).ChildNodes.Item(0).Value);
                }
                XmlElement element2 = (XmlElement)parentNode;
                XmlNodeList ConnectionsCurrent2 = element2.GetElementsByTagName("mem_pw");
                foreach (XmlNode childNode in ConnectionsCurrent2)
                {
                    ((XmlElement)childNode).ChildNodes.Item(0).Value = TripleDESEncode();
                }
            }
        }
        using (Stream stream = request.GetRequestStream())
        {
            soapEnvelopeXml.Save(stream);
        }
        using (WebResponse response = request.GetResponse())
        {
            using (StreamReader rd = new StreamReader(response.GetResponseStream()))
            {
                string soapResult = rd.ReadToEnd();
                result = soapResult;
                //Response.Write(soapResult);
                //Console.WriteLine(soapResult);
            }
        }
        return result;
    }
    public HttpWebRequest CreateIVWebRequest()
    {
        HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(@"http://testinvoice.paynow.com.tw/PayNowEInvoice.asmx?op=UploadInvoice");
        webRequest.Headers.Add(@"SOAP:Action");
        webRequest.ContentType = "text/xml;charset=\"utf-8\"";
        webRequest.Accept = "text/xml";
        webRequest.Method = "POST";
        return webRequest;
    }

    public string createIVB(string ivnumber)
    {
        string result;
        HttpWebRequest request = CreateIVWebRequest();
        XmlDocument soapEnvelopeXml = new XmlDocument();
        soapEnvelopeXml.LoadXml(@"<?xml version=""1.0"" encoding=""utf-8""?>
<soap:Envelope xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">
<soap:Body>
<UploadInvoice_Body xmlns=""http://testinvoice.PayNow.com.tw/"">
      <kMapingCode>ivnumber</kMapingCode>
      <Description>測試商品2</Description>
      <Amount>200</Amount>
      <Quantity>1</Quantity>
      <UnitPrice>200</UnitPrice>
      <mem_cid>70828783</mem_cid>
      <orderno>test0010</orderno>
      <Remark>此為測試資料</Remark>
    </UploadInvoice_Body>
</soap:Body>
</soap:Envelope>");
        XmlNodeList nodeList = soapEnvelopeXml.GetElementsByTagName("UploadInvoice_Body");
        foreach (XmlNode parentNode in nodeList)
        {
            if (parentNode is XmlElement)
            {
                XmlElement element = (XmlElement)parentNode;
                XmlNodeList ConnectionsCurrent = element.GetElementsByTagName("kMapingCode");

                foreach (XmlNode childNode in ConnectionsCurrent)
                {
                    //Console.Write("<" + ((XmlElement)childNode).Name + "> ");
                    ((XmlElement)childNode).ChildNodes.Item(0).Value = ivnumber;
                    //Response.Write(((XmlElement)childNode).ChildNodes.Item(0).Value);
                    //Console.Write(((XmlElement)childNode).ChildNodes.Item(0).Value);
                }
            }
        }
        using (Stream stream = request.GetRequestStream())
        {
            soapEnvelopeXml.Save(stream);
        }
        using (WebResponse response = request.GetResponse())
        {
            using (StreamReader rd = new StreamReader(response.GetResponseStream()))
            {
                string soapResult = rd.ReadToEnd();
                result = soapResult;
                //Response.Write(soapResult);
                //Console.WriteLine(soapResult);
            }
        }
        return result;
    }
    public HttpWebRequest CreateIVBWebRequest()
    {
        HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(@"http://testinvoice.paynow.com.tw/PayNowEInvoice.asmx?op=UploadInvoice_Body");
        webRequest.Headers.Add(@"SOAP:Action");
        webRequest.ContentType = "text/xml;charset=\"utf-8\"";
        webRequest.Accept = "text/xml";
        webRequest.Method = "POST";
        return webRequest;
    }

    public string getIVNumber()
    {
        string ivnumber;
        HttpWebRequest request = CreateWebRequest();
        XmlDocument soapEnvelopeXml = new XmlDocument();
        soapEnvelopeXml.LoadXml(@"<?xml version=""1.0"" encoding=""utf-8""?>
<soap:Envelope xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">
<soap:Body>
<GetInvoiceNumber xmlns=""http://testinvoice.PayNow.com.tw/"">
      <mem_cid>70828783</mem_cid>
      <Year>2017</Year>
      <period>0</period>
    </GetInvoiceNumber>
</soap:Body>
</soap:Envelope>");
        using (Stream stream = request.GetRequestStream())
        {
            soapEnvelopeXml.Save(stream);
        }
        using (WebResponse response = request.GetResponse())
        {
            using (StreamReader rd = new StreamReader(response.GetResponseStream()))
            {
                string soapResult = rd.ReadToEnd();
                ivnumber = soapResult;
                //Response.Write(soapResult);
                //Console.WriteLine(soapResult);
            }
        }
        return ivnumber;
    }
    public HttpWebRequest CreateWebRequest()
    {
        HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(@"http://testinvoice.paynow.com.tw/PayNowEInvoice.asmx?op=GetInvoiceNumber");
        webRequest.Headers.Add(@"SOAP:Action");
        webRequest.ContentType = "text/xml;charset=\"utf-8\"";
        webRequest.Accept = "text/xml";
        webRequest.Method = "POST";
        return webRequest;
    }
}