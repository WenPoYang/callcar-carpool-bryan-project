﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="page4.aspx.cs" Inherits="page4" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form method="post" action="https://ccore.spgateway.com/MPG/mpg_gateway">             <!-- 測試環境用的網址 正式的不一樣請注意 -->
  <input type="hidden" name="MerchantID" value="MS3446346">            <!-- 店家代號 -->
  <input type="hidden" name="RespondType" value="JSON"/>          <!-- 回傳資料的格式 -->
  <input type="hidden" name="CheckValue" value="B177E72C4A124F4F163BDF5A71D14D6E9DB994907584D1BEBF1AC9840875B1C3">     <!-- 驗證資料是否一致 -->
  <input type="hidden" name="TimeStamp" value="1480664364">               <!-- 訂單產生時間 -->
  <input type="hidden" name="Version" value="1.2">                <!-- api 版本 請看最新版說明文件 -->
  <input type="hidden" name="MerchantOrderNo" value="1480664364">   <!-- 店家產生的訂單編號 不可重覆 -->
  <input type="hidden" name="Amt" value="54">                    <!-- 商品價格 -->
  <input type="hidden" name="ItemDesc" value="玩具車">                           <!-- 商品名稱 -->
  <input type="hidden" name="Email" value="test@UUU.com">                   <!-- 付款成功會寄通知信到這個email -->
  <input type="hidden" name="LoginType" value="0">   
  <button type="submit">付款</button>
</form>
</body>
</html>
