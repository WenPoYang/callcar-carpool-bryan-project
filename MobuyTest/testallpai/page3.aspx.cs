﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Security.Cryptography;
using System.Text;
using System.Net;
using System.IO;
public partial class page3 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string strSandbox = "https://ccore.spgateway.com/API/CreditCard";
        HttpWebRequest req = (HttpWebRequest)WebRequest.Create(strSandbox);
        req.Method = "GET";
        req.Accept = "application/json";
        HttpWebResponse response = (HttpWebResponse)req.GetResponse();
        StreamReader reader = new StreamReader(response.GetResponseStream());
        StringBuilder output = new StringBuilder();
        output.Append(reader.ReadToEnd());
        StreamWriter swr = new StreamWriter(Server.MapPath("Textfile.txt"));
        swr.WriteLine("---- not verified(" + DateTime.Now.ToString() + ")--" + reader.ReadToEnd().ToString());
        swr.Dispose();
        response.Close();

    }
}