function showtitlebox() {

    addTitlebar();
    focusTitlebars(true);
  
}


function focusTitlebars(focus) {
  var bg_color = focus ? "#3a3d3d" : "#7a7c7c";
    
  var titlebar = document.getElementById("top-titlebar");
 
  if (titlebar)
    titlebar.style.backgroundColor = bg_color;
}

function createImage(image_id, image_url) {
  var image = document.createElement("img");
  image.setAttribute("id", image_id);
  image.src = image_url;
  return image;
}

function updateImageUrl(image_id, new_image_url) {
  var image = document.getElementById(image_id);
  if (image)
    image.src = new_image_url;
}

function createButton(button_id, button_name, normal_image_url,
                       hover_image_url, click_func) {
  var button = document.createElement("div");
  button.setAttribute("class", button_name);
  var button_img = createImage(button_id, normal_image_url);
  button.appendChild(button_img);
  button.onmouseover = function() {
    updateImageUrl(button_id, hover_image_url);
  }
  button.onmouseout = function() {
    updateImageUrl(button_id, normal_image_url);
  }
  button.onclick = click_func;
  return button;
}

function closeWindow() {
  window.close();
}

function miniWindow() {
  chrome.app.window.current().minimize();
}

function addTitlebar() {
  var titlebar_name = "top-titlebar";
  var titlebar_icon_url = "top-titlebar.png";
  var titlebar_text = "  ";
  var titlebar = document.createElement("div");
  titlebar.setAttribute("id", titlebar_name);
  titlebar.setAttribute("class", titlebar_name);

  var icon = document.createElement("div");
  icon.setAttribute("class", titlebar_name + "-icon");
  icon.appendChild(createImage( titlebar_name + "icon", titlebar_icon_url));
  titlebar.appendChild(icon);

  var title = document.createElement("div");
  title.setAttribute("class", titlebar_name + "-text");
  title.innerText = titlebar_text;
  titlebar.appendChild(title);

  var miniButton = createButton(titlebar_name + "-mini-button",
                                 titlebar_name + "-mini-button",
                                 "images\button_mini.png",
                                 "images\button_mini_hover.png",
                                 miniWindow);
  titlebar.appendChild(miniButton);  

  var divider = document.createElement("div");
  divider.setAttribute("class", titlebar_name + "-divider");
  titlebar.appendChild(divider);

  var closeButton = createButton(titlebar_name + "-close-button",
                                 titlebar_name + "-close-button",
                                 "images\button_close.png",
                                 "images\button_close_hover.png",
                                 closeWindow);
  titlebar.appendChild(closeButton);


  
  document.body.appendChild(titlebar);
}