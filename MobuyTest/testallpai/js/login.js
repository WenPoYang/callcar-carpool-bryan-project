var userj="";
var eid="";
var zid="";
var userId="";
var userPw="";
var name="";

function login(){ 
  userId = document.getElementById("username").value;
  userPw = document.getElementById("password").value;
  $(document).ready(function ()
  {           
      var url = 'http://zoomin.ws/API_REST/signin2?id='+userId+'&pw='+userPw+'&i={I}';
      $.getJSON(url, function (result)
      {
        $.each(result, function (i, field)
        {
          var json=null;
          json = $.parseJSON(result.signin2GResult);
          if(document.getElementById("username").value=="" || document.getElementById("password").value=="")
          {
            $("#hint").empty();            
            $('input[type="text"],input[type="password"]').css("border","2px solid red");
            $('input[type="text"],input[type="password"]').css("box-shadow","0 0 3px red");
            $("#hint").append("*請輸入正確的帳號或密碼");      
            console.log(userId);      
          }
          else if(json.indexOf("Error") == 0)
          {
            $("#hint").empty();            
            $('input[type="text"],input[type="password"]').css("border","2px solid red");
            $('input[type="text"],input[type="password"]').css("box-shadow","0 0 3px red");
            $("#hint").append("*請輸入正確的帳號或密碼");
            console.log(userId+"  f");
          }
          else
          { 
            $("#hint").empty();
            name=json[0].euNickname;
            eid=json[0].EneUserID;
            zid=json[0].ZOOMID;
            userj=json[0].PowerJson;
            check();
            sendValue(name,userj,eid,zid);
          }
        });
      });
    });
}
function sendValue(name,userj,eid,zid)
{
   chrome.app.window.create('bbb.html', 
     {
       id: 'bbb',
       width: 440,
       height: 560,
       frame: 'none'
     },
    function (createdWindow) {
      var win = createdWindow.contentWindow;
      win.onlo = function () {
        win.register();
        win.getvalues(name,userj,eid,zid);
      }
    }
  );
}

function showtitlebox() {

    addTitlebar();
    addlogo();
    focusTitlebars(true);
  
}


function focusTitlebars(focus) {
  var bg_color = focus ? "#3a3d3d" : "#7a7c7c";
    
  var titlebar = document.getElementById("top-titlebar");
 
  if (titlebar)
    titlebar.style.backgroundColor = bg_color;
}

function createImage(image_id, image_url) {
  var image = document.createElement("img");
  image.setAttribute("id", image_id);
  image.src = image_url;
  return image;
}

function updateImageUrl(image_id, new_image_url) {
  var image = document.getElementById(image_id);
  if (image)
    image.src = new_image_url;
}

function createButton(button_id, button_name, normal_image_url,
                       hover_image_url, click_func) {
  var button = document.createElement("div");
  button.setAttribute("class", button_name);
  var button_img = createImage(button_id, normal_image_url);
  button.appendChild(button_img);
  button.onmouseover = function() {
    updateImageUrl(button_id, hover_image_url);
  }
  button.onmouseout = function() {
    updateImageUrl(button_id, normal_image_url);
  }
  button.onclick = click_func;
  return button;
}

function closeWindow() {
  check();
  window.close();
}

function miniWindow() {
  chrome.app.window.current().minimize();
}

function addTitlebar() {
  var titlebar_name = "top-titlebar";
  var titlebar_icon_url = "images\\top-titlebar.png";
  var titlebar_text = "v1.2";
  var titlebar = document.createElement("div");
  titlebar.setAttribute("id", titlebar_name);
  titlebar.setAttribute("class", titlebar_name);

  var icon = document.createElement("div");
  icon.setAttribute("class", titlebar_name + "-icon");
  icon.appendChild(createImage( titlebar_name + "icon", titlebar_icon_url));
  titlebar.appendChild(icon);

  var title = document.createElement("div");
  title.setAttribute("class", titlebar_name + "-text");
  title.innerText = titlebar_text;
  titlebar.appendChild(title);

  var miniButton = createButton(titlebar_name + "-mini-button",
                                 titlebar_name + "-mini-button",
                                 "images\\button_mini.png",
                                 "images\\button_mini_hover.png",
                                 miniWindow);
  titlebar.appendChild(miniButton);  

  var divider = document.createElement("div");
  divider.setAttribute("class", titlebar_name + "-divider");
  titlebar.appendChild(divider);

  var closeButton = createButton(titlebar_name + "-close-button",
                                 titlebar_name + "-close-button",
                                 "images\\button_close.png",
                                 "images\\button_close_hover.png",
                                 closeWindow);
  titlebar.appendChild(closeButton);


  
  document.body.appendChild(titlebar);
}

function addlogo()
{
  var logo_name = "top-logo";
  var logo_icon_url = "images\\logo.png";
  var logo = document.createElement("div");
  logo.setAttribute("id", logo_name);
  logo.setAttribute("class", logo_name);

  var icon = document.createElement("div");
  icon.setAttribute("class", logo_name );
  icon.appendChild(createImage( logo_name , logo_icon_url));
  logo.appendChild(icon);

  document.body.appendChild(logo);
}

function getdata()
{
  
  chrome.storage.local.get("check", function (result) {
    document.getElementById("myCheck").checked = result.check;
    chrome.storage.local.set({"check": result.check});        
  });  
   
  chrome.storage.local.get("userId", function (result) {
    document.getElementById("username").value = result.userId;
    chrome.storage.local.set({"userId": result.userId});     
  });
  chrome.storage.local.get("userPw", function (result) {
    document.getElementById("password").value = result.userPw;
    chrome.storage.local.set({"userPw": result.userPw});   
  });      
  check(document.getElementById("username").value,document.getElementById("password").value);
  
}

function check(){
  if(document.getElementById("myCheck").checked==true)
  { 
    console.log("YY");
    chrome.storage.local.set({"userId": document.getElementById("username").value});
    chrome.storage.local.set({"userPw": document.getElementById("password").value});
    chrome.storage.local.set({"bool": true});
  }
  else{console.log("NN");
    chrome.storage.local.clear();
    chrome.storage.local.set({"userId": ""});
    chrome.storage.local.set({"userPw": ""});
    chrome.storage.local.set({"bool": false});
  }
}

function getvalue()
{
    chrome.storage.local.get("bool", function (result) {
      if(result.bool == true)
      {
        document.getElementById("myCheck").checked = result.bool;
        chrome.storage.local.get("userId", function (result) {
        document.getElementById("username").value = result.userId;
        });
        chrome.storage.local.get("userPw", function (result) {
        document.getElementById("password").value = result.userPw;  
        });   
      }     
    }); 
}

window.onload = function() {
  showtitlebox();
  getvalue();

  $('#myCheck').click(function(){
     check();
  });     
  document.getElementById("loginbtn").onclick = login;
        
}