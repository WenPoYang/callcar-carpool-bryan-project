﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLog;
using System.Net;
using System.Web;
using System.Configuration;
using System.Collections.Specialized;
using System.IO;
using DbConn;
using Dapper;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using CallCar;
using CallCar.Account;
using CallCar.Reservation;
using System.Windows.Forms;
using CallCar.Data;

namespace CallcarSMS
{
    class Program
    {
        private static string ConnectionString;
        private static string ResponseURL;
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private static string MessageTemplate;

        static void Main(string[] args)
        {
            NameValueCollection nc = ConfigurationManager.AppSettings;

            if (nc["Server"] == "Production")
            {
                ResponseURL = @"https://api2.ishareco.com/BAPI/REST/SMSReport";
                ConnectionString = "Server=10.0.2.23;Port=3306;Database=carpoollab;Uid=ishareco;Pwd=fu0!j0$su0^vup#;Allow User Variables=True";
            }
            else if (nc["Server"] == "Lab")
            {
                ResponseURL = @"http://apid.ishareco.com/BAPI/SMSReport";
                ConnectionString = "Server=61.63.55.200;Port=3306;Database=carpoollab;Uid=apiuser;Pwd=7oPOh88I;Allow User Variables=True";
            }

            logger.Info("=====" + DateTime.Now.ToString("yyyy-MM-dd HH:mm") + "執行" + nc["Server"] + "===== ");
            //舊   [CallCar共乘]貴賓您好，2018-04-15於機場TPE上車時間15:45，司機：魏先生RAJ6523，謝謝您! 
            //新   CallCar-某某某貴賓，2018-04-15上車指定時間15:45，司機RAJ6523魏先生0911123123謝謝您！
            MessageTemplate = "{0}-CallCar共乘貴賓，{1}上車指定時間{2}，司機{3}{4}{5}，謝謝您!艾雪敬上"; ; //{0}異動 {1}日期  {2}時間  {3}車號 {4}司機 {5}手機

            #region 簡訊發送
            try
            {
                List<ReservationSMS> List = GetList();

                if (List.Count == 0)
                {
                    logger.Info("=====執行完成：無資料===== ");
                }
                else
                {
                    BALAccount bala = new BALAccount(ConnectionString);

                    string TakeDate, TakeTime, DriverName, Msg, DriverPhone;
                    MiTakeSMS MiTak = new MiTakeSMS();

                    foreach (ReservationSMS sms in List)
                    {

                        if (sms.CarNo == "" || sms.DriverID == 0 || sms.MobilePhone == "")
                        {
                            RemoveSms(sms.SeqNo);
                            logger.Error(sms.ReservationNo + "無司機或車號或電話 已移除");
                            continue;
                        }

                        TimeSpan ts = new TimeSpan(sms.ServeTime.Ticks - DateTime.Now.Ticks);
                        if (ts.TotalHours > 20)
                            continue;

                        string ModifyTerm = string.Empty;
                        int SmsCount = GetCount(sms.ReservationNo);
                        if (SmsCount > 0)
                            ModifyTerm = string.Format("異動{0}", SmsCount.ToString());
                        else
                            ModifyTerm = string.Empty;

                        TakeDate = sms.ServeTime.ToString("yyyy-MM-dd");
                        //if (sms.ServiceType == "I")
                        //    ServiceType = "於";
                        //else
                        //    ServiceType = "往";

                        TakeTime = sms.ServeTime.ToString("HH:mm");
                        //DriverName = bale.GetEmployee(sms.DriverID).Name.Substring(0, 1) + "先生";
                        OEmployee oe = bala.GetEmployeeInfo(sms.DriverID);
                        DriverName = oe.Name.Substring(0, 1) + "先生";
                        DriverPhone = oe.MobilePhone;

                        Msg = string.Format(MessageTemplate, ModifyTerm, TakeDate, TakeTime, sms.CarNo, DriverName, DriverPhone);

                        MiTakeSMS.SMSBodyStruct smsBody = new MiTakeSMS.SMSBodyStruct();
                        smsBody.Body = Msg;
                        smsBody.Name = "";
                        smsBody.Number = sms.MobilePhone;
                        smsBody.Reference = sms.ReservationNo;

                        MiTak.SMSBody.Add(smsBody);
                    }

                    if (MiTak.SMSBody.Count > 0)
                        MiTak.SendSingleSMS();
                }
            }
            catch (Exception ex)
            {
                logger.Error("簡訊發生錯誤：" + ex.Message + Environment.NewLine + ex.StackTrace);
            }
            finally
            {
                logger.Info(DateTime.Now.ToString("yyyy-MM-dd HH:mm") + "=====簡訊執行完成===== ");
            }

            #endregion

            #region 信件發送
            try
            {

                List<ReservationEmail> EmailList = GetEmailList();

                foreach (ReservationEmail email in EmailList)
                {
                    OReservation or;
                    OAccount oa;
                    BALReservation balr = new BALReservation(ConnectionString);
                    BALAccount balu = new BALAccount(ConnectionString);
                    try
                    {
                        or = balr.GetReservation(email.ReservationNo);
                        oa = balu.GetUserAccount(or.UserID);
                    }
                    catch (Exception ex)
                    {
                        logger.Error("Email發生錯誤：" + JsonConvert.SerializeObject(email) + Environment.NewLine + ex.Message + Environment.NewLine + ex.StackTrace);
                        continue;
                    }
                    try
                    {
                        SendReservationEmail(email.SeqNo, email.EmailType, or.ServeDate, or.PreferTime, or.ServiceType, or.PickupAddress, or.TakeoffAddress, or.ChooseType, or.FlightNo, or.PassengerCnt, or.BaggageCnt, or.PassengerName, or.PassengerPhone, email.ReservationNo, oa.Email);
                    }
                    catch (Exception ex)
                    {
                        logger.Error("Email發生錯誤：" + JsonConvert.SerializeObject(email) + Environment.NewLine + ex.Message + Environment.NewLine + ex.StackTrace);
                        continue;
                    }
                    System.Threading.Thread.Sleep(1000);

                }
            }
            catch (Exception ex)
            {
                logger.Error("Email發生錯誤：" + ex.Message + Environment.NewLine + ex.StackTrace);
            }
            finally
            {
                logger.Info(DateTime.Now.ToString("yyyy-MM-dd HH:mm") + "=====Email執行完成===== ");
                Environment.Exit(0);
            }
            #endregion
        }

        private static List<ReservationSMS> GetList()
        {
            using (var conn = new MySqlConnection(ConnectionString))
            {
                conn.Open();
                string sql = "SELECT SeqNo, ReservationNo, ServiceType, SelectionType, FlightTime, PassengerName, MobilePhone,CarNo, DriverID, ServeTime    FROM reservation_sms Where ServeTime >= Now() Order by CreateTime ASC ";
                var ReservationList = conn.Query<ReservationSMS>(sql);
                return ReservationList.ToList();
            }
        }

        private static List<ReservationEmail> GetEmailList()
        {
            using (var conn = new MySqlConnection(ConnectionString))
            {
                conn.Open();
                string sql = "SELECT SeqNo, EmailType, ReservationNo, SendFlag, SendTime	FROM reservation_email where SendFlag = '0' order by createtime asc ";
                var ReservationList = conn.Query<ReservationEmail>(sql);
                return ReservationList.ToList();
            }
        }
        private static int GetCount(string Rno)
        {
            using (var conn = new MySqlConnection(ConnectionString))
            {
                conn.Open();
                var count = conn.ExecuteScalar("select count(*) from dispatch_sms_report where ReservationNo = @no", new { no = Rno });
                return Convert.ToInt32(count.ToString());
            }
        }

        private static void RemoveSms(int SeqNo)
        {
            using (var conn = new MySqlConnection(ConnectionString))
            {
                conn.Open();
                conn.Execute("Delete From reservation_sms Where SeqNo = @no", new { no = SeqNo });
            }
        }
        public class ReservationSMS
        {
            public int SeqNo { get; set; }
            public string ReservationNo { get; set; }
            public string ServiceType { get; set; }
            public string SelectionType { get; set; }
            public DateTime FlightTime { get; set; }
            public string PassengerName { get; set; }
            public string MobilePhone { get; set; }
            public int DriverID { get; set; }
            public string CarNo { get; set; }
            public DateTime ServeTime { get; set; }
        }

        public class ReservationEmail
        {
            public int SeqNo { get; set; }
            public string EmailType { get; set; }
            public string ReservationNo { get; set; }
            public string SendFlag { get; set; }
            public DateTime SendTime { get; set; }
        }

        public class MiTakeSMS
        {
            public MiTakeSMS()
            {
                this.UserName = "52439256";
                this.Password = "ishareco.com";
                this.SMSBody = new List<SMSBodyStruct>();
            }

            public void SendSingleSMS()
            {
                if (this.UserName == "" || this.Password == "" || this.SMSBody.Count == 0)
                    return;

                string URL = @"http://smexpress.mitake.com.tw:9600/SmSendGet.asp?username={0}&password={1}&dstaddr={2}&encoding=UTF8&smbody={3}&response={4}";

                foreach (SMSBodyStruct body in SMSBody)
                {
                    string RequestURL = string.Format(URL, this.UserName, this.Password, body.Number, HttpUtility.UrlEncode(body.Body), ResponseURL);
                    HttpWebRequest req = (HttpWebRequest)HttpWebRequest.Create(RequestURL);
                    req.Method = "GET";
                    string msgid = "", statuscode = "";

                    using (WebResponse wr = req.GetResponse())
                    {
                        using (var responseStream = wr.GetResponseStream())
                        {
                            using (var responseReader = new StreamReader(responseStream))
                            {
                                string str = responseReader.ReadLine();

                                while (str != null)
                                {
                                    if (str.IndexOf("msgid") >= 0)
                                    {
                                        msgid = str.Split('=')[1];
                                    }
                                    else if (str.IndexOf("statuscode") >= 0)
                                    {
                                        statuscode = str.Split('=')[1];
                                    }
                                    str = responseReader.ReadLine();
                                }
                            }
                        }
                    }

                    if (msgid != "")
                    {
                        StringBuilder sb = new StringBuilder();
                        List<MySqlParameter> parms = new List<MySql.Data.MySqlClient.MySqlParameter>();
                        sb.Append("INSERT INTO dispatch_sms_report (msgid, ReservationNo,dstaddr, statuscode,statusstr,msg) VALUES (@msgid, @ReservationNo, @dstaddr, @statuscode,@statusstr,@msg)");
                        parms.Add(new MySqlParameter("@msgid", msgid));
                        parms.Add(new MySqlParameter("@ReservationNo", body.Reference));
                        parms.Add(new MySqlParameter("@dstaddr", body.Number));
                        parms.Add(new MySqlParameter("@statuscode", statuscode));
                        parms.Add(new MySqlParameter("@statusstr", ""));
                        parms.Add(new MySqlParameter("@msg", body.Body));
                        try
                        {
                            using (dbConnectionMySQL conn = new dbConnectionMySQL(ConnectionString))
                            {
                                conn.executeInsertQuery(sb.ToString(), parms.ToArray());

                                sb.Length = 0;
                                parms.Clear();
                                sb.Append("Delete From reservation_sms Where ReservationNo = @rno");
                                parms.Add(new MySqlParameter("@rno", body.Reference));

                                conn.executeDeleteQuery(sb.ToString(), parms.ToArray());
                            }
                        }
                        catch (MySql.Data.MySqlClient.MySqlException)
                        {

                        }
                        logger.Info(body.Reference + "：新增至簡訊紀錄");
                    }

                    System.Threading.Thread.Sleep(1000);
                }
            }

            public string UserName { get; set; }
            public string Password { get; set; }
            public List<SMSBodyStruct> SMSBody { get; set; }

            public class SMSBodyStruct
            {
                public string Name { get; set; }
                public string Number { get; set; }
                public string Body { get; set; }
                public string Reference { get; set; }

            }

        }

        private static void SendReservationEmail(int SeqNo, string EmailType, string TakeDate, string PrefreTime, string ServiceType, string PickupAddress, string TakeoffAddress, string ChooseType, string FlightNo, int Pcnt, int Bcnt, string PName, string PMobile, string ReservationNo, string Email)
        {
            if (Email == "www@www")
                return;
            string HtmlBody = "";
            string filepath = "";
            string filename = "";

            if (EmailType == "NEW")
                filename = "callcar_email_zhtw.html";
            else if (EmailType == "CANCEL")
                filename = "callcar_cancel_email_zhtw.html";
            else
                return;

            filepath = System.IO.Path.GetDirectoryName(Application.ExecutablePath) + @"\template\" + filename;
            try
            {
                if (File.Exists(filepath))
                {
                    //讀取檔案
                    using (StreamReader streamReader = new StreamReader(filepath, Encoding.GetEncoding("utf-8")))
                    {
                        //轉字串給輸出
                        HtmlBody = streamReader.ReadToEnd();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            // {0} 訂單編號
            // {1} 乘車日期
            // {2} 預約乘車時間
            // {3} 上下車
            // {4} 服務地址
            // {5} 出回國
            // {6} 航班編號
            // {7} 機場資訊
            // {8} 乘車人數
            // {9} 行李數
            // {10} 連絡人姓名
            // {11} 聯絡人手機
            try
            {
                HtmlBody = HtmlBody.Replace("{0}", ReservationNo);
                HtmlBody = HtmlBody.Replace("{1}", TakeDate.Substring(0, 4) + "-" + TakeDate.Substring(4, 2) + "-" + TakeDate.Substring(6, 2));
                HtmlBody = HtmlBody.Replace("{2}", PrefreTime.Substring(0, 2) + "：" + PrefreTime.Substring(2, 2));
                HtmlBody = HtmlBody.Replace("{3}", (ServiceType == "O") ? "上車" : "下車");
                HtmlBody = HtmlBody.Replace("{4}", (ServiceType == "O") ? PickupAddress : TakeoffAddress);
                HtmlBody = HtmlBody.Replace("{5}", (ServiceType == "O") ? "送機" : "接機");
                HtmlBody = HtmlBody.Replace("{6}", (ChooseType == "F") ? FlightNo : "");
                HtmlBody = HtmlBody.Replace("{7}", (ServiceType == "O") ? TakeoffAddress : PickupAddress);
                HtmlBody = HtmlBody.Replace("{8}", Pcnt.ToString());
                HtmlBody = HtmlBody.Replace("{9}", Bcnt.ToString());
                HtmlBody = HtmlBody.Replace("{10}", PName);
                HtmlBody = HtmlBody.Replace("{11}", PMobile);

                CallCar.Tool.GmailSMTP smtp = new CallCar.Tool.GmailSMTP("info@ishareco.com", "share52439256i");

                string Subject;

                if (EmailType == "NEW")
                    Subject = "CallCar機場共乘 訂單確認信";
                else
                    Subject = "CallCar機場共乘 取消訂單確認信";
                List<string> MailAddress = new List<string>();
                MailAddress.Add(Email);


                if (smtp.Send(MailAddress, "艾雪科技客服通知", Subject, HtmlBody, null) == "Success")
                    SendComplete(SeqNo);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private static void SendComplete(int SeqNo)
        {
            using (var conn = new MySqlConnection(ConnectionString))
            {
                conn.Open();
                conn.Execute("Update reservation_email Set SendFlag = '1' , SendTime = Now()  Where  SeqNo = @no", new { no = SeqNo });
            }
        }

    }
}
