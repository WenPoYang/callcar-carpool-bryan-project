﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.IO;
using System.Web;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnSend_Click(object sender, EventArgs e)
        {
            string TakeDate, TakeTime, Msg, ModifyTerm;
            MiTakeSMS MiTak = new MiTakeSMS();
            string MessageTemplate = "{0}-CallCar共乘貴賓，{1}上車指定時間{2}，司機{3}{4}{5}，謝謝您!艾雪敬上"; //{0}異動 {1}日期  {2}時間  {3}車號 {4}司機 {5}手機
            ModifyTerm = "異動1";
            TakeDate = "2018-05-01";
            TakeTime = "11:30";

            Msg = string.Format(MessageTemplate, ModifyTerm, TakeDate, TakeTime, "RAJ6523", "魏先生", "0911123123");



            MiTakeSMS.SMSBodyStruct smsBody = new MiTakeSMS.SMSBodyStruct();
            smsBody.Body = Msg;
            smsBody.Name = "";
            smsBody.Number = txtPhone.Text.Trim();
            smsBody.Reference = "Test";

            MiTak.SMSBody.Add(smsBody);

            if (MiTak.SMSBody.Count > 0)
                MiTak.SendSingleSMS();
        }
    }

    public class MiTakeSMS
    {
        public MiTakeSMS()
        {
            this.UserName = "52439256";
            this.Password = "ishareco.com";
            this.SMSBody = new List<SMSBodyStruct>();
        }

        public void SendSingleSMS()
        {
            if (this.UserName == "" || this.Password == "" || this.SMSBody.Count == 0)
                return;

            string URL = @"http://smexpress.mitake.com.tw:9600/SmSendGet.asp?username={0}&password={1}&dstaddr={2}&encoding=UTF8&smbody={3}&response={4}";

            foreach (SMSBodyStruct body in SMSBody)
            {
                string RequestURL = string.Format(URL, this.UserName, this.Password, body.Number, HttpUtility.UrlEncode(body.Body), "");
                HttpWebRequest req = (HttpWebRequest)HttpWebRequest.Create(RequestURL);
                req.Method = "GET";
                string msgid = "", statuscode = "";

                using (WebResponse wr = req.GetResponse())
                {
                    using (var responseStream = wr.GetResponseStream())
                    {
                        using (var responseReader = new StreamReader(responseStream))
                        {
                            string str = responseReader.ReadLine();

                            while (str != null)
                            {
                                if (str.IndexOf("msgid") >= 0)
                                {
                                    msgid = str.Split('=')[1];
                                }
                                else if (str.IndexOf("statuscode") >= 0)
                                {
                                    statuscode = str.Split('=')[1];
                                }
                                str = responseReader.ReadLine();
                            }
                        }
                    }
                }
            }
        }

        public string UserName { get; set; }
        public string Password { get; set; }
        public List<SMSBodyStruct> SMSBody { get; set; }

        public class SMSBodyStruct
        {
            public string Name { get; set; }
            public string Number { get; set; }
            public string Body { get; set; }
            public string Reference { get; set; }

        }

    }
}
