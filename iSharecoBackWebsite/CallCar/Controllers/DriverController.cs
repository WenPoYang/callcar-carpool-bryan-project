﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using CallCar.Models;
using CallCar.BAL;
using System.Configuration;
using System.Data;
using System.IO;
using System.Net;
using System.Net.Mime;
using System.Text;
using System.Security.Cryptography;

namespace CallCar.Controllers
{
    public class DriverController : _AdminController
    {
        // GET: Driver
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Add()
        {
            List<SelectListItem> mySelectItemList = new List<SelectListItem>();

                mySelectItemList.Add(new SelectListItem()
                {
                    Text = "True",
                    Value = "1",
                    Selected = true
                });
                mySelectItemList.Add(new SelectListItem()
                {
                    Text = "False",
                    Value = "0",
                    Selected = false
                });
            ViewBag.Active = mySelectItemList;
            List<SelectListItem> mySelectItemList2 = new List<SelectListItem>();

                mySelectItemList2.Add(new SelectListItem()
                { Text = "全天", Value = "0", Selected = true });
                mySelectItemList2.Add(new SelectListItem()
                { Text = "早班", Value = "1", Selected = false });
                mySelectItemList2.Add(new SelectListItem()
                { Text = "晚班", Value = "2", Selected = false });
                mySelectItemList2.Add(new SelectListItem()
                { Text = "其他", Value = " ", Selected = false });
           
            ViewBag.ShiftType = mySelectItemList2;



            return View();

        }
        public static string ConnectionString = ConfigurationManager.ConnectionStrings["CarpoolLab"].ConnectionString;
        [HttpPost]
        public JsonResult GetList(int page ,string searchJson = null,  string orderField = "", string orderType = "", int itemPerPage = 10)
        {

            if (itemPerPage > 100)
            {
                itemPerPage = 100;
            }

            Dictionary<string, dynamic> JsonResult = new Dictionary<string, dynamic>();

            try
            {
                string CarTeam = "";
                string cellph = "";
                string Name = "";
                string ShiftType = "";
                if (searchJson != null)
                {
                    try
                    {

                        dynamic search = JObject.Parse(searchJson);

                        CarTeam = search.CarTeam;
                        cellph = search.cellph;
                        Name = search.Name;
                        ShiftType = search.ShiftType;
                    }
                    catch (Exception e)
                    {

                    }

                }

                DAL.DALGeneral dalr = new DAL.DALGeneral(ConnectionString);
                BALGeneral balg = new BALGeneral(ConnectionString);
                int DriverID = 0;
                List<OEmployee> List;
                if (Name != "")
                {
                    DriverID = dalr.GetDriverIDByName(Name);
                    if (CarTeam != "")
                        List = dalr.GetDriversByAll(dalr.GetFleet(CarTeam).FleetID.ToString(), DriverID.ToString(), cellph, ShiftType);
                    else
                        List = dalr.GetDriversByAll(CarTeam, DriverID.ToString(), cellph, ShiftType);
                }
                else
                {
                    if (CarTeam != "")
                        List = dalr.GetDriversByAll(dalr.GetFleet(CarTeam).FleetID.ToString(), DriverID.ToString(), cellph, ShiftType);
                    else
                        List = dalr.GetDriversByAll(CarTeam, DriverID.ToString(), cellph, ShiftType);
                }
                int countSkip = itemPerPage * (page - 1);
                int countTake = itemPerPage;

                int TotalItem = 0;

                List<dynamic> data = new List<dynamic>();

                if (List != null)
                {

                    foreach (OEmployee or in List)
                    {
                        TotalItem++;

                        Dictionary<string, dynamic> a = new Dictionary<string, dynamic>();

                        a.Add("UserName", or.Name.ToString()); //: "CR01E9541", 訂單編號
                        a.Add("MobilePhone", or.MobilePhone.ToString()); //: 100,  會員編號
                        a.Add("FleetID", dalr.GetFleet(or.FleetID).FleetName.ToString()); //: "O",  出國O,回國 I
                        a.Add("ShiftType", or.ShiftType.ToString()); //: "20170207", 乘車日期
                        a.Add("UserID", or.ID.ToString()); //: "20170207", 乘車日期

                        data.Add(a);

                    }

                }

                JsonResult.Add("data", data);

                double PageTotal = (double)TotalItem / itemPerPage;
                PageTotal = Math.Ceiling(PageTotal);



                JsonResult.Add("totalItem", TotalItem);
                JsonResult.Add("pageTotal", PageTotal);
                JsonResult.Add("nowpage", page);
            }
            catch (Exception)
            {

            }


            return Json(JsonResult);

        }

        public ActionResult Item(string id)
        {
            DAL.DALGeneral dalr = new DAL.DALGeneral(ConnectionString);

            List<OEmployee> List = dalr.GetDriverDataByID(id);


            Dictionary<string, dynamic> data = new Dictionary<string, dynamic>();

            foreach (OEmployee or in List)
            {
                data.Add("ID", or.ID.ToString()); //: "CR01E9541", 訂單編號
                data.Add("Name", or.Name.ToString()); //: 100,  會員編號
                data.Add("Password", or.Password.ToString()); //: "O",  出國O,回國 I
                data.Add("CitizenID", or.CitizenID.ToString()); //: "20170207", 乘車日期
                data.Add("MobilePhone", or.MobilePhone.ToString()); //: "O",  出國O,回國 I
                data.Add("HomePhone", or.HomePhone.ToString());
                data.Add("Role", or.Role.ToString()); //: "O",  出國O,回國 I
                data.Add("CompanyNo", or.CompanyNo.ToString()); //: "20170207", 乘車日期
                data.Add("FleetID", dalr.GetFleet(or.FleetID).FleetName.ToString()); //: "O",  出國O,回國 I
                data.Add("ShiftType", or.ShiftType.ToString()); //: "20170207", 乘車日期
                data.Add("City", or.City.ToString()); //: "20170207", 乘車日期
                data.Add("Distinct", or.Distinct.ToString()); //: "20170207", 乘車日期
                data.Add("Address", or.Address.ToString()); //: "20170207", 乘車日期
                data.Add("Active", or.Active.ToString()); //: "20170207", 乘車日期
                data.Add("CreateTime", or.CreateTime.ToString("yyyy/MM/dd HH:mm").ToString()); //: "2000", 預約時段
                data.Add("UpdTime", or.UpdTime.ToString("yyyy/MM/dd HH:mm").ToString()); //: "新北市",  上車城市

                string dataText = "";

                dataText = Helper.jsonEncode(data);

                List<SelectListItem> mySelectItemList = new List<SelectListItem>();
                if (or.Active.ToString() == "1")
                {
                    mySelectItemList.Add(new SelectListItem()
                    {
                        Text = "True",
                        Value = "1",
                        Selected = true
                    });
                    mySelectItemList.Add(new SelectListItem()
                    {
                        Text = "False",
                        Value = "0",
                        Selected = false
                    });
                }
                else
                {
                    mySelectItemList.Add(new SelectListItem()
                    {
                        Text = "True",
                        Value = "1",
                        Selected = false
                    });
                    mySelectItemList.Add(new SelectListItem()
                    {
                        Text = "False",
                        Value = "0",
                        Selected = true
                    });
                }
                ViewBag.Active = mySelectItemList;
                List<SelectListItem> mySelectItemList2 = new List<SelectListItem>();
                if (or.ShiftType.ToString() == "0")
                {
                    mySelectItemList2.Add(new SelectListItem()
                    {   Text = "全天",Value = "0", Selected = true    });
                    mySelectItemList2.Add(new SelectListItem()
                    {   Text = "早班",Value = "1",Selected = false   });
                    mySelectItemList2.Add(new SelectListItem()
                    { Text = "晚班", Value = "2", Selected = false });
                    mySelectItemList2.Add(new SelectListItem()
                    { Text = "其他", Value = " ", Selected = false });
                }
                else if(or.ShiftType.ToString() == "1")
                {
                    mySelectItemList2.Add(new SelectListItem()
                    { Text = "全天", Value = "0", Selected = false });
                    mySelectItemList2.Add(new SelectListItem()
                    { Text = "早班", Value = "1", Selected = true });
                    mySelectItemList2.Add(new SelectListItem()
                    { Text = "晚班", Value = "2", Selected = false });
                    mySelectItemList2.Add(new SelectListItem()
                    { Text = "其他", Value = " ", Selected = false });
                }
                else if (or.ShiftType.ToString() == "2")
                {
                    mySelectItemList2.Add(new SelectListItem()
                    { Text = "全天", Value = "0", Selected = false });
                    mySelectItemList2.Add(new SelectListItem()
                    { Text = "早班", Value = "1", Selected = false });
                    mySelectItemList2.Add(new SelectListItem()
                    { Text = "晚班", Value = "2", Selected = true });
                    mySelectItemList2.Add(new SelectListItem()
                    { Text = "其他", Value = " ", Selected = false });
                }
                else
                { 
                    mySelectItemList2.Add(new SelectListItem()
                    { Text = "全天", Value = "0", Selected = false });
                    mySelectItemList2.Add(new SelectListItem()
                    { Text = "早班", Value = "1", Selected = false });
                    mySelectItemList2.Add(new SelectListItem()
                    { Text = "晚班", Value = "2", Selected = false });
                    mySelectItemList2.Add(new SelectListItem()
                    { Text = "其他", Value = "", Selected = true });
                }
                ViewBag.ShiftType = mySelectItemList2;

                ViewBag.dataText = dataText;
            }



            return View();

        }

        public JsonResult ItemEdit(string ID, string Password, string Name, string CitizenID, string MobilePhone, string HomePhone, string CompanyNo, string FleetID, string ShiftType, string City, string Distinct, string Address, string Active)
        {

            Dictionary<string, dynamic> JsonResult = new Dictionary<string, dynamic>();
            DAL.DALGeneral dalr = new DAL.DALGeneral(ConnectionString);

            bool rtn = false;
            int fleetid = dalr.GetFleet(FleetID).FleetID;
            string MD5pw = "";
            MD5pw = Encrypt(Password);
            rtn = dalr.UpdateUser(ID, MD5pw, Name, CitizenID, MobilePhone, HomePhone, CompanyNo, fleetid, ShiftType, City, Distinct, Address, Active);

            if (rtn)
            {
                JsonResult.Add("Message", "Success");
                return Json(JsonResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                JsonResult.Add("Message", "Failure");
                return Json(JsonResult, JsonRequestBehavior.AllowGet);
            }

        }

        public JsonResult ItemCreate(string ID, string Password, string Name, string CitizenID, string MobilePhone, string HomePhone, string CompanyNo, string FleetID, string ShiftType, string City, string Distinct, string Address, string Active)
        {

            Dictionary<string, dynamic> JsonResult = new Dictionary<string, dynamic>();
            DAL.DALEmployee dalr = new DAL.DALEmployee(ConnectionString);

            bool rtn = false;
            DAL.DALGeneral dal = new DAL.DALGeneral(ConnectionString);
            int fleetid = dal.GetFleet(FleetID).FleetID;
            string cno = dal.GetFleet(FleetID).Company;

            string MD5pw = "";
            MD5pw = Encrypt(Password);
             //AddAccount(int uid, string pw, string Name, string cid, string phone1, string phone2, string role, string cno, string fno, string city, string distinct, string address)
            rtn = dalr.AddAccount(Convert.ToInt32(ID), MD5pw, Name, CitizenID, MobilePhone, HomePhone, "D", cno, fleetid.ToString(), City, Distinct, Address);

            if (rtn)
            {
                JsonResult.Add("Message", "Success");
                return Json(JsonResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                JsonResult.Add("Message", "Failure");
                return Json(JsonResult, JsonRequestBehavior.AllowGet);
            }

        }

        public static string Encrypt(string PlanText)
        {
            byte[] bytes = Encoding.Default.GetBytes(PlanText);
            byte[] bytes2 = Encoding.Default.GetBytes("callcar");
            byte[] array = new byte[bytes.Length + bytes2.Length];
            bytes.CopyTo(array, 0);
            bytes2.CopyTo(array, bytes.Length);
            MD5 mD = MD5.Create();
            byte[] array2 = mD.ComputeHash(array);
            byte[] array3 = new byte[array2.Length + bytes2.Length];
            array2.CopyTo(array3, 0);
            bytes2.CopyTo(array3, array2.Length);
            return Convert.ToBase64String(array3);
        }

    }
}