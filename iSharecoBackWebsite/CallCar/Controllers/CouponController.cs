﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using CallCar.Models;
using CallCar.BAL;
using System.Configuration;
using System.Data;
using System.IO;
using System.Net;
using System.Net.Mime;
using System.Security.Cryptography;
using ClosedXML.Excel;
using CallCar.Infrastructure;
using System.Text;

namespace CallCar.Controllers
{
    public class CouponController : Controller
    {
        // GET: Coupon
        public ActionResult Index()
        {
            return View();
        }

        public static string ConnectionString = ConfigurationManager.ConnectionStrings["CarpoolLab"].ConnectionString;
        public ActionResult GetList(int page,string searchJson = null)
        {
            int itemPerPage = 10;
            Dictionary<string, dynamic> JsonResult = new Dictionary<string, dynamic>();

            try
            {

                string Coupon = "";

                if (searchJson != null)
                {
                    try
                    {

                        dynamic search = JObject.Parse(searchJson);

                        Coupon = search.Coupon;
                    }
                    catch (Exception e)
                    {
                        e.ToString();
                    }

                }

                Dictionary<string, dynamic> parameters = new Dictionary<string, dynamic>();
                
                parameters.Add("Coupon", Coupon); 

                string dataString = Helper.jsonEncode(parameters);


                DAL.DALGeneral General = new DAL.DALGeneral(ConnectionString);
                QOrderCondStruct qs = JsonConvert.DeserializeObject<QOrderCondStruct>(dataString);
                DataTable dt = General.Coupon(Coupon);

                int TotalItem = 0;
                int countSkip = itemPerPage * (page - 1);
                int countTake = itemPerPage;
                List<dynamic> data = new List<dynamic>();
                TotalItem = dt.Rows.Count;
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        Dictionary<string, dynamic> a = new Dictionary<string, dynamic>();
                        a.Add("SeqNo", Convert.ToString(dt.Rows[i]["序號"]));
                        a.Add("CouponCode", Convert.ToString(dt.Rows[i]["折扣碼"]));
                        a.Add("StartDate", Convert.ToString(dt.Rows[i]["使用有效起日"]));
                        a.Add("EndDate", Convert.ToString(dt.Rows[i]["使用有效迄日"]));
                        a.Add("CarFeeSub", Convert.ToString(dt.Rows[i]["車資折抵優惠金額"]));
                        a.Add("FixCarFee", Convert.ToString(dt.Rows[i]["固定車資優惠金額"]));
                        a.Add("NightFeeSub", Convert.ToString(dt.Rows[i]["夜加優惠金額"]));
                        a.Add("Memo", Convert.ToString(dt.Rows[i]["說明"]));
                        a.Add("UseCnt", Convert.ToString(dt.Rows[i]["已使用數量"]));
                        a.Add("LimitCnt", Convert.ToString(dt.Rows[i]["限制數量"]));
                        a.Add("TakeDateS", Convert.ToString(dt.Rows[i]["可預約乘車日起日"]));
                        a.Add("TakeDateE", Convert.ToString(dt.Rows[i]["可預約乘車日迄日"]));
                        a.Add("CreateTime", Convert.ToString(dt.Rows[i]["新增時間"]));
                        a.Add("UpdTime", Convert.ToString(dt.Rows[i]["異動時間"]));
                        data.Add(a);
                    }
                }

                JsonResult.Add("data", data);

                double PageTotal = (double)TotalItem / itemPerPage;
                PageTotal = Math.Ceiling(PageTotal);


                JsonResult.Add("totalItem", TotalItem);
                JsonResult.Add("pageTotal", PageTotal);
                JsonResult.Add("nowpage", page);
            }
            catch (Exception e)
            {
                e.ToString();
            }


            return Json(JsonResult);

        }

        public ActionResult InsertCoupon(string InsertJson = null)
        {

            Dictionary<string, dynamic> JsonResult = new Dictionary<string, dynamic>();

            try
            {

                int Cnum = 0;
                int Clong = 0;
                int CarFeeSubInput = 0;
                int FixCarFeeInput = 0;
                int NightFeeSubInput = 0;

                string QSDate1 = "";
                string QSDate2 = "";
                string QEDate1 = "";
                string QEDate2 = "";
                string comment = "";
                if (InsertJson != null)
                {
                    try
                    {

                        dynamic input = JObject.Parse(InsertJson);

                        Cnum = input.Cnum;
                        Clong = input.Clong;
                        CarFeeSubInput = input.CarFeeSubInput;
                        FixCarFeeInput = input.FixCarFeeInput;
                        NightFeeSubInput = input.NightFeeSubInput;

                        QSDate1 = input.QSDate1;
                        QSDate2 = input.QSDate2;
                        QEDate1 = input.QEDate1;
                        QEDate2 = input.QEDate2;
                        comment = input.comment;
                    }
                    catch (Exception e)
                    {
                        e.ToString();
                    }

                }

                DAL.DALGeneral General = new DAL.DALGeneral(ConnectionString);
                List<string> List = new List<string>();
                string RandomString;

                List<string> couponlist = new List<string>();
                for (int i = 0; i < Cnum; i++)
                {
                    RandomString = GetRandomString(Clong).ToUpper();
                    couponlist.Add(RandomString);
                    System.Threading.Thread.Sleep(1000);
                }
                    for (int i = 0; i < Cnum; i++)
                    {
                         General.InsertCoupon(couponlist[i], QSDate1, QSDate2, QEDate1, QEDate2, comment, CarFeeSubInput, FixCarFeeInput, NightFeeSubInput);
                    }
                    JsonResult.Add("Message", "SUCCESS");
            }
            catch (Exception e)
            {
                JsonResult.Add("Message", e.ToString());
            }


            return Json(JsonResult);

        }

        public static string GetRandomString(int length)
        {
            string text = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
            Random random = new Random();
            StringBuilder stringBuilder = new StringBuilder();
            for (int i = 0; i < length; i++)
            {
                stringBuilder.Append(text[random.Next(0, text.Length)]);
            }
            return stringBuilder.ToString();
        }
    }
}