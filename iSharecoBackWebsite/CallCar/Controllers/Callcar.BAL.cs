﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using CallCar.Models;
using Newtonsoft.Json.Linq;

namespace CallCar.BAL
{
    public class CallcarBAL
    {
        protected string _ConnectionString;
        public CallcarBAL(string ConnectionString)
        {
            this._ConnectionString = ConnectionString;
        }
    }
    /// <summary>
    /// 會員帳號的BAL
    /// </summary>
    public class BALUserAccount : CallcarBAL
    {
        public BALUserAccount(string ConnectionString)
            : base(ConnectionString)
        {
        }

        public int CreateAccFromFB(string data, string token)
        {
            //string key = Common.AESCryptography.TokenAnalysis(token);
            string iv = "8417728284177282";
            Common.AESCryptography aes = new Common.AESCryptography(token, iv);
            string plantext = aes.Decrypt(data);
            APIInsAccFB js;
            try
            {
                js = JsonConvert.DeserializeObject<APIInsAccFB>(plantext);
            }
            catch (Exception)
            {
                throw new Exception("DataError");
            }
            OAccount acc = new OAccount();
            acc.Source = "Facebook";
            acc.SourceID = js.sid;
            acc.Email = js.email;
            acc.FName = js.fname;
            acc.Gender = Int32.Parse(js.sex);
            acc.LName = js.lname;
            acc.MName = js.mname;
            acc.MobilePhone = js.mobile;
            acc.NickName = js.nname;

            DAL.DALUserAccount u = new DAL.DALUserAccount(this._ConnectionString);
            try
            {
                return u.AddAccount(acc.Source, acc.SourceID, acc.Email, acc.FName, acc.LName, acc.MName, acc.Gender, acc.MobilePhone, acc.NickName, acc.Password);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public int CreateAccFromAPP(string data, string token)
        {
            //string key = Common.AESCryptography.TokenAnalysis(token);
            string iv = "8417728284177282";
            Common.AESCryptography aes = new Common.AESCryptography(token, iv);
            string plantext = aes.Decrypt(data);
            APIInsAccAPP js;
            try
            {
                js = JsonConvert.DeserializeObject<APIInsAccAPP>(plantext);
            }
            catch (Exception)
            {
                throw new Exception("DataError");
            }
            OAccount acc = new OAccount();
            acc.Source = "APP";
            acc.Email = js.email;
            acc.FName = js.fname;
            acc.LName = js.lname;
            acc.MobilePhone = js.mobile;
            acc.Password = Common.MD5Cryptography.Encrypt(js.pw);

            DAL.DALUserAccount u = new DAL.DALUserAccount(this._ConnectionString);
            try
            {
                return u.AddAccount(acc.Source, acc.SourceID, acc.Email, acc.FName, acc.LName, acc.MName, acc.Gender, acc.MobilePhone, acc.NickName, acc.Password);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public bool UpdAccountData(int index, string data, string token)
        {
            //string key = Common.AESCryptography.TokenAnalysis(token);
            string iv = "8417728284177282";
            Common.AESCryptography aes = new Common.AESCryptography(token, iv);
            string plantext = aes.Decrypt(data);

            string id, value;
            string[] param = plantext.Split(',');

            id = param[0];
            value = param[1];

            try
            {
                DAL.DALUserAccount dal = new DAL.DALUserAccount(this._ConnectionString);
                switch (index)
                {
                    case 1:

                        dal.UpdAccountByColumn(Int32.Parse(id), "NickName", value);
                        break;
                    case 2:
                        dal.UpdAccountByColumn(Int32.Parse(id), "FirstName", value);
                        break;
                    case 3:
                        dal.UpdAccountByColumn(Int32.Parse(id), "LastName", value);
                        break;
                    case 4:
                        dal.UpdAccountByColumn(Int32.Parse(id), "MidName", value);
                        break;
                    case 5:
                        dal.UpdAccountByColumn(Int32.Parse(id), "Gender", value);
                        break;
                    case 6:
                        dal.UpdAccountByColumn(Int32.Parse(id), "AgeRange", value);
                        break;
                    case 7:
                        dal.UpdAccountByColumn(Int32.Parse(id), "Local", value);
                        break;
                    case 8:
                        dal.UpdAccountByColumn(Int32.Parse(id), "Email", value);
                        break;
                    case 9:
                        dal.UpdAccountByColumn(Int32.Parse(id), "Birthday", value);
                        break;
                    case 10:
                        dal.UpdAccountByColumn(Int32.Parse(id), "MobilePhone", value);
                        break;
                    case 11:
                        dal.UpdAccountByColumn(Int32.Parse(id), "PicUrl", value);
                        break;
                }
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public OAccount GetAccountInfo(int UserID)
        {
            try
            {
                DAL.DALUserAccount dal = new DAL.DALUserAccount(this._ConnectionString);
                OAccount acc = dal.GetAccountInfo(UserID);
                return acc;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public OAccount GetAccountInfo(string Source, string SourceID)
        {
            try
            {
                DAL.DALUserAccount dal = new DAL.DALUserAccount(this._ConnectionString);
                OAccount acc = dal.GetAccountInfo(Source, SourceID);
                return acc;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public OAccount GetAccountInfo(string Email)
        {
            try
            {
                DAL.DALUserAccount dal = new DAL.DALUserAccount(this._ConnectionString);
                OAccount acc = dal.GetAccountInfo(Email);

                return acc;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public string ChkEmailExist(string data, string token)
        {
            //string key = Common.AESCryptography.TokenAnalysis(token);
            string iv = "8417728284177282";
            Common.AESCryptography aes = new Common.AESCryptography(token, iv);
            string plantext = aes.Decrypt(data);

            OAccount acc = GetAccountInfo(plantext);

            if (acc == null)
                return null;
            else
                return acc.Source;
        }

        public bool ChkAccountPW(string data, string token)
        {
            //string key = Common.AESCryptography.TokenAnalysis(token);
            string iv = "8417728284177282";
            Common.AESCryptography aes = new Common.AESCryptography(token, iv);
            string plantext = aes.Decrypt(data);

            AccountPWStruct ps = JsonConvert.DeserializeObject<AccountPWStruct>(plantext);
            string Email = ps.acc;
            string Password = ps.pw;

            DAL.DALUserAccount dal = new DAL.DALUserAccount(this._ConnectionString);
            if (Common.MD5Cryptography.Encrypt(Password).Equals(dal.GetPassword(Email)))
                return true;
            else
                return false;
        }

        public OAccount GetAccountFromEmail(string data, string token)
        {
            //string key = Common.AESCryptography.TokenAnalysis(token);
            string iv = "8417728284177282";
            Common.AESCryptography aes = new Common.AESCryptography(token, iv);
            string plantext = aes.Decrypt(data);

            OAccount acc = GetAccountInfo(plantext);

            if (acc == null)
                return null;
            else
                return acc;
        }

        public OAccount GetAccountFromFB(string data, string token)
        {
            //string key = Common.AESCryptography.TokenAnalysis(token);
            string iv = "8417728284177282";
            Common.AESCryptography aes = new Common.AESCryptography(token, iv);
            string plantext = aes.Decrypt(data);
            string Source = plantext.Split(',')[0];
            string SourceID = plantext.Split(',')[1];

            OAccount acc = GetAccountInfo(Source, SourceID);

            if (acc == null)
                return null;
            else
                return acc;
        }
    }

    /// <summary>
    /// 員工的BAL
    /// </summary>
    public class BALEmployee : CallcarBAL
    {
        public BALEmployee(string ConnectionString) : base(ConnectionString) { }

        public int AddNewEmployeeBackWeb(string data)
        {
            EmployeInfoStruct es = JsonConvert.DeserializeObject<EmployeInfoStruct>(data);

            DAL.DALSerialNum dSerial = new DAL.DALSerialNum(this._ConnectionString);
            int UID = 0;

            try
            {
                OSerialNum serial = dSerial.GetSerialNumObject("EmployeeID");
                UID = Int32.Parse(DateTime.Now.ToString(serial.Prefix) + dSerial.GetSeqNoString(serial));
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            DAL.DALEmployee dal = new DAL.DALEmployee(this._ConnectionString);
            try
            {
                dal.AddAccount(UID, es.pw, es.empname, es.cid, es.phone1, es.phone2, es.role, es.company, es.fleet, es.city, es.distinct, es.address);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return UID;
        }

        public bool ChkAccPWAPP(string data, string token)
        {

            string iv = "8417728284177282";
            Common.AESCryptography aes = new Common.AESCryptography(token, iv);
            string plantext = aes.Decrypt(data);
            //string plantext = data;
            AccountPWStruct ps = JsonConvert.DeserializeObject<AccountPWStruct>(plantext);
            int Account = Int32.Parse(ps.acc);
            string Password = ps.pw;

            return _chkAccPW(Account, Password);
        }

        public EmployeInfoStruct ChkAccPWandGetInfoBackWeb(string data)
        {
            EmployeInfoStruct result;
            try
            {
                AccountPWStruct accountPWStruct = JsonConvert.DeserializeObject<AccountPWStruct>(data);
                string acc = accountPWStruct.acc;
                string pw = accountPWStruct.pw;
                DAL.DALEmployee dALEmployee = new DAL.DALEmployee(this._ConnectionString);
                int employeeIDByEmail = dALEmployee.GetEmployeeIDByEmail(acc);
                if (employeeIDByEmail == 0)
                {
                    throw new Exception("帳號密碼錯誤");
                }
                if (this._chkAccPW(employeeIDByEmail, pw))
                {
                    OEmployee employeeInfoByID = dALEmployee.GetEmployeeInfoByID(employeeIDByEmail);
                    EmployeInfoStruct employeInfoStruct = new EmployeInfoStruct();
                    if (employeeInfoByID.Active == "0")
                    {
                        result = null;
                    }
                    else
                    {
                        employeInfoStruct.id = employeeIDByEmail;
                        employeInfoStruct.role = employeeInfoByID.Role;
                        employeInfoStruct.Logint_tGroups_AutoID = 1;
                        employeInfoStruct.empname = employeeInfoByID.Name;
                        employeInfoStruct.company = employeeInfoByID.CompanyNo;
                        employeInfoStruct.fleet = employeeInfoByID.FleetID.ToString();
                        employeInfoStruct.active = "1";
                        employeInfoStruct.address = "";
                        employeInfoStruct.cid = "";
                        employeInfoStruct.city = "";
                        employeInfoStruct.distinct = "";
                        employeInfoStruct.phone1 = "";
                        employeInfoStruct.phone2 = "";
                        employeInfoStruct.pw = "";
                        result = employeInfoStruct;
                    }
                }
                else
                {
                    result = null;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.StackTrace);
            }
            return result;
        }

        public string SetEmployeeStatus(string data)
        {
            int EmpID = Int32.Parse(data.Split(',')[0]);
            string OldStatus = data.Split(',')[1];
            if (OldStatus == "Active")
                OldStatus = "1";
            else
                OldStatus = "0";


            string NewStatus = data.Split(',')[2];
            if (NewStatus == "Active")
                NewStatus = "1";
            else
                NewStatus = "0";

            DAL.DALEmployee dal = new DAL.DALEmployee(this._ConnectionString);
            try
            {
                string status = dal.SetEmployeStatus(EmpID, OldStatus, NewStatus);
                if (status == "1")
                    return "Active";
                else
                    return "Deactive";
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private bool _chkAccPW(int EmpID, string PW)
        {
            DAL.DALEmployee dal = new DAL.DALEmployee(this._ConnectionString);
            if (Common.MD5Cryptography.Encrypt(PW).Equals(dal.GetPassword(EmpID)))
                return true;
            else
                return false;
        }

        public List<OEmployee> GetALLEmployee()
        {
            DAL.DALEmployee dal = new DAL.DALEmployee(this._ConnectionString);
            List<OEmployee> List = new List<OEmployee>();

            return dal.GetEmployeeInfo();

        }

        public DriverInfoStruct GetDriverInfo(int UserID)
        {
            DAL.DALEmployee dal = new DAL.DALEmployee(this._ConnectionString);
            OEmployee oe = dal.GetEmployeeInfoByID(UserID);
            if (oe != null)
            {
                DriverInfoStruct ds = new DriverInfoStruct();
                ds.id = oe.ID;
                ds.name = oe.Name;
                ds.fno = oe.FleetID;
                ds.fname = "測試車隊";
                return ds;
            }
            else
                return null;
        }

        public OEmployee GetEmployee(int EmpID)
        {
            DAL.DALEmployee dal = new DAL.DALEmployee(this._ConnectionString);
            try
            {
                return dal.GetEmployeeInfoByID(EmpID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<OEmployee> GetDriverByFleet(int FleetID)
        {
            DAL.DALGeneral dalg = new DAL.DALGeneral(this._ConnectionString);
            List<OFleet> fList = dalg.GetFleetList();
            OFleet f = fList.Find(x => x.FleetID == FleetID);
            DAL.DALEmployee dal = new DAL.DALEmployee(this._ConnectionString);
            List<OEmployee> List = dal.GetEmployeeByCompany(f.Company);
            if (List != null)
                return List.FindAll(x => x.Role == "D" && x.FleetID == FleetID);
            else
                return null;
        }
    }

    /// <summary>
    /// 電子發票的BAL
    /// </summary>
    public class BALInvoice : CallcarBAL
    {

        public BALInvoice(string ConnectionString)
            : base(ConnectionString)
        {

        }

        public string CreateNewInvoice(string data, string token)
        {
            //string key = Common.AESCryptography.TokenAnalysis(token);
            string iv = "8417728284177282";
            Common.AESCryptography aes = new Common.AESCryptography(token, iv);
            string plantext = aes.Decrypt(data);
            APINewInvoice ni;
            try
            {
                ni = JsonConvert.DeserializeObject<APINewInvoice>(data);
                DAL.DALInvoice dal = new DAL.DALInvoice(this._ConnectionString);
                return dal.CreatenewInvoice(Int32.Parse(ni.uid), ni.itype, ni.ititle, ni.ein, ni.iaddress);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public bool RemoveInvoice(string data, string token)
        {
            //string key = Common.AESCryptography.TokenAnalysis(token);
            string iv = "8417728284177282";
            Common.AESCryptography aes = new Common.AESCryptography(token, iv);
            string plantext = aes.Decrypt(data);
            string InvoiceID;
            int UserID;
            InvoiceID = plantext.Split(',')[0];
            UserID = Int32.Parse(plantext.Split(',')[1]);
            DAL.DALInvoice dal = new DAL.DALInvoice(this._ConnectionString);
            try
            {
                return dal.RemoveInvoice(UserID, InvoiceID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<OInvoice> SelectUserAllInvoice(string data, string token)
        {
            //string key = Common.AESCryptography.TokenAnalysis(token);
            string iv = "8417728284177282";
            Common.AESCryptography aes = new Common.AESCryptography(token, iv);
            string plantext = aes.Decrypt(data);
            int UserID = Int32.Parse(plantext);
            DAL.DALInvoice dal = new DAL.DALInvoice(this._ConnectionString);
            List<OInvoice> List = dal.QryUserInvoice(UserID, "");

            if (List != null)
                return List;
            else
                return null;
        }

        public OInvoice SelectUserUniqueInvoice(string data, string token)
        {
            //string key = Common.AESCryptography.TokenAnalysis(token);
            string iv = "8417728284177282";
            Common.AESCryptography aes = new Common.AESCryptography(token, iv);
            string plantext = aes.Decrypt(data);
            int UserID = Int32.Parse(plantext.Split(',')[0]);
            string InvoiceID = plantext.Split(',')[1];
            DAL.DALInvoice dal = new DAL.DALInvoice(this._ConnectionString);
            List<OInvoice> List = dal.QryUserInvoice(UserID, InvoiceID);

            if (List != null)
                return List[0];
            else
                return null;
        }

        public bool SetDefaultInvoice(string data, string token)
        {
            //string key = Common.AESCryptography.TokenAnalysis(token);
            string iv = "8417728284177282";
            Common.AESCryptography aes = new Common.AESCryptography(token, iv);
            string plantext = aes.Decrypt(data);
            int UserID = Int32.Parse(plantext.Split(',')[0]);
            string InvoiceID = plantext.Split(',')[1];
            DAL.DALInvoice dal = new DAL.DALInvoice(this._ConnectionString);
            return dal.SetDefault(UserID, InvoiceID);

        }
    }

    /// <summary>
    /// 信用卡的BAL
    /// </summary>
    public class BALCreditCard : CallcarBAL
    {
        public BALCreditCard(string ConnectionString)
            : base(ConnectionString)
        {

        }

        public int CreateNewCard(string data, string token)
        {
            //string key = Common.AESCryptography.TokenAnalysis(token);
            string iv = "8417728284177282";
            Common.AESCryptography aes = new Common.AESCryptography(token, iv);
            string plantext = aes.Decrypt(data);

            APINewCard ic = JsonConvert.DeserializeObject<APINewCard>(plantext);

            BALUserAccount bal = new BALUserAccount(this._ConnectionString);
            OAccount acc = bal.GetAccountInfo(Int32.Parse(ic.uid));
            string CreditOrderNo;
            DAL.DALSerialNum dSerial = new DAL.DALSerialNum(this._ConnectionString);
            try
            {
                OSerialNum serial = dSerial.GetSerialNumObject("CreditAuth");
                CreditOrderNo = DateTime.Now.ToString(serial.Prefix) + dSerial.GetSeqNoString(serial);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            Common.Pay2GoCardAuth auth = new Common.Pay2GoCardAuth(CreditOrderNo, 1, "卡片驗證，不執行請款", acc.Email, ic.cno, ic.edate, ic.acode);
            if (auth.Authorize() != "Success")
                throw new Exception("信用卡驗證失敗");

            DAL.DALCreditCard dCard = new DAL.DALCreditCard(this._ConnectionString);
            try
            {
                ic.edate = "20" + ic.edate;
                return dCard.CreateNewCard(acc.ID, ic.ccom, ic.cno, ic.edate, ic.acode, auth.GetToken(), auth.GetTokenLife());
            }
            catch (Exception ex)
            {
                throw new Exception("DB Insert 新增信用卡失敗" + Environment.NewLine + ex.StackTrace);
            }
        }

        public bool RemoveCard(string data, string token)
        {
            //string key = Common.AESCryptography.TokenAnalysis(token);
            string iv = "8417728284177282";
            Common.AESCryptography aes = new Common.AESCryptography(token, iv);
            string plantext = aes.Decrypt(data);
            string CardID;
            int UserID;
            CardID = data.Split(',')[0];
            UserID = Convert.ToInt32(data.Split(',')[1]);
            DAL.DALCreditCard dal = new DAL.DALCreditCard(this._ConnectionString);
            return dal.RemoveCard(UserID, CardID);
        }

        public OCard GetUserActiveCard(string data, string token)
        {
            string iv = "8417728284177282";
            Common.AESCryptography aes = new Common.AESCryptography(token, iv);
            string plantext = aes.Decrypt(data);
            int UserID = Int32.Parse(plantext);
            DAL.DALCreditCard dal = new DAL.DALCreditCard(this._ConnectionString);
            OCard card = dal.QryUserCards(UserID);

            if (card != null)
                return card;
            else
                return null;



        }
    }

    /// <summary>
    /// 訂單的BAL
    /// </summary>
    public class BALReservation : CallcarBAL
    {
        public BALReservation(string ConnectionString) : base(ConnectionString) { }

        public string CreateNewReservation(string data, string token)
        {
            string ReservationNo;
            string iv = "8417728284177282";
            Common.AESCryptography aes = new Common.AESCryptography(token, iv);
            string plantext = aes.Decrypt(data);



            APINewOrder os = JsonConvert.DeserializeObject<APINewOrder>(plantext);

            if (os.pcartype == "1")
            {
                BALDispatch bald = new BALDispatch(this._ConnectionString);
                try
                {
                    if (bald.isCarFull(os.dno))
                    {
                        throw new Exception("此車次已額滿");
                    }

                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }

            OReservation o = new OReservation();
            o.BaggageCnt = Int32.Parse(os.bcnt);
            o.ChooseType = os.ctype;
            o.Coupon = "";

            o.PickCartype = os.pcartype;
            if (os.ctype == "F")
            {
                o.FlightTime = Common.General.ConvertToDateTime(os.fdate + os.ftime, 12);
                o.FlightDate = os.fdate;
                o.FlightNo = os.fno;
            }
            else
            {
                o.FlightNo = "";
                o.FlightDate = "";
            }
            OCard card = new OCard();
            DAL.DALCreditCard dalc = new DAL.DALCreditCard(this._ConnectionString);
            OCard oc = dalc.QryUserCards(Int32.Parse(os.uid));
            DAL.DALInvoice dali = new DAL.DALInvoice(this._ConnectionString);
            o.Invoice = os.idata;//JsonConvert.SerializeObject(oi[0]);
            o.Credit = JsonConvert.SerializeObject(oc);
            o.PassengerCnt = Int32.Parse(os.pcnt);
            o.PickupCity = os.pcity;
            o.PickupDistinct = os.pdistinct;
            o.PickupAddress = os.paddress;
            o.PickupVillage = os.pvillage;
            o.PickupGPS = new OSpatialGPS(double.Parse(os.plat), double.Parse(os.plng));
            o.PreferTime = os.ptime;
            o.ProcessStage = "0";
            o.ServeDate = os.sdate;
            o.ServiceType = os.stype;
            o.TakeoffAddress = os.taddress;
            o.TakeoffCity = os.tcity;
            o.TakeoffDistinct = os.tdistinct;
            o.TakeoffVillage = os.tvillage;
            o.TakeoffGPS = new OSpatialGPS(double.Parse(os.tlat), double.Parse(os.tlng));
            o.UserID = Int32.Parse(os.uid);
            o.MaxFlag = os.max;
            o.TrailPrice = os.price;

            DAL.DALSerialNum dSerial = new DAL.DALSerialNum(this._ConnectionString);
            try
            {
                OSerialNum serial = dSerial.GetSerialNumObject("Reservation");
                ReservationNo = DateTime.Now.ToString(serial.Prefix) + dSerial.GetSeqNoString(serial);

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            o.ReservationNo = ReservationNo;

            try
            {
                DAL.DALReservation dalReservation = new DAL.DALReservation(this._ConnectionString);
                if (dalReservation.CreateNewReservation(o, os.pcartype, os.dno))
                {
                    if (o.ChooseType == "F")
                        try
                        {
                            dalReservation.AddTrackFlight(o.FlightNo, o.FlightTime);
                        }
                        catch (Exception)
                        {
                        }

                    return ReservationNo;
                }
                else
                    return null;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public List<OReservation> GetReservationByDate(string date)
        {
            DAL.DALReservation dalr = new DAL.DALReservation(this._ConnectionString);
            List<OReservation> ListR;
            try
            {
                ListR = dalr.GetReservations(date, date);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            if (ListR == null)
                return null;
            else
                return ListR;
        }

        public List<OReservation> GetReservationByDateAndStage(string QSdate,string QEdate, string stage, int FleetID)
        {
            DAL.DALReservation dalr = new DAL.DALReservation(this._ConnectionString);
            List<OReservation> ListR;
            try
            {
                ListR = dalr.GetReservationsStage(QSdate, QEdate, stage, FleetID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            if (ListR == null)
                return null;
            else
                return ListR;
        }

        //public List<OReservation> GetReservationByConditions(string data)
        //{
        //    try
        //    {
        //        QOrderCondStruct qs = JsonConvert.DeserializeObject<QOrderCondStruct>(data);
        //        if (string.IsNullOrWhiteSpace(qs.QSDate) || string.IsNullOrWhiteSpace(qs.QEDate))
        //        {
        //            throw new Exception("日期為必須條件");
        //        }

        //        if (qs.take < 0 || qs.skip < 0)
        //        {
        //            throw new Exception("查詢範圍異常");
        //        }

        //        DAL.DALReservation dalr = new DAL.DALReservation(this._ConnectionString);

        //        return dalr.GetReservationsMultipleCondition(qs, qs.take, qs.skip,"0","0");
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(ex.Message);
        //    }

        //}

        public OReservation GetReservationByNo(string RNo)
        {
            DAL.DALReservation dalr = new DAL.DALReservation(this._ConnectionString);
            OReservation R;
            try
            {
                R = dalr.GetReservation(RNo);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            if (R == null)
                return null;
            else
                return R;
        }

        public bool UpdServicePosition(string data)
        {
            UOrderStruct uo = JsonConvert.DeserializeObject<UOrderStruct>(data);
            if (uo.uType != "ServicePosition")
                return false;

            List<string> City = new List<string>();
            List<string> Distinct = new List<string>();
            List<string> Village = new List<string>();
            List<string> Address = new List<string>();
            List<string> Lat = new List<string>();
            List<string> Lng = new List<string>();
            string[] Olddata = uo.OldData.Split(',');
            string[] Newdata = uo.NewData.Split(',');
            City.Add(Olddata[0]);
            City.Add(Newdata[1]);
            Distinct.Add(Olddata[0]);
            Distinct.Add(Olddata[1]);
            Village.Add(Olddata[0]);
            Village.Add(Olddata[1]);
            Address.Add(Olddata[0]);
            Address.Add(Olddata[1]);
            Lat.Add(Olddata[0]);
            Lat.Add(Olddata[1]);
            Lng.Add(Olddata[0]);
            Lng.Add(Olddata[1]);

            DAL.DALReservation dalr = new DAL.DALReservation(this._ConnectionString);
            OReservation r = dalr.GetReservation(uo.RNo);

            try
            {
                return dalr.UpdReservationServicePostion(uo.RNo, r.ServiceType, City, Distinct, Village, Address, Lat, Lng);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        /// <summary>
        /// 取消訂單
        /// </summary>
        /// <param name="Source">取消來源 U:使用者 B:後台</param>
        /// <param name="Rno">訂單編號</param>
        /// <returns></returns>
        public string CancelReservation(string Source, string data, string token)
        {
            BALReservation balr = new BALReservation(this._ConnectionString);
            OReservation or;
            string Rno = string.Empty;

            if (Source == "U")
            {
                string iv = "8417728284177282";
                Common.AESCryptography aes = new Common.AESCryptography(token, iv);
                Rno = aes.Decrypt(data);
            }
            else
                Rno = data;

            try
            {
                or = balr.GetReservationByNo(Rno);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            DAL.DALReservation dalr = new DAL.DALReservation(this._ConnectionString);

            DateTime NowTime = DateTime.Now;
            bool rtn = false;

            if (or == null)
                return "XXX";

            if (or.ProcessStage == "X" || or.ProcessStage == "B" || or.ProcessStage == "C" || or.ProcessStage == "Z")
            {
                return "999";// 目前狀態無法取消
            }
            else if (or.ProcessStage == "A")
            {
                if (Source == "U")
                {
                    DateTime TakeDatetime = Common.General.ConvertToDateTime(or.ServeDate + or.PreferTime, 12);

                    double diffTime = new TimeSpan(TakeDatetime.Ticks - NowTime.Ticks).TotalHours;
                    if (diffTime < 24)
                        return "998";//24小時內無法取消
                }

                try
                {
                    rtn = dalr.CancelReservation(Rno, or.ProcessStage);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }


            }
            else if (or.ProcessStage == "0")
            {
                try
                {
                    rtn = dalr.CancelReservation(Rno, or.ProcessStage);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }

            }
            else
            {
                return "XXX";
            }

            if (rtn)
                return "Success";
            else
                return "Failure";
        }
    }

    /// <summary>
    /// 航班的BAL
    /// </summary>
    public class BALFlight : CallcarBAL
    {
        public BALFlight(string ConnectionString) : base(ConnectionString) { }

        public APIFlightStruct GetFlightInfo(string data, string token)
        {
            //string key = Common.AESCryptography.TokenAnalysis(token);
            string iv = "8417728284177282";
            Common.AESCryptography aes = new Common.AESCryptography(token, iv);
            string plantext = aes.Decrypt(data);
            string FlightNo, FlightDate;
            FlightNo = plantext.Split(',')[0];
            FlightDate = plantext.Split(',')[1];

            DAL.DALFlight dal = new DAL.DALFlight(this._ConnectionString);
            try
            {
                OScheduledFlight fs = dal.GetFlightInfo(FlightDate, FlightNo);
                if (fs == null)
                    return null;

                APIFlightStruct afs = new APIFlightStruct();
                afs.ft = fs.FlightType;
                afs.fd = fs.FlightDate;
                afs.aid = fs.AirlineID;
                afs.fno = fs.FlightNo;
                afs.da = fs.DepartureAirportID;
                afs.dt = fs.DepartureTime;
                afs.aa = fs.ArrivalAirportID;
                afs.at = fs.ArrivalTime;
                afs.ter = fs.Ternimal;
                return afs;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }


    /// <summary>
    /// 選車次的BAL
    /// </summary>
    public class BALPickCar : CallcarBAL
    {
        public BALPickCar(string ConnectionString) : base(ConnectionString) { }


        /// <summary>
        /// 取得該日期時間內的可撿車次
        /// </summary>
        /// <param name="data">密文</param>
        /// <param name="token">ASE KEY</param>
        /// <returns></returns>
        public List<APIPickCarStruct> GetCarsCanBePick(string data, string token)
        {
            string iv = "8417728284177282";
            Common.AESCryptography aes = new Common.AESCryptography(token, iv);
            string plantext = aes.Decrypt(data);


            APIQCarStruct qc = JsonConvert.DeserializeObject<APIQCarStruct>(plantext);
            int bcnt = Convert.ToInt32(qc.b);
            int pcnt = Convert.ToInt32(qc.p);
            double lat = Convert.ToDouble(qc.lat);
            double lng = Convert.ToDouble(qc.lng);


            //
            DateTime TakeDatetime = Common.General.ConvertToDateTime(qc.d + qc.t, 12);
            DateTime MatchDatetime = Common.General.ConvertToDateTime(DateTime.Now.AddDays(3).ToString("yyyyMMdd") + "0259", 12);

            double diffTime = new TimeSpan(TakeDatetime.Ticks - MatchDatetime.Ticks).TotalMinutes;
            if (diffTime > 0)
                return null;

            DAL.DALPickCar dal = new DAL.DALPickCar(this._ConnectionString);
            List<OPickCarInfo> pci = dal.GetPickCarsList(qc.d, qc.t, qc.s);
            List<APIPickCarStruct> List = new List<APIPickCarStruct>();

            if (pci != null)
            {
                //比對資料是否選擇此車次
                foreach (OPickCarInfo car in pci)
                {
                    //Step1. 人數行李決定是顯示
                    int PCapacity = 0, BCapacity = 0;
                    if (car.CarType == "四人座")
                    {
                        PCapacity = 3;
                        BCapacity = 3;
                    }
                    else if (car.CarType == "七人座")
                    {
                        if (car.MaxFlag == "1")
                        {
                            PCapacity = 7;
                            BCapacity = 7;
                        }
                        else
                        {
                            PCapacity = 5;
                            BCapacity = 7;
                        }
                    }

                    if (car.RemainBCapacity + bcnt > BCapacity)
                        car.ShowFlag = false;
                    else if (car.RemainPCapacity + pcnt > PCapacity)
                        car.ShowFlag = false;
                    else
                    {
                        car.RemainPCapacity = PCapacity - car.RemainPCapacity;
                        car.RemainBCapacity = BCapacity - car.RemainBCapacity;
                        car.ShowFlag = true;
                    }
                    //TODO:Step2航班時間決定是否顯示該班次 出國必須>3hr 回國必須>1.5hr

                    //Step3 地點距離決定是否顯示

                    OPickCarInfo.DispatchUnit last = car.Reservations.FindLast(x => x.SeqNo >= 0);
                    double distent = Common.General.GetDistance(lat, lng, last.lat, last.lng);

                    if (car.ServiceType == "O")
                    {
                        //出國 取最後一組客人 距離保守抓500M                     
                        if (distent > 0.5)
                            car.ShowFlag = false;
                        else
                            car.ShowFlag = true;
                    }
                    else
                    {
                        //出國 取最後一組客人 距離抓2KM                    
                        if (distent > 2)
                            car.ShowFlag = false;
                        else
                            car.ShowFlag = true;
                    }

                    //需要顯示的才計算時間
                    if (car.ShowFlag)
                    {
                        if (car.ServiceType == "I")
                        {
                            //回國一律統一上車時間
                            car.ShowServiceTime = last.ScheduleTime;
                        }
                        else
                        {
                            //回國抓最後一組預約時間十五分鐘後
                            car.ShowServiceTime = last.ScheduleTime.AddMinutes(15);
                        }
                    }

                    if (car.ShowFlag)
                    {
                        APIPickCarStruct s = new APIPickCarStruct();
                        s.cn = car.CarNo;
                        s.ct = car.CarType;
                        s.dn = car.DriverName;
                        s.dno = car.DispatchNo;
                        s.pt = car.ShowServiceTime.ToString("HH:mm");
                        s.rbc = car.RemainBCapacity;
                        s.rpc = car.RemainPCapacity;
                        s.sc = car.StopCnt;
                        s.st = car.ServiceType;
                        s.td = car.TakeDate;
                        s.tr = car.TimeRange;
                        List.Add(s);
                    }
                }

                return List;
            }
            else
            {
                return List;
            }
        }
    }


    public class BALDispatch : CallcarBAL
    {
        public BALDispatch(string ConnectionString) : base(ConnectionString) { }

        public List<ODispatchSheet> GetDispatchSheetByDriver(int DriverID)
        {
            //string plantext;

            //string iv = "8417728284177282";
            //Common.AESCryptography aes = new Common.AESCryptography(token, iv);
            //plantext = aes.Decrypt(data);
            ////plantext = data;

            //int DriverID = Convert.ToInt32(plantext);
            DateTime StartTime = DateTime.Now.AddHours(-12);
            string StartDate = StartTime.ToString("yyyyMMdd");
            string Stime = StartTime.ToString("HH") + "00";
            DateTime EndTime = DateTime.Now.AddHours(24);
            string EndDate = EndTime.ToString("yyyyMMdd");
            string Etime = EndTime.ToString("HH") + "00";

            DAL.DALDispatch dal = new DAL.DALDispatch(this._ConnectionString);
            try
            {
                List<ODispatchSheet> ListD = dal.GetUnServeDispatchSheets(DriverID, StartDate, Stime, EndDate, Etime);
                if (ListD != null)
                    return ListD;
                else
                    return null;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<ODispatchSheet> GetDispatchSheetByDate(string StartDate, string EndDate)
        {
            //string StartDate = data.Split(',')[0];
            //string EndDate = data.Split(',')[1];

            DAL.DALDispatch dal = new DAL.DALDispatch(this._ConnectionString);
            try
            {
                List<ODispatchSheet> ListD = dal.GetDispatchSheets(StartDate, EndDate);
                if (ListD != null)
                    return ListD;
                else
                    return null;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<ODispatchSheet> GetDispatchSheetByDateAndStage(string StartDate, string EndDate, string Stage, int FleetID)
        {
            //string StartDate = data.Split(',')[0];
            //string EndDate = data.Split(',')[1];

            DAL.DALDispatch dal = new DAL.DALDispatch(this._ConnectionString);
            try
            {
                List<ODispatchSheet> ListD = dal.GetDispatchSheetsbyStage(StartDate, EndDate, Stage, FleetID);
                if (ListD != null)
                    return ListD;
                else
                    return null;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public ODispatchSheet GetDispatchSheetByNo(string Dno)
        {
            DAL.DALDispatch dal = new DAL.DALDispatch(this._ConnectionString);
            try
            {
                ODispatchSheet dispatch = dal.GetDispatchSheets(Dno);
                if (dispatch != null)
                    return dispatch;
                else
                    return null;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public ODispatchSheet GetDispatchSheetNoUserByNo(string Dno)
        {
            DAL.DALDispatch dal = new DAL.DALDispatch(this._ConnectionString);
            try
            {
                ODispatchSheet dispatch = dal.GetDispatchSheetsNoUser(Dno);
                if (dispatch != null)
                    return dispatch;
                else
                    return null;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public bool UpdateServiceStatusByDriver(ODriverServiceRecord odr)
        {

            try
            {
                DAL.DALDispatch dal = new DAL.DALDispatch(this._ConnectionString);

                if (dal.UpdateReservationServiceStatus(odr))
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public OMachSourceData GetMatchSourceByDate(string date)
        {
            DAL.DALReservation dalr = new DAL.DALReservation(this._ConnectionString);
            List<OReservation> RList;
            try
            {
                RList = dalr.GetReservationForMatch(date);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            if (RList == null || RList.Count == 0)
                return null;

            OMachSourceData Match = new OMachSourceData();
            Match.MatchDate = date;

            foreach (OReservation r in RList)
            {
                OMatchRData m = new OMatchRData();
                m.ReservationNo = r.ReservationNo;
                m.ServiceType = r.ServiceType;
                m.TimeSegment = r.PreferTime;
                m.BaggageCnt = r.BaggageCnt;
                m.PassengerCnt = r.PassengerCnt;
                m.PickupLat = r.PickupGPS.GetLat();
                m.PickupLng = r.PickupGPS.GetLng();
                m.TakeoffLat = r.TakeoffGPS.GetLat();
                m.TakeoffLng = r.TakeoffGPS.GetLng();
                m.MaxFlag = r.MaxFlag;

                Match.ReservationList.Add(m);
            }

            DAL.DALDispatch dald = new DAL.DALDispatch(this._ConnectionString);
            List<OScheduleData> SList = dald.GetScheduleByDate(date);

            foreach (OScheduleData s in SList)
            {
                OMatchCData c = new OMatchCData();
                c.CarType = s.CarType;
                c.ScheduleID = s.ScheduleID;
                c.ShiftType = s.ShiftType;
                if (s.CarType == "四人座")
                {
                    c.NCapacity = 3;
                    c.MCapacity = 4;
                }
                else if (s.CarType == "七人座")
                {
                    c.NCapacity = 5;
                    c.MCapacity = 7;
                }

                Match.CarList.Add(c);
            }

            return Match;
        }

        public bool SaveMatchRecord(string date, string Status, int ID, string MatchJson, string ErrMsg)
        {
            DAL.DALDispatch dald = new DAL.DALDispatch(this._ConnectionString);
            try
            {
                return dald.SaveMatchRecord(date, Status, ID, MatchJson, ErrMsg);
            }
            catch (Exception ex)

            { throw new Exception(ex.Message); }
        }

        /// <summary>
        /// 批次新增媒合結果
        /// </summary>
        /// <param name="data">媒合結果的JSON</param>
        /// <returns></returns>
        public bool BatchAddDispatchSheet(string data)
        {

            MatchResultJsonStruct MatchResult = JsonConvert.DeserializeObject<MatchResultJsonStruct>(data);



            DAL.DALDispatch dald = new DAL.DALDispatch(this._ConnectionString);
            DAL.DALEmployee dale = new DAL.DALEmployee(this._ConnectionString);
            DAL.DALReservation dalr = new DAL.DALReservation(this._ConnectionString);
            DAL.DALSerialNum dals = new DAL.DALSerialNum(this._ConnectionString);
            //將Json資料設定至派遣Object中
            foreach (DispatchInfo info in MatchResult.DispatchInfo)
            {
                ODispatchSheet od = new ODispatchSheet();

                string DispatchNo = string.Empty;
                try
                {
                    OSerialNum serial = dals.GetSerialNumObject("Dispatch");
                    DispatchNo = DateTime.Now.ToString(serial.Prefix) + dals.GetSeqNoString(serial);
                }
                catch (Exception ex)
                {
                    throw new Exception("取派車單號失敗" + ex.Message + Environment.NewLine + JsonConvert.SerializeObject(info));
                }

                od.DispatchNo = DispatchNo;

                int PCnt = 0, BCnt = 0;
                string TakeDate = string.Empty;

                foreach (ServiceList s in info.Detail.ServiceList)
                {
                    ODpServiceUnit su = new ODpServiceUnit();
                    OReservation r;
                    try
                    {
                        r = dalr.GetReservation(s.ReservationNo);
                    }
                    catch (Exception ex)
                    {
                        //寫入媒合紀錄檔
                        JObject jo = JsonConvert.DeserializeObject<JObject>(data);
                        JArray arryDispatch = (JArray)jo["DispatchInfo"];
                        for (int i = 0; i < arryDispatch.Count; i++)
                        {
                            if (Convert.ToInt32(arryDispatch[i]["Index"].ToString()) == info.Index)
                            {
                                SaveMatchRecord(od.TakeDate, "E", info.Index, arryDispatch[i]["Detail"].ToString(), ex.Message);
                                break;
                            }
                        }
                        continue;
                    }

                    su.DispatchNo = DispatchNo;
                    su.ScheduleTime = Convert.ToDateTime(s.ServiceTime);
                    su.ReservationNo = s.ReservationNo;
                    su.ServiceRemark = "0";
                    if (r.ServiceType == "I")
                        su.ServiceGPS = r.TakeoffGPS;
                    else
                        su.ServiceGPS = r.PickupGPS;
                    PCnt += r.PassengerCnt;
                    BCnt += r.BaggageCnt;
                    TakeDate = r.ServeDate;

                    od.ServiceList.Add(su);
                }

                od.BaggageCnt = BCnt;
                od.PassengerCnt = PCnt;
                if (info.Detail.ScheduleID != -1)
                {
                    OScheduleData sd = dald.GetScheduleByID(info.Detail.ScheduleID);
                    od.CarNo = sd.CarNo;
                    od.CarType = sd.CarType;
                    od.DriverID = Int32.Parse(sd.DriverID.ToString());
                    od.DriverName = dale.GetEmployeeInfoByID(sd.DriverID).Name;
                    od.FleetID = sd.FleetID;
                }
                else
                {
                    od.CarNo = "";
                    od.CarType = "";
                    od.DriverID = 0;
                    od.DriverName = "";
                    od.FleetID = 0;
                }
                od.TimeSegment = info.Detail.TimeSegment;


                od.FisrtServiceTime = "";
                od.ServiceCnt = info.Detail.ServiceCnt;
                od.ServiceType = info.Detail.ServiceType;
                od.TakeDate = TakeDate;// info.Detail.ServiceDate.Replace("/", "");


                //儲存媒合資料
                try
                {
                    dald.addDispatchSheet(od);
                }
                catch (Exception ex)
                {
                    //寫入媒合紀錄檔
                    JObject jo = JsonConvert.DeserializeObject<JObject>(data);
                    JArray arryDispatch = (JArray)jo["DispatchInfo"];
                    for (int i = 0; i < arryDispatch.Count; i++)
                    {
                        if (Convert.ToInt32(arryDispatch[i]["Index"].ToString()) == info.Index)
                        {
                            SaveMatchRecord(od.TakeDate, "E", info.Index, arryDispatch[i]["Detail"].ToString(), ex.Message);
                            break;
                        }
                    }
                    continue;
                }

            }
            return true;
        }

        public bool isCarFull(string Dno)
        {
            //撿車的 先撿查出車狀態
            DAL.DALDispatch dald = new DAL.DALDispatch(this._ConnectionString);
            ODispatchSheet od = dald.GetDispatchSheetBrief(Dno);
            if (od == null)
            {
                throw new Exception("派車單號異常");
            }
            else
            {
                if (od.ServiceCnt > 2)
                    return true;
                else
                    return false;
            }
        }

        public List<ODispatchSheet> GetDispatchByConditions(string data)
        {
            try
            {
                QDispatchCondStruct qs = JsonConvert.DeserializeObject<QDispatchCondStruct>(data);
                if (string.IsNullOrWhiteSpace(qs.QSDate) || string.IsNullOrWhiteSpace(qs.QEDate))
                {
                    throw new Exception("日期為必須條件");
                }
                if (qs.take < 0 || qs.skip < 0)
                {
                    throw new Exception("查詢範圍異常");
                }
                DAL.DALDispatch dald = new DAL.DALDispatch(this._ConnectionString);
                return dald.GetDispatchMultipleCondition(qs, qs.take, qs.skip);

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public bool UpdDispatchCarDriver(string data)
        {
            //data
            //dno,old fleet id,old car no,old employee id,new  fleet id,new car no,new employee id
            string Dno, OldCarNo, NewCarNo, NewFleetID, OldFleetID;
            int OldEmpID, NewEmpID;
            string[] parm = data.Split(',');
            try
            {
                Dno = parm[0];
                OldFleetID = parm[1];
                OldCarNo = parm[2];
                OldEmpID = Int32.Parse(parm[3]);
                NewFleetID = parm[4];
                NewCarNo = parm[5];
                NewEmpID = Int32.Parse(parm[6]);
            }
            catch (Exception)
            {
                throw new Exception("Data Error");
            }
            string CarType = string.Empty;

            DAL.DALDispatch dald = new DAL.DALDispatch(this._ConnectionString);
            try
            {
                return dald.UpdDispatchCarDriver(Dno, OldFleetID, OldCarNo, OldEmpID, NewFleetID, NewCarNo, NewEmpID, CarType);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DispatchLocationsStruct GetDispatchLocationByNo(string Dno)
        {
            try
            {
                DAL.DALDispatch dald = new DAL.DALDispatch(this._ConnectionString);

                ODispatchSheet ods = dald.GetDispatchSheetBrief(Dno);
                if (ods == null)
                    return null;

                DispatchLocationsStruct dl = new DispatchLocationsStruct();
                dl.dno = Dno;
                dl.cnt = ods.ServiceCnt;
                dl.Service = dald.GetDispatchService(Dno);

                return dl;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


    }
    /// <summary>
    /// 其他BAL
    /// </summary>
    public class BALGeneral : CallcarBAL
    {
        public BALGeneral(string ConnectionString) : base(ConnectionString) { }

        public OHoliday GetHoliday(DateTime inDate)
        {
            try
            {
                DAL.DALGeneral dal = new DAL.DALGeneral(this._ConnectionString);
                return dal.GetHoliday(inDate.ToString("yyyyMMdd"));
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public string CalculateTrailFee(string data, string token)
        {
            try
            {
                string iv = "8417728284177282";
                Common.AESCryptography aes = new Common.AESCryptography(token, iv);
                string plantext = aes.Decrypt(data);

                int CarFee = 0, NightFee = 0, BagFee = 0;
                int PromotFee = 0;

                APITrailFee f = JsonConvert.DeserializeObject<APITrailFee>(plantext);
                List<OFare> FareTable = new List<OFare>();
                DAL.DALGeneral dal = new DAL.DALGeneral(this._ConnectionString);

                FareTable = dal.GetFareTable(f.od);
                if (FareTable.Count > 0)
                {
                    //取得車資 ChargeType = 1
                    int index = -1;
                    index = FareTable.FindIndex(x => x.ChargeType == "1" && x.MinQty == f.pc);
                    if (index == -1)
                        throw new Exception("無此服務費率表");
                    else
                        CarFee = FareTable[index].Amount;

                    //取得夜加  ChargeType = 2                    
                    if (Int32.Parse(f.tt) > 2200 || Int32.Parse(f.tt) < 600)
                    {

                        index = FareTable.FindIndex(x => x.ChargeType == "2");
                        if (index == -1)
                            throw new Exception("無此服務費率表");
                        else
                            NightFee = FareTable[index].Amount * f.pc;
                    }
                    else
                        NightFee = 0;
                    //行李加費  ChargeType = 3

                    if (f.bc - f.pc > 0)
                    {
                        int BagCnt = f.bc - f.pc;
                        index = FareTable.FindIndex(x => x.ChargeType == "3");
                        BagFee = FareTable[index].Amount * f.pc;
                    }
                    else
                        BagFee = 0;

                    //TODO: 優惠計算
                    DateTime TakeDate = Common.General.ConvertToDateTime(f.td, 8);
                    BAL.BALGeneral bal = new BAL.BALGeneral(this._ConnectionString);

                    //Callcar.CObject.OHoliday h = bal.GetHoliday(TakeDate);
                    //if (h == null)
                    //{
                    //    PromotFee = 0;
                    //    //TODO:計算優惠金額
                    //}
                    //else
                    //{
                    //    //特殊連假不計算優惠
                    //    PromotFee = 0;
                    //}

                    PromotFee = 0;
                    return CarFee.ToString() + "_" + NightFee.ToString() + "_" + BagFee.ToString() + "_" + PromotFee.ToString();
                }
                else
                    throw new Exception("無此服務費率表");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public static List<string> GetTimeRange(string data, string token)
        {
            string iv = "8417728284177282";
            Common.AESCryptography aes = new Common.AESCryptography(token, iv);
            string plantext = aes.Decrypt(data);

            string[] param = plantext.Split(',');
            string ServiceType = param[0];
            string TakeDate = param[1];
            string FlightDate = param[2];
            string FlightTime = param[3];
            string SelectionType = param[4];
            DateTime dFlightTime;
            List<string> List = new List<string>();
            int NowHour;

            DateTime NowTime = DateTime.Now;

            DateTime dTakeDate = Common.General.ConvertToDateTime(TakeDate, 8);


            if (dTakeDate.Date < NowTime.Date)
                throw new Exception("搭乘日期不可小於今日");

            if (dTakeDate.Date == NowTime.Date)
                NowHour = DateTime.Now.Hour;
            else
                NowHour = 0;

            if (SelectionType == "F")
            {
                dFlightTime = Common.General.ConvertToDateTime(FlightDate + FlightTime, 12);

                if (FlightDate == null || FlightDate == "" || FlightTime == null || FlightTime == "")
                    throw new Exception("搭乘日期時間不可為空");

                if (dFlightTime.Date < dTakeDate.Date && SelectionType == "O")
                    throw new Exception("搭乘日期不可大於航班日期");
                else if (dFlightTime.Date > dTakeDate.Date && SelectionType == "I")
                    throw new Exception("搭乘日期不可小於航班日期");

                for (int i = NowHour; i < 24; i++)
                {
                    string Hour = i.ToString().PadLeft(2, '0') + "00";
                    DateTime dTakeTime = Common.General.ConvertToDateTime(TakeDate + Hour, 12);

                    double TimeDiff;
                    if (ServiceType == "I")
                    {
                        TimeDiff = new TimeSpan(dTakeTime.Ticks - dFlightTime.Ticks).TotalMinutes;
                        if (TimeDiff > 90)
                            List.Add(Hour);
                    }
                    else if (ServiceType == "O")
                    {
                        TimeDiff = new TimeSpan(dFlightTime.Ticks - dTakeTime.Ticks).TotalHours;
                        if (TimeDiff > 3)
                            List.Add(Hour);
                    }
                    else
                        throw new Exception("ServiceType Data Error");
                }

            }
            else
            {
                if (dTakeDate.Date < NowTime.Date)
                    throw new Exception("搭乘日期不可小於今日");

                if (Int32.Parse(TakeDate) > Int32.Parse(DateTime.Now.ToString("yyyyMMdd")))
                {
                    for (int i = 0; i < 24; i++)
                        List.Add(i.ToString().PadLeft(2, '0') + "00");
                }
                else
                {
                    if (NowHour + 3 >= 24)
                        return null;

                    for (int i = NowHour + 3; i < 24; i++)
                    {
                        List.Add(i.ToString().PadLeft(2, '0') + "00");
                    }
                }

            }

            return List;
        }

        public int GetMinServicePassenger(string data, string token)
        {
            string iv = "8417728284177282";
            Common.AESCryptography aes = new Common.AESCryptography(token, iv);
            string plantext = aes.Decrypt(data);

            string CityName = plantext.Split(',')[0];
            string DistinctName = plantext.Split(',')[1];
            string VillageName = plantext.Split(',')[2];
            DAL.DALGeneral dal = new DAL.DALGeneral(this._ConnectionString);
            string Status;
            try
            {
                Status = dal.GetDistinctServiceStatus(CityName, DistinctName);
            }
            catch (Exception ex)
            {
                throw new Exception
                (ex.Message);
            }
            if (Status != null)
            {
                if (Status == "1")
                    return 1;
                else if (Status == "X")
                    return -1;
            }
            else
                throw new Exception("無定義之區域");
            try
            {
                Status = dal.GetVillageServiceStatus(CityName, DistinctName, VillageName);
            }
            catch (Exception ex)
            {
                throw new Exception
                (ex.Message);
            }
            if (Status != null)
            {
                string VillStatus = Status.Split(',')[0];
                int Cnt = Int32.Parse(Status.Split(',')[1]);
                if (VillStatus == "1")
                    return 1;
                else if (VillStatus == "X")
                    return -1;
                else
                    return Cnt;
            }
            else
                throw new Exception("無定義之區域");
        }

        public List<OFleet> GetFleets()
        {
            DAL.DALGeneral dal = new DAL.DALGeneral(this._ConnectionString);
            try
            {
                return dal.GetFleetList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public OFleet GetFleet(int FleetID)
        {
            DAL.DALGeneral dal = new DAL.DALGeneral(this._ConnectionString);
            try
            {
                return dal.GetFleet(FleetID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public OFleet GetFleet(string carteam)
        {
            DAL.DALGeneral dal = new DAL.DALGeneral(this._ConnectionString);
            try
            {
                return dal.GetFleet(carteam);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public List<OCar> GetCars(string fid)
        {
            DAL.DALGeneral dal = new DAL.DALGeneral(this._ConnectionString);
            try
            {
                return dal.GetCarsByFleet(fid);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<OEmployee> GetDrivers(string fid)
        {
            DAL.DALGeneral dal = new DAL.DALGeneral(this._ConnectionString);
            try
            {
                List<OEmployee> DriverList = new List<OEmployee>();
                return DriverList;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}