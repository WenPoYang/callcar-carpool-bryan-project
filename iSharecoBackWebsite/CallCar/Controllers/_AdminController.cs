﻿using System;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;

namespace CallCar.Controllers
{

    public class _AdminController : _BaseController
    {

        protected int maxItemPerPage = 100;


        public _AdminController()
        {

        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {

            //is login
            if (!isLogin())
            {
                filterContext.Result = RedirectToAction("Index", "Login");
                return;
            }

            ViewBag.headerUserName = getSession("userName");
            ViewBag.UserRole = getSession("userRole");
            ViewBag.UserEmail = getSession("userEmail");

        }


        public bool isLogin()
        {

            bool? isLogin = false;
            try
            {
                isLogin = getSession("isLogin");
                if (isLogin == null)
                {
                    isLogin = false;
                }

            }
            catch (Exception e)
            {
                e.ToString();
            }

            return isLogin.Value;
        }
    }

}