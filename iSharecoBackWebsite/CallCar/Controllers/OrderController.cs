﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using CallCar.Models;
using CallCar.BAL;
using System.Configuration;
using System.Data;
using System.IO;
using System.Net;
using System.Net.Mime;
using System.Security.Cryptography;
using System.Text;


namespace CallCar.Controllers
{
    public class OrderController : _AdminController
    {
        public ActionResult Index()
        {

            return View();
        }

        public static string ConnectionString = ConfigurationManager.ConnectionStrings["CarpoolLab"].ConnectionString;
        public ActionResult Item(string id)
        {


            //string dataString = Helper.jsonEncode(parameters);

            // http://apid.ishareco.com/bapi/gOrder?no=AF00018517


            /*string url = Constant.apiBaseUrl + "gOrder?no=" + id;

            string responseText = sendHttpRequest(url);

            dynamic jsonData = Helper.jsonDecode(responseText);
            string GetOrderByNoResult = jsonData.GetOrderByNoResult;
            dynamic jsonData2 = Helper.jsonDecode(GetOrderByNoResult);

            string Status = jsonData2.Status;
            JObject RtnObject = jsonData2.RtnObject;*/

            DAL.DALReservation dalr = new DAL.DALReservation(ConnectionString);
            OReservation R;
            R = dalr.GetReservation(id);
            dynamic search = JObject.Parse(R.Credit);
            string IDMD5;
            IDMD5 = Encrypt(search.UID.Value.ToString());
            string cardno = Decrypt(search.CNo.Value.ToString(), IDMD5.Substring(0, 16), IDMD5.Substring(16, 16));
            string card4 = cardno.Substring(0, 4);
            string card12 = cardno.Substring(12, 4);
            string mmcard = card4 + "-********-" + card12;
            Dictionary<string, dynamic> data = new Dictionary<string, dynamic>();
            DAL.DALUserAccount dalu = new DAL.DALUserAccount(ConnectionString);
            OAccount A;
            A = dalu.GetAccountInfo(R.UserID);
            OReservation_process_log Rpl;
            data.Add("rno", R.ReservationNo.ToString()); // 01E9541", 訂單編號
            data.Add("uid", R.UserID.ToString()); // ,  會員編號
            data.Add("uname", A.FullName.ToString()); // ,  會員編號
            data.Add("uphone", A.MobilePhone.ToString()); // ,  會員編號
            data.Add("uemail", A.Email.ToString()); // ,  會員編號
            data.Add("sertype", R.ServiceType.ToString()); // ,  出國O,回國 I
            data.Add("tdate", R.ServeDate); // 170207", 乘車日期
            data.Add("timeseg", R.PreferTime); // 00", 預約時段
            data.Add("seltype", R.ChooseType); // , 預約方式 指定航班 F, 指定航廈 T
            data.Add("pcartype", R.PickCartype); // , 訂單種類 預約0 , 臨時預約 1
            data.Add("fno", R.FlightNo); // 26", 航班編號
            data.Add("fdatetime", R.FlightTime.ToString("yyyy/MM/dd HH:mm").ToString()); // 17/02/07 23:40", 航班時間
            data.Add("pcity", R.PickupCity); // 市",  上車城市
            data.Add("pdistinct", R.PickupDistinct.ToString()); // 區", 上車區里
            data.Add("pvillage", R.PickupVillage.ToString()); //  上車區里
            data.Add("paddress", R.PickupAddress.ToString()); // 北路一段305號", 上車地址
            data.Add("plat", R.PickupGPS.GetLat().ToString()); // 182815, 上車緯度
            data.Add("plng", R.PickupGPS.GetLng().ToString()); // .444322, 上車經度
            data.Add("tcity", R.TakeoffCity.ToString()); // 市", 下車城市
            data.Add("tdistinct", R.TakeoffDistinct.ToString()); // 區", 下車區里
            data.Add("tvillage", R.TakeoffVillage.ToString()); //  下車區里
            data.Add("taddress", R.TakeoffAddress.ToString()); // 國際機場 第二航廈", 下車地址
            data.Add("tlat", R.TakeoffGPS.GetLat().ToString()); // 076821, 下車緯度
            data.Add("tlng", R.TakeoffGPS.GetLng().ToString()); // .231683, 下車經度
            data.Add("pcnt", R.PassengerCnt.ToString()); // 乘客人數
            data.Add("bcnt", R.BaggageCnt.ToString()); // 行李數量
            data.Add("max", R.MaxFlag.ToString()); // , 是否滿車
            data.Add("trialprice", R.TrailPrice.ToString()); // 試算金額
            data.Add("coupun", R.Coupon.ToString()); //  優惠內容
            data.Add("invoice", R.Invoice.ToString()); //  發票資料
            data.Add("credit", mmcard.ToString()); //  信用卡資料
                                                   // 0：新增 X：取消 A：已媒合 B：媒合中 C：帳務處理中 Z：已結帳
            if (R.ProcessStage.ToString() == "0")
                data.Add("stage", "新增"); // , 狀態
            else if (R.ProcessStage.ToString() == "X")
                data.Add("stage", "取消"); // , 狀態
            else if (R.ProcessStage.ToString() == "A")
                data.Add("stage", "已媒合"); // , 狀態
            else if (R.ProcessStage.ToString() == "B")
                data.Add("stage", "媒合中"); // , 狀態
            else if (R.ProcessStage.ToString() == "C")
                data.Add("stage", "帳務處理中"); // , 狀態
            else if (R.ProcessStage.ToString() == "Z")
                data.Add("stage", "已結帳"); // , 狀態
            else if (R.ProcessStage.ToString() == "Y")
            {
                Rpl = dalr.GetReservation_process_log(id);
                data.Add("Memo", Rpl.Memo.ToString());
                data.Add("stage", "計價&取消"); // , 狀態
            }
            else if (R.ProcessStage.ToString() == "W")
            {
                Rpl = dalr.GetReservation_process_log(id);
                data.Add("Memo", Rpl.Memo.ToString());
                data.Add("stage", "不計價取消"); // , 狀態
            }
            else
                data.Add("stage", R.ProcessStage.ToString()); // , 狀態
            data.Add("ein", R.EIN.ToString()); //  發票號碼
            data.Add("cdatetime", R.OrderTime.ToString("yyyy/MM/dd HH:mm").ToString()); // 17/02/01 00:05" 建立時間
            data.Add("udatetime", R.udpTime.ToString("yyyy/MM/dd HH:mm").ToString()); // 17/02/01 00:05" 建立時間
            data.Add("pn", R.PassengerName.ToString()); // 17/02/01 00:05" 建立時間
            data.Add("pp", R.PassengerPhone.ToString()); // 17/02/01 00:05" 建立時間



            string dataText = "";

            dataText = Helper.jsonEncode(data);

            ViewBag.dataText = dataText;


            return View();

        }

        public JsonResult ItemCancel(string id, string memo)
        {
            string Source = "B";
            string data = id;
            string token = "";
            Dictionary<string, dynamic> JsonResult = new Dictionary<string, dynamic>();
            BALReservation balr = new BALReservation(ConnectionString);
            OReservation or;
            string Rno = string.Empty;

            if (Source == "U")
            {
                string iv = "8417728284177282";
                Common.AESCryptography aes = new Common.AESCryptography(token, iv);
                Rno = aes.Decrypt(data);
            }
            else
                Rno = data;

            try
            {
                or = balr.GetReservationByNo(Rno);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            DAL.DALReservation dalr = new DAL.DALReservation(ConnectionString);

            DateTime NowTime = DateTime.Now;
            bool rtn = false;

            if (or == null)
            {
                JsonResult.Add("Message", "XXX");
                return Json(JsonResult);
            }
            if (or.ProcessStage == "X" || or.ProcessStage == "W" || or.ProcessStage == "Y" || or.ProcessStage == "Z")
            {
                JsonResult.Add("Message", "不可取消");
                return Json(JsonResult);
            }
            else if (or.ProcessStage == "A")
            {
                if (Source == "U")
                {
                    DateTime TakeDatetime = Common.General.ConvertToDateTime(or.ServeDate + or.PreferTime, 12);

                    double diffTime = new TimeSpan(TakeDatetime.Ticks - NowTime.Ticks).TotalHours;
                    if (diffTime < 24)
                    {
                        JsonResult.Add("Message", "998");
                        return Json(JsonResult);
                    }
                }

                try
                {
                    rtn = dalr.CancelReservation(Rno, or.ProcessStage);
                    string neID = getSession("userID").ToString();
                    dalr.AddReservation_process_log(Rno, "W", memo, neID);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }


            }
            else if (or.ProcessStage == "0")
            {
                try
                {
                    rtn = dalr.CancelReservation(Rno, or.ProcessStage);
                    string neID = getSession("userID").ToString();
                    dalr.AddReservation_process_log(Rno, "W", memo, neID);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }

            }
            else
            {
                JsonResult.Add("Message", "XXX");
                return Json(JsonResult);
            }

            if (rtn)
            {
                JsonResult.Add("Message", "Success");
                return Json(JsonResult);
            }
            else
            {
                JsonResult.Add("Message", "Failure");
                return Json(JsonResult);
            }

        }

        public JsonResult ItemCancel2(string id, string memo)
        {
            string Source = "B";
            string data = id;
            string token = "";
            Dictionary<string, dynamic> JsonResult = new Dictionary<string, dynamic>();
            BALReservation balr = new BALReservation(ConnectionString);
            OReservation or;
            string Rno = string.Empty;

            if (Source == "U")
            {
                string iv = "8417728284177282";
                Common.AESCryptography aes = new Common.AESCryptography(token, iv);
                Rno = aes.Decrypt(data);
            }
            else
                Rno = data;

            try
            {
                or = balr.GetReservationByNo(Rno);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            DAL.DALReservation dalr = new DAL.DALReservation(ConnectionString);

            DateTime NowTime = DateTime.Now;
            bool rtn = false;

            if (or == null)
            {
                JsonResult.Add("Message", "XXX");
                return Json(JsonResult);
            }
            if (or.ProcessStage == "X" || or.ProcessStage == "W" || or.ProcessStage == "Y" || or.ProcessStage == "Z")
            {
                JsonResult.Add("Message", "不可取消");
                return Json(JsonResult);
            }
            else if (or.ProcessStage == "A")
            {
                if (Source == "U")
                {
                    DateTime TakeDatetime = Common.General.ConvertToDateTime(or.ServeDate + or.PreferTime, 12);

                    double diffTime = new TimeSpan(TakeDatetime.Ticks - NowTime.Ticks).TotalHours;
                    if (diffTime < 24)
                    {
                        JsonResult.Add("Message", "998");
                        return Json(JsonResult);
                    }
                }

                try
                {
                    string neID = getSession("userID").ToString();
                    dalr.AddReservation_process_log(Rno, "Y", memo, neID);
                    rtn = dalr.CancelReservation2(Rno, or.ProcessStage);

                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }


            }
            else if (or.ProcessStage == "0")
            {
                try
                {
                    string neID = getSession("userID").ToString();
                    dalr.AddReservation_process_log(Rno, "Y", memo, neID);
                    rtn = dalr.CancelReservation(Rno, or.ProcessStage);

                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }

            }
            else
            {
                JsonResult.Add("Message", "XXX");
                return Json(JsonResult);
            }

            if (rtn)
            {
                JsonResult.Add("Message", "Success");
                return Json(JsonResult);
            }
            else
            {
                JsonResult.Add("Message", "Failure");
                return Json(JsonResult);
            }

        }

        [HttpPost]
        //public JsonResult GetList(int page = 1, int itemPerPage = 20)
        public JsonResult GetList(int page, string ProcessStage, string CarpoolFlag,string searchJson = null, string orderField = "", string orderType = "", int itemPerPage = 10)
        {
            if (itemPerPage > 100)
            {
                itemPerPage = 100;
            }

            Dictionary<string, dynamic> JsonResult = new Dictionary<string, dynamic>();

            try
            {

                string QSDate = "";
                string QEDate = "";
                string OrderNo = "";
                string MobilePhone = "";
                string PMobilePhone = "";
                string UserID = "0";

                if (searchJson != null)
                {
                    try
                    {

                        dynamic search = JObject.Parse(searchJson);

                        QSDate = search.QSDate;
                        QEDate = search.QEDate;
                        OrderNo = search.OrderNo;
                        UserID = search.UserID;
                        MobilePhone = search.MobilePhone;
                        PMobilePhone = search.PMobilePhone;
                        if (search.UserID == "")
                        {
                            UserID = "0";
                        }
                        else
                        {
                            UserID = search.UserID;
                        }

                        //CarNo = search.CarNo;

                    }
                    catch (Exception e)
                    {
                        e.ToString();
                    }

                }

                Dictionary<string, dynamic> parameters = new Dictionary<string, dynamic>();

                /*
                parameters.Add("QSDate", "20170101");//查詢派車的開始日期
                parameters.Add("QEDate", "20170102");//查詢派車的結束日期
                parameters.Add("DispatchNo", "20170126578");//指定派車單號查詢
                parameters.Add("OrderNo", "20170156878");//指定訂單編號查詢
                parameters.Add("DriverID", 123456);//指定司機查詢
                parameters.Add("CarNo", "RAM-0158");//指定車號查詢
                */

                parameters.Add("QSDate", QSDate); //:"20170101",//查詢派車的開始日期
                parameters.Add("QEDate", QEDate); //:"20170102",//查詢派車的結束日期
                parameters.Add("OrderNo", OrderNo); //:"20170156878",//指定訂單編號查詢
                parameters.Add("UserID", UserID); //:123456,//指定會員編號查詢
                parameters.Add("MobilePhone", MobilePhone); //:"0912XXXXXX"//指定手機號碼查詢
                parameters.Add("PMobilePhone", PMobilePhone);

                string dataString = Helper.jsonEncode(parameters);

                /*string url = Constant.apiBaseUrl + "gOrders";


                string responseText = sendPOST(url, dataString);

                dynamic jsonData = Helper.jsonDecode(responseText);

                string Status = jsonData.Status;
                var RtnObject = jsonData.RtnObject;*/

                DAL.DALReservation dalr = new DAL.DALReservation(ConnectionString);
                QOrderCondStruct qs = JsonConvert.DeserializeObject<QOrderCondStruct>(dataString);
                List<OReservation> List = dalr.GetReservationsMultipleCondition(qs, qs.take, qs.skip, ProcessStage, CarpoolFlag);

                int countSkip = itemPerPage * (page - 1);
                int countTake = itemPerPage;

                int TotalItem = 0;

                //RepStarDateTime = "2016-12-20";
                //RepEndDateTime = "2017-01-04";

                List<dynamic> data = new List<dynamic>();
                //var items = List;


                if (List != null)
                {

                    foreach (OReservation or in List)
                    {
                        TotalItem++;
                        DAL.DALUserAccount gg = new DAL.DALUserAccount(ConnectionString);
                        OAccount ggll = gg.GetID(or.UserID.ToString());
                        Dictionary<string, dynamic> a = new Dictionary<string, dynamic>();

                        a.Add("rno", or.ReservationNo.ToString()); //: "CR01E9541", 訂單編號
                        //a.Add("uid", or.UserID.ToString()); //: 100,  會員編號
                        a.Add("poolflag", or.CarpoolFlag.ToString()); // 專接共乘
                        a.Add("passname", or.PassengerName.ToString());
                        a.Add("uidname", ggll.LName.ToString() + ggll.FName.ToString()); //: 100,  會員編號
                        a.Add("sertype", or.ServiceType.ToString()); //: "O",  出國O,回國 I
                        if (or.ServeDate.ToString() != "")
                            a.Add("tdate", or.ServeDate.Substring(0, 4) + "-" + or.ServeDate.Substring(4, 2) + "-" + or.ServeDate.Substring(6, 2));
                        else
                            a.Add("tdate", or.ServeDate.ToString()); //: "20170207", 乘車日期

                        if (or.PreferTime.ToString() != "")
                            a.Add("timeseg", or.PreferTime.Substring(0, 2) + ":" + or.PreferTime.Substring(2, 2));
                        else
                            a.Add("timeseg", or.PreferTime.ToString()); //: "2000", 預約時段
                        a.Add("Stimeseg", or.ScheduleServeTime.ToString()); //
                        a.Add("pcity", or.PickupCity.ToString()); //: "新北市",  上車城市
                        a.Add("pdistinct", or.PickupDistinct.ToString()); //: "淡水區", 上車區里
                        a.Add("paddress", or.PickupAddress.ToString()); //: "中山北路一段305號", 上車地址
                        a.Add("plat", or.PickupGPS.GetLat().ToString()); //: 25.182815, 上車緯度
                        a.Add("plng", or.PickupGPS.GetLng().ToString()); //: 121.444322, 上車經度
                        a.Add("tcity", or.TakeoffCity.ToString()); //: "桃園市", 下車城市
                        a.Add("tdistinct", or.TakeoffDistinct.ToString()); //: "大園區", 下車區里
                        a.Add("taddress", or.TakeoffAddress.ToString()); //: "桃園國際機場 第二航廈", 下車地址
                        a.Add("tlat", or.TakeoffGPS.GetLat().ToString()); //: 25.076821, 下車緯度
                        a.Add("tlng", or.TakeoffGPS.GetLng().ToString()); //: 121.231683, 下車經度
                        a.Add("pcnt", or.PassengerCnt.ToString()); //: 1, 乘客人數
                        a.Add("bcnt", or.BaggageCnt.ToString()); //: 1, 行李數量
                        a.Add("stage", or.ProcessStage.ToString()); //: "0", 狀態
                        a.Add("cdatetime", or.OrderTime.ToString("yyyy/MM/dd HH:mm").ToString()); //: "2017/02/01 00:05" 建立時間


                        data.Add(a);

                    }

                }

                JsonResult.Add("data", data);

                double PageTotal = (double)TotalItem / itemPerPage;
                PageTotal = Math.Ceiling(PageTotal);

                //PageTotal = 1;

                JsonResult.Add("totalItem", TotalItem);
                JsonResult.Add("pageTotal", PageTotal);
                JsonResult.Add("nowpage", page);
            }
            catch (Exception e)
            {
                e.ToString();
            }


            return Json(JsonResult);

        }

        public static string Encrypt(string PlanText)
        {
            byte[] bytes = Encoding.Default.GetBytes(PlanText);
            byte[] bytes2 = Encoding.Default.GetBytes("callcar");
            byte[] array = new byte[bytes.Length + bytes2.Length];
            bytes.CopyTo(array, 0);
            bytes2.CopyTo(array, bytes.Length);
            MD5 mD = MD5.Create();
            byte[] array2 = mD.ComputeHash(array);
            byte[] array3 = new byte[array2.Length + bytes2.Length];
            array2.CopyTo(array3, 0);
            bytes2.CopyTo(array3, array2.Length);
            return Convert.ToBase64String(array3);
        }

        public string Decrypt(string CipherText, string key, string iv)
        {
            byte[] bytes = Encoding.UTF8.GetBytes(key);
            byte[] bytes2 = Encoding.UTF8.GetBytes(iv);
            byte[] cipherText = Convert.FromBase64String(CipherText);
            return DecryptStringFromBytes(cipherText, bytes, bytes2);
        }

        private static string DecryptStringFromBytes(byte[] cipherText, byte[] key, byte[] iv)
        {
            if (cipherText == null || cipherText.Length == 0)
            {
                throw new ArgumentNullException("cipherText");
            }
            if (key == null || key.Length == 0)
            {
                throw new ArgumentNullException("key");
            }
            if (iv == null || iv.Length == 0)
            {
                throw new ArgumentNullException("key");
            }
            string result = null;
            using (RijndaelManaged rijndaelManaged = new RijndaelManaged())
            {
                rijndaelManaged.Mode = CipherMode.CBC;
                rijndaelManaged.Padding = PaddingMode.PKCS7;
                rijndaelManaged.FeedbackSize = 128;
                rijndaelManaged.Key = key;
                rijndaelManaged.IV = iv;
                ICryptoTransform transform = rijndaelManaged.CreateDecryptor(rijndaelManaged.Key, rijndaelManaged.IV);
                try
                {
                    using (MemoryStream memoryStream = new MemoryStream(cipherText))
                    {
                        using (CryptoStream cryptoStream = new CryptoStream(memoryStream, transform, CryptoStreamMode.Read))
                        {
                            using (StreamReader streamReader = new StreamReader(cryptoStream))
                            {
                                result = streamReader.ReadToEnd();
                            }
                        }
                    }
                }
                catch (Exception)
                {
                    result = "keyError";
                }
            }
            return result;
        }

    }
}