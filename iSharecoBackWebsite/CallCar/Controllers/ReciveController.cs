﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using CallCar.Models;
using CallCar.BAL;
using System.Configuration;
using ClosedXML.Excel;
using System.IO;
using System.Net;
using System.Net.Mime;
using System.Text;
using System.Security.Cryptography;
using System.Data;
namespace CallCar.Controllers
{
    public class ReciveController : Controller
    {
        // GET: Recive
        public ActionResult Index()
        {
            return View();
        }
        public static string ConnectionString = ConfigurationManager.ConnectionStrings["CarpoolLab"].ConnectionString;
        public JsonResult GetList(int page,string searchJson = null,  string orderField = "", string orderType = "", int itemPerPage = 10)
        {

            if (itemPerPage > 100)
            {
                itemPerPage = 100;
            }
            string QSDate = "";
            string QEDate = "";
            string stage = "";
            string Carteam = "";

            if (searchJson != null)
            {
                try
                {

                    dynamic search = JObject.Parse(searchJson);

                    QSDate = search.QSDate;
                    QEDate = search.QEDate;

                }
                catch (Exception e)
                {
                    e.ToString();
                }

            }
            Dictionary<string, dynamic> JsonResult = new Dictionary<string, dynamic>();

            RtnStruct r = new RtnStruct();
            BALDispatch bal = new BALDispatch(ConnectionString);
            BALGeneral balg = new BALGeneral(ConnectionString);
            try
            {
                int fleetid = 0;
                string fstage = "";

                DAL.DALReservation dal = new DAL.DALReservation(ConnectionString);
                List<ORecev> GoRecev = dal.GetRecevs(QSDate, QEDate);
                List<dynamic> data = new List<dynamic>();
                int countSkip = itemPerPage * (page - 1);
                int countTake = itemPerPage;

                int TotalItem = 0;
                foreach (ORecev gnow in GoRecev)
                {
                    TotalItem++;
                    Dictionary<string, dynamic> a = new Dictionary<string, dynamic>();

                    a.Add("tdate", gnow.takedate.ToString());
                    a.Add("ttime", gnow.TimeSegment.ToString());
                    a.Add("ccSno", gnow.DispatchNo.ToString());
                    a.Add("sno", gnow.ReservationNo.ToString());
                    a.Add("Apct", gnow.APassengerCnt.ToString());
                    a.Add("pct", gnow.PassengerCnt.ToString());
                    a.Add("bct", gnow.BaggageCnt.ToString());
                    a.Add("abct", gnow.ABaggageCnt.ToString());
                    a.Add("cpn", gnow.Coupon.ToString());
                    a.Add("pr", gnow.Price.ToString());
                    a.Add("realpr", gnow.realamt.ToString());
                    a.Add("stage", gnow.ChargeResult.ToString());
                    a.Add("cancelT", gnow.UpdTime.ToString());
                    a.Add("memo", gnow.Memo.ToString());
                    data.Add(a);
                }



                JsonResult.Add("data", data);


                double PageTotal = (double)TotalItem / itemPerPage;
                PageTotal = Math.Ceiling(PageTotal);

                JsonResult.Add("totalItem", TotalItem);
                JsonResult.Add("pageTotal", PageTotal);
                JsonResult.Add("nowpage", page);

            }
            catch (Exception e)
            {
                e.ToString();
            }
            return Json(JsonResult);
        }

        [HttpPost]
        public HttpStatusCodeResult CreateExcel(string searchJson)
        {
            XLWorkbook wb = new XLWorkbook(XLEventTracking.Disabled); //create Excel

            string QSDate = "";
            string QEDate = "";
            string Stage = "";
            string Carteam = "";

            if (searchJson != null)
            {
                try
                {

                    dynamic search = JObject.Parse(searchJson);

                    QSDate = search.QSDate;
                    QEDate = search.QEDate;

                }
                catch (Exception e)
                {
                    e.ToString();
                }

            }
            Dictionary<string, dynamic> JsonResult = new Dictionary<string, dynamic>();
            DataTable dt = null;
            RtnStruct r = new RtnStruct();
            BALDispatch bal = new BALDispatch(ConnectionString);
            BALGeneral balg = new BALGeneral(ConnectionString);
            try
            {
                int fleetid = 0;
                string fstage = "";

                DAL.DALReservation dal = new DAL.DALReservation(ConnectionString);
                List<ORecev> GoRecev = dal.GetRecevs(QSDate, QEDate);
                List<dynamic> data = new List<dynamic>();

                dt = new DataTable();
                dt.Columns.Add("乘車日期");
                dt.Columns.Add("乘車時間");
                dt.Columns.Add("CallCar單號");
                dt.Columns.Add("艾雪單號");
                dt.Columns.Add("總乘車人數");
                dt.Columns.Add("乘車人數");
                dt.Columns.Add("行李件數");
                dt.Columns.Add("臨加行李");
                dt.Columns.Add("優惠碼");
                dt.Columns.Add("小計");
                dt.Columns.Add("實收金額");
                dt.Columns.Add("刷卡結果");
                dt.Columns.Add("取消時間");
                dt.Columns.Add("備註");

                foreach (ORecev gnow in GoRecev)
                {
                    DataRow dataRow = dt.NewRow();
                    dataRow["乘車日期"] = gnow.takedate.ToString();
                    dataRow["乘車時間"] = gnow.TimeSegment.ToString();
                    dataRow["CallCar單號"] = gnow.DispatchNo.ToString();
                    dataRow["艾雪單號"] = gnow.ReservationNo.ToString();
                    dataRow["總乘車人數"] = gnow.APassengerCnt.ToString();
                    dataRow["乘車人數"] = gnow.PassengerCnt.ToString();
                    dataRow["行李件數"] = gnow.BaggageCnt.ToString();
                    dataRow["臨加行李"] = gnow.ABaggageCnt.ToString();
                    dataRow["優惠碼"] = gnow.Coupon.ToString();
                    dataRow["小計"] = gnow.Price.ToString();
                    dataRow["實收金額"] = gnow.realamt.ToString();

                    if (gnow.ChargeResult.ToString() == "0")
                        dataRow["刷卡結果"] = "尚未刷卡";
                    else if (gnow.ChargeResult.ToString() == "1")
                        dataRow["刷卡結果"] = "刷卡成功";
                    else if (gnow.ChargeResult.ToString() == "X")
                        dataRow["刷卡結果"] = "刷卡失敗";
                    else if (gnow.ChargeResult.ToString() == "2")
                        dataRow["刷卡結果"] = "和相刷卡";
                    else
                        dataRow["刷卡結果"] = gnow.ChargeResult.ToString();
                   
                    dataRow["取消時間"] = gnow.UpdTime.ToString();
                    dataRow["備註"] = gnow.Memo.ToString();

                    dt.Rows.Add(dataRow);

                }


            }
            catch (Exception e)
            {
                e.ToString();
            }
            wb.AddWorksheet(dt, "Sheet1");
            if (wb != null)
            {
                Session["ExcelResult"] = wb;
                return new HttpStatusCodeResult(HttpStatusCode.OK);
            }

            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

        }

        public FileContentResult ExcelResult(string reportHeader) //it's your data passed to controller
        {

            byte[] fileBytes = GetExcel((XLWorkbook)Session["ExcelResult"]);
            var exportFileName = string.Concat(
                "Report2Recive_",
                DateTime.Now.ToString("yyyyMMdd"),
                ".xlsx");
            return File(fileBytes, MediaTypeNames.Application.Octet, exportFileName);
        }
        public static byte[] GetExcel(XLWorkbook wb)
        {
            using (var ms = new MemoryStream())
            {
                wb.SaveAs(ms);
                return ms.ToArray();
            }
        }
    }



}