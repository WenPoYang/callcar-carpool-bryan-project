﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using System.IO;
using System.Net;
using Newtonsoft.Json;
using CallCar.Models;

namespace CallCar.Common
{
    public class General
    {
        private const double EARTH_RADIUS = 6378.137;//地球半徑 
        private static double rad(double d)
        {
            return d * Math.PI / 180.0;
        }
        public static double GetDistance(double lat1, double lng1, double lat2, double lng2)
        {
            double radLat1 = rad(lat1);
            double radLat2 = rad(lat2);
            double a = radLat1 - radLat2;
            double b = rad(lng1) - rad(lng2);
            double s = 2 * Math.Asin(Math.Sqrt(Math.Pow(Math.Sin(a / 2), 2) +
            Math.Cos(radLat1) * Math.Cos(radLat2) * Math.Pow(Math.Sin(b / 2), 2)));
            s = s * EARTH_RADIUS;
            s = Math.Round(s * 10000) / 10000;
            return s; //單位公里
        }

        public static DateTime ConvertToDateTime(string input, int length)
        {
            if (length == 8)
                return DateTime.Parse(input.Substring(0, 4) + "/" + input.Substring(4, 2) + "/" + input.Substring(6, 2));
            else if (length == 12)
                return DateTime.Parse(input.Substring(0, 4) + "/" + input.Substring(4, 2) + "/" + input.Substring(6, 2) + " " + input.Substring(8, 2) + ":" + input.Substring(10, 2));
            else
                throw new Exception("Convert Length Error");

        }

        public static string OutputCrypt(string plantext, string key)
        {

            string iv = "8417728284177282";
            Common.AESCryptography aes = new Common.AESCryptography(key, iv);
            return aes.Encrypt(plantext);
        }
    }

    public class AESCryptography
    {
        public AESCryptography(string key, string iv)
        {

            _key = key;
            _iv = iv;


        }

        public string Decrypt(string CipherText)
        {
            byte[] Key, IV;
            Key = Encoding.UTF8.GetBytes(this._key);
            IV = Encoding.UTF8.GetBytes(this._iv);

            byte[] encrypted = Convert.FromBase64String(CipherText);
            return DecryptStringFromBytes(encrypted, Key, IV);
        }

        public string Encrypt(string PlanText)
        {
            byte[] Key, IV;
            Key = Encoding.UTF8.GetBytes(this._key);
            IV = Encoding.UTF8.GetBytes(this._iv);
            var EncryptString = EncryptStringToBytes(PlanText, Key, IV);
            return Convert.ToBase64String(EncryptString);
        }

        private static string DecryptStringFromBytes(byte[] cipherText, byte[] key, byte[] iv)
        {
            if (cipherText == null || cipherText.Length <= 0)
            {
                throw new ArgumentNullException("cipherText");
            }
            if (key == null || key.Length <= 0)
            {
                throw new ArgumentNullException("key");
            }
            if (iv == null || iv.Length <= 0)
            {
                throw new ArgumentNullException("key");
            }
            string plaintext = null;
            using (var rijAlg = new RijndaelManaged())
            {
                rijAlg.Mode = CipherMode.CBC;
                rijAlg.Padding = PaddingMode.PKCS7;
                rijAlg.FeedbackSize = 128;
                rijAlg.Key = key;
                rijAlg.IV = iv;
                var decryptor = rijAlg.CreateDecryptor(rijAlg.Key, rijAlg.IV);
                try
                {
                    using (var msDecrypt = new MemoryStream(cipherText))
                    {
                        using (var csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                        {
                            using (var srDecrypt = new StreamReader(csDecrypt))
                            {
                                plaintext = srDecrypt.ReadToEnd();
                            }
                        }
                    }
                }
                catch
                {
                    plaintext = "keyError";
                }
            }
            return plaintext;
        }
        private static byte[] EncryptStringToBytes(string plainText, byte[] key, byte[] iv)
        {
            if (plainText == null || plainText.Length <= 0)
            {
                throw new ArgumentNullException("plainText");
            }
            if (key == null || key.Length <= 0)
            {
                throw new ArgumentNullException("key");
            }
            if (iv == null || iv.Length <= 0)
            {
                throw new ArgumentNullException("key");
            }
            byte[] encrypted;
            using (var rijAlg = new RijndaelManaged())
            {
                rijAlg.Mode = CipherMode.CBC;
                rijAlg.Padding = PaddingMode.PKCS7;
                rijAlg.FeedbackSize = 128;
                rijAlg.Key = key;
                rijAlg.IV = iv;
                var encryptor = rijAlg.CreateEncryptor(rijAlg.Key, rijAlg.IV);
                using (var msEncrypt = new MemoryStream())
                {
                    using (var csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    {
                        using (var swEncrypt = new StreamWriter(csEncrypt))
                        {
                            swEncrypt.Write(plainText);
                        }
                        encrypted = msEncrypt.ToArray();
                    }
                }
            }
            return encrypted;
        }


        public static string TokenAnalysis(ref string data)
        {
            if (data.Length <= 16)
                throw new Exception("Data Error");

            string token = data.Substring(data.Length - 16, 16);

            data = data.Substring(0, data.Length - 16);
            return token;
        }

        private string _key { get; set; }
        private string _iv { get; set; }
    }

    public class MD5Cryptography
    {
        private const string salted = "callcar";

        public static string Encrypt(string PlanText)
        {
            byte[] Original = Encoding.Default.GetBytes(PlanText);//將來源字串轉為byte[] 
            byte[] SaltValue = Encoding.Default.GetBytes(salted);//將Salted Value轉為byte[] 
            byte[] ToSalt = new byte[Original.Length + SaltValue.Length]; //宣告新的byte[]來儲存加密後的值 
            Original.CopyTo(ToSalt, 0);//將來源字串複製到新byte[] 
            SaltValue.CopyTo(ToSalt, Original.Length);//將Salted Value複製到新byte[] 
            MD5 st = MD5.Create();//使用MD5 
            byte[] SaltPWD = st.ComputeHash(ToSalt);//進行加密 
            byte[] PWD = new byte[SaltPWD.Length + SaltValue.Length];//宣告新byte[]儲存加密及Salted的值 
            SaltPWD.CopyTo(PWD, 0);//將加密後的值複製到新byte[] 
            SaltValue.CopyTo(PWD, SaltPWD.Length);//將Salted Value複製到新byte[] 
            return Convert.ToBase64String(PWD);//顯示Salted Hash後的字串
        }
    }

    public class Pay2GoCardAuth
    {
        private const string HashKey = "ax9hhIc6W3oOCEn0GzxzMW7zwKKczmNJ";
        private const string HashIV = "UW1bFeiChGrTlOU1";
        private const string Pay2GoURL = @"https://ccore.spgateway.com/API/CreditCard";

        public Pay2GoCardAuth(string AuthOrderNo, int Amt, string ProdDesc, string Email, string CardNo, string ExpMonth, string CVCode)
        {
            this.Amt = Amt;
            this.CardNo = CardNo;
            this.CVC = CVCode;
            this.Exp = ExpMonth;
            this.MerchantOrderNo = AuthOrderNo;
            this.PayerEmail = Email;
            this.ProdDesc = ProdDesc;
            this.TokenLife = DateTime.Now.ToString("yyyyMMdd");
            this.TokenSwitch = "get";
            this.TokenTerm = "abc";
            this.Version = "1.0";
            this.AuthToken = "";
        }

        public string Authorize()
        {
            WebRequest tRequest;
            tRequest = WebRequest.Create(Pay2GoURL);
            tRequest.Method = "post";
            tRequest.ContentType = " application/x-www-form-urlencoded;charset=UTF-8";

            int TimeStamp = (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
            string postData = "MerchantID_=MS3446346&PostData_=" + EncryptAES256("TimeStamp=" + TimeStamp + "&Version=" + this.Version + "&MerchantOrderNo=" + this.MerchantOrderNo + "&Amt=" + this.Amt.ToString() + "&ProdDesc=" + this.ProdDesc + "&PayerEmail=" + this.PayerEmail + "&CardNo=" + this.CardNo + "&Exp=" + this.Exp + "&CVC=" + CVC + "&TokenSwitch=" + this.TokenSwitch + "&TokenTerm=" + this.TokenTerm, HashKey, HashIV) + "&Pos_=JSON";
            string tempData = "MerchantID_=MS3446346&PostData_=" + "TimeStamp=" + TimeStamp + "&Version=" + this.Version + "&MerchantOrderNo=" + this.MerchantOrderNo + "&Amt=" + this.Amt.ToString() + "&ProdDesc=" + this.ProdDesc + "&PayerEmail=" + this.PayerEmail + "&CardNo=" + this.CardNo + "&Exp=" + this.Exp + "&CVC=" + CVC + "&TokenSwitch=" + this.TokenSwitch + "&TokenTerm=" + this.TokenTerm + "&Pos_=JSON"; ;
            //Console.WriteLine(postData);
            Byte[] byteArray = Encoding.UTF8.GetBytes(postData);
            tRequest.ContentLength = byteArray.Length;

            Stream dataStream = tRequest.GetRequestStream();
            dataStream.Write(byteArray, 0, byteArray.Length);
            dataStream.Close();

            WebResponse tResponse = tRequest.GetResponse();

            dataStream = tResponse.GetResponseStream();

            StreamReader tReader = new StreamReader(dataStream);

            String sResponseFromServer = tReader.ReadToEnd();


            tReader.Close();
            dataStream.Close();
            tResponse.Close();

            Pay2goResponse pr = JsonConvert.DeserializeObject<Pay2goResponse>(sResponseFromServer);

            if (pr.Status == "SUCCESS")
            {
                this.AuthToken = pr.Result.TokenValue;
                return "Success";
            }
            else
            {
                this.AuthToken = "";
                return pr.Status + "(" + tempData + ")";
            }
        }

        public string GetToken()
        {
            return this.AuthToken;
        }
        public string GetTokenLife()
        {
            return this.TokenLife;
        }
        #region Private
        private string EncryptAES256(string source, string sSecretKey, string iv)//加密
        {
            byte[] sourceBytes = AddPKCS7Padding(Encoding.UTF8.GetBytes(source),
            32);
            var aes = new RijndaelManaged();
            aes.Key = Encoding.UTF8.GetBytes(sSecretKey);
            aes.IV = Encoding.UTF8.GetBytes(iv);
            aes.Mode = CipherMode.CBC;
            aes.Padding = PaddingMode.None;
            ICryptoTransform transform = aes.CreateEncryptor();
            return ByteArrayToHex(transform.TransformFinalBlock(sourceBytes, 0,
            sourceBytes.Length)).ToLower();
        }
        private string DecryptAES256(string encryptData)//解密
        {
            string sSecretKey = "ax9hhIc6W3oOCEn0GzxzMW7zwKKczmNJ";
            string iv = "UW1bFeiChGrTlOU1";
            var encryptBytes = HexStringToByteArray(encryptData.ToUpper());
            var aes = new RijndaelManaged();
            aes.Key = Encoding.UTF8.GetBytes(sSecretKey);
            aes.IV = Encoding.UTF8.GetBytes(iv);
            aes.Mode = CipherMode.CBC;
            aes.Padding = PaddingMode.None;
            ICryptoTransform transform = aes.CreateDecryptor();
            return Encoding.UTF8.GetString(RemovePKCS7Padding(transform.TransformFinalBlock(encryptBytes, 0, encryptBytes.Length)));
        }
        private static byte[] AddPKCS7Padding(byte[] data, int iBlockSize)
        {
            int iLength = data.Length;
            byte cPadding = (byte)(iBlockSize - (iLength % iBlockSize));
            var output = new byte[iLength + cPadding];
            Buffer.BlockCopy(data, 0, output, 0, iLength);
            for (var i = iLength; i < output.Length; i++)
                output[i] = (byte)cPadding;
            return output;
        }
        private static byte[] RemovePKCS7Padding(byte[] data)
        {
            int iLength = data[data.Length - 1];
            var output = new byte[data.Length - iLength];
            Buffer.BlockCopy(data, 0, output, 0, output.Length);
            return output;
        }
        private static string ByteArrayToHex(byte[] barray)
        {
            char[] c = new char[barray.Length * 2];
            byte b;
            for (int i = 0; i < barray.Length; ++i)
            {
                b = ((byte)(barray[i] >> 4));
                c[i * 2] = (char)(b > 9 ? b + 0x37 : b + 0x30);
                b = ((byte)(barray[i] & 0xF));
                c[i * 2 + 1] = (char)(b > 9 ? b + 0x37 : b + 0x30);
            }
            return new string(c);
        }
        private static byte[] HexStringToByteArray(string hexString)
        {
            int hexStringLength = hexString.Length;
            byte[] b = new byte[hexStringLength / 2];
            for (int i = 0; i < hexStringLength; i += 2)
            {
                int topChar = (hexString[i] > 0x40 ? hexString[i] - 0x37 : hexString[i] - 0x30)
                << 4;
                int bottomChar = hexString[i + 1] > 0x40 ? hexString[i + 1] - 0x37 :
                hexString[i + 1] - 0x30;
                b[i / 2] = Convert.ToByte(topChar + bottomChar);
            }
            return b;
        }
        #endregion

        private string Version { get; set; }  //固定 1.0
        private string MerchantOrderNo { get; set; } //商店訂單編號
        private int Amt { get; set; }           //金額
        private string ProdDesc { get; set; }      //產品說明
        private string PayerEmail { get; set; }    //消費者Email
        private string CardNo { get; set; }        //卡號
        private string Exp { get; set; }           //有效年月
        private string CVC { get; set; }           //授權碼
        private string TokenSwitch { get; set; } //固定 get
        private string TokenTerm { get; set; }     //自訂
        private string TokenLife { get; set; } //設定與信用卡有效年月相同
        private string AuthToken { get; set; } //取得的 token
    }
}