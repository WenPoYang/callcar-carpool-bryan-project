﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using CallCar.Models;
using CallCar.BAL;
using System.Configuration;

using System.IO;
using System.Net;
using System.Text;
using System.Security.Cryptography;

namespace CallCar.Controllers
{
    public class DashboardController : _AdminController
    {
        public static string ConnectionString = ConfigurationManager.ConnectionStrings["CarpoolLab"].ConnectionString;
        public ActionResult Index()
        {

            //string gg = ApiHelper.gDispatchs();

            //ViewBag.gg = gg;
            DAL.DALGeneral ddd = new DAL.DALGeneral(ConnectionString);
            ViewBag.Oc =  ddd.Ordercount();
            ViewBag.Dc = ddd.Discount();
            return View();
        }
        public JsonResult GetList(string sidx, string sord, int page, int rows)
        {
            string searchJson = null; int itemPerPage = 10;
            if (itemPerPage > 100)
            {
                itemPerPage = 100;
            }
            string QSDate = "";
            string QEDate = "";
            string stage = "";
            string Carteam = "";

            if (searchJson != null)
            {
                try
                {

                    dynamic search = JObject.Parse(searchJson);

                    QSDate = search.QSDate;
                    QEDate = search.QEDate;
                    stage = search.stage;
                    Carteam = search.Carteam;

                }
                catch (Exception e)
                {

                }

            }
            Dictionary<string, dynamic> JsonResult = new Dictionary<string, dynamic>();

            RtnStruct r = new RtnStruct();
            DAL.DALDispatch dal = new DAL.DALDispatch(ConnectionString);
            BALGeneral balg = new BALGeneral(ConnectionString);
            try
            {
                int fleetid = 0;
                string fstage = "";
                if (Carteam != "")
                    fleetid = balg.GetFleet(Carteam).FleetID;
                if (stage.IndexOf("一般") != -1)
                    fstage = "0";
                else if (stage.IndexOf("完成") != -1)
                    fstage = "C";
                else if (stage.IndexOf("計價") != -1)
                    fstage = "N";
                else if (stage.IndexOf("失敗") != -1)
                    fstage = "F";
                else
                    fstage = "";
                List<ODispatchSheet> List = dal.GetDispatchSheetForIndex();
                if (List == null)
                    r.RtnObject = null;
                else
                {
                    r.RtnObject = List;
                }
                r.Status = "Success";
            }
            catch (Exception e)
            {
                e.ToString();
            }
            dynamic jsonData = Helper.jsonDecode(JsonConvert.SerializeObject(r));
            string Status = jsonData.Status;
            var RtnObject = jsonData.RtnObject;

            int countSkip = itemPerPage * (page - 1);
            int countTake = itemPerPage;

            int TotalItem = 0;

            //RepStarDateTime = "2016-12-20";
            //RepEndDateTime = "2017-01-04";

            List<dynamic> data = new List<dynamic>();
            DAL.DALReservation ddalr = new DAL.DALReservation(ConnectionString);
            var items = RtnObject;

            if (items != null)
            {
                try
                {
                    foreach (JObject item in items)
                    {
                        TotalItem++;

                        Dictionary<string, dynamic> a = new Dictionary<string, dynamic>();
                        a.Add("dno", item.GetValue("DispatchNo").ToString()); //:"20170118", //搭乘日期
                        a.Add("cp", item.GetValue("CarType").ToString()); //:"20170118", //搭乘日期
                        if (item.GetValue("ServiceType").ToString() == "I")
                            a.Add("st", "回國"); //:"2017/01/18T09:25",//服務時間                                  
                        else
                            a.Add("st", "出國"); //:"2017/01/18T09:25",//服務時間                    
                        //a.Add("td", item.GetValue("TakeDate").ToString()); //:5,//全車人
                        if (item.GetValue("TakeDate").ToString() != "")
                            a.Add("td", item.GetValue("TakeDate").ToString().Substring(0, 2) + ":" + item.GetValue("TakeDate").ToString().Substring(2, 2));
                        else
                            a.Add("td", item.GetValue("TakeDate").ToString()); //: "2000", 預約時段
                        data.Add(a);
                    }
                }
                catch (Exception e)
                {
                    e.ToString();
                }
            }




            double PageTotal = (double)TotalItem / itemPerPage;
            PageTotal = Math.Ceiling(PageTotal);

            PageTotal = 1;
            JsonResult.Add("total", PageTotal);
            JsonResult.Add("page", PageTotal);
            JsonResult.Add("records", TotalItem);
            JsonResult.Add("rows", data);
            return Json(JsonResult);

        }

        [HttpPost]
        //public JsonResult GetList(int page = 1, int itemPerPage = 20)
        public JsonResult GetList2(string sidx, string sord, int page, int rows)
        {
            string searchJson = null; int itemPerPage = 10;
            if (itemPerPage > 100)
            {
                itemPerPage = 100;
            }

            Dictionary<string, dynamic> JsonResult = new Dictionary<string, dynamic>();

            try
            {

                string QSDate = "";
                string QEDate = "";
                string DispatchNo = "";
                string OrderNo = "";
                string DriverID = "0";
                string CarNo = "";


                if (searchJson != null)
                {
                    try
                    {

                        dynamic search = JObject.Parse(searchJson);

                        QSDate = search.QSDate;
                        QEDate = search.QEDate;
                        DispatchNo = search.DispatchNo;
                        OrderNo = search.OrderNo;

                        if (search.DriverID == "")
                        {
                            DriverID = "0";
                        }
                        else
                        {
                            DriverID = search.DriverID;
                        }

                        CarNo = search.CarNo;

                    }
                    catch (Exception e)
                    {

                    }

                }

                //Dictionary<string, dynamic> dataParameters = new Dictionary<string, dynamic>();


                DAL.DALReservation bal = new DAL.DALReservation(ConnectionString);
                BALGeneral balg = new BALGeneral(ConnectionString);
                RtnStruct r = new RtnStruct();
                List<OReservation> List = bal.GetReservationsForIndex();
                if (List == null)
                    r.RtnObject = null;
                else
                {

                    r.RtnObject = List;
                }
                r.Status = "Success";



                dynamic jsonData = Helper.jsonDecode(JsonConvert.SerializeObject(r));


                string Status = jsonData.Status;
                var RtnObject = jsonData.RtnObject;

                int countSkip = itemPerPage * (page - 1);
                int countTake = itemPerPage;

                int TotalItem = 0;

                //RepStarDateTime = "2016-12-20";
                //RepEndDateTime = "2017-01-04";

                List<dynamic> data = new List<dynamic>();
                var items = RtnObject;

                if (items != null)
                {

                    foreach (JObject item in items)
                    {
                        TotalItem++;

                        Dictionary<string, dynamic> a = new Dictionary<string, dynamic>();

                        //dynamic zzz = Helper.jsonDecode(item);
                        //a.Add("zzz", zzz);

                        a.Add("rno", item.GetValue("ReservationNo").ToString()); //:"20170118", //搭乘日期
                        a.Add("td", item.GetValue("ServeDate").ToString()); //:"20170118", //搭乘日期
                        a.Add("name", item.GetValue("PickupCity").ToString()); //:"2017/01/18T09:25",//服務時間
                        a.Add("money", item.GetValue("Credit").ToString()); //:"O", //出回國 (I→回國/O→出國)

                        data.Add(a);

                    }

                }




                double PageTotal = (double)TotalItem / itemPerPage;
                PageTotal = Math.Ceiling(PageTotal);

                PageTotal = 1;
                JsonResult.Add("total", PageTotal);
                JsonResult.Add("page", PageTotal);
                JsonResult.Add("records", TotalItem);
                JsonResult.Add("rows", data);

            }
            catch (Exception)
            {

            }

            return Json(JsonResult);

        }

        public JsonResult GetList3(string sidx, string sord, int page, int rows)
        {
            string searchJson = null; int itemPerPage = 10;
            if (itemPerPage > 100)
            {
                itemPerPage = 100;
            }
            string QSDate = "";
            string QEDate = "";
            string stage = "";
            string Carteam = "";

            if (searchJson != null)
            {
                try
                {

                    dynamic search = JObject.Parse(searchJson);

                    QSDate = search.QSDate;
                    QEDate = search.QEDate;
                    stage = search.stage;
                    Carteam = search.Carteam;

                }
                catch (Exception e)
                {

                }

            }
            Dictionary<string, dynamic> JsonResult = new Dictionary<string, dynamic>();

            RtnStruct r = new RtnStruct();
            DAL.DALDispatch dal = new DAL.DALDispatch(ConnectionString);
            BALGeneral balg = new BALGeneral(ConnectionString);
            try
            {
                int fleetid = 0;
                string fstage = "";
                if (Carteam != "")
                    fleetid = balg.GetFleet(Carteam).FleetID;
                if (stage.IndexOf("一般") != -1)
                    fstage = "0";
                else if (stage.IndexOf("完成") != -1)
                    fstage = "C";
                else if (stage.IndexOf("計價") != -1)
                    fstage = "N";
                else if (stage.IndexOf("失敗") != -1)
                    fstage = "F";
                else
                    fstage = "";
                List<ODispatchSheet> List = dal.GetDispatchSheet3DForIndex();
                if (List == null)
                    r.RtnObject = null;
                else
                {
                    r.RtnObject = List;
                }
                r.Status = "Success";
            }
            catch (Exception e)
            {
                e.ToString();
            }
            dynamic jsonData = Helper.jsonDecode(JsonConvert.SerializeObject(r));
            string Status = jsonData.Status;
            var RtnObject = jsonData.RtnObject;

            int countSkip = itemPerPage * (page - 1);
            int countTake = itemPerPage;

            int TotalItem = 0;

            //RepStarDateTime = "2016-12-20";
            //RepEndDateTime = "2017-01-04";

            List<dynamic> data = new List<dynamic>();
            DAL.DALReservation ddalr = new DAL.DALReservation(ConnectionString);
            var items = RtnObject;

            if (items != null)
            {

                foreach (JObject item in items)
                {
                    TotalItem++;

                    Dictionary<string, dynamic> a = new Dictionary<string, dynamic>();
                    a.Add("dno", item.GetValue("DispatchNo").ToString()); //:"20170118", //搭乘日期
                    a.Add("cp", item.GetValue("CarType").ToString()); //:"20170118", //搭乘日期
                    a.Add("fs", item.GetValue("FisrtServiceTime").ToString()); //:5,//全車人
                    a.Add("cn", item.GetValue("CarNo").ToString()); //:5,//全車人

                    if (item.GetValue("ServiceRemark").ToString() == "0")
                        a.Add("sr", "一般"); //:"2017/01/18T09:25",//服務時間                                  
                    else if (item.GetValue("ServiceRemark").ToString() == "C")
                        a.Add("sr", "完成"); //:"2017/01/18T09:25",//服務時間                    
                    else if(item.GetValue("ServiceRemark").ToString() == "N")
                        a.Add("sr", "計價");
                    else if (item.GetValue("ServiceRemark").ToString() != "F")
                        a.Add("sr", "失敗");
                    else if (item.GetValue("ServiceRemark").ToString() != "X")
                        a.Add("sr", "取消");
                    else
                        a.Add("sr", "未知"); //: "2000", 預約時段                   
                    a.Add("dn", item.GetValue("DriverName").ToString()); //:5,//全車人
                    if (item.GetValue("ServiceType").ToString() == "I")
                        a.Add("st", "回國"); //:"2017/01/18T09:25",//服務時間                                  
                    else
                        a.Add("st", "出國"); //:"2017/01/18T09:25",//服務時間                    
                    if (item.GetValue("TakeDate").ToString() != "")
                        a.Add("td", item.GetValue("TakeDate").ToString().Substring(0, 2) + ":" + item.GetValue("TakeDate").ToString().Substring(2, 2));
                    else
                        a.Add("td", item.GetValue("TakeDate").ToString()); //: "2000", 預約時段
                    data.Add(a);
                }

            }




            double PageTotal = (double)TotalItem / itemPerPage;
            PageTotal = Math.Ceiling(PageTotal);

            PageTotal = 1;
            JsonResult.Add("total", PageTotal);
            JsonResult.Add("page", PageTotal);
            JsonResult.Add("records", TotalItem);
            JsonResult.Add("rows", data);
            return Json(JsonResult);

        }

        public JsonResult GetList4(string sidx, string sord, int page, int rows)
        {
            string searchJson = null; int itemPerPage = 10;
            if (itemPerPage > 100)
            {
                itemPerPage = 100;
            }

            Dictionary<string, dynamic> JsonResult = new Dictionary<string, dynamic>();

            try
            {

                string QSDate = "";
                string QEDate = "";
                string DispatchNo = "";
                string OrderNo = "";
                string DriverID = "0";
                string CarNo = "";


                if (searchJson != null)
                {
                    try
                    {

                        dynamic search = JObject.Parse(searchJson);

                        QSDate = search.QSDate;
                        QEDate = search.QEDate;
                        DispatchNo = search.DispatchNo;
                        OrderNo = search.OrderNo;

                        if (search.DriverID == "")
                        {
                            DriverID = "0";
                        }
                        else
                        {
                            DriverID = search.DriverID;
                        }

                        CarNo = search.CarNo;

                    }
                    catch (Exception e)
                    {

                    }

                }

                //Dictionary<string, dynamic> dataParameters = new Dictionary<string, dynamic>();


                DAL.DALReservation bal = new DAL.DALReservation(ConnectionString);
                BALGeneral balg = new BALGeneral(ConnectionString);
                RtnStruct r = new RtnStruct();
                List<O24Reservation> List = bal.Get24HrReservations();
                if (List == null)
                    r.RtnObject = null;
                else
                {

                    r.RtnObject = List;
                }
                r.Status = "Success";



                dynamic jsonData = Helper.jsonDecode(JsonConvert.SerializeObject(r));


                string Status = jsonData.Status;
                var RtnObject = jsonData.RtnObject;

                int countSkip = itemPerPage * (page - 1);
                int countTake = itemPerPage;

                int TotalItem = 0;

                //RepStarDateTime = "2016-12-20";
                //RepEndDateTime = "2017-01-04";

                List<dynamic> data = new List<dynamic>();
                var items = RtnObject;

                if (items != null)
                {

                    foreach (JObject item in items)
                    {
                        TotalItem++;

                        Dictionary<string, dynamic> a = new Dictionary<string, dynamic>();


                        a.Add("rno", item.GetValue("ReservationNo").ToString()); 
                        a.Add("td", item.GetValue("TakeDate").ToString()); 
                        a.Add("name", item.GetValue("name").ToString());
                        if(item.GetValue("TimeSegment").ToString() !="")
                            a.Add("ts", item.GetValue("TimeSegment").ToString().Substring(0, 2) + ":" + item.GetValue("TimeSegment").ToString().Substring(2, 2));
                        else 
                            a.Add("ts", item.GetValue("TimeSegment").ToString());
                        a.Add("fn", item.GetValue("FlightNo").ToString());
                        a.Add("ad", item.GetValue("address").ToString());
                        a.Add("at", item.GetValue("airsTer").ToString());
                        if (item.GetValue("ServiceType").ToString() == "I")
                            a.Add("st", "回國"); //:"2017/01/18T09:25",//服務時間                                  
                        else
                            a.Add("st", "出國");
                        a.Add("uid", item.GetValue("UserID").ToString());
                        data.Add(a);

                    }

                }




                double PageTotal = (double)TotalItem / itemPerPage;
                PageTotal = Math.Ceiling(PageTotal);

                PageTotal = 1;
                JsonResult.Add("total", PageTotal);
                JsonResult.Add("page", PageTotal);
                JsonResult.Add("records", TotalItem);
                JsonResult.Add("rows", data);

            }
            catch (Exception)
            {

            }

            return Json(JsonResult);

        }

        public JsonResult GetList5(string sidx, string sord, int page, int rows)
        {
            string searchJson = null; int itemPerPage = 10;
            if (itemPerPage > 100)
            {
                itemPerPage = 100;
            }

            Dictionary<string, dynamic> JsonResult = new Dictionary<string, dynamic>();

            try
            {

                string QSDate = "";
                string QEDate = "";
                string DispatchNo = "";
                string OrderNo = "";
                string DriverID = "0";
                string CarNo = "";


                if (searchJson != null)
                {
                    try
                    {

                        dynamic search = JObject.Parse(searchJson);

                        QSDate = search.QSDate;
                        QEDate = search.QEDate;
                        DispatchNo = search.DispatchNo;
                        OrderNo = search.OrderNo;

                        if (search.DriverID == "")
                        {
                            DriverID = "0";
                        }
                        else
                        {
                            DriverID = search.DriverID;
                        }

                        CarNo = search.CarNo;

                    }
                    catch (Exception e)
                    {

                    }

                }

                //Dictionary<string, dynamic> dataParameters = new Dictionary<string, dynamic>();


                DAL.DALReservation bal = new DAL.DALReservation(ConnectionString);
                BALGeneral balg = new BALGeneral(ConnectionString);
                RtnStruct r = new RtnStruct();
                List<O24Reservation> List = bal.GetUnReservations();
                if (List == null)
                    r.RtnObject = null;
                else
                {

                    r.RtnObject = List;
                }
                r.Status = "Success";



                dynamic jsonData = Helper.jsonDecode(JsonConvert.SerializeObject(r));


                string Status = jsonData.Status;
                var RtnObject = jsonData.RtnObject;

                int countSkip = itemPerPage * (page - 1);
                int countTake = itemPerPage;

                int TotalItem = 0;

                //RepStarDateTime = "2016-12-20";
                //RepEndDateTime = "2017-01-04";

                List<dynamic> data = new List<dynamic>();
                var items = RtnObject;

                if (items != null)
                {

                    foreach (JObject item in items)
                    {
                        TotalItem++;

                        Dictionary<string, dynamic> a = new Dictionary<string, dynamic>();


                        a.Add("rno", item.GetValue("ReservationNo").ToString());
                        a.Add("td", item.GetValue("TakeDate").ToString());
                        a.Add("name", item.GetValue("name").ToString());
                        if (item.GetValue("TimeSegment").ToString() != "")
                            a.Add("ts", item.GetValue("TimeSegment").ToString().Substring(0, 2) + ":" + item.GetValue("TimeSegment").ToString().Substring(2, 2));
                        else
                            a.Add("ts", item.GetValue("TimeSegment").ToString());
                        a.Add("fn", item.GetValue("FlightNo").ToString());
                        a.Add("ad", item.GetValue("address").ToString());
                        a.Add("at", item.GetValue("airsTer").ToString());
                        if (item.GetValue("ServiceType").ToString() == "I")
                            a.Add("st", "回國"); //:"2017/01/18T09:25",//服務時間                                  
                        else
                            a.Add("st", "出國");
                        a.Add("uid", item.GetValue("UserID").ToString());
                        data.Add(a);

                    }

                }




                double PageTotal = (double)TotalItem / itemPerPage;
                PageTotal = Math.Ceiling(PageTotal);

                PageTotal = 1;
                JsonResult.Add("total", PageTotal);
                JsonResult.Add("page", PageTotal);
                JsonResult.Add("records", TotalItem);
                JsonResult.Add("rows", data);

            }
            catch (Exception)
            {

            }

            return Json(JsonResult);

        }

        public JsonResult GetList6(string sidx, string sord, int page, int rows)
        {
            string searchJson = null; int itemPerPage = 10;
            if (itemPerPage > 100)
            {
                itemPerPage = 100;
            }

            Dictionary<string, dynamic> JsonResult = new Dictionary<string, dynamic>();

            try
            {

                string QSDate = "";
                string QEDate = "";
                string DispatchNo = "";
                string OrderNo = "";
                string DriverID = "0";
                string CarNo = "";


                if (searchJson != null)
                {
                    try
                    {

                        dynamic search = JObject.Parse(searchJson);

                        QSDate = search.QSDate;
                        QEDate = search.QEDate;
                        DispatchNo = search.DispatchNo;
                        OrderNo = search.OrderNo;

                        if (search.DriverID == "")
                        {
                            DriverID = "0";
                        }
                        else
                        {
                            DriverID = search.DriverID;
                        }

                        CarNo = search.CarNo;

                    }
                    catch (Exception e)
                    {

                    }

                }

                //Dictionary<string, dynamic> dataParameters = new Dictionary<string, dynamic>();


                DAL.DALReservation bal = new DAL.DALReservation(ConnectionString);
                BALGeneral balg = new BALGeneral(ConnectionString);
                RtnStruct r = new RtnStruct();
                List<OErrorSheet> List = bal.GetErrorSheets();
                if (List == null)
                    r.RtnObject = null;
                else
                {

                    r.RtnObject = List;
                }
                r.Status = "Success";



                dynamic jsonData = Helper.jsonDecode(JsonConvert.SerializeObject(r));


                string Status = jsonData.Status;
                var RtnObject = jsonData.RtnObject;

                int countSkip = itemPerPage * (page - 1);
                int countTake = itemPerPage;

                int TotalItem = 0;

                //RepStarDateTime = "2016-12-20";
                //RepEndDateTime = "2017-01-04";

                List<dynamic> data = new List<dynamic>();
                var items = RtnObject;

                if (items != null)
                {

                    foreach (JObject item in items)
                    {
                        TotalItem++;

                        Dictionary<string, dynamic> a = new Dictionary<string, dynamic>();


                        a.Add("rno", item.GetValue("ReservationNo").ToString());
                        a.Add("td", item.GetValue("TakeDate").ToString());
                        a.Add("name", item.GetValue("passengername").ToString());
                        if (item.GetValue("TimeSegment").ToString() != "")
                            a.Add("ts", item.GetValue("TimeSegment").ToString().Substring(0, 2) + ":" + item.GetValue("TimeSegment").ToString().Substring(2, 2));
                        else
                            a.Add("ts", item.GetValue("TimeSegment").ToString());
                        a.Add("phone", item.GetValue("passengerphone").ToString());

                        data.Add(a);

                    }

                }




                double PageTotal = (double)TotalItem / itemPerPage;
                PageTotal = Math.Ceiling(PageTotal);

                PageTotal = 1;
                JsonResult.Add("total", PageTotal);
                JsonResult.Add("page", PageTotal);
                JsonResult.Add("records", TotalItem);
                JsonResult.Add("rows", data);

            }
            catch (Exception)
            {

            }

            return Json(JsonResult);

        }
    }
}