﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using CallCar.Models;
using CallCar.BAL;
using System.Configuration;
using System.Data;
using System.IO;
using System.Net;
using System.Net.Mime;
using System.Security.Cryptography;
using ClosedXML.Excel;
using System.Text;

namespace CallCar.Controllers
{
    public class PaylistController : Controller
    {
        // GET: Paylist
        public ActionResult Index()
        {
            return View();
        }

        public static string ConnectionString = ConfigurationManager.ConnectionStrings["CarpoolLab"].ConnectionString;

        public JsonResult GetList( int page ,string searchJson = null, string orderField = "", string orderType = "", int itemPerPage = 10)
        {

            if (itemPerPage > 100)
            {
                itemPerPage = 100;
            }
            string QSDate = "";
            string QEDate = "";
            string stage = "";
            string Carteam = "";

            if (searchJson != null)
            {
                try
                {

                    dynamic search = JObject.Parse(searchJson);

                    QSDate = search.QSDate;
                    QEDate = search.QEDate;

                }
                catch (Exception e)
                {
                    e.ToString();
                }

            }
            Dictionary<string, dynamic> JsonResult = new Dictionary<string, dynamic>();

            RtnStruct r = new RtnStruct();
            BALDispatch bal = new BALDispatch(ConnectionString);
            BALGeneral balg = new BALGeneral(ConnectionString);
            int countSkip = itemPerPage * (page - 1);
            int countTake = itemPerPage;

            int TotalItem = 0;
            try
            {
                int fleetid = 0;
                string fstage = "";

                DAL.DALReservation dal = new DAL.DALReservation(ConnectionString);
                List<OPay> GetPays = dal.GetPays(QSDate, QEDate);
                List<dynamic> data = new List<dynamic>();
                foreach (OPay gnow in GetPays)
                {
                    Dictionary<string, dynamic> a = new Dictionary<string, dynamic>();
                    TotalItem++;
                    a.Add("tdate", gnow.takedate.ToString());
                    a.Add("ttime", gnow.TimeSegment.ToString());
                    a.Add("ccSno", gnow.DispatchNo.ToString());
                    a.Add("sno", gnow.ReservationNo.ToString());
                    a.Add("Apct", gnow.APassengerCnt.ToString());
                    a.Add("address", gnow.address.ToString());
                    a.Add("stop", gnow.stop.ToString());
                    a.Add("night", gnow.night.ToString());
                    a.Add("baby", gnow.baby.ToString());
                    a.Add("car", gnow.car.ToString());
                    a.Add("team", gnow.team.ToString());
                    a.Add("carnum", gnow.carnum.ToString());
                    a.Add("service", gnow.service.ToString());
                    a.Add("memo", gnow.Memo.ToString());
                    data.Add(a);
                }



                JsonResult.Add("data", data);


                double PageTotal = (double)TotalItem / itemPerPage;
                PageTotal = Math.Ceiling(PageTotal);


                JsonResult.Add("totalItem", PageTotal);
                JsonResult.Add("pageTotal", PageTotal);
                JsonResult.Add("nowpage", page);

            }
            catch (Exception e)
            {
                e.ToString();
            }
            return Json(JsonResult);
        }

        [HttpPost]
        public HttpStatusCodeResult CreateExcel(string searchJson)
        {
            XLWorkbook wb = new XLWorkbook(XLEventTracking.Disabled); //create Excel

            string QSDate = "";
            string QEDate = "";
            string Stage = "";
            string Carteam = "";

            if (searchJson != null)
            {
                try
                {

                    dynamic search = JObject.Parse(searchJson);

                    QSDate = search.QSDate;
                    QEDate = search.QEDate;

                }
                catch (Exception e)
                {
                    e.ToString();
                }

            }
            Dictionary<string, dynamic> JsonResult = new Dictionary<string, dynamic>();
            DataTable dt = null;
            RtnStruct r = new RtnStruct();
            BALDispatch bal = new BALDispatch(ConnectionString);
            BALGeneral balg = new BALGeneral(ConnectionString);
            try
            {
                int fleetid = 0;
                string fstage = "";

                DAL.DALReservation dal = new DAL.DALReservation(ConnectionString);
                List<OPay> GetPays = dal.GetPays(QSDate, QEDate);
                List<dynamic> data = new List<dynamic>();

                    dt = new DataTable();
                    dt.Columns.Add("乘車日期");
                    dt.Columns.Add("乘車時間");
                    dt.Columns.Add("CallCar單號");
                    dt.Columns.Add("艾雪單號");
                    dt.Columns.Add("服務地址");
                    dt.Columns.Add("總乘車人數");
                    dt.Columns.Add("停靠(訂單數-1)");
                    dt.Columns.Add("夜間");
                    dt.Columns.Add("安椅");
                    dt.Columns.Add("車款");
                dt.Columns.Add("廠商");
                dt.Columns.Add("車號");
                dt.Columns.Add("服務人員");
                dt.Columns.Add("備註");
                foreach (OPay gnow in GetPays)
                {
                    DataRow dataRow = dt.NewRow();
                    dataRow["乘車日期"] = gnow.takedate.ToString();
                    dataRow["乘車時間"] = gnow.TimeSegment.ToString();
                    dataRow["CallCar單號"] = gnow.DispatchNo.ToString();
                    dataRow["艾雪單號"] = gnow.ReservationNo.ToString();
                    dataRow["服務地址"] = gnow.address.ToString();
                    dataRow["總乘車人數"] = gnow.APassengerCnt.ToString();
                    dataRow["停靠(訂單數-1)"] = gnow.stop.ToString();
                    dataRow["夜間"] = gnow.night.ToString();
                    dataRow["安椅"] = gnow.baby.ToString();
                    dataRow["車款"] = gnow.car.ToString();
                    dataRow["廠商"] = gnow.team.ToString();
                    dataRow["車號"] = gnow.carnum.ToString();
                    dataRow["服務人員"] = gnow.service.ToString();
                    dataRow["備註"] = gnow.Memo.ToString();

                    dt.Rows.Add(dataRow);
                   
                }

                
            }
            catch (Exception e)
            {
                e.ToString();
            }
            wb.AddWorksheet(dt, "Sheet1");
            if (wb != null)
            {
                Session["ExcelResult"] = wb;
                return new HttpStatusCodeResult(HttpStatusCode.OK);
            }

            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

        }

        public FileContentResult ExcelResult(string reportHeader) //it's your data passed to controller
        {

            byte[] fileBytes = GetExcel((XLWorkbook)Session["ExcelResult"]);
            var exportFileName = string.Concat(
                "Report2Paylist_",
                DateTime.Now.ToString("yyyyMMdd"),
                ".xlsx");
            return File(fileBytes, MediaTypeNames.Application.Octet, exportFileName);
        }
        public static byte[] GetExcel(XLWorkbook wb)
        {
            using (var ms = new MemoryStream())
            {
                wb.SaveAs(ms);
                return ms.ToArray();
            }
        }
    }
}