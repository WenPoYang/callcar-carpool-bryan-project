﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using CallCar.Models;
using CallCar.BAL;
using System.Configuration;

using System.IO;
using System.Net;
using System.Text;
using System.Security.Cryptography;

namespace CallCar.Controllers
{
    public class CreditController : _AdminController
    {
        // GET: Credit
        public ActionResult Index()
        {
            return View();
        }

        public static string ConnectionString = ConfigurationManager.ConnectionStrings["CarpoolLab"].ConnectionString;
        public JsonResult GetList( int page ,string searchJson = null, string orderField = "", string orderType = "", int itemPerPage = 10)
        {

            if (itemPerPage > 100)
            {
                itemPerPage = 100;
            }
            string QSDate = "";
            string QEDate = "";
            string stage = "";
            string Carteam = "";

            if (searchJson != null)
            {
                try
                {

                    dynamic search = JObject.Parse(searchJson);

                    QSDate = search.QSDate;
                    QEDate = search.QEDate;
                    stage = search.stage;
                    Carteam = search.Carteam;

                }
                catch (Exception e)
                {
                    e.ToString();
                }

            }
            Dictionary<string, dynamic> JsonResult = new Dictionary<string, dynamic>();

            RtnStruct r = new RtnStruct();
            BALDispatch bal = new BALDispatch(ConnectionString);
            BALGeneral balg = new BALGeneral(ConnectionString);
            try
            {
                int fleetid = 0;
                string fstage = "";
                if (Carteam != "")
                    fleetid = balg.GetFleet(Carteam).FleetID;
                if (stage.IndexOf("一般") != -1)
                    fstage = "0";
                else if (stage.IndexOf("完成") != -1)
                    fstage = "C";
                else if (stage.IndexOf("計價") != -1)
                    fstage = "N";
                else if (stage.IndexOf("失敗") != -1)
                    fstage = "F";
                else
                    fstage = "";
                List<ODispatchSheet> List = bal.GetDispatchSheetByDateAndStage(QSDate, QEDate, fstage, fleetid);
                DAL.DALDispatch dal = new DAL.DALDispatch(ConnectionString);
                List<OGoPaylist> GoPayList = dal.GetGoPaylist(QSDate, QEDate, fstage);
                if (List == null)
                    r.RtnObject = null;
                else
                {
                    List<DispatchPayStruct> rfsList = new List<DispatchPayStruct>();

                    foreach (ODispatchSheet or in List)
                    {
                        foreach (ODpServiceUnit now in or.ServiceList)
                        {                      
                            DispatchPayStruct rfs = new DispatchPayStruct();
                            if (or.FleetID == 0)
                                rfs.cp = "none";
                            else
                                rfs.cp = balg.GetFleet(or.FleetID).FleetName;
                            rfs.dno = or.DispatchNo;
                            rfs.st = or.ServiceType;
                            rfs.td = or.TakeDate;
                            rfs.sr = now.ServiceRemark;
                            rfs.rno =  now.ReservationNo;
                            rfs.uid = now.uid;
                            rfs.pc = or.PassengerCnt;
                            rfs.pct = now.PassengerCnt;
                            rfs.bct = now.BaggageCnt;
                            rfs.abct = now.AddBaggageCnt;
                            rfs.cpn = now.Coupon;
                            rfsList.Add(rfs);
                        }
                    }
                    r.RtnObject = rfsList;
                }
                r.Status = "Success";

            dynamic jsonData = Helper.jsonDecode(JsonConvert.SerializeObject(r));
            string Status = jsonData.Status;
            var RtnObject = jsonData.RtnObject;

            int countSkip = itemPerPage * (page - 1);
            int countTake = itemPerPage;

            int TotalItem = 0;

            List<dynamic> data = new List<dynamic>();
            DAL.DALReservation ddalr = new DAL.DALReservation(ConnectionString);

            var items = RtnObject;
                foreach (OGoPaylist go in GoPayList)
                {
                    foreach (OGoPaylistUnit gnow in go.UnitList)
                    {
                        TotalItem++;
                        Dictionary<string, dynamic> a = new Dictionary<string, dynamic>();
                        a.Add("rno", gnow.ReservationNo.ToString());
                        a.Add("uid", gnow.UserID.ToString());
                        a.Add("uidname", gnow.UserName.ToString());
                        a.Add("st", gnow.ServiceType.ToString());
                        a.Add("td", gnow.takedate.ToString());
                        a.Add("pct", gnow.PassengerCnt.ToString());
                        a.Add("Apct", dal.GetDispNumber(gnow.ReservationNo.ToString()));
                        a.Add("bct", gnow.BaggageCnt.ToString());
                        a.Add("abct", gnow.AddBaggageCnt.ToString());
                        a.Add("cpn", gnow.Coupon.ToString());
                        a.Add("stage", gnow.ChargeResult.ToString());
                        a.Add("pr", gnow.Price.ToString());
                        a.Add("sr", gnow.ServiceRemark.ToString());
                        a.Add("RealAmt", gnow.RealAmt.ToString());
                        a.Add("PromoteAmt", gnow.PromoteAmt.ToString());
                        a.Add("CollectAmt", gnow.CollectAmt.ToString());
                        a.Add("TotalAmt", gnow.TotalAmt.ToString());
                        a.Add("OfficialTotalAmt", gnow.OfficialTotalAmt.ToString());
                        a.Add("CarFee", gnow.CarFee.ToString());
                        a.Add("NightFee", gnow.NightFee.ToString());
                        a.Add("BaggageFee", gnow.BaggageFee.ToString());
                        a.Add("OtherFee", gnow.OtherFee.ToString());
                        a.Add("OtherSaleFee", gnow.OtherSaleFee.ToString());

                        data.Add(a);
                    }
                }


            JsonResult.Add("data", data);


            double PageTotal = (double)TotalItem / itemPerPage;
            PageTotal = Math.Ceiling(PageTotal);

            //PageTotal = 1;

            JsonResult.Add("totalItem", TotalItem);
            JsonResult.Add("pageTotal", PageTotal);
            JsonResult.Add("nowpage", page);

            }
            catch (Exception e)
            {
                e.ToString();
            }
            return Json(JsonResult);
        }

        public JsonResult GetList2(int page ,string searchJson = null, string orderField = "", string orderType = "", int itemPerPage = 10)
        {

            if (itemPerPage > 100)
            {
                itemPerPage = 100;
            }
            string QSDate = "";
            string QEDate = "";
            string stage = "";
            string Carteam = "";

            if (searchJson != null)
            {
                try
                {

                    dynamic search = JObject.Parse(searchJson);

                    QSDate = search.QSDate2;
                    QEDate = search.QEDate2;

                }
                catch (Exception e)
                {
                    e.ToString();
                }

            }
            Dictionary<string, dynamic> JsonResult = new Dictionary<string, dynamic>();

            RtnStruct r = new RtnStruct();
            BALDispatch bal = new BALDispatch(ConnectionString);
            BALGeneral balg = new BALGeneral(ConnectionString);
            try
            {
                int fleetid = 0;
                string fstage = "";

                DAL.DALReservation dal = new DAL.DALReservation(ConnectionString);
                List<ORecev> GoRecev = dal.GetRecevs(QSDate, QEDate);
                List<dynamic> data = new List<dynamic>();
                foreach (ORecev gnow in GoRecev)
                {
                    Dictionary<string, dynamic> a = new Dictionary<string, dynamic>();

                    a.Add("tdate", gnow.takedate.ToString());
                    a.Add("ttime", gnow.TimeSegment.ToString());
                    a.Add("ccSno", gnow.DispatchNo.ToString());
                    a.Add("sno", gnow.ReservationNo.ToString());
                    a.Add("Apct", gnow.APassengerCnt.ToString());
                    a.Add("pct", gnow.PassengerCnt.ToString());
                    a.Add("bct", gnow.BaggageCnt.ToString());
                    a.Add("abct", gnow.ABaggageCnt.ToString());
                    a.Add("cpn", gnow.Coupon.ToString());
                    a.Add("pr", gnow.Price.ToString());
                    a.Add("realpr", gnow.realamt.ToString());
                    a.Add("stage", gnow.ChargeResult.ToString());
                    a.Add("cancelT", gnow.UpdTime.ToString());
                    a.Add("memo", gnow.Memo.ToString());
                    data.Add(a);
                }
                


                JsonResult.Add("data", data);


                double PageTotal = 0;
                PageTotal = Math.Ceiling(PageTotal);

                PageTotal = 1;

                JsonResult.Add("totalItem", PageTotal);
                JsonResult.Add("pageTotal", PageTotal);


            }
            catch (Exception e)
            {
                e.ToString();
            }
            return Json(JsonResult);
        }

        public JsonResult GetList3(int page ,string searchJson = null,  string orderField = "", string orderType = "", int itemPerPage = 10)
        {

            if (itemPerPage > 100)
            {
                itemPerPage = 100;
            }
            string QSDate = "";
            string QEDate = "";
            string stage = "";
            string Carteam = "";

            if (searchJson != null)
            {
                try
                {

                    dynamic search = JObject.Parse(searchJson);

                    QSDate = search.QSDate3;
                    QEDate = search.QEDate3;

                }
                catch (Exception e)
                {
                    e.ToString();
                }

            }
            Dictionary<string, dynamic> JsonResult = new Dictionary<string, dynamic>();

            RtnStruct r = new RtnStruct();
            BALDispatch bal = new BALDispatch(ConnectionString);
            BALGeneral balg = new BALGeneral(ConnectionString);
            try
            {
                int fleetid = 0;
                string fstage = "";

                DAL.DALReservation dal = new DAL.DALReservation(ConnectionString);
                List<OPay> GetPays = dal.GetPays(QSDate, QEDate);
                List<dynamic> data = new List<dynamic>();
                foreach (OPay gnow in GetPays)
                {
                    Dictionary<string, dynamic> a = new Dictionary<string, dynamic>();

                    a.Add("tdate", gnow.takedate.ToString());
                    a.Add("ttime", gnow.TimeSegment.ToString());
                    a.Add("ccSno", gnow.DispatchNo.ToString());
                    a.Add("sno", gnow.ReservationNo.ToString());
                    a.Add("Apct", gnow.APassengerCnt.ToString());
                    a.Add("address", gnow.address.ToString());
                    a.Add("stop", gnow.stop.ToString());
                    a.Add("night", gnow.night.ToString());
                    a.Add("baby", gnow.baby.ToString());
                    a.Add("car", gnow.car.ToString());
                    a.Add("team", gnow.team.ToString());
                    a.Add("carnum", gnow.carnum.ToString());
                    a.Add("service", gnow.service.ToString());
                    a.Add("memo", gnow.Memo.ToString());
                    data.Add(a);
                }



                JsonResult.Add("data", data);


                double PageTotal = 0;
                PageTotal = Math.Ceiling(PageTotal);

                PageTotal = 1;

                JsonResult.Add("totalItem", PageTotal);
                JsonResult.Add("pageTotal", PageTotal);


            }
            catch (Exception e)
            {
                e.ToString();
            }
            return Json(JsonResult);
        }

        public string Go2Credit(string SNO, string DATE)//使用授權碼刷卡
        {
            BALReservation bal = new BALReservation(ConnectionString);


            OReservation R = bal.GetReservationByNo(SNO);
            if (R != null)
            {
                try
                {
                    string HashKey = "FMJKUkbQ6pmh7bJyd1iOfRsmJyR0Neys";
                    string HashIV = "0hdVCpLJP81WjLvx";
                    if (R.Credit.ToString() == "")
                    {
                        return "無信用卡資料!!";
                    }
                    dynamic search = JObject.Parse(R.Credit);

                    DAL.DALUserAccount dalAcc = new DAL.DALUserAccount(ConnectionString);

                    OAccount UserInfo = dalAcc.GetID(search.UID.Value.ToString());
                    
                    WebRequest tRequest;
                    tRequest = WebRequest.Create("https://core.spgateway.com/API/CreditCard");
                    tRequest.Method = "post";
                    tRequest.ContentType = " application/x-www-form-urlencoded;charset=UTF-8";
                    string Version = "1.0";
                    string TokenSwitch = "on";
                    string postData = "MerchantID_=MS31451783&PostData_=" + EncryptAES256("TimeStamp=" + DATE + "&Version=" + Version + "&MerchantOrderNo=" + SNO + DATE + "&Amt=" + R.TrailPrice + "&ProdDesc=" + "乘車費" + "&PayerEmail=" + UserInfo.Email.ToString() + "&TokenValue=" + search.t.Value + "&TokenTerm=" + search.UID.Value + "&TokenSwitch=" + TokenSwitch, HashKey, HashIV) + "&Pos_=JSON";

                    Console.WriteLine(postData);

                    Byte[] byteArray = Encoding.UTF8.GetBytes(postData);
                    tRequest.ContentLength = byteArray.Length;

                    Stream dataStream = tRequest.GetRequestStream();
                    dataStream.Write(byteArray, 0, byteArray.Length);
                    dataStream.Close();

                    WebResponse tResponse = tRequest.GetResponse();

                    dataStream = tResponse.GetResponseStream();

                    StreamReader tReader = new StreamReader(dataStream);

                    String sResponseFromServer = tReader.ReadToEnd();


                    tReader.Close();
                    dataStream.Close();
                    tResponse.Close();
                    dynamic jsonData = Helper.jsonDecode(sResponseFromServer);
                    if (jsonData.Status == "SUCCESS")
                    {
                        //dynamic ODR = JObject.Parse(jsonData.Result.Replace("{{", "{").Replace("}}", "}"));

                        DAL.DALReservation dalr = new DAL.DALReservation(ConnectionString);
                        DAL.DALGeneral ddl = new DAL.DALGeneral(ConnectionString);
                        DAL.DALInvoice dInv = new DAL.DALInvoice(ConnectionString);
                        dalr.UpdReservationProStage(SNO);
  
                        OPay2goResult pdata = new OPay2goResult();
                        if (ddl.Re_Charge(SNO).ToString() != "")
                        {
                            OPay2goResult OD = JsonConvert.DeserializeObject<OPay2goResult>(ddl.Re_Charge(SNO).ToString());

                            for (int i = 0; i < OD.Result.Count(); i++)
                            {
                                OPay2goResultUnit punitO = new OPay2goResultUnit();
                                int temp = Convert.ToInt32(OD.Result[i].Index.ToString());
                                punitO.Index = (temp + 1).ToString();
                                punitO.Status = OD.Result[i].Status.ToString();
                                punitO.Message = OD.Result[i].Message.ToString();
                                pdata.Result.Add(punitO);
                            }
                        }
                        OPay2goResultUnit punit = new OPay2goResultUnit();
                        punit.Index = "1";
                        punit.Status = jsonData.Status.ToString();
                        punit.Message = jsonData.Message.ToString();
                        pdata.Result.Add(punit);
                        ddl.UpdateRe_Charge(SNO, "1", JsonConvert.SerializeObject(pdata));
                        ddl.InsertRe_Charge_Data(SNO, jsonData.Result.ToString());
                        ///GenInvoiceNum找不到資料
                        string invnum = dInv.GenInvoiceNum(DateTime.Now.Year.ToString(), DateTime.Now.Month.ToString());
                        dInv.Insert2InvoiceRecord(invnum, SNO, "0");
                        dalr.UpdReservationInvNum(invnum, SNO);

                        string wtfMemo = dalr.getMemo(SNO);
                        string reMemo = DateTime.Now.ToString() + " " + getSession("userName") + " 金額:" + R.TrailPrice ;
                        if (wtfMemo != "")
                        {
                            string nwtfMemo = wtfMemo + Environment.NewLine + reMemo;
                            dalr.UpReservation_charge_list(SNO, R.TrailPrice, nwtfMemo);
                        }
                        else
                        {
                            dalr.UpReservation_charge_list(SNO, R.TrailPrice, reMemo);
                        }

                        return "SUCCESS";
                    }
                    else
                    {
                        //DAL.DALReservation dalr = new DAL.DALReservation(ConnectionString);
                        //dalr.UpdReservationProStage(jsonData.Status.ToString(), SNO);
                        DAL.DALGeneral ddl = new DAL.DALGeneral(ConnectionString);
                        OPay2goResult pdata = new OPay2goResult();
                        int index = 1;
                        if (ddl.Re_Charge(SNO).ToString() != "")
                        {
                            OPay2goResult OD = JsonConvert.DeserializeObject<OPay2goResult>(ddl.Re_Charge(SNO).ToString());

                            for (int i = 0; i < OD.Result.Count(); i++)
                            {
                                OPay2goResultUnit punitO = new OPay2goResultUnit();
                                int temp = Convert.ToInt32(OD.Result[i].Index.ToString());
                                punitO.Index = (temp+1).ToString();
                                punitO.Status = OD.Result[i].Status.ToString();
                                punitO.Message = OD.Result[i].Message.ToString();
                                pdata.Result.Add(punitO);
                            }
                        }
                        OPay2goResultUnit punit = new OPay2goResultUnit();
                        punit.Index = index.ToString();
                        punit.Status = jsonData.Status.ToString();
                        punit.Message = jsonData.Message.ToString();
                        pdata.Result.Add(punit);
                        ddl.UpdateRe_Charge(SNO,  "X", JsonConvert.SerializeObject(pdata));
                        return jsonData.Message.ToString();
                    }

                }
                catch (Exception ex)
                {
                    return ex.ToString();
                }

            }
            return "無訂單資料";

        }
        public JsonResult Go2Callcarpay(string SNO, int CAMT,string DATE,string memo)//使用授權碼刷卡
        {
            BALReservation bal = new BALReservation(ConnectionString);
            Dictionary<string, dynamic> JsonResult = new Dictionary<string, dynamic>();
            List<dynamic> data = new List<dynamic>();
            OReservation R = bal.GetReservationByNo(SNO);
            if (R != null)
            {
                try
                {                   
                    if (R.Credit.ToString() == "")
                    {
                        //return "無信用卡資料!!";
                    }
                    dynamic search = JObject.Parse(R.Credit);

                    DAL.DALUserAccount dalAcc = new DAL.DALUserAccount(ConnectionString);

                    OAccount UserInfo = dalAcc.GetID(search.UID.Value.ToString());
                    DAL.DALReservation dalr = new DAL.DALReservation(ConnectionString);
                    dalr.UpReservation_charge_list2(SNO, CAMT, memo);
                    Dictionary<string, dynamic> a = new Dictionary<string, dynamic>();

                    a.Add("Message", "SUCCESS");

                    data.Add(a);
                    JsonResult.Add("data", data);


                    return Json(JsonResult);

                }
                catch (Exception ex)
                {
                    return Json(JsonResult);
                }

            }
            return Json(JsonResult);

        }
        public string Go2Credit2(string SNO, string DATE, int AMT, string MEMO)//使用授權碼刷卡
        {
            BALReservation bal = new BALReservation(ConnectionString);


            OReservation R = bal.GetReservationByNo(SNO);
            if (R != null)
            {
                try
                {
                    if (AMT <= R.TrailPrice)
                    {

                        string HashKey = "FMJKUkbQ6pmh7bJyd1iOfRsmJyR0Neys";
                        string HashIV = "0hdVCpLJP81WjLvx";
                        if (R.Credit.ToString() == "")
                        {
                            return "無信用卡資料!!";
                        }

                        dynamic search = JObject.Parse(R.Credit);

                        DAL.DALUserAccount dalAcc = new DAL.DALUserAccount(ConnectionString);

                        OAccount UserInfo = dalAcc.GetID(search.UID.Value.ToString());

                        WebRequest tRequest;
                        tRequest = WebRequest.Create("https://core.spgateway.com/API/CreditCard");
                        tRequest.Method = "post";
                        tRequest.ContentType = " application/x-www-form-urlencoded;charset=UTF-8";
                        string Version = "1.0";
                        string TokenSwitch = "on";
                        string postData = "MerchantID_=MS31451783&PostData_=" + EncryptAES256("TimeStamp=" + DATE + "&Version=" + Version + "&MerchantOrderNo=" + SNO + DATE + "&Amt=" + AMT + "&ProdDesc=" + "乘車費" + "&PayerEmail=" + UserInfo.Email.ToString() + "&TokenValue=" + search.t.Value + "&TokenTerm=" + search.UID.Value + "&TokenSwitch=" + TokenSwitch, HashKey, HashIV) + "&Pos_=JSON";

                        Console.WriteLine(postData);

                        Byte[] byteArray = Encoding.UTF8.GetBytes(postData);
                        tRequest.ContentLength = byteArray.Length;

                        Stream dataStream = tRequest.GetRequestStream();
                        dataStream.Write(byteArray, 0, byteArray.Length);
                        dataStream.Close();

                        WebResponse tResponse = tRequest.GetResponse();

                        dataStream = tResponse.GetResponseStream();

                        StreamReader tReader = new StreamReader(dataStream);

                        String sResponseFromServer = tReader.ReadToEnd();


                        tReader.Close();
                        dataStream.Close();
                        tResponse.Close();
                        dynamic jsonData = Helper.jsonDecode(sResponseFromServer);
                        if (jsonData.Status == "SUCCESS")
                        {
                            //dynamic ODR = JObject.Parse(jsonData.Result.Replace("{{", "{").Replace("}}", "}"));

                            DAL.DALReservation dalr = new DAL.DALReservation(ConnectionString);
                            DAL.DALGeneral ddl = new DAL.DALGeneral(ConnectionString);
                            DAL.DALInvoice dInv = new DAL.DALInvoice(ConnectionString);
                            dalr.UpdReservationProStage(SNO);

                            OPay2goResult pdata = new OPay2goResult();
                            if (ddl.Re_Charge(SNO).ToString() != "")
                            {
                                OPay2goResult OD = JsonConvert.DeserializeObject<OPay2goResult>(ddl.Re_Charge(SNO).ToString());

                                for (int i = 0; i < OD.Result.Count(); i++)
                                {
                                    OPay2goResultUnit punitO = new OPay2goResultUnit();
                                    int temp = Convert.ToInt32(OD.Result[i].Index.ToString());
                                    punitO.Index = (temp + 1).ToString();
                                    punitO.Status = OD.Result[i].Status.ToString();
                                    punitO.Message = OD.Result[i].Message.ToString();
                                    pdata.Result.Add(punitO);
                                }
                            }
                            OPay2goResultUnit punit = new OPay2goResultUnit();
                            punit.Index = "1";
                            punit.Status = jsonData.Status.ToString();
                            punit.Message = jsonData.Message.ToString();
                            pdata.Result.Add(punit);
                            ddl.UpdateRe_Charge(SNO, "1", JsonConvert.SerializeObject(pdata));
                            ddl.InsertRe_Charge_Data(SNO, jsonData.Result.ToString());
                            ///GenInvoiceNum找不到資料
                            string invnum = dInv.GenInvoiceNum(DateTime.Now.Year.ToString(), DateTime.Now.Month.ToString());
                            dInv.Insert2InvoiceRecord(invnum, SNO, "0");
                            dalr.UpdReservationInvNum(invnum, SNO);
                            string wtfMemo = dalr.getMemo(SNO);
                            string reMemo = DateTime.Now.ToString() + " " + getSession("userName") + " 金額:" + AMT + MEMO;
                            if (wtfMemo != "")
                            {
                                string nwtfMemo = wtfMemo + Environment.NewLine + reMemo;
                                dalr.UpReservation_charge_list(SNO, AMT, nwtfMemo);
                            }
                            else
                            {
                                dalr.UpReservation_charge_list(SNO, AMT, reMemo);
                            }


                            return "SUCCESS";
                        }
                        else
                        {
                            //DAL.DALReservation dalr = new DAL.DALReservation(ConnectionString);
                            //dalr.UpdReservationProStage(jsonData.Status.ToString(), SNO);
                            DAL.DALGeneral ddl = new DAL.DALGeneral(ConnectionString);
                            OPay2goResult pdata = new OPay2goResult();
                            int index = 1;
                            if (ddl.Re_Charge(SNO).ToString() != "")
                            {
                                OPay2goResult OD = JsonConvert.DeserializeObject<OPay2goResult>(ddl.Re_Charge(SNO).ToString());

                                for (int i = 0; i < OD.Result.Count(); i++)
                                {
                                    OPay2goResultUnit punitO = new OPay2goResultUnit();
                                    int temp = Convert.ToInt32(OD.Result[i].Index.ToString());
                                    punitO.Index = (temp + 1).ToString();
                                    punitO.Status = OD.Result[i].Status.ToString();
                                    punitO.Message = OD.Result[i].Message.ToString();
                                    pdata.Result.Add(punitO);
                                }
                            }
                            OPay2goResultUnit punit = new OPay2goResultUnit();
                            punit.Index = index.ToString();
                            punit.Status = jsonData.Status.ToString();
                            punit.Message = jsonData.Message.ToString();
                            pdata.Result.Add(punit);
                            ddl.UpdateRe_Charge(SNO, "X", JsonConvert.SerializeObject(pdata));
                            return jsonData.Message.ToString();
                        }
                    }
                    else
                    {
                        return "刷卡金額不可大於訂單金額!!";
                    }
                }
                catch (Exception ex)
                {
                    return ex.ToString();
                }

            }
            return "無訂單資料";

        }

        public JsonResult Go2Pay2(string SNO, string DATE, int AMT, string MEMO)//授權碼，有效日期更新
        {
            string[] sArray = SNO.Split(',');
            BALReservation bal = new BALReservation(ConnectionString);
            Dictionary<string, dynamic> JsonResult = new Dictionary<string, dynamic>();
            List<dynamic> data = new List<dynamic>();
            foreach (string i in sArray)
            {
                if (i != "")
                {

                    // OReservation R = bal.GetReservationByNo(i);
                    // if (R != null)
                    // {
                    //dynamic search = JObject.Parse(R.Credit);
                    Dictionary<string, dynamic> a = new Dictionary<string, dynamic>();

                    a.Add("Message", Go2Credit2(i, DATE,AMT,MEMO));

                    data.Add(a);

                    //}

                }
            }
            JsonResult.Add("data", data);

            return Json(JsonResult);

        }
        public JsonResult Go2Pay(string paydata, string DATE)//授權碼，有效日期更新
        {
            string[] sArray = paydata.Split(',');
            BALReservation bal = new BALReservation(ConnectionString);
            Dictionary<string, dynamic> JsonResult = new Dictionary<string, dynamic>();
            List<dynamic> data = new List<dynamic>();
            foreach (string i in sArray)
            {
                if (i != "")
                {

                    // OReservation R = bal.GetReservationByNo(i);
                    // if (R != null)
                    // {
                    //dynamic search = JObject.Parse(R.Credit);
                    Dictionary<string, dynamic> a = new Dictionary<string, dynamic>();

                    a.Add("Message", Go2Credit(i, DATE));

                    data.Add(a);
                  
                    //}

                }
            }
            JsonResult.Add("data", data);

            return Json(JsonResult);

        }

        public JsonResult wtCreditD(string id)
        {
            DAL.DALGeneral ddl = new DAL.DALGeneral(ConnectionString);
            BALReservation bal = new BALReservation(ConnectionString);
            OReservation R = bal.GetReservationByNo(id);

            Dictionary<string, dynamic> JsonResult = new Dictionary<string, dynamic>();
            if (ddl.Re_Charge(id).ToString() != "")
            {
                OPay2goResult OD = JsonConvert.DeserializeObject<OPay2goResult>(ddl.Re_Charge(id).ToString());
                List<dynamic> data = new List<dynamic>();
                for (int i = 0; i < OD.Result.Count(); i++)
                {

                        dynamic search = JObject.Parse(R.Credit);

                        DAL.DALUserAccount dalAcc = new DAL.DALUserAccount(ConnectionString);
                        string IDMD5;
                        IDMD5 = Encrypt(search.UID.Value.ToString());
                        string cardno = Decrypt(search.CNo.Value.ToString(), IDMD5.Substring(0, 16), IDMD5.Substring(16, 16));

                    Dictionary<string, dynamic> a = new Dictionary<string, dynamic>();
                    string card4 = cardno.Substring(0, 4);
                    string card12 = cardno.Substring(12, 4);
                    string mmcard = card4 + "-********-" + card12;
                    a.Add("CardNo", mmcard.ToString());
                    a.Add("Status", OD.Result[i].Status.ToString()); //:"20170118", //搭乘日期
                    a.Add("Message", OD.Result[i].Message.ToString()); //:"20170118", //搭乘日期

                    data.Add(a);
                }
                JsonResult.Add("data", data);
            }
            else
            {
                JsonResult.Add("data", "nodata");
            }

            return Json(JsonResult, JsonRequestBehavior.AllowGet);
        }
        public static string Encrypt(string PlanText)
        {
            byte[] bytes = Encoding.Default.GetBytes(PlanText);
            byte[] bytes2 = Encoding.Default.GetBytes("callcar");
            byte[] array = new byte[bytes.Length + bytes2.Length];
            bytes.CopyTo(array, 0);
            bytes2.CopyTo(array, bytes.Length);
            MD5 mD = MD5.Create();
            byte[] array2 = mD.ComputeHash(array);
            byte[] array3 = new byte[array2.Length + bytes2.Length];
            array2.CopyTo(array3, 0);
            bytes2.CopyTo(array3, array2.Length);
            return Convert.ToBase64String(array3);
        }

        public string Decrypt(string CipherText,string key, string iv)
        {
            byte[] bytes = Encoding.UTF8.GetBytes(key);
            byte[] bytes2 = Encoding.UTF8.GetBytes(iv);
            byte[] cipherText = Convert.FromBase64String(CipherText);
            return DecryptStringFromBytes(cipherText, bytes, bytes2);
        }

        private static string DecryptStringFromBytes(byte[] cipherText, byte[] key, byte[] iv)
        {
            if (cipherText == null || cipherText.Length == 0)
            {
                throw new ArgumentNullException("cipherText");
            }
            if (key == null || key.Length == 0)
            {
                throw new ArgumentNullException("key");
            }
            if (iv == null || iv.Length == 0)
            {
                throw new ArgumentNullException("key");
            }
            string result = null;
            using (RijndaelManaged rijndaelManaged = new RijndaelManaged())
            {
                rijndaelManaged.Mode = CipherMode.CBC;
                rijndaelManaged.Padding = PaddingMode.PKCS7;
                rijndaelManaged.FeedbackSize = 128;
                rijndaelManaged.Key = key;
                rijndaelManaged.IV = iv;
                ICryptoTransform transform = rijndaelManaged.CreateDecryptor(rijndaelManaged.Key, rijndaelManaged.IV);
                try
                {
                    using (MemoryStream memoryStream = new MemoryStream(cipherText))
                    {
                        using (CryptoStream cryptoStream = new CryptoStream(memoryStream, transform, CryptoStreamMode.Read))
                        {
                            using (StreamReader streamReader = new StreamReader(cryptoStream))
                            {
                                result = streamReader.ReadToEnd();
                            }
                        }
                    }
                }
                catch (Exception)
                {
                    result = "keyError";
                }
            }
            return result;
        }

        public string EncryptAES256(string source, string sSecretKey, string iv)//加密
        {
            byte[] sourceBytes = AddPKCS7Padding(Encoding.UTF8.GetBytes(source),
            32);
            var aes = new RijndaelManaged();
            aes.Key = Encoding.UTF8.GetBytes(sSecretKey);
            aes.IV = Encoding.UTF8.GetBytes(iv);
            aes.Mode = CipherMode.CBC;
            aes.Padding = PaddingMode.None;
            ICryptoTransform transform = aes.CreateEncryptor();
            return ByteArrayToHex(transform.TransformFinalBlock(sourceBytes, 0,
            sourceBytes.Length)).ToLower();
        }

        private static byte[] AddPKCS7Padding(byte[] data, int iBlockSize)
        {
            int iLength = data.Length;
            byte cPadding = (byte)(iBlockSize - (iLength % iBlockSize));
            var output = new byte[iLength + cPadding];
            Buffer.BlockCopy(data, 0, output, 0, iLength);
            for (var i = iLength; i < output.Length; i++)
                output[i] = (byte)cPadding;
            return output;
        }
        private static string ByteArrayToHex(byte[] barray)
        {
            char[] c = new char[barray.Length * 2];
            byte b;
            for (int i = 0; i < barray.Length; ++i)
            {
                b = ((byte)(barray[i] >> 4));
                c[i * 2] = (char)(b > 9 ? b + 0x37 : b + 0x30);
                b = ((byte)(barray[i] & 0xF));
                c[i * 2 + 1] = (char)(b > 9 ? b + 0x37 : b + 0x30);
            }
            return new string(c);
        }

        public static string TokenAnalysis(string data)
        {
            if (data.Length <= 16)
            {
                throw new Exception("Data Error");
            }
            string token = data.Substring(data.Length - 16, 16);
            data = data.Substring(0, data.Length - 16);
            return token;
        }


        //重新取有效信用卡token
        /*                        DateTime NewDate = DateTime.ParseExact(search.d.Value, "yyyyMMdd", null, System.Globalization.DateTimeStyles.AllowWhiteSpaces);
                        if (NewDate < DateTime.Now)
                        {
                            try
                            {
                                string HashKey = "ax9hhIc6W3oOCEn0GzxzMW7zwKKczmNJ";
                                string HashIV = "UW1bFeiChGrTlOU1";
                                WebRequest tRequest;
                                tRequest = WebRequest.Create("https://ccore.spgateway.com/API/CreditCard");
                                tRequest.Method = "post";
                                tRequest.ContentType = " application/x-www-form-urlencoded;charset=UTF-8";
                                string Version = "1.0";
                                string TokenSwitch = "get";
                                string CEXP = "";
                                if (search.Expire.Value.Length > 4)
                                {
                                    CEXP = search.Expire.Value.Substring(search.Expire.Value.Length - 4);
                                }
                                else
                                    CEXP = search.Expire.Value;
                                string postData = "MerchantID_=MS3446346&PostData_=" + EncryptAES256("TimeStamp=" + DATE + "&Version=" + Version + "&MerchantOrderNo=" + i + "z" + "&Amt=" + "1" + "&ProdDesc=" + "乘車費" + "&PayerEmail=" + "jonathan_chiu@cablesoft.com.tw" + "&CardNo=" + search.CNo.Value + "&Exp=" + CEXP + "&CVC=" + search.Code.Value + "&TokenSwitch=" + TokenSwitch + "&TokenTerm=" + "testby20170303", HashKey, HashIV) + "&Pos_=JSON";

                                Console.WriteLine(postData);
                                Byte[] byteArray = Encoding.UTF8.GetBytes(postData);
                                tRequest.ContentLength = byteArray.Length;

                                Stream dataStream = tRequest.GetRequestStream();
                                dataStream.Write(byteArray, 0, byteArray.Length);
                                dataStream.Close();

                                WebResponse tResponse = tRequest.GetResponse();

                                dataStream = tResponse.GetResponseStream();

                                StreamReader tReader = new StreamReader(dataStream);

                                String sResponseFromServer = tReader.ReadToEnd();


                                tReader.Close();
                                dataStream.Close();
                                tResponse.Close();

                                dynamic jsonData = Helper.jsonDecode(sResponseFromServer);

                                //OriginalData OD = JsonConvert.DeserializeObject<OriginalData>(sResponseFromServer);
                                if (jsonData.Status == "SUCCESS")
                                {
                                    //dynamic ODR = JObject.Parse(jsonData.Result.Replace("{{","{").Replace("}}","}"));

                                    search.t.Value = jsonData.Result.TokenValue.ToString().Replace("{", "").Replace("}", "");
                                    search.d.Value = jsonData.Result.TokenLife.ToString().Replace("{", "").Replace("}", "");
                                    string cardData = search.ToString();
                                    DAL.DALReservation dalr = new DAL.DALReservation(ConnectionString);

                                    dalr.UpdReservationTokenValueandEXP(cardData.Trim(), i);
                                    Go2Credit(i, DATE);
                                    Dictionary<string, dynamic> b = new Dictionary<string, dynamic>();
                                    b.Add("Status", jsonData.Status.ToString());
                                    b.Add("Message", jsonData.Message.ToString());

                                    data.Add(b);
                                }
                                else
                                {

                                    Dictionary<string, dynamic> a = new Dictionary<string, dynamic>();

                                    //dynamic zzz = Helper.jsonDecode(item);
                                    //a.Add("zzz", zzz);

                                    a.Add("Status", jsonData.Status.ToString());
                                    a.Add("Message", jsonData.Message.ToString()); 

                                    data.Add(a);
                                }

                            }
                            catch (Exception ex)
                            {
                                ex.ToString();
                            }
                        }*/
        
    }
}