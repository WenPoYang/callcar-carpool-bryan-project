﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using CallCar.Models;
using CallCar.BAL;
using System.Configuration;
using System.Data;
using System.IO;
using System.Net;
using System.Net.Mime;
using System.Security.Cryptography;
using ClosedXML.Excel;
using CallCar.Infrastructure;
using System.Text;

namespace CallCar.Controllers
{
    public class ReportController : Controller
    {
        // GET: Report
        public ActionResult Index()
        {
            return View();
        }
        public static string ConnectionString = ConfigurationManager.ConnectionStrings["CarpoolLab"].ConnectionString;
        public ActionResult GetList(int page ,string searchJson = null,  string orderField = "", string orderType = "", int itemPerPage = 10, string Partner = null)
        {

            if (itemPerPage > 100)
            {
                itemPerPage = 100;
            }

            Dictionary<string, dynamic> JsonResult = new Dictionary<string, dynamic>();

            try
            {

                string QSDate = "";
                string QEDate = "";
              

                if (searchJson != null)
                {
                    try
                    {

                        dynamic search = JObject.Parse(searchJson);

                        QSDate = search.QSDate;
                        QEDate = search.QEDate;                      

                    }
                    catch (Exception e)
                    {
                        e.ToString();
                    }

                }

                Dictionary<string, dynamic> parameters = new Dictionary<string, dynamic>();


                parameters.Add("QSDate", QSDate); //:"20170101",//查詢派車的開始日期
                parameters.Add("QEDate", QEDate); //:"20170102",//查詢派車的結束日期

                string dataString = Helper.jsonEncode(parameters);
      

                DAL.DALReservation Reservation = new DAL.DALReservation(ConnectionString);
                QOrderCondStruct qs = JsonConvert.DeserializeObject<QOrderCondStruct>(dataString);
                DataTable dt = Reservation.GetReport(Partner, QSDate, QEDate);

                int countSkip = itemPerPage * (page - 1);
                int countTake = itemPerPage;

                int TotalItem = 0;

                //RepStarDateTime = "2016-12-20";
                //RepEndDateTime = "2017-01-04";
                TotalItem = dt.Rows.Count;
                List<dynamic> data = new List<dynamic>();
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        Dictionary<string, dynamic> a = new Dictionary<string, dynamic>();
                        a.Add("rno", Convert.ToString(dt.Rows[i]["編號"]));
                        a.Add("st", Convert.ToString(dt.Rows[i]["出回國"]));
                        a.Add("td", Convert.ToString(dt.Rows[i]["乘車日"]));
                        a.Add("pc", Convert.ToString(dt.Rows[i]["人數"]));
                        a.Add("coupon", Convert.ToString(dt.Rows[i]["優惠碼"]));
                        a.Add("ps", Convert.ToString(dt.Rows[i]["進度"]));

                        data.Add(a);
                    }
                }



                JsonResult.Add("data", data);

                double PageTotal = (double)TotalItem / itemPerPage;
                PageTotal = Math.Ceiling(PageTotal);

                JsonResult.Add("totalItem", TotalItem);
                JsonResult.Add("pageTotal", PageTotal);
                JsonResult.Add("nowpage", page);
            }
            catch (Exception e)
            {
                e.ToString();
            }


            return Json(JsonResult);

        }

        [HttpPost]
        public HttpStatusCodeResult CreateExcel(string searchJson, string Partner)
        {
            XLWorkbook wb = new XLWorkbook(XLEventTracking.Disabled); //create Excel

            string QSDate = "";
            string QEDate = "";


            if (searchJson != null)
            {
                try
                {

                    dynamic search = JObject.Parse(searchJson);

                    QSDate = search.QSDate;
                    QEDate = search.QEDate;


                }
                catch (Exception e)
                {
                    e.ToString();
                }

            }
            Dictionary<string, dynamic> JsonResult = new Dictionary<string, dynamic>();
            DataTable dt = null;
            RtnStruct r = new RtnStruct();
            DAL.DALReservation Reservation = new DAL.DALReservation(ConnectionString);

           
            try
            {

                DataTable at = Reservation.GetReport(Partner, QSDate, QEDate);
                dt = new DataTable();
                dt.Columns.Add("編號");
                dt.Columns.Add("出回國");
                dt.Columns.Add("乘車日");
                dt.Columns.Add("人數");
                dt.Columns.Add("優惠碼");
                dt.Columns.Add("進度");
                if (at.Rows.Count > 0)
                {
                    for (int i = 0; i < at.Rows.Count; i++)
                    {
                        DataRow dataRow = dt.NewRow();
                        dataRow["編號"] = Convert.ToString(at.Rows[i]["編號"]);
                        if (Convert.ToString(at.Rows[i]["出回國"]) == "I")
                            dataRow["出回國"] = "回國";
                        else if (Convert.ToString(at.Rows[i]["出回國"]) == "O")
                            dataRow["出回國"] = "出國";
                        else
                            dataRow["出回國"] = "未知";
                        dataRow["乘車日"] = Convert.ToString(at.Rows[i]["乘車日"]);
                        dataRow["人數"] = Convert.ToString(at.Rows[i]["人數"]);
                        dataRow["優惠碼"] = Convert.ToString(at.Rows[i]["優惠碼"]);
                        dataRow["進度"] = Convert.ToString(at.Rows[i]["進度"]);
      

                        dt.Rows.Add(dataRow);
                    }
                }
            }
            catch (Exception e)
            {
                e.ToString();
            }
            wb.AddWorksheet(dt, "Sheet1");
            if (wb != null)
            {
                Session["ExcelResult"] = wb;
                return new HttpStatusCodeResult(HttpStatusCode.OK);
            }

            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

        }

        [HttpGet]
        public FileContentResult ExcelResult(string reportHeader) //it's your data passed to controller
        {

            byte[] fileBytes = GetExcel((XLWorkbook)Session["ExcelResult"]);
            var exportFileName = string.Concat(
                "Report-",
                DateTime.Now.ToString("yyyyMMdd"),
                ".xlsx");
            return File(fileBytes, MediaTypeNames.Application.Octet, exportFileName);
        }
        public static byte[] GetExcel(XLWorkbook wb)
        {
            using (var ms = new MemoryStream())
            {
                wb.SaveAs(ms);
                return ms.ToArray();
            }
        }
    }
}