﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(CallCar.Startup))]
namespace CallCar
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
