﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DbConn;
using MySql.Data.MySqlClient;
using System.Data;


namespace DispatchGen
{
    public class DispatchGenFunction
    {
        private string _connectionstring;
        public DispatchGenFunction(string ConnectionString) { this._connectionstring = ConnectionString; }

        public MatchHolidayInfo GetHoliday(string QueryDate)
        {
            MatchHolidayInfo info = new MatchHolidayInfo();
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            DataTable dt;

            sb.Append("SELECT LockDate, BeginDate, EndDate 	FROM match_holiday_data 	where LockDate = @date");
            parms.Add(new MySqlParameter("@date", QueryDate));

            using (dbConnectionMySQL conn = new dbConnectionMySQL(this._connectionstring))
            {
                dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
            }

            if (dt.Rows.Count == 1)
            {
                info.LockDate = QueryDate;
                info.BeginDate = Convert.ToString(dt.Rows[0]["BeginDate"]);
                info.EndDate = Convert.ToString(dt.Rows[0]["EndDate"]);
                return info;
            }
            else
                return null;
        }

        public bool isAlreadyMatched(string MatchDate)
        {
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            sb.Append("select count(1) FROM  match_record  WHERE MatchDate  = @MatchDate");
            parms.Add(new MySqlParameter("@MatchDate", MatchDate));

            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._connectionstring))
                {
                    DataTable dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());

                    if (Convert.ToInt32(dt.Rows[0][0]) == 0)
                        return false;
                    else
                        return true;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public OMachSourceData GetMatchSourceByDate(string date)
        {
            OMachSourceData Match = new OMachSourceData();
            DataTable dt = new DataTable();

            try
            {
                dt = GetReservationForMatch(date);
            }
            catch (Exception e)
            {
                throw e;
            }

            try
            {
                Match.MatchDate = date;

                if (dt.Rows.Count > 0)
                {

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        OMatchRData m = new OMatchRData();
                        m.ReservationNo = Convert.ToString(dt.Rows[i]["ReservationNo"]);
                        m.ServiceType = Convert.ToString(dt.Rows[i]["ServiceType"]);
                        m.TimeSegment = Convert.ToString(dt.Rows[i]["TimeSegment"]);
                        m.BaggageCnt = Convert.ToInt32(dt.Rows[i]["BaggageCnt"]);
                        m.PassengerCnt = Convert.ToInt32(dt.Rows[i]["PassengerCnt"]);
                        m.PickupLat = Convert.ToDouble(dt.Rows[i]["plng"]);
                        m.PickupLng = Convert.ToDouble(dt.Rows[i]["plat"]);
                        m.TakeoffLat = Convert.ToDouble(dt.Rows[i]["tlat"]);
                        m.TakeoffLng = Convert.ToDouble(dt.Rows[i]["tlng"]);
                        m.MaxFlag = Convert.ToString(dt.Rows[i]["MaxFlag"]);

                        if (m.ServiceType == "O")
                        {
                            if (m.TimeSegment.Substring(2, 2) == "15" || m.TimeSegment.Substring(2, 2) == "30" || m.TimeSegment.Substring(2, 2) == "00")
                                m.MatchRegion = m.TimeSegment.Substring(0, 2) + "00";
                            else if (m.TimeSegment.Substring(2, 2) == "45")
                            {
                                int hour;
                                hour = Convert.ToInt32(m.TimeSegment.Substring(0, 2)) + 1;
                                if (hour == 24)
                                    m.MatchRegion = m.TimeSegment.Substring(0, 2) + "00";
                                else
                                    m.MatchRegion = hour.ToString().PadLeft(2, '0') + "00";
                            }
                        }
                        else
                        {
                            if (m.TimeSegment.Substring(2, 2) == "15" || m.TimeSegment.Substring(2, 2) == "00")
                                m.MatchRegion = m.TimeSegment.Substring(0, 2) + "00";
                            else if (m.TimeSegment.Substring(2, 2) == "45" || m.TimeSegment.Substring(2, 2) == "30")
                            {
                                int hour;
                                hour = Convert.ToInt32(m.TimeSegment.Substring(0, 2)) + 1;
                                if (hour == 24)
                                    m.MatchRegion = m.TimeSegment.Substring(0, 2) + "00";
                                else
                                    m.MatchRegion = hour.ToString().PadLeft(2, '0') + "00";
                            }
                        }

                        Match.ReservationList.Add(m);
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return Match;
        }

        /// <summary>
        /// 將試搓結果計算金額 寫入試搓價檔
        /// </summary>
        /// <param name="DispatchDate"></param>
        /// <param name="MatchResult"></param>
        private DataTable GetReservationForMatch(string sdate)
        {

            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            sb.Append("SELECT ReservationNo, UserID, ServiceType, TakeDate, TimeSegment, SelectionType, PickCartype, FlightNo, ScheduleFlightTime, PickupCity, ");
            sb.Append("PickupDistinct, PickupVillage, PickupAddress, X(PickupGPS) as plng, Y(PickupGPS) as plat, TakeoffCity, TakeoffDistinct, TakeoffVillage, TakeoffAddress, X(TakeoffGPS) as tlng ,Y(TakeoffGPS) as tlat , PassengerCnt, ");
            sb.Append("BaggageCnt, MaxFlag, Price, Coupon, InvoiceData, CreditData, ProcessStage, InvoiceNum, CreateTime, UpdTime ");
            sb.Append("FROM reservation_sheet where TakeDate = @sdate  AND ProcessStage = '0' AND  CarpoolFlag = 'S'  and PassengerCnt < 5 order by TakeDate,TimeSegment asc ");

            parms.Add(new MySqlParameter("@sdate", sdate));
            DataTable dt = new DataTable();
            //List<OReservation> List = new List<OReservation>();
            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._connectionstring))
                {
                    return dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                }
            }
            catch (MySqlException ex)
            {
                throw ex;
            }
        }

      
    }

    public class MatchHolidayInfo
    {
        public string LockDate { get; set; }
        public string BeginDate { get; set; }
        public string EndDate { get; set; }
    }

    public class OMachSourceData
    {
        public OMachSourceData()
        {

            this.ReservationList = new List<OMatchRData>();
            this.CarList = new List<OMatchCData>();
        }

        public string MatchDate { get; set; }
        public List<OMatchRData> ReservationList { get; set; }
        public List<OMatchCData> CarList { get; set; }
    }

    public class OMatchRData
    {
        public string ReservationNo { get; set; }
        public string ServiceType { get; set; }
        public string TimeSegment { get; set; }
        public string MatchRegion { get; set; }
        public double PickupLat { get; set; }
        public double PickupLng { get; set; }
        public double TakeoffLat { get; set; }
        public double TakeoffLng { get; set; }
        public int PassengerCnt { get; set; }
        public int BaggageCnt { get; set; }
        public string MaxFlag { get; set; }
    }

    public class OMatchCData
    {
        public int ScheduleID { get; set; }
        public string ShiftType { get; set; }
        public string CarType { get; set; }
        public int NCapacity { get; set; }
        public int MCapacity { get; set; }
    }

    public class MatchResultJsonStruct
    {
        public List<DispatchInfo> DispatchInfo { get; set; }
    }

    public class DispatchInfo
    {
        public DispatchInfo()
        {
            this.Detail = new Detail();
        }
        public int Index { get; set; }
        public Detail Detail { get; set; }
    }

    public class Detail
    {
        public Detail()
        {
            this.ServiceList = new List<ServiceList>();
        }
        public string ServiceDate { get; set; }
        public string ServiceType { get; set; }
        public int ScheduleID { get; set; }
        public string TimeSegment { get; set; }
        public int ServiceCnt { get; set; }
        public int PassengerCnt { get; set; }
        public int BaggageCnt { get; set; }
        public string MaxFlag { get; set; }
        public List<ServiceList> ServiceList { get; set; }
    }

    public class ServiceList
    {
        public string ReservationNo { get; set; }
        public string ServiceTime { get; set; }
    }
}
