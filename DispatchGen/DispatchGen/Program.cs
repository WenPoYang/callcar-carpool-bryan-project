﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.IO;
using System.Net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Text.RegularExpressions;
using Callcar.CObject;
using Callcar.Struct;
using NLog;
using NLog.Config;
using System.Collections.Specialized;
using System.Configuration;
using System.Data.SqlClient;

namespace DispatchGen
{
    class Program
    {
        private static string SourceUrl;
        private static string ConnectionString;
        private static string DispatchUrl;
        private static Logger logger = NLog.LogManager.GetCurrentClassLogger();
        private static DispatchGenFunction func;

        static void Main(string[] args)
        {
            try
            {
                NameValueCollection nc = ConfigurationManager.AppSettings;
                if (nc["Server"] == "Production")
                {
                    //SourceUrl = @"https://api2.ishareco.com/bapi/rest/GetMatchSource?date=";
                    ConnectionString = "Server=10.0.2.20;Port=3306;Database=carpoollab;Uid=ishareco;Pwd=fu0!j0$su0^vup#;Allow User Variables=True";
                    DispatchUrl = @"http://10.0.2.16:3000/api/match";
                }
                else if (nc["Server"] == "Lab")
                {
                    //SourceUrl = @"http://apid.ishareco.com/bapi/rest/GetMatchSource?date=";
                    ConnectionString = "Server=10.0.2.21;Port=3306;Database=carpoollab;Uid=apiuser;Pwd=7oPOh88I;Allow User Variables=True";
                    DispatchUrl = @"http://10.0.2.22:3000/api/match";
                }

                /*
                 * 加入連續假日提前媒合 20171229
                 * 檢查當日是否為連續假日截止預約日 
                 * 如果是 先媒合連續假期的訂單
                 * 再依序媒合 D+7(試搓)~D+2(正式)
                */
                DateTime Today = DateTime.Now;
                DateTime MatchDate;
                //Step 檢查是否有連續假日
                func = new DispatchGen.DispatchGenFunction(ConnectionString);
                MatchHolidayInfo info = func.GetHoliday(Today.ToString("yyyyMMdd"));
                Callcar.DAL.DALDispatch dald = new Callcar.DAL.DALDispatch(ConnectionString);
                if (info != null)
                {
                    DateTime HolidayBeginDate = DateTime.Parse(info.BeginDate.Substring(0, 4) + "/" + info.BeginDate.Substring(4, 2) + "/" + info.BeginDate.Substring(6, 2));
                    DateTime HolidayEndDate = DateTime.Parse(info.EndDate.Substring(0, 4) + "/" + info.EndDate.Substring(4, 2) + "/" + info.EndDate.Substring(6, 2));
                    MatchDate = HolidayEndDate;

                    while (MatchDate >= HolidayBeginDate.Date)
                    {
                        string Result = string.Empty, Json = string.Empty;
                        Result = Match(MatchDate.ToString("yyyyMMdd"));

                        if (Result == "")
                        {
                            try
                            {
                                dald.SaveMatchRecord(MatchDate.ToString("yyyyMMdd"), 0);
                            }
                            catch (Exception ex)
                            {
                                logger.Error(DateTime.Now.ToString("yyyyMMdd HH:mm:ss") + "   " + MatchDate.ToString("yyyyMMdd") + "媒合結果檔寫入失敗：" + ex.Message + Environment.NewLine + ex.StackTrace);
                                MatchDate = MatchDate.AddDays(-1).Date;
                                continue;
                            }

                            MatchDate = MatchDate.AddDays(-1).Date;
                            continue;

                        }
                        JObject jo = JsonConvert.DeserializeObject<JObject>(Result);
                        Json = jo["data"].ToString();
                        MatchResultJsonStruct MatchResult = JsonConvert.DeserializeObject<MatchResultJsonStruct>(Json);

                        //試搓 寫入價格檔
                        PreMatch(MatchDate.ToString("yyyyMMdd"), MatchResult);

                        //假日固定要寫入
                        WriteResult2DB(MatchDate.ToString("yyyyMMdd"), Json);

                        MatchDate = MatchDate.AddDays(-1).Date;
                    }
                }

                //D+7開始試搓  D+2 正式媒合需寫入資料庫
                DateTime BeginDate = DateTime.Now.AddDays(7).Date;
                DateTime OfficialDate = DateTime.Now.AddDays(2).Date;

                MatchDate = BeginDate;
                while (MatchDate >= OfficialDate.Date)
                {
                    if (func.isAlreadyMatched(MatchDate.ToString("yyyyMMdd")))
                    {
                        MatchDate = MatchDate.AddDays(-1).Date;
                        continue;
                    }
                    string Result = string.Empty, Json = string.Empty;
                    Result = Match(MatchDate.ToString("yyyyMMdd"));

                    if (Result == "")
                    {
                        try
                        {
                            if (MatchDate.Date == OfficialDate.Date)
                                dald.SaveMatchRecord(MatchDate.ToString("yyyyMMdd"), 0);
                        }
                        catch (Exception ex)
                        {
                            logger.Error(DateTime.Now.ToString("yyyyMMdd HH:mm:ss") + "   " + MatchDate.ToString("yyyyMMdd") + "媒合結果檔寫入失敗：" + ex.Message + Environment.NewLine + ex.StackTrace);
                            MatchDate = MatchDate.AddDays(-1).Date;
                            continue;
                        }
                        MatchDate = MatchDate.AddDays(-1).Date;
                        continue;
                    }
                    JObject jo = JsonConvert.DeserializeObject<JObject>(Result);
                    Json = jo["data"].ToString();
                    MatchResultJsonStruct MatchResult = JsonConvert.DeserializeObject<MatchResultJsonStruct>(Json);

                    //試搓 寫入價格檔
                    PreMatch(MatchDate.ToString("yyyyMMdd"), MatchResult);


                    //D+2日 將媒合寫入資料庫
                    if (MatchDate.Date == OfficialDate.Date)
                        WriteResult2DB(MatchDate.ToString("yyyyMMdd"), Json);

                    MatchDate = MatchDate.AddDays(-1).Date;
                }
            }
            catch (Exception ex)
            {
                logger.Error(DateTime.Now.ToString("yyyyMMdd HH:mm:ss") + "媒合失敗：  " + ex.Message + Environment.NewLine + ex.StackTrace);
            }
            finally
            {
                logger.Info(DateTime.Now.ToString("yyyyMMdd") + "媒合執行完成");
                Environment.Exit(0);
            }
        }

        /// <summary>
        /// 丟到allen的媒合程式
        /// </summary>
        /// <param name="DispatchDate"></param>
        /// <returns></returns>
        private static string Match(string DispatchDate)
        {

            try
            {
                OMachSourceData source = func.GetMatchSourceByDate(DispatchDate);
                if (source.ReservationList.Count == 0)
                    return "";

                string MatchJson = JsonConvert.SerializeObject(source);

                HttpWebRequest request = HttpWebRequest.Create(DispatchUrl) as HttpWebRequest;
                string result = null;
                request.Method = "POST";    // 方法
                request.KeepAlive = true; //是否保持連線
                request.ContentType = "application/json";

                byte[] bs = Encoding.ASCII.GetBytes(MatchJson);

                using (Stream Stream = request.GetRequestStream())
                {
                    Stream.Write(bs, 0, bs.Length);
                }

                using (WebResponse response = request.GetResponse())
                {
                    StreamReader sr = new StreamReader(response.GetResponseStream());
                    result = sr.ReadToEnd();
                    sr.Close();
                }
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// 將媒合結果寫入派車資料
        /// </summary>
        /// <param name="DispatchDate"></param>
        /// <param name="result"></param>
        /// <returns></returns>
        private static bool WriteResult2DB(string DispatchDate, string result)
        {
            try
            {
                int sheetCnt = 0;
                Callcar.BAL.BALDispatch bald = new Callcar.BAL.BALDispatch(ConnectionString);
                sheetCnt = bald.BatchAddDispatchSheet(DispatchDate, result);
                Callcar.DAL.DALDispatch dald = new Callcar.DAL.DALDispatch(ConnectionString);
                try
                {
                    dald.SaveMatchRecord(DispatchDate, sheetCnt);
                }
                catch (Exception)
                {
                    return true;
                }

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 將試搓結果計算金額 寫入試搓價檔
        /// </summary>
        /// <param name="DispatchDate"></param>
        /// <param name="MatchResult"></param>
        private static void PreMatch(string DispatchDate, MatchResultJsonStruct MatchResult)
        {
            int CarFee = 0, NightFee = 0, BagFee = 0;
            int PromotFee = 0, TotalPrice = 0, CarCnt = 0;
            Callcar.BAL.BALReservation balr = new Callcar.BAL.BALReservation(ConnectionString);
            Callcar.BAL.BALGeneral balg = new Callcar.BAL.BALGeneral(ConnectionString);
            Callcar.DAL.DALGeneral dalg = new Callcar.DAL.DALGeneral(ConnectionString);

            try
            {
                foreach (DispatchInfo di in MatchResult.DispatchInfo)
                {
                    foreach (ServiceList sl in di.Detail.ServiceList)
                    {
                        OReservation order = balr.GetReservationByNo(sl.ReservationNo);


                        CarCnt = di.Detail.PassengerCnt - order.PassengerCnt;
                        try
                        {
                            string FeeString = balg.CalculateTrailFee(order.OrderTime.ToString("yyyyMMdd"), order.ServeDate, order.PreferTime, order.PassengerCnt, order.BaggageCnt, order.Coupon, CarCnt, order.AddBagCnt, false);
                            string[] Fee = FeeString.Split('_');
                            Int32.TryParse(Fee[0], out CarFee);
                            Int32.TryParse(Fee[1], out NightFee);
                            Int32.TryParse(Fee[2], out BagFee);
                            Int32.TryParse(Fee[3], out PromotFee);
                            TotalPrice = CarFee + NightFee + BagFee - PromotFee;


                            dalg.InsertTrailFee(order.ReservationNo, TotalPrice);
                        }
                        catch (Exception ex)
                        {
                            logger.Error("Prematch 試算金額 Order：" + order.ReservationNo + "發生錯誤：" + ex.Message);
                            continue;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error("Prematch Date：" + DispatchDate + "發生錯誤：" + ex.Message);
                return;
            }
        }

        /// <summary>
        /// 偵測NLog檔案路徑及名稱 REF: http://stackoverflow.com/a/11629408
        /// </summary>
        /// <param name="targetName">目標名稱</param>
        /// <param name="loggerName">Logger名稱</param>
        /// <returns></returns>
        public static string ProbeNLogFilePath(string targetName, string loggerName = "LoggerName")
        {
            return (NLog.LogManager.Configuration.FindTargetByName(targetName) as NLog.Targets.FileTarget)
            .FileName.Render(new LogEventInfo() { TimeStamp = DateTime.Now, LoggerName = loggerName });
        }

    }
}
