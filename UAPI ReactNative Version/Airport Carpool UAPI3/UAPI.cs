﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.IO;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Text;
using CallCar.Data;
using CallCar.Reservation;
using CallCar.Payment.BAL;
using CallCar.Account;
using Newtonsoft.Json.Linq;
using CallCar.Tool;
using CallCar.Market;
using System.Net;
using CallCar.Flight;
using CallCar.Dispatch;
using CallCar.ServiceBase;
using CallCar.ServicePlan;
using System.Linq;
using DbConn;
using MySql.Data.MySqlClient;
using System.Data;

namespace Airport_Carpool_UAPI
{
    [ServiceContract(Namespace = "Airport_Carpool_UAPI")]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]



    public class UAPI
    {
        public static string ConnectionString = ConfigurationManager.ConnectionStrings["CarpoolLab"].ConnectionString;
        public static string Pay2GoUrl = ConfigurationManager.ConnectionStrings["Pay2Go"].ConnectionString;
        public static string MerchantID = ConfigurationManager.ConnectionStrings["MerchantID"].ConnectionString;
        public static string Pay2GoHash = ConfigurationManager.ConnectionStrings["Pay2GoHash"].ConnectionString;
        public static string Pay2GoIV = ConfigurationManager.ConnectionStrings["Pay2GoIV"].ConnectionString;
        public static string PickCarURL = ConfigurationManager.ConnectionStrings["PickCar"].ConnectionString;
        public static string FlightStatusAPPID = ConfigurationManager.ConnectionStrings["FlightStatusAPPID"].ConnectionString;
        public static string FlightStatusAPPKEY = ConfigurationManager.ConnectionStrings["FlightStatusAPPKEY"].ConnectionString;
        public static string _iv = "8417728284177282";

        #region 帳號
        [Description("APP用戶手動新增新帳號資料")]
        [OperationContract, WebInvoke(UriTemplate = "GenAccAPPN", Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public ReturnMsg addAccAPP(Stream input)
        {
            ReturnMsg r = new ReturnMsg();
            string token = string.Empty;
            string data = new StreamReader(input).ReadToEnd();
            //try
            //{
            //    data = DeCrypt(data, out token);
            //}
            //catch (Exception ex)
            //{
            //    r.Status = "Error";
            //    r.DebugMsg = "傳入字串解密失敗：" + ex.Message;
            //    r.DisplayMsg = "Oops! 發生錯誤！請確認您用的最新版APP";
            //    r.RtnObject = null;
            //    return r;
            //}
            structAccInfo ai;
            try
            {
                ai = JsonConvert.DeserializeObject<structAccInfo>(data);
            }
            catch (Exception ex)
            {
                r.Status = "Error";
                r.DebugMsg = "傳入字串解析失敗：" + ex.Message;
                r.DisplayMsg = "Oops! 發生錯誤！請確認您用的最新版APP";
                r.RtnObject = null;
                return r;
            }

            BALAccount bala = new BALAccount(ConnectionString);
            OAccount oa = new OAccount();
            try
            {
                oa.AgeRange = "";
                oa.Birthday = "";
                oa.Email = ai.email;
                oa.FName = ai.fname;
                oa.Gender = "2";
                oa.ID = 0;
                oa.LName = ai.lname;
                oa.Local = "";
                oa.MName = "";
                oa.MobilePhone = ai.mobile;
                oa.NickName = "";
                oa.Password = ai.pw;
                oa.Source = ai.source;
                oa.SourceID = ai.sid;
                oa.Introducer = ai.intro;

                int id = bala.CreateNewUserAccount(oa);

                r.Status = "Success";
                r.DebugMsg = null;
                r.DisplayMsg = null;
                r.RtnObject = id;
                return r;

            }
            catch (Exception ex)
            {
                r.Status = "Error";
                r.DebugMsg = "新增帳號失敗：" + ex.Message;
                r.DisplayMsg = "Oops! 發生錯誤！請確認您用的最新版APP";
                r.RtnObject = null;
                return r;
            }
        }

        [Description("驗證帳密 data:帳戶資料  ")]
        [OperationContract, WebInvoke(UriTemplate = "ChkAccAPPW", Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        public ReturnMsg chkAccPW(Stream input)
        {
            ReturnMsg r = new ReturnMsg();
            string token = string.Empty;
            string data = new StreamReader(input).ReadToEnd();
            string Account, Password;
            try
            {
                //data = DeCrypt(data, out token);

                JObject jo = JsonConvert.DeserializeObject<JObject>(data);

                Account = Convert.ToString(jo["acc"]);
                Password = Convert.ToString(jo["pw"]);
            }
            catch (Exception ex)
            {
                r.Status = "Error";
                r.DebugMsg = "字串處理失敗：" + ex.Message;
                r.DisplayMsg = "Oops! 發生錯誤！請確認您用的最新版APP";
                r.RtnObject = null;
                return r;
            }
            try
            {
                BALAccount bal = new BALAccount(ConnectionString);

                if (bal.VarifyUserAccount(Account, Password))
                {
                    r.RtnObject = "True";
                    r.DisplayMsg = "";
                }
                else
                {
                    r.RtnObject = "False";
                    r.DisplayMsg = "帳密驗證錯誤";
                }

                r.Status = "Success";
                r.DebugMsg = "";

                return r;
            }
            catch (Exception ex)
            {
                r.Status = "Error";
                r.DebugMsg = ex.Message;
                r.DisplayMsg = "Oops! 發生錯誤！請確認您用的最新版APP";
                r.RtnObject = null;
                return r;
            }
        }

        [Description("驗證Email是否存在")]
        [OperationContract, WebInvoke(UriTemplate = "getAccAPPE", Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public ReturnMsg chkEmailAPP(Stream input)
        {
            ReturnMsg r = new ReturnMsg();
            string data = new StreamReader(input).ReadToEnd();
            string token = string.Empty;
            string Account;

            try
            {
                //data = DeCrypt(data, out token);
                JObject jo = JsonConvert.DeserializeObject<JObject>(data);
                if (jo["acc"] == null)
                {
                    r.DebugMsg = "傳入參數錯誤";
                    r.DisplayMsg = "Oops! 發生錯誤！請確認您用的最新版APP";
                    r.RtnObject = "";
                    r.Status = "Error";
                    return r;
                }
                Account = Convert.ToString(jo["acc"]);
            }
            catch (Exception ex)
            {
                r.DebugMsg = ex.Message;
                r.DisplayMsg = "Oops! 發生錯誤！請確認您用的最新版APP";
                r.RtnObject = ex.Message;
                r.Status = "Error";
                return r;
            }


            try
            {

                BALAccount bal = new BALAccount(ConnectionString);
                OAccount oa = bal.GetUserAccount(Account);

                if (oa != null)
                {
                    structAccInfo info = new structAccInfo();
                    info.source = oa.Source;
                    info.sid = oa.SourceID;
                    info.fname = oa.FName;
                    info.lname = oa.LName;
                    info.email = oa.Email;
                    info.mobile = oa.MobilePhone;
                    info.uid = oa.ID;
                    info.nname = oa.NickName;
                    info.gender = oa.Gender;
                    info.mname = oa.MName;
                    info.age = oa.AgeRange;
                    info.local = oa.Local;
                    info.birthday = oa.Birthday;


                    r.DebugMsg = "";
                    r.DisplayMsg = "";
                    r.RtnObject = JsonConvert.SerializeObject(info);
                    r.Status = "Success";
                    return r;
                }
                else
                {
                    r.DebugMsg = "查無此帳號資料";
                    r.DisplayMsg = "Oops！帳號資料錯誤，請確認是否已登入CallCar";
                    r.RtnObject = null;
                    r.Status = "Error";
                    return r;
                }
            }
            catch (Exception ex)
            {
                r.DebugMsg = ex.Message;
                r.DisplayMsg = "Oops! 發生錯誤！";
                r.RtnObject = ex.Message;
                r.Status = "Error";
                return r;
            }
        }

        [Description("用ID取得會員資料")]
        [OperationContract, WebInvoke(UriTemplate = "getAccAPPID", Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        public ReturnMsg getAccUID(Stream input)
        {
            ReturnMsg r = new ReturnMsg();
            string data = new StreamReader(input).ReadToEnd();
            string token = string.Empty;
            int UserID;

            try
            {
                //data = DeCrypt(data, out token);
                JObject jo = JsonConvert.DeserializeObject<JObject>(data);
                UserID = Convert.ToInt32(jo["id"]);
                if (jo["id"] == null)
                {
                    r.DebugMsg = "傳入參數錯誤";
                    r.DisplayMsg = "Oops! 發生錯誤！請確認您用的最新版APP";
                    r.RtnObject = "";
                    r.Status = "Error";
                    return r;
                }
            }
            catch (Exception ex)
            {
                r.DebugMsg = ex.Message;
                r.DisplayMsg = "Oops! 發生錯誤！請確認您用的最新版APP";
                r.RtnObject = ex.Message;
                r.Status = "Error";
                return r;
            }


            try
            {

                BALAccount bal = new BALAccount(ConnectionString);
                OAccount oa = bal.GetUserAccount(UserID);

                if (oa != null)
                {
                    structAccInfo info = new structAccInfo();
                    info.source = oa.Source;
                    info.sid = oa.SourceID;
                    info.fname = oa.FName;
                    info.lname = oa.LName;
                    info.email = oa.Email;
                    info.mobile = oa.MobilePhone;
                    info.uid = oa.ID;
                    info.nname = oa.NickName;
                    info.gender = oa.Gender;
                    info.mname = oa.MName;
                    info.age = oa.AgeRange;
                    info.local = oa.Local;
                    info.birthday = oa.Birthday;


                    r.DebugMsg = "";
                    r.DisplayMsg = "";
                    r.RtnObject = JsonConvert.SerializeObject(info);
                    r.Status = "Success";
                    return r;
                }
                else
                {
                    r.DebugMsg = "查無此ID資料";
                    r.DisplayMsg = "Oops！帳號資料錯誤，請確認是否已登入CallCar";
                    r.RtnObject = null;
                    r.Status = "Error";
                    return r;
                }
            }
            catch (Exception ex)
            {
                r.DebugMsg = ex.Message;
                r.DisplayMsg = "Oops! 發生錯誤！";
                r.RtnObject = ex.Message;
                r.Status = "Error";
                return r;
            }
        }

        [Description("更新密碼")]
        [OperationContract, WebInvoke(UriTemplate = "UpdAccpw", Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        public ReturnMsg UpdAccountPW(Stream input)
        {
            ReturnMsg r = new ReturnMsg();
            string data = new StreamReader(input).ReadToEnd();
            string token = string.Empty;
            int uid;
            string old, newp;
            try
            {
                //  data = DeCrypt(data, out token);

                JObject jo = JsonConvert.DeserializeObject<JObject>(data);
                if (jo["id"] == null || jo["old"] == null || jo["newp"] == null)
                {
                    r.DebugMsg = "傳入參數錯誤";
                    r.DisplayMsg = "Oops! 發生錯誤！請確認您用的最新版APP";
                    r.RtnObject = "";
                    r.Status = "Error";
                    return r;
                }
                uid = Convert.ToInt32(jo["id"]);
                old = Convert.ToString(jo["old"]);
                newp = Convert.ToString(jo["newp"]);

            }
            catch (Exception ex)
            {
                r.DebugMsg = ex.Message;
                r.DisplayMsg = "Oops! 發生錯誤！請確認您用的最新版APP";
                r.RtnObject = ex.Message;
                r.Status = "Error";
                return r;
            }

            try
            {
                BALAccount bal = new BALAccount(ConnectionString);
                if (bal.UpdUserPassword(uid, old, newp))
                {
                    r.DebugMsg = "";
                    r.DisplayMsg = "密碼更新成功";
                    r.RtnObject = "True";
                    r.Status = "Success";
                    return r;
                }
                else
                {
                    r.DebugMsg = "";
                    r.DisplayMsg = "密碼更新失敗";
                    r.RtnObject = "False";
                    r.Status = "Success";
                    return r;
                }
            }
            catch (Exception ex)
            {
                r.DebugMsg = ex.Message;
                r.DisplayMsg = "Oops! 發生錯誤！請再試一次！";
                r.RtnObject = ex.Message;
                r.Status = "ERROR";
                return r;
            }
        }

        [Description("更新帳號資料")]
        [OperationContract, WebInvoke(UriTemplate = "UpdAccInfo", Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        public ReturnMsg UpdAccInfo(Stream input)
        {
            ReturnMsg r = new ReturnMsg();
            string data = new StreamReader(input).ReadToEnd();
            string token = string.Empty;
            int UserID;
            try
            {
                //data = DeCrypt(data, out token);
                JObject jo = JsonConvert.DeserializeObject<JObject>(data);
                if (jo["uid"] == null || jo["data"] == null)
                {
                    r.DebugMsg = "傳入參數錯誤";
                    r.DisplayMsg = "Oops! 發生錯誤！請確認您用的最新版APP";
                    r.RtnObject = "";
                    r.Status = "Error";
                    return r;
                }

                UserID = Convert.ToInt32(jo["uid"]);
                data = Convert.ToString(jo["data"]);

            }
            catch (Exception ex)
            {
                r.DebugMsg = ex.Message;
                r.DisplayMsg = "Oops! 發生錯誤！請確認您用的最新版APP";
                r.RtnObject = ex.Message;
                r.Status = "Error";
                return r;
            }

            structAccInfo json;
            OAccount oa = new OAccount();
            try
            {
                json = JsonConvert.DeserializeObject<structAccInfo>(data);

                oa.AgeRange = json.age;
                oa.Birthday = json.birthday;
                oa.FName = json.fname;
                oa.Gender = json.gender;
                oa.LName = json.lname;
                oa.Local = json.local;
                oa.MName = json.mname;
                oa.MobilePhone = json.mobile;
                oa.NickName = json.nname;
            }
            catch (Exception ex)
            {
                r.DebugMsg = ex.Message;
                r.DisplayMsg = "Oops! 發生錯誤！請確認您用的最新版APP";
                r.RtnObject = ex.Message;
                r.Status = "Error";
                return r;
            }

            BALAccount bal = new BALAccount(ConnectionString);
            try
            {
                oa = bal.UpdUserInfo(UserID, oa);

                structAccInfo info = new structAccInfo();
                info.source = oa.Source;
                info.sid = oa.SourceID;
                info.fname = oa.FName;
                info.lname = oa.LName;
                info.email = oa.Email;
                info.mobile = oa.MobilePhone;
                info.uid = oa.ID;
                info.nname = oa.NickName;
                info.gender = oa.Gender;
                info.mname = oa.MName;
                info.age = oa.AgeRange;
                info.local = oa.Local;
                info.birthday = oa.Birthday;


                r.DebugMsg = "";
                r.DisplayMsg = "";
                r.RtnObject = JsonConvert.SerializeObject(info);
                r.Status = "Success";
                return r;
            }
            catch (Exception ex)
            {
                r.DebugMsg = ex.Message;
                r.DisplayMsg = "Oops! 發生錯誤！請再試一次！";
                r.RtnObject = ex.Message;
                r.Status = "Error";
                return r;
            }

        }

        [Description("系統重設密碼並通知 ")]
        [OperationContract, WebInvoke(UriTemplate = "ReNewP", Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        public ReturnMsg PublishNewPW(Stream input)
        {
            string data = new StreamReader(input).ReadToEnd();
            ReturnMsg r = new ReturnMsg();
            string token = string.Empty;
            string Email;
            try
            {
                //data = DeCrypt(data, out token);
                JObject jo = JsonConvert.DeserializeObject<JObject>(data);
                if (jo["email"] == null)
                {
                    r.DebugMsg = "傳入參數錯誤";
                    r.DisplayMsg = "Oops! 發生錯誤！請確認您用的最新版APP";
                    r.RtnObject = "";
                    r.Status = "Error";
                    return r;
                }
                Email = Convert.ToString(jo["email"]);
            }
            catch (Exception ex)
            {
                r.DebugMsg = ex.Message;
                r.DisplayMsg = "Oops! 發生錯誤！請確認您用的最新版APP";
                r.RtnObject = ex.Message;
                r.Status = "Error";
                return r;
            }
            BALAccount bal = new BALAccount(ConnectionString);
            OAccount oa;
            try
            {
                oa = bal.GetUserAccount(Email);
            }
            catch (Exception ex)
            {
                r.DebugMsg = "帳號查詢錯誤：" + ex.Message;
                r.DisplayMsg = "Oops! 查詢Email發生錯誤！";
                r.RtnObject = null;
                r.Status = "Error";
                return r;
            }
            if (oa != null && oa.ID > 0)
            {
                string newPW = General.GetRandomString(8);
                try
                {
                    bal.UpdUserPassword(oa.ID, newPW);
                }
                catch (Exception ex)
                {
                    r.DebugMsg = "更新密碼發生錯誤：" + ex.Message;
                    r.DisplayMsg = "Oops! 發送密碼發生錯誤！";
                    r.RtnObject = null;
                    r.Status = "Error";
                    return r;
                }
                GmailSMTP smtp = new GmailSMTP("support@ishareco.com", "share52439256b");
                string Subject;
                StringBuilder Body = new StringBuilder();

                Body.Append(oa.LName + oa.FName + " 您好： <br />");

                Body.Append("您在" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "  通知艾雪科技服務中心, 要求我們EMAIL您新的密碼 <p>");

                Body.Append("會員帳號: " + Email + "<br />");
                Body.Append("密碼: " + newPW + "<p>");

                Body.Append("請您務必盡快登入後，更改成您所慣用之密碼 <br /><p>");

                Body.Append("艾雪科技 服務中心<br />");
                Body.Append(@"網址: <a href='https://www.ishareco.com/callcar'> https://www.ishareco.com</a> <br />");
                Body.Append(@"Email: <a href='mailto:support@ishareco.com'>support@ishareco.com</a> <br />");
                Subject = "CallCar機場接駁 新密碼通知";
                List<string> MailAddress = new List<string>();
                MailAddress.Add(Email);
                try
                {
                    string Result = smtp.Send(MailAddress, "艾雪科技客服通知", Subject, Body.ToString(), null);
                    if (Result != "Success")
                    {
                        r.DebugMsg = "寄送信件發生錯誤";
                        r.DisplayMsg = "Oops! 發生錯誤！請再操作一次";
                        r.RtnObject = null;
                        r.Status = "Error";
                        return r;
                    }
                }
                catch (Exception ex)
                {
                    r.DebugMsg = "寄送信件發生錯誤：" + ex.Message;
                    r.DisplayMsg = "Oops! 發生錯誤！請再操作一次";
                    r.RtnObject = null;
                    r.Status = "Error";
                    return r;
                }
            }
            r.DebugMsg = "";
            r.DisplayMsg = "";
            r.RtnObject = null;
            r.Status = "Success";
            return r;


        }
        #endregion

        #region 信用卡發票資料
        [Description("新增一筆新發票資料")]
        [OperationContract, WebInvoke(UriTemplate = "InvAPPN", Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        public ReturnMsg CreateNewInvoice(Stream input)
        {
            ReturnMsg r = new ReturnMsg();
            string data = new StreamReader(input).ReadToEnd();
            string token = string.Empty;
            int uid;
            string title, ein, address;

            try
            {
                //data = DeCrypt(data, out token);

                JObject jo = JsonConvert.DeserializeObject<JObject>(data);
                if (jo["uid"] == null || jo["title"] == null || jo["ein"] == null || jo["address"] == null)
                {
                    r.DebugMsg = "傳入參數錯誤";
                    r.DisplayMsg = "Oops! 發生錯誤！請確認您用的最新版APP";
                    r.RtnObject = "";
                    r.Status = "Error";
                    return r;
                }
                uid = Convert.ToInt32(jo["uid"]);
                title = Convert.ToString(jo["title"]);
                ein = Convert.ToString(jo["ein"]);
                address = Convert.ToString(jo["address"]);

            }
            catch (Exception ex)
            {
                r.DebugMsg = ex.Message;
                r.DisplayMsg = "Oops! 發生錯誤！請確認您用的最新版APP";
                r.RtnObject = ex.Message;
                r.Status = "Error";
                return r;
            }
            try
            {

                BALPayment bal = new BALPayment(ConnectionString);
                int InvoiceID = bal.CreateNewInvoiceInfo(uid, title, ein, address);
                if (InvoiceID > 0)
                {
                    r.Status = "Success";
                    r.RtnObject = InvoiceID;
                    r.DisplayMsg = "";
                    r.DebugMsg = "";
                }
                else
                {
                    r.Status = "Error";
                    r.RtnObject = null;
                    r.DisplayMsg = "Oops! 發生錯誤！";
                    r.DebugMsg = "新增置資料庫發生錯誤";
                }

                return r;
            }
            catch (Exception ex)
            {
                r.DebugMsg = ex.Message;
                r.DisplayMsg = "Oops! 發生錯誤！";
                r.RtnObject = ex.Message;
                r.Status = "Error";
                return r;
            }
        }

        [Description("移除一筆新發票資料")]
        [OperationContract, WebInvoke(UriTemplate = "InvAPPR", Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        public ReturnMsg RemoveInvoice(Stream input)
        {
            ReturnMsg r = new ReturnMsg();
            string data = new StreamReader(input).ReadToEnd();
            string token = string.Empty;
            int UserID, InvoiceID;
            try
            {
                // data = DeCrypt(data, out token);

                JObject jo = JsonConvert.DeserializeObject<JObject>(data);
                if (jo["uid"] == null || jo["iid"] == null)
                {
                    r.DebugMsg = "傳入參數錯誤";
                    r.DisplayMsg = "Oops! 發生錯誤！請確認您用的最新版APP";
                    r.RtnObject = "";
                    r.Status = "Error";
                    return r;
                }
                UserID = Convert.ToInt32(jo["uid"]);
                InvoiceID = Convert.ToInt32(jo["iid"]);

            }
            catch (Exception ex)
            {
                r.DebugMsg = ex.Message;
                r.DisplayMsg = "Oops! 發生錯誤！請確認您用的最新版APP";
                r.RtnObject = ex.Message;
                r.Status = "Error";
                return r;
            }

            try
            {
                BALPayment bal = new BALPayment(ConnectionString);

                if (bal.CancelInvoiceInfo(UserID, InvoiceID))
                {
                    r.RtnObject = "True";
                    r.DisplayMsg = "";
                    r.DebugMsg = "";
                }
                else
                {
                    r.RtnObject = "False";
                    r.DisplayMsg = "Oops! 發生錯誤！";
                    r.DebugMsg = "資料庫刪除發生錯誤";
                }
                r.Status = "Success";
                return r;
            }
            catch (Exception ex)
            {
                r.Status = "Error";
                r.RtnObject = null;
                r.DisplayMsg = "Oops! 發生錯誤！請確認您用的最新版APP";
                r.DebugMsg = ex.Message;
                return r;
            }
        }

        [Description("查詢發票資料")]
        [OperationContract, WebInvoke(UriTemplate = "InvAPPQU", Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        public ReturnMsg QryUniqueInvoice(Stream input)
        {
            ReturnMsg r = new ReturnMsg();
            string data = new StreamReader(input).ReadToEnd();
            string token = string.Empty;
            int uid;
            try
            {
                // data = DeCrypt(data, out token);
                JObject jo = JsonConvert.DeserializeObject<JObject>(data);
                if (jo["uid"] == null)
                {
                    r.DebugMsg = "傳入參數錯誤";
                    r.DisplayMsg = "Oops! 發生錯誤！請確認您用的最新版APP";
                    r.RtnObject = "";
                    r.Status = "Error";
                    return r;
                }
                uid = Convert.ToInt32(jo["uid"]);
            }
            catch (Exception ex)
            {
                r.DebugMsg = ex.Message;
                r.DisplayMsg = "Oops! 發生錯誤！請確認您用的最新版APP";
                r.RtnObject = ex.Message;
                r.Status = "Error";
                return r;
            }

            try
            {
                BALPayment bal = new BALPayment(ConnectionString);
                OInvoice i = bal.GetInvoiceData(uid.ToString());
                if (i != null)
                {

                    structInvoice o = new structInvoice();
                    o.address = i.InvoiceAddress;
                    o.ein = i.EIN;
                    o.iid = i.SeqNo;
                    o.uid = i.UserID;

                    r.RtnObject = JsonConvert.SerializeObject(o);
                }
                else
                    r.RtnObject = null;

                r.DebugMsg = "";
                r.DisplayMsg = "";
                r.Status = "Success";

                return r;
            }
            catch (Exception ex)
            {
                r.DebugMsg = ex.Message;
                r.DisplayMsg = "Oops! 發生錯誤！請確認您用的最新版APP";
                r.RtnObject = ex.Message;
                r.Status = "Error";
                return r;
            }
        }

        [Description("新增一筆信用卡資料")]
        [OperationContract, WebInvoke(UriTemplate = "CardAPPN", Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        public ReturnMsg CreateNewCard(Stream input)
        {
            ReturnMsg r = new ReturnMsg();
            string data = new StreamReader(input).ReadToEnd();
            string token = string.Empty;
            int UserID;
            string CardNo, EDate, ACode;

            try
            {
                // data = DeCrypt(data, out token);

                JObject jo = JsonConvert.DeserializeObject<JObject>(data);
                if (jo["uid"] == null || jo["cno"] == null || jo["date"] == null || jo["code"] == null)
                {
                    r.DebugMsg = "傳入參數錯誤";
                    r.DisplayMsg = "Oops! 發生錯誤！請確認您用的最新版APP";
                    r.RtnObject = "";
                    r.Status = "Error";
                    return r;
                }
                UserID = Convert.ToInt32(jo["uid"]);
                CardNo = Convert.ToString(jo["cno"]);
                EDate = Convert.ToString(jo["date"]);
                ACode = Convert.ToString(jo["code"]);

            }
            catch (Exception ex)
            {
                r.DebugMsg = ex.Message;
                r.DisplayMsg = "Oops! 發生錯誤！請確認您用的最新版APP";
                r.RtnObject = ex.Message;
                r.Status = "Error";
                return r;
            }
            try
            {

                BALPayment bal = new BALPayment(ConnectionString);
                int CardID = bal.CreateNewCreditCard(UserID, CardNo, EDate, ACode, Pay2GoUrl, MerchantID, Pay2GoHash, Pay2GoIV);
                if (CardID > 0)
                {
                    r.Status = "Success";
                    r.RtnObject = CardID;
                    r.DisplayMsg = "";
                    r.DebugMsg = "";
                }
                else
                {
                    r.Status = "Error";
                    r.RtnObject = null;
                    r.DisplayMsg = "Oops! 發生錯誤！";
                    r.DebugMsg = "新增置資料庫發生錯誤";
                }

                return r;
            }
            catch (Exception ex)
            {
                r.DebugMsg = ex.Message;
                r.DisplayMsg = "Oops! 發生錯誤！";
                r.RtnObject = ex.Message;
                r.Status = "Error";
                return r;
            }
        }

        [Description("移除一筆信用卡資料")]
        [OperationContract, WebInvoke(UriTemplate = "CardAPPR", Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        public ReturnMsg RemoveCard(Stream input)
        {
            ReturnMsg r = new ReturnMsg();
            string data = new StreamReader(input).ReadToEnd();
            string token = string.Empty;
            int UserID, CardID;
            try
            {
                //data = DeCrypt(data, out token);

                JObject jo = JsonConvert.DeserializeObject<JObject>(data);
                if (jo["uid"] == null || jo["cid"] == null)
                {
                    r.DebugMsg = "傳入參數錯誤";
                    r.DisplayMsg = "Oops! 發生錯誤！請確認您用的最新版APP";
                    r.RtnObject = "";
                    r.Status = "Error";
                    return r;
                }
                UserID = Convert.ToInt32(jo["uid"]);
                CardID = Convert.ToInt32(jo["cid"]);

            }
            catch (Exception ex)
            {
                r.DebugMsg = ex.Message;
                r.DisplayMsg = "Oops! 發生錯誤！請確認您用的最新版APP";
                r.RtnObject = ex.Message;
                r.Status = "Error";
                return r;
            }

            try
            {
                BALPayment bal = new BALPayment(ConnectionString);

                if (bal.CancelCreditCard(UserID, CardID))
                {
                    r.RtnObject = "True";
                    r.DisplayMsg = "";
                    r.DebugMsg = "";
                }
                else
                {
                    r.RtnObject = "False";
                    r.DisplayMsg = "Oops! 發生錯誤！";
                    r.DebugMsg = "資料庫刪除發生錯誤";
                }
                r.Status = "Success";
                return r;
            }
            catch (Exception ex)
            {
                r.Status = "Error";
                r.RtnObject = null;
                r.DisplayMsg = "Oops! 發生錯誤！請確認您用的最新版APP";
                r.DebugMsg = ex.Message;
                return r;
            }
        }

        [Description("查詢信用卡資料")]
        [OperationContract, WebInvoke(UriTemplate = "gCardAPPU", Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        public ReturnMsg GetUniqueCard(Stream input)
        {

            ReturnMsg r = new ReturnMsg();
            string data = new StreamReader(input).ReadToEnd();
            string token = string.Empty;
            int uid;

            try
            {
                //data = DeCrypt(data, out token);
                JObject jo = JsonConvert.DeserializeObject<JObject>(data);
                if (jo["uid"] == null)
                {
                    r.DebugMsg = "傳入參數錯誤";
                    r.DisplayMsg = "Oops! 發生錯誤！請確認您用的最新版APP";
                    r.RtnObject = "";
                    r.Status = "Error";
                    return r;
                }
                uid = Convert.ToInt32(jo["uid"]);
            }
            catch (Exception ex)
            {
                r.DebugMsg = ex.Message;
                r.DisplayMsg = "Oops! 發生錯誤！請確認您用的最新版APP";
                r.RtnObject = ex.Message;
                r.Status = "Error";
                return r;
            }

            try
            {
                BALPayment bal = new BALPayment(ConnectionString);
                OCard i = bal.GetCreditCard(uid.ToString());
                if (i != null)
                {
                    i.CNo = i.CNo.Substring(0, 4) + "-****-****-" + i.CNo.Substring(12, 4);
                    i.Code = "***";
                    i.Expire = "****";
                    i.d = "";
                    i.t = "";

                    r.RtnObject = JsonConvert.SerializeObject(i);
                }
                else
                    r.RtnObject = null;

                r.DebugMsg = "";
                r.DisplayMsg = "";

                r.Status = "Success";
                return r;
            }
            catch (Exception ex)
            {
                r.DebugMsg = ex.Message;
                r.DisplayMsg = "Oops! 發生錯誤！請確認您用的最新版APP";
                r.RtnObject = ex.Message;
                r.Status = "Error";
                return r;
            }
        }

        #endregion

        #region Marketing
        [Description("驗證優惠碼")]
        [OperationContract, WebInvoke(UriTemplate = "vCoupon", Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public ReturnMsg VarifyCouponCode(Stream input)
        {
            ReturnMsg r = new ReturnMsg();
            string data = new StreamReader(input).ReadToEnd();
            string token = string.Empty;

            try
            {

                string Code, TakeDate, OrderDate;
                JObject jo = JsonConvert.DeserializeObject<JObject>(data);
                if (jo["code"] == null || jo["date"] == null)
                {
                    r.DebugMsg = "傳入參數錯誤";
                    r.DisplayMsg = "Oops! 發生錯誤！請確認您用的最新版APP";
                    r.RtnObject = "";
                    r.Status = "Error";
                    return r;
                }
                Code = Convert.ToString(jo["code"]);
                TakeDate = Convert.ToString(jo["date"]);
                OrderDate = DateTime.Now.ToString("yyyyMMdd");

                BALCoupon bal = new BALCoupon(ConnectionString);
                OCoupon coupon = bal.ChkCouponAvailable(Code, OrderDate);
                if (coupon == null)
                    r.RtnObject = "False";
                else
                {
                    if (Int32.Parse(coupon.TakeDateS) <= Int32.Parse(TakeDate) && Int32.Parse(coupon.TakeDateE) <= Int32.Parse(TakeDate) && coupon.UseCnt < coupon.UseLimit)
                        r.RtnObject = "True";
                    else
                        r.RtnObject = "Expire";
                }

                r.Status = "Success";

                return r;
            }
            catch (Exception ex)
            {
                r.Status = "Error";
                r.RtnObject = ex.Message;
                return r;
            }
        }

        [Description("取得試算金額")]
        [OperationContract, WebInvoke(UriTemplate = "gPrice", Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public ReturnMsg GetShareFee(Stream input)
        {
            ReturnMsg r = new ReturnMsg();
            string data = new StreamReader(input).ReadToEnd();
            string token = string.Empty;

            try
            {
                string OrderDate, TakeDate, TimeSegment, Coupon, Dno, CityName, DistinctName, Airport, CarType;
                int CarPcnt, UserID, BonusUseAmt, DepositUseAmt, Pcnt, Bcnt;
                string CarpoolFlag;

                JObject jo = JsonConvert.DeserializeObject<JObject>(data);
                OrderDate = DateTime.Now.ToString("yyyyMMdd");
                TakeDate = Convert.ToString(jo["date"]);
                TimeSegment = Convert.ToString(jo["time"]);
                CarpoolFlag = Convert.ToString(jo["share"]);
                Coupon = Convert.ToString(jo["coupon"]);
                Dno = Convert.ToString(jo["dno"]);
                UserID = Convert.ToInt32(jo["uid"]);
                BonusUseAmt = Convert.ToInt32(jo["bonus"]);
                DepositUseAmt = Convert.ToInt32(jo["deposit"]);
                Pcnt = Convert.ToInt32(jo["pcnt"]);
                Bcnt = Convert.ToInt32(jo["bcnt"]);

                CityName = Convert.ToString(jo["cname"]);
                DistinctName = Convert.ToString(jo["dname"]);
                Airport = Convert.ToString(jo["airport"]);
                CarType = Convert.ToString(jo["ctype"]);

                if (Dno != null && Dno != "")
                {
                    BALDispatch bald = new BALDispatch(ConnectionString);
                    ODispatchSheet ods = bald.GetDispatchInfo(Dno);
                    CarPcnt = ods.PassengerCnt;
                }
                else
                    CarPcnt = 0;

                BALPrice price = new BALPrice(ConnectionString);
                OPrice op;
                if (CarpoolFlag == "S")
                    op = price.GetSharePrice(OrderDate, TakeDate, TimeSegment, Pcnt, Bcnt, Coupon, BonusUseAmt, DepositUseAmt, CarPcnt, 0, TimeSegment, UserID);
                else
                    op = price.GetVIPPrice(OrderDate, TakeDate, TimeSegment, CityName, DistinctName, Airport, CarType, Coupon, BonusUseAmt, DepositUseAmt);


                r.Status = "Success";


                r.RtnObject = JsonConvert.SerializeObject(op);

                return r;
            }
            catch (Exception ex)
            {
                r.Status = "Error";
                r.RtnObject = ex.Message;
                r.DisplayMsg = "";
                return r;
            }
        }

        [Description("取得專接區域目前最低金額")]
        [OperationContract, WebInvoke(UriTemplate = "gVMPrice", Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public ReturnMsg GetVIPMinPrice(Stream input)
        {
            ReturnMsg r = new ReturnMsg();
            string data = new StreamReader(input).ReadToEnd();
            string token = string.Empty;

            string OrderDate, CityName, DistinctName, Airport;
            try
            {
                JObject jo = JsonConvert.DeserializeObject<JObject>(data);
                OrderDate = DateTime.Now.ToString("yyyyMMdd");
                CityName = Convert.ToString(jo["city"]);
                DistinctName = Convert.ToString(jo["distinct"]);
                Airport = Convert.ToString(jo["airport"]);
            }
            catch (Exception ex)
            {
                r.Status = "Error";
                r.RtnObject = ex.Message;
                r.DisplayMsg = "Oops! 發生錯誤！請確認您用的最新版APP";
                return r;
            }
            dmProServicePlan plan;
            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(ConnectionString))
                {
                    plan = new dmProServicePlan(conn);
                    plan.Select(CityName, DistinctName);
                }
            }
            catch (Exception ex)
            {
                r.Status = "Error";
                r.RtnObject = ex.Message;
                r.DisplayMsg = "Oops! 發生錯誤！請確認您用的最新版APP";
                return r;
            }
            if (plan.AreaID == "")
            {
                r.Status = "Error";
                r.RtnObject = "此區域非台灣本島服務區域";
                r.DisplayMsg = "此地點地址資料有誤，請重新選擇地點";
                return r;
            }
            else
            {
                switch (Airport)
                {
                    case "TPE":
                        if (plan.TPEServiceStatus == "X")
                        {
                            r.Status = "Error";
                            r.RtnObject = "此行政區非專接服務區域";
                            r.DisplayMsg = "超出專接務範圍，請重新選擇地點或機場";
                        }
                        else
                        {
                            r.Status = "Success";
                            r.RtnObject = plan.TPESalePrice4;
                            r.DisplayMsg = "";
                        }
                        break;
                    case "RMQ":
                        if (plan.RMQServiceStatus == "X")
                        {
                            r.Status = "Error";
                            r.RtnObject = "此行政區非專接服務區域";
                            r.DisplayMsg = "超出專接務範圍，請重新選擇地點或機場";
                        }
                        else
                        {
                            r.Status = "Success";
                            r.RtnObject = plan.RMQSalePrice4;
                            r.DisplayMsg = "";
                        }
                        break;
                    case "KHH":
                        if (plan.KHHServiceStatus == "X")
                        {
                            r.Status = "Error";
                            r.RtnObject = "此行政區非專接服務區域";
                            r.DisplayMsg = "超出專接務範圍，請重新選擇地點或機場";
                        }
                        else
                        {
                            r.Status = "Success";
                            r.RtnObject = plan.KHHSalePrice4;
                            r.DisplayMsg = "";
                        }
                        break;
                    default:
                        r.Status = "Error";
                        r.RtnObject = "此機場非本公司服務之機場";
                        r.DisplayMsg = "此機場非本公司服務之機場，請重新選擇機場";
                        break;
                }
            }
            return r;
        }
        #endregion

        #region 趟次資料來源
        [Description("取得車趟/時段資料")]
        [OperationContract, WebInvoke(UriTemplate = "gTimeCar", Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public ReturnMsg GetTimeCarsInfo(Stream input)
        {
            ReturnMsg r = new ReturnMsg();
            string token = string.Empty;
            string data = new StreamReader(input).ReadToEnd();
            string FlightDate, FlightTime, CarpoolType, ServiceType, Airport, Ternimal;
            double Lat, Lng;
            int Pcnt, Bcnt;
            DateTime dtFlightTime;
            try
            {
                JObject jo = JsonConvert.DeserializeObject<JObject>(data);
                FlightDate = Convert.ToString(jo["fdate"]);
                FlightTime = Convert.ToString(jo["ftime"]);
                CarpoolType = Convert.ToString(jo["stype"]);
                ServiceType = Convert.ToString(jo["ftype"]);
                Lat = Convert.ToDouble(jo["lat"]);
                Lng = Convert.ToDouble(jo["lng"]);
                Airport = Convert.ToString(jo["airport"]);
                Ternimal = Convert.ToString(jo["ter"]);
                Pcnt = Convert.ToInt32(jo["pcnt"]);
                Bcnt = Convert.ToInt32(jo["bcnt"]);
            }
            catch (Exception ex)
            {
                r.Status = "Error";
                r.DebugMsg = ex.Message;
                r.DisplayMsg = "";
                return r;
            }
            //先判斷日期時間
            dtFlightTime = General.ConvertToDateTime(FlightDate + FlightTime, 12);
            DateTime TakeTimeBegin, TakeTimeEnd;
            UAPIFunc.GetTimeList(ServiceType, dtFlightTime, out TakeTimeBegin, out TakeTimeEnd);
            List<structPickCarOut> OutList = new List<structPickCarOut>();
            BALDispatch bald = new BALDispatch(ConnectionString);
            DateTime CaluDate = TakeTimeBegin.Date;
            while (CaluDate <= TakeTimeEnd)
            {
                BALServiceBase bals = new BALServiceBase(ConnectionString);
                if (bals.ChkMatchDate(CaluDate.ToString("yyyyMMdd")))
                {
                    //揀車
                    List<ODispatchSheet> SheetList = bald.GetDispatchInfo(FlightDate, ServiceType);
                    if (SheetList.Count == 0)
                        CaluDate.AddDays(1);

                    for (int i = SheetList.Count - 1; i >= 0; i--)
                    {
                        bool lb_remove = false;
                        int TotalPCnt = 0, TotalBCnt = 0, MinDistance = 0;
                        //判斷此派車是否在落在航班推算的合理時間區間內
                        DateTime ServeTime = General.ConvertToDateTime(SheetList[i].TakeDate + SheetList[i].TimeSegment, 12);

                        if (ServeTime < TakeTimeBegin || ServeTime > TakeTimeEnd)
                            lb_remove = true;

                        //目前先服務一個停靠點
                        if (SheetList[i].ServiceCnt > 1)
                            lb_remove = true;

                        //計算人數行李數
                        if (SheetList[i].CarType == "四人座")
                        {
                            TotalPCnt = 3;
                            TotalBCnt = 3;
                            if ((TotalPCnt - (SheetList[i].PassengerCnt + Pcnt) < 0) || (TotalBCnt - (SheetList[i].BaggageCnt + Bcnt) < 0))
                                lb_remove = true;
                        }
                        else
                        {
                            TotalPCnt = 5;
                            TotalBCnt = 7;
                            if ((TotalPCnt - (SheetList[i].PassengerCnt + Pcnt) < 0) || (TotalBCnt - (SheetList[i].BaggageCnt + Bcnt) < 0))
                                lb_remove = true;
                        }

                        #region 單機板 揀車
                        List<dmDispatchReservation.ServiceUnit> UnitList = bald.GetAllDispatchUnit(SheetList[i].DispatchNo);
                        if (ServiceType == "O")
                            MinDistance = 2;
                        else
                            MinDistance = 8;

                        if (General.GetDistance(Lat, Lng, UnitList[0].LocationGPSLat, UnitList[0].LocationGPSLng) > MinDistance)
                            lb_remove = true;


                        if (!lb_remove)
                        {
                            structPickCarOut o = new structPickCarOut();
                            o.carno = SheetList[i].CarNo;
                            o.cartype = SheetList[i].CarType;
                            o.date = CaluDate.Date.ToString("yyyyMMdd");
                            o.share = CarpoolType;
                            o.time = UnitList[0].ScheduleServeTime.AddMinutes(15).ToString("HHmm");
                            o.dno = SheetList[i].DispatchNo;
                            o.driver = "";
                            OutList.Add(o);
                        }
                        #endregion
                    }

                    /* API版
                    structPickCarAPI api = new structPickCarAPI();
                    api.type = ServiceType;
                    api.order.pcnt = Pcnt;
                    api.order.bcnt = Bcnt;
                    api.order.ftime = dtFlightTime.ToString("yyyy-MM-dd HH:mm");
                    api.order.lat = Lat;
                    api.order.lng = Lng;
                    foreach (ODispatchSheet od in SheetList)
                    {
                        Car c = new Car();
                        c.bcnt = od.BaggageCnt;
                        c.pcnt = od.PassengerCnt;
                        c.scnt = od.ServiceCnt;
                        c.dno = od.DispatchNo;
                        List<dmDispatchReservation.ServiceUnit> UnitList = bald.GetAllDispatchUnit(od.DispatchNo);
                        foreach (dmDispatchReservation.ServiceUnit su in UnitList)
                        {
                            BALReservation balr = new BALReservation(ConnectionString);
                            OReservation or = balr.GetReservation(su.ReservationNo);
                            Stop s = new Stop();
                            s.bcnt = or.BaggageCnt;
                            if (or.ChooseType == "F")
                                s.ftime = or.FlightTime.ToString("yyyy-MM-dd HH:mm");
                            else
                                s.ftime = "";
                            s.lat = su.LocationGPSLat;
                            s.lng = su.LocationGPSLng;
                            s.pcnt = or.PassengerCnt;
                            s.stime = su.ScheduleServeTime.ToString("yyyy-MM-dd HH:mm");

                            c.Stops.Add(s);
                        }
                        api.CarList.Add(c);
                    }
                    */

                }
                else
                {
                    DateTime dtSTime = General.ConvertToDateTime(CaluDate.Date.ToString("yyyyMMdd") + "0000", 12);
                    DateTime tempTime = dtSTime;
                    while (tempTime <= TakeTimeEnd)
                    {
                        structPickCarOut o = new structPickCarOut();
                        o.carno = "";
                        o.cartype = "";
                        o.date = CaluDate.Date.ToString("yyyyMMdd");
                        o.share = CarpoolType;
                        o.time = tempTime.ToString("HHmm");
                        o.dno = "";
                        o.driver = "";
                        OutList.Add(o);

                        tempTime.AddMinutes(15);
                    }
                }
                CaluDate = CaluDate.AddDays(1);
            }

            if (OutList.Count > 0)
            {
                r.Status = "Success";
                OutList = OutList.OrderBy(o => o.date).ThenBy(o => o.time).ToList();
                r.RtnObject = JsonConvert.SerializeObject(OutList);
            }
            else
            {
                r.Status = "None";
                r.DisplayMsg = "此時段已無可供預約之車次！";
                r.RtnObject = null;
            }
            return r;

        }

        [Description("取得專接可預約之日期清單")]
        [OperationContract, WebInvoke(UriTemplate = "gVIPDate", Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public ReturnMsg getVIPOrderDate(Stream input)
        {
            ReturnMsg r = new ReturnMsg();
            string token = string.Empty;
            string data = new StreamReader(input).ReadToEnd();

            try
            {
                DateTime Today = DateTime.Now.Date;
                DateTime BeginDate = Today.AddDays(4);
                DateTime EndDate = Today.AddDays(90);
                DateTime temp;
                DateTime LockBeginDate = DateTime.Now;
                DateTime LockEndDate = DateTime.Now;
                StringBuilder sb = new StringBuilder();
                DataTable dt;
                bool lbLock = false;
                List<string> AllowDates = new List<string>();
                sb.Append("select lockdate,begindate,enddate from match_holiday_data  where @date between lockdate and enddate");
                List<MySqlParameter> parms = new List<MySqlParameter>();
                parms.Add(new MySqlParameter("@date", Today.ToString("yyyyMMdd")));
                using (dbConnectionMySQL conn = new dbConnectionMySQL(ConnectionString))
                {
                    dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                }
                if (dt.Rows.Count > 0)
                {
                    lbLock = true;
                    LockBeginDate = General.ConvertToDateTime(Convert.ToString(dt.Rows[0]["lockdate"]), 8);
                    LockEndDate = General.ConvertToDateTime(Convert.ToString(dt.Rows[0]["enddate"]), 8);
                }

                temp = BeginDate;
                while (DateTime.Compare(temp, EndDate) <= 0)
                {
                    if (lbLock)
                    {
                        if (DateTime.Compare(temp, LockBeginDate) >= 0 && DateTime.Compare(temp, LockEndDate) <= 0)
                            continue;
                        else
                            AllowDates.Add(temp.ToString("yyyy-MM-dd"));
                    }
                    else
                        AllowDates.Add(temp.ToString("yyyy-MM-dd"));

                    temp.AddDays(1);
                }

                r.Status = "Success";
                r.DebugMsg = "";
                r.DisplayMsg = "";
                r.RtnObject = AllowDates;

                return r;
            }
            catch (Exception ex)
            {
                r.Status = "Error";
                r.DebugMsg = ex.Message;
                r.DisplayMsg = "取得可預約日期發生錯誤";
                r.RtnObject = null;

                return r;
            }
        }
        #endregion

        #region 訂單
        [Description("新增預約訂單")]
        [OperationContract, WebInvoke(UriTemplate = "GenOrderAPP", Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        public ReturnMsg CreateNewOrder(Stream input)
        {
            string data = new StreamReader(input).ReadToEnd();
            ReturnMsg r = new ReturnMsg();

            string token = string.Empty;
            //try
            //{
            //    data = DeCrypt(data, out token);
            //}
            //catch (Exception ex)
            //{
            //    r.Status = "Error";
            //    r.DebugMsg = "傳入字串解密失敗：" + ex.Message;
            //    r.DisplayMsg = "Oops! 發生錯誤！請確認您用的最新版APP";
            //    r.RtnObject = null;
            //    return r;
            //}
            APINewOrder os = null;
            try
            {
                os = JsonConvert.DeserializeObject<APINewOrder>(data);
            }
            catch (Exception ex)
            {
                r.Status = "Error";
                r.RtnObject = null;
                r.DisplayMsg = "Oops! 請確認已更新至最新版本！";
                r.DebugMsg = "傳入參數錯誤" + ex.Message;
                return r;
            }
            try
            {
                //if (os.airport == null || os.airport == "" || os.airport == "null")
                //    os.airport = "TPE";

                if (os.airport == "TPE")
                {
                    //    if (os.terminal == null || os.terminal == "" || os.terminal == "null")
                    //    {
                    //        if (os.stype == "O")
                    //        {
                    //            if (os.taddress.Contains("第二航廈"))
                    //                os.terminal = "T2";
                    //            else if (os.taddress.Contains("第一航廈"))
                    //                os.terminal = "T1";
                    //            else if (os.taddress.Contains("第三航廈"))
                    //                os.terminal = "T3";
                    //        }
                    //        else if (os.stype == "I")
                    //        {
                    //            if (os.paddress.Contains("第二航廈"))
                    //                os.terminal = "T2";
                    //            else if (os.paddress.Contains("第一航廈"))
                    //                os.terminal = "T1";
                    //            else if (os.paddress.Contains("第三航廈"))
                    //                os.terminal = "T3";
                    //        }
                    //    }

                    if (os.stype == "O" && os.terminal == "T2")
                    {
                        os.tlat = 25.076838;
                        os.tlng = 121.232336;
                    }
                    else if (os.stype == "I" && os.terminal == "T2")
                    {
                        os.plat = 25.076838;
                        os.plng = 121.232336;
                    }
                    else if (os.stype == "O" && os.terminal == "T1")
                    {
                        os.tlat = 25.081056;
                        os.tlng = 121.237585;
                    }
                    else if (os.stype == "I" && os.terminal == "T1")
                    {
                        os.plat = 25.081056;
                        os.plng = 121.237585;
                    }
                }

                ////2017-08-25 由於APP端資料丟反了 API端 先除錯
                //string temp;
                //if (os.stype == "O")
                //{

                //    if (os.pvillage.Trim() == "台北市" || os.pvillage.Trim() == "新北市" || os.pvillage.Trim() == "桃園市")
                //    {
                //        temp = os.pvillage;
                //        os.pvillage = os.pcity;
                //        os.pcity = temp;
                //    }
                //}
                //else
                //{
                //    if (os.tvillage.Trim() == "台北市" || os.tvillage.Trim() == "新北市" || os.tvillage.Trim() == "桃園市")
                //    {
                //        temp = os.tvillage;
                //        os.tvillage = os.tcity;
                //        os.tcity = temp;
                //    }
                //}

                OReservation or = new OReservation();
                string ReservationNo;

                or.AddBagCnt = 0;
                or.Airport = os.airport;
                or.BaggageCnt = os.bcnt;
                or.PassengerCnt = os.pcnt;
                or.CarpoolFlag = os.cpflag;
                or.ChooseType = os.ctype;
                or.DiscountType = os.dtype;

                if (or.DiscountType == "C")
                    or.Coupon = os.coupon;
                else
                    or.Coupon = "";
                or.DiscountAmt = os.damt;
                BALPayment balp = new BALPayment(ConnectionString);
                OCard oc;
                try
                {
                    oc = balp.GetCreditCard(Int32.Parse(os.cdata));

                }
                catch (Exception ex)
                {
                    r.Status = "Error";
                    r.RtnObject = null;
                    r.DisplayMsg = "Oops! 支付資料讀取失敗！";
                    r.DebugMsg = "Oops! 支付資料讀取失敗！" + ex.Message;
                    return r;
                }
                or.Credit = JsonConvert.SerializeObject(oc);


                if (os.idata != "" && os.idata != "null")
                {
                    try
                    {
                        OInvoice oi = balp.GetInvoiceData(Int32.Parse(os.idata));
                        or.Invoice = JsonConvert.SerializeObject(oi);
                    }
                    catch (Exception ex)
                    {
                        r.Status = "Error";
                        r.RtnObject = null;
                        r.DisplayMsg = "Oops! 發票資料讀取失敗！";
                        r.DebugMsg = "Oops! 發票資料讀取失敗！" + ex.Message;
                        return r;
                    }
                }
                else
                    or.Invoice = "";

                or.EIN = "";

                if (os.ctype == "F")
                {
                    or.FlightDate = os.fdate;
                    or.FlightNo = os.fno;
                    or.FlightTime = General.ConvertToDateTime(os.fdate + os.ftime, 12);
                }
                else
                {
                    or.FlightDate = "";
                    or.FlightNo = "";
                }
                or.MaxFlag = os.max;
                or.OrderTime = DateTime.Now;

                BALAccount bala = new BALAccount(ConnectionString);
                OAccount oa = bala.GetUserAccount(os.uid);
                if (os.pname == null || os.pname == "null" || os.pname == "")
                    or.PassengerName = oa.LName + oa.FName;
                else
                    or.PassengerName = os.pname;

                if (os.pmobile == null || os.pmobile == "null" || os.pmobile == "")
                    or.PassengerPhone = os.pmobile;
                else
                    or.PassengerPhone = os.pmobile;

                or.PickCartype = os.pcartype;
                or.PickupAddress = os.paddress;
                or.PickupCity = os.pcity;
                or.PickupDistinct = os.pdistinct;
                or.PickupGPS = new OSpatialGPS(os.plat, os.plng);
                or.PreferTime = os.ptime;
                or.ProcessStage = "0";
                or.ReservationNo = "";
                or.ServeDate = os.sdate;
                or.ServiceType = os.stype;
                or.TakeoffAddress = os.taddress;
                or.TakeoffCity = os.tcity;
                or.TakeoffDistinct = os.tdistinct;
                or.TakeoffGPS = new OSpatialGPS(os.tlat, os.tlng);
                or.TakeoffVillage = os.tvillage;
                or.Ternimal = os.terminal;
                or.UserID = os.uid;
                or.CarType = os.cartype;

                BALPrice price = new BALPrice(ConnectionString);
                OPrice op;

                if (or.CarpoolFlag == "S")
                    op = price.GetSharePrice(DateTime.Now.ToString("yyyyMMdd"), or.ServeDate, or.PreferTime, or.PassengerCnt, or.BaggageCnt, or.Coupon, or.BonusPoint, 0, 0, 0, or.PreferTime, or.UserID);
                else
                    op = price.GetVIPPrice(DateTime.Now.ToString("yyyyMMdd"), or.ServeDate, or.PreferTime, (or.ServiceType == "I" ? or.TakeoffCity : or.PickupCity), (or.ServiceType == "I" ? or.TakeoffDistinct : or.PickupDistinct), or.Airport, or.CarType, or.Coupon, or.BonusPoint, 0);

                //TODO:紅利點數計算
                or.BonusPoint = 0;

                BALReservation balr = new BALReservation(ConnectionString);
                ReservationNo = balr.CreateNewReservation(or);
                if (ReservationNo != null && ReservationNo != string.Empty)
                {
                    r.Status = "Success";
                    r.RtnObject = ReservationNo;
                }
                else
                {
                    r.Status = "Error";
                    r.DebugMsg = "新增訂單發生失敗，訂單編號回傳為空";
                    r.DisplayMsg = "Oops！預約發生問題";
                    r.RtnObject = null;
                }
                return r;
            }
            catch (Exception ex)
            {
                r.Status = "Error";
                r.DebugMsg = "新增訂單發生失敗！" + ex.Message;
                r.DisplayMsg = "Oops！預約發生問題";
                r.RtnObject = null;
                return r;
            }
        }

        [Description("新增撿車訂單")]
        [OperationContract, WebInvoke(UriTemplate = "GenPickOrder", Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        public ReturnMsg CreateNewPickOrder(Stream input)
        {
            string data = new StreamReader(input).ReadToEnd();
            ReturnMsg r = new ReturnMsg();

            string token = string.Empty;
            string dno;
            string servetime;
            try
            {
                JObject jo = JsonConvert.DeserializeObject<JObject>(data);
                if (jo["data"] == null || jo["dno"] == null || jo["time"] == null)
                {
                    r.DebugMsg = "傳入參數錯誤";
                    r.DisplayMsg = "Oops! 發生錯誤！請確認您用的最新版APP";
                    r.RtnObject = "";
                    r.Status = "Error";
                    return r;
                }
                data = Convert.ToString(jo["data"]);
                dno = Convert.ToString(jo["dno"]);
                servetime = Convert.ToString(jo["time"]);
            }
            catch (Exception ex)
            {
                r.Status = "Error";
                r.DebugMsg = "傳入字串解密失敗：" + ex.Message;
                r.DisplayMsg = "Oops! 發生錯誤！請確認您用的最新版APP";
                r.RtnObject = null;
                return r;
            }
            try
            {

                APINewOrder os = JsonConvert.DeserializeObject<APINewOrder>(data);
                if (os.airport == null || os.airport == "" || os.airport == "null")
                    os.airport = "TPE";

                if (os.airport == "TPE")
                {
                    //if (os.terminal == null || os.terminal == "" || os.terminal == "null")
                    //{
                    //    if (os.stype == "O")
                    //    {
                    //        if (os.taddress.Contains("第二航廈"))
                    //            os.terminal = "T2";
                    //        else if (os.taddress.Contains("第一航廈"))
                    //            os.terminal = "T1";
                    //        else if (os.taddress.Contains("第三航廈"))
                    //            os.terminal = "T3";
                    //    }
                    //    else if (os.stype == "I")
                    //    {
                    //        if (os.paddress.Contains("第二航廈"))
                    //            os.terminal = "T2";
                    //        else if (os.paddress.Contains("第一航廈"))
                    //            os.terminal = "T1";
                    //        else if (os.paddress.Contains("第三航廈"))
                    //            os.terminal = "T3";
                    //    }
                    //}

                    if (os.stype == "O" && os.terminal == "T2")
                    {
                        os.tlat = 25.076838;
                        os.tlng = 121.232336;
                    }
                    else if (os.stype == "I" && os.terminal == "T2")
                    {
                        os.plat = 25.076838;
                        os.plng = 121.232336;
                    }
                    else if (os.stype == "O" && os.terminal == "T1")
                    {
                        os.tlat = 25.081056;
                        os.tlng = 121.237585;
                    }
                    else if (os.stype == "I" && os.terminal == "T1")
                    {
                        os.plat = 25.081056;
                        os.plng = 121.237585;
                    }
                }

                OReservation or = new OReservation();
                string ReservationNo;

                or.AddBagCnt = 0;
                or.Airport = os.airport;
                or.BaggageCnt = os.bcnt;

                or.CarpoolFlag = os.cpflag;
                or.ChooseType = os.ctype;
                or.DiscountType = os.dtype;
                if (or.DiscountType == "C")
                    or.Coupon = os.coupon;
                else
                    or.Coupon = "";
                or.DiscountAmt = os.damt;
                BALPayment balp = new BALPayment(ConnectionString);
                OCard oc;
                try
                {
                    oc = balp.GetCreditCard(Int32.Parse(os.cdata));

                }
                catch (Exception ex)
                {
                    r.Status = "Error";
                    r.RtnObject = null;
                    r.DisplayMsg = "Oops! 支付資料讀取失敗！";
                    r.DebugMsg = "Oops! 支付資料讀取失敗！" + ex.Message;
                    return r;
                }
                or.Credit = JsonConvert.SerializeObject(oc);


                if (os.idata != "" && os.idata != "null")
                {
                    try
                    {
                        OInvoice oi = balp.GetInvoiceData(Int32.Parse(os.idata));
                        or.Invoice = JsonConvert.SerializeObject(oi);
                    }
                    catch (Exception ex)
                    {
                        r.Status = "Error";
                        r.RtnObject = null;
                        r.DisplayMsg = "Oops! 發票資料讀取失敗！";
                        r.DebugMsg = "Oops! 發票資料讀取失敗！" + ex.Message;
                        return r;
                    }
                }
                else
                    or.Invoice = "";

                or.EIN = "";

                if (os.ctype == "F")
                {
                    or.FlightDate = os.fdate;
                    or.FlightNo = os.fno;
                    or.FlightTime = General.ConvertToDateTime(os.fdate + os.ftime, 12);
                }
                else
                {
                    or.FlightDate = "";
                    or.FlightNo = "";
                }
                or.MaxFlag = os.max;
                or.OrderTime = DateTime.Now;

                BALAccount bala = new BALAccount(ConnectionString);
                OAccount oa = bala.GetUserAccount(os.uid);
                if (os.pname == null || os.pname == "null" || os.pname == "")
                    or.PassengerName = oa.LName + oa.FName;
                else
                    or.PassengerName = os.pname;

                if (os.pmobile == null || os.pmobile == "null" || os.pmobile == "")
                    or.PassengerPhone = os.pmobile;
                else
                    or.PassengerPhone = os.pmobile;

                or.PickCartype = os.pcartype;
                or.PickupAddress = os.paddress;
                or.PickupCity = os.pcity;
                or.PickupDistinct = os.pdistinct;
                or.PickupGPS = new OSpatialGPS(os.plat, os.plng);
                or.PreferTime = os.ptime;
                or.ProcessStage = "0";
                or.PassengerCnt = os.pcnt;
                or.ReservationNo = "";
                or.ServeDate = os.sdate;
                or.ServiceType = os.stype;
                or.TakeoffAddress = os.taddress;
                or.TakeoffCity = os.tcity;
                or.TakeoffDistinct = os.tdistinct;
                or.TakeoffGPS = new OSpatialGPS(os.tlat, os.tlng);
                or.TakeoffVillage = os.tvillage;
                or.Ternimal = os.terminal;
                or.UserID = os.uid;

                BALPrice price = new BALPrice(ConnectionString);
                OPrice op;

                //if (or.CarpoolFlag == "S")
                op = price.GetSharePrice(DateTime.Now.ToString("yyyyMMdd"), or.ServeDate, or.PreferTime, or.PassengerCnt, or.BaggageCnt, or.Coupon, or.BonusPoint, 0, 0, 0, or.PreferTime, or.UserID);
                //else
                //op = price.GetVIPPrice(DateTime.Now.ToString("yyyyMMdd"), or.ServeDate, or.PreferTime, (or.ServiceType == "I" ? or.TakeoffCity : or.PickupCity), (or.ServiceType == "I" ? or.TakeoffDistinct : or.PickupDistinct), or.Airport, or.CarType, or.Coupon, or.BonusPoint, 0);

                //TODO:紅利點數計算
                or.BonusPoint = 0;


                BALDispatch bald = new BALDispatch(ConnectionString);
                if (bald.isCarFull(dno))
                {
                    r.Status = "Error";
                    r.DebugMsg = "此車預訂已滿";
                    r.DisplayMsg = "Oops！此車預訂已滿";
                    r.RtnObject = null;
                    return r;
                }
                DateTime ServiceTime;
                ServiceTime = General.ConvertToDateTime(or.ServeDate + servetime, 12);
                BALReservation balr = new BALReservation(ConnectionString);

                ReservationNo = balr.CreateNewReservation(or, dno, ServiceTime);
                if (ReservationNo != null && ReservationNo != string.Empty & ReservationNo != "")
                {
                    r.Status = "Success";
                    r.RtnObject = ReservationNo;
                }
                else
                {
                    r.Status = "Error";
                    r.DebugMsg = "新增訂單發生失敗，訂單編號回傳為空";
                    r.DisplayMsg = "Oops！預約發生問題";
                    r.RtnObject = null;
                    return r;
                }
                //新增乘車簡訊
                ODispatchSheet od;
                try
                {
                    od = bald.GetDispatchInfo(dno);
                }
                catch (Exception ex)
                {
                    r.DebugMsg = "新增揀車訂單成功，但是查詢派車資料失敗：" + ex.Message;
                    r.DisplayMsg = "新增訂單成功";
                    return r;
                }

                try
                {
                    if (od != null && od.CarNo != null && od.DriverID > 0 && od.CarNo != "")
                    {
                        BALCallCarSMS sms = new BALCallCarSMS(ConnectionString);
                        sms.AddNewSMS(ReservationNo, or.ServiceType, or.ChooseType, or.FlightTime, or.PassengerPhone, od.CarNo, od.DriverID, ServiceTime);
                    }
                }
                catch (Exception ex)
                {
                    r.DebugMsg = "新增揀車訂單成功，但是新增乘車簡訊失敗：" + ex.Message;
                    r.DisplayMsg = "新增訂單成功";
                    return r;
                }
                r.DebugMsg = "";
                r.DisplayMsg = "新增訂單成功";
                return r;
            }
            catch (Exception ex)
            {
                r.Status = "Error";
                r.DebugMsg = "新增訂單發生失敗！" + ex.Message;
                r.DisplayMsg = "Oops！預約發生問題";
                r.RtnObject = null;
                return r;
            }
        }

        [Description("取消訂單")]
        [OperationContract, WebInvoke(UriTemplate = "cOrder", Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        public ReturnMsg CancelOrder(Stream input)
        {
            string data = new StreamReader(input).ReadToEnd();

            ReturnMsg r = new ReturnMsg();
            string token = string.Empty;
            //try
            //{
            //    data = DeCrypt(data, out token);
            //}
            //catch (Exception ex)
            //{
            //    r.Status = "Error";
            //    r.DebugMsg = "傳入字串解密失敗：" + ex.Message;
            //    r.DisplayMsg = "Oops! 發生錯誤！請確認您用的最新版APP";
            //    r.RtnObject = null;
            //    return r;
            //}

            string ReservationNo;

            try
            {
                try
                {
                    JObject jo = JsonConvert.DeserializeObject<JObject>(data);
                    if (jo["no"] == null)
                    {
                        r.DebugMsg = "傳入參數錯誤";
                        r.DisplayMsg = "Oops! 發生錯誤！請確認您用的最新版APP";
                        r.RtnObject = "";
                        r.Status = "Error";
                        return r;
                    }
                    ReservationNo = Convert.ToString(jo["no"]);
                }
                catch (Exception ex)
                {
                    r.Status = "Error";
                    r.DebugMsg = "傳入字串解析失敗：" + ex.Message;
                    r.DisplayMsg = "Oops! 發生錯誤！請確認您用的最新版APP";
                    r.RtnObject = null;
                    return r;
                }

                BALReservation bal = new BALReservation(ConnectionString);
                if (!bal.CancelReservation("X", data))
                {
                    r.Status = "Error";
                    r.RtnObject = null;
                    r.DebugMsg = "該單號(" + data + ")查無更新資料！'";
                    r.DisplayMsg = "Oops！訂單取消失敗！";
                    return r;
                }
                else
                {
                    r.Status = "Success";
                    r.RtnObject = null;
                    r.DebugMsg = null;
                    r.DisplayMsg = null;
                    return r;
                }
            }
            catch (Exception ex)
            {
                r.Status = "Error";
                r.RtnObject = null;
                r.DebugMsg = "該單號(" + data + ")更新失敗！" + ex.Message;
                r.DisplayMsg = "Oops！訂單取消失敗！";
                return r;
            }
        }

        [Description("檢查該地點是否提供共乘或專接")]
        [OperationContract, WebInvoke(UriTemplate = "qPosService", Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        public ReturnMsg QryPositionServiceStatus(Stream input)
        {
            ReturnMsg r = new ReturnMsg();
            string data;
            string token = string.Empty;
            data = new StreamReader(input).ReadToEnd();
            string City, Distinct, Village, Airport;

            try
            {
                //data = DeCrypt(data, out token);
                JObject jo = JsonConvert.DeserializeObject<JObject>(data);
                if (jo["city"] == null || jo["dist"] == null || jo["vill"] == null || jo["airport"] == null)
                {
                    r.DebugMsg = "傳入參數錯誤";
                    r.DisplayMsg = "Oops! 發生錯誤！請確認您用的最新版APP";
                    r.RtnObject = "";
                    r.Status = "Error";
                    return r;
                }
                City = Convert.ToString(jo["city"]);
                Distinct = Convert.ToString(jo["dist"]);
                Village = Convert.ToString(jo["vill"]);
                Airport = Convert.ToString(jo["airport"]);
            }
            catch (Exception ex)
            {
                r.Status = "Error";
                r.DebugMsg = "傳入字串解密失敗：" + ex.Message;
                r.DisplayMsg = "Oops! 發生錯誤！請確認您用的最新版APP";
                r.RtnObject = null;
                return r;
            }

            try
            {
                StringBuilder sb = new StringBuilder();

                if (City == "台北市" || City == "臺北市")
                    City = "台北市";

                BALServicePlan bal = new BALServicePlan(ConnectionString);
                structPosService service = new structPosService();

                if (bal.VarifyPosProServiceStatus(City, Distinct, Airport))
                {
                    service.vip.Status = "True";
                    service.vip.MinPassenger = 1;
                    //計算專接四人座價格
                    dmProServicePlan plan;
                    dmChargeBasicInfo charge;
                    try
                    {
                        using (dbConnectionMySQL conn = new dbConnectionMySQL(ConnectionString))
                        {
                            plan = new dmProServicePlan(conn);
                            plan.Select(City, Distinct);
                        }
                    }
                    catch (Exception ex)
                    {
                        r.Status = "Error";
                        r.RtnObject = ex.Message;
                        r.DisplayMsg = "Oops! 查詢服務區域發生錯誤！";
                        return r;
                    }
                    using (dbConnectionMySQL conn = new dbConnectionMySQL(ConnectionString))
                    {
                        charge = new dmChargeBasicInfo(conn);
                        List<OChargeItem> items;
                        int index;
                        switch (Airport)
                        {
                            case "TPE":
                                items = charge.Select(plan.TPESalePrice4);
                                index = items.FindIndex(x => Int32.Parse(x.StartDate) <= Int32.Parse(DateTime.Now.ToString("yyyyMMdd")) && Int32.Parse(x.EndDate) >= Int32.Parse(DateTime.Now.ToString("yyyyMMdd")));
                                service.vip.Price = items[index].Amount;
                                break;
                            case "RMQ":
                                items = charge.Select(plan.RMQSalePrice4);
                                index = items.FindIndex(x => Int32.Parse(x.StartDate) <= Int32.Parse(DateTime.Now.ToString("yyyyMMdd")) && Int32.Parse(x.EndDate) >= Int32.Parse(DateTime.Now.ToString("yyyyMMdd")));
                                service.vip.Price = items[index].Amount;
                                break;
                            case "KHH":
                                items = charge.Select(plan.KHHSalePrice4);
                                index = items.FindIndex(x => Int32.Parse(x.StartDate) <= Int32.Parse(DateTime.Now.ToString("yyyyMMdd")) && Int32.Parse(x.EndDate) >= Int32.Parse(DateTime.Now.ToString("yyyyMMdd")));
                                service.vip.Price = items[index].Amount;
                                break;
                            default:
                                r.Status = "Error";
                                r.RtnObject = "此機場非本公司服務之機場";
                                r.DisplayMsg = "此機場非本公司服務之機場，請重新選擇機場";
                                break;
                        }
                    }
                }
                else
                {
                    service.vip.Status = "False";
                    service.vip.Price = -1;
                    service.vip.MinPassenger = -1;
                }
                if (bal.VarifyPosShareServiceStatus(City, Distinct, Village))
                {
                    service.share.Status = "True";
                    service.share.MinPassenger = 1;
                    service.share.Price = 389;
                }
                else
                {
                    service.share.Status = "False";
                    service.share.MinPassenger = -1;
                    service.share.Price = -1;
                }

                r.Status = "Success";
                r.DebugMsg = "";
                r.DisplayMsg = "";
                r.RtnObject = JsonConvert.SerializeObject(service);
                return r;
            }
            catch (Exception ex)
            {
                r.Status = "Error";
                r.DebugMsg = ex.Message;
                r.DisplayMsg = "Oops！GoogleMap地點查詢失敗！";
                r.RtnObject = ex.Message;
                return r;
            }
        }


        #endregion

        [Description("查詢航班資料")]
        [OperationContract, WebInvoke(UriTemplate = "gFlight", Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        public ReturnMsg QryFlightInfo(Stream input)
        {

            ReturnMsg r = new ReturnMsg();
            string token = string.Empty;
            string data = new StreamReader(input).ReadToEnd();
            string FlightNo, AirlineID, FlightNumber, FlightDate, ServiceType;
            JObject jo;
            try
            {
                //  data = DeCrypt(data, out token);
                jo = JsonConvert.DeserializeObject<JObject>(data);
                if (jo["no"] == null || jo["date"] == null || jo["type"] == null)
                {
                    r.DebugMsg = "傳入參數錯誤";
                    r.DisplayMsg = "Oops! 發生錯誤！請確認您用的最新版APP";
                    r.RtnObject = "";
                    r.Status = "Error";
                    return r;
                }
                FlightNo = Convert.ToString(jo["no"]);
                FlightDate = Convert.ToString(jo["date"]);
                ServiceType = Convert.ToString(jo["type"]);


            }
            catch (Exception ex)
            {
                r.Status = "Error";
                r.DebugMsg = "傳入字串解密失敗：" + ex.Message;
                r.DisplayMsg = "Oops! 發生錯誤！請確認您用的最新版APP";
                r.RtnObject = null;
                return r;
            }

            FlightNo = FlightNo.Replace("-", "");
            AirlineID = FlightNo.Substring(0, 2);
            FlightNumber = FlightNo.Substring(2, FlightNo.Length - 2);
            FlightNo = Int32.Parse(FlightNumber).ToString();

            string APIURL = @"https://api.flightstats.com/flex/schedules/rest/v1/json/flight/{0}/{1}/{2}/{3}/{4}/{5}?appId={6}&appKey={7}";
            string FlightURL = string.Empty;
            string type;
            string responseFromServer;
            type = "departing";
            FlightURL = string.Format(APIURL, AirlineID, FlightNo, type, FlightDate.Substring(0, 4), FlightDate.Substring(4, 2), FlightDate.Substring(6, 2), FlightStatusAPPID, FlightStatusAPPKEY);

            try
            {
                WebRequest request = WebRequest.Create(FlightURL);
                //request.Credentials = CredentialCache.DefaultCredentials;
                System.Net.ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                WebResponse response = request.GetResponse();
                Console.WriteLine(((HttpWebResponse)response).StatusDescription);
                Stream dataStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(dataStream);
                responseFromServer = reader.ReadToEnd();
                reader.Close();
                response.Close();
            }
            catch (Exception ex)
            {
                r.DebugMsg = "呼叫航班API失敗：" + ex.Message;
                r.DisplayMsg = "Oops! 發生錯誤！請確認您用的最新版APP";
                r.RtnObject = null;
                r.Status = "Error";

                return r;
            }
            jo = JsonConvert.DeserializeObject<JObject>(responseFromServer);

            string scheduledFlights = Convert.ToString(jo["scheduledFlights"]);
            if (scheduledFlights == "[]")
                return null;

            List<OFSScheduledFlight> ofs;
            try
            {
                ofs = JsonConvert.DeserializeObject<List<OFSScheduledFlight>>(scheduledFlights);
            }
            catch (Exception ex)
            {
                r.DebugMsg = "解析定期航班JSON失敗：" + ex.Message;
                r.DisplayMsg = "Oops! 發生錯誤！請確認您用的最新版APP";
                r.RtnObject = null;
                r.Status = "Error";

                return r;
            }
            List<structFlightStruct> List = new List<structFlightStruct>();
            foreach (OFSScheduledFlight afs in ofs)
            {
                structFlightStruct afs2 = new structFlightStruct();
                if (ServiceType == "O")
                {
                    if (afs.departureAirportFsCode != "TPE")
                        continue;
                    afs2.ft = "離站";
                }
                else
                {
                    if (afs.arrivalAirportFsCode != "TPE")
                        continue;

                    afs2.ft = "到站";
                }

                BALFlight bal = new BALFlight(ConnectionString);
                afs2.fd = FlightDate;
                afs2.aid = AirlineID;
                OAirline oa = bal.GetAirlineInfo(afs2.aid);
                afs2.alname = (oa == null) ? " " : oa.AirlineName;
                afs2.fno = AirlineID + FlightNo;
                afs2.da = afs.departureAirportFsCode;
                OAirport oap = bal.GetAirportInfo(afs2.da);
                afs2.dpcity = (oap == null) ? " " : oap.CityName;
                afs2.dpname = (oap == null) ? " " : oap.AirportName;
                afs2.dt = afs.departureTime.ToString("HHmm");
                afs2.fulldt = afs.departureTime.ToString("yyyyMMddHHmm");
                afs2.aa = afs.arrivalAirportFsCode;
                oap = bal.GetAirportInfo(afs2.aa);
                afs2.apcity = (oap == null) ? " " : oap.CityName;
                afs2.apname = (oap == null) ? " " : oap.AirportName;
                afs2.at = afs.arrivalTime.ToString("HHmm");
                afs2.fullat = afs.arrivalTime.ToString("yyyyMMddHHmm");
                if (ServiceType == "O")
                    afs2.ter = afs.departureTerminal;
                else
                    afs2.ter = afs.arrivalTerminal;

                List.Add(afs2);
            }

            r.DebugMsg = "";
            r.DisplayMsg = "";
            r.RtnObject = JsonConvert.SerializeObject(List);
            r.Status = "Success";

            return r;

        }

        /* 歷史訂單紀錄查詢 */
        //[Description("歷史紀錄查詢")] //暫無調整
        //[OperationContract, WebInvoke(UriTemplate = "qUPOrder", Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        //public string GetUserPassOrders(Stream input)
        //{
        //    ReturnMsg r = new ReturnMsg();
        //    string data = new StreamReader(input).ReadToEnd();
        //    string token = string.Empty;

        //    try
        //    {
        //        data = DeCrypt(data, out token);
        //    }
        //    catch (Exception ex)
        //    {
        //        r.Status = "Error";
        //        r.RtnObject = ex;
        //        r.DebugMsg = ex.Message;
        //        r.DisplayMsg = "Oops！發生錯誤";
        //        return JsonConvert.SerializeObject(r);
        //    }
        //    try
        //    {
        //        BALReservation bal = new BALReservation(ConnectionString);
        //        int UserID;
        //        List<OReservation> List = new List<OReservation>();
        //        List<APIQOrderPast> ListQ = new List<APIQOrderPast>();
        //        Callcar.DAL.DALReservation dal = new Callcar.DAL.DALReservation(ConnectionString);

        //        if (Int32.TryParse(data, out UserID))
        //        {
        //            List = bal.GetReservationsByUser(UserID);
        //            if (List != null)
        //            {
        //                foreach (OReservation or in List)
        //                {
        //                    if (or.ProcessStage == "X")
        //                        continue;
        //                    DateTime TakeDate = Callcar.Common.General.ConvertToDateTime(or.ServeDate, 8);
        //                    DateTime Today = DateTime.Now.Date;
        //                    if (TakeDate.Date < Today.Date)
        //                    {
        //                        APIQOrderPast o = new APIQOrderPast();
        //                        if (or.ServiceType == "I")
        //                            o.add = or.TakeoffAddress;
        //                        else
        //                            o.add = or.PickupAddress;

        //                        o.bcnt = or.BaggageCnt;
        //                        o.io = or.ServiceType;
        //                        o.pcnt = or.PassengerCnt;
        //                        o.td = or.ServeDate;
        //                        string Info = dal.GetOrderDispatchData(or.ReservationNo);
        //                        if (Info != "")
        //                        {
        //                            string[] Dispatch = Info.Split(',');
        //                            o.st = Dispatch[0];
        //                            o.carno = Dispatch[2];
        //                            o.driver = Dispatch[3].Trim();
        //                            if (o.driver != "")
        //                                o.driver = o.driver.Substring(0, 1) + "先生";
        //                        }
        //                        else
        //                        {
        //                            o.st = "";
        //                            o.carno = "";
        //                            o.driver = "";
        //                        }
        //                        o.comment = "尚未寫入";
        //                        o.score = 0.0;
        //                        o.rno = or.ReservationNo;

        //                        ListQ.Add(o);
        //                    }
        //                }
        //                r.RtnObject = ListQ;
        //            }
        //            else
        //                r.RtnObject = null;

        //            r.Status = "Success";
        //        }
        //        else
        //        {
        //            r.RtnObject = null;
        //            r.DebugMsg = "會員編號錯誤";
        //            r.DisplayMsg = "";
        //            r.Status = "Error";
        //        }
        //        return Callcar.Common.General.OutputCrypt(JsonConvert.SerializeObject(r), token);
        //    }
        //    catch (Exception ex)
        //    {
        //        r.Status = "Error";
        //        r.RtnObject = ex;
        //        r.DebugMsg = ex.Message;
        //        r.DisplayMsg = "Oops！發生錯誤";
        //        return Callcar.Common.General.OutputCrypt(JsonConvert.SerializeObject(r), token);

        //    }
        //}

        /*預約查詢*/
        //[Description("預約查詢")] //暫無調整
        //[OperationContract, WebInvoke(UriTemplate = "qUFOrder", Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        //public string GetUserFeatureOrders(Stream input)
        //{
        //    ReturnMsg r = new ReturnMsg();
        //    string data = new StreamReader(input).ReadToEnd();
        //    string token = string.Empty;

        //    try
        //    {
        //        data = DeCrypt(data, out token);
        //    }
        //    catch (Exception ex)
        //    {
        //        r.Status = "Error";
        //        r.RtnObject = ex.Message;
        //        return JsonConvert.SerializeObject(r);
        //    }
        //    try
        //    {
        //        BALReservation bal = new BALReservation(ConnectionString);
        //        int UserID;
        //        List<OReservation> List = new List<OReservation>();
        //        List<APIQOrder> ListQ = new List<APIQOrder>();
        //        Callcar.DAL.DALReservation dal = new Callcar.DAL.DALReservation(ConnectionString);

        //        if (Int32.TryParse(data, out UserID))
        //        {
        //            List = bal.GetReservationsByUser(UserID);
        //            if (List != null)
        //            {
        //                foreach (OReservation or in List)
        //                {
        //                    if (or.ProcessStage == "X")
        //                        continue;
        //                    DateTime TakeDate = Callcar.Common.General.ConvertToDateTime(or.ServeDate, 8);
        //                    DateTime Today = DateTime.Now.Date;
        //                    if (TakeDate.Date >= Today.Date)
        //                    {
        //                        APIQOrder o = new APIQOrder();
        //                        if (or.ServiceType == "I")
        //                            o.add = or.TakeoffAddress;
        //                        else
        //                            o.add = or.PickupAddress;

        //                        o.bcnt = or.BaggageCnt;
        //                        o.io = or.ServiceType;
        //                        o.pcnt = or.PassengerCnt;
        //                        o.td = or.ServeDate;
        //                        o.tt = or.PreferTime;
        //                        string Info = dal.GetOrderDispatchData(or.ReservationNo);
        //                        if (Info != "")
        //                        {
        //                            string[] Dispatch = Info.Split(',');
        //                            o.st = Dispatch[0];
        //                            o.cartype = Dispatch[1];
        //                            o.carno = Dispatch[2];
        //                            o.driver = Dispatch[3].Trim();
        //                            if (o.driver != "")
        //                                o.driver = o.driver.Substring(0, 1) + "先生";
        //                            o.phone = Dispatch[4];
        //                        }
        //                        else
        //                        {
        //                            o.st = "";
        //                            o.cartype = "";
        //                            o.carno = "";
        //                            o.driver = "";
        //                            o.phone = "";
        //                        }
        //                        o.rno = or.ReservationNo;

        //                        ListQ.Add(o);
        //                    }
        //                }
        //                r.RtnObject = ListQ;
        //            }
        //            else
        //                r.RtnObject = ListQ;

        //            r.DebugMsg = "";
        //            r.DisplayMsg = "";
        //            r.Status = "Success";
        //        }
        //        else
        //        {
        //            r.Status = "Error";
        //            r.DebugMsg = "會員編號錯誤";
        //            r.DisplayMsg = "Oops! 發生錯誤！";
        //            r.RtnObject = null;
        //        }
        //        return Callcar.Common.General.OutputCrypt(JsonConvert.SerializeObject(r), token);
        //    }
        //    catch (Exception ex)
        //    {
        //        r.Status = "Error";
        //        r.RtnObject = ex;
        //        r.DebugMsg = ex.Message;
        //        r.DisplayMsg = "Oops! 發生錯誤！";
        //        return Callcar.Common.General.OutputCrypt(JsonConvert.SerializeObject(r), token);

        //    }
        //}



        private string DeCrypt(string CryptedText, out string token)
        {
            string data = CryptedText;
            try
            {
                token = AESCryptography.TokenAnalysis(ref data);
                AESCryptography aes = new AESCryptography(token, _iv);
                return aes.Decrypt(data);
            }
            catch (Exception)
            {
                throw new Exception("密文錯誤");
            }
        }


        [Description("測試")]
        [OperationContract, WebInvoke(UriTemplate = "Test", Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        public ReturnMsg Test(Stream input)
        {
            ReturnMsg r = new ReturnMsg();
            return r;
        }
    }
}
