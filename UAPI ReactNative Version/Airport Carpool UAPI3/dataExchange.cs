﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Airport_Carpool_UAPI
{

 

    public class APIGCarTime
    {
        public string ServiceType { get; set; }
        public string TakeDate { get; set; }
        public string FlightDate { get; set; }
        public string FlightTime { get; set; }
        public string SelectionType { get; set; }
        public int PassengerCnt { get; set; }
        public int BaggageCnt { get; set; }
        public double lat { get; set; }
        public double lng { get; set; }
        public string airport { get; set; } //機場代碼
        public string terminal { get; set; } //航廈
    }

    public class APIPickCarTime
    {
        public APIPickCarTime()
        {
            this.TimeRange = new List<string>();
            this.PickCars = new List<PCarInfo>();
        }
        public List<string> TimeRange { get; set; }
        public List<PCarInfo> PickCars { get; set; }

        public class PCarInfo
        {
            public string dno { get; set; } //派車單號
            public string td { get; set; } //搭乘日
            public string tr { get; set; } //時間區間
            public string st { get; set; } //出回國       
            public int rpc { get; set; } //剩餘乘客空間
            public int rbc { get; set; } //剩餘行李空間
            public int sc { get; set; } //乘客組數
            public string ct { get; set; } //車種
            public string cn { get; set; } //車牌        
            public string dn { get; set; } //司機名稱
            public string pt { get; set; } //服務時間
            public string max { get; set; } //滿車註記
            public int cte { get; set; } //車種英文
        }
    }

    public class APIQOrder
    {
        public string rno { get; set; }
        public string io { get; set; }
        public string td { get; set; }
        public string tt { get; set; }
        public string st { get; set; }
        public int pcnt { get; set; }
        public int bcnt { get; set; }
        public string add { get; set; }
        public string carno { get; set; }
        public string cartype { get; set; }
        public string driver { get; set; }
        public string phone { get; set; }
    }

    public class APIQOrderPast
    {
        public string rno { get; set; }
        public string io { get; set; }
        public string td { get; set; }
        public string st { get; set; }
        public int pcnt { get; set; }
        public int bcnt { get; set; }
        public string add { get; set; }
        public string comment { get; set; }
        public double score { get; set; }
        public string carno { get; set; }
        public string driver { get; set; }
    }

    public class OrderPriceHisotryStruct
    {
        public OrderPriceHisotryStruct()
        {
            Price = new List<FloatingPrice>();
        }

        public string Rno { get; set; }
        public List<FloatingPrice> Price { get; set; }

        public class FloatingPrice
        {
            public string DateTime { get; set; }
            public int Price { get; set; }
        }
    }

    public class APITrailFee
    {
        public string od { get; set; }
        public string td { get; set; }
        public string tt { get; set; }
        public int pc { get; set; }
        public int bc { get; set; }
        public string dno { get; set; }
        public string dtype { get; set; } //折抵種類 "c","b","g"
        public int bpint { get; set; }
        public int gpoint { get; set; }
        public string cp { get; set; } //折扣碼
    }




}