﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CallCar.Tool;

namespace Airport_Carpool_UAPI
{
    public class UAPIFunc
    {
        public static void GetTimeList(string ServiceType, DateTime FlightTime, out DateTime TakeTimeBegin, out DateTime TakeTimeEnd)
        {
            List<DateTime> List = new List<DateTime>();

            DateTime dTakeTimeBegin = DateTime.Now, dTakeTimeEnd = DateTime.Now;
            string tmpMin;
            int mm;

            //出國   航班時間 前6小時 ~ 前2.5hr
            //回國   航班時間 後1.5小時 ~後3小時

            if (ServiceType == "I")
            {
                dTakeTimeBegin = FlightTime.AddMinutes(75);
                dTakeTimeEnd = FlightTime.AddHours(3);
            }
            else if (ServiceType == "O")
            {
                dTakeTimeEnd = FlightTime.AddMinutes(-150);
                dTakeTimeBegin = FlightTime.AddHours(-6);
            }
            else
                TakeTimeEnd = DateTime.Now;


            mm = Convert.ToInt32(dTakeTimeBegin.ToString("mm"));
            if (mm > 45)
                TakeTimeBegin = General.ConvertToDateTime(dTakeTimeBegin.AddHours(1).ToString("yyyyMMddHH") + "00", 12);
            else
            {
                if (mm == 0)
                    tmpMin = "00";
                else if (mm <= 15 && mm > 0)
                    tmpMin = "15";
                else if (mm <= 30 && mm > 15)
                    tmpMin = "30";
                else
                    tmpMin = "45";

                TakeTimeBegin = General.ConvertToDateTime(dTakeTimeBegin.ToString("yyyyMMddHH") + tmpMin, 12);                
            }

            TakeTimeEnd = dTakeTimeEnd;
        }
    }
}