﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Airport_Carpool_UAPI
{
    public class APINewOrder
    {

        public int uid { get; set; }  //用戶序號
        public string stype { get; set; } //出國或回國
        public string sdate { get; set; } //乘車日期
        public string ptime { get; set; } //預約時段
        public string ctype { get; set; } //航班或航廈
        public string pcartype { get; set; } //預約或撿車
        public string fno { get; set; }  //航班編號
        public string fdate { get; set; } //航班日期
        public string ftime { get; set; } //航班時間
        public string pcity { get; set; } //上車地點
        public string pdistinct { get; set; } //上車地點
        public string pvillage { get; set; }
        public string paddress { get; set; } //上車地點
        public double plat { get; set; }
        public double plng { get; set; }
        public string tcity { get; set; } //上車地點
        public string tdistinct { get; set; } //上車地點
        public string tvillage { get; set; }
        public string taddress { get; set; } //上車地點
        public double tlat { get; set; }
        public double tlng { get; set; }
        public int pcnt { get; set; } //人數
        public int bcnt { get; set; } //行李數量
        public string max { get; set; } // 挑戰滿車
        public string idata { get; set; } //發票編號
        public string cdata { get; set; } //信用卡編號        
        public int price { get; set; }//金額
        public string airport { get; set; } //機場代碼
        public string terminal { get; set; } //航廈
        public string coupon { get; set; } //優惠碼
        public string pname { get; set; } //乘客姓名
        public string pmobile { get; set; }//乘客手機
        public string dtype { get; set; }//優惠類別 C：Coupon  B：Bonus  R：儲值金
        public int damt { get; set; }//折底金額
        public string cpflag { get; set; }//共乘註記 S：共乘訂單 P：專接訂單
        public string cartype { get; set; } //專接車型
    }

    public class ReturnMsg
    {
        public string Status { get; set; }
        public string DebugMsg { get; set; }
        public string DisplayMsg { get; set; }
        public object RtnObject { get; set; }
    }

    public class structAccInfo
    {
        /* APP新增帳號用 */
        public string source { get; set; } //帳號來源
        public string sid { get; set; } //來源辨識碼
        public string fname { get; set; }
        public string lname { get; set; }
        public string email { get; set; }
        public string mobile { get; set; }
        public string pw { get; set; }
        public int intro { get; set; }

        /*其它欄位*/
        public int uid { get; set; }
        public string nname { get; set; }
        public string gender { get; set; }
        public string mname { get; set; }
        public string age { get; set; }
        public string local { get; set; }
        public string birthday { get; set; }
    }

    public class structInvoice
    {
        public int iid { get; set; }
        public int uid { get; set; }
        public string title { get; set; }
        public string ein { get; set; }
        public string address { get; set; }
    }

    public class OFSScheduledFlight
    {
        public string carrierFsCode { get; set; }
        public string flightNumber { get; set; }
        public string departureAirportFsCode { get; set; }
        public string arrivalAirportFsCode { get; set; }
        public int stops { get; set; }
        public string departureTerminal { get; set; }
        public string arrivalTerminal { get; set; }
        public DateTime departureTime { get; set; }
        public DateTime arrivalTime { get; set; }
        public string flightEquipmentIataCode { get; set; }
        public bool isCodeshare { get; set; }
        public bool isWetlease { get; set; }
        public string serviceType { get; set; }
        public List<string> serviceClasses { get; set; }
        public List<object> trafficRestrictions { get; set; }
        public List<Codeshare> codeshares { get; set; }
        public string referenceCode { get; set; }

        public class Codeshare
        {
            public string carrierFsCode { get; set; }
            public string flightNumber { get; set; }
            public string serviceType { get; set; }
            public List<string> serviceClasses { get; set; }
            public List<string> trafficRestrictions { get; set; }
            public int referenceCode { get; set; }
        }
    }

    public class structFlightStruct
    {

        public string ft { get; set; }
        public string fd { get; set; }
        public string aid { get; set; }
        public string fno { get; set; }
        public string da { get; set; }
        public string dt { get; set; }
        public string fulldt { get; set; }
        public string aa { get; set; }
        public string at { get; set; }
        public string fullat { get; set; }
        public string ter { get; set; }
        public string alname { get; set; }
        public string apname { get; set; }
        public string apcity { get; set; }
        public string dpname { get; set; }
        public string dpcity { get; set; }

    }

    public class structPickCarOut
    {
        public string date { get; set; }
        public string time { get; set; }
        public string share { get; set; }
        public string carno { get; set; }
        public string cartype { get; set; }
        public string driver { get; set; }
        public string dno { get; set; }

    }

    public class structPosService
    {
        public structPosService()
        {
            share = new ShareService();
            vip = new VIPService();
        }
        public ShareService share { get; set; }
        public VIPService vip { get; set; }

        public class ShareService
        {
            public string Status { get; set; }
            public int Price { get; set; }
            public int MinPassenger { get; set; }
        }
        public class VIPService
        {
            public string Status { get; set; }
            public int Price { get; set; }
            public int MinPassenger { get; set; }
        }


    }

    #region 呼叫揀車API Json
    public class structPickCarAPI
    {
        public structPickCarAPI() { this.CarList = new List<Car>(); this.order = new Order(); }
        public string type { get; set; }
        public Order order { get; set; }
        public List<Car> CarList { get; set; }
    }
    public class Order
    {
        public double lat { get; set; }
        public double lng { get; set; }
        public string ftime { get; set; }
        public int pcnt { get; set; }
        public int bcnt { get; set; }
    }
    public class Car
    {
        public Car() { Stops = new List<Stop>(); }
        public string dno { get; set; }
        public int scnt { get; set; }
        public int pcnt { get; set; }
        public int bcnt { get; set; }
        public List<Stop> Stops { get; set; }
    }


    public class Stop
    {
        public double lat { get; set; }
        public double lng { get; set; }
        public string ftime { get; set; }
        public string stime { get; set; }
        public int pcnt { get; set; }
        public int bcnt { get; set; }
    }

    #endregion
}