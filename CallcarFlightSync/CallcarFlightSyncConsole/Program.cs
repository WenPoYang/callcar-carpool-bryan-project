﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MySql.Data.MySqlClient;
using System.Net;
using Callcar.Struct;
using System.IO;
using System.Data;
using System.Collections.Specialized;
using System.Configuration;
using NLog.Config;
using NLog;

namespace CallcarFlightSyncConsole
{
    class Program
    {
        private static Logger logger = NLog.LogManager.GetCurrentClassLogger();
        private static string ConnectionString;
        static void Main(string[] args)
        {

            NameValueCollection nc = ConfigurationManager.AppSettings;

            if (nc["Server"] == "Production")
            {
                ConnectionString = "Server=10.0.2.20;Port=3306;Database=carpoollab;Uid=ishareco;Pwd=fu0!j0$su0^vup#;Allow User Variables=True";
            }
            else if (nc["Server"] == "Lab")
            {
                ConnectionString = "Server=10.0.2.21;Port=3306;Database=carpoollab;Uid=apiuser;Pwd=7oPOh88I;Allow User Variables=True";
            }


            string csv_url = @"http://www.taoyuan-airport.com/uploads/govdata/FidsSchedule.csv";
            List<GenScheduleStrct> List = new List<GenScheduleStrct>();

            try
            {
                WebRequest req = HttpWebRequest.Create(csv_url);
                using (Stream fs = req.GetResponse().GetResponseStream())
                {
                    StreamReader sr = new StreamReader(fs, Encoding.UTF8);

                    string strLine = "";
                    string[] aryLine = null;

                    while ((strLine = sr.ReadLine()) != null)
                    {
                        aryLine = strLine.Split(',');
                        GenScheduleStrct s = new GenScheduleStrct();
                        string type = aryLine[0].Trim();

                        s.AirlineID = aryLine[1].Trim();
                        s.FlightNumber = s.AirlineID + aryLine[2].Trim();


                        if (type == "PA")
                        {
                            s.ArrivalTime = aryLine[3].Trim();
                            s.DepartureAirportID = aryLine[4].Trim();
                            s.ArrivalAirportID = "TPE";
                            s.DepartureTime = aryLine[5].Trim();
                            s.FlightType = type;
                        }
                        else if (type == "PD")
                        {
                            s.DepartureTime = aryLine[3].Trim();
                            s.DepartureAirportID = "TPE";
                            s.ArrivalAirportID = aryLine[4].Trim();
                            s.ArrivalTime = aryLine[5].Trim();
                            s.FlightType = type;
                        }
                        else
                            continue;
                        s.Terminal = aryLine[18].Trim();
                        string days = aryLine[19].Trim();
                        StringBuilder sb = new StringBuilder();
                        if (days.Contains("1"))
                        {
                            sb.Append("1");
                            s.Monday = true;
                        }
                        else
                        {
                            sb.Append("0");
                            s.Monday = false;
                        }
                        if (days.Contains("2"))
                        {
                            sb.Append("1");
                            s.Tuesday = true;
                        }
                        else
                        {
                            sb.Append("0");
                            s.Tuesday = false;
                        }
                        if (days.Contains("3"))
                        {
                            sb.Append("1");
                            s.Wednesday = true;
                        }
                        else
                        {
                            sb.Append("0");
                            s.Wednesday = false;
                        }
                        if (days.Contains("4"))
                        {
                            sb.Append("1");
                            s.Thursday = true;
                        }
                        else
                        {
                            sb.Append("0");
                            s.Thursday = false;
                        }
                        if (days.Contains("5"))
                        {
                            sb.Append("1");
                            s.Friday = true;
                        }
                        else
                        {
                            sb.Append("0");
                            s.Friday = false;
                        }
                        if (days.Contains("6"))
                        {
                            sb.Append("1");
                            s.Saturday = true;
                        }
                        else
                        {
                            sb.Append("0");
                            s.Saturday = false;
                        }
                        if (days.Contains("7"))
                        {
                            sb.Append("1");
                            s.Sunday = true;
                        }
                        else
                        {
                            sb.Append("0");
                            s.Sunday = false;
                        }
                        s.FlyDay = sb.ToString();
                        s.ScheduleStartDate = aryLine[20].Trim().Replace("/", "");
                        s.ScheduleEndDate = aryLine[21].Trim().Replace("/", "");

                        List.Add(s);
                    }

                    string rtn = Update2DB("csv", List);
                    if (rtn.Substring(0, 5) == "Error")
                        logger.Error(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss" + "    " + rtn));
                    else
                        Environment.Exit(0);
                }
            }
            catch (Exception ex)
            {
                logger.Error(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss" + "    " + ex.Message));
            }
        }

        private static string Update2DB(string source, List<GenScheduleStrct> gsList)
        {
            DataTable dtData = new DataTable();
            string sDate, eDate;
            sDate = DateTime.Now.ToString("yyyyMMdd");
            eDate = DateTime.Now.AddMonths(1).ToString("yyyyMMdd");

            try
            {
                using (MySqlConnection conn = new MySqlConnection(ConnectionString))
                {
                    conn.Open();

                    using (MySqlCommand cmd = new MySqlCommand())
                    {
                        cmd.CommandText = "SELECT ScheduleID, FlightType, AirlineID, FlightNo, StartDate, EndDate, DepartureAirportID, DepartureTime, ArrivalAirportID, ArrivalTime, FlyDay, Terminal, FlightShare " +
                                "FROM flight_schedule ";
                        cmd.Parameters.AddWithValue("@sdate", sDate);
                        cmd.Parameters.AddWithValue("@edate", eDate);
                        cmd.Connection = conn;
                        MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                        da.Fill(dtData);
                    }

                    foreach (GenScheduleStrct s in gsList)
                    {
                        DataRow[] Selected = dtData.Select("FlightType = '" + s.FlightType + "' and  FlightNo = '" + s.FlightNumber + "' and StartDate = '" + s.ScheduleStartDate.Replace("-", "") + "' and EndDate = '" + s.ScheduleEndDate.Replace("-", "") + "'");



                        if (Selected.Length > 0)
                        {
                            using (MySqlCommand cmd = new MySqlCommand())
                            {
                                cmd.Connection = conn;
                                StringBuilder sb = new StringBuilder();
                                cmd.CommandText = "UPDATE flight_schedule " +
                                        "SET DepartureAirportID=@daid,DepartureTime=@dtime,ArrivalAirportID=@aaid, " +
                                        "ArrivalTime=@atime,FlyDay=@fday,Terminal=@terminal,FlightShare=@fs " +
                                        "WHERE FlightNo = @no and StartDate = @sdate and EndDate=@edate and FlightType = @type";

                                cmd.Parameters.AddWithValue("@type", s.FlightType);
                                cmd.Parameters.AddWithValue("@daid", s.DepartureAirportID);
                                cmd.Parameters.AddWithValue("@dtime", s.DepartureTime);
                                cmd.Parameters.AddWithValue("@aaid", s.ArrivalAirportID);
                                cmd.Parameters.AddWithValue("@atime", s.ArrivalTime);
                                if (s.Monday)
                                    sb.Append("1");
                                else
                                    sb.Append("0");
                                if (s.Tuesday)
                                    sb.Append("1");
                                else
                                    sb.Append("0");
                                if (s.Wednesday)
                                    sb.Append("1");
                                else
                                    sb.Append("0");
                                if (s.Thursday)
                                    sb.Append("1");
                                else
                                    sb.Append("0");
                                if (s.Friday)
                                    sb.Append("1");
                                else
                                    sb.Append("0");
                                if (s.Saturday)
                                    sb.Append("1");
                                else
                                    sb.Append("0");
                                if (s.Sunday)
                                    sb.Append("1");
                                else
                                    sb.Append("0");
                                cmd.Parameters.AddWithValue("@fday", sb.ToString());
                                cmd.Parameters.AddWithValue("@terminal", s.Terminal);
                                cmd.Parameters.AddWithValue("@fs", "");
                                cmd.Parameters.AddWithValue("@no", s.FlightNumber);
                                cmd.Parameters.AddWithValue("@sdate", s.ScheduleStartDate);
                                cmd.Parameters.AddWithValue("@edate", s.ScheduleEndDate);

                                try
                                {
                                    cmd.ExecuteNonQuery();
                                }
                                catch (MySqlException ex)
                                {
                                    return "Error" + " " + ex.Message;
                                }
                            }
                        }
                        else
                        {
                            using (MySqlCommand cmd = new MySqlCommand())
                            {
                                cmd.Connection = conn;
                                StringBuilder sb = new StringBuilder();
                                cmd.CommandText = "INSERT INTO flight_schedule " +
                                   "(AirlineID, FlightType,FlightNo, StartDate, EndDate, DepartureAirportID, DepartureTime, ArrivalAirportID, ArrivalTime, FlyDay, Terminal, FlightShare) " +
                                   "VALUES (@aid, @type,@no, @sdate, @edate, @daid, @dtime, @aaid, @atime, @fday, @terminal, @fs)";

                                cmd.Parameters.AddWithValue("@aid", s.AirlineID);
                                cmd.Parameters.AddWithValue("@type", s.FlightType);
                                if (source == "csv")
                                    cmd.Parameters.AddWithValue("@no", s.FlightNumber);
                                else
                                    cmd.Parameters.AddWithValue("@no", s.FlightNumber);
                                cmd.Parameters.AddWithValue("@sdate", s.ScheduleStartDate);
                                cmd.Parameters.AddWithValue("@edate", s.ScheduleEndDate);
                                cmd.Parameters.AddWithValue("@daid", s.DepartureAirportID);
                                cmd.Parameters.AddWithValue("@dtime", s.DepartureTime);
                                cmd.Parameters.AddWithValue("@aaid", s.ArrivalAirportID);
                                cmd.Parameters.AddWithValue("@atime", s.ArrivalTime);
                                if (s.Monday)
                                    sb.Append("1");
                                else
                                    sb.Append("0");
                                if (s.Tuesday)
                                    sb.Append("1");
                                else
                                    sb.Append("0");
                                if (s.Wednesday)
                                    sb.Append("1");
                                else
                                    sb.Append("0");
                                if (s.Thursday)
                                    sb.Append("1");
                                else
                                    sb.Append("0");
                                if (s.Friday)
                                    sb.Append("1");
                                else
                                    sb.Append("0");
                                if (s.Saturday)
                                    sb.Append("1");
                                else
                                    sb.Append("0");
                                if (s.Sunday)
                                    sb.Append("1");
                                else
                                    sb.Append("0");
                                cmd.Parameters.AddWithValue("@fday", sb.ToString());
                                cmd.Parameters.AddWithValue("@terminal", s.Terminal);
                                cmd.Parameters.AddWithValue("@fs", "");

                                try
                                {
                                    cmd.ExecuteNonQuery();
                                }
                                catch (MySqlException ex)
                                {
                                    if (ex.Number == 1062)
                                        continue;
                                    else
                                        return "Error" + " " + ex.Message;
                                }
                            }
                        }
                    }
                    return "Success";
                }
            }
            catch (Exception ex)
            {
                return "Error" + " " + ex.Message;
            }

        }
    }
}
