﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Text;
using DbConn;
using System.ComponentModel;
using System.Configuration;
using Newtonsoft.Json;
using System.IO;
using System.Web;
using MySql.Data.MySqlClient;
using MySql.Data;

namespace ER_RxAPI
{
    [ServiceContract]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]

    public class RxAPI
    {
        public static string ConnectionString = ConfigurationManager.ConnectionStrings["BeaconRxDB"].ConnectionString;
        public static string WebDir = HttpContext.Current.Server.MapPath("../");

        [Description("接收Rx的資料 ")]
        [OperationContract, WebInvoke(UriTemplate = "RxInput", Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public void GetRxData(Stream input)
        {
            string data = new StreamReader(input).ReadToEnd();

            RxOutputData Rx = JsonConvert.DeserializeObject<RxOutputData>(data);
            DateTime Receivetime = DateTime.Now;

            if (Rx.Tag.Count == 0)
                return;

            if (!Directory.Exists(WebDir + @"Rx\"))
                Directory.CreateDirectory(WebDir + "Rx");

            File.WriteAllText(WebDir + @"Rx\" + Receivetime.ToString("yyyyMMddHHmmss") + "_" + Rx.RxMac+".txt", data);
        }
    }

    public class RxOutputData
    {
        public RxOutputData()
        {
            Tag = new List<TagOutputData>();
        }
        public string RxMac { get; set; }
        public List<TagOutputData> Tag { get; set; }
    }

    public class TagOutputData
    {
        public string TxMac { get; set; }
        public string Rtime { get; set; }
        public string Power { get; set; }
        public string RSSI { get; set; }
        public string Button { get; set; }
        public string GSensor { get; set; }
    }
}
