﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using uPLibrary.Networking.M2Mqtt;
using uPLibrary.Networking.M2Mqtt.Messages;

namespace CallCarMQTTServer
{
    public partial class Form1 : Form
    {
        MqttClient mqtt;
        private string ClientID;


        public Form1()
        {
            InitializeComponent();
        }

        private void btnConnect_Click(object sender, EventArgs e)
        {
            mqtt = new MqttClient(txtBrokerIP.Text);
            mqtt.MqttMsgPublishReceived += client_MqttMsgPublishReceived;
            ClientID = Guid.NewGuid().ToString();
            mqtt.Connect(ClientID);
            if (mqtt.IsConnected)
                lbStatus.Text = "Connected";
            else
                lbStatus.Text = "Disconnected";

        }

        private void client_MqttMsgPublishReceived(object sender, MqttMsgPublishEventArgs e)
        {
            string msg = System.Text.Encoding.Default.GetString(e.Message);
            this.Invoke((MethodInvoker)(() => lbLogs.Items.Add("收到消息:" + msg)));
        }



        private void btnSub_Click(object sender, EventArgs e)
        {
            if (txtSubscribe.Text.Trim() != "")
                mqtt.Subscribe(new string[] { txtSubscribe.Text.Trim() }, new byte[] { MqttMsgBase.QOS_LEVEL_AT_LEAST_ONCE });
            else
                MessageBox.Show("尚未輸入訂閱主題");
        }

        private void btnPublish_Click(object sender, EventArgs e)
        {
            if (TxtPublish.Text.Trim() == "")
            {
                MessageBox.Show("尚未輸入發行主題");
                return;
            }
            mqtt.Publish(TxtPublish.Text.Trim(), Encoding.UTF8.GetBytes(txtMessage.Text.Trim()), MqttMsgBase.QOS_LEVEL_AT_LEAST_ONCE, false);
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            mqtt.Disconnect();
        }
    }
}
