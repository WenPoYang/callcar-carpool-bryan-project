﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MySql.Data.MySqlClient;
using Callcar.BAL;
using Callcar.DAL;
using Callcar.CObject;
using Newtonsoft.Json;
using System.Data;
using System.Data.SqlClient;
using DbConn;
using NLog;

using System.Collections.Specialized;
using System.Configuration;

namespace DispatchRefresh
{
    class Program
    {
        public static StringBuilder sb = new StringBuilder();
        public static List<MySqlParameter> parms = new List<MySqlParameter>();
        public static string ConnectionString;
        private static Logger logger = NLog.LogManager.GetCurrentClassLogger();
        private static string CRMSConnectionString = "Data Source=112.121.69.1;Initial Catalog=CRMS_v2;Persist Security Info=True;User ID=sa;password=2Axijoll";
        private static string Server;

        static void Main(string[] args)
        {
            NameValueCollection nc = ConfigurationManager.AppSettings;
            string Dno, RefreshType;

            Server = nc["Server"];
            if (Server == "Production")
            {
                ConnectionString = "Server=10.0.2.23;Port=3306;Database=carpoollab;Uid=ishareco;Pwd=fu0!j0$su0^vup#;Allow User Variables=True";
            }
            else if (Server == "Lab")
            {
                ConnectionString = "Server=10.0.2.21;Port=3306;Database=carpoollab;Uid=apiuser;Pwd=7oPOh88I;Allow User Variables=True";
            }

            //1. 取得需檢查資料的派車單
            #region 檢查dispatch_refresh
            DataTable dtDispatch = new DataTable();
            try
            {
                dtDispatch = GetDispatchs();
            }
            catch (Exception ex)
            {
                logger.Error(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "取得派車更新資料失敗: " + ex.Message + Environment.NewLine + ex.StackTrace);
                Environment.Exit(0);
            }

            if (dtDispatch == null || dtDispatch.Rows.Count == 0)
            {
                logger.Info(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + " 無派車資料更新");
                Environment.Exit(0);
            }
            //2. 逐筆檢查

            for (int i = 0; i < dtDispatch.Rows.Count; i++)
            {
                Dno = Convert.ToString(dtDispatch.Rows[i]["DispatchNo"]);
                RefreshType = Convert.ToString(dtDispatch.Rows[i]["RefreshType"]);

                if (RefreshType == "I" || RefreshType == "D")
                {
                    //只在正式環境執行
                    if (Server == "Lab")
                        continue;

                    //新增或移除CRMS資料
                    try
                    {
                        if (ModifyCRMS(RefreshType, Dno))
                        {
                            try
                            {
                                FinishRefresh(RefreshType, Dno);
                            }
                            catch (Exception ex)
                            {
                                logger.Error(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "更新派車重整檔失敗: " + Dno + Environment.NewLine + "錯誤訊息：" + ex.Message + Environment.NewLine + ex.StackTrace);
                                continue;
                            }
                        }
                    }
                    catch (SqlException ex)
                    {
                        logger.Error(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "更新派車重整檔失敗: " + Dno + Environment.NewLine + "錯誤訊息：" + ex.Message + Environment.NewLine + ex.StackTrace);
                        continue;
                    }
                }
                else if (RefreshType == "U")
                {
                    //更新派車單
                    try
                    {
                        if (UpdateDispatch(Dno))
                        {
                            try
                            {
                                //更新CRMS
                                if (ModifyCRMS(RefreshType, Dno))
                                {
                                    //3. 更新 派車重整檔
                                    try
                                    {
                                        FinishRefresh(RefreshType, Dno);
                                    }
                                    catch (Exception ex)
                                    {
                                        logger.Error(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "更新派車重整檔失敗: " + Dno + Environment.NewLine + "錯誤訊息：" + ex.Message + Environment.NewLine + ex.StackTrace);
                                        continue;
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                logger.Error(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "更新CRMS失敗: " + Dno + Environment.NewLine + "錯誤訊息：" + ex.Message + Environment.NewLine + ex.StackTrace);
                                continue;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        logger.Error(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "更新派車資料失敗: " + Dno + Environment.NewLine + "錯誤訊息：" + ex.Message + Environment.NewLine + ex.StackTrace);
                        continue;
                    }
                }
            }
            #endregion
            dtDispatch = null;

            #region 移除服務件數為0的
            try
            {
                dtDispatch = GetZeroDispatch();
            }
            catch (Exception ex)
            {
                logger.Error(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "取得StopCnt = 0 派車單失敗: " + ex.Message + Environment.NewLine + ex.StackTrace);
                Environment.Exit(0);
            }
            if (dtDispatch == null || dtDispatch.Rows.Count == 0)
            {
                logger.Info(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "完成更新派車資料");
                Environment.Exit(0);
            }
            for (int i = 0; i < dtDispatch.Rows.Count; i++)
            {
                Dno = Convert.ToString(dtDispatch.Rows[i]["DispatchNo"]);

                try
                {
                    RemoveZeroDispatch(Dno);
                }
                catch (Exception ex)
                {
                    logger.Error(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "移除StopCnt = 0 派車單失敗 Dno = " + Dno + " : " + ex.Message + Environment.NewLine + ex.StackTrace);
                    continue;
                }

            }
            #endregion

            logger.Info(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "完成更新派車資料");
            Environment.Exit(0);
        }

        public static DataTable GetDispatchs()
        {
            sb.Length = 0;
            parms.Clear();

            sb.Append("SELECT Distinct RefreshType, DispatchNo FROM dispatch_refresh_record where RefreshFlag = '0' Order by CreateTime ASC ");

            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(ConnectionString))
                {
                    return conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static DataTable GetZeroDispatch()
        {
            sb.Length = 0;
            parms.Clear();

            sb.Append("SELECT DispatchNo  FROM dispatch_sheet  where StopCnt = 0  and ServeDate > date_format(now(),'%Y%m%d')  Order by UpdTime ASC ");

            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(ConnectionString))
                {
                    return conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool UpdateDispatch(string Dno)
        {
            BALDispatch bald = new BALDispatch(ConnectionString);
            ODispatchSheet dispatch = bald.GetDispatchSheetByNo(Dno);
            if (dispatch == null)
            {
                FinishRefresh("U", Dno);
                throw new Exception("查無此派車單號：" + Dno);

            }
            int PCnt = 0, BCnt = 0;
            sb.Length = 0;
            parms.Clear();
            List<DateTime> ServiceTime = new List<DateTime>();

            if (dispatch.ServiceCnt == 0)
            {
                sb.Append("DELETE FROM dispatch_sheet   WHERE DispatchNo = @no ");
                parms.Add(new MySqlParameter("@no", Dno));

                try
                {
                    using (dbConnectionMySQL conn = new dbConnectionMySQL(ConnectionString))
                    {
                        if (conn.executeDeleteQuery(sb.ToString(), parms.ToArray()) <= 0)
                            return false;
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("更新派車主檔失敗：" + ex.Message + Environment.NewLine + ex.StackTrace);
                }
            }
            else
            {
                foreach (ODpServiceUnit su in dispatch.ServiceList)
                {
                    PCnt += su.PassengerCnt;
                    BCnt += su.BaggageCnt;
                    ServiceTime.Add(su.ScheduleTime);
                }
                ServiceTime.Sort((a, b) => a.CompareTo(b));

                //訂單數量不符 or 人數不符 or 行李數量不符 更新訂單主檔
                if (dispatch.ServiceCnt != dispatch.ServiceList.Count || dispatch.PassengerCnt != PCnt || dispatch.BaggageCnt != BCnt || dispatch.FisrtServiceTime != ServiceTime[0].ToString("HHmm"))
                {
                    sb.Append("UPDATE dispatch_sheet  SET PassengerCnt=@pcnt,BaggageCnt=@bcnt,StopCnt=@scnt,DispatchTime = @DispatchTime  WHERE DispatchNo = @no ");
                    parms.Add(new MySqlParameter("@pcnt", PCnt));
                    parms.Add(new MySqlParameter("@bcnt", BCnt));
                    parms.Add(new MySqlParameter("@scnt", dispatch.ServiceList.Count));
                    parms.Add(new MySqlParameter("@DispatchTime", ServiceTime[0].ToString("HHmm")));
                    parms.Add(new MySqlParameter("@no", Dno));
                    try
                    {
                        using (dbConnectionMySQL conn = new dbConnectionMySQL(ConnectionString))
                        {
                            if (conn.executeUpdateQuery(sb.ToString(), parms.ToArray()) <= 0)
                                return false;
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("更新派車主檔失敗：" + ex.Message + Environment.NewLine + ex.StackTrace);
                    }
                }

                //計算訂單金額
                OReservation or;
                BALReservation balr = new BALReservation(ConnectionString);
                BALGeneral balg = new BALGeneral(ConnectionString);
                DALGeneral dalg = new DALGeneral(ConnectionString);
                //int Price = 0;
                //string[] Fee;
                OPrice op;
                foreach (ODpServiceUnit su in dispatch.ServiceList)
                {
                    or = balr.GetReservationByNo(su.ReservationNo);
                    op = balg.CalculateTrailFee(or.OrderTime.ToString("yyyyMMdd"), or.ServeDate, or.PreferTime, or.PassengerCnt, or.BaggageCnt, or.Coupon, PCnt - or.PassengerCnt, or.AddBagCnt, false, su.ScheduleTime.ToString("HHmm"), or.UserID);
                    // Price = op.Price;// Int32.Parse(Fee[0]) + Int32.Parse(Fee[1]) + Int32.Parse(Fee[2]) - Int32.Parse(Fee[3]);

                    try
                    {
                        if (!dalg.InsertTrailFee(su.ReservationNo, op)) // Price))
                            return false;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("更新訂單價格資料失敗" + dispatch.DispatchNo + " " + or.ReservationNo + " : " + Environment.NewLine + ex.Message + Environment.NewLine + ex.StackTrace);
                    }
                }
            }
            return true;
        }

        public static bool ModifyCRMS(string ModifyFlag, string Dno)
        {
            if (Server == "Lab")
                return true;

            if (ModifyFlag == "U")
            {
                BALDispatch bald = new BALDispatch(ConnectionString);
                ODispatchSheet dispatch = bald.GetDispatchSheetByNo(Dno);
                if (dispatch == null)
                {
                    FinishRefresh("U", Dno);
                    return true;
                }
                string CityCode, DistinctCode;
                using (SqlConnection conn = new SqlConnection(CRMSConnectionString))
                {
                    try
                    {
                        conn.Open();
                    }
                    catch (SqlException ex)
                    {
                        throw ex;
                    }

                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Connection = conn;
                        cmd.CommandText = "SELECT (SELECT a.AreaId FROM CD003 a WHERE a.AreaName2 = @city AND a.AreaId = b.ParentId) AS 'city',b.AreaId AS 'distinct' " +
                                                                  "FROM CD003 b WHERE b.AreaName2 = @distinct  AND b.ParentId = ( SELECT AreaId FROM CD003 WHERE AreaName2 = @city)";

                        cmd.Parameters.AddWithValue("@city", dispatch.ServiceList[0].MainCity);
                        cmd.Parameters.AddWithValue("@distinct", dispatch.ServiceList[0].MainDistinct);

                        try
                        {
                            using (SqlDataReader dr = cmd.ExecuteReader())
                            {
                                if (dr.HasRows)
                                {
                                    dr.Read();
                                    CityCode = Convert.ToString(dr["city"]);
                                    DistinctCode = Convert.ToString(dr["distinct"]);
                                }
                                else
                                {
                                    logger.Error(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + " 新增CRMS：派車編號  " + Dno + "   查無城市資料");
                                    CityCode = "00000";
                                    DistinctCode = "00000";
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            logger.Error(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + " 新增CRMS：派車編號  " + Dno + "   城市查詢失敗" + Environment.NewLine + ex.StackTrace);
                            CityCode = "00000";
                            DistinctCode = "00000";
                        }
                    }

                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Connection = conn;
                        cmd.CommandText = "Update CRM013 Set    ServiceTime1_Hour = @time, AreaNameA1 = @areaname1, AreaNameA2 = @areaname2, AddressA = @address, PassengerCnt = @pcnt, BaggageCnt = @bcnt,StopCnt = @scnt  Where Sno = @Sno ";
                        cmd.Parameters.AddWithValue("@time", dispatch.FisrtServiceTime.Substring(0, 2) + ":" + dispatch.FisrtServiceTime.Substring(2, 2));
                        cmd.Parameters.AddWithValue("@areaname1", CityCode);
                        cmd.Parameters.AddWithValue("@areaname2", DistinctCode);
                        cmd.Parameters.AddWithValue("@address", dispatch.ServiceList[0].MainAddress);
                        cmd.Parameters.AddWithValue("@pcnt", dispatch.PassengerCnt);
                        cmd.Parameters.AddWithValue("@bcnt", dispatch.BaggageCnt);
                        cmd.Parameters.AddWithValue("@scnt", dispatch.ServiceCnt - 1);
                        cmd.Parameters.AddWithValue("@Sno", Dno);

                        try
                        {
                            cmd.ExecuteNonQuery();
                        }
                        catch (SqlException ex)
                        {
                            throw ex;
                        }
                    }
                }
            }
            else if (ModifyFlag == "D")
            {
                using (SqlConnection conn = new SqlConnection(CRMSConnectionString))
                {
                    try
                    {
                        conn.Open();
                    }
                    catch (SqlException ex)
                    {
                        throw ex;
                    }
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Connection = conn;
                        cmd.CommandText = "Update CRM013 Set CloseFlag = 4 ,Cancel = 0 , FleetCode = '',FleetName = '',CarNo = '', DriverEmpId = ''  Where Sno = @Sno ";
                        cmd.Parameters.AddWithValue("@Sno", Dno);

                        try
                        {
                            cmd.ExecuteNonQuery();
                        }
                        catch (SqlException ex)
                        {
                            throw ex;
                        }
                    }
                }
            }
            else if (ModifyFlag == "I")
            {
                BALDispatch bald = new BALDispatch(ConnectionString);
                ODispatchSheet dispatch = bald.GetDispatchSheetByNo(Dno);
                StringBuilder sb = new StringBuilder();
                string City, Distinct;
                sb.Append("INSERT INTO CRM013 ");
                sb.Append("(Sno, UserId, ServiceCode, ServiceName, ServiceType, ReturnFlag, CompCode, CompName, MbrId, Employer, CallCarUser_Name, CallCarUser_CellPhone, CallCarUser_Tel, Email, SendSMSToCallCarUser, AcceptEmpId, AcceptEmpName, AcceptTime, ReserveTime, ArriveTimeFlag, CarTypeCode, CarTypeName, ServiceTime1, ServiceTime1_Hour, SpecifiedTime, ServiceTime2, AreaNameA1, AreaNameA2, AddressA, AreaNameB1, AreaNameB2, AddressB, FlightNo, FlightTime, Terminal, BabyAmount, TimeItemCode, TimeItemName, ExtraHour, MbrCompCode, ContactNameA, ContactTelA1, ContactTelA2, ContactAddressA, ContactAddressKmA, SendSMSToContactA, ContactNameB, ContactTelB1, ContactTelB2, ContactAddressB, ContactAddressKmB, SendSMSToContactB, ContactNameC, ContactTelC1, ContactTelC2, ContactAddressC, ContactAddressKmC, SendSMSToContactC, ContactNameD, ContactTelD1, ContactTelD2, ContactAddressD, ContactAddressKmD, SendSMSToContactD, PassengerCnt, PassengerList, BaggageCnt, BaggageNote, BoardFlag, StopCnt, StopNote, Memo1, Memo2, SpecDriverFlag, SpecDriverEmpId, SpecDriverEmpName, CMCode, CMName, IssueBankName, CreditCardNo, CreditCardNo_LastNum, Department, PrjLevel, ValidYM, CardVerifyNo, CardVerifyNo2, Pay_Date, InvoiceReceiver, InvoiceAddress, InvoiceType, GUINo, InvoiceTitle, ATMAccountNo, StartRoute1, StopRoute1, StartRoute2, StopRoute2, OrgClctAmt, ShouldClctAmt, RealClctAmt, RealClctDate, DiscountNote, CouponNo, CouponAmt, PromotionCode, PromotionName, FleetCode, FleetName, CarNo, DriverEmpId, CloseFlag, Cancel, CancelCode, CancelName, CancelTime, CancelEmpId, RecoveryName, RecoveryTime, RecoveryEmpId, CallbackEmpId, CallbackTime, SendEmailTime, SendEmailFlag, needSMS, SendSMSTime1, SendSMSFlag1, SendSMSTime2, SendSMSFlag2, CloseRcdTime, CloseRcdEmpId, UpdTime, UpdEmpId, UpdEmpName, Route1Kilo, Route2Kilo, SnoSendSMSFlag1, SnoSendSMSFlag2, SnoSendEmailFlag1, SnoSendEmailFlag2, SalesEmpId, SalesEmpName, BoardMemo, FlightNo2, FlightTime2, AreaNameC1, AreaNameC2, AddressC, AreaNameD1, AreaNameD2, AddressD, OrderMbrName, OrderMbrTel, ExSMSReceive, CorelateSno, MbrCostDep, OrderChgDt, SpecCarGroup, SpecCarTypeWish, SpecCarTypeSelf, VIP_Flag, JCB_CardNo, Call_Location, InStopCnt, OutStopCnt, Card_Type, SafeseatCnt, TransDate, J_Expense, J_Channel, CompanyBaseCost, CompanyOtherCost, CompanyNightCost, CompanyDockCost, CompanyBoardCost, CompanyWaitCost, CompanyBabyCost, CompanySuburbsCost, CompanyNoBodyCost, CompanyCostAmount, CashBaseCost, CashOtherCost, CashNightCost, CashDockCost, CashBoardCost, CashWaitCost, CashBabyCost, CashSuburbsCost, CashNoBodyCost, CashAgencyCost, CashCostAmount, CardFundsBaseCost, CardFundsOtherCost, CardFundsNightCost, CardFundsDockCost, CardFundsBoardCost, CardFundsWaitCost, CardFundsBabyCost, CardFundsSuburbsCost, CardFundsNoBodyCost, CardFundsCostAmount, CardFundsCreditCardNumber, CardFundsHolder, CardFundsExpirationDate, CardFundsSecurityCode, CardFundsInvoiceAddressee, CardFundsInvoice, CardFundsVAT, CardFundsInvoiceAddress, TotalCost, NotificationDriver, Create_Date, JB, DriverNote, Signature_AutoID, CompanyBaseCostForCus, CompanyOtherCostForCus, CompanyNightCostForCus, CompanyDockCostForCus, CompanyBoardCostForCus, CompanyWaitCostForCus, CompanyBabyCostForCus, CompanySuburbsCostForCus, CompanyNoBodyCostForCus, CompanyCostAmountForCus, CashAgencyCostForCus, Memo_Get_ForCus, initForCus, Favorability, Q2, Q3, Q4, Q5, Q6, SignType, SignFilePath, BoardPath, BoardFileName, AllianceDPAmount, newAreaNameA1, newAreaNameA2, newAreaNameB1, newAreaNameB2, CRM011_Email) ");
                sb.Append("VALUES ");
                sb.Append("(@Sno, NULL, NULL, @ServiceName, NULL, 0, NULL, NULL, 56347, '艾雪科技', 'CallCar共乘', '0', '', '', 0, '', '', getdate(), NULL, 1, NULL, @CarType, @ServiceTime1, @ServiceTime1_Hour, 0, NULL, @AreaNameA1, @AreaNameA2, @AddressA, '-1', '-1', '', '', '', @Ternimal, @BabyAmt, NULL, NULL, 0, NULL, '', '', NULL, '', 'KM', 0, '', '', NULL, '', 'KM', 0, '', '', NULL, '', 'KM', 0, '', '', NULL, '', 'KM', 0, @Pcnt, NULL, @Bcnt, '', 0, @Scnt, NULL, '', '', 0, NULL, NULL, NULL, NULL, NULL, '-    -', '', '', '尚無選擇', NULL, '', '', getdate(), NULL, NULL, 0, NULL, NULL, NULL, ' ', ' ', ' ', ' ', NULL, NULL, 0, getdate(), NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, @CloseFlag, @Cancel, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, NULL, 0, NULL, NULL, NULL, '', '', 0, 0, 1, 1, 1, 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 1, NULL, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '-    -    -', '', '', '', '', '無', '', '', 1, 0, getdate(), NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL) ");

                using (SqlConnection conn = new SqlConnection(CRMSConnectionString))
                {
                    try
                    {
                        conn.Open();
                    }
                    catch (SqlException ex)
                    {
                        throw ex;
                    }
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Connection = conn;
                        cmd.CommandText = "SELECT (SELECT a.AreaId FROM CD003 a WHERE a.AreaName2 = @city AND a.AreaId = b.ParentId) AS 'city',b.AreaId AS 'distinct' " +
                                                                  "FROM CD003 b WHERE b.AreaName2 = @distinct  AND b.ParentId = ( SELECT AreaId FROM CD003 WHERE AreaName2 = @city)";

                        cmd.Parameters.AddWithValue("@city", dispatch.ServiceList[0].MainCity);
                        cmd.Parameters.AddWithValue("@distinct", dispatch.ServiceList[0].MainDistinct);

                        try
                        {
                            using (SqlDataReader dr = cmd.ExecuteReader())
                            {
                                if (dr.HasRows)
                                {
                                    dr.Read();
                                    City = Convert.ToString(dr["city"]);
                                    Distinct = Convert.ToString(dr["distinct"]);
                                }
                                else
                                {
                                    logger.Error(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + " 新增CRMS：派車編號  " + Dno + "   查無城市資料");
                                    City = "00000";
                                    Distinct = "00000";
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            logger.Error(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + " 新增CRMS：派車編號  " + Dno + "   城市查詢失敗" + Environment.NewLine + ex.StackTrace);
                            City = "00000";
                            Distinct = "00000";
                        }
                    }

                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Connection = conn;
                        cmd.CommandText = sb.ToString();
                        cmd.Parameters.AddWithValue("@Sno", dispatch.DispatchNo);
                        cmd.Parameters.AddWithValue("@ServiceName", (dispatch.ServiceType == "I") ? "回國" : "出國");
                        cmd.Parameters.AddWithValue("@CarType", dispatch.CarType);
                        cmd.Parameters.AddWithValue("@ServiceTime1", dispatch.ServiceList[0].ScheduleTime);
                        cmd.Parameters.AddWithValue("@ServiceTime1_Hour", dispatch.ServiceList[0].ScheduleTime.ToString("HH:mm"));
                        cmd.Parameters.AddWithValue("@AreaNameA1", City);
                        cmd.Parameters.AddWithValue("@AreaNameA2", Distinct);
                        cmd.Parameters.AddWithValue("@AddressA", dispatch.ServiceList[0].MainAddress);
                        if (dispatch.ServiceList[0].AirportAddress.Contains("第二航廈"))
                            cmd.Parameters.AddWithValue("@Ternimal", "桃T2");
                        else if (dispatch.ServiceList[0].AirportAddress.Contains("第一航廈"))
                            cmd.Parameters.AddWithValue("@Ternimal", "桃T1");
                        else
                            cmd.Parameters.AddWithValue("@Ternimal", "");

                        cmd.Parameters.AddWithValue("@BabyAmt", 0);
                        cmd.Parameters.AddWithValue("@Pcnt", dispatch.PassengerCnt);
                        cmd.Parameters.AddWithValue("@Bcnt", dispatch.BaggageCnt);
                        cmd.Parameters.AddWithValue("@Scnt", dispatch.ServiceList.Count - 1);
                        cmd.Parameters.AddWithValue("@CloseFlag", '1');
                        cmd.Parameters.AddWithValue("@Cancel", '0');

                        try
                        {
                            cmd.ExecuteNonQuery();
                        }
                        catch (SqlException ex)
                        {
                            logger.Error(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + " 新增CRMS 失敗：派車編號  " + Dno + Environment.NewLine + ex.StackTrace);
                            throw ex;
                        }
                    }
                }
            }

            return true;
        }

        public static int FinishRefresh(string ModifyFlag, string Dno)
        {
            sb.Length = 0;
            parms.Clear();
            sb.Append("UPDATE dispatch_refresh_record SET RefreshFlag='1' 	WHERE DispatchNo = @no and RefreshType = @type");
            parms.Add(new MySqlParameter("@no", Dno));
            parms.Add(new MySqlParameter("@type", ModifyFlag));

            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(ConnectionString))
                {
                    return conn.executeUpdateQuery(sb.ToString(), parms.ToArray());
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public static bool RemoveZeroDispatch(string Dno)
        {
            sb.Length = 0;
            parms.Clear();

            sb.Append("DELETE FROM dispatch_sheet   WHERE DispatchNo = @no ");
            parms.Add(new MySqlParameter("@no", Dno));

            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(ConnectionString))
                {
                    if (conn.executeDeleteQuery(sb.ToString(), parms.ToArray()) <= 0)
                        return false;
                    else
                        return true;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("刪除StopCnt = 0 派車主檔失敗：" + ex.Message + Environment.NewLine + ex.StackTrace);
            }
        }
    }
}
