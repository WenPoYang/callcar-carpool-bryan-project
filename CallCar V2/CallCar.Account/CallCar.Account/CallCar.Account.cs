﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CallCar;
using MySql.Data.MySqlClient;
using System.Data;
using DbConn;


namespace CallCar.Account
{
    public class dmUserAccount : DataModel
    {
        public dmUserAccount(dbConnectionMySQL Conn) : base(Conn) { }

        public override int Insert()
        {
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            sb.Append("INSERT INTO account ");
            sb.Append("(UserID, Source, SourceID, Password, NickName, FirstName, LastName, MidName, Gender, AgeRange, Local, Email, Birthday, MobilePhone, Introducer) ");
            sb.Append("VALUES(@UserID, @Source, @SourceID, @Password, @NickName, @FirstName, @LastName, @MidName, @Gender, @AgeRange, @Local, @Email, @Birthday, @MobilePhone, @Introducer )");

            parms.Add(new MySqlParameter("@UserID", this.UserID));
            parms.Add(new MySqlParameter("@Source", this.Source));
            parms.Add(new MySqlParameter("@SourceID", this.SourceID));
            parms.Add(new MySqlParameter("@Password", this.Password));
            parms.Add(new MySqlParameter("@NickName", this.NickName));
            parms.Add(new MySqlParameter("@FirstName", this.FirstName));
            parms.Add(new MySqlParameter("@LastName", this.LastName));
            parms.Add(new MySqlParameter("@MidName", this.MidName));
            parms.Add(new MySqlParameter("@Gender", this.Gender));
            parms.Add(new MySqlParameter("@AgeRange", this.AgeRange));
            parms.Add(new MySqlParameter("@Local", this.Local));
            parms.Add(new MySqlParameter("@Email", this.Email));
            parms.Add(new MySqlParameter("@Birthday", this.Birthday));
            parms.Add(new MySqlParameter("@MobilePhone", this.MobilePhone));
            parms.Add(new MySqlParameter("@Introducer", this.Introducer));

            try
            {
                return this._conn.executeInsertQuery(sb.ToString(), parms.ToArray());
            }
            catch (MySqlException ex)
            {
                throw ex;
            }
        }
        public override void Select(int UserID)
        {
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            DataTable dt;
            sb.Append("SELECT UserID, Source, SourceID, Password, NickName, FirstName, LastName, MidName, Gender, AgeRange, Local, Email, Birthday, MobilePhone,Introducer, CreateTime, UpdTime ");
            sb.Append("FROM account where UserID = @UserID  ");
            parms.Add(new MySqlParameter("@UserID", UserID));
            try
            {
                dt = this._conn.executeSelectQuery(sb.ToString(), parms.ToArray());
            }
            catch (MySqlException ex)
            {
                throw ex;
            }

            if (dt.Rows.Count == 1)
            {
                try
                {
                    this.UserID = Convert.ToInt32(dt.Rows[0]["UserID"]);
                    this.Source = Convert.ToString(dt.Rows[0]["Source"]);
                    this.SourceID = Convert.ToString(dt.Rows[0]["SourceID"]);
                    this.Password = Convert.ToString(dt.Rows[0]["Password"]);
                    this.NickName = Convert.ToString(dt.Rows[0]["NickName"]);
                    this.FirstName = Convert.ToString(dt.Rows[0]["FirstName"]);
                    this.LastName = Convert.ToString(dt.Rows[0]["LastName"]);
                    this.MidName = Convert.ToString(dt.Rows[0]["MidName"]);
                    this.Gender = Convert.ToString(dt.Rows[0]["Gender"]);
                    this.AgeRange = Convert.ToString(dt.Rows[0]["AgeRange"]);
                    this.Local = Convert.ToString(dt.Rows[0]["Local"]);
                    this.Email = Convert.ToString(dt.Rows[0]["Email"]);
                    this.Birthday = Convert.ToString(dt.Rows[0]["Birthday"]);
                    this.MobilePhone = Convert.ToString(dt.Rows[0]["MobilePhone"]);
                    this.Introducer = Convert.ToInt32(dt.Rows[0]["Introducer"]);
                    this.CreateTime = Convert.ToDateTime(dt.Rows[0]["CreateTime"]);
                    this.UpdTime = Convert.ToDateTime(dt.Rows[0]["UpdTime"]);
                }
                catch (Exception ex)
                { throw ex; }
            }
            else
            {
                this.UserID = 0;
            }
        }

        public override void Select(string Email)
        {
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            DataTable dt;
            sb.Append("SELECT UserID, Source, SourceID, Password, NickName, FirstName, LastName, MidName, Gender, AgeRange, Local, Email, Birthday, MobilePhone, Introducer,CreateTime, UpdTime ");
            sb.Append("FROM account where Email = @Email  ");
            parms.Add(new MySqlParameter("@Email", Email));
            try
            {
                dt = this._conn.executeSelectQuery(sb.ToString(), parms.ToArray());
            }
            catch (MySqlException ex)
            {
                throw ex;
            }

            if (dt.Rows.Count == 1)
            {
                try
                {
                    this.UserID = Convert.ToInt32(dt.Rows[0]["UserID"]);
                    this.Source = Convert.ToString(dt.Rows[0]["Source"]);
                    this.SourceID = Convert.ToString(dt.Rows[0]["SourceID"]);
                    this.Password = Convert.ToString(dt.Rows[0]["Password"]);
                    this.NickName = Convert.ToString(dt.Rows[0]["NickName"]);
                    this.FirstName = Convert.ToString(dt.Rows[0]["FirstName"]);
                    this.LastName = Convert.ToString(dt.Rows[0]["LastName"]);
                    this.MidName = Convert.ToString(dt.Rows[0]["MidName"]);
                    this.Gender = Convert.ToString(dt.Rows[0]["Gender"]);
                    this.AgeRange = Convert.ToString(dt.Rows[0]["AgeRange"]);
                    this.Local = Convert.ToString(dt.Rows[0]["Local"]);
                    this.Email = Convert.ToString(dt.Rows[0]["Email"]);
                    this.Birthday = Convert.ToString(dt.Rows[0]["Birthday"]);
                    this.MobilePhone = Convert.ToString(dt.Rows[0]["MobilePhone"]);
                    this.Introducer = Convert.ToInt32(dt.Rows[0]["Introducer"]);
                    this.CreateTime = Convert.ToDateTime(dt.Rows[0]["CreateTime"]);
                    this.UpdTime = Convert.ToDateTime(dt.Rows[0]["UpdTime"]);
                }
                catch (Exception ex)
                { throw ex; }
            }
            else
                this.UserID = 0;
        }
        public override int Update()
        {
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            sb.Append("UPDATE account ");
            sb.Append("SET Source = @Source, SourceID = @SourceID, Password = @Password, NickName = @NickName, FirstName = @FirstName, LastName = @LastName,Introducer = @Introducer, ");
            sb.Append("MidName = @MidName, Gender = @Gender, AgeRange = @AgeRange, Local = @Local, Email = @Email, Birthday = @Birthday, MobilePhon = @MobilePhon ");
            sb.Append("WHERE UserID = @UserID ");

            parms.Add(new MySqlParameter("@Source", this.Source));
            parms.Add(new MySqlParameter("@Source", this.Source));
            parms.Add(new MySqlParameter("@SourceID", this.SourceID));
            parms.Add(new MySqlParameter("@Password", this.Password));
            parms.Add(new MySqlParameter("@NickName", this.NickName));
            parms.Add(new MySqlParameter("@FirstName", this.FirstName));
            parms.Add(new MySqlParameter("@LastName", this.LastName));
            parms.Add(new MySqlParameter("@MidName", this.MidName));
            parms.Add(new MySqlParameter("@Gender", this.Gender));
            parms.Add(new MySqlParameter("@AgeRange", this.AgeRange));
            parms.Add(new MySqlParameter("@Local", this.Local));
            parms.Add(new MySqlParameter("@Email", this.Email));
            parms.Add(new MySqlParameter("@Birthday", this.Birthday));
            parms.Add(new MySqlParameter("@MobilePhone", this.MobilePhone));
            parms.Add(new MySqlParameter("@Introducer", this.Introducer));
            parms.Add(new MySqlParameter("@UserID", this.UserID));

            try
            {
                return this._conn.executeUpdateQuery(sb.ToString(), parms.ToArray());
            }
            catch (MySqlException ex)
            {
                throw ex;
            }
        }

        public int UserID { get; set; }
        public string Source { get; set; }
        public string SourceID { get; set; }
        public string Password { get; set; }
        public string NickName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MidName { get; set; }
        public string Gender { get; set; }
        public string AgeRange { get; set; }
        public string Local { get; set; }
        public string Email { get; set; }
        public string Birthday { get; set; }
        public string MobilePhone { get; set; }
        public int Introducer { get; set; }
        public DateTime CreateTime { get; set; }
        public DateTime UpdTime { get; set; }
    }
    public class dmEmployeeAccount : DataModel
    {
        public dmEmployeeAccount(dbConnectionMySQL Conn) : base(Conn) { }

        public override void Select(string CitizenID)
        {
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            DataTable dt;
            sb.Append("SELECT UserID, Password, UserName, CitizenID, Email, MobilePhone, HomePhone, UserRole, CompanyNo, FleetID, ShiftType, City, DistinctName, Address, ActiveFlag,CreateTime,UpdTime ");
            sb.Append("FROM user_info where CitizenID = @CitizenID");
            parms.Add(new MySqlParameter("@CitizenID", CitizenID));
            try
            {
                dt = this._conn.executeSelectQuery(sb.ToString(), parms.ToArray());
            }
            catch (MySqlException ex)
            {
                throw ex;
            }

            if (dt.Rows.Count == 1)
            {
                try
                {
                    this.UserID = Convert.ToInt32(dt.Rows[0]["UserID"]);
                    this.Password = Convert.ToString(dt.Rows[0]["Password"]);
                    this.ActiveFlag = Convert.ToString(dt.Rows[0]["ActiveFlag"]);
                    this.Address = Convert.ToString(dt.Rows[0]["Address"]);
                    this.CitizenID = Convert.ToString(dt.Rows[0]["CitizenID"]);
                    this.City = Convert.ToString(dt.Rows[0]["City"]);
                    this.CompanyNo = Convert.ToString(dt.Rows[0]["CompanyNo"]);
                    this.DistinctName = Convert.ToString(dt.Rows[0]["DistinctName"]);
                    this.Email = Convert.ToString(dt.Rows[0]["Email"]);
                    this.FleetID = Convert.ToInt32(dt.Rows[0]["FleetID"]);
                    this.MobilePhone = Convert.ToString(dt.Rows[0]["MobilePhone"]);
                    this.HomePhone = Convert.ToString(dt.Rows[0]["HomePhone"]);
                    this.UserName = Convert.ToString(dt.Rows[0]["UserName"]);
                    this.UserRole = Convert.ToString(dt.Rows[0]["UserRole"]);
                    this.ShiftType = Convert.ToString(dt.Rows[0]["ShiftType"]);
                    this.CreateTime = Convert.ToDateTime(dt.Rows[0]["CreateTime"]);
                    this.UpdTime = Convert.ToDateTime(dt.Rows[0]["UpdTime"]);
                }
                catch (Exception ex)
                { throw ex; }
            }
            else
                this.UserID = 0;
        }

        public override void Select(int UserID)
        {
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            DataTable dt;
            sb.Append("SELECT UserID, Password, UserName, CitizenID, Email, MobilePhone, HomePhone, UserRole, CompanyNo, FleetID, ShiftType, City, DistinctName, Address, ActiveFlag,CreateTime,UpdTime  ");
            sb.Append("FROM user_info where UserID = @UserID");
            parms.Add(new MySqlParameter("@UserID", UserID));
            try
            {
                dt = this._conn.executeSelectQuery(sb.ToString(), parms.ToArray());
            }
            catch (MySqlException ex)
            {
                throw ex;
            }

            if (dt.Rows.Count == 1)
            {
                try
                {
                    this.UserID = Convert.ToInt32(dt.Rows[0]["UserID"]);
                    this.Password = Convert.ToString(dt.Rows[0]["Password"]);
                    this.ActiveFlag = Convert.ToString(dt.Rows[0]["ActiveFlag"]);
                    this.Address = Convert.ToString(dt.Rows[0]["Address"]);
                    this.CitizenID = Convert.ToString(dt.Rows[0]["CitizenID"]);
                    this.City = Convert.ToString(dt.Rows[0]["City"]);
                    this.CompanyNo = Convert.ToString(dt.Rows[0]["CompanyNo"]);
                    this.DistinctName = Convert.ToString(dt.Rows[0]["DistinctName"]);
                    this.Email = Convert.ToString(dt.Rows[0]["Email"]);
                    this.FleetID = Convert.ToInt32(dt.Rows[0]["FleetID"]);
                    this.MobilePhone = Convert.ToString(dt.Rows[0]["MobilePhone"]);
                    this.HomePhone = Convert.ToString(dt.Rows[0]["HomePhone"]);
                    this.UserName = Convert.ToString(dt.Rows[0]["UserName"]);
                    this.UserRole = Convert.ToString(dt.Rows[0]["UserRole"]);
                    this.ShiftType = Convert.ToString(dt.Rows[0]["ShiftType"]);
                    this.CreateTime = Convert.ToDateTime(dt.Rows[0]["CreateTime"]);
                    this.UpdTime = Convert.ToDateTime(dt.Rows[0]["UpdTime"]);
                }
                catch (Exception ex)
                { throw ex; }
            }
            else
                this.UserID = 0;
        }

        public int UserID { get; set; }
        public string Password { get; set; }
        public string UserName { get; set; }
        public string CitizenID { get; set; }
        public string Email { get; set; }
        public string MobilePhone { get; set; }
        public string HomePhone { get; set; }
        public string UserRole { get; set; }
        public string CompanyNo { get; set; }
        public int FleetID { get; set; }
        public string ShiftType { get; set; }
        public string City { get; set; }
        public string DistinctName { get; set; }
        public string Address { get; set; }
        public string ActiveFlag { get; set; }
        public DateTime CreateTime { get; set; }
        public DateTime UpdTime { get; set; }
    }
}
