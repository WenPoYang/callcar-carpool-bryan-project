﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CallCar;
using CallCar.Data;
using DbConn;
using CallCar.Tool;

namespace CallCar.Account
{
    public class BALAccount : CallCarBAL
    {
        public BALAccount(string ConnectionString) : base(ConnectionString) { }

        public OAccount GetUserAccount(int UserID)
        {
            OAccount oa = new OAccount();
            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._connectstring))
                {
                    dmUserAccount dm = new Account.dmUserAccount(conn);
                    dm.Select(UserID);

                    if (dm.UserID == 0)
                        return null;

                    oa.AgeRange = dm.AgeRange;
                    oa.Birthday = dm.Birthday;
                    oa.Email = dm.Email;
                    oa.FName = dm.FirstName;
                    oa.Gender = dm.Gender;
                    oa.ID = dm.UserID;
                    oa.LName = dm.LastName;
                    oa.Local = dm.Local;
                    oa.MName = dm.MidName;
                    oa.MobilePhone = dm.MobilePhone;
                    oa.NickName = dm.NickName;
                    oa.Password = dm.Password;
                    oa.Source = dm.Source;
                    oa.SourceID = dm.SourceID;
                    oa.Introducer = dm.Introducer;

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return oa;
        }
        public OAccount GetUserAccount(string Email)
        {
            OAccount oa = new OAccount();
            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._connectstring))
                {
                    dmUserAccount dm = new Account.dmUserAccount(conn);
                    dm.Select(Email);

                    if (dm.UserID == 0)
                        return null;

                    oa.AgeRange = dm.AgeRange;
                    oa.Birthday = dm.Birthday;
                    oa.Email = dm.Email;
                    oa.FName = dm.FirstName;
                    oa.Gender = dm.Gender;
                    oa.ID = dm.UserID;
                    oa.LName = dm.LastName;
                    oa.Local = dm.Local;
                    oa.MName = dm.MidName;
                    oa.MobilePhone = dm.MobilePhone;
                    oa.NickName = dm.NickName;
                    oa.Password = dm.Password;
                    oa.Source = dm.Source;
                    oa.SourceID = dm.SourceID;
                    oa.Introducer = dm.Introducer;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return oa;
        }
        public int CreateNewUserAccount(OAccount oa)
        {
            using (dbConnectionMySQL conn = new dbConnectionMySQL(this._connectstring))
            {
                dmUserAccount dm = new Account.dmUserAccount(conn);
                dm.AgeRange = oa.AgeRange;
                dm.Birthday = oa.Birthday;
                dm.Email = oa.Email;
                dm.FirstName = oa.FName;
                dm.Gender = oa.Gender;
                dm.LastName = oa.LName;
                dm.Local = oa.Local;
                dm.MidName = oa.MName;
                dm.MobilePhone = oa.MobilePhone;
                dm.NickName = oa.NickName;
                dm.Password = MD5Cryptography.Encrypt(oa.Password);
                dm.Source = oa.Source;
                dm.SourceID = oa.SourceID;
                dm.UserID = (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
                dm.Introducer = oa.Introducer;

                try
                {
                    dm.Insert();
                }
                catch (Exception ex)
                { throw ex; }

                return dm.UserID;
            }
        }
        public bool UpdUserPassword(int UserID, string oldPW, string newPW)
        {
            using (dbConnectionMySQL conn = new dbConnectionMySQL(this._connectstring))
            {
                dmUserAccount acc = new Account.dmUserAccount(conn);
                acc.Select(UserID);

                if (acc.Password != MD5Cryptography.Encrypt(oldPW))
                    return false;

                acc.Password = MD5Cryptography.Encrypt(newPW);

                try
                {
                    if (acc.Update() > 0)
                        return true;
                    else
                        return false;
                }
                catch (MySql.Data.MySqlClient.MySqlException ex)
                {
                    throw ex;
                }
            }

        }
        public bool UpdUserPassword(int UserID, string newPW)
        {
            using (dbConnectionMySQL conn = new dbConnectionMySQL(this._connectstring))
            {
                dmUserAccount acc = new Account.dmUserAccount(conn);
                acc.Select(UserID);

                acc.Password = MD5Cryptography.Encrypt(newPW);

                try
                {
                    if (acc.Update() > 0)
                        return true;
                    else
                        return false;
                }
                catch (MySql.Data.MySqlClient.MySqlException ex)
                {
                    throw ex;
                }
            }
        }
        public bool VarifyUserAccount(string Account, string Password)
        {
            using (dbConnectionMySQL conn = new dbConnectionMySQL(this._connectstring))
            {
                dmUserAccount acc = new Account.dmUserAccount(conn);
                acc.Select(Account);

                if (acc.Password != MD5Cryptography.Encrypt(Password))
                    return false;
                else
                    return true;
            }
        }
        public OAccount UpdUserInfo(int UserID, OAccount oa)
        {
            dmUserAccount dm;
            using (dbConnectionMySQL conn = new dbConnectionMySQL(this._connectstring))
            {
                dm = new Account.dmUserAccount(conn);

                dm.Select(UserID);

                dm.AgeRange = oa.AgeRange;
                dm.Birthday = oa.Birthday;
                dm.FirstName = oa.FName;
                dm.Gender = oa.Gender;
                dm.LastName = oa.LName;
                dm.Local = oa.Local;
                dm.MidName = oa.MName;
                dm.MobilePhone = oa.MobilePhone;
                dm.NickName = oa.NickName;

                dm.Update();
            }

            OAccount oa2 = new OAccount();
            oa2.AgeRange = dm.AgeRange;
            oa2.Birthday = dm.Birthday;
            oa2.Email = dm.Email;
            oa2.FName = dm.FirstName;
            oa2.Gender = dm.Gender;
            oa2.ID = dm.UserID;
            oa2.LName = dm.LastName;
            oa2.Local = dm.Local;
            oa2.MName = dm.MidName;
            oa2.MobilePhone = dm.MobilePhone;
            oa2.NickName = dm.NickName;
            oa2.Source = dm.Source;
            oa2.SourceID = dm.SourceID;
            return oa2;
        }

        public OEmployee GetEmployeeInfo(string CitizenID)
        {
            dmEmployeeAccount dm;
            using (dbConnectionMySQL conn = new dbConnectionMySQL(this._connectstring))
            {
                dm = new dmEmployeeAccount(conn);
                dm.Select(CitizenID);
            }

            OEmployee oe = new OEmployee();
            oe.Active = dm.ActiveFlag;
            oe.Address = dm.Address;
            oe.CID = dm.CitizenID;
            oe.City = dm.City;
            oe.CompanyNo = dm.CompanyNo;
            oe.Distinct = dm.DistinctName;
            oe.Email = dm.Email;
            oe.FleetID = dm.FleetID;
            oe.HomePhone = dm.HomePhone;
            oe.ID = dm.UserID;
            oe.MobilePhone = dm.MobilePhone;
            oe.Name = dm.UserName;
            oe.Role = dm.UserRole;
            return oe;
        }

        public OEmployee GetEmployeeInfo(int EmployeeID)
        {
            dmEmployeeAccount dm;
            using (dbConnectionMySQL conn = new dbConnectionMySQL(this._connectstring))
            {
                dm = new dmEmployeeAccount(conn);
                dm.Select(EmployeeID);
            }

            OEmployee oe = new OEmployee();
            oe.Active = dm.ActiveFlag;
            oe.Address = dm.Address;
            oe.CID = dm.CitizenID;
            oe.City = dm.City;
            oe.CompanyNo = dm.CompanyNo;
            oe.Distinct = dm.DistinctName;
            oe.Email = dm.Email;
            oe.FleetID = dm.FleetID;
            oe.HomePhone = dm.HomePhone;
            oe.ID = dm.UserID;
            oe.MobilePhone = dm.MobilePhone;
            oe.Name = dm.UserName;
            oe.Role = dm.UserRole;
            return oe;
        }
    }
}
