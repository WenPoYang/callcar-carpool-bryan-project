﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DbConn;
using MySql.Data.MySqlClient;
using System.Data;

namespace CallCar.Payment
{
    public class dmCreditCard : DataModel
    {
        public dmCreditCard(dbConnectionMySQL Conn) : base(Conn) { }

        public override int Insert()
        {
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();

            sb.Append("INSERT INTO profile_card_info ");
            sb.Append("(UserID, CardNo, Pay2goToken, Pay2goTokenExpire, CheckStatus) ");
            sb.Append("VALUES(@UserID, @CardNo, @Pay2goToken, @Pay2goTokenExpire, @CheckStatus)");

            parms.Add(new MySqlParameter("@UserID", this.UserID));
            parms.Add(new MySqlParameter("@CardNo", this.CardNo));
            parms.Add(new MySqlParameter("@Pay2goToken", this.Pay2goToken));
            parms.Add(new MySqlParameter("@Pay2goTokenExpire", this.Pay2goTokenExpire));
            parms.Add(new MySqlParameter("@CheckStatus", this.CheckStatus));

            try
            {
                return this._conn.executeInsertQuery(sb.ToString(), parms.ToArray());
            }
            catch (MySqlException ex)
            {
                throw ex;
            }
        }
        public override int Delete()
        {
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();

            sb.Append("Delete From profile_card_info ");
            sb.Append("Where  CardID = @CardID ");

            parms.Add(new MySqlParameter("@CardID", this.CardID));

            try
            {
                return this._conn.executeDeleteQuery(sb.ToString(), parms.ToArray());
            }
            catch (MySqlException ex)
            {
                throw ex;
            }
        }
        public override void Select(int CardID)
        {
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            DataTable dt;


            sb.Append("SELECT CardID, UserID, CardNo, Pay2goToken, Pay2goTokenExpire, CheckStatus,CreateTime, UpdTime ");
            sb.Append("FROM profile_card_info ");
            sb.Append("Where  CardID = @CardID ");

            parms.Add(new MySqlParameter("@CardID", CardID));

            try
            {
                dt = this._conn.executeSelectQuery(sb.ToString(), parms.ToArray());
            }
            catch (MySqlException ex)
            {
                throw ex;
            }

            try
            {
                this.CardID = Convert.ToInt32(dt.Rows[0]["CardID"]);
                this.UserID = Convert.ToInt32(dt.Rows[0]["UserID"]);
                this.CardNo = Convert.ToString(dt.Rows[0]["CardNo"]);
                this.Pay2goToken = Convert.ToString(dt.Rows[0]["Pay2goToken"]);
                this.Pay2goTokenExpire = Convert.ToString(dt.Rows[0]["Pay2goTokenExpire"]);
                this.CheckStatus = Convert.ToInt32(dt.Rows[0]["CheckStatus"]);
                this.CreateTime = Convert.ToDateTime(dt.Rows[0]["CreateTime"]);
                this.UpdTime = Convert.ToDateTime(dt.Rows[0]["UpdTime"]);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public override void Select(string UserID)
        {
            int UID = Int32.Parse(UserID);
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            DataTable dt;


            sb.Append("SELECT CardID, UserID, CardNo, Pay2goToken, Pay2goTokenExpire, CheckStatus,CreateTime, UpdTime ");
            sb.Append("FROM profile_card_info ");
            sb.Append("Where  UserID = @UID ");

            parms.Add(new MySqlParameter("@UID", UID));


            try
            {
                dt = this._conn.executeSelectQuery(sb.ToString(), parms.ToArray());
            }
            catch (MySqlException ex)
            {
                throw ex;
            }

            if (dt.Rows.Count == 1)
            {
                try
                {
                    this.CardID = Convert.ToInt32(dt.Rows[0]["CardID"]);
                    this.UserID = Convert.ToInt32(dt.Rows[0]["UserID"]);
                    this.CardNo = Convert.ToString(dt.Rows[0]["CardNo"]);
                    this.Pay2goToken = Convert.ToString(dt.Rows[0]["Pay2goToken"]);
                    this.Pay2goTokenExpire = Convert.ToString(dt.Rows[0]["Pay2goTokenExpire"]);
                    this.CheckStatus = Convert.ToInt32(dt.Rows[0]["CheckStatus"]);
                    this.CreateTime = Convert.ToDateTime(dt.Rows[0]["CreateTime"]);
                    this.UpdTime = Convert.ToDateTime(dt.Rows[0]["UpdTime"]);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
            {
                this.CardID = 0;
                this.UserID = 0;
            }
        }
        public int GetINSCardID()
        {
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            DataTable dt;
            sb.Append("SELECT LAST_INSERT_ID()");
            try
            {
                dt = this._conn.executeSelectQuery(sb.ToString(), parms.ToArray());

                return Convert.ToInt32(dt.Rows[0][0]);

            }
            catch (MySqlException ex)
            {
                throw ex;
            }

        }
        public int CardID { get; set; }
        public int UserID { get; set; }
        public string CardNo { get; set; }
        public string Pay2goToken { get; set; }
        public string Pay2goTokenExpire { get; set; }
        public int CheckStatus { get; set; }
        public DateTime CreateTime { get; set; }
        public DateTime UpdTime { get; set; }
    }

    public class dmInvoice : DataModel
    {
        public dmInvoice(dbConnectionMySQL Conn) : base(Conn) { }

        public override int Insert()
        {
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();


            sb.Append("INSERT INTO profile_invoice_info(UserID, InvoiceTitle, EIN, InvoiceAddress) ");
            sb.Append("VALUES(@UserID, @InvoiceTitle,@EIN  ,@InvoiceAddress ) ");

            parms.Add(new MySqlParameter("@UserID", this.UserID));
            parms.Add(new MySqlParameter("@InvoiceTitle", this.InvoiceTitle));
            parms.Add(new MySqlParameter("@EIN", this.EIN));
            parms.Add(new MySqlParameter("@InvoiceAddress", this.InvoiceAddress));

            try
            {
                return this._conn.executeInsertQuery(sb.ToString(), parms.ToArray());
            }
            catch (MySqlException ex)
            {
                throw ex;
            }
        }
        public override int Delete()
        {
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();

            sb.Append("Delete From profile_invoice_info ");
            sb.Append("Where  SeqNo = @SeqNo ");

            parms.Add(new MySqlParameter("@SeqNo", this.SeqNo));

            try
            {
                return this._conn.executeDeleteQuery(sb.ToString(), parms.ToArray());
            }
            catch (MySqlException ex)
            {
                throw ex;
            }
        }
        public override void Select(int SeqNo)
        {
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            DataTable dt;


            sb.Append("SELECT SeqNo, UserID, InvoiceTitle, EIN, InvoiceAddress, CreateTime, UpdTime ");
            sb.Append("FROM profile_invoice_info ");
            sb.Append("Where  SeqNo = @SeqNo ");

            parms.Add(new MySqlParameter("@SeqNo", SeqNo));

            try
            {
                dt = this._conn.executeSelectQuery(sb.ToString(), parms.ToArray());
            }
            catch (MySqlException ex)
            {
                throw ex;
            }

            if (dt.Rows.Count == 1)
            {
                try
                {
                    this.SeqNo = Convert.ToInt32(dt.Rows[0]["SeqNo"]);
                    this.UserID = Convert.ToInt32(dt.Rows[0]["UserID"]);
                    this.EIN = Convert.ToString(dt.Rows[0]["EIN"]);
                    this.InvoiceAddress = Convert.ToString(dt.Rows[0]["InvoiceAddress"]);
                    this.InvoiceTitle = Convert.ToString(dt.Rows[0]["InvoiceTitle"]);
                    this.CreateTime = Convert.ToDateTime(dt.Rows[0]["CreateTime"]);
                    this.UpdTime = Convert.ToDateTime(dt.Rows[0]["UpdTime"]);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
            {
                this.SeqNo = 0;
                this.UserID = 0;
            }
        }
        public override void Select(string UserID)
        {
            int UID = Int32.Parse(UserID);
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            DataTable dt;


            sb.Append("SELECT SeqNo, UserID, InvoiceTitle, EIN, InvoiceAddress, CreateTime, UpdTime ");
            sb.Append("FROM profile_invoice_info ");
            sb.Append("Where  UserID = @UID ");

            parms.Add(new MySqlParameter("@UID", UID));

            try
            {
                dt = this._conn.executeSelectQuery(sb.ToString(), parms.ToArray());
            }
            catch (MySqlException ex)
            {
                throw ex;
            }

            if (dt.Rows.Count == 1)
            {
                try
                {
                    this.SeqNo = Convert.ToInt32(dt.Rows[0]["SeqNo"]);
                    this.UserID = Convert.ToInt32(dt.Rows[0]["UserID"]);
                    this.EIN = Convert.ToString(dt.Rows[0]["EIN"]);
                    this.InvoiceAddress = Convert.ToString(dt.Rows[0]["InvoiceAddress"]);
                    this.InvoiceTitle = Convert.ToString(dt.Rows[0]["InvoiceTitle"]);
                    this.CreateTime = Convert.ToDateTime(dt.Rows[0]["CreateTime"]);
                    this.UpdTime = Convert.ToDateTime(dt.Rows[0]["UpdTime"]);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
            {
                this.SeqNo = 0;
                this.UserID = 0;
            }
        }

        public int GetINSInvoiceID()
        {
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            DataTable dt;
            sb.Append("SELECT LAST_INSERT_ID()");
            try
            {
                dt = this._conn.executeSelectQuery(sb.ToString(), parms.ToArray());

                return Convert.ToInt32(dt.Rows[0][0]);

            }
            catch (MySqlException ex)
            {
                throw ex;
            }

        }
        public int SeqNo { get; set; }
        public int UserID { get; set; }
        public string InvoiceTitle { get; set; }
        public string EIN { get; set; }
        public string InvoiceAddress { get; set; }
        public DateTime CreateTime { get; set; }
        public DateTime UpdTime { get; set; }
    }
}
