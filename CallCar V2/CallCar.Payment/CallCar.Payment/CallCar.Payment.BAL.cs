﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CallCar.Data;
using System.Net;
using System.IO;
using System.Security.Cryptography;
using Newtonsoft.Json;
using CallCar.Tool;
using DbConn;
using CallCar.Account;

namespace CallCar.Payment.BAL
{
    public class BALPayment : CallCarBAL
    {
        public BALPayment(string ConnectionString) : base(ConnectionString) { }

        /// <summary>
        /// 利用Card SeqNo 取得信用卡資料
        /// </summary>
        /// <param name="CardID">信用卡序號</param>
        /// <returns>信用卡資料(OCard)</returns>
        public OCard GetCreditCard(int CardID)
        {
            OCard card = new OCard();
            string IDMD5;

            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._connectstring))
                {
                    dmCreditCard dm = new Payment.dmCreditCard(conn);
                    dm.Select(CardID);
                    if (dm.CardID == 0)
                        return null;

                    card.UID = dm.UserID;
                    IDMD5 = MD5Cryptography.Encrypt(dm.UserID.ToString());
                    AESCryptography aes = new AESCryptography(IDMD5.Substring(0, 16), IDMD5.Substring(16, 16));
                    card.CNo = aes.Decrypt(dm.CardNo);
                    card.Check = dm.CheckStatus;
                    card.Code = "";
                    card.d = dm.Pay2goTokenExpire;
                    card.Expire = "";
                    card.ID = dm.CardID;
                    card.t = dm.Pay2goToken;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return card;
        }

        /// <summary>
        /// 利用使用者ID 取得信用卡資料
        /// </summary>
        /// <param name="UserID">使用者序號</param>
        /// <returns>信用卡資料(OCard)</returns>
        public OCard GetCreditCard(string UserID)
        {
            OCard card = new OCard();
            string IDMD5;

            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._connectstring))
                {
                    dmCreditCard dm = new Payment.dmCreditCard(conn);
                    dm.Select(UserID);
                    if (dm.CardID == 0)
                    {
                        return null;
                    }
                    card.UID = dm.UserID;
                    IDMD5 = MD5Cryptography.Encrypt(dm.UserID.ToString());
                    AESCryptography aes = new AESCryptography(IDMD5.Substring(0, 16), IDMD5.Substring(16, 16));
                    card.CNo = aes.Decrypt(dm.CardNo);
                    card.Check = dm.CheckStatus;
                    card.Code = "";
                    card.d = dm.Pay2goTokenExpire;
                    card.Expire = "";
                    card.ID = dm.CardID;
                    card.t = dm.Pay2goToken;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return card;
        }

        /// <summary>
        /// 新增信用卡&信用卡PaytoGO授權
        /// </summary>
        /// <param name="UserID">使用者序號</param>
        /// <param name="CardNo">卡號</param>
        /// <param name="EDate">有效期限</param>
        /// <param name="ACode">授權碼</param>
        /// <param name="Pay2GoUrl">授權URL</param>
        /// <param name="MerchantID">PaytoGo商店代碼</param>
        /// <param name="Hash">PaytoGO加密參數</param>
        /// <param name="IV">PaytoGO加密參數</param>
        /// <returns>CardID</returns>
        public int CreateNewCreditCard(int UserID, string CardNo, string EDate, string ACode, string Pay2GoUrl, string MerchantID, string Hash, string IV)
        {
            BALAccount bala = new BALAccount(this._connectstring);
            OAccount acc = bala.GetUserAccount(UserID);
            string CreditOrderNo;
            BALSerialNum dSerial = new BALSerialNum(this._connectstring);
            try
            {
                OSerialNum serial = dSerial.GetSerialNumObject("CreditAuth");
                CreditOrderNo = DateTime.Now.ToString(serial.Prefix) + dSerial.GetSeqNoString(serial);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


            Pay2GoCardAuth auth = new Pay2GoCardAuth(Pay2GoUrl, MerchantID, Hash, IV);
            auth.SetAuthData(CreditOrderNo, 1, "卡片驗證，不執行請款", acc.Email, CardNo, EDate, ACode, acc.ID.ToString());
            if (auth.Authorize() != "Success")
                throw new Exception("信用卡驗證失敗");

            string CrypCardNo;
            string IDMD5;
            try
            {
                //利用UserID將卡號加密           
                IDMD5 = MD5Cryptography.Encrypt(UserID.ToString());
                AESCryptography aes = new AESCryptography(IDMD5.Substring(0, 16), IDMD5.Substring(16, 16));
                CrypCardNo = aes.Encrypt(CardNo);
            }
            catch (Exception ex)
            {
                throw new Exception("新增信用卡失敗" + Environment.NewLine + ex.StackTrace);
            }

            int CardID = 0;
            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._connectstring))
                {
                    dmCreditCard dmCard = new dmCreditCard(conn);
                    dmCard.CardID = CardID;
                    dmCard.CardNo = CrypCardNo;
                    dmCard.CheckStatus = 1;
                    dmCard.Pay2goToken = auth.GetToken();
                    dmCard.Pay2goTokenExpire = auth.GetTokenLife();
                    dmCard.UserID = UserID;
                    try
                    {
                        dmCard.Insert();
                        CardID = dmCard.GetINSCardID();

                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return CardID;

        }

        /// <summary>
        ///  取消信用卡
        /// </summary>
        /// <param name="UserID">使用者序號</param>
        /// <param name="CardID">信用卡序號</param>
        /// <returns>刪除成功/失敗</returns>
        public bool CancelCreditCard(int UserID, int CardID)
        {
            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._connectstring))
                {
                    dmCreditCard dm = new dmCreditCard(conn);
                    dm.Select(CardID);
                    if (dm.UserID != UserID)
                        return false;

                    if (dm.Delete() > 0)
                        return true;
                    else
                        return false;
                }
            }
            catch (MySql.Data.MySqlClient.MySqlException ex)
            {
                throw ex;
            }
        }


        public OInvoice GetInvoiceData(string UserID)
        {
            OInvoice invoice = new OInvoice();
            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._connectstring))
                {
                    dmInvoice dm = new dmInvoice(conn);
                    dm.Select(UserID);
                    if (dm.SeqNo == 0)
                        return null;
                    invoice.UserID = dm.UserID;
                    invoice.SeqNo = dm.SeqNo;
                    invoice.InvoiceTitle = dm.InvoiceTitle;
                    invoice.InvoiceAddress = dm.InvoiceAddress;
                    invoice.EIN = dm.EIN;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return invoice;
        }

        public OInvoice GetInvoiceData(int SeqNo)
        {
            OInvoice invoice = new OInvoice();
            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._connectstring))
                {
                    dmInvoice dm = new dmInvoice(conn);
                    dm.Select(SeqNo);
                    if (dm.SeqNo == 0)
                        return null;

                    invoice.UserID = dm.UserID;
                    invoice.SeqNo = dm.SeqNo;
                    invoice.InvoiceTitle = dm.InvoiceTitle;
                    invoice.InvoiceAddress = dm.InvoiceAddress;
                    invoice.EIN = dm.EIN;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return invoice;
        }

        /// <summary>
        ///  新增一筆發票資訊
        /// </summary>
        /// <param name="UserID">使用者序號</param>
        /// <param name="InvoiceTitle">發票Title</param>
        /// <param name="EIN">統一編號</param>
        /// <param name="InvoiceAddress">寄送地址</param>
        /// <returns></returns>
        public int CreateNewInvoiceInfo(int UserID, string InvoiceTitle, string EIN, string InvoiceAddress)
        {
            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._connectstring))
                {
                    dmInvoice dm = new dmInvoice(conn);
                    dm.EIN = EIN;
                    dm.InvoiceAddress = InvoiceAddress;
                    dm.InvoiceTitle = InvoiceTitle;
                    dm.UserID = UserID;
                    try
                    {
                        dm.Insert();
                        return dm.GetINSInvoiceID();

                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        ///  取消發票資料
        /// </summary>
        /// <param name="UserID">使用者序號</param>
        /// <param name="InvoiceID">發票序號</param>
        /// <returns>刪除成功/失敗</returns>
        public bool CancelInvoiceInfo(int UserID, int InvoiceID)
        {
            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._connectstring))
                {
                    dmInvoice dm = new dmInvoice(conn);
                    dm.Select(InvoiceID);
                    if (dm.UserID != UserID)
                        return false;

                    if (dm.Delete() > 0)
                        return true;
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


    }

    public class Pay2GoCardAuth
    {
        private string HashKey;
        private string HashIV;
        private string Pay2GoURL;
        private string MerchantID;

        public Pay2GoCardAuth(string ConnectString, string MerchantID, string Hash, string IV)
        {
            this.Pay2GoURL = ConnectString;
            this.MerchantID = MerchantID;
            this.HashKey = Hash;
            this.HashIV = IV;
        }

        public void SetAuthData(string AuthOrderNo, int Amt, string ProdDesc, string Email, string CardNo, string ExpMonth, string CVCode, string Term)
        {
            this.Amt = Amt;
            this.CardNo = CardNo;
            this.CVC = CVCode;
            this.Exp = ExpMonth.Substring(2, 2) + ExpMonth.Substring(0, 2);
            this.MerchantOrderNo = AuthOrderNo;
            this.PayerEmail = Email;
            this.ProdDesc = ProdDesc;
            this.TokenLife = this.Exp;
            this.TokenSwitch = "get";
            this.TokenTerm = Term;
            this.Version = "1.0";
            this.AuthToken = "";
        }
        public string Authorize()
        {
            WebRequest tRequest;
            tRequest = WebRequest.Create(Pay2GoURL);
            tRequest.Method = "post";
            tRequest.ContentType = " application/x-www-form-urlencoded;charset=UTF-8";

            int TimeStamp = (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
            string postData = "MerchantID_=" + this.MerchantID + "&PostData_=" + EncryptAES256("TimeStamp=" + TimeStamp + "&Version=" + this.Version + "&MerchantOrderNo=" + this.MerchantOrderNo + "&Amt=" + this.Amt.ToString() + "&ProdDesc=" + this.ProdDesc + "&PayerEmail=" + this.PayerEmail + "&CardNo=" + this.CardNo + "&Exp=" + this.Exp + "&CVC=" + CVC + "&TokenSwitch=" + this.TokenSwitch + "&TokenTerm=" + this.TokenTerm + "&TokenLife=" + TokenLife, HashKey, HashIV) + "&Pos_=JSON";
            string tempData = "MerchantID_=" + this.MerchantID + "&PostData_=" + "TimeStamp=" + TimeStamp + "&Version=" + this.Version + "&MerchantOrderNo=" + this.MerchantOrderNo + "&Amt=" + this.Amt.ToString() + "&ProdDesc=" + this.ProdDesc + "&PayerEmail=" + this.PayerEmail + "&CardNo=" + this.CardNo + "&Exp=" + this.Exp + "&CVC=" + CVC + "&TokenSwitch=" + this.TokenSwitch + "&TokenTerm=" + this.TokenTerm + "&Pos_=JSON"; ;
            //Console.WriteLine(postData);
            Byte[] byteArray = Encoding.UTF8.GetBytes(postData);
            tRequest.ContentLength = byteArray.Length;

            Stream dataStream = tRequest.GetRequestStream();
            dataStream.Write(byteArray, 0, byteArray.Length);
            dataStream.Close();

            WebResponse tResponse = tRequest.GetResponse();

            dataStream = tResponse.GetResponseStream();

            StreamReader tReader = new StreamReader(dataStream);

            String sResponseFromServer = tReader.ReadToEnd();


            tReader.Close();
            dataStream.Close();
            tResponse.Close();

            Pay2goResponse pr = JsonConvert.DeserializeObject<Pay2goResponse>(sResponseFromServer);

            if (pr.Status == "SUCCESS")
            {
                this.AuthToken = pr.Result.TokenValue;
                return "Success";
            }
            else
            {
                this.AuthToken = "";
                return pr.Status + "(" + tempData + ")";
            }
        }

        public string GetToken()
        {
            return this.AuthToken;
        }
        public string GetTokenLife()
        {
            return this.TokenLife;
        }
        #region Private
        private string EncryptAES256(string source, string sSecretKey, string iv)//加密
        {
            byte[] sourceBytes = AddPKCS7Padding(Encoding.UTF8.GetBytes(source),
            32);
            var aes = new RijndaelManaged();
            aes.Key = Encoding.UTF8.GetBytes(sSecretKey);
            aes.IV = Encoding.UTF8.GetBytes(iv);
            aes.Mode = CipherMode.CBC;
            aes.Padding = PaddingMode.None;
            ICryptoTransform transform = aes.CreateEncryptor();
            return ByteArrayToHex(transform.TransformFinalBlock(sourceBytes, 0,
            sourceBytes.Length)).ToLower();
        }
        private string DecryptAES256(string encryptData)//解密
        {
            string sSecretKey = "ax9hhIc6W3oOCEn0GzxzMW7zwKKczmNJ";
            string iv = "UW1bFeiChGrTlOU1";
            var encryptBytes = HexStringToByteArray(encryptData.ToUpper());
            var aes = new RijndaelManaged();
            aes.Key = Encoding.UTF8.GetBytes(sSecretKey);
            aes.IV = Encoding.UTF8.GetBytes(iv);
            aes.Mode = CipherMode.CBC;
            aes.Padding = PaddingMode.None;
            ICryptoTransform transform = aes.CreateDecryptor();
            return Encoding.UTF8.GetString(RemovePKCS7Padding(transform.TransformFinalBlock(encryptBytes, 0, encryptBytes.Length)));
        }
        private static byte[] AddPKCS7Padding(byte[] data, int iBlockSize)
        {
            int iLength = data.Length;
            byte cPadding = (byte)(iBlockSize - (iLength % iBlockSize));
            var output = new byte[iLength + cPadding];
            Buffer.BlockCopy(data, 0, output, 0, iLength);
            for (var i = iLength; i < output.Length; i++)
                output[i] = (byte)cPadding;
            return output;
        }
        private static byte[] RemovePKCS7Padding(byte[] data)
        {
            int iLength = data[data.Length - 1];
            var output = new byte[data.Length - iLength];
            Buffer.BlockCopy(data, 0, output, 0, output.Length);
            return output;
        }
        private static string ByteArrayToHex(byte[] barray)
        {
            char[] c = new char[barray.Length * 2];
            byte b;
            for (int i = 0; i < barray.Length; ++i)
            {
                b = ((byte)(barray[i] >> 4));
                c[i * 2] = (char)(b > 9 ? b + 0x37 : b + 0x30);
                b = ((byte)(barray[i] & 0xF));
                c[i * 2 + 1] = (char)(b > 9 ? b + 0x37 : b + 0x30);
            }
            return new string(c);
        }
        private static byte[] HexStringToByteArray(string hexString)
        {
            int hexStringLength = hexString.Length;
            byte[] b = new byte[hexStringLength / 2];
            for (int i = 0; i < hexStringLength; i += 2)
            {
                int topChar = (hexString[i] > 0x40 ? hexString[i] - 0x37 : hexString[i] - 0x30)
                << 4;
                int bottomChar = hexString[i + 1] > 0x40 ? hexString[i + 1] - 0x37 :
                hexString[i + 1] - 0x30;
                b[i / 2] = Convert.ToByte(topChar + bottomChar);
            }
            return b;
        }
        #endregion

        private string Version { get; set; }  //固定 1.0
        private string MerchantOrderNo { get; set; } //商店訂單編號
        private int Amt { get; set; }           //金額
        private string ProdDesc { get; set; }      //產品說明
        private string PayerEmail { get; set; }    //消費者Email
        private string CardNo { get; set; }        //卡號
        private string Exp { get; set; }           //有效年月
        private string CVC { get; set; }           //授權碼
        private string TokenSwitch { get; set; } //固定 get
        private string TokenTerm { get; set; }     //自訂
        private string TokenLife { get; set; } //設定與信用卡有效年月相同
        private string AuthToken { get; set; } //取得的 token

        public class Pay2goResponse
        {
            public Pay2goResponse() { }

            public string Status { get; set; }
            public string Message { get; set; }
            public Pay2goResponseResult Result { get; set; }


            public class Pay2goResponseResult
            {
                public Pay2goResponseResult() { }

                public string MerchantID { get; set; }
                public string Amt { get; set; }
                public string TradeNo { get; set; }
                public string MerchantOrderNo { get; set; }
                public string RespondCode { get; set; }
                public string Auth { get; set; }
                public string AuthDate { get; set; }
                public string AuthTime { get; set; }
                public string Card6No { get; set; }
                public string Card4No { get; set; }
                public string Exp { get; set; }
                public string ECI { get; set; }
                public string IP { get; set; }
                public string EscrowBank { get; set; }
                public string TokenLife { get; set; }
                public string TokenValue { get; set; }
                public string CheckCode { get; set; }
            }
        }
    }
}
