﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CallCar;
using DbConn;
using System.Data;
using MySql.Data.MySqlClient;
using CallCar.Data;

namespace CallCar.Market
{
    public class BALCoupon : CallCarBAL
    {
        public BALCoupon(string Connectionstring) : base(Connectionstring) { }

        public OCoupon GetCoupon(string CouponCode, string OrderDate, string TakeDate)
        {
            dmCoupon dm;
            using (dbConnectionMySQL conn = new dbConnectionMySQL(this._connectstring))
            {
                dm = new Market.dmCoupon(conn);
                dm.Select(CouponCode, OrderDate);
            }

            if (dm.SeqNo == 0)
            {
                OCoupon coupon = new OCoupon();
                coupon.CarFeeSub = dm.CarFeeSub;
                coupon.CouponCode = dm.CouponCode;
                coupon.EndDate = dm.EndDate;
                coupon.FixCarFee = dm.FixCarFee;
                coupon.Memo = dm.Memo;
                coupon.NightFeeSub = dm.NightFeeSub;
                coupon.StartDate = dm.StartDate;
                coupon.TakeDateS = dm.TakeDateS;
                coupon.TakeDateE = dm.TakeDateE;

                if (Int32.Parse(TakeDate) > Int32.Parse(coupon.TakeDateE) || Int32.Parse(TakeDate) < Int32.Parse(coupon.TakeDateS))
                    return null;
                else
                    return coupon;
            }
            else
                return null;
        }

        public OCoupon ChkCouponAvailable(string CouponCode, string OrderDate)
        {
            dmCoupon dm;
            using (dbConnectionMySQL conn = new dbConnectionMySQL(this._connectstring))
            {
                dm = new Market.dmCoupon(conn);
                dm.Select(CouponCode, OrderDate);
            }

            if (dm.SeqNo == 0)
            {
                OCoupon coupon = new OCoupon();
                coupon.CarFeeSub = dm.CarFeeSub;
                coupon.CouponCode = dm.CouponCode;
                coupon.EndDate = dm.EndDate;
                coupon.FixCarFee = dm.FixCarFee;
                coupon.Memo = dm.Memo;
                coupon.NightFeeSub = dm.NightFeeSub;
                coupon.StartDate = dm.StartDate;
                coupon.TakeDateS = dm.TakeDateS;
                coupon.TakeDateE = dm.TakeDateE;
                coupon.UseCnt = dm.UseCnt;
                coupon.UseLimit = dm.LimitCnt;


                return coupon;

            }
            else
                return null;
        }
        ///// <summary>
        /////  取得折扣碼折扣金額
        ///// </summary>
        ///// <param name="CouponCode">優惠碼</param>
        ///// <param name="OrderDate">預約日期</param>
        ///// <param name="TakeDate">乘車日期</param>
        ///// <param name="CarFee">目前車資</param>
        ///// <param name="Pcnt">乘車人數</param>
        ///// /// <param name="NightFee">夜加費用</param>
        ///// <param name="PCT">固定車資的起算客單價</param>
        ///// <returns>優惠金額</returns>
        //public int GetCouponDisocunt(string CarpoolFlag, string CouponCode, string OrderDate, string TakeDate, ref int CarFee, int Pcnt, int NightFee, int PCT)
        //{
        //    OCoupon coupon = GetCoupon(CouponCode, OrderDate, TakeDate);
        //    if (coupon == null)
        //        return 0;

        //    int PromotFee = 0;

        //    if (CarpoolFlag == "S")
        //    {
        //        if (coupon.CarFeeSub != -1)
        //        {
        //            if (coupon.CarFeeSub > 0)
        //            {
        //                //林氏璧專案 以人數折扣
        //                if (coupon.CouponCode == "LINSHIBI")
        //                    PromotFee += coupon.CarFeeSub * Pcnt;
        //                else
        //                    PromotFee += coupon.CarFeeSub;
        //            }
        //        }
        //        else if (coupon.FixCarFee != -1)
        //        {
        //            //使用固定車資時 車資以單人計算 併計算優惠金額
        //            CarFee = PCT * Pcnt;

        //            if (coupon.FixCarFee == 0)
        //                PromotFee += CarFee;
        //            else if (coupon.FixCarFee > 0)
        //            {
        //                int CarFeeSub = coupon.FixCarFee * Pcnt;
        //                int temp = CarFee - CarFeeSub;
        //                PromotFee += temp;
        //            }
        //        }

        //        if (coupon.NightFeeSub != -1)
        //        {
        //            if (coupon.NightFeeSub == 0)
        //                PromotFee += NightFee;
        //        }
        //        return PromotFee;

        //    }
        //    else
        //    {
        //        if (coupon.CarFeeSub != -1)
        //        {
        //            if (coupon.CarFeeSub > 0)
        //                PromotFee += coupon.CarFeeSub;
        //        }
        //        else if (coupon.FixCarFee != -1)
        //        {
        //            if (coupon.FixCarFee == 0)
        //                PromotFee += CarFee;
        //            else if (coupon.FixCarFee > 0)
        //                PromotFee += coupon.FixCarFee;
        //        }

        //        if (coupon.NightFeeSub != -1)
        //        {
        //            if (coupon.NightFeeSub == 0)
        //                PromotFee += NightFee;
        //            else if (coupon.NightFeeSub > 0)
        //                PromotFee += coupon.NightFeeSub;
        //        }
        //        return PromotFee;
        //    }
        //}
    }

    public class dmCoupon : CallCar.DataModel
    {
        public dmCoupon(dbConnectionMySQL conn) : base(conn) { this.SeqNo = 0; this.CouponCode = ""; }

        public override void Select(string CouponCode, string OrderDate)
        {
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            DataTable dt;

            sb.Append("SELECT SeqNo, CouponCode,PromoteType, StartDate, EndDate, CarFeeSub, FixCarFee, NightFeeSub, Memo, UseCnt, LimitCnt, TakeDateS, TakeDateE, CreateTime, UpdTime ");
            sb.Append("FROM coupon_info Where CouponCode = @code and @date between StartDate and EndDate ");

            parms.Add(new MySqlParameter("@code", CouponCode));
            parms.Add(new MySqlParameter("@date", OrderDate));

            try
            {
                dt = this._conn.executeSelectQuery(sb.ToString(), parms.ToArray());
            }
            catch (MySqlException ex)
            {
                throw ex;
            }
            if (dt.Rows.Count > 0)
            {
                this.CouponCode = Convert.ToString(dt.Rows[0]["CouponCode"]);
                this.PromoteType = Convert.ToString(dt.Rows[0]["PromoteType"]);
                this.StartDate = Convert.ToString(dt.Rows[0]["StartDate"]);
                this.EndDate = Convert.ToString(dt.Rows[0]["EndDate"]);
                this.CarFeeSub = Convert.ToInt32(dt.Rows[0]["CarFeeSub"]);
                this.CreateTime = Convert.ToDateTime(dt.Rows[0]["CreateTime"]);
                this.EndDate = Convert.ToString(dt.Rows[0]["EndDate"]);
                this.FixCarFee = Convert.ToInt32(dt.Rows[0]["FixCarFee"]);
                this.LimitCnt = Convert.ToInt32(dt.Rows[0]["LimitCnt"]);
                this.Memo = Convert.ToString(dt.Rows[0]["Memo"]);
                this.NightFeeSub = Convert.ToInt32(dt.Rows[0]["NightFeeSub"]);
                this.SeqNo = Convert.ToInt32(dt.Rows[0]["SeqNo"]);
                this.TakeDateE = Convert.ToString(dt.Rows[0]["TakeDateE"]);
                this.TakeDateS = Convert.ToString(dt.Rows[0]["TakeDateS"]);
                this.UpdTime = Convert.ToDateTime(dt.Rows[0]["UpdTime"]);
                this.UseCnt = Convert.ToInt32(dt.Rows[0]["UseCnt"]);
            }
            else
            {
                this.SeqNo = 0;
                this.CouponCode = "";
            }

        }
        public int SeqNo { get; set; }
        public string PromoteType { get; set; }
        public string CouponCode { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public int CarFeeSub { get; set; }
        public int FixCarFee { get; set; }
        public int NightFeeSub { get; set; }
        public string Memo { get; set; }
        public int UseCnt { get; set; }
        public int LimitCnt { get; set; }
        public string TakeDateS { get; set; }
        public string TakeDateE { get; set; }
        public DateTime CreateTime { get; set; }
        public DateTime UpdTime { get; set; }
    }
}
