﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CallCar.ServicePlan;
using CallCar.Data;
using DbConn;
using MySql.Data.MySqlClient;
using System.Data;

namespace CallCar.Market
{
    public class BALPrice : CallCarBAL
    {
        public BALPrice(string Connectionstring) : base(Connectionstring) { }

        /// <summary>
        ///  試算共乘價格資料
        /// </summary>
        /// <param name="OrderDate">預約日期</param>
        /// <param name="TakeDate">乘車日期</param>
        /// <param name="TimeSegment">乘車時段</param>
        /// <param name="Pcnt">訂單人數</param>
        /// <param name="Bcnt">訂單行李數</param>
        /// <param name="Coupon">優惠代碼</param>
        /// <param name="BonusUseAmt">紅利點數使用數量</param>
        /// <param name="DepositUseAmt">代金使用數量</param>
        /// <param name="CarPcnt">該車目前人數(尚未媒合則為0)</param>
        /// <param name="addBag">行李臨加數量</param>
        /// <returns></returns>
        public OPrice GetSharePrice(string OrderDate, string TakeDate, string TimeSegment, int Pcnt, int Bcnt, string Coupon, int BonusUseAmt, int DepositUseAmt, int CarPcnt, int addBag, string ServeTime, int UserID)
        {
            try
            {
                int CarFee = 0, NightFee = 0, BagFee = 0, PromotFee = 0;
                List<OChargeItem> ChargeList = new List<OChargeItem>();
                OPrice price = new OPrice();

                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._connectstring))
                {
                    dmChargeBasicInfo dmCharge = new dmChargeBasicInfo(conn);

                    ChargeList = dmCharge.Select("SHARE", OrderDate);
                }


                if (ChargeList.Count > 0)
                {
                    int index = -1;
                    //取得車資 ChargeType = 1
                    index = ChargeList.FindIndex(x => x.ChargeType == "1" && x.MinQty == CarPcnt + Pcnt);
                    if (index == -1)
                        throw new Exception("無此服務費率表");
                    else
                        CarFee = ChargeList[index].Amount * Pcnt;

                    price.CarFee = CarFee;

                    //取得夜加  ChargeType = 2                    
                    if (ServeTime != null && ServeTime != "")
                    {
                        if ((Int32.Parse(TimeSegment) >= 2200 || Int32.Parse(TimeSegment) < 600) && (Int32.Parse(ServeTime) >= 2200 || Int32.Parse(ServeTime) < 600))
                        {
                            index = ChargeList.FindIndex(x => x.ChargeType == "2");
                            if (index == -1)
                                throw new Exception("無此服務費率表");
                            else
                                NightFee = ChargeList[index].Amount * Pcnt;
                        }
                        else
                            NightFee = 0;
                    }
                    else
                    {
                        if (Int32.Parse(TimeSegment) >= 2200 || Int32.Parse(TimeSegment) < 600)
                        {
                            index = ChargeList.FindIndex(x => x.ChargeType == "2");
                            if (index == -1)
                                throw new Exception("無此服務費率表");
                            else
                                NightFee = ChargeList[index].Amount * Pcnt;
                        }
                        else
                            NightFee = 0;
                    }
                    price.NightFee = NightFee;


                    //行李加費  ChargeType = 3
                    if (Bcnt - Pcnt > 0)
                    {
                        int BagCnt = Bcnt - Pcnt;
                        index = ChargeList.FindIndex(x => x.ChargeType == "3");
                        BagFee = ChargeList[index].Amount * BagCnt;
                    }
                    else
                        BagFee = 0;

                    if (addBag > 0)
                    {
                        index = ChargeList.FindIndex(x => x.ChargeType == "3");
                        BagFee += addBag * ChargeList[index].Amount;
                    }

                    price.BaggageFee = BagFee;

                    if (Coupon != "")
                    {
                        BALCoupon balc = new Market.BALCoupon(this._connectstring);
                        //PromotFee = balc.GetCouponDisocunt("S", Coupon, OrderDate, TakeDate, ref CarFee, Pcnt, NightFee, ShareBaseCarFee);

                        OCoupon coupon = balc.GetCoupon(Coupon, OrderDate, TakeDate);
                        if (coupon != null)
                        {
                            if (coupon.CouponType == "S")
                            {
                                if (coupon.CarFeeSub != -1)
                                {
                                    if (coupon.CarFeeSub > 0)
                                    {
                                        //林氏璧專案 以人數折扣
                                        if (coupon.CouponCode == "LINSHIBI")
                                            PromotFee += coupon.CarFeeSub * Pcnt;
                                        else
                                            PromotFee += coupon.CarFeeSub;
                                    }
                                }
                                //使用固定車資時 車資以單人計算 併計算優惠金額
                                if (coupon.FixCarFee != -1)
                                {


                                    if (coupon.FixCarFee == 0)
                                        PromotFee += CarFee;
                                    else if (coupon.FixCarFee > 0)
                                    {
                                        int CarFeeSub = coupon.FixCarFee * Pcnt;
                                        int temp = CarFee - CarFeeSub;
                                        PromotFee += temp;
                                    }
                                }

                                if (coupon.NightFeeSub != -1)
                                {
                                    if (coupon.NightFeeSub == 0)
                                        PromotFee += NightFee;
                                    else if (coupon.NightFeeSub > 0)
                                    {
                                        if (NightFee <= coupon.NightFeeSub)
                                            PromotFee += NightFee;
                                        else
                                            PromotFee += NightFee - coupon.NightFeeSub;
                                    }
                                }
                            }
                        }
                    }
                    else if (BonusUseAmt > 0)
                        PromotFee += BonusUseAmt;
                    else if (DepositUseAmt > 0)
                        PromotFee += DepositUseAmt;
                    else
                        PromotFee = 0;

                    price.PromoteAmt = PromotFee;

                    price.TotalAmt = price.CarFee + price.NightFee + price.BaggageFee + price.OtherFee + price.OtherSaleFee; //原始總金額 = 各項費用加總
                    price.Price = price.TotalAmt - price.PromoteAmt; //自付金額 = 原始總金額 - 優惠金額
                    price.OfficialTotalAmt = price.CollectAmt + price.Price; //實收總金額 = 自付金額+月結金額



                }
                else
                {
                    price = null;
                }
                return price;
            }
            catch (Exception ex)
            { throw ex; }
        }

        /// <summary>
        /// 試算專接價格
        /// </summary>
        /// <param name="OrderDate"></param>
        /// <param name="TakeDate"></param>
        /// <param name="FlightTime"></param>
        /// <param name="CityName"></param>
        /// <param name="DistinctName"></param>
        /// <param name="Airport"></param>
        /// <param name="CarType"></param>
        /// <param name="Coupon"></param>
        /// <param name="BonusUseAmt"></param>
        /// <param name="DepositUseAmt"></param>
        /// <returns></returns>
        public OPrice GetVIPPrice(string OrderDate, string TakeDate, string FlightTime, string CityName, string DistinctName, string Airport, string CarType, string Coupon, int BonusUseAmt, int DepositUseAmt)
        {
            try
            {
                int CarFee = 0, NightFee = 0, PromotFee = 0;
                dmProServicePlan dmService;
                OPrice price = new OPrice();

                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._connectstring))
                {
                    dmService = new dmProServicePlan(conn);

                    dmService.Select(CityName, DistinctName);
                }


                if (dmService.AreaID != "")
                {
                    //取得車資
                    string ChargeCode = "";
                    if (Airport == "TPE")
                    {
                        if (CarType == "四人座")
                            ChargeCode = dmService.TPESalePrice4;
                        else
                            ChargeCode = dmService.TPESalePrice7;
                    }
                    else if (Airport == "RMQ")
                    {
                        if (CarType == "四人座")
                            ChargeCode = dmService.RMQSalePrice4;
                        else
                            ChargeCode = dmService.RMQSalePrice7;
                    }
                    else if (Airport == "KHH")
                    {
                        if (CarType == "四人座")
                            ChargeCode = dmService.KHHSalePrice4;
                        else
                            ChargeCode = dmService.KHHSalePrice7;
                    }
                    List<OChargeItem> ChargeList;
                    List<OChargeItem> ChargeListVIP;
                    using (dbConnectionMySQL conn = new dbConnectionMySQL(this._connectstring))
                    {
                        dmChargeBasicInfo dmChargeList = new dmChargeBasicInfo(conn);
                        ChargeList = dmChargeList.Select(ChargeCode);
                        ChargeListVIP = dmChargeList.Select("VIP", OrderDate);
                    }
                    int index = -1;
                    index = ChargeList.FindIndex(x => Int32.Parse(x.StartDate) <= Int32.Parse(OrderDate) && Int32.Parse(x.EndDate) >= Int32.Parse(OrderDate));
                    if (index >= 0)
                        CarFee = ChargeList[index].Amount;
                    else
                        throw new Exception("無此專接價格");

                    price.CarFee = CarFee;

                    //取得夜加  ChargeType = 2   
                    //專接用航班時間決定夜加                 
                    if (Int32.Parse(FlightTime) > 2200 || Int32.Parse(FlightTime) < 600)
                    {
                        index = ChargeListVIP.FindIndex(x => x.ChargeType == "2");
                        if (index == -1)
                            throw new Exception("無此服務費率表");
                        else
                            NightFee = ChargeListVIP[index].Amount;
                    }
                    else
                        NightFee = 0;

                    price.NightFee = NightFee;

                    if (Coupon != "")
                    {
                        BALCoupon balc = new Market.BALCoupon(this._connectstring);
                        OCoupon coupon = balc.GetCoupon(Coupon, OrderDate, TakeDate);
                        if (coupon != null)
                        {
                            if (coupon.CouponType == "P")
                            {
                                if (coupon.CarFeeSub != -1)
                                {
                                    if (coupon.CarFeeSub > 0)
                                    {
                                        if (price.CarFee <= coupon.CarFeeSub)
                                            PromotFee += price.CarFee;
                                        else
                                            PromotFee += coupon.CarFeeSub;
                                    }
                                }
                                //使用固定車資時 車資以單人計算 併計算優惠金額
                                if (coupon.FixCarFee != -1)
                                {
                                    if (coupon.FixCarFee == 0)
                                        PromotFee += CarFee;
                                    else if (coupon.FixCarFee > 0)
                                        PromotFee += price.CarFee - coupon.FixCarFee;
                                }

                                if (coupon.NightFeeSub != -1)
                                {
                                    if (coupon.NightFeeSub == 0)
                                        PromotFee += NightFee;
                                    else if (coupon.NightFeeSub > 0)
                                    {
                                        if (NightFee <= coupon.NightFeeSub)
                                            PromotFee += NightFee;
                                        else
                                            PromotFee += coupon.NightFeeSub;
                                    }
                                }
                            }
                        }
                    }


                    price.PromoteAmt = PromotFee;


                    price.TotalAmt = price.CarFee + price.NightFee + price.BaggageFee + price.OtherFee + price.OtherSaleFee; //原始總金額 = 各項費用加總
                    price.Price = price.TotalAmt - price.PromoteAmt; //自付金額 = 原始總金額 - 優惠金額
                    price.OfficialTotalAmt = price.CollectAmt + price.Price; //實收總金額 = 自付金額+月結金額

                }
                else
                    price = null;

                return price;
            }
            catch (Exception ex)
            { throw ex; }
        }
    }
}