﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DbConn;
using MySql.Data.MySqlClient;
using System.Data;
using CallCar.Data;

namespace CallCar.ServicePlan
{
    public class BALServicePlan : CallCarBAL
    {
        public BALServicePlan(string ConnectionString) : base(ConnectionString) { }

        public bool VarifyPosProServiceStatus(string City, string Distinct, string Airport)
        {
            dmProServicePlan plan;
            using (dbConnectionMySQL conn = new dbConnectionMySQL(this._connectstring))
            {
                plan = new dmProServicePlan(conn);

                plan.Select(City, Distinct);
            }
            bool ServiceRemark = false;
            switch (Airport)
            {
                case "TPE":
                    if (plan.TPEServiceStatus == "1")
                        ServiceRemark = true;
                    break;
                case "RMQ":
                    if (plan.RMQServiceStatus == "1")
                        ServiceRemark = true;
                    break;
                case "KHH":
                    if (plan.KHHServiceStatus == "1")
                        ServiceRemark = true;
                    break;
            }
            return ServiceRemark;
        }

        public bool VarifyPosShareServiceStatus(string CityName, string DistinctName, string VillageName)
        {
            dmShareServicePlan dm;
            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._connectstring))
                {
                    dm = new dmShareServicePlan(conn);
                    dm.Select(CityName, DistinctName, VillageName);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            if (dm.DistinctID == 0 || dm.ServiceStatus == "X")
                return false;
            else
                return true;
        }
    }

    public class dmProServicePlan : DataModel
    {
        public dmProServicePlan(dbConnectionMySQL Conn) : base(Conn) { this.AreaID = ""; }

        public override void Select(string City, string Distinct)
        {
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            DataTable dt;

            sb.Append("SELECT AreaID, CityName,DistinctName,TPEServiceStatus, TPEPrice4, TPEPrice7, TPESalePrice4, TPESalePrice7, RMQServiceStatus, RMQPrice4, RMQPrice7, RMQSalePrice4, RMQSalePrice7, KHHServiceStatus, KHHPrice4, KHHPrice7, KHHSalePrice4, KHHSalePrice7,CreateTime,UpdTime ");
            sb.Append("FROM pro_service_plan where CityName = @city and DistinctName = @distinct  ");

            parms.Add(new MySqlParameter("@city", City));
            parms.Add(new MySqlParameter("@distinct", Distinct));

            try
            {
                dt = this._conn.executeSelectQuery(sb.ToString(), parms.ToArray());
            }
            catch (MySqlException ex)
            { throw ex; }

            if (dt.Rows.Count == 0)
            {
                this.AreaID = "";
            }
            else
            {
                AreaID = Convert.ToString(dt.Rows[0]["AreaID"]);
                CityName = Convert.ToString(dt.Rows[0]["CityName"]);
                DistinctName = Convert.ToString(dt.Rows[0]["DistinctName"]);
                TPEServiceStatus = Convert.ToString(dt.Rows[0]["TPEServiceStatus"]);
                TPEPrice4 = Convert.ToInt32(dt.Rows[0]["TPEPrice4"]);
                TPEPrice7 = Convert.ToInt32(dt.Rows[0]["TPEPrice7"]);
                TPESalePrice4 = Convert.ToString(dt.Rows[0]["TPESalePrice4"]);
                TPESalePrice7 = Convert.ToString(dt.Rows[0]["TPESalePrice7"]);
                RMQServiceStatus = Convert.ToString(dt.Rows[0]["RMQServiceStatus"]);
                RMQPrice4 = Convert.ToInt32(dt.Rows[0]["RMQPrice4"]);
                RMQPrice7 = Convert.ToInt32(dt.Rows[0]["RMQPrice7"]);
                RMQSalePrice4 = Convert.ToString(dt.Rows[0]["RMQSalePrice4"]);
                RMQSalePrice7 = Convert.ToString(dt.Rows[0]["RMQSalePrice7"]);
                KHHServiceStatus = Convert.ToString(dt.Rows[0]["KHHServiceStatus"]);
                KHHPrice4 = Convert.ToInt32(dt.Rows[0]["KHHPrice4"]);
                KHHPrice7 = Convert.ToInt32(dt.Rows[0]["KHHPrice7"]);
                KHHSalePrice4 = Convert.ToString(dt.Rows[0]["KHHSalePrice4"]);
                KHHSalePrice7 = Convert.ToString(dt.Rows[0]["KHHSalePrice7"]);
                CreateTime = Convert.ToDateTime(dt.Rows[0]["CreateTime"]);
                UpdTime = Convert.ToDateTime(dt.Rows[0]["UpdTime"]);
            }
        }

        public string AreaID { get; set; }
        public string CityName { get; set; }
        public string DistinctName { get; set; }
        public string TPEServiceStatus { get; set; }
        public int TPEPrice4 { get; set; }
        public int TPEPrice7 { get; set; }
        public string TPESalePrice4 { get; set; }
        public string TPESalePrice7 { get; set; }
        public string RMQServiceStatus { get; set; }
        public int RMQPrice4 { get; set; }
        public int RMQPrice7 { get; set; }
        public string RMQSalePrice4 { get; set; }
        public string RMQSalePrice7 { get; set; }
        public string KHHServiceStatus { get; set; }
        public int KHHPrice4 { get; set; }
        public int KHHPrice7 { get; set; }
        public string KHHSalePrice4 { get; set; }
        public string KHHSalePrice7 { get; set; }
        public DateTime CreateTime { get; set; }
        public DateTime UpdTime { get; set; }
    }

    public class dmShareServicePlan : DataModel
    {
        public dmShareServicePlan(dbConnectionMySQL Conn) : base(Conn) { }

        public override void Select(string CityName, string DistinctName, string VillageName)
        {
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            DataTable dt;

            sb.Append("select c.DistinctID,c.CityName,c.DistinctName,r.VillageName,r.ServiceStatus ");
            sb.Append("from city_distinct_data c, service_range_data r ");
            sb.Append("where c.DistinctID = r.DistinctID and c.ServiceStatus in ('1','2') and c.ServiceStatus <> 'X' ");
            sb.Append("and c.CityName = @city and c.DistinctName = @distinct and r.VillageName = @village ");

            parms.Add(new MySqlParameter("@city", CityName));
            parms.Add(new MySqlParameter("@distinct", DistinctName));
            parms.Add(new MySqlParameter("@village", VillageName));
            try
            {
                dt = this._conn.executeSelectQuery(sb.ToString(), parms.ToArray());
            }
            catch (MySqlException ex)
            {
                throw ex;
            }

            if (dt.Rows.Count == 1)
            {
                this.DistinctID = Convert.ToInt32(dt.Rows[0]["DistinctID"]);
                this.CityName = Convert.ToString(dt.Rows[0]["CityName"]);
                this.DistinctName = Convert.ToString(dt.Rows[0]["DistinctName"]);
                this.VillageName = Convert.ToString(dt.Rows[0]["VillageName"]);
                this.ServiceStatus = Convert.ToString(dt.Rows[0]["ServiceStatus"]);
            }
            else
            {
                this.DistinctID = 0;
                this.CityName = "";
                this.DistinctName = "";
                this.VillageName = "";
                this.ServiceStatus = "X";
            }

        }

        public int DistinctID { get; set; }
        public string CityName { get; set; }
        public string DistinctName { get; set; }
        public string VillageName { get; set; }
        public string ServiceStatus { get; set; }
    }

    public class dmChargeBasicInfo : DataModel
    {
        public dmChargeBasicInfo(dbConnectionMySQL Conn) : base(Conn) { this.SeqNo = 0; this.ChargeCode = ""; }

        public new List<OChargeItem> Select(string FeeTableID, string OrderDate)
        {
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            DataTable dt;
            List<OChargeItem> ChargeList = new List<OChargeItem>();

            sb.Append("SELECT SeqNo, ChargeCode, StartDate, EndDate, ChargeName, FeeTableID, ChargeType, FlowType, MinQty, Currency, Amount, CreateTime, UpdTime ");
            sb.Append("FROM charge_item Where FeeTableID = @id and  @date between StartDate and EndDate ");

            parms.Add(new MySqlParameter("@id", FeeTableID));
            parms.Add(new MySqlParameter("@date", OrderDate));

            try
            {
                dt = this._conn.executeSelectQuery(sb.ToString(), parms.ToArray());
            }
            catch (MySqlException ex)
            {
                throw ex;
            }
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    OChargeItem item = new OChargeItem();
                    item.ChargeCode = Convert.ToString(dt.Rows[0]["ChargeCode"]);
                    item.StartDate = Convert.ToString(dt.Rows[0]["StartDate"]);
                    item.EndDate = Convert.ToString(dt.Rows[0]["EndDate"]);
                    item.ChargeName = Convert.ToString(dt.Rows[0]["ChargeName"]);
                    item.FeeTableID = Convert.ToString(dt.Rows[0]["FeeTableID"]);
                    item.ChargeType = Convert.ToString(dt.Rows[0]["ChargeType"]);
                    item.FlowType = Convert.ToString(dt.Rows[0]["FlowType"]);
                    item.MinQty = Convert.ToInt32(dt.Rows[0]["MinQty"]);
                    item.Currency = Convert.ToString(dt.Rows[0]["Currency"]);
                    item.Amount = Convert.ToInt32(dt.Rows[0]["Amount"]);
                    ChargeList.Add(item);
                }
            }
            return ChargeList;
        }

        public new List<OChargeItem> Select(string ChargeCode)
        {
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            DataTable dt;
            List<OChargeItem> ChargeList = new List<OChargeItem>();

            sb.Append("SELECT SeqNo, ChargeCode, StartDate, EndDate, ChargeName, FeeTableID, ChargeType, FlowType, MinQty, Currency, Amount, CreateTime, UpdTime ");
            sb.Append("FROM charge_item Where ChargeCode = @ChargeCode ");

            parms.Add(new MySqlParameter("@ChargeCode", ChargeCode));

            try
            {
                dt = this._conn.executeSelectQuery(sb.ToString(), parms.ToArray());
            }
            catch (MySqlException ex)
            {
                throw ex;
            }
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    OChargeItem item = new OChargeItem();
                    item.ChargeCode = Convert.ToString(dt.Rows[0]["ChargeCode"]);
                    item.StartDate = Convert.ToString(dt.Rows[0]["StartDate"]);
                    item.EndDate = Convert.ToString(dt.Rows[0]["EndDate"]);
                    item.ChargeName = Convert.ToString(dt.Rows[0]["ChargeName"]);
                    item.FeeTableID = Convert.ToString(dt.Rows[0]["FeeTableID"]);
                    item.ChargeType = Convert.ToString(dt.Rows[0]["ChargeType"]);
                    item.FlowType = Convert.ToString(dt.Rows[0]["FlowType"]);
                    item.MinQty = Convert.ToInt32(dt.Rows[0]["MinQty"]);
                    item.Currency = Convert.ToString(dt.Rows[0]["Currency"]);
                    item.Amount = Convert.ToInt32(dt.Rows[0]["Amount"]);
                    ChargeList.Add(item);
                }
            }
            return ChargeList;
        }
        public int SeqNo { get; set; }
        public string ChargeCode { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string ChargeName { get; set; }
        public string FeeTableID { get; set; }
        public string ChargeType { get; set; }
        public string FlowType { get; set; }
        public int MinQty { get; set; }
        public string Currency { get; set; }
        public int Amount { get; set; }
        public DateTime CreateTime { get; set; }
        public DateTime UpdTime { get; set; }
    }    
}
