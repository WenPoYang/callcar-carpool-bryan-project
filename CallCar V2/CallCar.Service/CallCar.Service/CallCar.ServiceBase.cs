﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DbConn;
using MySql.Data.MySqlClient;
using System.Data;

namespace CallCar.ServiceBase
{
    public class BALServiceBase : CallCarBAL
    {
        public BALServiceBase(string ConnectionString) : base(ConnectionString) { }

        public bool ChkMatchDate(string date)
        {
            dmMatchData dm;
            using (dbConnectionMySQL conn = new dbConnectionMySQL(this._connectstring))
            {
                dm = new dmMatchData(conn);
                dm.Select(date);
            }

            if (dm.SeqNo > 0)
                return true;
            else
                return false;
        }

        private class dmMatchData : DataModel
        {
            public dmMatchData(dbConnectionMySQL Conn) : base(Conn) { this.SeqNo = 0; }

            public override void Select(string MatchDate)
            {
                StringBuilder sb = new StringBuilder();
                List<MySqlParameter> parms = new List<MySqlParameter>();
                sb.Append("select SeqNo, MatchDate, DispatchCnt From match_record Where MatchDate = @date");
                parms.Add(new MySqlParameter("@date", MatchDate));
                DataTable dt;
                try
                {
                    dt = this._conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                }
                catch (MySqlException ex)
                {
                    throw ex;
                }

                this.SeqNo = Convert.ToInt32(dt.Rows[0]["SeqNo"]);
                this.MatchDate = Convert.ToString(dt.Rows[0]["MatchDate"]);
                this.DispatchCnt = Convert.ToInt32(dt.Rows[0]["DispatchCnt"]);
            }

            public int SeqNo { get; set; }
            public string MatchDate { get; set; }
            public int DispatchCnt { get; set; }
        }
    }

}
