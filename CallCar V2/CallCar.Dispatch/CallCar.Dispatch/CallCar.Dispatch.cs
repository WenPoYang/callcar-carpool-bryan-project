﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CallCar;
using DbConn;
using MySql.Data.MySqlClient;
using System.Data;

namespace CallCar.Dispatch
{
    public class dmDispatch : DataModel
    {
        public dmDispatch(dbConnectionMySQL Conn) : base(Conn) { this.DispatchNo = ""; }

        public override int Insert()
        {
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            sb.Append("INSERT INTO dispatch_sheet ");
            sb.Append("(DispatchNo, ServiceType, ServeDate, TimeSegment,CarpoolFlag,CanPick, DispatchTime, PassengerCnt, BaggageCnt, StopCnt, MaxFlag, FleetID, CarType, CarNo, DriverID, ServiceRemark, UpdFlag)  ");
            sb.Append("VALUES(@DispatchNo ,@ServiceType ,@ServeDate ,@TimeSegment ,@CarpoolFlag,@CanPick,@DispatchTime ,@PassengerCnt ,@BaggageCnt ,@StopCnt ,@MaxFlag ,@FleetID ,@CarType ,@CarNo ,@DriverID ,@ServiceRemark ,@UpdFlag) ");

            parms.Add(new MySqlParameter("@DispatchNo", this.DispatchNo));
            parms.Add(new MySqlParameter("@ServiceType", this.ServiceType));
            parms.Add(new MySqlParameter("@ServeDate", this.ServeDate));
            parms.Add(new MySqlParameter("@TimeSegment", this.TimeSegment));
            parms.Add(new MySqlParameter("@CarpoolFlag", this.CarpoolFlag));
            parms.Add(new MySqlParameter("@CanPick", this.CanPick));
            parms.Add(new MySqlParameter("@DispatchTime", this.DispatchTime));
            parms.Add(new MySqlParameter("@PassengerCnt", this.PassengerCnt));
            parms.Add(new MySqlParameter("@BaggageCnt", this.BaggageCnt));
            parms.Add(new MySqlParameter("@StopCnt", this.StopCnt));
            parms.Add(new MySqlParameter("@MaxFlag", this.MaxFlag));
            parms.Add(new MySqlParameter("@FleetID", this.FleetID));
            parms.Add(new MySqlParameter("@CarType", this.CarType));
            parms.Add(new MySqlParameter("@CarNo", this.CarNo));
            parms.Add(new MySqlParameter("@DriverID", this.DriverID));
            parms.Add(new MySqlParameter("@ServiceRemark", this.ServiceRemark));
            parms.Add(new MySqlParameter("@UpdFlag", this.UpdFlag));

            try
            {
                return this._conn.executeInsertQuery(sb.ToString(), parms.ToArray());
            }
            catch (MySqlException ex)
            {
                throw ex;
            }
        }

        public override int Delete()
        {
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            sb.Append("DELETE FROM dispatch_sheet WHERE DispatchNo=@DispatchNo ");
            parms.Add(new MySqlParameter("@DispatchNo", this.DispatchNo));
            try
            {
                return this._conn.executeDeleteQuery(sb.ToString(), parms.ToArray());
            }
            catch (MySqlException ex)
            {
                throw ex;
            }
        }

        public override void Select(string DispatchNo)
        {
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            DataTable dt;
            sb.Append("SELECT DispatchNo, ServiceType, ServeDate, TimeSegment,CarpoolFlag,CanPick, DispatchTime, PassengerCnt, BaggageCnt, StopCnt, MaxFlag, FleetID, CarType, CarNo, DriverID, ServiceRemark, UpdFlag, CreateTime, UpdTime ");
            sb.Append("FROM dispatch_sheet where DispatchNo = @DispatchNo ");
            parms.Add(new MySqlParameter("@DispatchNo", DispatchNo));

            try
            {
                dt = this._conn.executeSelectQuery(sb.ToString(), parms.ToArray());
            }
            catch (MySqlException ex)
            {
                throw ex;
            }
            try
            {
                if (dt.Rows.Count == 1)
                {
                    this.DispatchNo = Convert.ToString(dt.Rows[0]["DispatchNo"]);
                    this.ServiceType = Convert.ToString(dt.Rows[0]["ServiceType"]);
                    this.ServeDate = Convert.ToString(dt.Rows[0]["ServeDate"]);
                    this.TimeSegment = Convert.ToString(dt.Rows[0]["TimeSegment"]);
                    this.CarpoolFlag = Convert.ToString(dt.Rows[0]["CarpoolFlag"]);
                    this.CanPick = Convert.ToString(dt.Rows[0]["CanPick"]);
                    this.DispatchTime = Convert.ToString(dt.Rows[0]["DispatchTime"]);
                    this.PassengerCnt = Convert.ToInt32(dt.Rows[0]["PassengerCnt"]);
                    this.BaggageCnt = Convert.ToInt32(dt.Rows[0]["BaggageCnt"]);
                    this.StopCnt = Convert.ToInt32(dt.Rows[0]["StopCnt"]);
                    this.MaxFlag = Convert.ToString(dt.Rows[0]["MaxFlag"]);
                    this.FleetID = Convert.ToInt32(dt.Rows[0]["FleetID"]);
                    this.CarType = Convert.ToString(dt.Rows[0]["CarType"]);
                    this.CarNo = Convert.ToString(dt.Rows[0]["CarNo"]);
                    this.DriverID = Convert.ToInt32(dt.Rows[0]["DriverID"]);
                    this.ServiceRemark = Convert.ToString(dt.Rows[0]["ServiceRemark"]);
                    this.UpdFlag = Convert.ToString(dt.Rows[0]["UpdFlag"]);
                    this.CreateTime = Convert.ToDateTime(dt.Rows[0]["CreateTime"]);
                    this.UpdTime = Convert.ToDateTime(dt.Rows[0]["UpdTime"]);
                }
                else
                    this.DispatchNo = "";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public new List<string> Select(string TakeDate, string ServiceType)
        {
            List<string> List = new List<string>();
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            DataTable dt;
            sb.Append("SELECT DispatchNo, ServiceType, ServeDate, TimeSegment,CarpoolFlag,CanPick, DispatchTime, PassengerCnt, BaggageCnt, StopCnt, MaxFlag, FleetID, CarType, CarNo, DriverID, ServiceRemark, UpdFlag, CreateTime, UpdTime ");
            sb.Append("FROM dispatch_sheet where ServeDate = @date and ServiceType = @type ");
            parms.Add(new MySqlParameter("@date", TakeDate));
            parms.Add(new MySqlParameter("@type", ServiceType));

            try
            {
                dt = this._conn.executeSelectQuery(sb.ToString(), parms.ToArray());
            }
            catch (MySqlException ex)
            {
                throw ex;
            }
            try
            {
                if (dt.Rows.Count > 0)
                {
                    for(int i = 0;i<dt.Rows.Count;i++)
                        List.Add(Convert.ToString(dt.Rows[i]["DispatchNo"]));
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return List;
        }

        public override int Update()
        {
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            sb.Append("UPDATE dispatch_sheet SET ");
            sb.Append("DispatchNo = @DispatchNo , ServiceType = @ServiceType , ServeDate = @ServeDate , TimeSegment = @TimeSegment, CarpoolFlag = @CarpoolFlag, CanPick = @CanPick , DispatchTime = @DispatchTime , ");
            sb.Append("PassengerCnt = @PassengerCnt , BaggageCnt = @BaggageCnt , StopCnt = @StopCnt , MaxFlag = @MaxFlag, FleetID = @FleetID , CarType = @CarType , ");
            sb.Append("CarNo = @CarNo , DriverID = @DriverID , ServiceRemark = @ServiceRemark, UpdFlag = @UpdFlag ");
            sb.Append("WHERE DispatchNo = @DispatchNo ");

            parms.Add(new MySqlParameter("@DispatchNo", this.DispatchNo));
            parms.Add(new MySqlParameter("@ServiceType", this.ServiceType));
            parms.Add(new MySqlParameter("@ServeDate", this.ServeDate));
            parms.Add(new MySqlParameter("@TimeSegment", this.TimeSegment));
            parms.Add(new MySqlParameter("@CarpoolFlag", this.CarpoolFlag));
            parms.Add(new MySqlParameter("@CanPick", this.CanPick));
            parms.Add(new MySqlParameter("@DispatchTime", this.DispatchTime));
            parms.Add(new MySqlParameter("@PassengerCnt", this.PassengerCnt));
            parms.Add(new MySqlParameter("@BaggageCnt", this.BaggageCnt));
            parms.Add(new MySqlParameter("@StopCnt", this.StopCnt));
            parms.Add(new MySqlParameter("@MaxFlag", this.MaxFlag));
            parms.Add(new MySqlParameter("@FleetID", this.FleetID));
            parms.Add(new MySqlParameter("@CarType", this.CarType));
            parms.Add(new MySqlParameter("@CarNo", this.CarNo));
            parms.Add(new MySqlParameter("@DriverID", this.DriverID));
            parms.Add(new MySqlParameter("@ServiceRemark", this.ServiceRemark));
            parms.Add(new MySqlParameter("@UpdFlag", this.UpdFlag));

            try
            {
                return this._conn.executeUpdateQuery(sb.ToString(), parms.ToArray());
            }
            catch (MySqlException ex)
            {
                throw ex;
            }
        }

        public string DispatchNo { get; set; }
        public string ServiceType { get; set; }
        public string ServeDate { get; set; }
        public string TimeSegment { get; set; }
        public string DispatchTime { get; set; }
        public string CarpoolFlag { get; set; }
        public string CanPick { get; set; }
        public int PassengerCnt { get; set; }
        public int BaggageCnt { get; set; }
        public int StopCnt { get; set; }
        public string MaxFlag { get; set; }
        public int FleetID { get; set; }
        public string CarType { get; set; }
        public string CarNo { get; set; }
        public int DriverID { get; set; }
        public string ServiceRemark { get; set; }
        public string UpdFlag { get; set; }
        public DateTime CreateTime { get; set; }
        public DateTime UpdTime { get; set; }
    }

    public class dmDispatchReservation : DataModel
    {
        public dmDispatchReservation(dbConnectionMySQL Conn) : base(Conn) { }

        public override int Insert()
        {
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            sb.Append("INSERT INTO dispatch_reservation ");
            sb.Append("(DispatchNo, ReservationNo, ServeSeq, ScheduleServeTime, ActualServreTime, LocationGPS, AddBaggageCnt, ServiceRemark) ");
            sb.Append("VALUES(@DispatchNo, @ReservationNo, @ServeSeq, @ScheduleServeTime, @ActualServreTime, @LocationGPS, @AddBaggageCnt, @ServiceRemark) ");

            parms.Add(new MySqlParameter("@DispatchNo", this.DispatchNo));
            parms.Add(new MySqlParameter("@ReservationNo", this.ReservationNo));
            parms.Add(new MySqlParameter("@ServeSeq", this.ServeSeq));
            parms.Add(new MySqlParameter("@ScheduleServeTime", this.ScheduleServeTime));
            parms.Add(new MySqlParameter("@ActualServreTime", this.ActualServreTime));
            parms.Add(new MySqlParameter("@LocationGPS", "Point(" + this.LocationGPSLng.ToString() + " " + this.LocationGPSLat.ToString() + ")"));
            parms.Add(new MySqlParameter("@AddBaggageCnt", this.AddBaggageCnt));
            parms.Add(new MySqlParameter("@ServiceRemark", this.ServiceRemark));

            try
            {
                return this._conn.executeInsertQuery(sb.ToString(), parms.ToArray());
            }
            catch (MySqlException ex)
            {
                throw ex;
            }

        }

        public override int Delete()
        {
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            sb.Append("Delete From dispatch_reservation Where DispatchNo = @DispatchNo ");
            parms.Add(new MySqlParameter("@DispatchNo", this.DispatchNo));

            try
            {
                return this._conn.executeDeleteQuery(sb.ToString(), parms.ToArray());
            }
            catch (MySqlException ex)
            {
                throw ex;
            }
        }

        public override int Update()
        {
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            sb.Append("UPDATE dispatch_reservation SET ");
            sb.Append("ServeSeq = @ServeSeq, ScheduleServeTime = @ScheduleServeTime, ActualServreTime = @ActualServreTime, ");
            sb.Append("LocationGPS = @LocationGPS, AddBaggageCnt = @AddBaggageCnt, ServiceRemark = @ServiceRemark ");
            sb.Append("WHERE DispatchNo = @DispatchNo AND ReservationNo = @ReservationNo ");

            parms.Add(new MySqlParameter("@DispatchNo", this.DispatchNo));
            parms.Add(new MySqlParameter("@ReservationNo", this.ReservationNo));
            parms.Add(new MySqlParameter("@ServeSeq", this.ServeSeq));
            parms.Add(new MySqlParameter("@ScheduleServeTime", this.ScheduleServeTime));
            parms.Add(new MySqlParameter("@ActualServreTime", this.ActualServreTime));
            parms.Add(new MySqlParameter("@LocationGPS", "Point(" + this.LocationGPSLng.ToString() + " " + this.LocationGPSLat.ToString() + ")"));
            parms.Add(new MySqlParameter("@AddBaggageCnt", this.AddBaggageCnt));
            parms.Add(new MySqlParameter("@ServiceRemark", this.ServiceRemark));

            try
            {
                return this._conn.executeUpdateQuery(sb.ToString(), parms.ToArray());
            }
            catch (MySqlException ex)
            {
                throw ex;
            }
        }

        public override void Select(string DispatchNo, string ReservationNo)
        {
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            DataTable dt;
            sb.Append("SELECT DispatchNo, ReservationNo, ServeSeq, ScheduleServeTime, ActualServreTime, X(LocationGPS) as LocationGPSLng, Y(LocationGPS) as LocationGPSLat, AddBaggageCnt, ServiceRemark, CreateTime, UpdTime ");
            sb.Append("FROM dispatch_reservation where DispatchNo = @DispatchNo and  ReservationNo = @ReservationNo");
            parms.Add(new MySqlParameter("@DispatchNo", DispatchNo));
            parms.Add(new MySqlParameter("@ReservationNo", ReservationNo));

            try
            {
                dt = this._conn.executeSelectQuery(sb.ToString(), parms.ToArray());
            }
            catch (MySqlException ex)
            {
                throw ex;
            }
            try
            {
                this.DispatchNo = Convert.ToString(dt.Rows[0]["DispatchNo"]);
                this.ReservationNo = Convert.ToString(dt.Rows[0]["ReservationNo"]);
                this.ServeSeq = Convert.ToInt32(dt.Rows[0]["ServeSeq"]);
                this.ScheduleServeTime = Convert.ToDateTime(dt.Rows[0]["ScheduleServeTime"]);
                if (dt.Rows[0]["ActualServreTime"] != null && dt.Rows[0]["ActualServreTime"] != DBNull.Value)
                    this.ActualServreTime = Convert.ToDateTime(dt.Rows[0]["ActualServreTime"]);

                this.LocationGPSLng = Convert.ToDouble(dt.Rows[0]["LocationGPSLng"]);
                this.LocationGPSLat = Convert.ToDouble(dt.Rows[0]["LocationGPSLat"]);
                this.AddBaggageCnt = Convert.ToInt32(dt.Rows[0]["AddBaggageCnt"]);
                this.ServiceRemark = Convert.ToString(dt.Rows[0]["ServiceRemark"]);
                this.CreateTime = Convert.ToDateTime(dt.Rows[0]["CreateTime"]);
                this.UpdTime = Convert.ToDateTime(dt.Rows[0]["UpdTime"]);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public new List<ServiceUnit> Select(string DispatchNo)
        {
            List<ServiceUnit> List = new List<ServiceUnit>();

            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            DataTable dt;
            sb.Append("SELECT DispatchNo, ReservationNo, ServeSeq, ScheduleServeTime, ActualServreTime, X(LocationGPS) as LocationGPSLng, Y(LocationGPS) as LocationGPSLat, AddBaggageCnt, ServiceRemark, CreateTime, UpdTime ");
            sb.Append("FROM dispatch_reservation where DispatchNo = @DispatchNo order by ScheduleServeTime ASC ");
            parms.Add(new MySqlParameter("@DispatchNo", DispatchNo));
            parms.Add(new MySqlParameter("@ReservationNo", ReservationNo));

            try
            {
                dt = this._conn.executeSelectQuery(sb.ToString(), parms.ToArray());
            }
            catch (MySqlException ex)
            {
                throw ex;
            }
            try
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    ServiceUnit u = new ServiceUnit();
                    u.DispatchNo = Convert.ToString(dt.Rows[i]["DispatchNo"]);
                    u.ReservationNo = Convert.ToString(dt.Rows[i]["ReservationNo"]);
                    u.ServeSeq = Convert.ToInt32(dt.Rows[i]["ServeSeq"]);
                    u.ScheduleServeTime = Convert.ToDateTime(dt.Rows[i]["ScheduleServeTime"]);
                    if (dt.Rows[i]["ActualServreTime"] != null && dt.Rows[i]["ActualServreTime"] != DBNull.Value)
                        u.ActualServreTime = Convert.ToDateTime(dt.Rows[i]["ActualServreTime"]);

                    u.LocationGPSLng = Convert.ToDouble(dt.Rows[i]["LocationGPSLng"]);
                    u.LocationGPSLat = Convert.ToDouble(dt.Rows[i]["LocationGPSLat"]);
                    u.AddBaggageCnt = Convert.ToInt32(dt.Rows[i]["AddBaggageCnt"]);
                    u.ServiceRemark = Convert.ToString(dt.Rows[i]["ServiceRemark"]);
                    List.Add(u);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return List;
        }

        public string DispatchNo { get; set; }
        public string ReservationNo { get; set; }
        public int ServeSeq { get; set; }
        public DateTime ScheduleServeTime { get; set; }
        public DateTime ActualServreTime { get; set; }
        public double LocationGPSLng { get; set; }
        public double LocationGPSLat { get; set; }
        public int AddBaggageCnt { get; set; }
        public string ServiceRemark { get; set; }
        public DateTime CreateTime { get; set; }
        public DateTime UpdTime { get; set; }

        public class ServiceUnit
        {
            public string DispatchNo { get; set; }
            public string ReservationNo { get; set; }
            public int ServeSeq { get; set; }
            public DateTime ScheduleServeTime { get; set; }
            public DateTime ActualServreTime { get; set; }
            public double LocationGPSLng { get; set; }
            public double LocationGPSLat { get; set; }
            public int AddBaggageCnt { get; set; }
            public string ServiceRemark { get; set; }
        }
    }
}

