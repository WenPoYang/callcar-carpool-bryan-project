﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CallCar;
using DbConn;
using CallCar.Data;

namespace CallCar.Dispatch
{
    public class BALDispatch : CallCarBAL
    {
        public BALDispatch(string ConnectionString) : base(ConnectionString) { }

        public bool isCarFull(string DispatchNo)
        {
            dmDispatch dm;
            using (dbConnectionMySQL conn = new dbConnectionMySQL(this._connectstring))
            {
                dm = new Dispatch.dmDispatch(conn);
                dm.Select(DispatchNo);
            }

            if (dm.StopCnt == 3)
                return true;

            if (dm.CarType == "四人座")
            {
                if (dm.PassengerCnt >= 3 || dm.BaggageCnt >= 3)
                    return true;
            }
            else if (dm.CarType == "七人座")
            {
                if (dm.PassengerCnt >= 5 || dm.BaggageCnt >= 7)
                    return true;
            }

            return false;
        }

        public List<dmDispatchReservation.ServiceUnit> GetAllDispatchUnit(string DispatchNo)
        {
            using (dbConnectionMySQL conn = new dbConnectionMySQL(this._connectstring))
            {
                dmDispatchReservation dm = new Dispatch.dmDispatchReservation(conn);
                return dm.Select(DispatchNo);
            }
        }

        public ODispatchSheet GetDispatchInfo(string DispatchNo)
        {
            ODispatchSheet od = new ODispatchSheet();
            dmDispatch dm;
            using (dbConnectionMySQL conn = new dbConnectionMySQL(this._connectstring))
            {
                dm = new dmDispatch(conn);
                dm.Select(DispatchNo);
            }

            if (dm.DispatchNo != "")
            {
                od.DispatchNo = dm.DispatchNo;
                od.BaggageCnt = dm.BaggageCnt;
                od.CarNo = dm.CarNo;
                od.CarType = dm.CarType;
                od.DriverID = dm.DriverID;
                od.FleetID = dm.FleetID;
                od.PassengerCnt = dm.PassengerCnt;
                od.ServiceCnt = dm.StopCnt;
                od.ServiceRemark = dm.ServiceRemark;
                od.ServiceType = dm.ServiceType;
                od.TakeDate = dm.ServeDate;
                od.TimeSegment = dm.TimeSegment;
                od.UpdFlag = dm.UpdFlag;
                od.CarpoolFlag = dm.CarpoolFlag;
                od.CanPick = dm.CanPick;
            }

            return od;
        }

        public List<ODispatchSheet> GetDispatchInfo(string TakeDate, string ServiceType)
        {
            dmDispatch dm;
            List<string> DispatchNos;
            List<ODispatchSheet> sheet = new List<ODispatchSheet>();
            using (dbConnectionMySQL conn = new dbConnectionMySQL(this._connectstring))
            {
                dm = new dmDispatch(conn);
                DispatchNos = dm.Select(TakeDate, ServiceType);
            }

            if (DispatchNos.Count > 0)
            {
                foreach (string no in DispatchNos)
                {
                    sheet.Add(GetDispatchInfo(no));
                }
            }
            return sheet;
        }
    }
}
