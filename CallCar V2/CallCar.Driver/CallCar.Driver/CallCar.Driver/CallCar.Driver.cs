﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CallCar.Data;
using DbConn;
using MySql.Data.MySqlClient;
using System.Data;
using System.Transactions;

namespace CallCar.Driver
{
    public class DALDriverDispatch
    {
        private string _ConnectionString;
        public DALDriverDispatch(string ConnectionString)
        {
            this._ConnectionString = ConnectionString;
        }

        /// <summary>
        /// 司機帳密驗證
        /// </summary>
        /// <param name="Account">帳號</param>
        /// <param name="Password">密碼</param>
        /// <returns></returns>
        public bool ChkAccPWAPP(string Account, string Password)
        {
            int UserID;
            try
            {
                StringBuilder sb = new StringBuilder();
                DataTable dt = new DataTable();
                DateTime today = DateTime.Now;

                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    sb.Append("select UserID FROM user_info where CitizenID = @ID ");
                    List<MySqlParameter> parms = new List<MySqlParameter>();
                    parms.Add(new MySqlParameter("@ID", Account));

                    dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                }
                if (dt.Rows.Count == 1)
                    UserID = Convert.ToInt32(dt.Rows[0]["UserID"]);
                else
                    return false;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            if (Account.Substring(5, 5).Equals(Password))
                return true;
            else
                return false;
        }

        /// <summary>
        ///  回傳司機待服務清單
        /// </summary>
        /// <param name="DriverID"></param>
        /// <returns></returns>
        public List<ODriverDispatchSheet> GetDispatchSheetByDriver(int DriverID)
        {

            DateTime StartTime = DateTime.Now.AddHours(-12);
            //string StartDate = StartTime.ToString("yyyyMMdd");

            DateTime EndTime = DateTime.Now.AddHours(24);
            //string EndDate = EndTime.ToString("yyyyMMdd");
            //string Etime = EndTime.ToString("HH") + "00";

            try
            {

                #region
                List<ODriverDispatchSheet> List = new List<ODriverDispatchSheet>();

                List<MySqlParameter> parms = new List<MySqlParameter>();
                DataTable dt = new DataTable();
                parms.Add(new MySqlParameter("@id", DriverID));
                parms.Add(new MySqlParameter("@sdate", StartTime));
                parms.Add(new MySqlParameter("@edate", EndTime));
                try
                {
                    using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                    {
                        dt = conn.executeSelectQuery(GetDriverDispatchSQL("UnServed"), parms.ToArray());
                    }
                    if (dt.Rows.Count > 0)
                    {
                        string DispatchNo = "";

                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            if (DispatchNo != Convert.ToString(dt.Rows[i]["DispatchNo"]))
                            {
                                ODriverDispatchSheet sheet = new ODriverDispatchSheet();
                                sheet.BaggageCnt = Convert.ToInt32(dt.Rows[i]["BaggageCnt"]);
                                sheet.CarNo = Convert.ToString(dt.Rows[i]["CarNo"]);
                                sheet.CarType = Convert.ToString(dt.Rows[i]["CarType"]);
                                sheet.DispatchNo = Convert.ToString(dt.Rows[i]["DispatchNo"]);
                                sheet.DriverID = Convert.ToInt32(dt.Rows[i]["DriverID"]);
                                sheet.DriverName = Convert.ToString(dt.Rows[i]["UserName"]);
                                sheet.PassengerCnt = Convert.ToInt32(dt.Rows[i]["PassengerCnt"]);
                                sheet.ServiceCnt = Convert.ToInt32(dt.Rows[i]["StopCnt"]);
                                sheet.ServiceType = Convert.ToString(dt.Rows[i]["ServiceType"]);
                                sheet.TakeDate = Convert.ToString(dt.Rows[i]["ServeDate"]);
                                sheet.ServiceRemark = Convert.ToString(dt.Rows[i]["ServiceRemarkS"]);
                                sheet.UpdFlag = Convert.ToString(dt.Rows[i]["UpdFlag"]);
                                sheet.CarpoolFlag = Convert.ToString(dt.Rows[i]["CarpoolFlag"]);
                                sheet.CanPick = Convert.ToString(dt.Rows[i]["CanPick"]);

                                ODriverServiceUnit u = new ODriverServiceUnit();

                                u.BaggageCnt = Convert.ToInt32(dt.Rows[i]["BaggageCnt"]);

                                if (dt.Rows[i]["ActualServreTime"] != DBNull.Value)
                                    u.ActualTime = Convert.ToDateTime(dt.Rows[i]["ActualServreTime"]);
                                u.DispatchNo = Convert.ToString(dt.Rows[i]["DispatchNo"]);
                                u.FlightNo = Convert.ToString(dt.Rows[i]["FlightNo"]);
                                u.PassengerCnt = Convert.ToInt32(dt.Rows[i]["PassengerCntR"]);
                                u.BaggageCnt = Convert.ToInt32(dt.Rows[i]["BaggageCntR"]);
                                u.ReservationNo = Convert.ToString(dt.Rows[i]["ReservationNo"]);
                                u.ScheduleTime = Convert.ToDateTime(dt.Rows[i]["ScheduleServeTime"]);
                                u.ServiceGPS = new OSpatialGPS(Convert.ToDouble(dt.Rows[i]["lat"]), Convert.ToDouble(dt.Rows[i]["lng"]));
                                u.ServiceOrder = Convert.ToInt32(dt.Rows[i]["ServeSeq"]);
                                u.ServiceRemark = Convert.ToString(dt.Rows[i]["ServiceRemark"]);
                                u.AddBaggageCnt = Convert.ToInt32(dt.Rows[i]["AddBaggageCnt"]);

                                string ServiceType = Convert.ToString(dt.Rows[i]["ServiceType"]);
                                if (ServiceType == "I")
                                {
                                    u.MainAddress = Convert.ToString(dt.Rows[i]["TakeoffAddress"]);
                                    u.AirportAddress = Convert.ToString(dt.Rows[i]["PickupAddress"]);
                                }
                                else
                                {
                                    u.MainAddress = Convert.ToString(dt.Rows[i]["PickupAddress"]);
                                    u.AirportAddress = Convert.ToString(dt.Rows[i]["TakeoffAddress"]);
                                }
                                sheet.ServiceList.Add(u);

                                List.Add(sheet);
                            }
                            else
                            {
                                ODriverServiceUnit u = new ODriverServiceUnit();

                                if (dt.Rows[i]["ActualServreTime"] != DBNull.Value)
                                    u.ActualTime = Convert.ToDateTime(dt.Rows[i]["ActualServreTime"]);
                                u.DispatchNo = Convert.ToString(dt.Rows[i]["DispatchNo"]);
                                u.PassengerCnt = Convert.ToInt32(dt.Rows[i]["PassengerCntR"]);
                                u.BaggageCnt = Convert.ToInt32(dt.Rows[i]["BaggageCntR"]);
                                u.FlightNo = Convert.ToString(dt.Rows[i]["FlightNo"]);
                                u.ReservationNo = Convert.ToString(dt.Rows[i]["ReservationNo"]);
                                u.ScheduleTime = Convert.ToDateTime(dt.Rows[i]["ScheduleServeTime"]);
                                u.ServiceGPS = new OSpatialGPS(Convert.ToDouble(dt.Rows[i]["lat"]), Convert.ToDouble(dt.Rows[i]["lng"]));
                                u.ServiceOrder = Convert.ToInt32(dt.Rows[i]["ServeSeq"]);
                                u.ServiceRemark = Convert.ToString(dt.Rows[i]["ServiceRemark"]);
                                u.AddBaggageCnt = Convert.ToInt32(dt.Rows[i]["AddBaggageCnt"]);

                                string ServiceType = Convert.ToString(dt.Rows[i]["ServiceType"]);
                                if (ServiceType == "I")
                                {
                                    u.MainAddress = Convert.ToString(dt.Rows[i]["TakeoffAddress"]);
                                    u.AirportAddress = Convert.ToString(dt.Rows[i]["PickupAddress"]);
                                }
                                else
                                {
                                    u.MainAddress = Convert.ToString(dt.Rows[i]["PickupAddress"]);
                                    u.AirportAddress = Convert.ToString(dt.Rows[i]["TakeoffAddress"]);
                                }
                                List[List.Count - 1].ServiceList.Add(u);
                            }

                            DispatchNo = Convert.ToString(dt.Rows[i]["DispatchNo"]);

                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
                #endregion
                return List;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// 取得司機已完成(上傳)之清單
        /// </summary>
        /// <param name="DriverID"></param>
        /// <returns></returns>
        public List<ODriverDispatchSheet> GetServedDispatchSheetByDriver(int DriverID)
        {

            DateTime StartTime = DateTime.Now.AddHours(-12);
            DateTime EndTime = DateTime.Now.AddHours(24);


            try
            {
                List<ODriverDispatchSheet> List = new List<ODriverDispatchSheet>();


                List<MySqlParameter> parms = new List<MySqlParameter>();
                DataTable dt = new DataTable();
                parms.Add(new MySqlParameter("@id", DriverID));
                parms.Add(new MySqlParameter("@sdate", StartTime));
                parms.Add(new MySqlParameter("@edate", EndTime));
                //parms.Add(new MySqlParameter("@stime", StartTime));
                //parms.Add(new MySqlParameter("@etime", EndTime));
                try
                {
                    using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                    {
                        dt = conn.executeSelectQuery(GetDriverDispatchSQL("Served"), parms.ToArray());
                    }
                    if (dt.Rows.Count > 0)
                    {
                        string DispatchNo = "";

                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            if (DispatchNo != Convert.ToString(dt.Rows[i]["DispatchNo"]))
                            {
                                ODriverDispatchSheet sheet = new ODriverDispatchSheet();
                                sheet.BaggageCnt = Convert.ToInt32(dt.Rows[i]["BaggageCnt"]);
                                sheet.CarNo = Convert.ToString(dt.Rows[i]["CarNo"]);
                                sheet.CarType = Convert.ToString(dt.Rows[i]["CarType"]);
                                sheet.DispatchNo = Convert.ToString(dt.Rows[i]["DispatchNo"]);
                                sheet.DriverID = Convert.ToInt32(dt.Rows[i]["DriverID"]);
                                sheet.DriverName = Convert.ToString(dt.Rows[i]["UserName"]);
                                sheet.PassengerCnt = Convert.ToInt32(dt.Rows[i]["PassengerCnt"]);
                                sheet.ServiceCnt = Convert.ToInt32(dt.Rows[i]["StopCnt"]);
                                sheet.ServiceType = Convert.ToString(dt.Rows[i]["ServiceType"]);
                                sheet.TakeDate = Convert.ToString(dt.Rows[i]["ServeDate"]);
                                sheet.ServiceRemark = Convert.ToString(dt.Rows[i]["ServiceRemarkS"]);
                                sheet.UpdFlag = Convert.ToString(dt.Rows[i]["UpdFlag"]);
                                sheet.CarpoolFlag = Convert.ToString(dt.Rows[i]["CarpoolFlag"]);
                                sheet.CanPick = Convert.ToString(dt.Rows[i]["CanPick"]);

                                ODriverServiceUnit u = new ODriverServiceUnit();

                                u.BaggageCnt = Convert.ToInt32(dt.Rows[i]["BaggageCnt"]);

                                if (dt.Rows[i]["ActualServreTime"] != DBNull.Value)
                                    u.ActualTime = Convert.ToDateTime(dt.Rows[i]["ActualServreTime"]);
                                u.DispatchNo = Convert.ToString(dt.Rows[i]["DispatchNo"]);
                                u.FlightNo = Convert.ToString(dt.Rows[i]["FlightNo"]);
                                u.PassengerCnt = Convert.ToInt32(dt.Rows[i]["PassengerCntR"]);
                                u.BaggageCnt = Convert.ToInt32(dt.Rows[i]["BaggageCntR"]);
                                u.ReservationNo = Convert.ToString(dt.Rows[i]["ReservationNo"]);
                                u.ScheduleTime = Convert.ToDateTime(dt.Rows[i]["ScheduleServeTime"]);
                                u.ServiceGPS = new OSpatialGPS(Convert.ToDouble(dt.Rows[i]["lat"]), Convert.ToDouble(dt.Rows[i]["lng"]));
                                u.ServiceOrder = Convert.ToInt32(dt.Rows[i]["ServeSeq"]);
                                u.ServiceRemark = Convert.ToString(dt.Rows[i]["ServiceRemark"]);
                                u.AddBaggageCnt = Convert.ToInt32(dt.Rows[i]["AddBaggageCnt"]);

                                string ServiceType = Convert.ToString(dt.Rows[i]["ServiceType"]);
                                if (ServiceType == "I")
                                {
                                    u.MainAddress = Convert.ToString(dt.Rows[i]["TakeoffAddress"]);
                                    u.AirportAddress = Convert.ToString(dt.Rows[i]["PickupAddress"]);
                                }
                                else
                                {
                                    u.MainAddress = Convert.ToString(dt.Rows[i]["PickupAddress"]);
                                    u.AirportAddress = Convert.ToString(dt.Rows[i]["TakeoffAddress"]);
                                }
                                sheet.ServiceList.Add(u);

                                List.Add(sheet);
                            }
                            else
                            {
                                ODriverServiceUnit u = new ODriverServiceUnit();

                                if (dt.Rows[i]["ActualServreTime"] != DBNull.Value)
                                    u.ActualTime = Convert.ToDateTime(dt.Rows[i]["ActualServreTime"]);
                                u.DispatchNo = Convert.ToString(dt.Rows[i]["DispatchNo"]);
                                u.PassengerCnt = Convert.ToInt32(dt.Rows[i]["PassengerCntR"]);
                                u.BaggageCnt = Convert.ToInt32(dt.Rows[i]["BaggageCntR"]);
                                u.FlightNo = Convert.ToString(dt.Rows[i]["FlightNo"]);
                                u.ReservationNo = Convert.ToString(dt.Rows[i]["ReservationNo"]);
                                u.ScheduleTime = Convert.ToDateTime(dt.Rows[i]["ScheduleServeTime"]);
                                u.ServiceGPS = new OSpatialGPS(Convert.ToDouble(dt.Rows[i]["lat"]), Convert.ToDouble(dt.Rows[i]["lng"]));
                                u.ServiceOrder = Convert.ToInt32(dt.Rows[i]["ServeSeq"]);
                                u.ServiceRemark = Convert.ToString(dt.Rows[i]["ServiceRemark"]);
                                u.AddBaggageCnt = Convert.ToInt32(dt.Rows[i]["AddBaggageCnt"]);

                                string ServiceType = Convert.ToString(dt.Rows[i]["ServiceType"]);
                                if (ServiceType == "I")
                                {
                                    u.MainAddress = Convert.ToString(dt.Rows[i]["TakeoffAddress"]);
                                    u.AirportAddress = Convert.ToString(dt.Rows[i]["PickupAddress"]);
                                }
                                else
                                {
                                    u.MainAddress = Convert.ToString(dt.Rows[i]["PickupAddress"]);
                                    u.AirportAddress = Convert.ToString(dt.Rows[i]["TakeoffAddress"]);
                                }
                                List[List.Count - 1].ServiceList.Add(u);
                            }

                            DispatchNo = Convert.ToString(dt.Rows[i]["DispatchNo"]);

                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }

                return List;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        /// <summary>
        /// 司機取得完整派車單
        /// </summary>
        /// <param name="Dno">派車單編號</param>
        /// <returns></returns>
        public ODriverDispatchSheet GetDriverDispatchSheet(string DispatchNo)
        {
            List<ODriverDispatchSheet> List = new List<ODriverDispatchSheet>();

            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            DataTable dt = new DataTable();
            sb.Append("select s.DispatchNo, s.ServiceType, s.ServeDate,s.TimeSegment, s.DispatchTime,s.PassengerCnt, s.BaggageCnt, s.StopCnt, s.CarType, s.CarNo, s.DriverID, s.FleetID, ");
            sb.Append("d.ReservationNo, d.ServeSeq, d.ScheduleServeTime, d.ActualServreTime, X(d.LocationGPS)as lng,Y(d.LocationGPS) as lat, d.ServiceRemark,d.AddBaggageCnt, ");
            sb.Append("r.FlightNo, r.PickupCity, r.PickupDistinct, r.PickupAddress, r.TakeoffCity, r.TakeoffDistinct, r.TakeoffAddress, r.PassengerCnt as PassengerCntR, r.BaggageCnt as BaggageCntR  ,s.ServiceRemark as ServiceRemarkS, ");
            sb.Append("s.CarpoolFlag, s.CanPick ");
            sb.Append("from dispatch_sheet s,dispatch_reservation d,reservation_sheet r ");
            sb.Append("where s.DispatchNo = d.DispatchNo and d.ReservationNo = r.ReservationNo ");
            sb.Append("and   s.DispatchNo = @no Order by d.ScheduleServeTime  ASC");
            parms.Add(new MySqlParameter("@no", DispatchNo));
            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                }
                if (dt.Rows.Count > 0)
                {
                    string Reservation = string.Empty;
                    ODriverDispatchSheet sheet = new ODriverDispatchSheet();
                    sheet.BaggageCnt = Convert.ToInt32(dt.Rows[0]["BaggageCnt"]);
                    sheet.CarNo = Convert.ToString(dt.Rows[0]["CarNo"]);
                    sheet.CarType = Convert.ToString(dt.Rows[0]["CarType"]);
                    sheet.DispatchNo = Convert.ToString(dt.Rows[0]["DispatchNo"]);
                    sheet.FleetID = Convert.ToInt32(dt.Rows[0]["FleetID"]);
                    sheet.DriverID = Convert.ToInt32(dt.Rows[0]["DriverID"]);
                    sheet.PassengerCnt = Convert.ToInt32(dt.Rows[0]["PassengerCnt"]);
                    sheet.ServiceCnt = Convert.ToInt32(dt.Rows[0]["StopCnt"]);
                    sheet.ServiceType = Convert.ToString(dt.Rows[0]["ServiceType"]);
                    sheet.TakeDate = Convert.ToString(dt.Rows[0]["ServeDate"]);
                    sheet.ServiceRemark = Convert.ToString(dt.Rows[0]["ServiceRemarkS"]);
                    sheet.TimeSegment = Convert.ToString(dt.Rows[0]["TimeSegment"]);
                    //sheet.FisrtServiceTime = Convert.ToString(dt.Rows[0]["DispatchTime"]);
                    sheet.CarpoolFlag = Convert.ToString(dt.Rows[0]["CarpoolFlag"]);
                    sheet.CanPick = Convert.ToString(dt.Rows[0]["CanPick"]);

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        ODriverServiceUnit u = new ODriverServiceUnit();

                        u.BaggageCnt = Convert.ToInt32(dt.Rows[i]["BaggageCntR"]);
                        if (dt.Rows[i]["ActualServreTime"] != DBNull.Value)
                            u.ActualTime = Convert.ToDateTime(dt.Rows[i]["ActualServreTime"]);
                        u.DispatchNo = Convert.ToString(dt.Rows[i]["DispatchNo"]);
                        u.FlightNo = Convert.ToString(dt.Rows[i]["FlightNo"]);
                        u.PassengerCnt = Convert.ToInt32(dt.Rows[i]["PassengerCntR"]);
                        u.ReservationNo = Convert.ToString(dt.Rows[i]["ReservationNo"]);
                        u.ScheduleTime = Convert.ToDateTime(dt.Rows[i]["ScheduleServeTime"]);
                        u.ServiceGPS = new OSpatialGPS(Convert.ToDouble(dt.Rows[i]["lat"]), Convert.ToDouble(dt.Rows[i]["lng"]));
                        u.ServiceOrder = Convert.ToInt32(dt.Rows[i]["ServeSeq"]);
                        u.ServiceRemark = Convert.ToString(dt.Rows[i]["ServiceRemark"]);
                        u.AddBaggageCnt = Convert.ToInt32(dt.Rows[i]["AddBaggageCnt"]);

                        if (sheet.ServiceType == "I")
                        {
                            u.MainCity = Convert.ToString(dt.Rows[i]["TakeoffCity"]);
                            u.MainDistinct = Convert.ToString(dt.Rows[i]["TakeoffDistinct"]);
                            u.MainAddress = Convert.ToString(dt.Rows[i]["TakeoffAddress"]);
                            u.AirportAddress = Convert.ToString(dt.Rows[i]["PickupAddress"]);
                        }
                        else
                        {
                            u.MainCity = Convert.ToString(dt.Rows[i]["PickupCity"]);
                            u.MainDistinct = Convert.ToString(dt.Rows[i]["PickupDistinct"]);
                            u.MainAddress = Convert.ToString(dt.Rows[i]["PickupAddress"]);
                            u.AirportAddress = Convert.ToString(dt.Rows[i]["TakeoffAddress"]);
                        }
                        sheet.ServiceList.Add(u);
                    }
                    return sheet;

                }
                else
                    return null;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        /// <summary>
        ///  司機更新派車單
        /// </summary>
        /// <param name="odr"></param>
        /// <returns></returns>
        public bool UpdateServiceStatusByDriver(ODriverServiceRecord odr)
        {

            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();

            using (TransactionScope scope = new TransactionScope())
            {
                try
                {
                    using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                    {
                        sb.Append("UPDATE dispatch_reservation SET ActualServreTime = @time,ServiceRemark = @remark , AddBaggageCnt = @AddBaggageCnt  WHERE DispatchNo = @dno and ReservationNo = @rno");
                        parms.Add(new MySqlParameter("@time", MySqlDbType.DateTime));
                        parms.Add(new MySqlParameter("@remark", MySqlDbType.String));
                        parms.Add(new MySqlParameter("@dno", MySqlDbType.VarChar));
                        parms.Add(new MySqlParameter("@rno", MySqlDbType.VarChar));
                        parms.Add(new MySqlParameter("@AddBaggageCnt", MySqlDbType.Int32));

                        if (!string.IsNullOrWhiteSpace(odr.Reservation1))
                        {
                            parms[0].Value = odr.ServiceTime1;
                            parms[1].Value = odr.ServiceRemark1;
                            parms[2].Value = odr.Dno;
                            parms[3].Value = odr.Reservation1;
                            parms[4].Value = odr.addBag1;
                            conn.executeUpdateQuery(sb.ToString(), parms.ToArray());
                        }

                        if (!string.IsNullOrWhiteSpace(odr.Reservation2))
                        {
                            parms[0].Value = odr.ServiceTime2;
                            parms[1].Value = odr.ServiceRemark2;
                            parms[2].Value = odr.Dno;
                            parms[3].Value = odr.Reservation2;
                            parms[4].Value = odr.addBag2;

                            conn.executeUpdateQuery(sb.ToString(), parms.ToArray());
                        }

                        if (!string.IsNullOrWhiteSpace(odr.Reservation3))
                        {
                            parms[0].Value = odr.ServiceTime3;
                            parms[1].Value = odr.ServiceRemark3;
                            parms[2].Value = odr.Dno;
                            parms[3].Value = odr.Reservation3;
                            parms[4].Value = odr.addBag3;

                            conn.executeUpdateQuery(sb.ToString(), parms.ToArray());
                        }

                        sb.Length = 0;
                        parms.Clear();

                        sb.Append("Update dispatch_sheet Set ServiceRemark = @new , UpdFlag = @flag Where DispatchNo = @dno and DriverID = @id");
                        parms.Add(new MySqlParameter("@new", odr.SheetServiceRemark));
                        parms.Add(new MySqlParameter("@flag", odr.UpdFlag));
                        parms.Add(new MySqlParameter("@dno", odr.Dno));
                        parms.Add(new MySqlParameter("@id", odr.DriverID));

                        conn.executeUpdateQuery(sb.ToString(), parms.ToArray());

                    }
                    scope.Complete();
                    return true;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
              
            }

        }

        /// <summary>
        ///  組成SQL
        /// </summary>
        /// <param name="Remark">UnServed / Served / Serving</param>
        /// <returns></returns>
        private string GetDriverDispatchSQL(string Remark)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("select s.DispatchNo, s.ServiceType, s.ServeDate, s.PassengerCnt, s.BaggageCnt, s.StopCnt, s.CarType, s.CarNo, s.DriverID, ");
            sb.Append("d.ReservationNo, d.ServeSeq, d.ScheduleServeTime, d.ActualServreTime, X(d.LocationGPS)as lng,Y(d.LocationGPS) as lat, d.ServiceRemark,d.AddBaggageCnt, ");
            sb.Append("u.UserName,r.FlightNo, r.PickupCity, r.PickupDistinct, r.PickupAddress, r.TakeoffCity, r.TakeoffDistinct, r.TakeoffAddress, r.PassengerCnt as PassengerCntR,   r.BaggageCnt as BaggageCntR,s.ServiceRemark as ServiceRemarkS,s.UpdFlag, ");
            sb.Append("s.CarpoolFlag, s.CanPick ");
            sb.Append("from dispatch_sheet s,dispatch_reservation d,user_info u,reservation_sheet r ");
            sb.Append("where s.DispatchNo = d.DispatchNo and d.ReservationNo = r.ReservationNo and s.DriverID = u.UserID ");
            sb.Append("and s.DriverID = @id and d.ScheduleServeTime  between @sdate and  @edate  ");
            sb.Append("and r.ProcessStage not in ('X','W','Y') ");

            if (Remark == "UnServed")
                sb.Append(" and  s.UpdFlag = '0'  ");
            else if (Remark == "Served")
                sb.Append(" and  s.UpdFlag = '1'  ");


            sb.Append("Order by s.ServeDate , s.DispatchNo,d.ScheduleServeTime ASC");

            return sb.ToString();
        }
    }


    public class ODriverDispatchSheet
    {
        public ODriverDispatchSheet()
        {
            this.ServiceList = new List<ODriverServiceUnit>();
        }

        public string DispatchNo { get; set; }
        public string TakeDate { get; set; }
        public string ServiceType { get; set; }
        public string TimeSegment { get; set; }
        public string CarpoolFlag { get; set; }
        public string CanPick { get; set; }
        public List<ODriverServiceUnit> ServiceList { get; set; }
        public int PassengerCnt { get; set; }
        public int BaggageCnt { get; set; }
        public int ServiceCnt { get; set; }
        public int FleetID { get; set; }
        public string FleetName { get; set; }
        public string CarType { get; set; }
        public string CarNo { get; set; }
        public int DriverID { get; set; }
        public string DriverName { get; set; }
        public string FisrtServiceTime { get; set; }
        public string ServiceRemark { get; set; }
        public string UpdFlag { get; set; }


    }

    public class ODriverServiceUnit
    {
        public string DispatchNo { get; set; }
        public string ReservationNo { get; set; }
        public int ServiceOrder { get; set; }
        public int PassengerCnt { get; set; }
        public int BaggageCnt { get; set; }
        public string FlightNo { get; set; }
        public string MainCity { get; set; }
        public string MainDistinct { get; set; }
        public string MainAddress { get; set; }
        public string AirportAddress { get; set; }
        public OSpatialGPS ServiceGPS { get; set; }
        public DateTime ScheduleTime { get; set; }
        public DateTime ActualTime { get; set; }
        public string ServiceRemark { get; set; }
        public int AddBaggageCnt { get; set; }


    }

    public class ODriverServiceRecord
    {
        public string Dno { get; set; }
        public int DriverID { get; set; }
        public string SheetServiceRemark { get; set; }
        public string UpdFlag { get; set; }
        public string Reservation1 { get; set; }
        public string ServiceRemark1 { get; set; }
        public DateTime ServiceTime1 { get; set; }
        public int addBag1 { get; set; }
        public string Reservation2 { get; set; }
        public string ServiceRemark2 { get; set; }
        public DateTime ServiceTime2 { get; set; }
        public int addBag2 { get; set; }
        public string Reservation3 { get; set; }
        public string ServiceRemark3 { get; set; }
        public DateTime ServiceTime3 { get; set; }
        public int addBag3 { get; set; }
    }

}
