﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CallCar;
using CallCar.Data;
using DbConn;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using CallCar.Tool;
using CallCar.Account;
using System.Transactions;


namespace CallCar.Reservation
{
    public class BALReservation : CallCarBAL
    {
        public BALReservation(string ConnectString) : base(ConnectString) { }

        /// <summary>
        ///  新增單一筆訂單 (不適用撿車模式)
        /// </summary>
        /// <param name="or">OReservation物件</param>
        /// <returns>訂單編號</returns>
        public string CreateNewReservation(OReservation or)
        {
            string ReservationNo = string.Empty;

            BALSerialNum dSerial = new BALSerialNum(this._connectstring);
            try
            {
                OSerialNum serial = dSerial.GetSerialNumObject("Reservation");
                ReservationNo = DateTime.Now.ToString(serial.Prefix) + dSerial.GetSeqNoString(serial);

            }
            catch (Exception ex)
            {
                throw ex;
            }

            using (dbConnectionMySQL conn = new dbConnectionMySQL(this._connectstring))
            {
                dmReservation o = new dmReservation(conn);

                o.BaggageCnt = or.BaggageCnt;
                o.SelectionType = or.ChooseType;
                o.Coupon = or.Coupon;
                o.DiscountType = or.DiscountType;
                o.DiscountAmt = or.DiscountAmt;
                o.CarpoolFlag = or.CarpoolFlag;
                o.PickCartype = or.PickCartype;

                if (o.SelectionType == "F")
                {
                    o.ScheduleFlightTime = or.FlightTime;
                    o.FlightNo = or.FlightNo;
                }

                o.CreditData = or.Credit;

                o.InvoiceData = or.Invoice;



                o.InvoiceNum = or.EIN;

                o.PassengerCnt = or.PassengerCnt;
                o.Ternimal = or.Ternimal;
                o.Airport = or.Airport;
                o.PickupCity = or.PickupCity;
                o.PickupDistinct = or.PickupDistinct;
                o.PickupAddress = or.PickupAddress;
                o.PickupVillage = or.PickupVillage;
                o.PickupLat = or.PickupGPS.GetLat();
                o.PickupLng = or.PickupGPS.GetLng();
                o.TimeSegment = or.PreferTime;
                o.ProcessStage = "0";
                o.TakeDate = or.ServeDate;
                o.ServiceType = or.ServiceType;
                o.TakeoffAddress = or.TakeoffAddress;
                o.TakeoffCity = or.TakeoffCity;
                o.TakeoffDistinct = or.TakeoffDistinct;
                o.TakeoffVillage = or.TakeoffVillage;
                o.TakeoffLng = or.TakeoffGPS.GetLng();
                o.TakeoffLat = or.TakeoffGPS.GetLat();
                o.UserID = or.UserID;
                o.MaxFlag = or.MaxFlag;
                o.Price = or.Price;
                o.PassengerName = or.PassengerName;
                o.PassengerPhone = or.PassengerPhone;

                o.ReservationNo = ReservationNo;

                try
                {
                    o.Insert();
                }
                catch (Exception ex)
                {
                    throw ex;
                }

                return ReservationNo;
            }
        }

        /// <summary>
        /// 新增單一筆訂單 (撿車模式)
        /// </summary>
        /// <param name="or">OReservation物件</param>
        /// <param name="DispatchNo">車趟編號</param>
        /// <param name="ServeTime">預約時間</param>
        /// <returns></returns>
        public string CreateNewReservation(OReservation or, string DispatchNo, DateTime ServiceTime)
        {
            BALAccount acc = new BALAccount(this._connectstring);
            OAccount oa = acc.GetUserAccount(or.UserID);
            string ReservationNo = string.Empty;

            //取訂單編號
            BALSerialNum dSerial = new BALSerialNum(this._connectstring);

            try
            {
                OSerialNum serial = dSerial.GetSerialNumObject("Reservation");
                ReservationNo = DateTime.Now.ToString(serial.Prefix) + dSerial.GetSeqNoString(serial);

            }
            catch (Exception ex)
            {
                throw ex;
            }

            //新增訂單檔
            List<MySqlParameter> parms = new List<MySqlParameter>();
            StringBuilder sb = new StringBuilder();
            sb.Append("INSERT INTO reservation_sheet  ");
            sb.Append("(ReservationNo,UserID, ServiceType, TakeDate, TimeSegment, SelectionType,PickCartype, FlightNo, ScheduleFlightTime,Airport,Ternimal,  ");
            sb.Append("PickupCity, PickupDistinct,PickupVillage, PickupAddress,PickupGPS, TakeoffCity, TakeoffDistinct,TakeoffVillage, TakeoffAddress, TakeoffGPS, PassengerCnt,   ");
            sb.Append("BaggageCnt,MaxFlag, Price, Coupon, InvoiceData,CreditData, ProcessStage,PassengerName, PassengerPhone)  ");
            sb.Append("VALUES (@ReservationNo,@UserID, @ServiceType, @TakeDate, @TimeSegment, @SelectionType, @PickCartype,@FlightNo, @ScheduleFlightTime,@Airport,@Ternimal,  ");
            sb.Append("@PickupCity, @PickupDistinct,@PickupVillage, @PickupAddress,GeomFromText(@PickupGPS), @TakeoffCity, @TakeoffDistinct, @TakeoffVillage, @TakeoffAddress, GeomFromText(@TakeoffGPS), @PassengerCnt,   ");
            sb.Append("@BaggageCnt, @MaxFlag,@Price, @Coupon, @InvoiceData,@CreditData, @ProcessStage,@PassengerName, @PassengerPhone)  ");

            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    using (dbConnectionMySQL conn = new dbConnectionMySQL(this._connectstring))
                    {
                        parms.Add(new MySqlParameter("@ReservationNo", ReservationNo));
                        parms.Add(new MySqlParameter("@UserID", or.UserID));
                        parms.Add(new MySqlParameter("@ServiceType", or.ServiceType));
                        parms.Add(new MySqlParameter("@TakeDate", or.ServeDate));
                        parms.Add(new MySqlParameter("@TimeSegment", or.PreferTime));
                        parms.Add(new MySqlParameter("@SelectionType", or.ChooseType));

                        if (or.ChooseType == "F")
                        {
                            parms.Add(new MySqlParameter("@FlightNo", or.FlightNo));
                            parms.Add(new MySqlParameter("@ScheduleFlightTime", or.FlightTime));
                        }
                        else
                        {
                            parms.Add(new MySqlParameter("@FlightNo", ""));
                            parms.Add(new MySqlParameter("@ScheduleFlightTime", null));
                        }
                        parms.Add(new MySqlParameter("@Airport", or.Airport));
                        parms.Add(new MySqlParameter("@Ternimal", or.Ternimal));
                        parms.Add(new MySqlParameter("@PickCartype", or.PickCartype));
                        parms.Add(new MySqlParameter("@PickupCity", or.PickupCity));
                        parms.Add(new MySqlParameter("@PickupDistinct", or.PickupDistinct));
                        parms.Add(new MySqlParameter("@PickupVillage", or.PickupVillage));
                        parms.Add(new MySqlParameter("@PickupAddress", or.PickupAddress));
                        parms.Add(new MySqlParameter("@PickupGPS", "Point(" + or.PickupGPS.GetLng().ToString() + " " + or.PickupGPS.GetLat().ToString() + ")"));
                        parms.Add(new MySqlParameter("@TakeoffCity", or.TakeoffCity));
                        parms.Add(new MySqlParameter("@TakeoffDistinct", or.TakeoffDistinct));
                        parms.Add(new MySqlParameter("@TakeoffVillage", or.TakeoffVillage));
                        parms.Add(new MySqlParameter("@TakeoffAddress", or.TakeoffAddress));
                        parms.Add(new MySqlParameter("@TakeoffGPS", "Point(" + or.TakeoffGPS.GetLng().ToString() + " " + or.TakeoffGPS.GetLat().ToString() + ")"));
                        parms.Add(new MySqlParameter("@PassengerCnt", or.PassengerCnt));
                        parms.Add(new MySqlParameter("@BaggageCnt", or.BaggageCnt));
                        parms.Add(new MySqlParameter("@MaxFlag", or.MaxFlag));
                        parms.Add(new MySqlParameter("@Coupon", or.Coupon));
                        parms.Add(new MySqlParameter("@InvoiceData", or.Invoice));
                        parms.Add(new MySqlParameter("@CreditData", or.Credit));
                        parms.Add(new MySqlParameter("@ProcessStage", "A"));
                        parms.Add(new MySqlParameter("@Price", or.Price));
                        parms.Add(new MySqlParameter("@PassengerName", or.PassengerName));
                        parms.Add(new MySqlParameter("@PassengerPhone", or.PassengerPhone));


                        conn.executeInsertQuery(sb.ToString(), parms.ToArray());

                        //撿車的 必須新增派車單訂單資料
                        //新增一筆dispatch_reservation
                        sb.Length = 0;
                        parms.Clear();


                        sb.Append("INSERT INTO dispatch_reservation	(DispatchNo, ReservationNo, ServeSeq, ScheduleServeTime, ActualServreTime, LocationGPS, ServiceRemark) ");
                        sb.Append("VALUES (@DispatchNo, @ReservationNo, 0, @ScheduleServeTime,NULL , GeomFromText(@LocationGPS), '0')");
                        parms.Add(new MySqlParameter("@DispatchNo", DispatchNo));
                        parms.Add(new MySqlParameter("@ReservationNo", ReservationNo));
                        parms.Add(new MySqlParameter("@ScheduleServeTime", ServiceTime));
                        if (or.ServiceType == "O")
                            parms.Add(new MySqlParameter("@LocationGPS", "Point(" + or.PickupGPS.GetLng().ToString() + " " + or.PickupGPS.GetLat().ToString() + ")"));
                        else
                            parms.Add(new MySqlParameter("@LocationGPS", "Point(" + or.TakeoffGPS.GetLng().ToString() + " " + or.TakeoffGPS.GetLat().ToString() + ")"));

                        conn.executeInsertQuery(sb.ToString(), parms.ToArray());
                    }
                    scope.Complete();
                    return ReservationNo;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        /// <summary>
        ///  取消一筆訂單
        /// </summary>
        /// <param name="CancelType">取消類別(X 用戶取消,W 不計價取消,Y 計價取消)</param>
        /// <param name="ReservationNo"></param>
        /// <returns></returns>
        public bool CancelReservation(string CancelType, string ReservationNo)
        {
            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._connectstring))
                {
                    dmReservation dm = new Reservation.dmReservation(conn);

                    dm.Select(ReservationNo);
                    dm.ProcessStage = CancelType;
                    if (dm.Update() != 1)
                        return false;
                    else
                        return true;
                }
            }
            catch (MySqlException ex)
            {
                throw ex;
            }
        }

        /// <summary>
        ///  利用訂單編號取得訂單
        /// </summary>
        /// <param name="ReservationNo"></param>
        /// <returns>OReservation Object or NULL</returns>
        public OReservation GetReservation(string ReservationNo)
        {
            dmReservation dm;
            using (dbConnectionMySQL conn = new dbConnectionMySQL(this._connectstring))
            {
                dm = new dmReservation(conn);
                dm.Select(ReservationNo);
            }
            OReservation or = new OReservation();
            if (dm.ReservationNo != "")
            {
                or.ReservationNo = dm.ReservationNo;
                or.AddBagCnt = dm.AddBaggageCnt;
                or.Airport = dm.Airport;
                or.BaggageCnt = dm.BaggageCnt;
                or.BonusPoint = dm.NewBonusPoint;
                or.CarpoolFlag = dm.CarpoolFlag;
                or.ChooseType = dm.SelectionType;
                or.Coupon = dm.Coupon;
                or.Credit = dm.CreditData;
                or.DiscountAmt = dm.DiscountAmt;
                or.DiscountType = dm.DiscountType;
                or.EIN = dm.InvoiceNum;
                if (dm.FlightNo != null && dm.FlightNo != "")
                {
                    or.FlightDate = dm.ScheduleFlightTime.ToString("yyyyMMdd");
                    or.FlightNo = dm.FlightNo;
                    or.FlightTime = dm.ScheduleFlightTime;
                }
                else
                {
                    or.FlightDate = "";
                    or.FlightNo = "";
                }
                or.Invoice = dm.InvoiceData;
                or.MaxFlag = dm.MaxFlag;
                or.OrderTime = dm.CreateTime;
                or.PassengerCnt = dm.PassengerCnt;
                or.PassengerName = dm.PassengerName;
                or.PassengerPhone = dm.PassengerPhone;
                or.PickCartype = dm.PickCartype;
                or.PickupAddress = dm.PickupAddress;
                or.PickupCity = dm.PickupCity;
                or.PickupDistinct = dm.PickupDistinct;
                or.PickupGPS = new OSpatialGPS(dm.PickupLat, dm.PickupLng);
                or.PickupVillage = dm.PickupVillage;
                or.PreferTime = dm.TimeSegment;
                or.Price = dm.Price;
                or.ProcessStage = dm.ProcessStage;
                or.ServeDate = dm.TakeDate;
                or.ServiceType = dm.ServiceType;
                or.TakeoffAddress = dm.TakeoffAddress;
                or.TakeoffCity = dm.TakeoffCity;
                or.TakeoffDistinct = dm.TakeoffDistinct;
                or.TakeoffGPS = new OSpatialGPS(dm.TakeoffLat, dm.TakeoffLng);
                or.TakeoffVillage = dm.TakeoffVillage;
                or.Ternimal = dm.Ternimal;
                or.UserID = dm.UserID;
                return or;
            }
            else
                return null;

        }
    }
}
