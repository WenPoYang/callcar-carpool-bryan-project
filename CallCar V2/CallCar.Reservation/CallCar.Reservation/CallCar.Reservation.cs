﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CallCar;
using DbConn;
using MySql.Data.MySqlClient;
using System.Data;

namespace CallCar.Reservation
{
    public class dmReservation : DataModel
    {
        public dmReservation(dbConnectionMySQL Conn) : base(Conn) { this.ReservationNo = ""; }

        public override int Insert()
        {
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            sb.Append("INSERT INTO reservation_sheet ");
            sb.Append("(ReservationNo, UserID, ServiceType, TakeDate, TimeSegment, SelectionType, PickCartype,  CarpoolFlag, FlightNo, ScheduleFlightTime, Airport, Ternimal, PickupCity, PickupDistinct, PickupVillage, PickupAddress, PickupGPS, TakeoffCity, TakeoffDistinct, TakeoffVillage, TakeoffAddress, TakeoffGPS, PassengerCnt, BaggageCnt, AddBaggageCnt, MaxFlag, Price, DiscountType, Coupon, DiscountAmt, NewBonusPoint, InvoiceData, CreditData, ProcessStage, InvoiceNum, PassengerName, PassengerPhone) ");
            sb.Append("VALUES(@ReservationNo, @UserID, @ServiceType, @TakeDate, @TimeSegment, @SelectionType, @PickCartype,  @CarpoolFlag, @FlightNo, @ScheduleFlightTime, @Airport, @Ternimal, @PickupCity, @PickupDistinct, @PickupVillage, @PickupAddress, GeomFromText(@PickupGPS), @TakeoffCity, @TakeoffDistinct, @TakeoffVillage, @TakeoffAddress, GeomFromText(@TakeoffGPS), @PassengerCnt, @BaggageCnt, @AddBaggageCnt, @MaxFlag, @Price, @DiscountType, @Coupon, @DiscountAmt, @NewBonusPoint, @InvoiceData, @CreditData, @ProcessStage, @InvoiceNum, @PassengerName, @PassengerPhone)");

            parms.Add(new MySqlParameter("@ReservationNo", this.ReservationNo));
            parms.Add(new MySqlParameter("@UserID", this.UserID));
            parms.Add(new MySqlParameter("@ServiceType", this.ServiceType));
            parms.Add(new MySqlParameter("@TakeDate", this.TakeDate));
            parms.Add(new MySqlParameter("@TimeSegment", this.TimeSegment));
            parms.Add(new MySqlParameter("@SelectionType", this.SelectionType));
            parms.Add(new MySqlParameter("@PickCartype", this.PickCartype));
            parms.Add(new MySqlParameter("@CarpoolFlag", this.CarpoolFlag));
            parms.Add(new MySqlParameter("@FlightNo", this.FlightNo));
            if (this.FlightNo == null || this.FlightNo == "")
                parms.Add(new MySqlParameter("@ScheduleFlightTime", DBNull.Value));
            else
                parms.Add(new MySqlParameter("@ScheduleFlightTime", this.ScheduleFlightTime));
            parms.Add(new MySqlParameter("@Airport", this.Airport));
            parms.Add(new MySqlParameter("@Ternimal", this.Ternimal));
            parms.Add(new MySqlParameter("@PickupCity", this.PickupCity));
            parms.Add(new MySqlParameter("@PickupDistinct", this.PickupDistinct));
            parms.Add(new MySqlParameter("@PickupVillage", this.PickupVillage));
            parms.Add(new MySqlParameter("@PickupAddress", this.PickupAddress));
            parms.Add(new MySqlParameter("@PickupGPS", "Point(" + this.PickupLng.ToString() + " " + this.PickupLat.ToString() + ")"));
            parms.Add(new MySqlParameter("@TakeoffCity", this.TakeoffCity));
            parms.Add(new MySqlParameter("@TakeoffDistinct", this.TakeoffDistinct));
            parms.Add(new MySqlParameter("@TakeoffVillage", this.TakeoffVillage));
            parms.Add(new MySqlParameter("@TakeoffAddress", this.TakeoffAddress));
            parms.Add(new MySqlParameter("@TakeoffGPS", "Point(" + this.TakeoffLng.ToString() + " " + this.TakeoffLat.ToString() + ")"));
            parms.Add(new MySqlParameter("@PassengerCnt", this.PassengerCnt));
            parms.Add(new MySqlParameter("@BaggageCnt", this.BaggageCnt));
            parms.Add(new MySqlParameter("@AddBaggageCnt", this.AddBaggageCnt));
            parms.Add(new MySqlParameter("@MaxFlag", this.MaxFlag));
            parms.Add(new MySqlParameter("@Price", this.Price));
            parms.Add(new MySqlParameter("@DiscountType", this.DiscountType));
            parms.Add(new MySqlParameter("@Coupon", this.Coupon));
            parms.Add(new MySqlParameter("@DiscountAmt", this.DiscountAmt));
            parms.Add(new MySqlParameter("@NewBonusPoint", this.NewBonusPoint));
            parms.Add(new MySqlParameter("@InvoiceData", this.InvoiceData));
            parms.Add(new MySqlParameter("@CreditData", this.CreditData));
            parms.Add(new MySqlParameter("@ProcessStage", this.ProcessStage));
            parms.Add(new MySqlParameter("@InvoiceNum", this.InvoiceNum));
            parms.Add(new MySqlParameter("@PassengerName", this.PassengerName));
            parms.Add(new MySqlParameter("@PassengerPhone", this.PassengerPhone));

            try
            {
                return this._conn.executeInsertQuery(sb.ToString(), parms.ToArray());
            }
            catch (MySqlException ex)
            {
                throw ex;
            }
        }

        public override int Delete()
        {
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();

            sb.Append("DELETE FROM reservation_sheet WHERE ReservationNo=@ReservationNo");
            parms.Add(new MySqlParameter("@ReservationNo", this.ReservationNo));

            try
            {
                return this._conn.executeDeleteQuery(sb.ToString(), parms.ToArray());
            }
            catch (MySqlException ex)
            {
                throw ex;
            }
        }

        public override int Update()
        {
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            sb.Append("UPDATE reservation_sheet SET ");
            sb.Append("UserID = @UserID , ServiceType = @ServiceType, TakeDate = @TakeDate, TimeSegment = @TimeSegment, SelectionType = @SelectionType, PickCartype = @PickCartype, ");
            sb.Append("CarpoolFlag = @CarpoolFlag, FlightNo = @FlightNo, ScheduleFlightTime = @ScheduleFlightTime, Airport = @Airport, ");
            sb.Append("Ternimal = @Ternimal, PickupCity = @PickupCity, PickupDistinct = @PickupDistinct, PickupVillage = @PickupVillage, PickupAddress = @PickupAddress, ");
            sb.Append("PickupGPS = GeomFromText(@PickupGPS), TakeoffCity = @TakeoffCity, TakeoffDistinct = @TakeoffDistinct, TakeoffVillage = @TakeoffVillage, TakeoffAddress = @TakeoffAddress, ");
            sb.Append("TakeoffGPS = GeomFromText(@TakeoffGPS), PassengerCnt = @PassengerCnt, BaggageCnt = @BaggageCnt, AddBaggageCnt = @AddBaggageCnt, MaxFlag = @MaxFlag, ");
            sb.Append("Price = @Price, DiscountType = @DiscountType, Coupon = @Coupon, DiscountAmt = @DiscountAmt, NewBonusPoint = @NewBonusPoint, InvoiceData = @InvoiceData, ");
            sb.Append("CreditData = @CreditData, ProcessStage = @ProcessStage, InvoiceNum = @InvoiceNum, PassengerName = @PassengerName, PassengerPhone = @PassengerPhone ");
            sb.Append("WHERE ReservationNo = @ReservationNo ");

            parms.Add(new MySqlParameter("@ReservationNo", this.ReservationNo));
            parms.Add(new MySqlParameter("@UserID", this.UserID));
            parms.Add(new MySqlParameter("@ServiceType", this.ServiceType));
            parms.Add(new MySqlParameter("@TakeDate", this.TakeDate));
            parms.Add(new MySqlParameter("@TimeSegment", this.TimeSegment));
            parms.Add(new MySqlParameter("@SelectionType", this.SelectionType));
            parms.Add(new MySqlParameter("@PickCartype", this.PickCartype));
            parms.Add(new MySqlParameter("@CarpoolFlag", this.CarpoolFlag));
            parms.Add(new MySqlParameter("@FlightNo", this.FlightNo));
            parms.Add(new MySqlParameter("@ScheduleFlightTime", this.ScheduleFlightTime));
            parms.Add(new MySqlParameter("@Airport", this.Airport));
            parms.Add(new MySqlParameter("@Ternimal", this.Ternimal));
            parms.Add(new MySqlParameter("@PickupCity", this.PickupCity));
            parms.Add(new MySqlParameter("@PickupDistinct", this.PickupDistinct));
            parms.Add(new MySqlParameter("@PickupVillage", this.PickupVillage));
            parms.Add(new MySqlParameter("@PickupAddress", this.PickupAddress));
            parms.Add(new MySqlParameter("@PickupGPS", "Point(" + this.PickupLng.ToString() + " " + this.PickupLat.ToString() + ")"));
            parms.Add(new MySqlParameter("@TakeoffCity", this.TakeoffCity));
            parms.Add(new MySqlParameter("@TakeoffDistinct", this.TakeoffDistinct));
            parms.Add(new MySqlParameter("@TakeoffVillage", this.TakeoffVillage));
            parms.Add(new MySqlParameter("@TakeoffAddress", this.TakeoffAddress));
            parms.Add(new MySqlParameter("@TakeoffGPS", "Point(" + this.TakeoffLng.ToString() + " " + this.TakeoffLat.ToString() + ")"));
            parms.Add(new MySqlParameter("@PassengerCnt", this.PassengerCnt));
            parms.Add(new MySqlParameter("@BaggageCnt", this.BaggageCnt));
            parms.Add(new MySqlParameter("@AddBaggageCnt", this.AddBaggageCnt));
            parms.Add(new MySqlParameter("@MaxFlag", this.MaxFlag));
            parms.Add(new MySqlParameter("@Price", this.Price));
            parms.Add(new MySqlParameter("@DiscountType", this.DiscountType));
            parms.Add(new MySqlParameter("@Coupon", this.Coupon));
            parms.Add(new MySqlParameter("@DiscountAmt", this.DiscountAmt));
            parms.Add(new MySqlParameter("@NewBonusPoint", this.NewBonusPoint));
            parms.Add(new MySqlParameter("@InvoiceData", this.InvoiceData));
            parms.Add(new MySqlParameter("@CreditData", this.CreditData));
            parms.Add(new MySqlParameter("@ProcessStage", this.ProcessStage));
            parms.Add(new MySqlParameter("@InvoiceNum", this.InvoiceNum));
            parms.Add(new MySqlParameter("@PassengerName", this.PassengerName));
            parms.Add(new MySqlParameter("@PassengerPhone", this.PassengerPhone));

            try
            {
                return this._conn.executeUpdateQuery(sb.ToString(), parms.ToArray());
            }
            catch (MySqlException ex)
            {
                throw ex;
            }
        }

        public override void Select(string ReservationNo)
        {
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            DataTable dt = new DataTable();
            sb.Append("SELECT ReservationNo, UserID, ServiceType, TakeDate, TimeSegment, SelectionType, PickCartype,  CarpoolFlag, FlightNo, ScheduleFlightTime, Airport, Ternimal, PickupCity, PickupDistinct, PickupVillage, PickupAddress, X(PickupGPS) as PickupLng,Y(PickupGPS) as PickupLat, TakeoffCity, TakeoffDistinct, TakeoffVillage, TakeoffAddress, X(TakeoffGPS) as TakeoffLng , Y(TakeoffGPS) as TakeoffLat, PassengerCnt, BaggageCnt, AddBaggageCnt, MaxFlag, Price, DiscountType, Coupon, DiscountAmt, NewBonusPoint, InvoiceData, CreditData, ProcessStage, InvoiceNum, PassengerName, PassengerPhone, CreateTime, UpdTime ");
            sb.Append("FROM reservation_sheet Where ReservationNo = @ReservationNo");
            parms.Add(new MySqlParameter("@ReservationNo", ReservationNo));

            try
            {
                dt = this._conn.executeSelectQuery(sb.ToString(), parms.ToArray());
            }
            catch (MySqlException ex)
            {
                throw ex;
            }

            if (dt.Rows.Count == 1)
            {
                try
                {
                    this.ReservationNo = Convert.ToString(dt.Rows[0]["ReservationNo"]);
                    this.UserID = Convert.ToInt32(dt.Rows[0]["UserID"]);
                    this.ServiceType = Convert.ToString(dt.Rows[0]["ServiceType"]);
                    this.TakeDate = Convert.ToString(dt.Rows[0]["TakeDate"]);
                    this.TimeSegment = Convert.ToString(dt.Rows[0]["TimeSegment"]);
                    this.SelectionType = Convert.ToString(dt.Rows[0]["SelectionType"]);
                    this.PickCartype = Convert.ToString(dt.Rows[0]["PickCartype"]);
                    this.CarpoolFlag = Convert.ToString(dt.Rows[0]["CarpoolFlag"]);
                    this.FlightNo = Convert.ToString(dt.Rows[0]["FlightNo"]);
                    if (dt.Rows[0]["ScheduleFlightTime"] != DBNull.Value && dt.Rows[0]["ScheduleFlightTime"] != null)
                        this.ScheduleFlightTime = Convert.ToDateTime(dt.Rows[0]["ScheduleFlightTime"]);

                    this.Airport = Convert.ToString(dt.Rows[0]["Airport"]);
                    this.Ternimal = Convert.ToString(dt.Rows[0]["Ternimal"]);
                    this.PickupCity = Convert.ToString(dt.Rows[0]["PickupCity"]);
                    this.PickupDistinct = Convert.ToString(dt.Rows[0]["PickupDistinct"]);
                    this.PickupVillage = Convert.ToString(dt.Rows[0]["PickupVillage"]);
                    this.PickupAddress = Convert.ToString(dt.Rows[0]["PickupAddress"]);
                    this.PickupLat = Convert.ToDouble(dt.Rows[0]["PickupLat"]);
                    this.PickupLng = Convert.ToDouble(dt.Rows[0]["PickupLng"]);
                    this.TakeoffCity = Convert.ToString(dt.Rows[0]["TakeoffCity"]);
                    this.TakeoffDistinct = Convert.ToString(dt.Rows[0]["TakeoffDistinct"]);
                    this.TakeoffVillage = Convert.ToString(dt.Rows[0]["TakeoffVillage"]);
                    this.TakeoffAddress = Convert.ToString(dt.Rows[0]["TakeoffAddress"]);
                    this.TakeoffLat = Convert.ToDouble(dt.Rows[0]["TakeoffLat"]);
                    this.TakeoffLng = Convert.ToDouble(dt.Rows[0]["TakeoffLng"]);
                    this.PassengerCnt = Convert.ToInt32(dt.Rows[0]["PassengerCnt"]);
                    this.BaggageCnt = Convert.ToInt32(dt.Rows[0]["BaggageCnt"]);
                    this.AddBaggageCnt = Convert.ToInt32(dt.Rows[0]["AddBaggageCnt"]);
                    this.MaxFlag = Convert.ToString(dt.Rows[0]["MaxFlag"]);
                    this.Price = Convert.ToInt32(dt.Rows[0]["Price"]);
                    this.DiscountType = Convert.ToString(dt.Rows[0]["DiscountType"]);
                    this.Coupon = Convert.ToString(dt.Rows[0]["Coupon"]);
                    this.DiscountAmt = Convert.ToInt32(dt.Rows[0]["DiscountAmt"]);
                    this.NewBonusPoint = Convert.ToInt32(dt.Rows[0]["NewBonusPoint"]);
                    this.InvoiceData = Convert.ToString(dt.Rows[0]["InvoiceData"]);
                    this.CreditData = Convert.ToString(dt.Rows[0]["CreditData"]);
                    this.ProcessStage = Convert.ToString(dt.Rows[0]["ProcessStage"]);
                    this.InvoiceNum = Convert.ToString(dt.Rows[0]["InvoiceNum"]);
                    this.PassengerName = Convert.ToString(dt.Rows[0]["PassengerName"]);
                    this.PassengerPhone = Convert.ToString(dt.Rows[0]["PassengerPhone"]);
                    this.CreateTime = Convert.ToDateTime(dt.Rows[0]["CreateTime"]);
                    this.UpdTime = Convert.ToDateTime(dt.Rows[0]["UpdTime"]);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
                this.ReservationNo = "";
        }


        public string ReservationNo { get; set; }
        public int UserID { get; set; }
        public string ServiceType { get; set; }
        public string TakeDate { get; set; }
        public string TimeSegment { get; set; }
        public string SelectionType { get; set; }
        public string PickCartype { get; set; }
        public string CarpoolFlag { get; set; }
        public string FlightNo { get; set; }
        public DateTime ScheduleFlightTime { get; set; }
        public string Airport { get; set; }
        public string Ternimal { get; set; }
        public string PickupCity { get; set; }
        public string PickupDistinct { get; set; }
        public string PickupVillage { get; set; }
        public string PickupAddress { get; set; }
        public double PickupLng { get; set; }
        public double PickupLat { get; set; }
        public string TakeoffCity { get; set; }
        public string TakeoffDistinct { get; set; }
        public string TakeoffVillage { get; set; }
        public string TakeoffAddress { get; set; }
        public double TakeoffLng { get; set; }
        public double TakeoffLat { get; set; }
        public int PassengerCnt { get; set; }
        public int BaggageCnt { get; set; }
        public int AddBaggageCnt { get; set; }
        public string MaxFlag { get; set; }
        public int Price { get; set; }
        public string DiscountType { get; set; }
        public string Coupon { get; set; }
        public int DiscountAmt { get; set; }
        public int NewBonusPoint { get; set; }
        public string InvoiceData { get; set; }
        public string CreditData { get; set; }
        public string ProcessStage { get; set; }
        public string InvoiceNum { get; set; }
        public string PassengerName { get; set; }
        public string PassengerPhone { get; set; }
        public DateTime CreateTime { get; set; }
        public DateTime UpdTime { get; set; }
    }

    public class dmFloatPrice : DataModel
    {
        public dmFloatPrice(dbConnectionMySQL Conn) : base(Conn) { }


        public int SeqNo { get; set; }
        public string ReservationNo { get; set; }
        public int price { get; set; }
        public DateTime CreateTime { get; set; }
        public DateTime UpdTime { get; set; }
    }
}
