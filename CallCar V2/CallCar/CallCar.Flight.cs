﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DbConn;
using MySql.Data.MySqlClient;
using System.Data;
using CallCar.Data;

namespace CallCar.Flight
{
    public class BALFlight : CallCarBAL
    {
        public BALFlight(string ConnectionString) : base(ConnectionString) { }

        public OAirline GetAirlineInfo(string AirlineIATACode)
        {
            dmAirline dm;
            using (dbConnectionMySQL conn = new dbConnectionMySQL(this._connectstring))
            {
                dm = new dmAirline(conn);
                dm.Select(AirlineIATACode);
            }

            if (dm.SeqNo == 0)
                return null;

            OAirline oa = new OAirline();
            oa.AirlineName = dm.AirlineName;
            oa.CallSign = dm.CallSign;
            oa.Country = dm.Country;
            oa.IATA = dm.IATA;
            oa.ICAO = dm.ICAO;

            return oa;
        }
        public OAirport GetAirportInfo(string AirportCode)
        {
            dmAirport dm;
            using (dbConnectionMySQL conn = new dbConnectionMySQL(this._connectstring))
            {
                dm = new dmAirport(conn);
                dm.Select(AirportCode);
            }
            if (dm.SeqNo == 0)
                return null;

            OAirport oa = new OAirport();
            oa.AirportCode = dm.AirportCode;
            oa.AirportName = dm.AirportName;
            oa.CityName = dm.CityName;
            oa.CountryAbbrev = dm.CountryAbbrev;
            oa.CountryName = dm.CountryName;
            return oa;
        }
    }

    public class dmAirport : DataModel
    {
        public dmAirport(dbConnectionMySQL Conn) : base(Conn) { this.SeqNo = 0; }

        public override void Select(string AirportCode)
        {
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            DataTable dt;

            sb.Append("SELECT SeqNo, AirportCode, AirportCodeICAO, AirportName, CityName, CountryName, CountryAbbrev, WorldAreacode, CreateTime, UpdTime ");
            sb.Append("FROM airport_info   where AirportCode = @code ");

            parms.Add(new MySqlParameter("@code", AirportCode));

            try
            {
                dt = this._conn.executeSelectQuery(sb.ToString(), parms.ToArray());
            }
            catch (MySqlException ex)
            {
                throw ex;
            }

            if (dt.Rows.Count == 1)
            {
                this.SeqNo = Convert.ToInt32(dt.Rows[0]["SeqNo"]);
                this.AirportCode = Convert.ToString(dt.Rows[0]["AirportCode"]);
                this.AirportCodeICAO = Convert.ToString(dt.Rows[0]["AirportCodeICAO"]);
                this.AirportName = Convert.ToString(dt.Rows[0]["AirportName"]);
                this.CityName = Convert.ToString(dt.Rows[0]["CityName"]);
                this.CountryAbbrev = Convert.ToString(dt.Rows[0]["CountryAbbrev"]);
                this.CountryName = Convert.ToString(dt.Rows[0]["CountryName"]);
                this.WorldAreacode = Convert.ToInt32(dt.Rows[0]["WorldAreacode"]);
                this.CreateTime = Convert.ToDateTime(dt.Rows[0]["CreateTime"]);
                this.UpdTime = Convert.ToDateTime(dt.Rows[0]["UpdTime"]);
            }
            else
                this.SeqNo = 0;

        }


        public int SeqNo { get; set; }
        public string AirportCode { get; set; }
        public string AirportCodeICAO { get; set; }
        public string AirportName { get; set; }
        public string CityName { get; set; }
        public string CountryName { get; set; }
        public string CountryAbbrev { get; set; }
        public int WorldAreacode { get; set; }
        public DateTime CreateTime { get; set; }
        public DateTime UpdTime { get; set; }
    }

    public class dmAirline : DataModel
    {
        public dmAirline(dbConnectionMySQL Conn) : base(Conn) { this.SeqNo = 0; }

        public override void Select(string IATACode)
        {
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            DataTable dt;

            sb.Append("SELECT SeqNo, IATA, ICAO, AirlineName, CallSign, Country, `Comment`, CreateTime, UpdTime ");
            sb.Append("FROM airline_info   where IATA = @code");

            parms.Add(new MySqlParameter("@code", IATACode));

            try
            {
                dt = this._conn.executeSelectQuery(sb.ToString(), parms.ToArray());
            }
            catch (MySqlException ex)
            {
                throw ex;
            }

            if (dt.Rows.Count == 1)
            {
                this.SeqNo = Convert.ToInt32(dt.Rows[0]["SeqNo"]);
                this.AirlineName = Convert.ToString(dt.Rows[0]["AirlineName"]);
                this.CallSign = Convert.ToString(dt.Rows[0]["CallSign"]);
                this.Comment = Convert.ToString(dt.Rows[0]["Comment"]);
                this.Country = Convert.ToString(dt.Rows[0]["Country"]);
                this.IATA = Convert.ToString(dt.Rows[0]["IATA"]);
                this.ICAO = Convert.ToString(dt.Rows[0]["ICAO"]);
                this.CreateTime = Convert.ToDateTime(dt.Rows[0]["CreateTime"]);
                this.UpdTime = Convert.ToDateTime(dt.Rows[0]["UpdTime"]);
            }
            else
                this.SeqNo = 0;

        }

        public int SeqNo { get; set; }
        public string IATA { get; set; }
        public string ICAO { get; set; }
        public string AirlineName { get; set; }
        public string CallSign { get; set; }
        public string Country { get; set; }
        public string Comment { get; set; }
        public DateTime CreateTime { get; set; }
        public DateTime UpdTime { get; set; }
    }
}
