﻿using DbConn;

namespace CallCar
{
    public class CallCarBAL
    {
        protected string _connectstring;

        public CallCarBAL(string ConnectionString)
        {
            this._connectstring = ConnectionString;
        }
    }
    public class DataModel
    {
        protected dbConnectionMySQL _conn;

        public DataModel(dbConnectionMySQL Conn)
        {
            this._conn = Conn;
        }

        public virtual int Insert()
        {
            return 0;
        }

        public virtual int Update()
        {
            return 0;
        }

        public virtual int Delete()
        {
            return 0;
        }
        public virtual void Select(string key) { }
        public virtual void Select(int key) { }
        public virtual void Select(string key1, string key2) { }
        public virtual void Select(string key1, string key2, string key3) { }
    }
}
