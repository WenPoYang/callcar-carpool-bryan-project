﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.IO;
using CallCar.Data;
using MySql.Data.MySqlClient;
using DbConn;
using System.Data;

namespace CallCar.Tool
{
    public class General
    {
        private const double EARTH_RADIUS = 6378.137;//地球半徑 
        private static double rad(double d)
        {
            return d * Math.PI / 180.0;
        }
        public static double GetDistance(double lat1, double lng1, double lat2, double lng2)
        {
            double radLat1 = rad(lat1);
            double radLat2 = rad(lat2);
            double a = radLat1 - radLat2;
            double b = rad(lng1) - rad(lng2);
            double s = 2 * Math.Asin(Math.Sqrt(Math.Pow(Math.Sin(a / 2), 2) +
            Math.Cos(radLat1) * Math.Cos(radLat2) * Math.Pow(Math.Sin(b / 2), 2)));
            s = s * EARTH_RADIUS;
            s = Math.Round(s * 10000) / 10000;
            return s; //單位公里
        }

        public static DateTime ConvertToDateTime(string input, int length)
        {
            if (length == 8)
                return DateTime.Parse(input.Substring(0, 4) + "/" + input.Substring(4, 2) + "/" + input.Substring(6, 2));
            else if (length == 12)
                return DateTime.Parse(input.Substring(0, 4) + "/" + input.Substring(4, 2) + "/" + input.Substring(6, 2) + " " + input.Substring(8, 2) + ":" + input.Substring(10, 2));
            else if (length == 14)
                return DateTime.Parse(input.Substring(0, 4) + "/" + input.Substring(4, 2) + "/" + input.Substring(6, 2) + " " + input.Substring(8, 2) + ":" + input.Substring(10, 2) + ":" + input.Substring(12, 2));
            throw new Exception("Convert Length Error");

        }

        public static string OutputCrypt(string plantext, string key)
        {

            string iv = "8417728284177282";
            AESCryptography aes = new AESCryptography(key, iv);
            return aes.Encrypt(plantext);
        }

        public static string GetRandomString(int length)
        {
            var str = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
            var next = new Random();
            var builder = new StringBuilder();
            for (var i = 0; i < length; i++)
            {
                builder.Append(str[next.Next(0, str.Length)]);
            }
            return builder.ToString();
        }
    }
    public class AESCryptography
    {
        public AESCryptography(string key, string iv)
        {

            _key = key;
            _iv = iv;


        }

        public string Decrypt(string CipherText)
        {
            byte[] Key, IV;
            Key = Encoding.UTF8.GetBytes(this._key);
            IV = Encoding.UTF8.GetBytes(this._iv);

            byte[] encrypted = Convert.FromBase64String(CipherText);
            return DecryptStringFromBytes(encrypted, Key, IV);
        }

        public string Encrypt(string PlanText)
        {
            byte[] Key, IV;
            Key = Encoding.UTF8.GetBytes(this._key);
            IV = Encoding.UTF8.GetBytes(this._iv);
            var EncryptString = EncryptStringToBytes(PlanText, Key, IV);
            return Convert.ToBase64String(EncryptString);
        }

        private static string DecryptStringFromBytes(byte[] cipherText, byte[] key, byte[] iv)
        {
            if (cipherText == null || cipherText.Length <= 0)
            {
                throw new ArgumentNullException("cipherText");
            }
            if (key == null || key.Length <= 0)
            {
                throw new ArgumentNullException("key");
            }
            if (iv == null || iv.Length <= 0)
            {
                throw new ArgumentNullException("key");
            }
            string plaintext = null;
            using (var rijAlg = new RijndaelManaged())
            {
                rijAlg.Mode = CipherMode.CBC;
                rijAlg.Padding = PaddingMode.PKCS7;
                rijAlg.FeedbackSize = 128;
                rijAlg.Key = key;
                rijAlg.IV = iv;
                var decryptor = rijAlg.CreateDecryptor(rijAlg.Key, rijAlg.IV);
                try
                {
                    using (var msDecrypt = new MemoryStream(cipherText))
                    {
                        using (var csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                        {
                            using (var srDecrypt = new StreamReader(csDecrypt))
                            {
                                plaintext = srDecrypt.ReadToEnd();
                            }
                        }
                    }
                }
                catch (Exception)
                {
                    plaintext = "keyError";
                }
            }
            return plaintext;
        }
        private static byte[] EncryptStringToBytes(string plainText, byte[] key, byte[] iv)
        {
            if (plainText == null || plainText.Length <= 0)
            {
                throw new ArgumentNullException("plainText");
            }
            if (key == null || key.Length <= 0)
            {
                throw new ArgumentNullException("key");
            }
            if (iv == null || iv.Length <= 0)
            {
                throw new ArgumentNullException("key");
            }
            byte[] encrypted;
            using (var rijAlg = new RijndaelManaged())
            {
                rijAlg.Mode = CipherMode.CBC;
                rijAlg.Padding = PaddingMode.PKCS7;
                rijAlg.FeedbackSize = 128;
                rijAlg.Key = key;
                rijAlg.IV = iv;
                var encryptor = rijAlg.CreateEncryptor(rijAlg.Key, rijAlg.IV);
                using (var msEncrypt = new MemoryStream())
                {
                    using (var csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    {
                        using (var swEncrypt = new StreamWriter(csEncrypt))
                        {
                            swEncrypt.Write(plainText);
                        }
                        encrypted = msEncrypt.ToArray();
                    }
                }
            }
            return encrypted;
        }


        public static string TokenAnalysis(ref string data)
        {
            if (data.Length <= 16)
                throw new Exception("Data Error");

            string token = data.Substring(data.Length - 16, 16);

            data = data.Substring(0, data.Length - 16);
            return token;
        }

        private string _key { get; set; }
        private string _iv { get; set; }
    }

    public class MD5Cryptography
    {
        private const string salted = "callcar";

        public static string Encrypt(string PlanText)
        {
            byte[] Original = Encoding.Default.GetBytes(PlanText);//將來源字串轉為byte[] 
            byte[] SaltValue = Encoding.Default.GetBytes(salted);//將Salted Value轉為byte[] 
            byte[] ToSalt = new byte[Original.Length + SaltValue.Length]; //宣告新的byte[]來儲存加密後的值 
            Original.CopyTo(ToSalt, 0);//將來源字串複製到新byte[] 
            SaltValue.CopyTo(ToSalt, Original.Length);//將Salted Value複製到新byte[] 
            MD5 st = MD5.Create();//使用MD5 
            byte[] SaltPWD = st.ComputeHash(ToSalt);//進行加密 
            byte[] PWD = new byte[SaltPWD.Length + SaltValue.Length];//宣告新byte[]儲存加密及Salted的值 
            SaltPWD.CopyTo(PWD, 0);//將加密後的值複製到新byte[] 
            SaltValue.CopyTo(PWD, SaltPWD.Length);//將Salted Value複製到新byte[] 
            return Convert.ToBase64String(PWD);//顯示Salted Hash後的字串
        }
    }

    /// <summary>
    /// 發送EMAIL
    /// </summary>
    public class GmailSMTP
    {
        private System.Net.Mail.SmtpClient MySmtp;
        private string Account;
        private string Password;

        public GmailSMTP(string Account, string Password)
        {
            this.Account = Account;
            this.Password = Password;
            /*
              *  outlook.com smtp.live.com port:25
              *  yahoo smtp.mail.yahoo.com.tw port:465
             */
            //建立 SmtpClient 物件 並設定 Gmail的smtp主機及Port 
            MySmtp = new System.Net.Mail.SmtpClient("smtp.gmail.com", 587);
            //設定你的帳號密碼            
            MySmtp.Credentials = new System.Net.NetworkCredential(this.Account, this.Password);
            //Gmial 的 smtp 使用 SSL
            MySmtp.EnableSsl = true;
        }

        public string Send(List<string> MailList, string DisplayName, string Subject, string Body, List<System.Net.Mail.Attachment> Attach)
        {
            try
            {
                System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();
                //收件者，以逗號分隔不同收件者 ex "test@gmail.com,test2@gmail.com"
                msg.To.Add(string.Join(",", MailList.ToArray()));
                msg.From = new System.Net.Mail.MailAddress(this.Account, DisplayName, System.Text.Encoding.UTF8);
                //郵件標題 
                msg.Subject = Subject;
                //郵件標題編碼  
                msg.SubjectEncoding = System.Text.Encoding.UTF8;
                //郵件內容
                msg.Body = Body;
                msg.IsBodyHtml = true;
                msg.BodyEncoding = System.Text.Encoding.UTF8;//郵件內容編碼 
                msg.Priority = System.Net.Mail.MailPriority.Normal;//郵件優先級 
                if (Attach != null && Attach.Count > 0)
                {
                    foreach (System.Net.Mail.Attachment a in Attach)
                        msg.Attachments.Add(a);
                }

                MySmtp.Send(msg);
                return "Success";
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }

    /// <summary>
    /// 取系統流水號
    /// </summary>
    public class BALSerialNum : CallCarBAL
    {
        public BALSerialNum(string ConnectionString)
            : base(ConnectionString)
        {
        }

        public OSerialNum GetSerialNumObject(string Purpose)
        {
            OSerialNum o = new OSerialNum();
            StringBuilder sb = new StringBuilder();
            sb.Append("SELECT Prefix, Digit, Current, Used FROM sequence_maintain Where Purpose = @p");
            List<MySqlParameter> parms = new List<MySqlParameter>();
            DataTable dt = new DataTable();
            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._connectstring))
                {
                    parms.Add(new MySqlParameter("@p", Purpose));
                    dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                }
            }
            catch (MySqlException ex)
            {
                throw new Exception(ex.Message);
            }

            if (dt.Rows.Count > 0)
            {
                o.Prefix = Convert.ToString(dt.Rows[0]["Prefix"]);
                o.Current = Convert.ToString(dt.Rows[0]["Current"]);
                o.Digit = Convert.ToInt32(dt.Rows[0]["Digit"]);
                o.Used = Convert.ToInt32(dt.Rows[0]["Used"]);
                o.Purpose = Purpose;
                return o;
            }
            else
                return null;
        }

        public string GetSeqNoString(OSerialNum os)
        {
            StringBuilder sb = new StringBuilder();


            int ReturnSeq = 0;
            sb.Append(" UPDATE sequence_maintain SET ");

            string NowPrefix = "";
            try
            {
                NowPrefix = DateTime.Now.ToString(os.Prefix);
            }
            catch (FormatException)
            {
                throw new Exception("Prefix格式不合");
            }

            if (NowPrefix != os.Current)
            {
                if (os.Purpose == "Reservation")
                    ReturnSeq = 6001;
                else
                    ReturnSeq = 1;
                sb.Append(" Current = '" + NowPrefix + "',");
                sb.Append(" Used = " + ReturnSeq.ToString() + " ");
            }
            else
            {
                ReturnSeq = os.Used + 1;
                sb.Append("Used = " + ReturnSeq.ToString() + " ");
            }
            sb.Append(" Where Purpose = @p ");
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("@p", os.Purpose));
            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._connectstring))
                {
                    conn.executeUpdateQuery(sb.ToString(), parms.ToArray());
                }
            }
            catch (MySqlException)
            {
                throw new Exception("更新序號失敗");
            }
            os.Used = ReturnSeq;
            return ReturnSeq.ToString().PadLeft(os.Digit, '0');
        }
    }

    public class BALCallCarSMS : CallCarBAL
    {
        public BALCallCarSMS(string Connectionstring) : base(Connectionstring) { }

        public bool AddNewSMS(string ReservationNo, string ServiceType, string SelectionType, DateTime FlightTime, string MobilePhone, string CarNo, int DriverID, DateTime ServiceTime)
        {
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();


            sb.Append("INSERT INTO reservation_sms ");
            sb.Append("(ReservationNo, ServiceType, SelectionType, FlightTime, MobilePhone, CarNo, DriverID, ServeTime) ");
            sb.Append("VALUES(@rno, @stype, @seltype, @ftime, @phone, @car, @did, @stime) ");
            parms.Add(new MySqlParameter("@rno", ReservationNo));
            parms.Add(new MySqlParameter("@stype", ServiceType));
            parms.Add(new MySqlParameter("@seltype", SelectionType));
            parms.Add(new MySqlParameter("@ftime", FlightTime));
            parms.Add(new MySqlParameter("@phone", MobilePhone));
            parms.Add(new MySqlParameter("@car", CarNo));
            parms.Add(new MySqlParameter("@did", DriverID));
            parms.Add(new MySqlParameter("@stime", ServiceTime));

            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._connectstring))
                {
                    if (conn.executeInsertQuery(sb.ToString(), parms.ToArray()) <= 0)
                        return false;
                    else
                        return true;
                }
            }
            catch (MySqlException ex)
            {
                throw ex;
            }
        }
    }
}
