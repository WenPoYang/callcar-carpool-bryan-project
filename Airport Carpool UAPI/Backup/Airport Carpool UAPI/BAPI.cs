﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Text;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.ComponentModel;
using System.Configuration;
using System.Net;
using Callcar.BAL;
using Callcar.Struct;
using System.Data;
using Callcar.CObject;
using System.IO;
using HttpUtils;

namespace Airport_Carpool_UAPI
{

    [ServiceContract]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]

    public class BAPI
    {
        public static string ConnectionString = ConfigurationManager.ConnectionStrings["CarpoolLab"].ConnectionString;

        #region 帳號管理
        [Description("帳密驗證  request body：AccountPWStruct轉json")]
        [OperationContract, WebInvoke(UriTemplate = "vAccPW", Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public string VarifyAccountPW(Stream input)
        {
            string data = new StreamReader(input).ReadToEnd();
            RtnStruct r = new RtnStruct();
            try
            {
                BALEmployee bal = new BALEmployee(ConnectionString);
                EmployeInfoStruct eis = bal.ChkAccPWandGetInfoBackWeb(data);
                if (eis != null)
                    r.RtnObject = eis;
                else
                    r.RtnObject = "False";
                r.Status = "Success";
                return JsonConvert.SerializeObject(r);
            }
            catch (Exception ex)
            {
                r.Status = "Error";
                r.RtnObject = ex.StackTrace;
                return JsonConvert.SerializeObject(r);
            }
        }

        [Description("取得帳號清單 request body：ALL / Active / Deactive  回傳資料：EmployeInfoStruct Json")]
        [OperationContract, WebInvoke(UriTemplate = "gAccInfo?status={status}", Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        public string GetAllAccountInfo(string status)
        {
            RtnStruct r = new RtnStruct();

            try
            {
                BALEmployee bal = new BALEmployee(ConnectionString);
                List<OEmployee> List = bal.GetALLEmployee();

                List<EmployeInfoStruct> returnList = new List<EmployeInfoStruct>();
                if (List != null)
                {
                    if (status == "Active")
                        List = List.FindAll(x => x.Active == "1");
                    else if (status == "Deactive")
                        List = List.FindAll(x => x.Active == "0");

                    foreach (OEmployee e in List)
                    {
                        EmployeInfoStruct dr = new EmployeInfoStruct();
                        dr.id = e.ID;
                        dr.empname = e.Name;
                        dr.phone1 = e.MobilePhone;
                        dr.phone2 = e.HomePhone;
                        dr.role = e.Role;
                        dr.company = e.CompanyNo;
                        dr.fleet = e.FleetID.ToString();
                        dr.city = e.City;
                        dr.distinct = e.Distinct;
                        dr.address = e.Address;
                        dr.active = e.Active;
                        returnList.Add(dr);
                    }
                    r.RtnObject = returnList;
                }
                else
                    r.RtnObject = null;
                r.Status = "Success";

                return JsonConvert.SerializeObject(r);
            }
            catch (Exception ex)
            {
                r.Status = "Error";
                r.RtnObject = ex.StackTrace;
                throw new Exception(ex.StackTrace);
            }
        }

        [Description("新增帳號  request body：EmployeInfoStruct轉Json字串")]
        [OperationContract, WebInvoke(UriTemplate = "addEmpAcc", Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public string AddNewEmployee(Stream input)
        {
            string data = new StreamReader(input).ReadToEnd();
            RtnStruct r = new RtnStruct();
            try
            {
                BALEmployee bal = new BALEmployee(ConnectionString);
                int uid = bal.AddNewEmployeeBackWeb(data);
                r.RtnObject = uid;
                r.Status = "Success";

                return JsonConvert.SerializeObject(r);
            }
            catch (Exception ex)
            {
                r.Status = "Error";
                r.RtnObject = ex.Message;
                return JsonConvert.SerializeObject(r);
            }
        }

        [Description("啟用/停用帳號  request body：EmployeeID,OldStatus,NewStatuds   Status：Active/Deactive")]
        [OperationContract, WebInvoke(UriTemplate = "setEmpAct", Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public string SetEmployeeStatus(Stream input)
        {
            string data = new StreamReader(input).ReadToEnd();
            RtnStruct r = new RtnStruct();
            try
            {
                BALEmployee bal = new BALEmployee(ConnectionString);
                r.RtnObject = bal.SetEmployeeStatus(data);
                r.Status = "Success";
                return JsonConvert.SerializeObject(r);
            }
            catch (Exception ex)
            {
                r.Status = "Error";
                r.RtnObject = ex.StackTrace;
                return JsonConvert.SerializeObject(r);
            }
        }
        #endregion

        #region 訂單與派車單管理

        [Description("依條件取得訂單  data為Json(QOrderCondStruct) 回傳為Json(ReservationFullStruct)")]
        [OperationContract, WebInvoke(UriTemplate = "gOrders", Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public string GetOrderByCondition(Stream input)
        {
            RtnStruct r = new RtnStruct();
            try
            {
                string data = new StreamReader(input).ReadToEnd();

                BALReservation bal = new BALReservation(ConnectionString);
                List<OReservation> List = bal.GetReservationByConditions(data);
                if (List == null)
                    r.RtnObject = null;
                else
                {
                    List<ReservationFullStruct> rfsList = new List<ReservationFullStruct>();

                    foreach (OReservation or in List)
                    {
                        ReservationFullStruct rfs = new ReservationFullStruct();
                        rfs.bcnt = or.BaggageCnt;
                        rfs.cdatetime = or.OrderTime.ToString("yyyy/MM/dd HH:mm");
                        rfs.coupun = "";// or.Coupon;
                        rfs.credit = "";// or.Credit;
                        rfs.ein = "";//or.EIN;
                        rfs.fdatetime = "";//or.FlightTime.ToString("yyyy/MM/dd HH:mm");
                        rfs.fno = "";//or.FlightNo;
                        rfs.invoice = "";//or.Invoice;
                        rfs.max = "";//or.MaxFlag;
                        rfs.paddress = or.PickupAddress;
                        rfs.pcartype = "";//or.PickCartype;
                        rfs.pcity = or.PickupCity;
                        rfs.pcnt = or.PassengerCnt;
                        rfs.pdistinct = or.PickupDistinct;
                        rfs.pvillage = "";//or.PickupVillage;
                        rfs.rno = or.ReservationNo;
                        rfs.seltype = "";//or.ChooseType;
                        rfs.sertype = or.ServiceType;
                        rfs.stage = or.ProcessStage;
                        rfs.taddress = or.TakeoffAddress;
                        rfs.tcity = or.TakeoffCity;
                        rfs.tdate = or.ServeDate;
                        rfs.tdistinct = or.TakeoffDistinct;
                        rfs.timeseg = or.PreferTime;
                        rfs.trialprice = 0;//or.TrailPrice;
                        rfs.tvillage = "";//or.TakeoffVillage;
                        rfs.tlat = or.TakeoffGPS.GetLat();
                        rfs.tlng = or.TakeoffGPS.GetLng();
                        rfs.plat = or.PickupGPS.GetLat();
                        rfs.plng = or.PickupGPS.GetLng();
                        rfs.uid = or.UserID;
                        rfs.plat = or.PickupGPS.GetLat();
                        rfs.plng = or.PickupGPS.GetLng();
                        rfs.tlat = or.TakeoffGPS.GetLat();
                        rfs.tlng = or.TakeoffGPS.GetLng();
                        rfsList.Add(rfs);
                    }
                    r.RtnObject = rfsList;
                }
                r.Status = "Success";

                return JsonConvert.SerializeObject(r);
            }
            catch (Exception ex)
            {
                r.Status = "Error";
                r.RtnObject = ex.Message;
                return JsonConvert.SerializeObject(r);
            }

        }

        [Description("依單號取得訂單  回傳Json：ReservationFullStruct")]
        [OperationContract, WebInvoke(UriTemplate = "gOrder", Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public string GetOrderByNo(Stream input)
        {
            RtnStruct r = new RtnStruct();
            try
            {
                string data = new StreamReader(input).ReadToEnd();

                BALReservation bal = new BALReservation(ConnectionString);
                OReservation R = bal.GetReservationByNo(data);
                if (R == null)
                    r.RtnObject = null;
                else
                {
                    ReservationFullStruct rfs = new ReservationFullStruct();
                    rfs.bcnt = R.BaggageCnt;
                    rfs.cdatetime = R.OrderTime.ToString("yyyy/MM/dd HH:mm");
                    rfs.coupun = R.Coupon;
                    rfs.credit = R.Credit;
                    rfs.ein = R.EIN;
                    rfs.fdatetime = R.FlightTime.ToString("yyyy/MM/dd HH:mm");
                    rfs.fno = R.FlightNo;
                    rfs.invoice = R.Invoice;
                    rfs.max = R.MaxFlag;
                    rfs.paddress = R.PickupAddress;
                    rfs.pcartype = R.PickCartype;
                    rfs.pcity = R.PickupCity;
                    rfs.pcnt = R.PassengerCnt;
                    rfs.pdistinct = R.PickupDistinct;
                    rfs.pvillage = R.PickupVillage;
                    rfs.rno = R.ReservationNo;
                    rfs.seltype = R.ChooseType;
                    rfs.sertype = R.ServiceType;
                    rfs.stage = R.ProcessStage;
                    rfs.taddress = R.TakeoffAddress;
                    rfs.tcity = R.TakeoffCity;
                    rfs.tdate = R.ServeDate;
                    rfs.tdistinct = R.TakeoffDistinct;
                    rfs.timeseg = R.PreferTime;
                    rfs.trialprice = R.TrailPrice;
                    rfs.tvillage = R.TakeoffVillage;
                    rfs.tlat = R.TakeoffGPS.GetLat();
                    rfs.tlng = R.TakeoffGPS.GetLng();
                    rfs.plat = R.PickupGPS.GetLat();
                    rfs.plng = R.PickupGPS.GetLng();
                    rfs.uid = R.UserID;
                    rfs.plat = R.PickupGPS.GetLat();
                    rfs.plng = R.PickupGPS.GetLng();
                    rfs.tlat = R.TakeoffGPS.GetLat();
                    rfs.tlng = R.TakeoffGPS.GetLng();

                    r.RtnObject = rfs;
                }
                r.Status = "Success";
                return JsonConvert.SerializeObject(r);
            }
            catch (Exception ex)
            {
                r.Status = "Error";
                r.RtnObject = ex.Message;
                return JsonConvert.SerializeObject(r);
            }
        }

        [Description("更新訂單服務地點 request body：UOrderStruct Json")]
        [OperationContract, WebInvoke(UriTemplate = "uOrderPos", Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public string UpdOrder(Stream input)
        {
            string data = new StreamReader(input).ReadToEnd();
            RtnStruct r = new RtnStruct();
            try
            {
                BALReservation bal = new BALReservation(ConnectionString);
                if (bal.UpdServicePosition(data))
                    r.Status = "Success";
                else
                    r.Status = "Failure";
                r.RtnObject = null;

                return JsonConvert.SerializeObject(r);
            }
            catch (Exception ex)
            {
                r.Status = "Error";
                r.RtnObject = ex.StackTrace;
                return JsonConvert.SerializeObject(r);
            }
        }

        //[Description("依日期取得派車單")]
        //[OperationContract, WebInvoke(UriTemplate = "GetDispatchDate?date={date}", Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        //public string GetDispatchByDate(string date)
        //{
        //    RtnStruct r = new RtnStruct();
        //    try
        //    {
        //        BALDispatch bal = new BALDispatch(ConnectionString);
        //        string output = JsonConvert.SerializeObject(r);
        //        return output;
        //    }
        //    catch (Exception ex)
        //    {
        //        r.Status = "Error";
        //        r.RtnObject = ex.StackTrace;
        //        return JsonConvert.SerializeObject(r);
        //    }
        //}

        [Description("依條件取得派車單  data為Json(QDispatchCondStruct) 回傳為Json(DispatchBriefStruct)")]
        [OperationContract, WebInvoke(UriTemplate = "gDispatchs", Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public string GetDispatchByCondition(Stream input)
        {
            RtnStruct r = new RtnStruct();
            try
            {
                string data = new StreamReader(input).ReadToEnd();


                BALDispatch bal = new BALDispatch(ConnectionString);
                BALGeneral balg = new BALGeneral(ConnectionString);

                List<ODispatchSheet> List = bal.GetDispatchByConditions(data);
                if (List == null)
                    r.RtnObject = null;
                else
                {
                    List<DispatchBriefStruct> rfsList = new List<DispatchBriefStruct>();

                    foreach (ODispatchSheet or in List)
                    {
                        DispatchBriefStruct rfs = new DispatchBriefStruct();
                        rfs.bc = or.BaggageCnt;
                        rfs.cno = or.CarNo;
                        if (or.FleetID > 0)
                        {
                            rfs.fid = or.FleetID;
                            rfs.cp = balg.GetFleet(or.FleetID).FleetName;
                        }
                        else
                        {
                            rfs.fid = 0;
                            rfs.cp = "";
                        }
                        rfs.ct = or.CarType;
                        rfs.dno = or.DispatchNo;
                        rfs.dr = or.DriverName;
                        rfs.pc = or.PassengerCnt;
                        rfs.sc = or.ServiceCnt;
                        rfs.st = or.ServiceType;
                        rfs.td = or.TakeDate;
                        rfs.tt = or.TimeSegment;
                        rfs.did = or.DriverID;
                       
                        

                        rfsList.Add(rfs);
                    }
                    r.RtnObject = rfsList;
                }
                r.Status = "Success";

                return JsonConvert.SerializeObject(r);

            }
            catch (Exception ex)
            {
                r.Status = "Error";
                r.RtnObject = ex.Message;
                return JsonConvert.SerializeObject(r);
            }

        }

        [Description("取得派車單服務地點  回傳為Json()")]
        [OperationContract, WebInvoke(UriTemplate = "gDispatchLocation?dno={dno}", Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        public string GetDispatchLocationInfo(string dno)
        {
            RtnStruct r = new RtnStruct();
            try
            {
                BALDispatch bal = new BALDispatch(ConnectionString);
                DispatchLocationsStruct dl = bal.GetDispatchLocationByNo(dno);

                if (dl != null)
                {
                    r.Status = "Success";
                    r.RtnObject = dl;
                }
                else
                {
                    r.Status = "Success";
                    r.RtnObject = null;
                }
                return JsonConvert.SerializeObject(r);
            }
            catch (Exception ex)
            {
                r.Status = "Error";
                r.RtnObject = ex.Message;
                return JsonConvert.SerializeObject(r);
            }
        }

        [Description("取得單一派車單明細  )")]
        [OperationContract, WebInvoke(UriTemplate = "gDispatch", Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public string GetDispatchDetail(Stream input)
        {
            RtnStruct r = new RtnStruct();
            try
            {

                string data = new StreamReader(input).ReadToEnd();

                BALDispatch bal = new BALDispatch(ConnectionString);
                ODispatchSheet dispatch = bal.GetDispatchSheetByNo(data);

                r.RtnObject = dispatch;

                r.Status = "Success";
                return JsonConvert.SerializeObject(r);
            }
            catch (Exception ex)
            {
                r.Status = "Error";
                r.RtnObject = ex.Message;
                return JsonConvert.SerializeObject(r);
            }

        }


        [Description("更新派車 request body：UDispatchCarStruct json ")]
        [OperationContract, WebInvoke(UriTemplate = "uDispatchCar", Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public string updDispatchCarDriver(Stream input)
        {
            string data = new StreamReader(input).ReadToEnd();
            RtnStruct r = new RtnStruct();
            try
            {
                BALDispatch bal = new BALDispatch(ConnectionString);
                bal.UpdDispatchCarDriver(data);

                r.Status = "Success";
                return JsonConvert.SerializeObject(r);
            }
            catch (Exception ex)
            {
                r.Status = "Error";
                r.RtnObject = ex.StackTrace;
                return JsonConvert.SerializeObject(r);
            }
        }

        [Description("取消訂單 request body : 訂單編號　")]
        [OperationContract, WebInvoke(UriTemplate = "cOrder", Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public string CancelOrder(Stream input)
        {
            string no = new StreamReader(input).ReadToEnd();
            RtnStruct r = new RtnStruct();
            try
            {
                BALReservation bal = new BALReservation(ConnectionString);
                string rtn = bal.CancelReservation("B", no);
                if (rtn != "Success")
                {
                    r.Status = "Failure";
                    r.RtnObject = rtn;
                }
                else
                {
                    r.Status = rtn;
                    r.RtnObject = null;
                }
                return JsonConvert.SerializeObject(r);
            }
            catch (Exception ex)
            {
                r.Status = "Error";
                r.RtnObject = ex.StackTrace;
                return JsonConvert.SerializeObject(r);
            }
        }
        #endregion

        #region 管理
        [Description("取得車隊清單(全) ")]
        [OperationContract, WebInvoke(UriTemplate = "gFleets", Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        public string GetFleets()
        {
            RtnStruct r = new RtnStruct();
            try
            {
                BALGeneral bal = new BALGeneral(ConnectionString);
                List<OFleet> List = bal.GetFleets();
                if (List != null)
                {
                    List<FleetListStruct> ListFleet = new List<FleetListStruct>();

                    foreach (OFleet f in List)
                    {
                        FleetListStruct fs = new FleetListStruct();
                        fs.Name = f.FleetName;
                        fs.ID = f.FleetID.ToString();
                        fs.ActiveFlag = f.ActiveFlag;

                        ListFleet.Add(fs);
                    }


                    r.RtnObject = ListFleet;
                }
                else
                    r.RtnObject = null;

                r.Status = "Success";
                return JsonConvert.SerializeObject(r);
            }
            catch (Exception ex)
            {
                r.Status = "Error";
                r.RtnObject = ex.StackTrace;
                return JsonConvert.SerializeObject(r);
            }
        }

        [Description("取得單一車隊明細  fid:車隊編號  回傳：FleetInfoStruct")]
        [OperationContract, WebInvoke(UriTemplate = "gFleet?fid={fid}", Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        public string GetFleet(string fid)
        {
            RtnStruct r = new RtnStruct();
            try
            {
                BALGeneral bal = new BALGeneral(ConnectionString);
                OFleet fleet = bal.GetFleet(Convert.ToInt32(fid));
                if (fleet != null)
                {
                    FleetInfoStruct fs = new FleetInfoStruct();
                    fs.ActiveFlag = fleet.ActiveFlag;
                    fs.CreateTime = fleet.CreateTime;
                    fs.ID = fleet.FleetID.ToString();
                    fs.Name = fleet.FleetName;
                    fs.UpdTime = fleet.UpdTime;

                    r.RtnObject = fs;
                }
                else
                    r.RtnObject = null;

                r.Status = "Success";
                return JsonConvert.SerializeObject(r);
            }
            catch (Exception ex)
            {
                r.Status = "Error";
                r.RtnObject = ex.StackTrace;
                return JsonConvert.SerializeObject(r);
            }
        }


        [Description("取得車隊車輛 fid=車隊編號  回傳Json:CarListStruct")]
        [OperationContract, WebInvoke(UriTemplate = "gCars?fid={fid}", Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        public string getCarsByFleet(string fid)
        {
            RtnStruct r = new RtnStruct();
            try
            {


                BALGeneral bal = new BALGeneral(ConnectionString);
                BALEmployee bale = new BALEmployee(ConnectionString);
                List<OCar> List = bal.GetCars(fid);
                List<CarListStruct> ListCar = new List<CarListStruct>();
                if (List != null)
                {
                    foreach (OCar c in List)
                    {
                        CarListStruct cs = new CarListStruct();
                        cs.CarNo = c.CarNo;
                        cs.DriverName = bale.GetEmployee(c.DriverID).Name;
                        cs.FleetName = bal.GetFleet(c.FleetID).FleetName;
                        cs.ScheduleFlag = c.ScheduleFlag;

                        ListCar.Add(cs);
                    }

                    r.RtnObject = ListCar;
                }
                else
                    r.RtnObject = null;

                r.Status = "Success";

                return JsonConvert.SerializeObject(r);
            }
            catch (Exception ex)
            {
                r.Status = "Error";
                r.RtnObject = ex.StackTrace;
                return JsonConvert.SerializeObject(r);
            }
        }

        [Description("取得車隊司機(未完成) ")]
        [OperationContract, WebInvoke(UriTemplate = "gDrivers", Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public string getDriverByFleet(Stream input)
        {
            RtnStruct r = new RtnStruct();
            try
            {
                string data = new StreamReader(input).ReadToEnd();
                int FleetID;
                if (!Int32.TryParse(data, out FleetID))
                {
                    r.Status = "Error";
                    r.RtnObject = "傳入參數錯誤";
                    return JsonConvert.SerializeObject(r);
                }

                BALEmployee bal = new BALEmployee(ConnectionString);
               List<OEmployee> List =   bal.GetDriverByFleet(FleetID);

               if (List == null)
                   r.RtnObject = null;
               else
                   r.RtnObject = List;

                r.Status = "Success";
                return JsonConvert.SerializeObject(r);
            }
            catch (Exception ex)
            {
                r.Status = "Error";
                r.RtnObject = ex.StackTrace;
                return JsonConvert.SerializeObject(r);
            }
        }


        #endregion

        #region 媒合用的API
        [Description("依日期取媒合來源")]
        [OperationContract, WebInvoke(UriTemplate = "GetMatchSource?date={date}", Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        public string GetMatchSouceByDate(string date)
        {
            RtnStruct r = new RtnStruct();
            try
            {
                BALDispatch bal = new BALDispatch(ConnectionString);
                OMachSourceData List = bal.GetMatchSourceByDate(date);

                r.RtnObject = List;
                r.Status = "Success";
                return JsonConvert.SerializeObject(r);
            }
            catch (Exception ex)
            {
                r.Status = "Error";
                r.RtnObject = ex.StackTrace;
                return JsonConvert.SerializeObject(r);
            }
        }

        [Description("紀錄媒合結果")]
        [OperationContract, WebInvoke(UriTemplate = "addMatchR?date={date}&json={json}", Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        public string StoreMatchResult(string date, string json)
        {
            RtnStruct r = new RtnStruct();
            try
            {
                BALDispatch bal = new BALDispatch(ConnectionString);
                bal.SaveMatchRecord(date, "M", 0, json, "");

                r.Status = "Success";
                r.RtnObject = "{}";
                return JsonConvert.SerializeObject(r);
            }
            catch (Exception ex)
            {
                r.Status = "Error";
                r.RtnObject = ex.StackTrace;
                return JsonConvert.SerializeObject(r);
            }
        }
        #endregion


        #region TEST
#if debug
        [Description("POST Test")]
        [OperationContract, WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public string POST(Stream json)
        {
            string body = new StreamReader(json).ReadToEnd();
            return body;
        }
#endif
        #endregion
    }
}
