﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Text;
using Newtonsoft.Json;
using System.ComponentModel;
using System.Configuration;
using Callcar.BAL;
using Callcar.Struct;
using Callcar.CObject;
using System.Runtime.Serialization;
using System.IO;

namespace Airport_Carpool_UAPI
{
    [ServiceContract]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]



    public class UAPI
    {
        public static string ConnectionString = ConfigurationManager.ConnectionStrings["CarpoolLab"].ConnectionString;
        public static string Pay2GoUrl = ConfigurationManager.ConnectionStrings["Pay2Go"].ConnectionString;
        public static string MerchantID = ConfigurationManager.ConnectionStrings["MerchantID"].ConnectionString;
        public static string Pay2GoHash = ConfigurationManager.ConnectionStrings["Pay2GoHash"].ConnectionString;
        public static string Pay2GoIV = ConfigurationManager.ConnectionStrings["Pay2GoIV"].ConnectionString;
        public static string PickCarURL = "";// ConfigurationManager.ConnectionStrings["PickCar"].ConnectionString;
        public static string FlightStatusAPPID = ConfigurationManager.ConnectionStrings["FlightStatusAPPID"].ConnectionString;
        public static string FlightStatusAPPKEY = ConfigurationManager.ConnectionStrings["FlightStatusAPPKEY"].ConnectionString;
        public static string _iv = "8417728284177282";

        #region 帳號Profile相關
        [Description("FB新增新帳號資料 data:FB資料  ")]
        [OperationContract, WebInvoke(UriTemplate = "GenAccFBN?d={data}", Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        public string addAccFB(string data)
        {
            RtnStruct r = new RtnStruct();
            string token = string.Empty;
            try
            {
                data = DeCrypt(data, out token);
            }
            catch (Exception ex)
            {
                r.Status = "Error";
                r.RtnObject = ex.Message;
                return JsonConvert.SerializeObject(r);
            }
            //string token = Callcar.Common.AESCryptography.TokenAnalysis(ref data);
            try
            {

                // Callcar.Common.AESCryptography aes = new Callcar.Common.AESCryptography(token, _iv);
                //string plantext = aes.Decrypt(data);
                APIInsAccFB js = JsonConvert.DeserializeObject<APIInsAccFB>(data);
                BALUserAccount bal = new BALUserAccount(ConnectionString);
                int id = bal.CreateAccFromFB(js);

                r.Status = "Success";
                if (id > 0)
                    r.RtnObject = id.ToString();
                else
                    r.RtnObject = null;

                return Callcar.Common.General.OutputCrypt(JsonConvert.SerializeObject(r), token);

            }
            catch (Exception ex)
            {
                r.Status = "Error";
                r.RtnObject = ex.Message;
                return Callcar.Common.General.OutputCrypt(JsonConvert.SerializeObject(r), token);
            }
        }

        [Description("用戶自行新增新帳號資料 data:帳戶資料  ")]
        [OperationContract, WebInvoke(UriTemplate = "GenAccAPPN?d={data}", Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        public string addAccAPP(string data)
        {
            RtnStruct r = new RtnStruct();
            string token = string.Empty;
            try
            {
                token = Callcar.Common.AESCryptography.TokenAnalysis(ref data);
            }
            catch (Exception ex)
            {
                r.Status = "Error";
                r.RtnObject = ex.Message;
                return Callcar.Common.General.OutputCrypt(JsonConvert.SerializeObject(r), token);
            }
            try
            {

                BALUserAccount bal = new BALUserAccount(ConnectionString);
                int id = bal.CreateAccFromAPP(data, token);
                r.Status = "Success";

                if (id > 0)
                    r.RtnObject = id.ToString();
                else
                    r.RtnObject = null;

                return Callcar.Common.General.OutputCrypt(JsonConvert.SerializeObject(r), token);

            }
            catch (Exception ex)
            {
                r.Status = "Error";
                r.RtnObject = ex.Message;
                return Callcar.Common.General.OutputCrypt(JsonConvert.SerializeObject(r), token);
            }
        }

        [Description("驗證Email是否存在 data:帳戶資料  ")]
        [OperationContract, WebInvoke(UriTemplate = "ChkAccAPPE?d={data}", Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        public string chkEmailAPP(string data)
        {
            RtnStruct r = new RtnStruct();


            string token = Callcar.Common.AESCryptography.TokenAnalysis(ref data);
            try
            {


                BALUserAccount bal = new BALUserAccount(ConnectionString);
                string Source = bal.ChkEmailExist(data, token);


                r.Status = "Success";
                if (Source != null)
                    r.RtnObject = Source;
                else
                    r.RtnObject = null;

                return Callcar.Common.General.OutputCrypt(JsonConvert.SerializeObject(r), token);

            }
            catch (Exception ex)
            {
                r.Status = "Error";
                r.RtnObject = ex.Message;
                return Callcar.Common.General.OutputCrypt(JsonConvert.SerializeObject(r), token);

            }
        }

        [Description("驗證帳密 data:帳戶資料  ")]
        [OperationContract, WebInvoke(UriTemplate = "ChkAccAPPW?d={data}", Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        public string chkAccPW(string data)
        {
            RtnStruct r = new RtnStruct();
            string token = string.Empty;
            try
            {
                data = DeCrypt(data, out token);
            }
            catch (Exception ex)
            {
                r.Status = "Error";
                r.RtnObject = ex.Message;
                return Callcar.Common.General.OutputCrypt(JsonConvert.SerializeObject(r), token);
            }
            try
            {
                BALUserAccount bal = new BALUserAccount(ConnectionString);
                //string[] str = System.Text.RegularExpressions.Regex.Split(data, "&&&", System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                //string email = str[0];
                //string pw = str[1];

                AccountPWStruct ps = JsonConvert.DeserializeObject<AccountPWStruct>(data);
                string email = ps.acc;
                string pw = ps.pw;

                r.Status = "Success";
                if (bal.ChkAccountPW(email, pw))
                    r.RtnObject = "Success";
                else
                    r.RtnObject = "false";
                return Callcar.Common.General.OutputCrypt(JsonConvert.SerializeObject(r), token);

            }
            catch (Exception ex)
            {
                r.Status = "Error";
                r.RtnObject = ex.Message;
                return Callcar.Common.General.OutputCrypt(JsonConvert.SerializeObject(r), token);

            }
        }

        [Description("用帳號取得會員資料 data:帳戶資料  ")]
        [OperationContract, WebInvoke(UriTemplate = "getAccAPPE?d={data}", Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        public string getAccEmail(string data)
        {
            RtnStruct r = new RtnStruct();
            string token = string.Empty;
            try
            {
                token = Callcar.Common.AESCryptography.TokenAnalysis(ref data);
            }
            catch (Exception ex)
            {
                r.Status = "Error";
                r.RtnObject = ex.Message;
                return Callcar.Common.General.OutputCrypt(JsonConvert.SerializeObject(r), token);
            }
            try
            {
                BALUserAccount bal = new BALUserAccount(ConnectionString);
                OAccount acc = bal.GetAccountFromEmail(data, token);

                r.Status = "Success";
                if (acc != null)
                    r.RtnObject = acc;
                else
                    r.RtnObject = null;
                return Callcar.Common.General.OutputCrypt(JsonConvert.SerializeObject(r), token);

            }
            catch (Exception ex)
            {
                r.Status = "Error";
                r.RtnObject = ex.Message;
                return Callcar.Common.General.OutputCrypt(JsonConvert.SerializeObject(r), token);

            }
        }

        [Description("用FB ID取得會員資料 data:帳戶資料  ")]
        [OperationContract, WebInvoke(UriTemplate = "getAccAPPF?d={data}", Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        public string getAccFB(string data)
        {
            RtnStruct r = new RtnStruct();

            string token = Callcar.Common.AESCryptography.TokenAnalysis(ref data);
            try
            {
                BALUserAccount bal = new BALUserAccount(ConnectionString);
                OAccount acc = bal.GetAccountFromFB(data, token);

                r.Status = "Success";
                if (acc != null)
                    r.RtnObject = acc;
                else
                    r.RtnObject = null;

                return Callcar.Common.General.OutputCrypt(JsonConvert.SerializeObject(r), token);


            }
            catch (Exception ex)
            {
                r.Status = "Error";
                r.RtnObject = ex.Message;
                return Callcar.Common.General.OutputCrypt(JsonConvert.SerializeObject(r), token);

            }
        }

        [Description("用ID取得會員資料")]
        [OperationContract, WebInvoke(UriTemplate = "getAccAPPID", Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public string getAccUID(Stream input)
        {
            RtnStruct r = new RtnStruct();
            string data = new StreamReader(input).ReadToEnd();
            string token = string.Empty;
            try
            {
                data = DeCrypt(data, out token);
            }
            catch (Exception ex)
            {
                r.Status = "Error";
                r.RtnObject = ex.Message;
                return JsonConvert.SerializeObject(r);
            }

            try
            {
                BALUserAccount bal = new BALUserAccount(ConnectionString);
                OAccount acc = bal.GetAccountInfo(Convert.ToInt32(data));

                r.Status = "Success";
                if (acc != null)
                    r.RtnObject = acc;
                else
                    r.RtnObject = null;

                return Callcar.Common.General.OutputCrypt(JsonConvert.SerializeObject(r), token);


            }
            catch (Exception ex)
            {
                r.Status = "Error";
                r.RtnObject = ex.Message;
                return Callcar.Common.General.OutputCrypt(JsonConvert.SerializeObject(r), token);

            }
        }


        #endregion

        #region 更新帳號資料
        [Description("更新密碼")]
        [OperationContract, WebInvoke(UriTemplate = "UpdAccpw", Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public string UpdAccountPW(Stream input)
        {
            RtnStruct r = new RtnStruct();
            string data = new StreamReader(input).ReadToEnd();
            string token = string.Empty;
            try
            {
                data = DeCrypt(data, out token);
            }
            catch (Exception ex)
            {
                r.Status = "Error";
                r.RtnObject = ex.Message;
                return JsonConvert.SerializeObject(r);
            }

            try
            {
                APIUPW json = JsonConvert.DeserializeObject<APIUPW>(data);

                BALUserAccount bal = new BALUserAccount(ConnectionString);

                if (bal.UpdAccountPW(json.Uid, json.old, json.newp))
                    r.Status = "Success";
                else
                    r.Status = "Error";

                r.RtnObject = null;
                return Callcar.Common.General.OutputCrypt(JsonConvert.SerializeObject(r), token);
            }
            catch (Exception ex)
            {
                r.Status = "Error";
                r.RtnObject = ex.Message;
                return Callcar.Common.General.OutputCrypt(JsonConvert.SerializeObject(r), token);
            }
        }

        [Description("更新暱稱 data:資料  ")]
        [OperationContract, WebInvoke(UriTemplate = "UpdAccNN?d={data}", Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        public string UpdAccNickName(string data)
        {
            RtnStruct r = new RtnStruct();
            string token = Callcar.Common.AESCryptography.TokenAnalysis(ref data);
            try
            {

                BALUserAccount bal = new BALUserAccount(ConnectionString);

                if (bal.UpdAccountData(1, data, token))
                    r.Status = "Success";
                else
                    r.Status = "Error";

                r.RtnObject = null;
                return Callcar.Common.General.OutputCrypt(JsonConvert.SerializeObject(r), token);
            }
            catch (Exception ex)
            {
                r.Status = "Error";
                r.RtnObject = ex.Message;
                return Callcar.Common.General.OutputCrypt(JsonConvert.SerializeObject(r), token);
            }
        }

        [Description("更新名 data:資料  ")]
        [OperationContract, WebInvoke(UriTemplate = "UpdAccFN?d={data}", Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        public string UpdAccFirstName(string data)
        {
            RtnStruct r = new RtnStruct();
            string token = Callcar.Common.AESCryptography.TokenAnalysis(ref data);
            try
            {
                BALUserAccount bal = new BALUserAccount(ConnectionString);

                if (bal.UpdAccountData(2, data, token))
                    r.Status = "Success";
                else
                    r.Status = "Error";
                r.RtnObject = null;
                return Callcar.Common.General.OutputCrypt(JsonConvert.SerializeObject(r), token);
            }
            catch (Exception ex)
            {
                r.Status = "Error";
                r.RtnObject = ex.Message;
                return Callcar.Common.General.OutputCrypt(JsonConvert.SerializeObject(r), token);
            }
        }

        [Description("更新姓 data:資料  ")]
        [OperationContract, WebInvoke(UriTemplate = "UpdAccLN?d={data}", Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        public string UpdAccLastName(string data)
        {
            RtnStruct r = new RtnStruct();
            string token = Callcar.Common.AESCryptography.TokenAnalysis(ref data);
            try
            {
                BALUserAccount bal = new BALUserAccount(ConnectionString);

                if (bal.UpdAccountData(3, data, token))
                    r.Status = "Success";
                else
                    r.Status = "Error";
                r.RtnObject = null;
                return Callcar.Common.General.OutputCrypt(JsonConvert.SerializeObject(r), token);
            }
            catch (Exception ex)
            {
                r.Status = "Error";
                r.RtnObject = ex.Message;
                return Callcar.Common.General.OutputCrypt(JsonConvert.SerializeObject(r), token);
            }
        }

        [Description("更新中間名 data:資料  ")]
        [OperationContract, WebInvoke(UriTemplate = "UpdAccMN?d={data}", Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        public string UpdAccMidName(string data)
        {
            RtnStruct r = new RtnStruct();
            string token = Callcar.Common.AESCryptography.TokenAnalysis(ref data);
            try
            {
                BALUserAccount bal = new BALUserAccount(ConnectionString);

                if (bal.UpdAccountData(4, data, token))
                    r.Status = "Success";
                else
                    r.Status = "Error";
                r.RtnObject = null;
                return Callcar.Common.General.OutputCrypt(JsonConvert.SerializeObject(r), token);
            }
            catch (Exception ex)
            {
                r.Status = "Error";
                r.RtnObject = ex.Message;
                return Callcar.Common.General.OutputCrypt(JsonConvert.SerializeObject(r), token);
            }
        }

        [Description("更新性別 data:資料  ")]
        [OperationContract, WebInvoke(UriTemplate = "UpdAccG?d={data}", Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        public string UpdAccGender(string data)
        {
            RtnStruct r = new RtnStruct();
            string token = Callcar.Common.AESCryptography.TokenAnalysis(ref data);
            try
            {
                BALUserAccount bal = new BALUserAccount(ConnectionString);

                if (bal.UpdAccountData(5, data, token))
                    r.Status = "Success";
                else
                    r.Status = "Error";
                r.RtnObject = null;
                return Callcar.Common.General.OutputCrypt(JsonConvert.SerializeObject(r), token);
            }
            catch (Exception ex)
            {
                r.Status = "Error";
                r.RtnObject = ex.Message;
                return Callcar.Common.General.OutputCrypt(JsonConvert.SerializeObject(r), token);
            }
        }

        [Description("更新年齡區間 data:資料  ")]
        [OperationContract, WebInvoke(UriTemplate = "UpdAccAR?d={data}", Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        public string UpdAccAge(string data)
        {
            RtnStruct r = new RtnStruct();
            string token = Callcar.Common.AESCryptography.TokenAnalysis(ref data);
            try
            {
                BALUserAccount bal = new BALUserAccount(ConnectionString);

                if (bal.UpdAccountData(6, data, token))
                    r.Status = "Success";
                else
                    r.Status = "Error";
                r.RtnObject = null;
                return Callcar.Common.General.OutputCrypt(JsonConvert.SerializeObject(r), token);
            }
            catch (Exception ex)
            {
                r.Status = "Error";
                r.RtnObject = ex.Message;
                return Callcar.Common.General.OutputCrypt(JsonConvert.SerializeObject(r), token);
            }
        }
        [Description("更新居住地 data:資料  ")]
        [OperationContract, WebInvoke(UriTemplate = "UpdAccLo?d={data}", Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        public string UpdAccLocal(string data)
        {
            RtnStruct r = new RtnStruct();
            string token = Callcar.Common.AESCryptography.TokenAnalysis(ref data);
            try
            {
                BALUserAccount bal = new BALUserAccount(ConnectionString);

                if (bal.UpdAccountData(7, data, token))
                    r.Status = "Success";
                else
                    r.Status = "Error";
                r.RtnObject = null;
                return Callcar.Common.General.OutputCrypt(JsonConvert.SerializeObject(r), token);
            }
            catch (Exception ex)
            {
                r.Status = "Error";
                r.RtnObject = ex.Message;
                return Callcar.Common.General.OutputCrypt(JsonConvert.SerializeObject(r), token);
            }
        }

        [Description("更新Email data:資料  ")]
        [OperationContract, WebInvoke(UriTemplate = "UpdAccMA?d={data}", Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        public string UpdAccEmail(string data)
        {
            RtnStruct r = new RtnStruct();
            string token = Callcar.Common.AESCryptography.TokenAnalysis(ref data);
            try
            {
                BALUserAccount bal = new BALUserAccount(ConnectionString);

                if (bal.UpdAccountData(8, data, token))
                    r.Status = "Success";
                else
                    r.Status = "Error";
                r.RtnObject = null;
                return Callcar.Common.General.OutputCrypt(JsonConvert.SerializeObject(r), token);
            }
            catch (Exception ex)
            {
                r.Status = "Error";
                r.RtnObject = ex.Message;
                return Callcar.Common.General.OutputCrypt(JsonConvert.SerializeObject(r), token);
            }
        }

        [Description("更新生日 data:資料  ")]
        [OperationContract, WebInvoke(UriTemplate = "UpdAccBD?d={data}", Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        public string UpdAccBirthday(string data)
        {
            RtnStruct r = new RtnStruct();
            string token = Callcar.Common.AESCryptography.TokenAnalysis(ref data);
            try
            {
                BALUserAccount bal = new BALUserAccount(ConnectionString);

                if (bal.UpdAccountData(9, data, token))
                    r.Status = "Success";
                else
                    r.Status = "Error";
                r.RtnObject = null;
                return Callcar.Common.General.OutputCrypt(JsonConvert.SerializeObject(r), token);
            }
            catch (Exception ex)
            {
                r.Status = "Error";
                r.RtnObject = ex.Message;
                return Callcar.Common.General.OutputCrypt(JsonConvert.SerializeObject(r), token);
            }

        }
        [Description("更新手機 data:資料  ")]
        [OperationContract, WebInvoke(UriTemplate = "UpdAccMP?d={data}", Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        public string UpdAccMobilePhone(string data)
        {
            RtnStruct r = new RtnStruct();
            string token = Callcar.Common.AESCryptography.TokenAnalysis(ref data);
            try
            {
                BALUserAccount bal = new BALUserAccount(ConnectionString);

                if (bal.UpdAccountData(10, data, token))
                    r.Status = "Success";
                else
                    r.Status = "Error";
                r.RtnObject = null;
                return Callcar.Common.General.OutputCrypt(JsonConvert.SerializeObject(r), token);
            }
            catch (Exception ex)
            {
                r.Status = "Error";
                r.RtnObject = ex.Message;
                return Callcar.Common.General.OutputCrypt(JsonConvert.SerializeObject(r), token);
            }
        }

        [Description("更新頭貼 data:資料  ")]
        [OperationContract, WebInvoke(UriTemplate = "UpdAccPic?d={data}", Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        public string UpdAccPicUrl(string data)
        {
            RtnStruct r = new RtnStruct();
            string token = Callcar.Common.AESCryptography.TokenAnalysis(ref data);
            try
            {
                BALUserAccount bal = new BALUserAccount(ConnectionString);

                if (bal.UpdAccountData(11, data, token))
                    r.Status = "Success";
                else
                    r.Status = "Error";

                r.RtnObject = null;
                return Callcar.Common.General.OutputCrypt(JsonConvert.SerializeObject(r), token);
            }
            catch (Exception ex)
            {
                r.Status = "Error";
                r.RtnObject = ex.Message;
                return Callcar.Common.General.OutputCrypt(JsonConvert.SerializeObject(r), token);
            }
        }

        [Description("重新發預設密碼 ")]
        [OperationContract, WebInvoke(UriTemplate = "ReNewP", Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public string PublishNewPW(Stream input)
        {
            string data = new StreamReader(input).ReadToEnd();
            RtnStruct r = new RtnStruct();
            string token = string.Empty;
            try
            {
                data = DeCrypt(data, out token);
            }
            catch (Exception)
            {
                r.Status = "Error";
                r.RtnObject = "參數解析錯誤";
                return JsonConvert.SerializeObject(r);
            }
            try
            {
                int UserID = Convert.ToInt32(data.Split(',')[0]);
                string Email = data.Split(',')[1];

                BALUserAccount bal = new BALUserAccount(ConnectionString);
                string rtn = bal.RenewAccountRandomPW(UserID, Email);

                if (rtn == "Success")
                    r.Status = "Success";
                else
                    r.Status = "Error";

                r.RtnObject = null;
                return Callcar.Common.General.OutputCrypt(JsonConvert.SerializeObject(r), token);
            }
            catch (Exception ex)
            {
                r.Status = "Error";
                r.RtnObject = ex.Message;
                return Callcar.Common.General.OutputCrypt(JsonConvert.SerializeObject(r), token);
            }
        }
        #endregion

        #region 發票作業
        [Description("新增一筆新發票資料 data:發票資料  ")]
        [OperationContract, WebInvoke(UriTemplate = "InvAPPN?d={data}", Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        public string CreateNewInvoice(string data)
        {
            RtnStruct r = new RtnStruct();
            string token = Callcar.Common.AESCryptography.TokenAnalysis(ref data);
            try
            {

                BALInvoice bal = new BALInvoice(ConnectionString);
                int InvoiceID = bal.CreateNewInvoice(data, token);
                if (InvoiceID > 0)
                {
                    r.Status = "Success";
                    r.RtnObject = InvoiceID;
                }
                else
                {
                    r.Status = "Error";
                    r.RtnObject = null;
                }
                return Callcar.Common.General.OutputCrypt(JsonConvert.SerializeObject(r), token);
            }
            catch (Exception ex)
            {
                r.Status = "Error";
                r.RtnObject = ex.Message;
                return Callcar.Common.General.OutputCrypt(JsonConvert.SerializeObject(r), token);
            }
        }

        [Description("移除一筆新發票資料 data:發票資料  ")]
        [OperationContract, WebInvoke(UriTemplate = "InvAPPR?d={data}", Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        public string RemoveInvoice(string data)
        {
            RtnStruct r = new RtnStruct();
            string token = string.Empty;
            try
            {
                data = DeCrypt(data, out token);
            }
            catch (Exception)
            {
                r.Status = "Error";
                r.RtnObject = "參數解析錯誤";
                return JsonConvert.SerializeObject(r);
            }
            try
            {
                BALInvoice bal = new BALInvoice(ConnectionString);
                int InvoiceID;
                int UserID;
                InvoiceID = Int32.Parse(data.Split(',')[0]);
                UserID = Int32.Parse(data.Split(',')[1]);
                if (bal.RemoveInvoice(InvoiceID, UserID))
                    r.Status = "Success";
                else
                    r.Status = "Error";

                r.RtnObject = null;
                return Callcar.Common.General.OutputCrypt(JsonConvert.SerializeObject(r), token);
            }
            catch (Exception ex)
            {
                r.Status = "Error";
                r.RtnObject = ex.Message;
                return Callcar.Common.General.OutputCrypt(JsonConvert.SerializeObject(r), token);
            }
        }

        //[Description("查詢所有發票資料 data:發票資料  ")]
        //[OperationContract, WebInvoke(UriTemplate = "InvAPPQA?d={data}", Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        //public string QryAllInvoice(string data)
        //{
        //    RtnStruct r = new RtnStruct();
        //    string token = Callcar.Common.AESCryptography.TokenAnalysis(ref data);
        //    try
        //    {
        //        BALInvoice bal = new BALInvoice(ConnectionString);
        //        List<OInvoice> List = bal.SelectUserAllInvoice(data, token);


        //        if (List != null)
        //        {
        //            List<APIInvoiceOutput> outList = new List<APIInvoiceOutput>();
        //            foreach (OInvoice i in List)
        //            {
        //                APIInvoiceOutput o = new APIInvoiceOutput();
        //                o.address = i.InvoiceAddress;
        //                o.ein = i.EIN;
        //                o.flag = i.DefaultFlag;
        //                o.iid = i.InvoiceID;
        //                o.type = i.InvoiceType;
        //                o.uid = i.UserID;
        //                outList.Add(o);
        //            }

        //            r.RtnObject = outList;
        //        }
        //        else
        //            r.RtnObject = null;

        //        r.Status = "Success";
        //        return Callcar.Common.General.OutputCrypt(JsonConvert.SerializeObject(r), token);
        //    }
        //    catch (Exception ex)
        //    {
        //        r.Status = "Error";
        //        r.RtnObject = ex.Message;
        //        return Callcar.Common.General.OutputCrypt(JsonConvert.SerializeObject(r), token);
        //    }
        //}

        [Description("查詢單一發票資料 data:發票資料  ")]
        [OperationContract, WebInvoke(UriTemplate = "InvAPPQU?d={data}", Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        public string QryUniqueInvoice(string data)
        {
            RtnStruct r = new RtnStruct();
            string token = Callcar.Common.AESCryptography.TokenAnalysis(ref data);
            try
            {
                BALInvoice bal = new BALInvoice(ConnectionString);
                OInvoice i = bal.GetUserInvoice(data, token);
                if (i != null)
                {

                    APIInvoiceOutput o = new APIInvoiceOutput();
                    o.address = i.InvoiceAddress;
                    o.ein = i.EIN;
                    o.iid = i.SeqNo;
                    o.uid = i.UserID;


                    r.RtnObject = o;
                }
                else
                    r.RtnObject = null;

                r.Status = "Success";
                return Callcar.Common.General.OutputCrypt(JsonConvert.SerializeObject(r), token);
            }
            catch (Exception ex)
            {
                r.Status = "Error";
                r.RtnObject = ex.Message;
                return Callcar.Common.General.OutputCrypt(JsonConvert.SerializeObject(r), token);
            }
        }

        //[Description("設定預設發票 data:發票資料  ")]
        //[OperationContract, WebInvoke(UriTemplate = "InvAPPS?d={data}", Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        //public string SetDefaultInvoice(string data)
        //{
        //    RtnStruct r = new RtnStruct();
        //    string token = Callcar.Common.AESCryptography.TokenAnalysis(ref data);
        //    try
        //    {
        //        BALInvoice bal = new BALInvoice(ConnectionString);

        //        if (bal.SetDefaultInvoice(data, token))
        //            r.Status = "Success";
        //        else
        //            r.Status = "Error";
        //        r.RtnObject = null;
        //        return Callcar.Common.General.OutputCrypt(JsonConvert.SerializeObject(r), token);
        //    }
        //    catch (Exception ex)
        //    {
        //        r.Status = "Error";
        //        r.RtnObject = ex.Message;
        //        return Callcar.Common.General.OutputCrypt(JsonConvert.SerializeObject(r), token);
        //    }
        //}
        #endregion

        #region 信用卡
        [Description("新增一筆信用卡資料 data:信用卡資料  ")]
        [OperationContract, WebInvoke(UriTemplate = "CardAPPN", Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public string CreateNewCard(Stream input)
        {
            string data = new StreamReader(input).ReadToEnd();
            RtnStruct r = new RtnStruct();
            string token = string.Empty;
            try
            {
                data = DeCrypt(data, out token);
            }
            catch (Exception)
            {
                r.Status = "Error";
                r.RtnObject = "參數解析錯誤";
                return JsonConvert.SerializeObject(r);
            }
            try
            {
                APINewCard ic = JsonConvert.DeserializeObject<APINewCard>(data);
                int no;
                BALCreditCard bal = new BALCreditCard(ConnectionString);
                no = bal.CreateNewCard(ic, Pay2GoUrl, MerchantID, Pay2GoHash, Pay2GoIV);
                if (no > 0)
                {
                    r.Status = "Success";
                    r.RtnObject = no;
                }
                else
                {
                    r.Status = "Error";
                    r.RtnObject = null;
                }
                return Callcar.Common.General.OutputCrypt(JsonConvert.SerializeObject(r), token);
            }
            catch (Exception ex)
            {
                r.Status = "Error";
                r.RtnObject = ex.Message;
                return Callcar.Common.General.OutputCrypt(JsonConvert.SerializeObject(r), token);
            }
        }

        [Description("移除一筆信用卡資料 data:信用卡資料  ")]
        [OperationContract, WebInvoke(UriTemplate = "CardAPPR?d={data}", Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        public string RemoveCard(string data)
        {
            RtnStruct r = new RtnStruct();
            string token = string.Empty;
            try
            {
                data = DeCrypt(data, out token);
            }
            catch (Exception)
            {
                r.Status = "Error";
                r.RtnObject = "參數解析錯誤";
                return JsonConvert.SerializeObject(r);
            }
            try
            {
                int CardID;
                int UserID;
                CardID = Convert.ToInt32(data.Split(',')[0]);
                UserID = Convert.ToInt32(data.Split(',')[1]);
                BALCreditCard bal = new BALCreditCard(ConnectionString);
                if (bal.RemoveCard(CardID, UserID))
                    r.Status = "Success";
                else
                    r.Status = "Error";
                r.RtnObject = null;
                return Callcar.Common.General.OutputCrypt(JsonConvert.SerializeObject(r), token);
            }
            catch (Exception ex)
            {
                r.Status = "Error";
                r.RtnObject = ex.Message;
                return Callcar.Common.General.OutputCrypt(JsonConvert.SerializeObject(r), token);
            }
        }

        [Description("取得一筆信用卡資料 data:信用卡資料")]
        [OperationContract, WebInvoke(UriTemplate = "gCardAPPU?d={data}", Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        public string GetUniqueCard(string data)
        {

            RtnStruct r = new RtnStruct();
            string token = string.Empty;
            try
            {
                data = DeCrypt(data, out token);
            }
            catch (Exception)
            {
                r.Status = "Error";
                r.RtnObject = "參數解析錯誤";
                return JsonConvert.SerializeObject(r);
            }

            try
            {
                int UserID = Convert.ToInt32(data);
                BALCreditCard bal = new BALCreditCard(ConnectionString);
                OCard i = bal.GetUserActiveCard(UserID);
                if (i != null)
                {
                    i.CNo = i.CNo.Substring(0, 4) + "-****-****-" + i.CNo.Substring(12, 4);
                    i.Code = "***";
                    i.Expire = "****";
                    i.d = "";
                    i.t = "";

                    r.RtnObject = i;
                }
                else
                    r.RtnObject = null;

                r.Status = "Success";
                return Callcar.Common.General.OutputCrypt(JsonConvert.SerializeObject(r), token);
            }
            catch (Exception ex)
            {
                r.Status = "Error";
                r.RtnObject = ex.Message;
                return Callcar.Common.General.OutputCrypt(JsonConvert.SerializeObject(r), token);
            }
        }
        #endregion

        #region 訂單
        [Description("APP產生訂單2")]
        [OperationContract, WebInvoke(UriTemplate = "GenOrderAPP2", Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public string CreateNewOrder2(Stream input)
        {
            string data = new StreamReader(input).ReadToEnd();
            RtnStruct r = new RtnStruct();
            string token = string.Empty;
            try
            {
                data = DeCrypt(data, out token);
            }
            catch (Exception ex)
            {
                r.Status = "Error";
                r.RtnObject = ex.Message;
                return JsonConvert.SerializeObject(r);
            }
            try
            {
                APINewOrder os = JsonConvert.DeserializeObject<APINewOrder>(data);

                if (os.uid == "1524496131" || os.uid == "1511713466")
                {
                    r.Status = "Error";
                    r.RtnObject = "VIP客戶 請聯絡客服";
                    return JsonConvert.SerializeObject(r);
                }
                if (os.airport == null || os.airport == "" || os.airport == "null")
                    os.airport = "TPE";

                if (os.airport == "TPE")
                {
                    if (os.terminal == null || os.terminal == "" || os.terminal == "null")
                    {
                        if (os.stype == "O")
                        {
                            if (os.taddress.Contains("第二航廈"))
                                os.terminal = "T2";
                            else if (os.taddress.Contains("第一航廈"))
                                os.terminal = "T1";
                            else if (os.taddress.Contains("第三航廈"))
                                os.terminal = "T3";
                        }
                        else if (os.stype == "I")
                        {
                            if (os.paddress.Contains("第二航廈"))
                                os.terminal = "T2";
                            else if (os.paddress.Contains("第一航廈"))
                                os.terminal = "T1";
                            else if (os.paddress.Contains("第三航廈"))
                                os.terminal = "T3";
                        }
                    }

                    if (os.stype == "O" && os.terminal == "T2")
                    {
                        os.tlat = "25.076838";
                        os.tlng = "121.232336";
                    }
                    else if (os.stype == "I" && os.terminal == "T2")
                    {
                        os.plat = "25.076838";
                        os.plng = "121.232336";
                    }
                    else if (os.stype == "O" && os.terminal == "T1")
                    {
                        os.tlat = "25.081056";
                        os.tlng = "121.237585";
                    }
                    else if (os.stype == "I" && os.terminal == "T1")
                    {
                        os.plat = "25.081056";
                        os.plng = "121.237585";
                    }
                }

                //2017-08-25 由於APP端資料丟反了 API端 先除錯
                string temp;
                if (os.stype == "O")
                {

                    if (os.pvillage.Trim() == "台北市" || os.pvillage.Trim() == "新北市" || os.pvillage.Trim() == "桃園市")
                    {
                        temp = os.pvillage;
                        os.pvillage = os.pcity;
                        os.pcity = temp;
                    }
                }
                else
                {
                    if (os.tvillage.Trim() == "台北市" || os.tvillage.Trim() == "新北市" || os.tvillage.Trim() == "桃園市")
                    {
                        temp = os.tvillage;
                        os.tvillage = os.tcity;
                        os.tcity = temp;
                    }
                }

                ////檢查優惠碼的正確性
                //if (os.coupon != null && os.coupon.Trim() != "")
                //{
                //    int price = Int32.Parse(os.pcnt) * 389;
                //    if (Int32.Parse(os.pcnt) < Int32.Parse(os.bcnt))
                //        price += (Int32.Parse(os.bcnt) - Int32.Parse(os.pcnt)) * 100;

                //    if (os.price == price)
                //        os.coupon = "";
                //}


                string rno;
                BALReservation bal = new BALReservation(ConnectionString);
                rno = bal.CreateNewReservation(os);
                if (rno == null || rno == "")
                {
                    r.Status = "Error";
                    r.RtnObject = null;
                    return Callcar.Common.General.OutputCrypt(JsonConvert.SerializeObject(r), token);
                }
                //寄送確認信
                //改從程式寄送
                //try
                //{
                //    BALUserAccount balu = new BALUserAccount(ConnectionString);
                //    OAccount oa = balu.GetAccountInfo(Convert.ToInt32(os.uid));
                //    SendReservationEmail("NEW", os.sdate, os.ptime, os.stype, os.paddress, os.taddress, os.ctype, os.fno, Int32.Parse(os.pcnt), Int32.Parse(os.bcnt), os.pname, os.pmobile, rno, oa.Email);
                //    //SendReservationEmail(os, rno, oa.Email);
                //}
                //catch (Exception)
                //{

                //}

                r.Status = "Success";
                r.RtnObject = rno;
                return Callcar.Common.General.OutputCrypt(JsonConvert.SerializeObject(r), token);
            }
            catch (Exception ex)
            {
                r.Status = "Error";
                r.RtnObject = ex.Message;
                return Callcar.Common.General.OutputCrypt(JsonConvert.SerializeObject(r), token);
            }
        }

        [Description("取得該區/里單筆訂單最低預約乘客數量")]
        [OperationContract, WebInvoke(UriTemplate = "gMinPAPP?d={data}", Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        public string GetMinPassenger(string data)
        {
            RtnStruct r = new RtnStruct();
            string token = string.Empty;
            try
            {
                data = DeCrypt(data, out token);
            }
            catch (Exception ex)
            {
                r.Status = "Error";
                r.RtnObject = ex.Message;
                return Callcar.Common.General.OutputCrypt(JsonConvert.SerializeObject(r), token);
            }
            try
            {
                string CityName = data.Split(',')[0];
                if (CityName == "台北市" || CityName == "臺北市")
                    CityName = "台北市";
                string DistinctName = data.Split(',')[1];
                string VillageName = data.Split(',')[2];
                BALGeneral bal = new BALGeneral(ConnectionString);
                int cnt;
                cnt = bal.GetMinServicePassenger(CityName, DistinctName, VillageName);
                if (cnt == -1)
                    r.RtnObject = null;
                else
                    r.RtnObject = cnt;
                r.Status = "Success";
                return Callcar.Common.General.OutputCrypt(JsonConvert.SerializeObject(r), token);
            }
            catch (Exception ex)
            {
                r.Status = "Error";
                r.RtnObject = ex.Message;
                return Callcar.Common.General.OutputCrypt(JsonConvert.SerializeObject(r), token);
            }
        }

        [Description("取消訂單")]
        [OperationContract, WebInvoke(UriTemplate = "cOrder", Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public string CancelOrder(Stream input)
        {
            string data = new StreamReader(input).ReadToEnd();

            RtnStruct r = new RtnStruct();
            string token = string.Empty;
            try
            {
                data = DeCrypt(data, out token);
            }
            catch (Exception ex)
            {
                r.Status = "Error";
                r.RtnObject = ex.Message;
                return JsonConvert.SerializeObject(r);
            }

            try
            {
                BALReservation bal = new BALReservation(ConnectionString);
                OReservation or = bal.GetReservationByNo(data);
                BALUserAccount balu = new BALUserAccount(ConnectionString);
                OAccount oa = balu.GetAccountInfo(or.UserID);

                string rtn = bal.CancelReservation("U", data);
                r.Status = rtn;
                r.RtnObject = null;

                //if (oa.ID == 1492783805 || oa.ID == 1496734211)
                //{
                //    try
                //    {
                //        SendReservationEmail("CANCEL", or.ServeDate, or.PreferTime, or.ServiceType, or.PickupAddress, or.TakeoffAddress, or.ChooseType, or.FlightNo, or.PassengerCnt, or.BaggageCnt, or.PassengerName, or.PassengerPhone, or.ReservationNo, oa.Email);
                //    }
                //    catch (Exception)
                //    {

                //    }
                //}

                return Callcar.Common.General.OutputCrypt(JsonConvert.SerializeObject(r), token);
            }
            catch (Exception ex)
            {
                r.Status = "Error";
                r.RtnObject = ex.StackTrace;
                return Callcar.Common.General.OutputCrypt(JsonConvert.SerializeObject(r), token);
            }
        }


        [Description("預約查詢")]
        [OperationContract, WebInvoke(UriTemplate = "qUFOrder", Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public string GetUserFeatureOrders(Stream input)
        {
            RtnStruct r = new RtnStruct();
            string data = new StreamReader(input).ReadToEnd();
            string token = string.Empty;

            try
            {
                data = DeCrypt(data, out token);
            }
            catch (Exception ex)
            {
                r.Status = "Error";
                r.RtnObject = ex.Message;
                return JsonConvert.SerializeObject(r);
            }
            try
            {
                BALReservation bal = new BALReservation(ConnectionString);
                int UserID;
                List<OReservation> List = new List<OReservation>();
                List<APIQOrder> ListQ = new List<APIQOrder>();
                Callcar.DAL.DALReservation dal = new Callcar.DAL.DALReservation(ConnectionString);

                if (Int32.TryParse(data, out UserID))
                {
                    List = bal.GetReservationsByUser(UserID);
                    if (List != null)
                    {
                        foreach (OReservation or in List)
                        {
                            if (or.ProcessStage == "X" || or.ProcessStage == "W" || or.ProcessStage == "Y")
                                continue;
                            DateTime TakeDate = Callcar.Common.General.ConvertToDateTime(or.ServeDate, 8);
                            DateTime Today = DateTime.Now.Date;
                            if (TakeDate.Date >= Today.Date)
                            {
                                APIQOrder o = new APIQOrder();
                                if (or.ServiceType == "I")
                                    o.add = or.TakeoffAddress;
                                else
                                    o.add = or.PickupAddress;

                                o.bcnt = or.BaggageCnt;
                                o.io = or.ServiceType;
                                o.pcnt = or.PassengerCnt;
                                o.td = or.ServeDate;
                                o.tt = or.PreferTime;
                                string Info = dal.GetOrderDispatchData(or.ReservationNo);
                                if (Info != "")
                                {
                                    string[] Dispatch = Info.Split(',');
                                    o.st = Dispatch[0];
                                    o.cartype = Dispatch[1];
                                    o.carno = Dispatch[2];
                                    o.driver = Dispatch[3].Trim();
                                    if (o.driver != "")
                                        o.driver = o.driver.Substring(0, 1) + "先生";
                                    o.phone = Dispatch[4];
                                }
                                else
                                {
                                    o.st = "";
                                    o.cartype = "";
                                    o.carno = "";
                                    o.driver = "";
                                    o.phone = "";
                                }
                                o.rno = or.ReservationNo;

                                ListQ.Add(o);
                            }
                        }

                    }

                    r.RtnObject = ListQ;
                    r.Status = "Success";
                }
                else
                {
                    r.Status = "Error";
                    r.RtnObject = "參數錯誤";
                }
                return Callcar.Common.General.OutputCrypt(JsonConvert.SerializeObject(r), token);
            }
            catch (Exception ex)
            {
                r.Status = "Error";
                r.RtnObject = ex.Message;
                return Callcar.Common.General.OutputCrypt(JsonConvert.SerializeObject(r), token);

            }
        }


        [Description("歷史紀錄查詢")]
        [OperationContract, WebInvoke(UriTemplate = "qUPOrder", Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public string GetUserPassOrders(Stream input)
        {
            RtnStruct r = new RtnStruct();
            string data = new StreamReader(input).ReadToEnd();
            string token = string.Empty;

            try
            {
                data = DeCrypt(data, out token);
            }
            catch (Exception ex)
            {
                r.Status = "Error";
                r.RtnObject = ex.Message;
                return JsonConvert.SerializeObject(r);
            }
            try
            {
                BALReservation bal = new BALReservation(ConnectionString);
                int UserID;
                List<OReservation> List = new List<OReservation>();
                List<APIQOrderPast> ListQ = new List<APIQOrderPast>();
                Callcar.DAL.DALReservation dal = new Callcar.DAL.DALReservation(ConnectionString);

                if (Int32.TryParse(data, out UserID))
                {
                    List = bal.GetReservationsByUser(UserID);
                    if (List != null)
                    {
                        foreach (OReservation or in List)
                        {
                            if (or.ProcessStage == "X" || or.ProcessStage == "W" || or.ProcessStage == "Y")
                                continue;
                            DateTime TakeDate = Callcar.Common.General.ConvertToDateTime(or.ServeDate, 8);
                            DateTime Today = DateTime.Now.Date;
                            if (TakeDate.Date < Today.Date)
                            {
                                APIQOrderPast o = new APIQOrderPast();
                                if (or.ServiceType == "I")
                                    o.add = or.TakeoffAddress;
                                else
                                    o.add = or.PickupAddress;

                                o.bcnt = or.BaggageCnt;
                                o.io = or.ServiceType;
                                o.pcnt = or.PassengerCnt;
                                o.td = or.ServeDate;
                                string Info = dal.GetOrderDispatchData(or.ReservationNo);
                                if (Info != "")
                                {
                                    string[] Dispatch = Info.Split(',');
                                    o.st = Dispatch[0];
                                    o.carno = Dispatch[2];
                                    o.driver = Dispatch[3].Trim();
                                    if (o.driver != "")
                                        o.driver = o.driver.Substring(0, 1) + "先生";
                                }
                                else
                                {
                                    o.st = "";
                                    o.carno = "";
                                    o.driver = "";
                                }
                                o.comment = "尚未寫入";
                                o.score = 0.0;
                                o.rno = or.ReservationNo;

                                ListQ.Add(o);
                            }
                        }
                        r.RtnObject = ListQ;
                    }
                    else
                        r.RtnObject = null;

                    r.Status = "Success";
                }
                else
                {
                    r.Status = "Error";
                    r.RtnObject = "參數錯誤";
                }
                return Callcar.Common.General.OutputCrypt(JsonConvert.SerializeObject(r), token);
            }
            catch (Exception ex)
            {
                r.Status = "Error";
                r.RtnObject = ex.Message;
                return Callcar.Common.General.OutputCrypt(JsonConvert.SerializeObject(r), token);

            }
        }

        [Description("浮動費率查詢")]
        [OperationContract, WebInvoke(UriTemplate = "qOFPrice", Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public string GetOrderFloatPrice(Stream input)
        {
            RtnStruct r = new RtnStruct();
            string data = new StreamReader(input).ReadToEnd();


            string token = string.Empty;

            try
            {
                data = DeCrypt(data, out token);
            }
            catch (Exception ex)
            {
                r.Status = "Error";
                r.RtnObject = ex.Message;
                return JsonConvert.SerializeObject(r);
            }

            try
            {
                BALReservation bal = new BALReservation(ConnectionString);
                Dictionary<DateTime, int> Price = bal.GetFloatingPriceByOrder(data);

                if (Price.Count == 0)
                    r.RtnObject = null;

                OrderPriceHisotryStruct ph = new OrderPriceHisotryStruct();
                ph.Rno = data;
                foreach (var item in Price)
                {
                    OrderPriceHisotryStruct.FloatingPrice fp = new OrderPriceHisotryStruct.FloatingPrice();
                    fp.DateTime = item.Key.ToString("yyyy/MM/dd HH:mm");
                    fp.Price = item.Value;
                    ph.Price.Add(fp);
                }

                r.RtnObject = ph;
                r.Status = "Success";

                return Callcar.Common.General.OutputCrypt(JsonConvert.SerializeObject(r), token);

            }
            catch (Exception ex)
            {
                r.Status = "Error";
                r.RtnObject = ex.Message;

                return Callcar.Common.General.OutputCrypt(JsonConvert.SerializeObject(r), token);


            }
        }
        #endregion

        #region 帳務
        [Description("取得試算金額")]
        [OperationContract, WebInvoke(UriTemplate = "gTrailFAPP?d={data}", Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        public string GetTrailFee(string data)
        {
            RtnStruct r = new RtnStruct();
            string token = string.Empty;
            try
            {
                data = DeCrypt(data, out token);
            }
            catch (Exception ex)
            {
                r.Status = "Error";
                r.RtnObject = ex.Message;
                return JsonConvert.SerializeObject(r);
            }

            try
            {

                APITrailFee f = JsonConvert.DeserializeObject<APITrailFee>(data);
                BALGeneral bal = new BALGeneral(ConnectionString);

                //訂車時不會有addBag
                OPrice op = new OPrice();
                //string rFee = bal.CalculateTrailFee(f.od, f.td, f.tt, f.pc, f.bc, f.cp, f.dno, 0, true, f.tt, 0);
                op = bal.CalculateTrailFee(f.od, f.td, f.tt, f.pc, f.bc, f.cp, f.dno, 0, true, f.tt, 0);
                //string[] Fee = rFee.Split('_');
                r.Status = "Success";

                r.RtnObject = op.CarFee + "_" + op.NightFee + "_" + op.BaggageFee + "_" + op.PromoteAmt;//rFee; //Int32.Parse(Fee[0]) + Int32.Parse(Fee[1]) + Int32.Parse(Fee[2]) - Int32.Parse(Fee[3]);

                return Callcar.Common.General.OutputCrypt(JsonConvert.SerializeObject(r), token);
            }
            catch (Exception ex)
            {
                r.Status = "Error";
                r.RtnObject = ex.Message;
                return Callcar.Common.General.OutputCrypt(JsonConvert.SerializeObject(r), token);
            }
        }

        [Description("驗證優惠碼")]
        [OperationContract, WebInvoke(UriTemplate = "vCoupon", Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public string VarifyCouponCode(Stream input)
        {
            RtnStruct r = new RtnStruct();
            string data = new StreamReader(input).ReadToEnd();
            string token = string.Empty;

            try
            {
                data = DeCrypt(data, out token);
            }
            catch (Exception)
            {
                r.Status = "Error";
                r.RtnObject = "傳入資料驗證錯誤";
                return JsonConvert.SerializeObject(r);
            }
            try
            {
                string Code, TakeDate;
                Code = data.Split(',')[0];
                TakeDate = data.Split(',')[1];
                BALGeneral bal = new BALGeneral(ConnectionString);
                if (bal.VarifyCoupon(Code, TakeDate, DateTime.Now.ToString("yyyyMMdd")))
                    r.RtnObject = "true";
                else
                    r.RtnObject = "false";
                r.Status = "Success";

                return Callcar.Common.General.OutputCrypt(JsonConvert.SerializeObject(r), token);
            }
            catch (Exception ex)
            {
                r.Status = "Error";
                r.RtnObject = ex.Message;
                return Callcar.Common.General.OutputCrypt(JsonConvert.SerializeObject(r), token);
            }
        }

        #endregion

        #region 趟次資料來源
        //[Description("取得該乘車日的車輛、時段(舊版 使用本地運算 小時制)")]
        //[OperationContract, WebInvoke(UriTemplate = "gTimePickCarAPP", Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        //public string GetTimeCarsInfo(Stream input)
        //{
        //    RtnStruct r = new RtnStruct();
        //    string token = string.Empty;
        //    string data = new StreamReader(input).ReadToEnd();
        //    try
        //    {
        //        data = DeCrypt(data, out token);
        //    }
        //    catch (Exception)
        //    {
        //        r.Status = "Error";
        //        r.RtnObject = "傳入字串異常";
        //        return JsonConvert.SerializeObject(r);
        //    }

        //    try
        //    {

        //        APIGCarTime jQuery = JsonConvert.DeserializeObject<APIGCarTime>(data);
        //        BALPickCar bal = new BALPickCar(ConnectionString);
        //        OPickCarTime oInfo = bal.GetPickCarTimeInfo(jQuery.ServiceType, jQuery.TakeDate, jQuery.FlightDate, jQuery.FlightTime, jQuery.SelectionType, jQuery.PassengerCnt, jQuery.BaggageCnt, jQuery.lat, jQuery.lng);

        //        APIPickCarTime rData = new APIPickCarTime();
        //        rData.TimeRange = oInfo.TimeRange;
        //        foreach (OPickCarInfo c in oInfo.PickCars)
        //        {
        //            APIPickCarTime.PCarInfo p = new APIPickCarTime.PCarInfo();
        //            p.cn = c.CarNo;
        //            p.ct = c.CarType;
        //            p.dn = c.DriverName;
        //            p.dno = c.DispatchNo;
        //            p.pt = c.ShowServiceTime.ToString("HH:mm");
        //            p.rbc = c.RemainBCapacity;
        //            p.rpc = c.RemainPCapacity;
        //            p.sc = c.StopCnt;
        //            p.st = c.ServiceType;
        //            p.td = c.TakeDate;
        //            p.tr = c.TimeRange;

        //            if (c.CarType == "四人座")
        //                p.cte = 4;
        //            else
        //                p.cte = 7;
        //            rData.PickCars.Add(p);
        //        }
        //        r.Status = "Success";
        //        r.RtnObject = rData;
        //        return Callcar.Common.General.OutputCrypt(JsonConvert.SerializeObject(r), token);
        //    }
        //    catch (Exception ex)
        //    {
        //        return Callcar.Common.General.OutputCrypt(JsonConvert.SerializeObject(ex.Message), token);
        //    }
        //}

        [Description("取得該乘車日的車輛、時段(New 使用司圖運算 15分鐘制)")]
        [OperationContract, WebInvoke(UriTemplate = "gTimePickCarAPP2", Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public string GetTimeCarsInfo2(Stream input)
        {
            RtnStruct r = new RtnStruct();
            string token = string.Empty;
            string data = new StreamReader(input).ReadToEnd();
            try
            {
                data = DeCrypt(data, out token);
            }
            catch (Exception)
            {
                r.Status = "Error";
                r.RtnObject = "傳入字串異常";
                return JsonConvert.SerializeObject(r);
            }

            try
            {

                APIGCarTime jQuery = JsonConvert.DeserializeObject<APIGCarTime>(data);
                BALPickCar bal = new BALPickCar(ConnectionString);

                double Pickuplat = 0, Pickuplng = 0, Takeofflat = 0, Takeofflng = 0;
                if (jQuery.ServiceType == "O")
                {
                    Pickuplat = jQuery.lat;
                    Pickuplng = jQuery.lng;

                    if (jQuery.airport == "TPE" && jQuery.terminal == "T1")
                    {
                        Takeofflat = 25.081056;
                        Takeofflng = 121.237585;
                    }
                    else if (jQuery.airport == "TPE" && jQuery.terminal == "T2")
                    {
                        Takeofflat = 25.076838;
                        Takeofflng = 121.232336;
                    }
                }
                else
                {
                    Takeofflat = jQuery.lat;
                    Takeofflng = jQuery.lng;

                    if (jQuery.airport == "TPE" && jQuery.terminal == "T1")
                    {
                        Pickuplat = 25.081056;
                        Pickuplng = 121.237585;
                    }
                    else if (jQuery.airport == "TPE" && jQuery.terminal == "T2")
                    {
                        Pickuplat = 25.076838;
                        Pickuplng = 121.232336;
                    }
                }

                OPickCarTime oInfo = bal.GetPickCarTimeInfo2(jQuery.ServiceType, jQuery.TakeDate, jQuery.FlightDate, jQuery.FlightTime, jQuery.SelectionType, jQuery.PassengerCnt, jQuery.BaggageCnt, Pickuplat, Pickuplng, Takeofflat, Takeofflng, PickCarURL, 30);

                APIPickCarTime rData = new APIPickCarTime();
                rData.TimeRange = oInfo.TimeRange;
                foreach (OPickCarInfo c in oInfo.PickCars)
                {
                    APIPickCarTime.PCarInfo p = new APIPickCarTime.PCarInfo();
                    p.cn = c.CarNo;
                    p.ct = c.CarType;
                    p.dn = c.DriverName;
                    p.dno = c.DispatchNo;
                    p.pt = c.ShowServiceTime.ToString("HH:mm");
                    p.rbc = c.RemainBCapacity;
                    p.rpc = c.RemainPCapacity;
                    p.sc = c.StopCnt;
                    p.st = c.ServiceType;
                    p.td = c.TakeDate;
                    p.tr = c.TimeRange;

                    if (c.CarType == "四人座")
                        p.cte = 4;
                    else
                        p.cte = 7;
                    rData.PickCars.Add(p);
                }
                r.Status = "Success";
                r.RtnObject = rData;
                return Callcar.Common.General.OutputCrypt(JsonConvert.SerializeObject(r), token);
            }
            catch (Exception ex)
            {
                return Callcar.Common.General.OutputCrypt(JsonConvert.SerializeObject(ex.Message), token);
            }
        }

        [Description("取得該乘車日是預約或撿車模式)")]
        [OperationContract, WebInvoke(UriTemplate = "gDPMode", Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public string getDatePickMode(Stream input)
        {
            RtnStruct r = new RtnStruct();
            string token = string.Empty;
            string data = new StreamReader(input).ReadToEnd();
            try
            {
                data = DeCrypt(data, out token);
            }
            catch (Exception)
            {
                r.Status = "Error";
                r.RtnObject = "傳入字串異常";
                return JsonConvert.SerializeObject(r);
            }

            try
            {
                Callcar.DAL.DALDispatch dald = new Callcar.DAL.DALDispatch(ConnectionString);
                if (dald.isMatchDateExist(data))
                    r.RtnObject = "Pick";
                else
                    r.RtnObject = "nPick";

                r.Status = "Success";

                return Callcar.Common.General.OutputCrypt(JsonConvert.SerializeObject(r), token);
            }
            catch (Exception ex)
            {
                r.Status = "Error";
                r.RtnObject = ex.Message;
                return Callcar.Common.General.OutputCrypt(JsonConvert.SerializeObject(r), token);
            }
        }
        #endregion

        #region 航班相關

        [Description("查詢航班資料 no：航班編號(ex.CI121) date：飛航日期(yyyy-MM-dd)")]
        [OperationContract, WebInvoke(UriTemplate = "gFlight?d={data}", Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        public string QryFlightInfo(string data)
        {

            RtnStruct r = new RtnStruct();
            string token = string.Empty;
            try
            {
                data = DeCrypt(data, out token);
            }
            catch (Exception)
            {
                r.Status = "Error";
                r.RtnObject = "參數解析錯誤";
                return JsonConvert.SerializeObject(r);
            }
            try
            {

                string FlightNo, FlightDate;
                FlightNo = data.Split(',')[0];
                FlightDate = data.Split(',')[1];

                BALFlight bal = new BALFlight(ConnectionString);

                APIFlightStruct fs = bal.GetFlightInfo(FlightNo, FlightDate);

                r.Status = "Success";

                if (fs != null)
                    r.RtnObject = fs;
                else
                    r.RtnObject = null;

                return Callcar.Common.General.OutputCrypt(JsonConvert.SerializeObject(r), token);
            }
            catch (Exception ex)
            {
                r.Status = "Error";
                r.RtnObject = ex.Message;
                return Callcar.Common.General.OutputCrypt(JsonConvert.SerializeObject(r), token);
            }
        }

        [Description("查詢航班資料 no：航班編號(ex.CI121) date：飛航日期(yyyy-MM-dd)")]
        [OperationContract, WebInvoke(UriTemplate = "gFlight2?d={data}", Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        public string QryFlightInfo2(string data)
        {

            RtnStruct r = new RtnStruct();
            string token = string.Empty;
            try
            {
                data = DeCrypt(data, out token);
            }
            catch (Exception)
            {
                r.Status = "Error";
                r.RtnObject = "參數解析錯誤";
                return JsonConvert.SerializeObject(r);
            }
            try
            {

                string FlightNo, FlightDate, ServiceType;
                FlightNo = data.Split(',')[0];
                FlightDate = data.Split(',')[1];
                ServiceType = data.Split(',')[2];

                BALFlight bal = new BALFlight(ConnectionString);
                //APIFlightStruct fs = bal.GetFlightInfo(ServiceType, FlightNo, FlightDate);
                APIFlightStruct fs = bal.GetFlightInfoAPI(FlightStatusAPPID, FlightStatusAPPKEY, ServiceType, FlightNo, FlightDate);
                r.Status = "Success";

                if (fs != null)
                    r.RtnObject = fs;
                else
                    r.RtnObject = null;

                return Callcar.Common.General.OutputCrypt(JsonConvert.SerializeObject(r), token);
            }
            catch (Exception ex)
            {
                r.Status = "Error";
                r.RtnObject = ex.Message;
                return Callcar.Common.General.OutputCrypt(JsonConvert.SerializeObject(r), token);
            }
        }

        #endregion

        //[Description("查詢版本編號Android")]
        //[OperationContract, WebInvoke(UriTemplate = "gVerA", Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        //public string GetVerisonAndroid()
        //{
        //    RtnStruct r = new RtnStruct();

        //    try
        //    {
        //        BALGeneral bal = new BALGeneral(ConnectionString);
        //        r.RtnObject = bal.GetAppVersion("CallcarUAPP", "Android");
        //        r.Status = "Success";

        //        return JsonConvert.SerializeObject(r);
        //    }
        //    catch (Exception ex)
        //    {
        //        r.Status = "Error";
        //        r.RtnObject = ex.Message;
        //        return JsonConvert.SerializeObject(r);
        //    }
        //}

        //[Description("查詢版本編號iOS")]
        //[OperationContract, WebInvoke(UriTemplate = "gVerI", Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        //public string GetVerisonIOS()
        //{
        //    RtnStruct r = new RtnStruct();

        //    try
        //    {
        //        BALGeneral bal = new BALGeneral(ConnectionString);
        //        r.RtnObject = bal.GetAppVersion("CallcarUAPP", "iOS");
        //        r.Status = "Success";

        //        return JsonConvert.SerializeObject(r);
        //    }
        //    catch (Exception ex)
        //    {
        //        r.Status = "Error";
        //        r.RtnObject = ex.Message;
        //        return JsonConvert.SerializeObject(r);
        //    }
        //}

        private string DeCrypt(string CryptedText, out string token)
        {
            string data = CryptedText;
            try
            {
                token = Callcar.Common.AESCryptography.TokenAnalysis(ref data);
                Callcar.Common.AESCryptography aes = new Callcar.Common.AESCryptography(token, _iv);
                return aes.Decrypt(data);
            }
            catch (Exception)
            {
                throw new Exception("密文錯誤");
            }
        }

        //private void SendReservationEmail(APINewOrder os, string ReservationNo, string Email)
        private void SendReservationEmail(string EmailType, string TakeDate, string PrefreTime, string ServiceType, string PickupAddress, string TakeoffAddress, string ChooseType, string FlightNo, int Pcnt, int Bcnt, string PName, string PMobile, string ReservationNo, string Email)
        {
            if (Email == "www@www")
                return;
            string HtmlBody = "";
            string filepath = "";
            if (EmailType == "NEW")
                filepath = System.Web.HttpContext.Current.Server.MapPath("../template/callcar_email_zhtw.html");
            else
                filepath = System.Web.HttpContext.Current.Server.MapPath("../template/callcar_cancel_email_zhtw.html");
            try
            {
                if (File.Exists(filepath))
                {
                    //讀取檔案
                    using (StreamReader streamReader = new StreamReader(filepath, Encoding.GetEncoding("utf-8")))
                    {
                        //轉字串給輸出
                        HtmlBody = streamReader.ReadToEnd();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            // {0} 訂單編號
            // {1} 乘車日期
            // {2} 預約乘車時間
            // {3} 上下車
            // {4} 服務地址
            // {5} 出回國
            // {6} 航班編號
            // {7} 機場資訊
            // {8} 乘車人數
            // {9} 行李數
            // {10} 連絡人姓名
            // {11} 聯絡人手機
            try
            {
                HtmlBody = HtmlBody.Replace("{0}", ReservationNo);
                HtmlBody = HtmlBody.Replace("{1}", TakeDate.Substring(0, 4) + "-" + TakeDate.Substring(4, 2) + "-" + TakeDate.Substring(6, 2));
                HtmlBody = HtmlBody.Replace("{2}", PrefreTime.Substring(0, 2) + "：" + PrefreTime.Substring(2, 2));
                HtmlBody = HtmlBody.Replace("{3}", (ServiceType == "O") ? "上車" : "下車");
                HtmlBody = HtmlBody.Replace("{4}", (ServiceType == "O") ? PickupAddress : PickupAddress);
                HtmlBody = HtmlBody.Replace("{5}", (ServiceType == "O") ? "送機" : "接機");
                HtmlBody = HtmlBody.Replace("{6}", (ChooseType == "F") ? FlightNo : "");
                HtmlBody = HtmlBody.Replace("{7}", (ServiceType == "O") ? TakeoffAddress : TakeoffAddress);
                HtmlBody = HtmlBody.Replace("{8}", Pcnt.ToString());
                HtmlBody = HtmlBody.Replace("{9}", Bcnt.ToString());
                HtmlBody = HtmlBody.Replace("{10}", PName);
                HtmlBody = HtmlBody.Replace("{11}", PMobile);

                Callcar.Common.GmailSMTP smtp = new Callcar.Common.GmailSMTP("support@ishareco.com", "share52439256b");

                string Subject;

                if (EmailType == "NEW")
                    Subject = "CallCar機場共乘 訂單確認信";
                else
                    Subject = "CallCar機場共乘 取消訂單確認信";
                List<string> MailAddress = new List<string>();
                MailAddress.Add(Email);


                smtp.Send(MailAddress, "艾雪科技客服通知", Subject, HtmlBody);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region 測試
        [Description("測試加密 {\"text\":\"明文\",\"Key\":\"key\"} ")]
        [OperationContract, WebInvoke(UriTemplate = "cryp", Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public string TestCry(Stream input)
        {
            string data = new StreamReader(input).ReadToEnd();
            RtnStruct r = new RtnStruct();
            OCrypto oc = JsonConvert.DeserializeObject<OCrypto>(data);
            try
            {
                Callcar.Common.AESCryptography cry = new Callcar.Common.AESCryptography(oc.Key, _iv);
                return cry.Encrypt(oc.text);
            }
            catch (Exception ex)
            {
                return ex.StackTrace;
            }
        }
        [Description("測試解密 {\"text\":\"密文\",\"Key\":\"key\"}")]
        [OperationContract, WebInvoke(UriTemplate = "decryp", Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public string TestDecry(Stream input)
        {
            string data = new StreamReader(input).ReadToEnd();
            RtnStruct r = new RtnStruct();
            OCrypto oc = JsonConvert.DeserializeObject<OCrypto>(data);
            try
            {
                Callcar.Common.AESCryptography cry = new Callcar.Common.AESCryptography(oc.Key, _iv);
                return cry.Decrypt(oc.text);
            }
            catch (Exception ex)
            {
                return ex.StackTrace;
            }
        }

        [Description("測試")]
        [OperationContract, WebInvoke(UriTemplate = "Test?d={data}", Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public string Test(string data)
        {
            RtnStruct r = new RtnStruct();
            // string token = string.Empty;
            //try
            //{
            //    data = DeCrypt(data, out token);
            //}
            //catch (Exception)
            //{
            //    r.Status = "Error";
            //    r.RtnObject = "參數解析錯誤";
            //    return JsonConvert.SerializeObject(r);
            //}
            try
            {

                string FlightNo, FlightDate, ServiceType;
                FlightNo = data.Split(',')[0];
                FlightDate = data.Split(',')[1];
                ServiceType = data.Split(',')[2];

                BALFlight bal = new BALFlight(ConnectionString);
                //APIFlightStruct fs = bal.GetFlightInfo(ServiceType, FlightNo, FlightDate);
                APIFlightStruct fs = bal.GetFlightInfoAPI(FlightStatusAPPID, FlightStatusAPPKEY, ServiceType, FlightNo, FlightDate);
                r.Status = "Success";

                if (fs != null)
                    r.RtnObject = fs;
                else
                    r.RtnObject = null;

                return JsonConvert.SerializeObject(r);
            }
            catch (Exception ex)
            {
                r.Status = "Error";
                r.RtnObject = ex.Message;
                return JsonConvert.SerializeObject(r);
            }
        }


        public class OCrypto
        {
            public string text { get; set; }
            public string Key { get; set; }
        }

        #endregion

        #region JsonStruct
        public class APIUPW
        {
            public int Uid { get; set; }
            public string old { get; set; }
            public string newp { get; set; }
        }

        public class APIGCarTime
        {
            public string ServiceType { get; set; }
            public string TakeDate { get; set; }
            public string FlightDate { get; set; }
            public string FlightTime { get; set; }
            public string SelectionType { get; set; }
            public int PassengerCnt { get; set; }
            public int BaggageCnt { get; set; }
            public double lat { get; set; }
            public double lng { get; set; }
            public string airport { get; set; } //機場代碼
            public string terminal { get; set; } //航廈
        }

        public class APIPickCarTime
        {
            public APIPickCarTime()
            {
                this.TimeRange = new List<string>();
                this.PickCars = new List<PCarInfo>();
            }
            public List<string> TimeRange { get; set; }
            public List<PCarInfo> PickCars { get; set; }

            public class PCarInfo
            {
                public string dno { get; set; } //派車單號
                public string td { get; set; } //搭乘日
                public string tr { get; set; } //時間區間
                public string st { get; set; } //出回國       
                public int rpc { get; set; } //剩餘乘客空間
                public int rbc { get; set; } //剩餘行李空間
                public int sc { get; set; } //乘客組數
                public string ct { get; set; } //車種
                public string cn { get; set; } //車牌        
                public string dn { get; set; } //司機名稱
                public string pt { get; set; } //服務時間
                public string max { get; set; } //滿車註記
                public int cte { get; set; } //車種英文
            }
        }

        public class APIQOrder
        {
            public string rno { get; set; }
            public string io { get; set; }
            public string td { get; set; }
            public string tt { get; set; }
            public string st { get; set; }
            public int pcnt { get; set; }
            public int bcnt { get; set; }
            public string add { get; set; }
            public string carno { get; set; }
            public string cartype { get; set; }
            public string driver { get; set; }
            public string phone { get; set; }
        }

        public class APIQOrderPast
        {
            public string rno { get; set; }
            public string io { get; set; }
            public string td { get; set; }
            public string st { get; set; }
            public int pcnt { get; set; }
            public int bcnt { get; set; }
            public string add { get; set; }
            public string comment { get; set; }
            public double score { get; set; }
            public string carno { get; set; }
            public string driver { get; set; }
        }

        public class OrderPriceHisotryStruct
        {
            public OrderPriceHisotryStruct()
            {
                Price = new List<FloatingPrice>();
            }

            public string Rno { get; set; }
            public List<FloatingPrice> Price { get; set; }

            public class FloatingPrice
            {
                public string DateTime { get; set; }
                public int Price { get; set; }
            }
        }


        public class TestStruct
        {
            public string field1 { get; set; }
            public string field2 { get; set; }

            [DataMember(IsRequired = false)]
            public string fieldnull { get; set; }
        }
        #endregion
    }
}
