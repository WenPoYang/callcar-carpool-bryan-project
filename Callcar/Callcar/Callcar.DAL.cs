﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MySql.Data.MySqlClient;
using DbConn;
using System.Data;
using Callcar.CObject;
using Callcar.Struct;
using Newtonsoft.Json;
//using Dapper;
using Callcar.DataModel;
using System.Transactions;

namespace Callcar.DAL
{
    public class CallcarDAL
    {
        protected string _ConnectionString = string.Empty;
        public CallcarDAL(string conn)
        {
            this._ConnectionString = conn;
        }
    }
    /// <summary>
    /// 內部人員的DAL
    /// </summary>
    public class DALEmployee : CallcarDAL
    {

        public DALEmployee(string conn)
            : base(conn)
        {
        }

        public int AddAccount(string pw, string Name, string cid, string phone1, string phone2, string role, string cno, string fno, string city, string distinct, string address, string Email)
        {

            StringBuilder sb = new StringBuilder();
            sb.Append("INSERT INTO user_info ( Password, UserName, CitizenID, MobilePhone, HomePhone, UserRole, CompanyNo,FleetID, City, DistinctName, Address,Email)");
            sb.Append("VALUES ( @Password,@UserName , @CitizenID, @MobilePhone, @HomePhone, @UserRole,@CompanyNo, @FleetID , @City, @Distinct, @Address,@Email)");
            List<MySqlParameter> parms = new List<MySqlParameter>();

            parms.Add(new MySqlParameter("@Password", Callcar.Common.MD5Cryptography.Encrypt(pw)));
            parms.Add(new MySqlParameter("@UserName", Name));
            parms.Add(new MySqlParameter("@CitizenID", cid));
            parms.Add(new MySqlParameter("@MobilePhone", phone1));
            parms.Add(new MySqlParameter("@HomePhone", phone2));
            parms.Add(new MySqlParameter("@CompanyNo", cno));
            parms.Add(new MySqlParameter("@UserRole", role));
            parms.Add(new MySqlParameter("@FleetID", fno));
            parms.Add(new MySqlParameter("@City", city));
            parms.Add(new MySqlParameter("@Distinct", distinct));
            parms.Add(new MySqlParameter("@Address", address));
            parms.Add(new MySqlParameter("@Email", Email));

            using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
            {
                try
                {
                    conn.executeInsertQuery(sb.ToString(), parms.ToArray());
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
                parms.Clear();
                sb.Length = 0;
                sb.Append("SELECT LAST_INSERT_ID()");
                DataTable dt = new DataTable();
                try
                {
                    dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());

                    return Convert.ToInt32(dt.Rows[0][0]);

                }
                catch (MySqlException ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }

        public string GetPassword(int EmployeeID)
        {
            try
            {


                StringBuilder sb = new StringBuilder();
                DataTable dt = new DataTable();
                DateTime today = DateTime.Now;

                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    sb.Append("select Password from user_info  where UserID = @ID ");
                    List<MySqlParameter> parms = new List<MySqlParameter>();
                    parms.Add(new MySqlParameter("@ID", EmployeeID));

                    dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                }
                if (dt.Rows.Count > 0)
                    return Convert.ToString(dt.Rows[0]["Password"]);
                else
                    return null;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public OEmployee GetEmployeeInfoByID(int ID)
        {
            OEmployee emp = new OEmployee();
            try
            {


                StringBuilder sb = new StringBuilder();
                DataTable dt = new DataTable();
                DateTime today = DateTime.Now;

                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    sb.Append("select UserID, UserName,CitizenID, MobilePhone, HomePhone, UserRole,CompanyNo, FleetID, City, DistinctName, Address,ActiveFlag,Email  from user_info  where UserID = @id  And ActiveFlag = true ");
                    List<MySqlParameter> parms = new List<MySqlParameter>();
                    parms.Add(new MySqlParameter("@id", ID));

                    dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                }
                if (dt.Rows.Count > 0)
                {
                    emp.ID = Convert.ToInt32(dt.Rows[0]["UserID"]);
                    emp.Name = Convert.ToString(dt.Rows[0]["UserName"]);
                    emp.MobilePhone = Convert.ToString(dt.Rows[0]["MobilePhone"]);
                    emp.HomePhone = Convert.ToString(dt.Rows[0]["HomePhone"]);
                    emp.Role = Convert.ToString(dt.Rows[0]["UserRole"]);
                    emp.CompanyNo = Convert.ToString(dt.Rows[0]["CompanyNo"]);
                    emp.FleetID = Convert.ToInt32(dt.Rows[0]["FleetID"]);
                    emp.City = Convert.ToString(dt.Rows[0]["City"]);
                    emp.Distinct = Convert.ToString(dt.Rows[0]["DistinctName"]);
                    emp.Address = Convert.ToString(dt.Rows[0]["Address"]);
                    emp.Active = Convert.ToString(dt.Rows[0]["ActiveFlag"]);
                    emp.Email = Convert.ToString(dt.Rows[0]["Email"]);
                    emp.CID = Convert.ToString(dt.Rows[0]["CitizenID"]);

                    return emp;
                }
                else
                    return null;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<OEmployee> GetEmployeeInfo()
        {


            StringBuilder sb = new StringBuilder();
            DataTable dt = new DataTable();
            List<OEmployee> empList = new List<OEmployee>();
            sb.Append("select UserID, UserName, MobilePhone,CitizenID, HomePhone, UserRole,CompanyNo, FleetID, City, DistinctName, Address,ActiveFlag,Email  from user_info   ");
            List<MySqlParameter> parms = new List<MySqlParameter>();
            try
            {

                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {

                    dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    OEmployee emp = new OEmployee();
                    emp.ID = Convert.ToInt32(dt.Rows[i]["UserID"]);
                    emp.Name = Convert.ToString(dt.Rows[i]["UserName"]);
                    emp.MobilePhone = Convert.ToString(dt.Rows[i]["MobilePhone"]);
                    emp.HomePhone = Convert.ToString(dt.Rows[i]["HomePhone"]);
                    emp.Role = Convert.ToString(dt.Rows[i]["UserRole"]);
                    emp.CompanyNo = Convert.ToString(dt.Rows[i]["CompanyNo"]);
                    emp.FleetID = Convert.ToInt32(dt.Rows[i]["FleetID"]);
                    emp.City = Convert.ToString(dt.Rows[i]["City"]);
                    emp.Distinct = Convert.ToString(dt.Rows[i]["DistinctName"]);
                    emp.Address = Convert.ToString(dt.Rows[i]["Address"]);
                    emp.Active = Convert.ToString(dt.Rows[i]["ActiveFlag"]);
                    emp.Email = Convert.ToString(dt.Rows[i]["Email"]);
                    emp.CID = Convert.ToString(dt.Rows[i]["CitizenID"]);

                    empList.Add(emp);
                }
            }
            else
                return null;

            return empList;

        }

        public List<OEmployee> GetEmployeeByCompany(string CompanyNo)
        {
            List<OEmployee> List = new List<OEmployee>();
            DataTable dt = new DataTable();

            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            sb.Append("SELECT UserID, UserName, CitizenID, MobilePhone, HomePhone, UserRole, CompanyNo, FleetID, ShiftType, City, DistinctName, Address, ActiveFlag, CreateTime, UpdTime,Email ");
            sb.Append("FROM user_info ");
            sb.Append("where CompanyNo = @no");

            parms.Add(new MySqlParameter("@no", CompanyNo));

            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    OEmployee emp = new OEmployee();
                    emp.ID = Convert.ToInt32(dt.Rows[i]["UserID"]);
                    emp.Name = Convert.ToString(dt.Rows[i]["UserName"]);
                    emp.MobilePhone = Convert.ToString(dt.Rows[i]["MobilePhone"]);
                    emp.HomePhone = Convert.ToString(dt.Rows[i]["HomePhone"]);
                    emp.Role = Convert.ToString(dt.Rows[i]["UserRole"]);
                    emp.CompanyNo = Convert.ToString(dt.Rows[i]["CompanyNo"]);
                    emp.FleetID = Convert.ToInt32(dt.Rows[i]["FleetID"]);
                    emp.City = Convert.ToString(dt.Rows[i]["City"]);
                    emp.Distinct = Convert.ToString(dt.Rows[i]["DistinctName"]);
                    emp.Address = Convert.ToString(dt.Rows[i]["Address"]);
                    emp.Active = Convert.ToString(dt.Rows[i]["ActiveFlag"]);
                    emp.Email = Convert.ToString(dt.Rows[i]["Email"]);
                    emp.CID = Convert.ToString(dt.Rows[i]["CitizenID"]);

                    List.Add(emp);
                }

                return List;
            }
            else
                return null;
        }

        public string SetEmployeStatus(int uid, string oldStatus, string newStatus)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("update user_info set activeflag = @new where UserID = @UserID and activeflag = @old");
            List<MySqlParameter> parms = new List<MySqlParameter>();

            parms.Add(new MySqlParameter("@UserID", uid));
            parms.Add(new MySqlParameter("@new", newStatus));
            parms.Add(new MySqlParameter("@old", oldStatus));

            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    if (conn.executeUpdateQuery(sb.ToString(), parms.ToArray()) > 0)
                        return newStatus;
                    else
                        throw new Exception("Update Error");
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.StackTrace);
            }
        }

        public int GetEmployeeIDByEmail(string Email)
        {
            OEmployee emp = new OEmployee();
            try
            {
                StringBuilder sb = new StringBuilder();
                DataTable dt = new DataTable();
                DateTime today = DateTime.Now;

                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    sb.Append("select UserID From user_info where Email = @mail ");
                    List<MySqlParameter> parms = new List<MySqlParameter>();
                    parms.Add(new MySqlParameter("@mail", Email));

                    dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                }
                if (dt.Rows.Count > 0)
                {
                    return Convert.ToInt32(dt.Rows[0]["UserID"]);
                }
                else
                    return 0;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public int GetEmployeeIDByCitizenID(string CitizenID)
        {
            OEmployee emp = new OEmployee();
            try
            {
                StringBuilder sb = new StringBuilder();
                DataTable dt = new DataTable();
                DateTime today = DateTime.Now;

                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    sb.Append("select UserID FROM user_info where CitizenID = @ID ");
                    List<MySqlParameter> parms = new List<MySqlParameter>();
                    parms.Add(new MySqlParameter("@ID", CitizenID));

                    dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                }
                if (dt.Rows.Count > 0)
                {
                    return Convert.ToInt32(dt.Rows[0]["UserID"]);
                }
                else
                    return 0;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }


    /// <summary>
    /// 會員帳戶的DAL
    /// </summary>
    public class DALUserAccount : CallcarDAL
    {
        public DALUserAccount(string ConnectionString)
            : base(ConnectionString)
        {
        }

        public int AddAccount(string Source, string SourceID, string Email, string FName, string LName, string MName, int Gender, string MobilePhone, string NickName, string Password)
        {
            int id = 0;
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            sb.Append("INSERT INTO account ");
            sb.Append("(UserID,Source,SourceID,Email,NickName,FirstName,LastName,MidName,Gender,MobilePhone,Password) ");
            sb.Append("VALUES ");
            sb.Append("(@UserID,@Source,@SourceID,@Email,@NickName,@FirstName,@LastName,@MidName,@Gender,@mphone,@Password)  ");

            id = (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
            parms.Add(new MySqlParameter("@UserID", id));
            parms.Add(new MySqlParameter("@Source", Source));
            parms.Add(new MySqlParameter("@SourceID", SourceID));
            parms.Add(new MySqlParameter("@Email", Email));
            parms.Add(new MySqlParameter("@NickName", NickName));
            parms.Add(new MySqlParameter("@FirstName", FName));
            parms.Add(new MySqlParameter("@LastName", LName));
            parms.Add(new MySqlParameter("@MidName", MName));
            parms.Add(new MySqlParameter("@Gender", Gender));
            parms.Add(new MySqlParameter("@mphone", MobilePhone));
            parms.Add(new MySqlParameter("@Password", Password));

            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    if (conn.executeInsertQuery(sb.ToString(), parms.ToArray()) > 0)
                        return id;
                    else
                        return 0;
                }
            }
            catch (Exception ex)
            {
                //  trn.Rollback();
                throw new Exception(ex.Message);
            }


        }

        public bool UpdAccountByColumn(int id, string Column, string newValue)
        {
            try
            {

                StringBuilder sb = new StringBuilder();
                sb.Append("Update account ");
                sb.Append("Set " + Column + " = @" + Column + " ");
                sb.Append("Where UserID = @UserID");
                List<MySqlParameter> parms = new List<MySqlParameter>();
                parms.Add(new MySqlParameter("@UserID", id));
                parms.Add(new MySqlParameter("@" + Column, newValue));
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    if (conn.executeUpdateQuery(sb.ToString(), parms.ToArray()) > 0)
                        return true;
                    else
                        return false;
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public string GetAccountPW(int UserID)
        {
            try
            {
                StringBuilder sb = new StringBuilder();
                DataTable dt = new DataTable();
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    sb.Append("select Password from account  where UserID = @id ");
                    List<MySqlParameter> parms = new List<MySqlParameter>();
                    parms.Add(new MySqlParameter("@id", UserID));

                    dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                }
                if (dt.Rows.Count > 0)
                {
                    return Convert.ToString(dt.Rows[0]["Password"]);
                }
                else
                    return "";
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public OAccount GetAccountInfo(int UserID)
        {
            OAccount acc = new OAccount();
            try
            {


                StringBuilder sb = new StringBuilder();
                DataTable dt = new DataTable();
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    sb.Append("select UserID, Source, SourceID,  NickName, FirstName, LastName, MidName, Gender, AgeRange, Local, Email, Birthday, MobilePhone from account  where UserID = @id ");
                    List<MySqlParameter> parms = new List<MySqlParameter>();
                    parms.Add(new MySqlParameter("@id", UserID));

                    dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                }
                if (dt.Rows.Count > 0)
                {
                    acc.ID = Convert.ToInt32(dt.Rows[0]["UserID"]);
                    acc.Source = Convert.ToString(dt.Rows[0]["Source"]);
                    acc.SourceID = Convert.ToString(dt.Rows[0]["SourceID"]);
                    acc.NickName = Convert.ToString(dt.Rows[0]["NickName"]);
                    acc.FName = Convert.ToString(dt.Rows[0]["FirstName"]);
                    acc.LName = Convert.ToString(dt.Rows[0]["LastName"]);
                    acc.MName = Convert.ToString(dt.Rows[0]["MidName"]);
                    acc.Gender = Convert.ToInt32(dt.Rows[0]["Gender"]);
                    acc.AgeRange = Convert.ToString(dt.Rows[0]["AgeRange"]);
                    acc.Local = Convert.ToString(dt.Rows[0]["Local"]);
                    acc.Email = Convert.ToString(dt.Rows[0]["Email"]);
                    acc.Birthday = Convert.ToString(dt.Rows[0]["Birthday"]);
                    acc.MobilePhone = Convert.ToString(dt.Rows[0]["MobilePhone"]);


                    return acc;
                }
                else
                    return null;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public OAccount GetAccountInfo(string Source, string SourceID)
        {
            OAccount acc = new OAccount();
            try
            {


                StringBuilder sb = new StringBuilder();
                DataTable dt = new DataTable();
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    sb.Append("select UserID, Source, SourceID,  NickName, FirstName, LastName, MidName, Gender, AgeRange, Local, Email, Birthday, MobilePhone from account  where Source = @Source and SourceID = @id");
                    List<MySqlParameter> parms = new List<MySqlParameter>();
                    parms.Add(new MySqlParameter("@Source", Source));
                    parms.Add(new MySqlParameter("@id", SourceID));

                    dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                }
                if (dt.Rows.Count > 0)
                {
                    acc.ID = Convert.ToInt32(dt.Rows[0]["UserID"]);
                    acc.Source = Convert.ToString(dt.Rows[0]["Source"]);
                    acc.SourceID = Convert.ToString(dt.Rows[0]["SourceID"]);
                    acc.NickName = Convert.ToString(dt.Rows[0]["NickName"]);
                    acc.FName = Convert.ToString(dt.Rows[0]["FirstName"]);
                    acc.LName = Convert.ToString(dt.Rows[0]["LastName"]);
                    acc.MName = Convert.ToString(dt.Rows[0]["MidName"]);
                    acc.Gender = Convert.ToInt32(dt.Rows[0]["Gender"]);
                    acc.AgeRange = Convert.ToString(dt.Rows[0]["AgeRange"]);
                    acc.Local = Convert.ToString(dt.Rows[0]["Local"]);
                    acc.Email = Convert.ToString(dt.Rows[0]["Email"]);
                    acc.Birthday = Convert.ToString(dt.Rows[0]["Birthday"]);
                    acc.MobilePhone = Convert.ToString(dt.Rows[0]["MobilePhone"]);


                    return acc;
                }
                else
                    return null;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public OAccount GetAccountInfo(string Email)
        {
            OAccount acc = new OAccount();
            try
            {


                StringBuilder sb = new StringBuilder();
                DataTable dt = new DataTable();
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    sb.Append("select UserID, Source, SourceID,  NickName, FirstName, LastName, MidName, Gender, AgeRange, Local, Email, Birthday, MobilePhone from account  where Email = @mail ");
                    List<MySqlParameter> parms = new List<MySqlParameter>();
                    parms.Add(new MySqlParameter("@mail", Email));

                    dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                }
                if (dt.Rows.Count > 0)
                {
                    acc.ID = Convert.ToInt32(dt.Rows[0]["UserID"]);
                    acc.Source = Convert.ToString(dt.Rows[0]["Source"]);
                    acc.SourceID = Convert.ToString(dt.Rows[0]["SourceID"]);
                    acc.NickName = Convert.ToString(dt.Rows[0]["NickName"]);
                    acc.FName = Convert.ToString(dt.Rows[0]["FirstName"]);
                    acc.LName = Convert.ToString(dt.Rows[0]["LastName"]);
                    acc.MName = Convert.ToString(dt.Rows[0]["MidName"]);
                    acc.Gender = Convert.ToInt32(dt.Rows[0]["Gender"]);
                    acc.AgeRange = Convert.ToString(dt.Rows[0]["AgeRange"]);
                    acc.Local = Convert.ToString(dt.Rows[0]["Local"]);
                    acc.Email = Convert.ToString(dt.Rows[0]["Email"]);
                    acc.Birthday = Convert.ToString(dt.Rows[0]["Birthday"]);
                    acc.MobilePhone = Convert.ToString(dt.Rows[0]["MobilePhone"]);

                    return acc;
                }
                else
                    return null;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public string GetPassword(string Email)
        {
            try
            {


                StringBuilder sb = new StringBuilder();
                DataTable dt = new DataTable();
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    sb.Append("select Password from account  where Email = @mail ");
                    List<MySqlParameter> parms = new List<MySqlParameter>();
                    parms.Add(new MySqlParameter("@mail", Email));

                    dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                }
                if (dt.Rows.Count > 0)
                    return Convert.ToString(dt.Rows[0]["Password"]);
                else
                    return null;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public bool UpdPassword(int UserID, string oldPWMD5, string newPWMD5)
        {
            try
            {

                StringBuilder sb = new StringBuilder();
                sb.Append("Update account set Password = @new  Where UserID = @UserID and Password = @old");
                List<MySqlParameter> parms = new List<MySqlParameter>();
                parms.Add(new MySqlParameter("@UserID", UserID));
                parms.Add(new MySqlParameter("@new", newPWMD5));
                parms.Add(new MySqlParameter("@old", oldPWMD5));
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    if (conn.executeUpdateQuery(sb.ToString(), parms.ToArray()) > 0)
                        return true;
                    else
                        return false;
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public bool UpdRandomPassword(int UserID, string newPWMD5)
        {
            try
            {

                StringBuilder sb = new StringBuilder();
                sb.Append("Update account set Password = @new  Where UserID = @UserID");
                List<MySqlParameter> parms = new List<MySqlParameter>();
                parms.Add(new MySqlParameter("@UserID", UserID));
                parms.Add(new MySqlParameter("@new", newPWMD5));
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    if (conn.executeUpdateQuery(sb.ToString(), parms.ToArray()) > 0)
                        return true;
                    else
                        return false;
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }



    /// <summary>
    /// 發票資料的DAL
    /// </summary>
    public class DALInvoice : CallcarDAL
    {
        public DALInvoice(string ConnectionString)
            : base(ConnectionString)
        {
        }

        public int CreatenewInvoice(int UserID, string InvoiceTitle, string EIN, string InvoiceAddress)
        {
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();

            sb.Append(" INSERT INTO profile_invoice_info ");
            sb.Append(" (UserID,  InvoiceTitle, EIN, InvoiceAddress) ");
            sb.Append(" VALUES (@UserID, @InvoiceTitle, @EIN, @InvoiceAddress)");


            parms.Add(new MySqlParameter("@UserID", UserID));
            parms.Add(new MySqlParameter("@InvoiceTitle", InvoiceTitle));
            parms.Add(new MySqlParameter("@EIN", EIN));
            parms.Add(new MySqlParameter("@InvoiceAddress", InvoiceAddress));

            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    conn.executeInsertQuery(sb.ToString(), parms.ToArray());

                    parms.Clear();
                    sb.Length = 0;
                    sb.Append("SELECT LAST_INSERT_ID()");
                    DataTable dt = new DataTable();
                    try
                    {
                        dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());

                        return Convert.ToInt32(dt.Rows[0][0]);

                    }
                    catch (MySqlException ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }

            }
            catch (MySqlException ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public bool RemoveInvoice(int UserID, int SeqNo)
        {
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            sb.Append("Delete From profile_invoice_info  WHERE SeqNo=@iid and UserID=@uid");

            parms.Add(new MySqlParameter("@iid", SeqNo));
            parms.Add(new MySqlParameter("@uid", UserID));
            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    if (conn.executeDeleteQuery(sb.ToString(), parms.ToArray()) > 0)
                        return true;
                    else
                        return false;
                }
            }
            catch (MySqlException ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public OInvoice QryUserInvoice(int UserID)
        {
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            DataTable dt = new DataTable();
            sb.Append("Select SeqNo,   InvoiceTitle, EIN, InvoiceAddress From profile_invoice_info  WHERE UserID = @uid ");

            parms.Add(new MySqlParameter("@uid", UserID));
            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                }
            }
            catch (MySqlException ex)
            {
                throw new Exception(ex.Message);
            }

            if (dt.Rows.Count > 0)
            {


                OInvoice invoice = new OInvoice();
                invoice.UserID = UserID;
                invoice.SeqNo = Convert.ToInt32(dt.Rows[0]["SeqNo"]);
                invoice.InvoiceTitle = Convert.ToString(dt.Rows[0]["InvoiceTitle"]);
                invoice.EIN = Convert.ToString(dt.Rows[0]["EIN"]);
                invoice.InvoiceAddress = Convert.ToString(dt.Rows[0]["InvoiceAddress"]);


                return invoice;
            }
            else
                return null;
        }
    }
    /// <summary>
    /// 取號的DAL
    /// </summary>
    public class DALSerialNum : CallcarDAL
    {
        public DALSerialNum(string ConnectionString)
            : base(ConnectionString)
        {
        }

        public OSerialNum GetSerialNumObject(string Purpose)
        {
            OSerialNum o = new OSerialNum();
            StringBuilder sb = new StringBuilder();
            sb.Append("SELECT Prefix, Digit, Current, Used FROM sequence_maintain Where Purpose = @p");
            List<MySqlParameter> parms = new List<MySqlParameter>();
            DataTable dt = new DataTable();
            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    parms.Add(new MySqlParameter("@p", Purpose));
                    dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                }
            }
            catch (MySqlException ex)
            {
                throw new Exception(ex.Message);
            }

            if (dt.Rows.Count > 0)
            {
                o.Prefix = Convert.ToString(dt.Rows[0]["Prefix"]);
                o.Current = Convert.ToString(dt.Rows[0]["Current"]);
                o.Digit = Convert.ToInt32(dt.Rows[0]["Digit"]);
                o.Used = Convert.ToInt32(dt.Rows[0]["Used"]);
                o.Purpose = Purpose;
                return o;
            }
            else
                return null;
        }

        public string GetSeqNoString(OSerialNum os)
        {
            StringBuilder sb = new StringBuilder();


            int ReturnSeq = 0;
            sb.Append(" UPDATE sequence_maintain SET ");

            string NowPrefix = "";
            try
            {
                NowPrefix = DateTime.Now.ToString(os.Prefix);
            }
            catch (FormatException)
            {
                throw new Exception("Prefix格式不合");
            }

            if (NowPrefix != os.Current)
            {
                if (os.Purpose == "Reservation")
                    ReturnSeq = 6001;
                else
                    ReturnSeq = 1;
                sb.Append(" Current = '" + NowPrefix + "',");
                sb.Append(" Used = " + ReturnSeq.ToString() + " ");
            }
            else
            {
                ReturnSeq = os.Used + 1;
                sb.Append("Used = " + ReturnSeq.ToString() + " ");
            }
            sb.Append(" Where Purpose = @p ");
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("@p", os.Purpose));
            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    conn.executeUpdateQuery(sb.ToString(), parms.ToArray());
                }
            }
            catch (MySqlException)
            {
                throw new Exception("更新序號失敗");
            }
            os.Used = ReturnSeq;
            return ReturnSeq.ToString().PadLeft(os.Digit, '0');
        }
    }

    /// <summary>
    /// 信用卡 DAL
    /// </summary>
    public class DALCreditCard : CallcarDAL
    {
        public DALCreditCard(string ConnectionString)
            : base(ConnectionString)
        {
        }

        public int CreateNewCard(int UserID, string CardNo, string Pay2goToken, string Pay2goTokenExpire)
        {
            List<MySqlParameter> parms = new List<MySqlParameter>();
            StringBuilder sb = new StringBuilder();
            sb.Append(" INSERT INTO profile_card_info (UserID,  CardNo,   Pay2goToken, Pay2goTokenExpire, CheckStatus) ");
            sb.Append(" VALUES (@UserID, @CardNo,  @Pay2goToken, @Pay2goTokenExpire ,@CheckStatus)");
            using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
            {
                parms.Add(new MySqlParameter("@UserID", UserID));
                //parms.Add(new MySqlParameter("@CardCompany", CardCompany));
                parms.Add(new MySqlParameter("@CardNo", CardNo));
                //parms.Add(new MySqlParameter("@DeadLine", DeadLine));
                //parms.Add(new MySqlParameter("@AuthCode", AuthCode));
                parms.Add(new MySqlParameter("@Pay2goToken", Pay2goToken));


                parms.Add(new MySqlParameter("@Pay2goTokenExpire", Pay2goTokenExpire));
                parms.Add(new MySqlParameter("@CheckStatus", 1));

                try
                {
                    conn.executeInsertQuery(sb.ToString(), parms.ToArray());
                }
                catch (MySqlException ex)
                {
                    throw new Exception(ex.Message);
                }
                parms.Clear();
                sb.Length = 0;
                sb.Append("SELECT LAST_INSERT_ID()");
                DataTable dt = new DataTable();
                try
                {
                    dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());

                    return Convert.ToInt32(dt.Rows[0][0]);

                }
                catch (MySqlException ex)
                {
                    throw new Exception(ex.Message);
                }
            }

        }

        public bool RemoveCard(int UserID, int CardID)
        {

            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            sb.Append("Delete From  profile_card_info WHERE CardID=@no and UserID=@id");
            using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
            {
                parms.Add(new MySqlParameter("@no", CardID));
                parms.Add(new MySqlParameter("@id", UserID));
                try
                {
                    if (conn.executeDeleteQuery(sb.ToString(), parms.ToArray()) > 0)
                        return true;
                    else
                        return false;
                }
                catch (MySqlException ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }


        public OCard QryUserCards(int UserID)
        {
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            DataTable dt = new DataTable();
            sb.Append("Select CardID, UserID,  CardNo,  CheckStatus,Pay2goToken, Pay2goTokenExpire From profile_card_info  WHERE UserID = @uid ");
            parms.Add(new MySqlParameter("@uid", UserID));

            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                }
            }
            catch (MySqlException ex)
            {
                throw new Exception(ex.Message);
            }

            if (dt.Rows.Count > 0)
            {


                OCard Card = new OCard();
                Card.UID = Convert.ToInt32(dt.Rows[0]["UserID"]);
                Card.ID = Convert.ToInt32(dt.Rows[0]["CardID"]);
                Card.CNo = Convert.ToString(dt.Rows[0]["CardNo"]);
                //Card.Code = Convert.ToString(dt.Rows[0]["AuthCode"]);
                //Card.Company = Convert.ToString(dt.Rows[0]["CardCompany"]);
                //Card.Expire = Convert.ToString(dt.Rows[0]["DeadLine"]);
                Card.Check = Convert.ToInt32(dt.Rows[0]["CheckStatus"]);
                Card.t = Convert.ToString(dt.Rows[0]["Pay2goToken"]);
                Card.d = Convert.ToString(dt.Rows[0]["Pay2goTokenExpire"]);


                return Card;
            }
            else
                return null;
        }
    }

    /// <summary>
    /// 訂單的DAL
    /// </summary>
    public class DALReservation : CallcarDAL
    {
        public DALReservation(string ConnectionString) : base(ConnectionString) { }

        /// <summary>
        /// 新增訂單
        /// </summary>
        /// <param name="or">訂單內容</param>
        /// <param name="PickType">預約0/撿車1</param>
        /// <param name="Dno">派車單號</param>
        /// <returns></returns>
        public bool CreateNewReservation(OReservation or, string PickType, string Dno, string ServeTime, OPrice op)
        {
            try
            {
                if (PickType == "1")
                    return AddPickCarReservation(or, PickType, Dno, ServeTime, op);
                else
                    return AddNewReservationAlong(or, op);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private bool AddNewReservationAlong(OReservation or, OPrice op)
        {
            List<MySqlParameter> parms = new List<MySqlParameter>();
            StringBuilder sb = new StringBuilder();
            sb.Append("INSERT INTO reservation_sheet  ");
            sb.Append("(ReservationNo,UserID, ServiceType, TakeDate, TimeSegment, SelectionType,PickCartype, FlightNo, ScheduleFlightTime,Airport,Ternimal,  ");
            sb.Append("PickupCity, PickupDistinct,PickupVillage, PickupAddress,PickupGPS, TakeoffCity, TakeoffDistinct,TakeoffVillage, TakeoffAddress, TakeoffGPS, PassengerCnt,   ");
            sb.Append("BaggageCnt,MaxFlag, Price, Coupon, InvoiceData,CreditData, ProcessStage,PassengerName, PassengerPhone)  ");
            sb.Append("VALUES (@ReservationNo,@UserID, @ServiceType, @TakeDate, @TimeSegment, @SelectionType, @PickCartype,@FlightNo, @ScheduleFlightTime,@Airport,@Ternimal,  ");
            sb.Append("@PickupCity, @PickupDistinct,@PickupVillage, @PickupAddress,GeomFromText(@PickupGPS), @TakeoffCity, @TakeoffDistinct, @TakeoffVillage, @TakeoffAddress, GeomFromText(@TakeoffGPS), @PassengerCnt,   ");
            sb.Append("@BaggageCnt, @MaxFlag,@Price, @Coupon, @InvoiceData,@CreditData, @ProcessStage,@PassengerName, @PassengerPhone)  ");

            using (TransactionScope scope = new TransactionScope())
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    parms.Add(new MySqlParameter("@ReservationNo", or.ReservationNo));
                    parms.Add(new MySqlParameter("@UserID", or.UserID));
                    parms.Add(new MySqlParameter("@ServiceType", or.ServiceType));
                    parms.Add(new MySqlParameter("@TakeDate", or.ServeDate));
                    parms.Add(new MySqlParameter("@TimeSegment", or.PreferTime));
                    parms.Add(new MySqlParameter("@SelectionType", or.ChooseType));

                    if (or.ChooseType == "F")
                    {
                        parms.Add(new MySqlParameter("@FlightNo", or.FlightNo));
                        parms.Add(new MySqlParameter("@ScheduleFlightTime", or.FlightTime));
                    }
                    else
                    {
                        parms.Add(new MySqlParameter("@FlightNo", ""));
                        parms.Add(new MySqlParameter("@ScheduleFlightTime", null));
                    }
                    parms.Add(new MySqlParameter("@Airport", or.Airport));
                    parms.Add(new MySqlParameter("@Ternimal", or.Ternimal));
                    parms.Add(new MySqlParameter("@PickCartype", or.PickCartype));
                    parms.Add(new MySqlParameter("@PickupCity", or.PickupCity));
                    parms.Add(new MySqlParameter("@PickupDistinct", or.PickupDistinct));
                    parms.Add(new MySqlParameter("@PickupVillage", or.PickupVillage));
                    parms.Add(new MySqlParameter("@PickupAddress", or.PickupAddress));
                    parms.Add(new MySqlParameter("@PickupGPS", "Point(" + or.PickupGPS.GetLng().ToString() + " " + or.PickupGPS.GetLat().ToString() + ")"));
                    parms.Add(new MySqlParameter("@TakeoffCity", or.TakeoffCity));
                    parms.Add(new MySqlParameter("@TakeoffDistinct", or.TakeoffDistinct));
                    parms.Add(new MySqlParameter("@TakeoffVillage", or.TakeoffVillage));
                    parms.Add(new MySqlParameter("@TakeoffAddress", or.TakeoffAddress));
                    parms.Add(new MySqlParameter("@TakeoffGPS", "Point(" + or.TakeoffGPS.GetLng().ToString() + " " + or.TakeoffGPS.GetLat().ToString() + ")"));
                    parms.Add(new MySqlParameter("@PassengerCnt", or.PassengerCnt));
                    parms.Add(new MySqlParameter("@BaggageCnt", or.BaggageCnt));
                    parms.Add(new MySqlParameter("@MaxFlag", or.MaxFlag));
                    parms.Add(new MySqlParameter("@Coupon", or.Coupon));
                    parms.Add(new MySqlParameter("@InvoiceData", or.Invoice));
                    parms.Add(new MySqlParameter("@CreditData", or.Credit));
                    parms.Add(new MySqlParameter("@ProcessStage", "0"));
                    parms.Add(new MySqlParameter("@Price", or.TrailPrice));
                    parms.Add(new MySqlParameter("@PassengerName", or.PassengerName));
                    parms.Add(new MySqlParameter("@PassengerPhone", or.PassengerPhone));

                    try
                    {
                        conn.executeInsertQuery(sb.ToString(), parms.ToArray());

                        //新增帳務檔
                        sb.Length = 0;
                        parms.Clear();
                        sb.Append("INSERT INTO reservation_charge_list ");
                        sb.Append("(ReservationNo, TakeDate, ServiceType, SelectionType, FlightNo, FlightDate, FlightTime, ServiceRemark, FlightRemark, ActualFlightTime, Price, RealAmt, PromoteAmt, CollectAmt, TotalAmt, OfficialTotalAmt, CarFee, NightFee, BaggageFee, OtherFee, OtherSaleFee, Memo, ChargeResult, ChargeErrMsg, ChargeData, ChargeStatus) ");
                        sb.Append("VALUES(@ReservationNo, @TakeDate, @ServiceType, @SelectionType, @FlightNo, @FlightDate, @FlightTime, '0', '', null, @Price, @RealAmt, @PromoteAmt, @CollectAmt, @TotalAmt, @OfficialTotalAmt, @CarFee, @NightFee, @BaggageFee, @OtherFee, @OtherSaleFee, '', '0', '', '', '1') ");

                        parms.Add(new MySqlParameter("@ReservationNo", or.ReservationNo));
                        parms.Add(new MySqlParameter("@TakeDate", or.ServeDate));
                        parms.Add(new MySqlParameter("@ServiceType", or.ServiceType));
                        parms.Add(new MySqlParameter("@SelectionType", or.ChooseType));
                        if (or.ChooseType == "F")
                        {
                            parms.Add(new MySqlParameter("@FlightNo", or.FlightNo));
                            parms.Add(new MySqlParameter("@FlightDate", or.FlightDate));
                            parms.Add(new MySqlParameter("@FlightTime", or.FlightTime));
                        }
                        else
                        {
                            parms.Add(new MySqlParameter("@FlightNo", ""));
                            parms.Add(new MySqlParameter("@FlightDate", ""));
                            parms.Add(new MySqlParameter("@FlightTime", null));
                        }
                        parms.Add(new MySqlParameter("@Price", op.Price));
                        parms.Add(new MySqlParameter("@RealAmt", op.RealAmt));
                        parms.Add(new MySqlParameter("@PromoteAmt", op.PromoteAmt));
                        parms.Add(new MySqlParameter("@CollectAmt", op.CollectAmt));
                        parms.Add(new MySqlParameter("@TotalAmt", op.TotalAmt));
                        parms.Add(new MySqlParameter("@OfficialTotalAmt", op.OfficialTotalAmt));
                        parms.Add(new MySqlParameter("@CarFee", op.CarFee));
                        parms.Add(new MySqlParameter("@NightFee", op.NightFee));
                        parms.Add(new MySqlParameter("@BaggageFee", op.BaggageFee));
                        parms.Add(new MySqlParameter("@OtherFee", op.OtherFee));
                        parms.Add(new MySqlParameter("@OtherSaleFee", op.OtherSaleFee));


                        conn.executeInsertQuery(sb.ToString(), parms.ToArray());
                        scope.Complete();
                        
                        return true;
                    }
                    catch (MySqlException ex)
                    {
                        throw new Exception(ex.Message);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
            }
        }

        private bool AddPickCarReservation(OReservation or, string PickType, string Dno, string ServeTime, OPrice op)
        {
            //先收集資料
            DateTime LastServiceTime;
            if (ServeTime == "null" || ServeTime == "")
            {
                DALDispatch dald = new DALDispatch(this._ConnectionString);
                try
                {
                    LastServiceTime = dald.GetLastScheduleServiceTime(Dno);
                }
                catch (Exception ex)
                {
                    throw ex;
                }

                if (or.ServiceType == "O")
                    LastServiceTime = LastServiceTime.AddMinutes(15);
            }
            else
                LastServiceTime = Common.General.ConvertToDateTime(or.ServeDate + ServeTime, 12);

            BAL.BALDispatch bald = new BAL.BALDispatch(this._ConnectionString);
            ODispatchSheet od;
            try
            {
                od = bald.GetDispatchSheetByNo(Dno);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            BAL.BALUserAccount acc = new BAL.BALUserAccount(this._ConnectionString);
            OAccount oa = acc.GetAccountInfo(or.UserID);

            if (bald.isCarFull(Dno))
                throw new Exception("此車預訂已滿");

            using (TransactionScope scope = new TransactionScope())
            {
                try
                {
                    List<MySqlParameter> parms = new List<MySqlParameter>();
                    StringBuilder sb = new StringBuilder();
                    sb.Append("INSERT INTO reservation_sheet  ");
                    sb.Append("(ReservationNo,UserID, ServiceType, TakeDate, TimeSegment, SelectionType,PickCartype, FlightNo, ScheduleFlightTime,Airport,Ternimal,  ");
                    sb.Append("PickupCity, PickupDistinct,PickupVillage, PickupAddress,PickupGPS, TakeoffCity, TakeoffDistinct,TakeoffVillage, TakeoffAddress, TakeoffGPS, PassengerCnt,   ");
                    sb.Append("BaggageCnt,MaxFlag, Price, Coupon, InvoiceData,CreditData, ProcessStage,PassengerName, PassengerPhone)  ");
                    sb.Append("VALUES (@ReservationNo,@UserID, @ServiceType, @TakeDate, @TimeSegment, @SelectionType, @PickCartype,@FlightNo, @ScheduleFlightTime,@Airport,@Ternimal,  ");
                    sb.Append("@PickupCity, @PickupDistinct,@PickupVillage, @PickupAddress,GeomFromText(@PickupGPS), @TakeoffCity, @TakeoffDistinct, @TakeoffVillage, @TakeoffAddress, GeomFromText(@TakeoffGPS), @PassengerCnt,   ");
                    sb.Append("@BaggageCnt, @MaxFlag,@Price, @Coupon, @InvoiceData,@CreditData, @ProcessStage,@PassengerName, @PassengerPhone)  ");


                    parms.Add(new MySqlParameter("@ReservationNo", or.ReservationNo));
                    parms.Add(new MySqlParameter("@UserID", or.UserID));
                    parms.Add(new MySqlParameter("@ServiceType", or.ServiceType));
                    parms.Add(new MySqlParameter("@TakeDate", or.ServeDate));
                    parms.Add(new MySqlParameter("@TimeSegment", LastServiceTime.ToString("HHmm")));
                    parms.Add(new MySqlParameter("@SelectionType", or.ChooseType));

                    if (or.ChooseType == "F")
                    {
                        parms.Add(new MySqlParameter("@FlightNo", or.FlightNo));
                        parms.Add(new MySqlParameter("@ScheduleFlightTime", or.FlightTime));
                    }
                    else
                    {
                        parms.Add(new MySqlParameter("@FlightNo", ""));
                        parms.Add(new MySqlParameter("@ScheduleFlightTime", DBNull.Value));
                    }
                    parms.Add(new MySqlParameter("@Airport", or.Airport));
                    parms.Add(new MySqlParameter("@Ternimal", or.Ternimal));
                    parms.Add(new MySqlParameter("@PickCartype", or.PickCartype));
                    parms.Add(new MySqlParameter("@PickupCity", or.PickupCity));
                    parms.Add(new MySqlParameter("@PickupDistinct", or.PickupDistinct));
                    parms.Add(new MySqlParameter("@PickupVillage", or.PickupVillage));
                    parms.Add(new MySqlParameter("@PickupAddress", or.PickupAddress));
                    parms.Add(new MySqlParameter("@PickupGPS", "Point(" + or.PickupGPS.GetLng().ToString() + " " + or.PickupGPS.GetLat().ToString() + ")"));
                    parms.Add(new MySqlParameter("@TakeoffCity", or.TakeoffCity));
                    parms.Add(new MySqlParameter("@TakeoffDistinct", or.TakeoffDistinct));
                    parms.Add(new MySqlParameter("@TakeoffVillage", or.TakeoffVillage));
                    parms.Add(new MySqlParameter("@TakeoffAddress", or.TakeoffAddress));
                    parms.Add(new MySqlParameter("@TakeoffGPS", "Point(" + or.TakeoffGPS.GetLng().ToString() + " " + or.TakeoffGPS.GetLat().ToString() + ")"));
                    parms.Add(new MySqlParameter("@PassengerCnt", or.PassengerCnt));
                    parms.Add(new MySqlParameter("@BaggageCnt", or.BaggageCnt));
                    parms.Add(new MySqlParameter("@MaxFlag", or.MaxFlag));
                    parms.Add(new MySqlParameter("@Coupon", or.Coupon));
                    parms.Add(new MySqlParameter("@InvoiceData", or.Invoice));
                    parms.Add(new MySqlParameter("@CreditData", or.Credit));
                    parms.Add(new MySqlParameter("@ProcessStage", "A"));
                    parms.Add(new MySqlParameter("@Price", or.TrailPrice));
                    parms.Add(new MySqlParameter("@PassengerName", or.PassengerName));
                    parms.Add(new MySqlParameter("@PassengerPhone", or.PassengerPhone));

                    using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                    {
                        conn.executeInsertQuery(sb.ToString(), parms.ToArray());

                        //新增帳務檔
                        sb.Length = 0;
                        parms.Clear();
                        sb.Append("INSERT INTO reservation_charge_list ");
                        sb.Append("(ReservationNo, TakeDate, ServiceType, SelectionType, FlightNo, FlightDate, FlightTime, ServiceRemark, FlightRemark, ActualFlightTime, Price, RealAmt, PromoteAmt, CollectAmt, TotalAmt, OfficialTotalAmt, CarFee, NightFee, BaggageFee, OtherFee, OtherSaleFee, Memo, ChargeResult, ChargeErrMsg, ChargeData, ChargeStatus) ");
                        sb.Append("VALUES(@ReservationNo, @TakeDate, @ServiceType, @SelectionType, @FlightNo, @FlightDate, @FlightTime, '0', '', null, @Price, @RealAmt, @PromoteAmt, @CollectAmt, @TotalAmt, @OfficialTotalAmt, @CarFee, @NightFee, @BaggageFee, @OtherFee, @OtherSaleFee, '', '0', '', '', '1') ");

                        parms.Add(new MySqlParameter("@ReservationNo", or.ReservationNo));
                        parms.Add(new MySqlParameter("@TakeDate", or.ServeDate));
                        parms.Add(new MySqlParameter("@ServiceType", or.ServiceType));
                        parms.Add(new MySqlParameter("@SelectionType", or.ChooseType));
                        if (or.ChooseType == "F")
                        {
                            parms.Add(new MySqlParameter("@FlightNo", or.FlightNo));
                            parms.Add(new MySqlParameter("@FlightDate", or.FlightDate));
                            parms.Add(new MySqlParameter("@FlightTime", or.FlightTime));
                        }
                        else
                        {
                            parms.Add(new MySqlParameter("@FlightNo", ""));
                            parms.Add(new MySqlParameter("@FlightDate", ""));
                            parms.Add(new MySqlParameter("@FlightTime", null));
                        }
                        parms.Add(new MySqlParameter("@Price", op.Price));
                        parms.Add(new MySqlParameter("@RealAmt", op.RealAmt));
                        parms.Add(new MySqlParameter("@PromoteAmt", op.PromoteAmt));
                        parms.Add(new MySqlParameter("@CollectAmt", op.CollectAmt));
                        parms.Add(new MySqlParameter("@TotalAmt", op.TotalAmt));
                        parms.Add(new MySqlParameter("@OfficialTotalAmt", op.OfficialTotalAmt));
                        parms.Add(new MySqlParameter("@CarFee", op.CarFee));
                        parms.Add(new MySqlParameter("@NightFee", op.NightFee));
                        parms.Add(new MySqlParameter("@BaggageFee", op.BaggageFee));
                        parms.Add(new MySqlParameter("@OtherFee", op.OtherFee));
                        parms.Add(new MySqlParameter("@OtherSaleFee", op.OtherSaleFee));


                        conn.executeInsertQuery(sb.ToString(), parms.ToArray());

                        //撿車的 必須新增派車單訂單資料
                        //新增一筆dispatch_reservation
                        sb.Length = 0;
                        parms.Clear();


                        sb.Append("INSERT INTO dispatch_reservation	(DispatchNo, ReservationNo, ServeSeq, ScheduleServeTime, ActualServreTime, LocationGPS, ServiceRemark) ");
                        sb.Append("VALUES (@DispatchNo, @ReservationNo, 0, @ScheduleServeTime,NULL , GeomFromText(@LocationGPS), '0')");
                        parms.Add(new MySqlParameter("@DispatchNo", Dno));
                        parms.Add(new MySqlParameter("@ReservationNo", or.ReservationNo));
                        parms.Add(new MySqlParameter("@ScheduleServeTime", LastServiceTime));
                        if (or.ServiceType == "O")
                            parms.Add(new MySqlParameter("@LocationGPS", "Point(" + or.PickupGPS.GetLng().ToString() + " " + or.PickupGPS.GetLat().ToString() + ")"));
                        else
                            parms.Add(new MySqlParameter("@LocationGPS", "Point(" + or.TakeoffGPS.GetLng().ToString() + " " + or.TakeoffGPS.GetLat().ToString() + ")"));


                        conn.executeInsertQuery(sb.ToString(), parms.ToArray());


                        //檢車的新增簡訊發送
                        sb.Length = 0;
                        parms.Clear();



                        if (od.CarNo != "" && od.DriverID > 0)
                        {
                            sb.Append("INSERT INTO reservation_sms ");
                            sb.Append("(ReservationNo, ServiceType, SelectionType, FlightTime, MobilePhone, CarNo, DriverID, ServeTime,PassengerName) ");
                            sb.Append("VALUES(@rno, @stype, @seltype, @ftime, @phone, @car, @did, @stime,@PassengerName) ");
                            parms.Add(new MySqlParameter("@rno", or.ReservationNo));
                            parms.Add(new MySqlParameter("@stype", or.ServiceType));
                            parms.Add(new MySqlParameter("@seltype", or.ChooseType));
                            parms.Add(new MySqlParameter("@ftime", or.FlightTime));
                            parms.Add(new MySqlParameter("@phone", oa.MobilePhone));
                            parms.Add(new MySqlParameter("@car", od.CarNo));
                            parms.Add(new MySqlParameter("@did", od.DriverID));
                            parms.Add(new MySqlParameter("@stime", LastServiceTime));
                            parms.Add(new MySqlParameter("@PassengerName", or.PassengerName));


                            conn.executeInsertQuery(sb.ToString(), parms.ToArray());

                        }
                    } //end using conn

                    scope.Complete();
                    return true;
                }//end try catch
                catch (Exception ex)
                {
                    throw ex;
                }
            }//end using scope
        }

        /// <summary>
        /// 新增追蹤航班
        /// </summary>
        /// <param name="FlightNo">航班編號</param>
        /// <param name="FlightTime">航班時間</param>
        /// <returns></returns>
        //public bool AddTrackFlight(string ServiceType, string FlightNo, DateTime FlightTime)
        //{
        //    string AirlineID, FlightNumber, FlightType;
        //    FlightNo = FlightNo.Trim();
        //    AirlineID = FlightNo.Substring(0, 2).Trim();
        //    FlightNumber = FlightNo.Substring(2, FlightNo.Length - 2).Trim();
        //    if (ServiceType == "O")
        //        FlightType = "PD";
        //    else
        //        FlightType = "PA";

        //    StringBuilder sb = new StringBuilder();
        //    List<MySqlParameter> parms = new List<MySqlParameter>();

        //    sb.Append("INSERT INTO flight_track (FlightDate, AirlineID,FlightNo,FlightNoFull,FlightTime,FlightType)VALUES (@FlightDate,@AirlineID,@FlightNo,@FlightNoFull,@FlightTime ,@FlightType)");

        //    using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
        //    {
        //        parms.Add(new MySqlParameter("@FlightDate", FlightTime.ToString("yyyyMMdd")));
        //        parms.Add(new MySqlParameter("@AirlineID", AirlineID));
        //        parms.Add(new MySqlParameter("@FlightNo", FlightNumber));
        //        parms.Add(new MySqlParameter("@FlightNoFull", AirlineID + FlightNumber));
        //        parms.Add(new MySqlParameter("@FlightTime", FlightTime));
        //        parms.Add(new MySqlParameter("@FlightType", FlightType));
        //        try
        //        {
        //            conn.executeInsertQuery(sb.ToString(), parms.ToArray());
        //        }
        //        catch (MySqlException)
        //        {
        //            return true;
        //        }
        //    }
        //    return true;
        //}

        /// <summary>
        /// 回傳一筆訂單物件
        /// </summary>
        /// <param name="RNo">訂單編號</param>
        /// <returns></returns>
        public OReservation GetReservation(string RNo)
        {
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            sb.Append("SELECT ReservationNo, UserID, ServiceType, TakeDate, TimeSegment, SelectionType, PickCartype, FlightNo, ScheduleFlightTime,Airport,Ternimal, PickupCity, ");
            sb.Append("PickupDistinct, PickupVillage, PickupAddress, X(PickupGPS) as plng, Y(PickupGPS) as plat, TakeoffCity, TakeoffDistinct, TakeoffVillage, TakeoffAddress, X(TakeoffGPS) as tlng ,Y(TakeoffGPS) as tlat , PassengerCnt, ");
            sb.Append("BaggageCnt, MaxFlag, Price, Coupon, InvoiceData, CreditData, ProcessStage, InvoiceNum, CreateTime, UpdTime,AddBaggageCnt,PassengerName, PassengerPhone ");
            sb.Append("FROM reservation_sheet where ReservationNo = @no");

            parms.Add(new MySqlParameter("@no", RNo));
            DataTable dt = new DataTable();
            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            if (dt.Rows.Count > 0)
            {

                OReservation r = new OReservation();
                r.BaggageCnt = Convert.ToInt32(dt.Rows[0]["BaggageCnt"]);
                r.ChooseType = Convert.ToString(dt.Rows[0]["SelectionType"]);
                if (r.ChooseType == "T")
                {
                    r.FlightDate = "";
                    r.FlightNo = "";
                }
                else
                {
                    r.FlightDate = Convert.ToDateTime(dt.Rows[0]["ScheduleFlightTime"]).ToString("yyyyMMdd");
                    r.FlightNo = Convert.ToString(dt.Rows[0]["FlightNo"]);
                    r.FlightTime = Convert.ToDateTime(dt.Rows[0]["ScheduleFlightTime"]);
                }
                r.Airport = Convert.ToString(dt.Rows[0]["Airport"]);
                r.Ternimal = Convert.ToString(dt.Rows[0]["Ternimal"]);
                r.Coupon = Convert.ToString(dt.Rows[0]["Coupon"]);
                r.Credit = Convert.ToString(dt.Rows[0]["CreditData"]);
                r.Invoice = Convert.ToString(dt.Rows[0]["InvoiceData"]);
                r.MaxFlag = Convert.ToString(dt.Rows[0]["MaxFlag"]);
                r.OrderTime = Convert.ToDateTime(dt.Rows[0]["CreateTime"]);
                r.PassengerCnt = Convert.ToInt32(dt.Rows[0]["PassengerCnt"]);
                r.PickCartype = Convert.ToString(dt.Rows[0]["PickCartype"]);
                r.PickupAddress = Convert.ToString(dt.Rows[0]["PickupAddress"]);
                r.PickupCity = Convert.ToString(dt.Rows[0]["PickupCity"]);
                r.PickupDistinct = Convert.ToString(dt.Rows[0]["PickupDistinct"]);
                r.PickupVillage = Convert.ToString(dt.Rows[0]["PickupVillage"]);
                r.PreferTime = Convert.ToString(dt.Rows[0]["TimeSegment"]);
                r.ProcessStage = Convert.ToString(dt.Rows[0]["ProcessStage"]);
                r.ReservationNo = Convert.ToString(dt.Rows[0]["ReservationNo"]);
                r.ServeDate = Convert.ToString(dt.Rows[0]["TakeDate"]);
                r.ServiceType = Convert.ToString(dt.Rows[0]["ServiceType"]);
                r.TakeoffAddress = Convert.ToString(dt.Rows[0]["TakeoffAddress"]);
                r.TakeoffCity = Convert.ToString(dt.Rows[0]["TakeoffCity"]);
                r.TakeoffDistinct = Convert.ToString(dt.Rows[0]["TakeoffDistinct"]);
                r.TakeoffVillage = Convert.ToString(dt.Rows[0]["TakeoffVillage"]);
                r.UserID = Convert.ToInt32(dt.Rows[0]["UserID"]);
                r.EIN = Convert.ToString(dt.Rows[0]["InvoiceNum"]);
                r.TakeoffGPS = new OSpatialGPS(Convert.ToDouble(dt.Rows[0]["tlat"]), Convert.ToDouble(dt.Rows[0]["tlng"]));
                r.PickupGPS = new OSpatialGPS(Convert.ToDouble(dt.Rows[0]["plat"]), Convert.ToDouble(dt.Rows[0]["plng"]));
                r.AddBagCnt = Convert.ToInt32(dt.Rows[0]["AddBaggageCnt"]);
                r.PassengerName = Convert.ToString(dt.Rows[0]["PassengerName"]);
                r.PassengerPhone = Convert.ToString(dt.Rows[0]["PassengerPhone"]);

                return r;

            }
            else
            {
                return null;
            }

        }

        /// <summary>
        /// 依照日期回傳一批訂單
        /// </summary>
        /// <param name="sdate">查詢起始時間</param>
        /// <param name="edate">查詢結束時間</param>
        /// <returns></returns>
        public List<OReservation> GetReservations(string sdate, string edate)
        {
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            sb.Append("SELECT ReservationNo, UserID, ServiceType, TakeDate, TimeSegment, SelectionType, PickCartype, FlightNo, ScheduleFlightTime, Airport,Ternimal,PickupCity, ");
            sb.Append("PickupDistinct, PickupVillage, PickupAddress, X(PickupGPS) as plng, Y(PickupGPS) as plat, TakeoffCity, TakeoffDistinct, TakeoffVillage, TakeoffAddress, X(TakeoffGPS) as tlng ,Y(TakeoffGPS) as tlat , PassengerCnt, ");
            sb.Append("BaggageCnt, MaxFlag, Price, Coupon, InvoiceData, CreditData, ProcessStage, InvoiceNum, CreateTime, UpdTime,AddBaggageCnt ,PassengerName, PassengerPhone ");
            sb.Append("FROM reservation_sheet where TakeDate between  @sdate AND @edate");

            parms.Add(new MySqlParameter("@sdate", sdate));
            parms.Add(new MySqlParameter("@edate", edate));
            DataTable dt = new DataTable();
            List<OReservation> List = new List<OReservation>();
            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    OReservation r = new OReservation();
                    r.BaggageCnt = Convert.ToInt32(dt.Rows[i]["BaggageCnt"]);
                    r.ChooseType = Convert.ToString(dt.Rows[i]["SelectionType"]);
                    r.Coupon = Convert.ToString(dt.Rows[i]["Coupon"]);
                    r.Credit = Convert.ToString(dt.Rows[i]["CreditData"]);
                    r.FlightDate = Convert.ToDateTime(dt.Rows[i]["ScheduleFlightTime"]).ToString("yyyyMMdd");
                    r.FlightNo = Convert.ToString(dt.Rows[i]["FlightNo"]);
                    r.FlightTime = Convert.ToDateTime(dt.Rows[i]["ScheduleFlightTime"]);
                    r.Airport = Convert.ToString(dt.Rows[i]["Airport"]);
                    r.Ternimal = Convert.ToString(dt.Rows[i]["Ternimal"]);
                    r.Invoice = Convert.ToString(dt.Rows[i]["InvoiceData"]);
                    r.MaxFlag = Convert.ToString(dt.Rows[i]["MaxFlag"]);
                    r.OrderTime = Convert.ToDateTime(dt.Rows[i]["CreateTime"]);
                    r.PassengerCnt = Convert.ToInt32(dt.Rows[i]["PassengerCnt"]);
                    r.PickCartype = Convert.ToString(dt.Rows[i]["PickCartype"]);
                    r.PickupAddress = Convert.ToString(dt.Rows[i]["PickupAddress"]);
                    r.PickupCity = Convert.ToString(dt.Rows[i]["PickupCity"]);
                    r.PickupDistinct = Convert.ToString(dt.Rows[i]["PickupDistinct"]);
                    r.PickupVillage = Convert.ToString(dt.Rows[i]["PickupVillage"]);
                    r.PreferTime = Convert.ToString(dt.Rows[i]["TimeSegment"]);
                    r.ProcessStage = Convert.ToString(dt.Rows[i]["ProcessStage"]);
                    r.ReservationNo = Convert.ToString(dt.Rows[i]["ReservationNo"]);
                    r.ServeDate = Convert.ToString(dt.Rows[i]["TakeDate"]);
                    r.ServiceType = Convert.ToString(dt.Rows[i]["ServiceType"]);
                    r.TakeoffAddress = Convert.ToString(dt.Rows[i]["TakeoffAddress"]);
                    r.TakeoffCity = Convert.ToString(dt.Rows[i]["TakeoffCity"]);
                    r.TakeoffDistinct = Convert.ToString(dt.Rows[i]["TakeoffDistinct"]);
                    r.TakeoffVillage = Convert.ToString(dt.Rows[i]["TakeoffVillage"]);
                    r.UserID = Convert.ToInt32(dt.Rows[i]["UserID"]);
                    r.EIN = Convert.ToString(dt.Rows[i]["InvoiceNum"]);
                    r.TakeoffGPS = new OSpatialGPS(Convert.ToDouble(dt.Rows[i]["tlat"]), Convert.ToDouble(dt.Rows[i]["tlng"]));
                    r.PickupGPS = new OSpatialGPS(Convert.ToDouble(dt.Rows[i]["plat"]), Convert.ToDouble(dt.Rows[i]["plng"]));
                    r.AddBagCnt = Convert.ToInt32(dt.Rows[i]["AddBaggageCnt"]);
                    r.PassengerName = Convert.ToString(dt.Rows[i]["PassengerName"]);
                    r.PassengerPhone = Convert.ToString(dt.Rows[i]["PassengerPhone"]);
                    List.Add(r);
                }
                return List;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 根據多重條件回傳一批訂單
        /// </summary>
        /// <param name="qs">QOrderCondStruct</param>
        /// <returns></returns>
        public List<OReservation> GetReservationsMultipleCondition(QOrderCondStruct qs, int take, int skip)
        {
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            sb.Append("SELECT r.ReservationNo, r.UserID, r.ServiceType, r.TakeDate, r.TimeSegment, r.SelectionType, r.PickCartype, r.FlightNo, r.ScheduleFlightTime, r.Airport,r.Ternimal,r.PickupCity, ");
            sb.Append("r.PickupDistinct, r.PickupVillage, r.PickupAddress, X(PickupGPS) as plng, Y(PickupGPS) as plat, r.TakeoffCity, r.TakeoffDistinct, r.TakeoffVillage, r.TakeoffAddress, X(TakeoffGPS) as tlng ,Y(TakeoffGPS) as tlat , r.PassengerCnt, ");
            sb.Append("r.BaggageCnt, r.MaxFlag, r.Price, r.Coupon, r.InvoiceData, r.CreditData, r.ProcessStage, r.InvoiceNum, r.CreateTime, r.UpdTime,AddBaggageCnt,PassengerName, PassengerPhone  ");
            sb.Append("FROM reservation_sheet r, account u ");
            sb.Append("where r.UserID = u.UserID ");

            if (qs.UserID > 0)
            {
                sb.Append(" And r.UserID = @id ");
                parms.Add(new MySqlParameter("@id", qs.UserID));
            }

            if (qs.MobilePhone != "")
            {
                sb.Append(" And u.MobilePhone = @phone ");
                parms.Add(new MySqlParameter("@phone", qs.MobilePhone));
            }

            if (qs.OrderNo != "")
            {
                sb.Append(" And r.ReservationNo = @No ");
                parms.Add(new MySqlParameter("@No", qs.OrderNo));
            }

            if (qs.QEDate != "" && qs.QSDate != "")
            {
                sb.Append(" And r.TakeDate between @sdate and @edate ");
                parms.Add(new MySqlParameter("@sdate", qs.QSDate));
                parms.Add(new MySqlParameter("@edate", qs.QEDate));
            }

            if (take != 0)
            {
                sb.Append(" limit " + (1 + skip).ToString() + "," + (1 + skip + take).ToString());
            }

            DataTable dt = new DataTable();
            List<OReservation> List = new List<OReservation>();
            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    OReservation r = new OReservation();
                    r.BaggageCnt = Convert.ToInt32(dt.Rows[i]["BaggageCnt"]);
                    r.ChooseType = Convert.ToString(dt.Rows[i]["SelectionType"]);
                    r.Coupon = Convert.ToString(dt.Rows[i]["Coupon"]);
                    r.Credit = Convert.ToString(dt.Rows[i]["CreditData"]);
                    if (r.ChooseType == "F")
                    {
                        r.FlightDate = Convert.ToDateTime(dt.Rows[i]["ScheduleFlightTime"]).ToString("yyyyMMdd");
                        r.FlightNo = Convert.ToString(dt.Rows[i]["FlightNo"]);
                        r.FlightTime = Convert.ToDateTime(dt.Rows[i]["ScheduleFlightTime"]);
                    }
                    r.Airport = Convert.ToString(dt.Rows[i]["Airport"]);
                    r.Ternimal = Convert.ToString(dt.Rows[i]["Ternimal"]);
                    r.Invoice = Convert.ToString(dt.Rows[i]["InvoiceData"]);
                    r.MaxFlag = Convert.ToString(dt.Rows[i]["MaxFlag"]);
                    r.OrderTime = Convert.ToDateTime(dt.Rows[i]["CreateTime"]);
                    r.PassengerCnt = Convert.ToInt32(dt.Rows[i]["PassengerCnt"]);
                    r.PickCartype = Convert.ToString(dt.Rows[i]["PickCartype"]);
                    r.PickupAddress = Convert.ToString(dt.Rows[i]["PickupAddress"]);
                    r.PickupCity = Convert.ToString(dt.Rows[i]["PickupCity"]);
                    r.PickupDistinct = Convert.ToString(dt.Rows[i]["PickupDistinct"]);
                    r.PickupVillage = Convert.ToString(dt.Rows[i]["PickupVillage"]);
                    r.PreferTime = Convert.ToString(dt.Rows[i]["TimeSegment"]);
                    r.ProcessStage = Convert.ToString(dt.Rows[i]["ProcessStage"]);
                    r.ReservationNo = Convert.ToString(dt.Rows[i]["ReservationNo"]);
                    r.ServeDate = Convert.ToString(dt.Rows[i]["TakeDate"]);
                    r.ServiceType = Convert.ToString(dt.Rows[i]["ServiceType"]);
                    r.TakeoffAddress = Convert.ToString(dt.Rows[i]["TakeoffAddress"]);
                    r.TakeoffCity = Convert.ToString(dt.Rows[i]["TakeoffCity"]);
                    r.TakeoffDistinct = Convert.ToString(dt.Rows[i]["TakeoffDistinct"]);
                    r.TakeoffVillage = Convert.ToString(dt.Rows[i]["TakeoffVillage"]);
                    r.UserID = Convert.ToInt32(dt.Rows[i]["UserID"]);
                    r.EIN = Convert.ToString(dt.Rows[i]["InvoiceNum"]);
                    r.TakeoffGPS = new OSpatialGPS(Convert.ToDouble(dt.Rows[i]["tlat"]), Convert.ToDouble(dt.Rows[i]["tlng"]));
                    r.PickupGPS = new OSpatialGPS(Convert.ToDouble(dt.Rows[i]["plat"]), Convert.ToDouble(dt.Rows[i]["plng"]));
                    r.AddBagCnt = Convert.ToInt32(dt.Rows[i]["AddBaggageCnt"]);
                    r.PassengerName = Convert.ToString(dt.Rows[i]["PassengerName"]);
                    r.PassengerPhone = Convert.ToString(dt.Rows[i]["PassengerPhone"]);


                    List.Add(r);
                }
                return List;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="UserID"></param>
        /// <returns></returns>
        public DataTable GetReservationsByUser(int UserID)
        {
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            sb.Append("SELECT r.ReservationNo, r.UserID, r.ServiceType, r.TakeDate, r.TimeSegment, r.SelectionType, r.PickCartype, r.FlightNo, r.ScheduleFlightTime,r.Airport,r.Ternimal, r.PickupCity, ");
            sb.Append("r.PickupDistinct, r.PickupVillage, r.PickupAddress, X(PickupGPS) as plng, Y(PickupGPS) as plat, r.TakeoffCity, r.TakeoffDistinct, r.TakeoffVillage, r.TakeoffAddress, X(TakeoffGPS) as tlng ,Y(TakeoffGPS) as tlat , r.PassengerCnt, ");
            sb.Append("r.BaggageCnt, r.MaxFlag, r.Price, r.Coupon, r.InvoiceData, r.CreditData, r.ProcessStage, r.InvoiceNum, r.CreateTime, r.UpdTime,r.AddBaggageCnt,PassengerName, PassengerPhone   ");
            sb.Append("FROM reservation_sheet r, account u ");
            sb.Append("where r.UserID = u.UserID ");


            sb.Append(" And r.UserID = @id ");
            sb.Append(" And r.ProcessStage not in ('X','Y','W') ");
            parms.Add(new MySqlParameter("@id", UserID));

            DataTable dt = new DataTable();
            List<OReservation> List = new List<OReservation>();
            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return dt;
        }

        /// <summary>
        /// 更新訂單服務地點
        /// </summary>
        /// <param name="Rno">訂單編號</param>
        /// <param name="City">城市數列</param>
        /// <param name="Distinct">區數列</param>
        /// <param name="Village">里數列</param>
        ///  <param name="lat">緯度數列</param>
        ///  <param name="lng">經度數列</param>
        /// <returns></returns>
        public bool UpdReservationServicePostion(string Rno, string ServiceType, List<string> City, List<string> Distinct, List<string> Village, List<string> Address, List<string> lat, List<string> lng)
        {
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();

            sb.Append("update reservation_sheet set ");
            if (ServiceType == "O")
                sb.Append("PickupCity = @city, PickupDistinct=@distinct, PickupVillage = @vil, PickupAddress = @add, PickupGPS = GEOFROMTEXT(@gps) ");
            else
                sb.Append("TakeoffCity = @city, TakeoffDistinct=@distinct, TakeoffVillage = @vil, TakeoffAddress = @add, TakeoffGPS = GEOFROMTEXT(@gps) ");

            parms.Add(new MySqlParameter("@city", City[1]));
            parms.Add(new MySqlParameter("@distinct", Distinct[1]));
            parms.Add(new MySqlParameter("@vil", Village[1]));
            parms.Add(new MySqlParameter("@add", Address[1]));
            parms.Add(new MySqlParameter("@gps", "POINT( " + lng[1] + " " + lat[1] + ")"));

            sb.Append(" where ReservationNo = @no ");

            if (ServiceType == "O")
                sb.Append(" And PickupCity = @city2 AND PickupDistinct=@distinct2 AND  PickupVillage = @vil2 AND PickupAddress = @add2 AND PickupGPS = GEOFROMTEXT(@gps2) ");
            else
                sb.Append(" AND TakeoffCity = @city2 AND  TakeoffDistinct=@distinct2 AND TakeoffVillage = @vil2 AND TakeoffAddress = @add2 AND TakeoffGPS = GEOFROMTEXT(@gps2) ");

            parms.Add(new MySqlParameter("@city2", City[0]));
            parms.Add(new MySqlParameter("@distinct2", Distinct[0]));
            parms.Add(new MySqlParameter("@vil2", Village[0]));
            parms.Add(new MySqlParameter("@add2", Address[0]));
            parms.Add(new MySqlParameter("@gps2", "POINT( " + lng[0] + " " + lat[0] + ")"));

            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    if (conn.executeUpdateQuery(sb.ToString(), parms.ToArray()) > 0)
                        return true;
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        /// <summary>
        /// 回傳待媒合訂單
        /// </summary>
        /// <param name="sdate">媒合日期</param>
        /// <returns></returns>
        public DataTable GetReservationForMatch(string sdate)
        {

            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            sb.Append("SELECT ReservationNo, UserID, ServiceType, TakeDate, TimeSegment, SelectionType, PickCartype, FlightNo, ScheduleFlightTime, PickupCity, ");
            sb.Append("PickupDistinct, PickupVillage, PickupAddress, X(PickupGPS) as plng, Y(PickupGPS) as plat, TakeoffCity, TakeoffDistinct, TakeoffVillage, TakeoffAddress, X(TakeoffGPS) as tlng ,Y(TakeoffGPS) as tlat , PassengerCnt, ");
            sb.Append("BaggageCnt, MaxFlag, Price, Coupon, InvoiceData, CreditData, ProcessStage, InvoiceNum, CreateTime, UpdTime ");
            sb.Append("FROM reservation_sheet where TakeDate = @sdate  AND ProcessStage = '0' order by TakeDate,TimeSegment asc ");

            parms.Add(new MySqlParameter("@sdate", sdate));
            DataTable dt = new DataTable();
            List<OReservation> List = new List<OReservation>();
            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    return dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                }
            }
            catch (MySqlException ex)
            {
                throw ex;
            }
        }

        public bool CancelReservation(string Rno, string OldStatus)
        {
            DALDispatch dald = new DALDispatch(this._ConnectionString);

            int rtn = 0;
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            OReservation or;

            try
            {
                BAL.BALReservation balr = new BAL.BALReservation(this._ConnectionString);
                or = balr.GetReservationByNo(Rno);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            sb.Append("UPDATE reservation_sheet SET ProcessStage= 'X'  WHERE ReservationNo = @no and ProcessStage = @old");
            parms.Add(new MySqlParameter("@no", Rno));
            parms.Add(new MySqlParameter("@old", OldStatus));

            using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
            {
                try
                {
                    rtn = conn.executeDeleteQuery(sb.ToString(), parms.ToArray());

                    if (rtn <= 0)
                        return false;
                    else
                        return true;

                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }


        public string GetOrderDispatchData(string Rno)
        {
            List<MySqlParameter> parms = new List<MySqlParameter>();
            StringBuilder sb = new StringBuilder();
            DataTable dt = new DataTable();
            sb.Append("select r.ScheduleServeTime,d.CarType,d.CarNo,d.DriverID ");
            sb.Append("from dispatch_reservation r,dispatch_sheet d ");
            sb.Append("where  r.DispatchNo = d.DispatchNo and  r.ReservationNo = @no and r.ServiceRemark <> 'X' ");

            parms.Add(new MySqlParameter("@no", Rno));

            using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
            {
                dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
            }

            if (dt.Rows.Count == 0)
                return "";
            else if (dt.Rows.Count > 1)
                return "Error";
            else
            {
                string time, type, CarNo, DriverName, MPhone;

                DALEmployee dale = new DALEmployee(this._ConnectionString);
                OEmployee oe = dale.GetEmployeeInfoByID(Convert.ToInt32(dt.Rows[0]["DriverID"]));
                if (oe == null)
                {
                    DriverName = "";
                    MPhone = "";
                }
                else
                {
                    DriverName = oe.Name;
                    MPhone = oe.MobilePhone;
                }

                time = Convert.ToDateTime(dt.Rows[0]["ScheduleServeTime"]).ToString("yyyy/MM/dd HH:mm");
                type = Convert.ToString(dt.Rows[0]["CarType"]);
                CarNo = Convert.ToString(dt.Rows[0]["CarNo"]);
                return time + "," + type + "," + CarNo + "," + DriverName + "," + MPhone;
            }
        }

        public DataTable GetFloatingPriceByOrder(string Rno)
        {
            List<MySqlParameter> parms = new List<MySqlParameter>();
            StringBuilder sb = new StringBuilder();
            DataTable dt = new DataTable();

            sb.Append("select price, CreateTime from price_float_record where ReservationNo = @no order by CreateTime ASC ");

            parms.Add(new MySqlParameter("@no", Rno));

            using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
            {
                dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
            }
            return dt;
        }
    }

    public class DALFlight : CallcarDAL
    {
        public DALFlight(string ConnectionString) : base(ConnectionString) { }

        /// <summary>
        /// 回傳表定班機資訊
        /// </summary>
        /// <param name="FlightDate">航班日期</param>
        /// <param name="FlightNo">航班編號</param>
        /// <returns></returns>
        public DataTable GetFlightInfo(string FlightDate, string FlightNo)
        {
            DataTable dt = new DataTable();
            using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
            {
                List<MySqlParameter> parms = new List<MySqlParameter>();
                StringBuilder sb = new StringBuilder();
                sb.Append("select FlightType, AirlineID, FlightNo, DepartureAirportID, DepartureTime, ArrivalAirportID, ArrivalTime, FlyDay, Terminal ");
                sb.Append("from flight_schedule where @date between StartDate and EndDate and FlightNo = @no ");

                parms.Add(new MySqlParameter("@date", FlightDate));
                parms.Add(new MySqlParameter("@no", FlightNo));
                try
                {
                    dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                }
                catch (MySqlException ex)
                {
                    throw new Exception(ex.Message);
                }

                return dt;
            }
        }


        /// <summary>
        /// 查詢資料庫是否已經有航班資料
        /// </summary>
        /// <param name="FlightDate">航班日期</param>
        /// <param name="FlightNo">航班編號</param>
        /// <returns></returns>
        public DataTable GetFlightInfoAPI(string ServiceType, string FlightDate, string airline, string FlightNo)
        {
            DataTable dt = new DataTable();
            using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
            {
                List<MySqlParameter> parms = new List<MySqlParameter>();
                StringBuilder sb = new StringBuilder();
                sb.Append("SELECT SeqNo, FlightType, AirlineID, FlightNo, LocalDepartureDate, LocalDepartureTime, LocalArrivalDate, LocalArrivalTime, DepartureAirport, ArrivalAirport, DepartureTernimal, ArrivalTernimal, Stops, CreateTime, UpdTime ");
                sb.Append("FROM flight_schedule2 ");
                sb.Append("Where FlightType = @type and AirlineID = @airline and FlightNo = @no");

                if (ServiceType == "O")
                    sb.Append("and LocalDepartureDate = @date ");
                else
                    sb.Append("and LocalArrivalDate = @date ");


                parms.Add(new MySqlParameter("@type", ServiceType));
                parms.Add(new MySqlParameter("@airline", airline));
                parms.Add(new MySqlParameter("@no", FlightNo));
                parms.Add(new MySqlParameter("@date", FlightDate));

                try
                {
                    dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                }
                catch (MySqlException ex)
                {
                    throw new Exception(ex.Message);
                }

                return dt;
            }
        }
        public DataTable GetAirportInfo(string AirportCode)
        {
            DataTable dt = new DataTable();
            using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
            {
                List<MySqlParameter> parms = new List<MySqlParameter>();
                StringBuilder sb = new StringBuilder();
                sb.Append("SELECT AirportCode, AirportName, CityName, CountryName, CountryAbbrev ");
                sb.Append("FROM airport_info where AirportCode = @code ");

                parms.Add(new MySqlParameter("@code", AirportCode));
                try
                {
                    dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                }
                catch (MySqlException ex)
                {
                    throw new Exception(ex.Message);
                }

                return dt;
            }
        }

        public DataTable GetAirlineInfo(string IATACode)
        {
            DataTable dt = new DataTable();
            using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
            {
                List<MySqlParameter> parms = new List<MySqlParameter>();
                StringBuilder sb = new StringBuilder();
                sb.Append("SELECT IATA, ICAO, AirlineName, CallSign, Country FROM airline_info where IATA = @code");

                parms.Add(new MySqlParameter("@code", IATACode));
                try
                {
                    dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                }
                catch (MySqlException ex)
                {
                    throw new Exception(ex.Message);
                }

                return dt;
            }
        }
    }

    /// <summary>
    /// 選車次的DAL
    /// </summary>
    public class DALPickCar : CallcarDAL
    {
        public DALPickCar(string ConnectionString) : base(ConnectionString) { }

        /// <summary>
        /// 回傳依時段未滿車之撿車資料
        /// </summary>
        /// <param name="Date"></param>
        /// <param name="Time"></param>
        /// <param name="Type"></param>
        /// <returns></returns>
        public List<OPickCarInfo> GetPickCarsList(string Date, string Time, string Type)
        {
            List<OPickCarInfo> List = new List<OPickCarInfo>();
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            DataTable dt = new DataTable();

            sb.Append("select distinct d.DispatchNo, d.PassengerCnt, d.BaggageCnt, d.StopCnt, d.MaxFlag, d.FleetID, d.CarType, d.CarNo, d.DriverID,r.ServeSeq,Y(r.LocationGPS) as lat, X(r.LocationGPS) as lng,r.ScheduleServeTime,r.ReservationNo ");
            sb.Append("from dispatch_sheet d,dispatch_reservation r ");
            sb.Append("where  d.ServiceType = @type and  d.ServeDate = @date and  d.TimeSegment = @time ");
            sb.Append("and d.DispatchNo = r.DispatchNo and d.StopCnt = 1 ");
            sb.Append("order by d.DispatchNo, r.ScheduleServeTime ASC ");
            using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
            {
                parms.Add(new MySqlParameter("@type", Type));
                parms.Add(new MySqlParameter("@date", Date));
                parms.Add(new MySqlParameter("@time", Time));

                try
                {
                    dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                }
                catch (MySqlException ex)
                {
                    throw new Exception(ex.Message);
                }
            }
            if (dt.Rows.Count > 0)
            {
                string DispatchNo = "";
                try
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {

                        if (DispatchNo != Convert.ToString(dt.Rows[i]["DispatchNo"]))
                        {
                            OPickCarInfo c = new OPickCarInfo();
                            c.CarNo = Convert.ToString(dt.Rows[i]["CarNo"]);
                            c.DispatchNo = Convert.ToString(dt.Rows[i]["DispatchNo"]);
                            c.DriverID = Convert.ToString(dt.Rows[i]["DriverID"]);
                            c.RemainBCapacity = Convert.ToInt32(dt.Rows[i]["BaggageCnt"]);
                            c.RemainPCapacity = Convert.ToInt32(dt.Rows[i]["PassengerCnt"]);
                            c.CarNo = Convert.ToString(dt.Rows[i]["CarNo"]);
                            c.StopCnt = Convert.ToInt32(dt.Rows[i]["StopCnt"]);
                            c.MaxFlag = Convert.ToString(dt.Rows[i]["MaxFlag"]);
                            c.CarType = Convert.ToString(dt.Rows[i]["CarType"]);
                            c.TakeDate = Date;
                            c.TimeRange = Time;
                            c.ServiceType = Type;


                            OPickCarInfo.DispatchUnit u = new OPickCarInfo.DispatchUnit();
                            u.No = Convert.ToString(dt.Rows[i]["ReservationNo"]);
                            u.SeqNo = Convert.ToInt32(dt.Rows[i]["ServeSeq"]);
                            u.ScheduleTime = Convert.ToDateTime(dt.Rows[i]["ScheduleServeTime"]);
                            u.lat = Convert.ToDouble(dt.Rows[i]["lat"]);
                            u.lng = Convert.ToDouble(dt.Rows[i]["lng"]);
                            c.Reservations.Add(u);

                            List.Add(c);

                        }
                        else
                        {
                            OPickCarInfo.DispatchUnit u = new OPickCarInfo.DispatchUnit();
                            u.No = Convert.ToString(dt.Rows[i]["ReservationNo"]);
                            u.SeqNo = Convert.ToInt32(dt.Rows[i]["ServeSeq"]);
                            u.ScheduleTime = Convert.ToDateTime(dt.Rows[i]["ScheduleServeTime"]);
                            u.lat = Convert.ToDouble(dt.Rows[i]["lat"]);
                            u.lng = Convert.ToDouble(dt.Rows[i]["lng"]);
                            List[List.Count - 1].Reservations.Add(u);
                        }
                        DispatchNo = Convert.ToString(dt.Rows[i]["DispatchNo"]);
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }

                return List;

            }
            else
                return null;
        }

        /// <summary>
        /// 回傳依時段未滿車之撿車資料 (用時間查詢)
        /// </summary>
        /// <param name="Date"></param>
        /// <param name="Time"></param>
        /// <param name="Type"></param>
        /// <returns></returns>
        public List<OPickCarInfo> GetPickCarsListByTime(DateTime sTime, DateTime eTime, string Type)
        {
            List<OPickCarInfo> List = new List<OPickCarInfo>();
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            DataTable dt = new DataTable();

            sb.Append("select distinct d.ServeDate, d.DispatchNo, d.TimeSegment,d.PassengerCnt, d.BaggageCnt, d.StopCnt, d.MaxFlag, d.FleetID, d.CarType, d.CarNo, d.DriverID,r.ServeSeq,Y(r.LocationGPS) as lat, X(r.LocationGPS) as lng,r.ScheduleServeTime,r.ReservationNo ");
            sb.Append("from dispatch_sheet d,dispatch_reservation r ");
            sb.Append("where  d.ServiceType = @type and  d.ServeDate = @date and r.ScheduleServeTime between @sTime and @eTime ");
            sb.Append("and d.DispatchNo = r.DispatchNo and d.StopCnt < 3 ");
            sb.Append("order by d.DispatchNo, r.ScheduleServeTime ASC ");
            using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
            {
                parms.Add(new MySqlParameter("@type", Type));
                parms.Add(new MySqlParameter("@date", sTime.ToString("yyyyMMdd")));
                parms.Add(new MySqlParameter("@stime", sTime));
                parms.Add(new MySqlParameter("@etime", eTime));

                try
                {
                    dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                }
                catch (MySqlException ex)
                {
                    throw new Exception(ex.Message);
                }
            }
            if (dt.Rows.Count > 0)
            {
                string DispatchNo = "";
                try
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {

                        if (DispatchNo != Convert.ToString(dt.Rows[i]["DispatchNo"]))
                        {
                            OPickCarInfo c = new OPickCarInfo();
                            c.CarNo = Convert.ToString(dt.Rows[i]["CarNo"]);
                            c.DispatchNo = Convert.ToString(dt.Rows[i]["DispatchNo"]);
                            c.DriverID = Convert.ToString(dt.Rows[i]["DriverID"]);
                            c.RemainBCapacity = Convert.ToInt32(dt.Rows[i]["BaggageCnt"]);
                            c.RemainPCapacity = Convert.ToInt32(dt.Rows[i]["PassengerCnt"]);
                            c.CarNo = Convert.ToString(dt.Rows[i]["CarNo"]);
                            c.StopCnt = Convert.ToInt32(dt.Rows[i]["StopCnt"]);
                            c.MaxFlag = Convert.ToString(dt.Rows[i]["MaxFlag"]);
                            c.CarType = Convert.ToString(dt.Rows[i]["CarType"]);
                            c.TakeDate = Convert.ToString(dt.Rows[i]["ServeDate"]);
                            c.TimeRange = Convert.ToString(dt.Rows[i]["TimeSegment"]);
                            c.ServiceType = Type;


                            OPickCarInfo.DispatchUnit u = new OPickCarInfo.DispatchUnit();
                            u.No = Convert.ToString(dt.Rows[i]["ReservationNo"]);
                            u.SeqNo = Convert.ToInt32(dt.Rows[i]["ServeSeq"]);
                            u.ScheduleTime = Convert.ToDateTime(dt.Rows[i]["ScheduleServeTime"]);
                            u.lat = Convert.ToDouble(dt.Rows[i]["lat"]);
                            u.lng = Convert.ToDouble(dt.Rows[i]["lng"]);
                            c.Reservations.Add(u);

                            List.Add(c);

                        }
                        else
                        {
                            OPickCarInfo.DispatchUnit u = new OPickCarInfo.DispatchUnit();
                            u.No = Convert.ToString(dt.Rows[i]["ReservationNo"]);
                            u.SeqNo = Convert.ToInt32(dt.Rows[i]["ServeSeq"]);
                            u.ScheduleTime = Convert.ToDateTime(dt.Rows[i]["ScheduleServeTime"]);
                            u.lat = Convert.ToDouble(dt.Rows[i]["lat"]);
                            u.lng = Convert.ToDouble(dt.Rows[i]["lng"]);
                            List[List.Count - 1].Reservations.Add(u);
                        }
                        DispatchNo = Convert.ToString(dt.Rows[i]["DispatchNo"]);
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }

                return List;

            }
            else
                return null;
        }

        /// <summary>
        /// 回傳全天未滿車之撿車資料
        /// </summary>
        /// <param name="Date"></param>
        /// <param name="Type"></param>
        /// <returns></returns>
        public List<OPickCarInfo> GetPickCarsList(string Date, string Type)
        {
            List<OPickCarInfo> List = new List<OPickCarInfo>();
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            DataTable dt = new DataTable();

            sb.Append("select distinct d.DispatchNo,d.TimeSegment, d.PassengerCnt, d.BaggageCnt, d.StopCnt, d.MaxFlag, d.FleetID, d.CarType, d.CarNo, d.DriverID,r.ServeSeq,Y(r.LocationGPS) as lat, X(r.LocationGPS) as lng,r.ScheduleServeTime,r.ReservationNo ");
            sb.Append("from dispatch_sheet d,dispatch_reservation r ");
            sb.Append("where  d.ServiceType = @type and  d.ServeDate = @date  ");
            sb.Append("and d.DispatchNo = r.DispatchNo and d.StopCnt < 3 ");
            sb.Append("order by d.DispatchNo, r.ScheduleServeTime ASC ");
            using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
            {
                parms.Add(new MySqlParameter("@type", Type));
                parms.Add(new MySqlParameter("@date", Date));

                try
                {
                    dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                }
                catch (MySqlException ex)
                {
                    throw new Exception(ex.Message);
                }
            }
            if (dt.Rows.Count > 0)
            {
                string DispatchNo = "";
                try
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {

                        if (DispatchNo != Convert.ToString(dt.Rows[i]["DispatchNo"]))
                        {
                            OPickCarInfo c = new OPickCarInfo();
                            c.CarNo = Convert.ToString(dt.Rows[i]["CarNo"]);
                            c.DispatchNo = Convert.ToString(dt.Rows[i]["DispatchNo"]);
                            c.DriverID = Convert.ToString(dt.Rows[i]["DriverID"]);
                            c.RemainBCapacity = Convert.ToInt32(dt.Rows[i]["BaggageCnt"]);
                            c.RemainPCapacity = Convert.ToInt32(dt.Rows[i]["PassengerCnt"]);
                            c.CarNo = Convert.ToString(dt.Rows[i]["CarNo"]);
                            c.StopCnt = Convert.ToInt32(dt.Rows[i]["StopCnt"]);
                            c.MaxFlag = Convert.ToString(dt.Rows[i]["MaxFlag"]);
                            c.CarType = Convert.ToString(dt.Rows[i]["CarType"]);
                            c.TakeDate = Date;
                            c.TimeRange = Convert.ToString(dt.Rows[i]["TimeSegment"]);
                            c.ServiceType = Type;


                            OPickCarInfo.DispatchUnit u = new OPickCarInfo.DispatchUnit();
                            u.No = Convert.ToString(dt.Rows[i]["ReservationNo"]);
                            u.SeqNo = Convert.ToInt32(dt.Rows[i]["ServeSeq"]);
                            u.ScheduleTime = Convert.ToDateTime(dt.Rows[i]["ScheduleServeTime"]);
                            u.lat = Convert.ToDouble(dt.Rows[i]["lat"]);
                            u.lng = Convert.ToDouble(dt.Rows[i]["lng"]);
                            c.Reservations.Add(u);

                            List.Add(c);

                        }
                        else
                        {
                            OPickCarInfo.DispatchUnit u = new OPickCarInfo.DispatchUnit();
                            u.No = Convert.ToString(dt.Rows[i]["ReservationNo"]);
                            u.SeqNo = Convert.ToInt32(dt.Rows[i]["ServeSeq"]);
                            u.ScheduleTime = Convert.ToDateTime(dt.Rows[i]["ScheduleServeTime"]);
                            u.lat = Convert.ToDouble(dt.Rows[i]["lat"]);
                            u.lng = Convert.ToDouble(dt.Rows[i]["lng"]);
                            List[List.Count - 1].Reservations.Add(u);
                        }
                        DispatchNo = Convert.ToString(dt.Rows[i]["DispatchNo"]);
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }

                return List;

            }
            else
                return null;
        }
    }


    public class DALDispatch : CallcarDAL
    {
        public DALDispatch(string ConnectionString) : base(ConnectionString) { }

        /// <summary>
        /// 取得一筆派車單主檔
        /// </summary>
        /// <param name="No">派車單號</param>
        /// <returns></returns>
        public ODispatchSheet GetDispatchSheetBrief(string No)
        {
            ODispatchSheet sheet = new ODispatchSheet();
            DALEmployee oe = new DALEmployee(this._ConnectionString);

            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            sb.Append("select s.DispatchNo,s.ServiceType,s.ServeDate,s.TimeSegment,s.PassengerCnt,s.BaggageCnt,s.StopCnt,s.FleetID,s.CarNo,s.CarType,s.DriverID ");
            sb.Append("from dispatch_sheet s  ");
            sb.Append("where  s.DispatchNo = @no");

            parms.Add(new MySqlParameter("@no", No));

            using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
            {
                DataTable dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                if (dt.Rows.Count > 0)
                {
                    sheet.BaggageCnt = Convert.ToInt32(dt.Rows[0]["BaggageCnt"]);
                    sheet.CarNo = Convert.ToString(dt.Rows[0]["CarNo"]);
                    sheet.CarType = Convert.ToString(dt.Rows[0]["CarType"]);
                    sheet.DispatchNo = Convert.ToString(dt.Rows[0]["DispatchNo"]);
                    sheet.DriverID = Convert.ToInt32(dt.Rows[0]["DriverID"]);
                    if (sheet.DriverID != 0)
                        sheet.DriverName = oe.GetEmployeeInfoByID(sheet.DriverID).Name;
                    else
                        sheet.DriverName = "";
                    sheet.PassengerCnt = Convert.ToInt32(dt.Rows[0]["PassengerCnt"]);
                    sheet.ServiceCnt = Convert.ToInt32(dt.Rows[0]["StopCnt"]);
                    sheet.ServiceType = Convert.ToString(dt.Rows[0]["ServiceType"]);
                    sheet.TakeDate = Convert.ToString(dt.Rows[0]["ServeDate"]);
                }
            }
            return sheet;
        }

        /// <summary>
        /// 取得派車單所服務的訂單資料
        /// </summary>
        /// <param name="DNo">派車單號</param>
        /// <returns></returns>
        public List<ODpServiceUnit> GetDispatchService(string DNo)
        {
            List<ODpServiceUnit> List = new List<ODpServiceUnit>();

            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            sb.Append("select d.DispatchNo, d.ReservationNo, d.ServeSeq, d.ScheduleServeTime, d.ActualServreTime,X(d.LocationGPS) as lng ,Y(d.LocationGPS) as lat , d.ServiceRemark,r.ServiceType,r.PickupCity,r.PickupDistinct,r.PickupAddress,");
            sb.Append("r.TakeoffCity,r.TakeoffDistinct,r.TakeoffAddress,r.PassengerCnt,r.BaggageCnt ");
            sb.Append("from dispatch_reservation d,reservation_sheet r  ");
            sb.Append("where d.ReservationNo = r.ReservationNo and  d.DispatchNo = @no Order by d.ServeSeq ASC");

            parms.Add(new MySqlParameter("@no", DNo));

            using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
            {
                DataTable dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        ODpServiceUnit sheet = new ODpServiceUnit();
                        sheet.BaggageCnt = Convert.ToInt32(dt.Rows[i]["BaggageCnt"]);
                        if (dt.Rows[i]["ActualServreTime"] != DBNull.Value)
                            sheet.ActualTime = Convert.ToDateTime(dt.Rows[i]["ActualServreTime"]);

                        sheet.DispatchNo = Convert.ToString(dt.Rows[i]["DispatchNo"]);
                        sheet.PassengerCnt = Convert.ToInt32(dt.Rows[i]["PassengerCnt"]);
                        sheet.PassengerCnt = Convert.ToInt32(dt.Rows[i]["BaggageCnt"]);
                        sheet.ReservationNo = Convert.ToString(dt.Rows[i]["ReservationNo"]);
                        sheet.ScheduleTime = Convert.ToDateTime(dt.Rows[i]["ScheduleServeTime"]);
                        sheet.ServiceGPS = new OSpatialGPS(Convert.ToDouble(dt.Rows[i]["lat"]), Convert.ToDouble(dt.Rows[i]["lng"]));
                        sheet.ServiceOrder = Convert.ToInt32(dt.Rows[i]["ServeSeq"]);
                        sheet.ServiceRemark = Convert.ToString(dt.Rows[i]["ServiceRemark"]);

                        string ServiceType = Convert.ToString(dt.Rows[i]["ServiceType"]);
                        if (ServiceType == "I")
                        {
                            sheet.MainAddress = Convert.ToString(dt.Rows[i]["TakeoffAddress"]);
                            sheet.AirportAddress = Convert.ToString(dt.Rows[i]["PickupAddress"]);
                        }
                        else
                        {
                            sheet.MainAddress = Convert.ToString(dt.Rows[i]["PickupAddress"]);
                            sheet.AirportAddress = Convert.ToString(dt.Rows[i]["TakeoffAddress"]);
                        }
                        List.Add(sheet);
                    }
                }
            }
            return List;
        }

        /// <summary>
        /// 取得司機時段內尚未上傳的派車單
        /// </summary>
        /// <param name="EmpID">司機編號</param>
        /// <param name="StartDate">查詢開始日</param>
        /// <param name="EndDate">查詢結束日</param>
        /// <returns></returns>
        public List<ODispatchSheet> GetUnServeDispatchSheets(int EmpID, string StartDate, string EndDate)//, string StartTime, string EndTime)
        {
            List<ODispatchSheet> List = new List<ODispatchSheet>();


            List<MySqlParameter> parms = new List<MySqlParameter>();
            DataTable dt = new DataTable();
            parms.Add(new MySqlParameter("@id", EmpID));
            parms.Add(new MySqlParameter("@sdate", StartDate));
            parms.Add(new MySqlParameter("@edate", EndDate));
            //parms.Add(new MySqlParameter("@stime", StartTime));
            //parms.Add(new MySqlParameter("@etime", EndTime));
            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    dt = conn.executeSelectQuery(GetDriverDispatchSQL("UnServed"), parms.ToArray());
                }
                if (dt.Rows.Count > 0)
                {
                    string DispatchNo = "";

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        if (DispatchNo != Convert.ToString(dt.Rows[i]["DispatchNo"]))
                        {
                            ODispatchSheet sheet = new ODispatchSheet();
                            sheet.BaggageCnt = Convert.ToInt32(dt.Rows[i]["BaggageCnt"]);
                            sheet.CarNo = Convert.ToString(dt.Rows[i]["CarNo"]);
                            sheet.CarType = Convert.ToString(dt.Rows[i]["CarType"]);
                            sheet.DispatchNo = Convert.ToString(dt.Rows[i]["DispatchNo"]);
                            sheet.DriverID = Convert.ToInt32(dt.Rows[i]["DriverID"]);
                            sheet.DriverName = Convert.ToString(dt.Rows[i]["UserName"]);
                            sheet.PassengerCnt = Convert.ToInt32(dt.Rows[i]["PassengerCnt"]);
                            sheet.ServiceCnt = Convert.ToInt32(dt.Rows[i]["StopCnt"]);
                            sheet.ServiceType = Convert.ToString(dt.Rows[i]["ServiceType"]);
                            sheet.TakeDate = Convert.ToString(dt.Rows[i]["ServeDate"]);
                            sheet.ServiceRemark = Convert.ToString(dt.Rows[i]["ServiceRemarkS"]);
                            sheet.UpdFlag = Convert.ToString(dt.Rows[i]["UpdFlag"]);

                            ODpServiceUnit u = new ODpServiceUnit();

                            u.BaggageCnt = Convert.ToInt32(dt.Rows[i]["BaggageCnt"]);

                            if (dt.Rows[i]["ActualServreTime"] != DBNull.Value)
                                u.ActualTime = Convert.ToDateTime(dt.Rows[i]["ActualServreTime"]);
                            u.DispatchNo = Convert.ToString(dt.Rows[i]["DispatchNo"]);
                            u.FlightNo = Convert.ToString(dt.Rows[i]["FlightNo"]);
                            u.PassengerCnt = Convert.ToInt32(dt.Rows[i]["PassengerCntR"]);
                            u.BaggageCnt = Convert.ToInt32(dt.Rows[i]["BaggageCntR"]);
                            u.ReservationNo = Convert.ToString(dt.Rows[i]["ReservationNo"]);
                            u.ScheduleTime = Convert.ToDateTime(dt.Rows[i]["ScheduleServeTime"]);
                            u.ServiceGPS = new OSpatialGPS(Convert.ToDouble(dt.Rows[i]["lat"]), Convert.ToDouble(dt.Rows[i]["lng"]));
                            u.ServiceOrder = Convert.ToInt32(dt.Rows[i]["ServeSeq"]);
                            u.ServiceRemark = Convert.ToString(dt.Rows[i]["ServiceRemark"]);
                            u.AddBaggageCnt = Convert.ToInt32(dt.Rows[i]["AddBaggageCnt"]);

                            string ServiceType = Convert.ToString(dt.Rows[i]["ServiceType"]);
                            if (ServiceType == "I")
                            {
                                u.MainAddress = Convert.ToString(dt.Rows[i]["TakeoffAddress"]);
                                u.AirportAddress = Convert.ToString(dt.Rows[i]["PickupAddress"]);
                            }
                            else
                            {
                                u.MainAddress = Convert.ToString(dt.Rows[i]["PickupAddress"]);
                                u.AirportAddress = Convert.ToString(dt.Rows[i]["TakeoffAddress"]);
                            }
                            sheet.ServiceList.Add(u);

                            List.Add(sheet);
                        }
                        else
                        {
                            ODpServiceUnit u = new ODpServiceUnit();

                            if (dt.Rows[i]["ActualServreTime"] != DBNull.Value)
                                u.ActualTime = Convert.ToDateTime(dt.Rows[i]["ActualServreTime"]);
                            u.DispatchNo = Convert.ToString(dt.Rows[i]["DispatchNo"]);
                            u.PassengerCnt = Convert.ToInt32(dt.Rows[i]["PassengerCntR"]);
                            u.BaggageCnt = Convert.ToInt32(dt.Rows[i]["BaggageCntR"]);
                            u.FlightNo = Convert.ToString(dt.Rows[i]["FlightNo"]);
                            u.ReservationNo = Convert.ToString(dt.Rows[i]["ReservationNo"]);
                            u.ScheduleTime = Convert.ToDateTime(dt.Rows[i]["ScheduleServeTime"]);
                            u.ServiceGPS = new OSpatialGPS(Convert.ToDouble(dt.Rows[i]["lat"]), Convert.ToDouble(dt.Rows[i]["lng"]));
                            u.ServiceOrder = Convert.ToInt32(dt.Rows[i]["ServeSeq"]);
                            u.ServiceRemark = Convert.ToString(dt.Rows[i]["ServiceRemark"]);
                            u.AddBaggageCnt = Convert.ToInt32(dt.Rows[i]["AddBaggageCnt"]);

                            string ServiceType = Convert.ToString(dt.Rows[i]["ServiceType"]);
                            if (ServiceType == "I")
                            {
                                u.MainAddress = Convert.ToString(dt.Rows[i]["TakeoffAddress"]);
                                u.AirportAddress = Convert.ToString(dt.Rows[i]["PickupAddress"]);
                            }
                            else
                            {
                                u.MainAddress = Convert.ToString(dt.Rows[i]["PickupAddress"]);
                                u.AirportAddress = Convert.ToString(dt.Rows[i]["TakeoffAddress"]);
                            }
                            List[List.Count - 1].ServiceList.Add(u);
                        }

                        DispatchNo = Convert.ToString(dt.Rows[i]["DispatchNo"]);

                    }
                    return List;
                }
                else
                    return null;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        /// <summary>
        /// 取得司機時段內已上傳的派車單
        /// </summary>
        /// <param name="EmpID">司機編號</param>
        /// <param name="StartDate">查詢開始日</param>
        /// <param name="EndDate">查詢結束日</param>
        /// <returns></returns>
        public List<ODispatchSheet> GetServedDispatchSheets(int EmpID, string StartDate, string EndDate)//, string StartTime, string EndTime)
        {
            List<ODispatchSheet> List = new List<ODispatchSheet>();


            List<MySqlParameter> parms = new List<MySqlParameter>();
            DataTable dt = new DataTable();
            parms.Add(new MySqlParameter("@id", EmpID));
            parms.Add(new MySqlParameter("@sdate", StartDate));
            parms.Add(new MySqlParameter("@edate", EndDate));
            //parms.Add(new MySqlParameter("@stime", StartTime));
            //parms.Add(new MySqlParameter("@etime", EndTime));
            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    dt = conn.executeSelectQuery(GetDriverDispatchSQL("Served"), parms.ToArray());
                }
                if (dt.Rows.Count > 0)
                {
                    string DispatchNo = "";

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        if (DispatchNo != Convert.ToString(dt.Rows[i]["DispatchNo"]))
                        {
                            ODispatchSheet sheet = new ODispatchSheet();
                            sheet.BaggageCnt = Convert.ToInt32(dt.Rows[i]["BaggageCnt"]);
                            sheet.CarNo = Convert.ToString(dt.Rows[i]["CarNo"]);
                            sheet.CarType = Convert.ToString(dt.Rows[i]["CarType"]);
                            sheet.DispatchNo = Convert.ToString(dt.Rows[i]["DispatchNo"]);
                            sheet.DriverID = Convert.ToInt32(dt.Rows[i]["DriverID"]);
                            sheet.DriverName = Convert.ToString(dt.Rows[i]["UserName"]);
                            sheet.PassengerCnt = Convert.ToInt32(dt.Rows[i]["PassengerCnt"]);
                            sheet.ServiceCnt = Convert.ToInt32(dt.Rows[i]["StopCnt"]);
                            sheet.ServiceType = Convert.ToString(dt.Rows[i]["ServiceType"]);
                            sheet.TakeDate = Convert.ToString(dt.Rows[i]["ServeDate"]);
                            sheet.ServiceRemark = Convert.ToString(dt.Rows[i]["ServiceRemarkS"]);
                            sheet.UpdFlag = Convert.ToString(dt.Rows[i]["UpdFlag"]);

                            ODpServiceUnit u = new ODpServiceUnit();

                            u.BaggageCnt = Convert.ToInt32(dt.Rows[i]["BaggageCnt"]);

                            if (dt.Rows[i]["ActualServreTime"] != DBNull.Value)
                                u.ActualTime = Convert.ToDateTime(dt.Rows[i]["ActualServreTime"]);
                            u.DispatchNo = Convert.ToString(dt.Rows[i]["DispatchNo"]);
                            u.FlightNo = Convert.ToString(dt.Rows[i]["FlightNo"]);
                            u.PassengerCnt = Convert.ToInt32(dt.Rows[i]["PassengerCntR"]);
                            u.BaggageCnt = Convert.ToInt32(dt.Rows[i]["BaggageCntR"]);
                            u.ReservationNo = Convert.ToString(dt.Rows[i]["ReservationNo"]);
                            u.ScheduleTime = Convert.ToDateTime(dt.Rows[i]["ScheduleServeTime"]);
                            u.ServiceGPS = new OSpatialGPS(Convert.ToDouble(dt.Rows[i]["lat"]), Convert.ToDouble(dt.Rows[i]["lng"]));
                            u.ServiceOrder = Convert.ToInt32(dt.Rows[i]["ServeSeq"]);
                            u.ServiceRemark = Convert.ToString(dt.Rows[i]["ServiceRemark"]);
                            u.AddBaggageCnt = Convert.ToInt32(dt.Rows[i]["AddBaggageCnt"]);

                            string ServiceType = Convert.ToString(dt.Rows[i]["ServiceType"]);
                            if (ServiceType == "I")
                            {
                                u.MainAddress = Convert.ToString(dt.Rows[i]["TakeoffAddress"]);
                                u.AirportAddress = Convert.ToString(dt.Rows[i]["PickupAddress"]);
                            }
                            else
                            {
                                u.MainAddress = Convert.ToString(dt.Rows[i]["PickupAddress"]);
                                u.AirportAddress = Convert.ToString(dt.Rows[i]["TakeoffAddress"]);
                            }
                            sheet.ServiceList.Add(u);

                            List.Add(sheet);
                        }
                        else
                        {
                            ODpServiceUnit u = new ODpServiceUnit();

                            if (dt.Rows[i]["ActualServreTime"] != DBNull.Value)
                                u.ActualTime = Convert.ToDateTime(dt.Rows[i]["ActualServreTime"]);
                            u.DispatchNo = Convert.ToString(dt.Rows[i]["DispatchNo"]);
                            u.PassengerCnt = Convert.ToInt32(dt.Rows[i]["PassengerCntR"]);
                            u.BaggageCnt = Convert.ToInt32(dt.Rows[i]["BaggageCntR"]);
                            u.FlightNo = Convert.ToString(dt.Rows[i]["FlightNo"]);
                            u.ReservationNo = Convert.ToString(dt.Rows[i]["ReservationNo"]);
                            u.ScheduleTime = Convert.ToDateTime(dt.Rows[i]["ScheduleServeTime"]);
                            u.ServiceGPS = new OSpatialGPS(Convert.ToDouble(dt.Rows[i]["lat"]), Convert.ToDouble(dt.Rows[i]["lng"]));
                            u.ServiceOrder = Convert.ToInt32(dt.Rows[i]["ServeSeq"]);
                            u.ServiceRemark = Convert.ToString(dt.Rows[i]["ServiceRemark"]);
                            u.AddBaggageCnt = Convert.ToInt32(dt.Rows[i]["AddBaggageCnt"]);

                            string ServiceType = Convert.ToString(dt.Rows[i]["ServiceType"]);
                            if (ServiceType == "I")
                            {
                                u.MainAddress = Convert.ToString(dt.Rows[i]["TakeoffAddress"]);
                                u.AirportAddress = Convert.ToString(dt.Rows[i]["PickupAddress"]);
                            }
                            else
                            {
                                u.MainAddress = Convert.ToString(dt.Rows[i]["PickupAddress"]);
                                u.AirportAddress = Convert.ToString(dt.Rows[i]["TakeoffAddress"]);
                            }
                            List[List.Count - 1].ServiceList.Add(u);
                        }

                        DispatchNo = Convert.ToString(dt.Rows[i]["DispatchNo"]);

                    }
                    return List;
                }
                else
                    return null;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Remark">UnServed / Served / Serving</param>
        /// <returns></returns>
        private string GetDriverDispatchSQL(string Remark)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("select s.DispatchNo, s.ServiceType, s.ServeDate, s.PassengerCnt, s.BaggageCnt, s.StopCnt, s.CarType, s.CarNo, s.DriverID, ");
            sb.Append("d.ReservationNo, d.ServeSeq, d.ScheduleServeTime, d.ActualServreTime, X(d.LocationGPS)as lng,Y(d.LocationGPS) as lat, d.ServiceRemark,d.AddBaggageCnt, ");
            sb.Append("u.UserName,r.FlightNo, r.PickupCity, r.PickupDistinct, r.PickupAddress, r.TakeoffCity, r.TakeoffDistinct, r.TakeoffAddress, r.PassengerCnt as PassengerCntR,   r.BaggageCnt as BaggageCntR,s.ServiceRemark as ServiceRemarkS,s.UpdFlag ");
            sb.Append("from dispatch_sheet s,dispatch_reservation d,user_info u,reservation_sheet r ");
            sb.Append("where s.DispatchNo = d.DispatchNo and d.ReservationNo = r.ReservationNo and s.DriverID = u.UserID ");
            sb.Append("and s.DriverID = @id and s.ServeDate >= @sdate and  s.ServeDate <= @edate ");
            sb.Append("and r.ProcessStage not in ('X','W','Y') ");

            if (Remark == "UnServed")
                sb.Append(" and  s.UpdFlag = '0'  ");
            else if (Remark == "Served")
                sb.Append(" and  s.UpdFlag = '1'  ");


            sb.Append("Order by s.ServeDate , s.DispatchNo,d.ScheduleServeTime ASC");

            return sb.ToString();
        }

        /// <summary>
        /// 取得完整派車單
        /// </summary>
        /// <param name="Dno">派車單編號</param>
        /// <returns></returns>
        public ODispatchSheet GetDispatchSheets(string Dno)
        {
            List<ODispatchSheet> List = new List<ODispatchSheet>();
            BAL.BALGeneral bal = new BAL.BALGeneral(this._ConnectionString);

            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            DataTable dt = new DataTable();
            sb.Append("select s.DispatchNo, s.ServiceType, s.ServeDate,s.TimeSegment, s.DispatchTime,s.PassengerCnt, s.BaggageCnt, s.StopCnt, s.CarType, s.CarNo, s.DriverID, s.FleetID, ");
            sb.Append("d.ReservationNo, d.ServeSeq, d.ScheduleServeTime, d.ActualServreTime, X(d.LocationGPS)as lng,Y(d.LocationGPS) as lat, d.ServiceRemark,d.AddBaggageCnt, ");
            sb.Append("r.FlightNo, r.PickupCity, r.PickupDistinct, r.PickupAddress, r.TakeoffCity, r.TakeoffDistinct, r.TakeoffAddress, r.PassengerCnt as PassengerCntR, r.BaggageCnt as BaggageCntR  ,s.ServiceRemark as ServiceRemarkS ");
            sb.Append("from dispatch_sheet s,dispatch_reservation d,reservation_sheet r ");
            sb.Append("where s.DispatchNo = d.DispatchNo and d.ReservationNo = r.ReservationNo ");
            sb.Append("and   s.DispatchNo = @no Order by d.ScheduleServeTime  ASC");
            parms.Add(new MySqlParameter("@no", Dno));
            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                }
                if (dt.Rows.Count > 0)
                {
                    string Reservation = string.Empty;
                    ODispatchSheet sheet = new ODispatchSheet();
                    sheet.BaggageCnt = Convert.ToInt32(dt.Rows[0]["BaggageCnt"]);
                    sheet.CarNo = Convert.ToString(dt.Rows[0]["CarNo"]);
                    sheet.CarType = Convert.ToString(dt.Rows[0]["CarType"]);
                    sheet.DispatchNo = Convert.ToString(dt.Rows[0]["DispatchNo"]);
                    sheet.FleetID = Convert.ToInt32(dt.Rows[0]["FleetID"]);
                    sheet.DriverID = Convert.ToInt32(dt.Rows[0]["DriverID"]);
                    sheet.PassengerCnt = Convert.ToInt32(dt.Rows[0]["PassengerCnt"]);
                    sheet.ServiceCnt = Convert.ToInt32(dt.Rows[0]["StopCnt"]);
                    sheet.ServiceType = Convert.ToString(dt.Rows[0]["ServiceType"]);
                    sheet.TakeDate = Convert.ToString(dt.Rows[0]["ServeDate"]);
                    sheet.ServiceRemark = Convert.ToString(dt.Rows[0]["ServiceRemarkS"]);
                    sheet.TimeSegment = Convert.ToString(dt.Rows[0]["TimeSegment"]);
                    sheet.FisrtServiceTime = Convert.ToString(dt.Rows[0]["DispatchTime"]);

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        ODpServiceUnit u = new ODpServiceUnit();

                        u.BaggageCnt = Convert.ToInt32(dt.Rows[i]["BaggageCntR"]);
                        if (dt.Rows[i]["ActualServreTime"] != DBNull.Value)
                            u.ActualTime = Convert.ToDateTime(dt.Rows[i]["ActualServreTime"]);
                        u.DispatchNo = Convert.ToString(dt.Rows[i]["DispatchNo"]);
                        u.FlightNo = Convert.ToString(dt.Rows[i]["FlightNo"]);
                        u.PassengerCnt = Convert.ToInt32(dt.Rows[i]["PassengerCntR"]);
                        u.ReservationNo = Convert.ToString(dt.Rows[i]["ReservationNo"]);
                        u.ScheduleTime = Convert.ToDateTime(dt.Rows[i]["ScheduleServeTime"]);
                        u.ServiceGPS = new OSpatialGPS(Convert.ToDouble(dt.Rows[i]["lat"]), Convert.ToDouble(dt.Rows[i]["lng"]));
                        u.ServiceOrder = Convert.ToInt32(dt.Rows[i]["ServeSeq"]);
                        u.ServiceRemark = Convert.ToString(dt.Rows[i]["ServiceRemark"]);
                        u.AddBaggageCnt = Convert.ToInt32(dt.Rows[i]["AddBaggageCnt"]);

                        if (sheet.ServiceType == "I")
                        {
                            u.MainCity = Convert.ToString(dt.Rows[i]["TakeoffCity"]);
                            u.MainDistinct = Convert.ToString(dt.Rows[i]["TakeoffDistinct"]);
                            u.MainAddress = Convert.ToString(dt.Rows[i]["TakeoffAddress"]);
                            u.AirportAddress = Convert.ToString(dt.Rows[i]["PickupAddress"]);
                        }
                        else
                        {
                            u.MainCity = Convert.ToString(dt.Rows[i]["PickupCity"]);
                            u.MainDistinct = Convert.ToString(dt.Rows[i]["PickupDistinct"]);
                            u.MainAddress = Convert.ToString(dt.Rows[i]["PickupAddress"]);
                            u.AirportAddress = Convert.ToString(dt.Rows[i]["TakeoffAddress"]);
                        }
                        sheet.ServiceList.Add(u);
                    }
                    return sheet;

                }
                else
                    return null;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }


        /// <summary>
        /// 取得完整派車單
        /// </summary>
        /// <param name="StartDate">查詢開始日</param>
        /// <param name="EndDate">查詢結束日</param>
        /// <returns></returns>
        public List<ODispatchSheet> GetDispatchSheets(string StartDate, string EndDate)
        {
            List<ODispatchSheet> List = new List<ODispatchSheet>();

            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            DataTable dt = new DataTable();
            sb.Append("select s.DispatchNo, s.ServiceType, s.ServeDate, s.PassengerCnt, s.BaggageCnt, s.StopCnt, s.CarType, s.CarNo, s.DriverID, ");
            sb.Append("d.ReservationNo, d.ServeSeq, d.ScheduleServeTime, d.ActualServreTime, X(d.LocationGPS)as lat,Y(d.LocationGPS) as lng, d.ServiceRemark, ");
            sb.Append("u.UserName,r.FlightNo, r.PickupCity, r.PickupDistinct, r.PickupAddress, r.TakeoffCity, r.TakeoffDistinct, r.TakeoffAddress, r.PassengerCnt, r.BaggageCnt ");
            sb.Append("from dispatch_sheet s,dispatch_reservation d,user_info u,reservation_sheet r ");
            sb.Append("where s.DispatchNo = d.DispatchNo and d.ReservationNo = r.ReservationNo and s.DriverID = u.UserID ");
            sb.Append("and s.ServeDate between @sdate and @edate and d.ServiceRemark  <> 'X' ");
            sb.Append("Order by d.ScheduleServeTime,s.DispatchNo,d.ServeSeq ASC");
            parms.Add(new MySqlParameter("@sdate", StartDate));
            parms.Add(new MySqlParameter("@edate", EndDate));
            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                }
                if (dt.Rows.Count > 0)
                {
                    string DispatchNo = "";

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        if (DispatchNo != Convert.ToString(dt.Rows[i]["DispatchNo"]))
                        {
                            ODispatchSheet sheet = new ODispatchSheet();
                            sheet.BaggageCnt = Convert.ToInt32(dt.Rows[i]["BaggageCnt"]);
                            sheet.CarNo = Convert.ToString(dt.Rows[i]["CarNo"]);
                            sheet.CarType = Convert.ToString(dt.Rows[i]["CarType"]);
                            sheet.DispatchNo = Convert.ToString(dt.Rows[i]["DispatchNo"]);
                            sheet.DriverID = Convert.ToInt32(dt.Rows[i]["DriverID"]);
                            sheet.DriverName = Convert.ToString(dt.Rows[i]["UserName"]);
                            sheet.PassengerCnt = Convert.ToInt32(dt.Rows[i]["PassengerCnt"]);
                            sheet.ServiceCnt = Convert.ToInt32(dt.Rows[i]["StopCnt"]);
                            sheet.ServiceType = Convert.ToString(dt.Rows[i]["ServiceType"]);
                            sheet.TakeDate = Convert.ToString(dt.Rows[i]["ServeDate"]);

                            ODpServiceUnit u = new ODpServiceUnit();

                            u.BaggageCnt = Convert.ToInt32(dt.Rows[i]["BaggageCnt"]);
                            u.ActualTime = Convert.ToDateTime(dt.Rows[i]["ActualServreTime"]);
                            u.DispatchNo = Convert.ToString(dt.Rows[i]["DispatchNo"]);
                            u.FlightNo = Convert.ToString(dt.Rows[i]["FlightNo"]);
                            u.PassengerCnt = Convert.ToInt32(dt.Rows[i]["PassengerCnt"]);
                            u.PassengerCnt = Convert.ToInt32(dt.Rows[i]["BaggageCnt"]);
                            u.ReservationNo = Convert.ToString(dt.Rows[i]["ReservationNo"]);
                            u.ScheduleTime = Convert.ToDateTime(dt.Rows[i]["ScheduleServeTime"]);
                            u.ServiceGPS = new OSpatialGPS(Convert.ToDouble(dt.Rows[i]["lat"]), Convert.ToDouble(dt.Rows[i]["lng"]));
                            u.ServiceOrder = Convert.ToInt32(dt.Rows[i]["ServeSeq"]);
                            u.ServiceRemark = Convert.ToString(dt.Rows[i]["ServiceRemark"]);

                            string ServiceType = Convert.ToString(dt.Rows[i]["ServiceType"]);
                            if (ServiceType == "I")
                            {
                                u.MainCity = Convert.ToString(dt.Rows[i]["TakeoffCity"]);
                                u.MainDistinct = Convert.ToString(dt.Rows[i]["TakeoffDistinct"]);
                                u.MainAddress = Convert.ToString(dt.Rows[i]["TakeoffAddress"]);
                                u.AirportAddress = Convert.ToString(dt.Rows[i]["PickupAddress"]);
                            }
                            else
                            {
                                u.MainCity = Convert.ToString(dt.Rows[i]["PickupCity"]);
                                u.MainDistinct = Convert.ToString(dt.Rows[i]["PickupDistinct"]);
                                u.MainAddress = Convert.ToString(dt.Rows[i]["PickupAddress"]);
                                u.AirportAddress = Convert.ToString(dt.Rows[i]["TakeoffAddress"]);
                            }
                            sheet.ServiceList.Add(u);

                            List.Add(sheet);
                        }
                        else
                        {
                            ODpServiceUnit u = new ODpServiceUnit();

                            u.BaggageCnt = Convert.ToInt32(dt.Rows[i]["BaggageCnt"]);
                            u.ActualTime = Convert.ToDateTime(dt.Rows[i]["ActualServreTime"]);
                            u.DispatchNo = Convert.ToString(dt.Rows[i]["DispatchNo"]);
                            u.PassengerCnt = Convert.ToInt32(dt.Rows[i]["PassengerCnt"]);
                            u.PassengerCnt = Convert.ToInt32(dt.Rows[i]["BaggageCnt"]);
                            u.FlightNo = Convert.ToString(dt.Rows[i]["FlightNo"]);
                            u.ReservationNo = Convert.ToString(dt.Rows[i]["ReservationNo"]);
                            u.ScheduleTime = Convert.ToDateTime(dt.Rows[i]["ScheduleServeTime"]);
                            u.ServiceGPS = new OSpatialGPS(Convert.ToDouble(dt.Rows[i]["lat"]), Convert.ToDouble(dt.Rows[i]["lng"]));
                            u.ServiceOrder = Convert.ToInt32(dt.Rows[i]["ServeSeq"]);
                            u.ServiceRemark = Convert.ToString(dt.Rows[i]["ServiceRemark"]);

                            string ServiceType = Convert.ToString(dt.Rows[i]["ServiceType"]);
                            if (ServiceType == "I")
                            {
                                u.MainCity = Convert.ToString(dt.Rows[i]["TakeoffCity"]);
                                u.MainDistinct = Convert.ToString(dt.Rows[i]["TakeoffDistinct"]);
                                u.MainAddress = Convert.ToString(dt.Rows[i]["TakeoffAddress"]);
                                u.AirportAddress = Convert.ToString(dt.Rows[i]["PickupAddress"]);
                            }
                            else
                            {
                                u.MainCity = Convert.ToString(dt.Rows[i]["PickupCity"]);
                                u.MainDistinct = Convert.ToString(dt.Rows[i]["PickupDistinct"]);
                                u.MainAddress = Convert.ToString(dt.Rows[i]["PickupAddress"]);
                                u.AirportAddress = Convert.ToString(dt.Rows[i]["TakeoffAddress"]);
                            }
                            List[List.Count - 1].ServiceList.Add(u);
                        }

                        DispatchNo = Convert.ToString(dt.Rows[i]["DispatchNo"]);

                    }
                    return List;
                }
                else
                    return null;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public List<ODpServiceUnit> GetServiceUnit(string Rno)
        {
            List<MySqlParameter> parms = new List<MySqlParameter>();
            StringBuilder sb = new StringBuilder();
            DataTable dt = new DataTable();

            sb.Append("select DispatchNo, ReservationNo, ServeSeq, ScheduleServeTime, ActualServreTime, X(LocationGPS) as lng ,Y(LocationGPS) as lat, ServiceRemark, CreateTime, UpdTime ");
            sb.Append("from dispatch_reservation  where ReservationNo = @no and ServiceRemark != 'X' ");

            parms.Add(new MySqlParameter("@no", Rno));

            using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
            {
                dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
            }
            List<ODpServiceUnit> List = new List<ODpServiceUnit>();
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DALReservation dal = new DALReservation(this._ConnectionString);
                    OReservation or = dal.GetReservation(Rno);

                    ODpServiceUnit su = new ODpServiceUnit();
                    if (dt.Rows[0]["ActualServreTime"] != null && dt.Rows[0]["ActualServreTime"] != DBNull.Value)
                        su.ActualTime = Convert.ToDateTime(dt.Rows[0]["ActualServreTime"]);

                    su.BaggageCnt = or.BaggageCnt;//Convert.ToInt32(dt.Rows[0]["BaggageCnt"]);
                    su.DispatchNo = Convert.ToString(dt.Rows[0]["DispatchNo"]);
                    su.PassengerCnt = or.PassengerCnt;// Convert.ToInt32(dt.Rows[0]["PassengerCnt"]);
                    su.ReservationNo = Rno;
                    su.ScheduleTime = Convert.ToDateTime(dt.Rows[0]["ScheduleServeTime"]);
                    su.ServiceGPS = new OSpatialGPS(Convert.ToDouble(dt.Rows[0]["lat"]), Convert.ToDouble(dt.Rows[0]["lng"]));
                    su.ServiceRemark = Convert.ToString(dt.Rows[0]["ServiceRemark"]);

                    if (or.ServiceType == "I")
                    {
                        su.MainCity = or.TakeoffCity;
                        su.MainDistinct = or.TakeoffDistinct;
                        su.MainAddress = or.TakeoffAddress;
                        su.AirportAddress = or.PickupCity + or.PickupDistinct + or.PickupAddress;
                    }
                    else
                    {
                        su.MainCity = or.PickupCity;
                        su.MainDistinct = or.PickupDistinct;
                        su.MainAddress = or.PickupAddress;
                        su.AirportAddress = or.TakeoffCity + or.TakeoffDistinct + or.TakeoffAddress;
                    }
                    List.Add(su);
                }
                return List;
            }
            return null;
        }

        /// <summary>
        /// 儲存媒合紀錄
        /// </summary>
        /// <param name="date"></param>
        /// <param name="Status"></param>
        /// <param name="ID"></param>
        /// <param name="Json"></param>
        /// <param name="ErrMsg"></param>
        /// <returns></returns>
        public bool SaveMatchError(string date, string Status, int ID, string Json, string ErrMsg)
        {
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            sb.Append("INSERT INTO dispatch_record 	(TakeDate, ExeStatus, MatchupID, Json,ErrorMsg) 	VALUES (@TakeDate,@ExeStatus,@MatchupID,@Json,@ErrorMsg)");
            parms.Add(new MySqlParameter("@TakeDate", date));
            parms.Add(new MySqlParameter("@ExeStatus", Status));
            parms.Add(new MySqlParameter("@MatchupID", ID));
            parms.Add(new MySqlParameter("@Json", Json));
            parms.Add(new MySqlParameter("@ErrorMsg", ErrMsg));
            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    if (conn.executeInsertQuery(sb.ToString(), parms.ToArray()) <= 0)
                        return false;
                    else
                        return true;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// 將媒合日期數量寫入紀錄檔
        /// </summary>
        /// <returns></returns>
        public bool SaveMatchRecord(string MatchDate, int Count)
        {
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            sb.Append("INSERT INTO match_record 	(MatchDate, DispatchCnt) 	VALUES (@MatchDate,@DispatchCnt)");
            parms.Add(new MySqlParameter("@MatchDate", MatchDate));
            parms.Add(new MySqlParameter("@DispatchCnt", Count));

            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    if (conn.executeInsertQuery(sb.ToString(), parms.ToArray()) > 0)
                        return true;
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public bool isMatchDateExist(string MatchDate)
        {
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            sb.Append("select count(1) FROM  match_record  WHERE MatchDate  = @MatchDate");
            parms.Add(new MySqlParameter("@MatchDate", MatchDate));

            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    DataTable dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());

                    if (Convert.ToInt32(dt.Rows[0][0]) == 0)
                        return false;
                    else
                        return true;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// 新增派車單
        /// </summary>
        /// <param name="DispatchUnit"></param>
        /// <returns></returns>
        public bool addDispatchSheet(ODispatchSheet DispatchUnit)
        {
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();


            using (dbTConnectionMySQL conn = new dbTConnectionMySQL(this._ConnectionString))
            {
                try
                {
                    //寫入派車預約單檔
                    foreach (ODpServiceUnit u in DispatchUnit.ServiceList)
                    {
                        sb.Length = 0;
                        parms.Clear();
                        sb.Append("INSERT INTO dispatch_reservation ");
                        sb.Append("(DispatchNo, ReservationNo, ScheduleServeTime, LocationGPS,ServiceRemark) ");
                        sb.Append("VALUES (@DispatchNo, @ReservationNo, @ScheduleServeTime,GeomFromText(@LocationGPS), '0') ");
                        parms.Add(new MySqlParameter("@DispatchNo", u.DispatchNo));
                        parms.Add(new MySqlParameter("@ReservationNo", u.ReservationNo));
                        parms.Add(new MySqlParameter("@ScheduleServeTime", u.ScheduleTime));
                        parms.Add(new MySqlParameter("@LocationGPS", "Point(" + u.ServiceGPS.GetLng().ToString() + " " + u.ServiceGPS.GetLat().ToString() + ")"));

                        try
                        {
                            conn.executeInsertQuery(sb.ToString(), parms.ToArray());
                        }
                        catch (MySqlException ex)
                        {
                            conn.Rollback();
                            throw new Exception(ex.Message);
                        }

                        //更新訂單狀態
                        sb.Length = 0;
                        parms.Clear();
                        sb.Append("UPDATE reservation_sheet SET ProcessStage='A'	WHERE ReservationNo = @ReservationNo");
                        parms.Add(new MySqlParameter("@ReservationNo", u.ReservationNo));
                        try
                        {
                            conn.executeUpdateQuery(sb.ToString(), parms.ToArray());
                        }
                        catch (Exception ex)
                        {
                            conn.Rollback();
                            throw new Exception(ex.Message);
                        }
                    }

                    //寫入派車檔
                    sb.Length = 0;
                    parms.Clear();
                    sb.Append("INSERT INTO dispatch_sheet ");
                    sb.Append("(DispatchNo, ServiceType, ServeDate, TimeSegment,DispatchTime,  PassengerCnt, BaggageCnt, StopCnt, MaxFlag, FleetID, CarType, CarNo, DriverID) ");
                    sb.Append("VALUES (@DispatchNo,@ServiceType,@ServeDate, @TimeSegment, @DispatchTime, @PassengerCnt,@BaggageCnt, @StopCnt, @MaxFlag, @FleetID, @CarType, @CarNo, @DriverID) ");
                    parms.Add(new MySqlParameter("@DispatchNo", DispatchUnit.DispatchNo));
                    parms.Add(new MySqlParameter("@ServiceType", DispatchUnit.ServiceType));
                    parms.Add(new MySqlParameter("@ServeDate", DispatchUnit.TakeDate));
                    parms.Add(new MySqlParameter("@TimeSegment", DispatchUnit.TimeSegment));
                    parms.Add(new MySqlParameter("@DispatchTime", DispatchUnit.FisrtServiceTime));
                    parms.Add(new MySqlParameter("@PassengerCnt", DispatchUnit.PassengerCnt));
                    parms.Add(new MySqlParameter("@BaggageCnt", DispatchUnit.BaggageCnt));
                    parms.Add(new MySqlParameter("@StopCnt", DispatchUnit.ServiceCnt));
                    parms.Add(new MySqlParameter("@MaxFlag", "0"));
                    parms.Add(new MySqlParameter("@FleetID", DispatchUnit.FleetID));
                    parms.Add(new MySqlParameter("@CarType", DispatchUnit.CarType));
                    parms.Add(new MySqlParameter("@CarNo", DispatchUnit.CarNo));
                    parms.Add(new MySqlParameter("@DriverID", DispatchUnit.DriverID));

                    try
                    {
                        conn.executeInsertQuery(sb.ToString(), parms.ToArray());
                    }
                    catch (Exception ex)
                    {
                        conn.Rollback();
                        throw new Exception(ex.Message);
                    }





                    conn.Commit();
                    return true;
                }
                catch (Exception ex)
                {
                    conn.Rollback();
                    throw new Exception(ex.Message);
                }
            }
        }

        /// <summary>
        /// 更新服務紀錄
        /// </summary>
        /// <param name="DispatchNo"></param>
        /// <param name="ReservationNo"></param>
        /// <param name="OldRemark"></param>
        /// <param name="NewRemark"></param>
        /// <returns></returns>
        public bool UpdateReservationServiceStatus(ODriverServiceRecord odr)
        {
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            sb.Append("Update dispatch_sheet Set ServiceRemark = @new , UpdFlag = @flag Where DispatchNo = @dno and DriverID = @id");
            parms.Add(new MySqlParameter("@new", odr.SheetServiceRemark));
            parms.Add(new MySqlParameter("@flag", odr.UpdFlag));
            parms.Add(new MySqlParameter("@dno", odr.Dno));
            parms.Add(new MySqlParameter("@id", odr.DriverID));


            try
            {
                using (dbTConnectionMySQL conn = new dbTConnectionMySQL(this._ConnectionString))
                {
                    if (conn.executeUpdateQuery(sb.ToString(), parms.ToArray()) <= 0)
                    {
                        conn.Rollback();
                        return false;
                    }

                    sb.Length = 0;
                    parms.Clear();

                    sb.Append("UPDATE dispatch_reservation SET ActualServreTime = @time,ServiceRemark = @remark , AddBaggageCnt = @AddBaggageCnt  WHERE DispatchNo = @dno and ReservationNo = @rno");
                    parms.Add(new MySqlParameter("@time", MySqlDbType.DateTime));
                    parms.Add(new MySqlParameter("@remark", MySqlDbType.String));
                    parms.Add(new MySqlParameter("@dno", MySqlDbType.VarChar));
                    parms.Add(new MySqlParameter("@rno", MySqlDbType.VarChar));
                    parms.Add(new MySqlParameter("@AddBaggageCnt", MySqlDbType.Int32));

                    if (!string.IsNullOrWhiteSpace(odr.Reservation1))
                    {
                        parms[0].Value = odr.ServiceTime1;
                        parms[1].Value = odr.ServiceRemark1;
                        parms[2].Value = odr.Dno;
                        parms[3].Value = odr.Reservation1;
                        parms[4].Value = odr.addBag1;
                        if (conn.executeUpdateQuery(sb.ToString(), parms.ToArray()) <= 0)
                        {
                            conn.Rollback();
                            return false;
                        }
                    }

                    if (!string.IsNullOrWhiteSpace(odr.Reservation2))
                    {
                        parms[0].Value = odr.ServiceTime2;
                        parms[1].Value = odr.ServiceRemark2;
                        parms[2].Value = odr.Dno;
                        parms[3].Value = odr.Reservation2;
                        parms[4].Value = odr.addBag2;
                        if (conn.executeUpdateQuery(sb.ToString(), parms.ToArray()) <= 0)
                        {
                            conn.Rollback();
                            return false;
                        }
                    }

                    if (!string.IsNullOrWhiteSpace(odr.Reservation3))
                    {
                        parms[0].Value = odr.ServiceTime3;
                        parms[1].Value = odr.ServiceRemark3;
                        parms[2].Value = odr.Dno;
                        parms[3].Value = odr.Reservation3;
                        parms[4].Value = odr.addBag3;
                        if (conn.executeUpdateQuery(sb.ToString(), parms.ToArray()) <= 0)
                        {
                            conn.Rollback();
                            return false;
                        }
                    }


                    conn.Commit();
                    return true;

                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// 取得排班資料
        /// </summary>
        /// <param name="ScheduleID"></param>
        /// <returns></returns>
        //public OScheduleData GetScheduleByID(int ScheduleID)
        //{
        //    StringBuilder sb = new StringBuilder();
        //    List<MySqlParameter> parms = new List<MySqlParameter>();
        //    DataTable dt = new DataTable();
        //    sb.Append("SELECT ScheduleID, ScheduleDate, ShiftType, CarNo, DriverID, FleetID, CarType FROM daily_schedule Where ScheduleID = @id");
        //    parms.Add(new MySqlParameter("@id", ScheduleID));

        //    try
        //    {
        //        using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
        //        {
        //            dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(ex.Message);
        //    }


        //    OScheduleData d = new OScheduleData();
        //    d.CarNo = Convert.ToString(dt.Rows[0]["CarNo"]);
        //    d.CarType = Convert.ToString(dt.Rows[0]["CarType"]);
        //    d.DriverID = Convert.ToInt32(dt.Rows[0]["DriverID"]);
        //    d.FleetID = Convert.ToInt32(dt.Rows[0]["FleetID"]);
        //    d.ScheduleDate = Convert.ToString(dt.Rows[0]["ScheduleDate"]);
        //    d.ScheduleID = Convert.ToInt32(dt.Rows[0]["ScheduleID"]);
        //    d.ShiftType = Convert.ToString(dt.Rows[0]["ShiftType"]);



        //    return d;
        //}

        /// <summary>
        /// 取得排班資料
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        //public List<OScheduleData> GetScheduleByDate(string date)
        //{
        //    StringBuilder sb = new StringBuilder();
        //    List<MySqlParameter> parms = new List<MySqlParameter>();
        //    DataTable dt = new DataTable();
        //    sb.Append("SELECT ScheduleID, ScheduleDate, ShiftType, CarNo, DriverID, FleetID, CarType FROM daily_schedule Where Scheduledate = @date");
        //    parms.Add(new MySqlParameter("@date", date));

        //    try
        //    {
        //        using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
        //        {
        //            dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(ex.Message);
        //    }

        //    List<OScheduleData> List = new List<OScheduleData>();
        //    for (int i = 0; i < dt.Rows.Count; i++)
        //    {
        //        OScheduleData d = new OScheduleData();
        //        d.CarNo = Convert.ToString(dt.Rows[i]["CarNo"]);
        //        d.CarType = Convert.ToString(dt.Rows[i]["CarType"]);
        //        d.DriverID = Convert.ToInt32(dt.Rows[i]["DriverID"]);
        //        d.FleetID = Convert.ToInt32(dt.Rows[i]["FleetID"]);
        //        d.ScheduleDate = Convert.ToString(dt.Rows[i]["ScheduleDate"]);
        //        d.ScheduleID = Convert.ToInt32(dt.Rows[i]["ScheduleID"]);
        //        d.ShiftType = Convert.ToString(dt.Rows[i]["ShiftType"]);
        //        List.Add(d);
        //    }

        //    return List;
        //}

        /// <summary>
        /// 取得第一個服務時間
        /// </summary>
        /// <param name="Dno"></param>
        /// <returns></returns>
        public DateTime GetFirstScheduleServiceTime(string Dno)
        {
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            sb.Append("select  ScheduleServeTime ");
            sb.Append("from dispatch_reservation ");
            sb.Append("where DispatchNo = @no Order by d.ScheduleServeTime ASC");

            parms.Add(new MySqlParameter("@no", Dno));

            using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
            {
                DataTable dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                if (dt.Rows.Count > 0)
                    return Convert.ToDateTime(dt.Rows[0]["ScheduleServeTime"]);
                else
                    throw new Exception("派車單號異常");
            }
        }

        /// <summary>
        /// 取得最後一個服務時間
        /// </summary>
        /// <param name="Dno"></param>
        /// <returns></returns>
        public DateTime GetLastScheduleServiceTime(string Dno)
        {
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            sb.Append("select  ScheduleServeTime ");
            sb.Append("from dispatch_reservation ");
            sb.Append("where DispatchNo = @no Order by ScheduleServeTime DESC");

            parms.Add(new MySqlParameter("@no", Dno));

            using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
            {
                DataTable dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                if (dt.Rows.Count > 0)
                    return Convert.ToDateTime(dt.Rows[0]["ScheduleServeTime"]);
                else
                    throw new Exception("派車單號異常");
            }

        }

        /// <summary>
        /// 根據多重條件回傳一批簡易派車單
        /// </summary>
        /// <param name="qs">QDispatchCondStruct</param>
        /// <returns></returns>
        public List<ODispatchSheet> GetDispatchMultipleCondition(QDispatchCondStruct qs, int take, int skip)
        {
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            sb.Append("SELECT Distinct d.DispatchNo, d.ServiceType, d.ServeDate, d.TimeSegment, d.DispatchTime, d.PassengerCnt, d.BaggageCnt, d.StopCnt, d.MaxFlag, d.FleetID, d.CarType, d.CarNo, d.DriverID ");
            sb.Append("FROM dispatch_sheet d   ");
            sb.Append(" where d.ServeDate between @sdate and @edate ");
            parms.Add(new MySqlParameter("@sdate", qs.QSDate));
            parms.Add(new MySqlParameter("@edate", qs.QEDate));



            if (qs.DriverID > 0)
            {
                sb.Append(" And d.DriverID = @id ");
                parms.Add(new MySqlParameter("@id", qs.DriverID));
            }

            if (qs.CarNo != "")
            {
                sb.Append(" And d.CarNo = @cno ");
                parms.Add(new MySqlParameter("@cno", qs.CarNo));
            }

            if (qs.DispatchNo != "")
            {
                sb.Append(" And d.DispatchNo = @dno ");
                parms.Add(new MySqlParameter("@dno", qs.DispatchNo));
            }



            if (qs.OrderNo != "")
            {
                sb.Append(" And r.ReservationNo = @rno ");
                parms.Add(new MySqlParameter("@rno", qs.OrderNo));
            }

            sb.Append(" order by  d.ServeDate,d.DispatchNo");

            //if (take != 0)
            //{
            //    sb.Append(" limit " + (1 + skip).ToString() + "," + (1 + skip + take).ToString());
            //}

            DataTable dt = new DataTable();
            List<ODispatchSheet> List = new List<ODispatchSheet>();
            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            if (dt.Rows.Count > 0)
            {
                BAL.BALEmployee bal = new BAL.BALEmployee(this._ConnectionString);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    ODispatchSheet d = new ODispatchSheet();
                    d.BaggageCnt = Convert.ToInt32(dt.Rows[i]["BaggageCnt"]);
                    d.DispatchNo = Convert.ToString(dt.Rows[i]["DispatchNo"]);
                    d.TakeDate = Convert.ToString(dt.Rows[i]["ServeDate"]);
                    d.ServiceType = Convert.ToString(dt.Rows[i]["ServiceType"]);
                    d.TimeSegment = Convert.ToString(dt.Rows[i]["TimeSegment"]);
                    d.FisrtServiceTime = Convert.ToString(dt.Rows[i]["DispatchTime"]);
                    d.PassengerCnt = Convert.ToInt32(dt.Rows[i]["PassengerCnt"]);
                    d.ServiceCnt = Convert.ToInt32(dt.Rows[i]["StopCnt"]);
                    d.FleetID = Convert.ToInt32(dt.Rows[i]["FleetID"]);
                    d.CarType = Convert.ToString(dt.Rows[i]["CarType"]);
                    d.CarNo = Convert.ToString(dt.Rows[i]["CarNo"]);
                    d.DriverID = Convert.ToInt32(dt.Rows[i]["DriverID"]);
                    if (d.DriverID > 0)
                    {
                        OEmployee emp = bal.GetEmployee(d.DriverID);
                        d.DriverName = emp.Name;
                    }
                    else
                        d.DriverName = "";


                    List.Add(d);
                }
                return List;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 更換派遣的司機
        /// </summary>
        /// <param name="Dno"></param>
        /// <param name="oFleetID"></param>
        /// <param name="oCarNo"></param>
        /// <param name="oEmpID"></param>
        /// <param name="nFleetID"></param>
        /// <param name="nCarNo"></param>
        /// <param name="nEmpID"></param>
        /// <param name="nCarType"></param>
        /// <returns></returns>
        public bool UpdDispatchCarDriver(string Dno, int nFleetID, string nCarNo, int nEmpID, string nCarType)
        {
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            sb.Append("Update dispatch_sheet SET FleetID= @nFleet,	CarNo=@nCarNo,DriverID= @nEmpID ");
            sb.Append("WHERE DispatchNo = @no ");
            parms.Add(new MySqlParameter("@nFleet", nFleetID));
            parms.Add(new MySqlParameter("@nCarNo", nCarNo));
            parms.Add(new MySqlParameter("@nEmpID", nEmpID));
            parms.Add(new MySqlParameter("@no", Dno));

            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    if (conn.executeUpdateQuery(sb.ToString(), parms.ToArray()) > 0)
                        return true;
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


    }


    /// <summary>
    /// 其他的DAL
    /// </summary>
    public class DALGeneral : CallcarDAL
    {
        public DALGeneral(string ConnectionString) : base(ConnectionString) { }

        public DataTable GetShowBulletin(string Target, int FleetNo, int EmployeeID)
        {
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            DataTable dt = new DataTable();
            sb.Append("SELECT  SeqNo, TargetType, FleetNo, EmployeeID, DisplayFlag, Content, CreateTime ");
            sb.Append("FROM bulletin ");
            sb.Append("WHERE (FleetNo = @no OR EmployeeID = @id OR EmployeeID = -1) AND DisplayFlag = '1'  AND TargetType = @type ");
            sb.Append("ORDER BY CreateTime DESC ");

            parms.Add(new MySqlParameter("@type", Target));
            parms.Add(new MySqlParameter("@no", FleetNo));
            parms.Add(new MySqlParameter("@id", EmployeeID));

            using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
            {
                dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
            }

            return dt;
        }

        //public dmAppSetting GetApp(string appName, string appOS)
        //{
        //    List<dmAppSetting> List;
        //    string sql = "SELECT SeqNo, AppName, SupportOS, LastVersion, Remark  FROM app_setting Where AppName = @name and SupportOS = @os";
        //    var parms = new { name = appName, os = appOS };

        //    using (var cn = new MySqlConnection(this._ConnectionString))
        //    {
        //        cn.Open();
        //        var app = cn.Query<dmAppSetting>(sql, parms);
        //        List = app.ToList<dmAppSetting>();
        //    }
        //    if (List.Count > 0)
        //        return List[0];
        //    else
        //        return null;
        //}

        public OHoliday GetHoliday(string date)
        {
            try
            {
                DataTable dt = new DataTable();
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    List<MySqlParameter> parms = new List<MySqlParameter>();
                    StringBuilder sb = new StringBuilder();
                    sb.Append("SELECT HolidayName, StartDate, EndDate, LastRStartDate FROM holiday where @date between StartDate and EndDate");


                    parms.Add(new MySqlParameter("@date", date));
                    dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                }

                if (dt.Rows.Count > 0)
                {

                    OHoliday h = new OHoliday();
                    h.HolidayName = Convert.ToString(dt.Rows[0]["HolidayName"]);
                    h.StartDate = Convert.ToString(dt.Rows[0]["StartDate"]);
                    h.EndDate = Convert.ToString(dt.Rows[0]["EndDate"]);
                    h.LastRDate = Convert.ToString(dt.Rows[0]["LastRDate"]);
                    return h;
                }
                else
                    return null;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<OFare> GetFareTable(string date)
        {
            List<OFare> List = new List<OFare>();
            DataTable dt = new DataTable();
            using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
            {
                StringBuilder sb = new StringBuilder();
                List<MySqlParameter> parms = new List<MySqlParameter>();
                sb.Append("SELECT  ChargeCode, StartDate, EndDate, ChargeName, FeeTableID, ChargeType, FlowType, MinQty, Currency, Amount  FROM charge_item where @date between StartDate and EndDate order by ChargeType ASC");

                parms.Add(new MySqlParameter("@date", date));
                try
                {
                    dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    OFare f = new OFare();
                    f.Amount = Convert.ToInt32(dt.Rows[i]["Amount"]);
                    f.ChargeCode = Convert.ToString(dt.Rows[i]["ChargeCode"]);
                    f.StartDate = Convert.ToString(dt.Rows[i]["StartDate"]);
                    f.EndDate = Convert.ToString(dt.Rows[i]["EndDate"]);
                    f.ChargeName = Convert.ToString(dt.Rows[i]["ChargeName"]);
                    f.ChargeType = Convert.ToString(dt.Rows[i]["ChargeType"]);
                    f.FlowType = Convert.ToString(dt.Rows[i]["FlowType"]);
                    f.MinQty = Convert.ToInt32(dt.Rows[i]["MinQty"]);
                    f.Currency = Convert.ToString(dt.Rows[i]["Currency"]);
                    List.Add(f);
                }
                return List;
            }
            else
                return null;
        }

        public string GetDistinctServiceStatus(string CityName, string DistinctName)
        {
            DataTable dt = new DataTable();
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            sb.Append("select ServiceStatus from city_distinct_data where CityName = @city and DistinctName = @dict");
            parms.Add(new MySqlParameter("@city", CityName));
            parms.Add(new MySqlParameter("@dict", DistinctName));

            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {

                    dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                    if (dt.Rows.Count > 0)
                    {
                        return Convert.ToString(dt.Rows[0]["ServiceStatus"]);
                    }
                    else
                        return null;
                }
            }
            catch (Exception ex)
            {
                throw new Exception
                (ex.Message);
            }
        }

        public string GetVillageServiceStatus(string CityName, string DistinctName, string VillageName)
        {
            DataTable dt = new DataTable();
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            sb.Append("select r.ServiceStatus,r.MinReserveCnt from city_distinct_data d,service_range_data r ");
            sb.Append("where d.DistinctID = r.DistinctID and  d.CityName = @city and d.DistinctName = @dict and r.VillageName = @vil ");
            parms.Add(new MySqlParameter("@city", CityName));
            parms.Add(new MySqlParameter("@dict", DistinctName));
            parms.Add(new MySqlParameter("@vil", VillageName));

            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                    if (dt.Rows.Count > 0)
                    {
                        return Convert.ToString(dt.Rows[0]["ServiceStatus"]) + "," + Convert.ToString(dt.Rows[0]["MinReserveCnt"]);
                    }
                    else
                        return null;
                }
            }
            catch (Exception ex)
            {
                throw new Exception
                (ex.Message);
            }
        }

        public List<OFleet> GetFleetList()
        {
            List<OFleet> List = new List<OFleet>();
            DataTable dt = new DataTable();

            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            sb.Append("	SELECT FleetID, FleetName,CompanyNo, ActiveFlag,CreateTime,UpdTime 	FROM fleet_info");

            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    OFleet f = new OFleet();
                    f.FleetID = Convert.ToInt32(dt.Rows[i]["FleetID"]);
                    f.FleetName = Convert.ToString(dt.Rows[i]["FleetName"]);
                    f.Company = Convert.ToString(dt.Rows[i]["CompanyNo"]);
                    f.ActiveFlag = Convert.ToBoolean(dt.Rows[i]["ActiveFlag"]);
                    f.CreateTime = Convert.ToDateTime(dt.Rows[i]["CreateTime"]);
                    f.UpdTime = Convert.ToDateTime(dt.Rows[i]["UpdTime"]);
                    List.Add(f);
                }

                return List;
            }
            else
                return null;
        }

        public OFleet GetFleet(int FleetID)
        {
            List<OFleet> List = new List<OFleet>();
            DataTable dt = new DataTable();

            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            sb.Append("	SELECT FleetID, FleetName, ActiveFlag,CreateTime,UpdTime 	FROM fleet_info Where FleetID = @ID");
            parms.Add(new MySqlParameter("@ID", FleetID));
            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            if (dt.Rows.Count > 0)
            {

                OFleet f = new OFleet();
                f.FleetID = Convert.ToInt32(dt.Rows[0]["FleetID"]);
                f.FleetName = Convert.ToString(dt.Rows[0]["FleetName"]);
                f.ActiveFlag = Convert.ToBoolean(dt.Rows[0]["ActiveFlag"]);
                f.CreateTime = Convert.ToDateTime(dt.Rows[0]["CreateTime"]);
                f.UpdTime = Convert.ToDateTime(dt.Rows[0]["UpdTime"]);

                return f;
            }
            else
                return null;
        }

        public List<OCar> GetCarsByFleet(string FleetID)
        {
            List<OCar> List = new List<OCar>();
            DataTable dt = new DataTable();

            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            sb.Append("SELECT CarNo, FleetID, CarType, Brand, Model, ManufactureDate, Color, Displacement, DriverID, ScheduleFlag, CreateTime, UpdTime ");
            sb.Append("FROM car_info where FleetID = @id");

            parms.Add(new MySqlParameter("@id", FleetID));

            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    OCar f = new OCar();
                    f.FleetID = Convert.ToInt32(dt.Rows[i]["FleetID"]);
                    f.Brand = Convert.ToString(dt.Rows[i]["Brand"]);
                    f.CarNo = Convert.ToString(dt.Rows[i]["CarNo"]);
                    f.Color = Convert.ToString(dt.Rows[i]["Color"]);
                    f.Displacement = Convert.ToString(dt.Rows[i]["Displacement"]);
                    f.DriverID = Convert.ToInt32(dt.Rows[i]["DriverID"]);

                    f.MDate = Convert.ToString(dt.Rows[i]["ManufactureDate"]);
                    f.Model = Convert.ToString(dt.Rows[i]["Model"]);
                    f.ScheduleFlag = Convert.ToString(dt.Rows[i]["ScheduleFlag"]);
                    f.Type = Convert.ToString(dt.Rows[i]["CarType"]);
                    f.CreateTime = Convert.ToDateTime(dt.Rows[i]["CreateTime"]);
                    f.UpdTime = Convert.ToDateTime(dt.Rows[i]["UpdTime"]);
                    List.Add(f);
                }

                return List;
            }
            else
                return null;
        }

        public List<OEmployee> GetDriversByFleet(string FleetID)
        {
            StringBuilder sb = new StringBuilder();
            DataTable dt = new DataTable();
            List<OEmployee> empList = new List<OEmployee>();

            sb.Append("select UserID, UserName,ActiveFlag  from user_info  Where FleetID = @id ");
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("@id", FleetID));
            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    OEmployee emp = new OEmployee();
                    emp.ID = Convert.ToInt32(dt.Rows[0]["UserID"]);
                    emp.Name = Convert.ToString(dt.Rows[0]["UserName"]);
                    emp.Active = Convert.ToString(dt.Rows[0]["ActiveFlag"]);

                    empList.Add(emp);
                }
            }
            else
                return null;

            return empList;


        }

        public bool InsertTrailFee(string Rno, OPrice op)//int Price)
        {
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();



            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    //sb.Append("UPDATE reservation_sheet SET Price=@price  WHERE ReservationNo = @no");
                    //parms.Add(new MySqlParameter("@no", Rno));
                    //parms.Add(new MySqlParameter("@price", op.Price));

                    //conn.executeUpdateQuery(sb.ToString(), parms.ToArray());

                    //sb.Length = 0;
                    //parms.Clear();

                    sb.Append("UPDATE reservation_charge_list ");
                    sb.Append("SET Price = @Price, RealAmt = @RealAmt, PromoteAmt = @PromoteAmt, CollectAmt = @CollectAmt, ");
                    sb.Append("TotalAmt = @TotalAmt, OfficialTotalAmt = @OfficialTotalAmt, CarFee = @CarFee, NightFee = @NightFee, ");
                    sb.Append("BaggageFee = @BaggageFee, OtherFee = @OtherFee, OtherSaleFee = @OtherSaleFee ");
                    sb.Append("WHERE  ReservationNo = @ReservationNo");

                    parms.Add(new MySqlParameter("@Price", op.Price));
                    parms.Add(new MySqlParameter("@RealAmt", op.RealAmt));
                    parms.Add(new MySqlParameter("@PromoteAmt", op.PromoteAmt));
                    parms.Add(new MySqlParameter("@CollectAmt", op.CollectAmt));
                    parms.Add(new MySqlParameter("@TotalAmt", op.TotalAmt));
                    parms.Add(new MySqlParameter("@OfficialTotalAmt", op.OfficialTotalAmt));
                    parms.Add(new MySqlParameter("@CarFee", op.CarFee));
                    parms.Add(new MySqlParameter("@NightFee", op.NightFee));
                    parms.Add(new MySqlParameter("@BaggageFee", op.BaggageFee));
                    parms.Add(new MySqlParameter("@OtherFee", op.OtherFee));
                    parms.Add(new MySqlParameter("@OtherSaleFee", op.OtherSaleFee));
                    parms.Add(new MySqlParameter("@ReservationNo", Rno));

                    conn.executeUpdateQuery(sb.ToString(), parms.ToArray());

                    return true;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public DataTable GetCoupon(string CouponCode)
        {
            StringBuilder sb = new StringBuilder();

            sb.Append("SELECT SeqNo, CouponCode, StartDate, EndDate, CarFeeSub,FixCarFee,  NightFeeSub, Memo,TakeDateS,TakeDateE,UseCnt,LimitCnt  ");
            sb.Append("FROM coupon_info 	where CouponCode = @code ");
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("@code", CouponCode));

            using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
            {
                return conn.executeSelectQuery(sb.ToString(), parms.ToArray());
            }
        }

        public int UpdSMSReport(string msgid, string dstaddr, DateTime dlvtime, DateTime donetime, string statusstr, string statuscode, string StatusFlag)
        {
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();

            sb.Append("UPDATE dispatch_sms_report ");
            sb.Append("SET dlvtime = @dlvtime, donetime = @donetime, statuscode = @statuscode, ");
            sb.Append("statusstr = @statusstr, StatusFlag = @StatusFlag WHERE msgid = @msgid ");

            parms.Add(new MySqlParameter("@msgid", msgid));
            parms.Add(new MySqlParameter("@dlvtime", dlvtime));
            parms.Add(new MySqlParameter("@donetime", donetime));
            parms.Add(new MySqlParameter("@statuscode", statuscode));
            parms.Add(new MySqlParameter("@statusstr", statusstr));
            parms.Add(new MySqlParameter("@StatusFlag", StatusFlag));

            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    return conn.executeUpdateQuery(sb.ToString(), parms.ToArray());
                }
            }
            catch (MySqlException ex)
            {
                throw ex;
            }
        }

        public bool InsSMSReport(string msgid, string ReservationNo, string PhoneNumber, string statusstr, string statuscode, string Message)
        {
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySql.Data.MySqlClient.MySqlParameter>();
            sb.Append("INSERT INTO dispatch_sms_report (msgid, ReservationNo,dstaddr, statuscode,statusstr,msg) VALUES (@msgid, @ReservationNo, @dstaddr, @statuscode,@statusstr,@msg)");
            parms.Add(new MySqlParameter("@msgid", msgid));
            parms.Add(new MySqlParameter("@ReservationNo", ReservationNo));
            parms.Add(new MySqlParameter("@dstaddr", PhoneNumber));
            parms.Add(new MySqlParameter("@statuscode", statuscode));
            parms.Add(new MySqlParameter("@statusstr", ""));
            parms.Add(new MySqlParameter("@msg", Message));
            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    if (conn.executeInsertQuery(sb.ToString(), parms.ToArray()) > 0)
                    {
                        sb.Length = 0;
                        parms.Clear();
                        sb.Append("Delete From reservation_sms Where ReservationNo = @rno");
                        parms.Add(new MySqlParameter("@rno", ReservationNo));

                        if (conn.executeDeleteQuery(sb.ToString(), parms.ToArray()) > 0)
                            return true;
                        else
                            return false;
                    }
                    else
                        return false;
                }
            }
            catch (MySql.Data.MySqlClient.MySqlException)
            {
                return false;
            }
        }
    }
}
