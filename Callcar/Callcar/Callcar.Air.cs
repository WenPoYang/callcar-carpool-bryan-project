﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Callcar.Air
{  

    public class AirInfo
    {

        public string GetGenFlightSchedule(string AirportID, int Top, int Skip)
        {

            string result = "", Ticket = "";

            Ticket = PtxRequest.GetTicket("cablesoft_nbd", "!QAZ2wsx");

            if (Ticket != "Error")
            {
                PtxRequest ptx = new PtxRequest(Ticket);

                ptx.SetRequest("", "Air", "GeneralSchedule", "International");

                Dictionary<string, string> dic = new Dictionary<string, string>();
                dic.Add("ticket", ptx.Ticket);
                dic.Add("$filter", "DepartureAirportID eq '" + AirportID + "' or ArrivalAirportID eq '" + AirportID + "'");
                dic.Add("$skip", Skip.ToString());
                dic.Add("$top", Top.ToString());


                dic.Add("$format", "json");
                ptx.SetParameter(dic);
                result = ptx.ExecuteRequest();

                if (result == "[]")
                    return null;
                else if (result == "Error")
                    return "Error";
                else
                    return result;
            }
            else
            {
                return "Error";
            }
        }

        public string GetGenFlightInfo(string FlightNo, string TakeDate)
        {

            string result = "", Ticket = "";
            TakeDate = TakeDate.Substring(0, 4) + "-" + TakeDate.Substring(4, 2) + "-" + TakeDate.Substring(6, 2);
            Ticket = PtxRequest.GetTicket("cablesoft_nbd", "!QAZ2wsx");

            if (Ticket != "Error")
            {
                PtxRequest ptx = new PtxRequest(Ticket);

                ptx.SetRequest("", "Air", "GeneralSchedule", "International");

                Dictionary<string, string> dic = new Dictionary<string, string>();
                dic.Add("ticket", ptx.Ticket);
                dic.Add("$filter", "FlightNumber eq '" + FlightNo + "' and ScheduleStartDate le '" + TakeDate + "' and ScheduleEndDate ge '" + TakeDate + "'");

                dic.Add("$top", "1");
                dic.Add("$format", "JSON");


                ptx.SetParameter(dic);
                result = ptx.ExecuteRequest();

                if (result == "[]")
                    return null;
                else if (result == "Error")
                    return "Error";
                else
                    return result;
            }
            else
            {
                return "Error";
            }
        }

        /// <summary>
        /// 查詢即時航班資訊
        /// </summary>
        /// <param name="FlightNo">航班編號中的數字欄位Ex：121</param>
        /// <returns></returns>
        public string GetInstantFlightInfo(string AirlineID,string FlightNo)
        {
            string result = "", Ticket = "";
            //TakeDate = TakeDate.Substring(0, 4) + "-" + TakeDate.Substring(4, 2) + "-" + TakeDate.Substring(6, 2);
            Ticket = PtxRequest.GetTicket("cablesoft_nbd", "!QAZ2wsx");

            if (Ticket != "Error")
            {
                PtxRequest ptx = new PtxRequest(Ticket);

                ptx.SetRequest("", "Air", "FIDS/Flight", FlightNo);

                Dictionary<string, string> dic = new Dictionary<string, string>();
                dic.Add("$filter", "AirlineID  eq '" + AirlineID + "'");
                dic.Add("ticket", ptx.Ticket);


                ptx.SetParameter(dic);
                result = ptx.ExecuteRequest();

                if (result == "[]")
                    return null;
                else if (result == "Error")
                    return "Error";
                else
                    return result;
            }
            else
            {
                return "Error";
            }
        }
    }

}
