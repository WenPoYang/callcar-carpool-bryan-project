﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Callcar.DataModel
{
    public class dmAppSetting
    {
        public int SeqNo { get; set; }
        public string AppName { get; set; }
        public string SupportOS { get; set; }
        public string LastVersion { get; set; }
        public string Remark { get; set; }
    }
}
