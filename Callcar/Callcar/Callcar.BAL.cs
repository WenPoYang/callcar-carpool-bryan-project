﻿using System;
using System.Collections.Generic;
using Callcar.Struct;
using Callcar.CObject;
using System.Data;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Callcar.DataModel;
using System.Text;
using System.Net;
using System.IO;
using System.Net.Security;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;


namespace Callcar.BAL
{
    public class CallcarBAL
    {
        protected string _ConnectionString;
        public CallcarBAL(string ConnectionString)
        {
            this._ConnectionString = ConnectionString;
        }
    }
    /// <summary>
    /// 會員帳號的BAL
    /// </summary>
    public class BALUserAccount : CallcarBAL
    {
        public BALUserAccount(string ConnectionString)
            : base(ConnectionString)
        {
        }

        /// <summary>
        /// 從社群軟體新增帳號
        /// </summary>
        /// <param name="data"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public int CreateAccFromFB(APIInsAccFB js)
        {
            //string key = Common.AESCryptography.TokenAnalysis(token);
            //string iv = "8417728284177282";
            //Common.AESCryptography aes = new Common.AESCryptography(token, iv);
            //string plantext = aes.Decrypt(data);
            //APIInsAccFB js;
            //try
            //{
            //    js = JsonConvert.DeserializeObject<APIInsAccFB>(plantext);
            //}
            //catch (Exception)
            //{
            //    throw new Exception("DataError");
            //}
            OAccount acc = new OAccount();
            acc.Source = js.source;
            acc.SourceID = js.sid;
            acc.Email = js.email;
            acc.FName = js.fname;
            acc.Gender = Int32.Parse(js.sex);
            acc.LName = js.lname;
            acc.MName = js.mname;
            acc.MobilePhone = js.mobile;
            acc.NickName = js.nname;

            Callcar.DAL.DALUserAccount u = new DAL.DALUserAccount(this._ConnectionString);
            try
            {
                return u.AddAccount(acc.Source, acc.SourceID, acc.Email, acc.FName, acc.LName, acc.MName, acc.Gender, acc.MobilePhone, acc.NickName, acc.Password);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public int CreateAccFromAPP(string data, string token)
        {
            //string key = Common.AESCryptography.TokenAnalysis(token);
            string iv = "8417728284177282";
            Common.AESCryptography aes = new Common.AESCryptography(token, iv);
            string plantext = aes.Decrypt(data);
            APIInsAccAPP js;
            try
            {
                js = JsonConvert.DeserializeObject<APIInsAccAPP>(plantext);
            }
            catch (Exception)
            {
                throw new Exception("DataError");
            }
            if (js.email.Contains(" ") || !js.email.Contains("@") || !js.email.Contains("."))
            {
                throw new Exception("帳號(Email)格式錯誤");
            }
            OAccount acc = new OAccount();
            acc.Source = "APP";
            acc.Email = js.email;
            acc.FName = js.fname;
            acc.LName = js.lname;
            acc.MobilePhone = js.mobile;
            acc.Password = Common.MD5Cryptography.Encrypt(js.pw);

            Callcar.DAL.DALUserAccount u = new DAL.DALUserAccount(this._ConnectionString);
            try
            {
                return u.AddAccount(acc.Source, acc.SourceID, acc.Email, acc.FName, acc.LName, acc.MName, acc.Gender, acc.MobilePhone, acc.NickName, acc.Password);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public bool UpdAccountData(int index, string data, string token)
        {
            //string key = Common.AESCryptography.TokenAnalysis(token);
            string iv = "8417728284177282";
            Common.AESCryptography aes = new Common.AESCryptography(token, iv);
            string plantext = aes.Decrypt(data);

            string id, value;
            string[] param = plantext.Split(',');

            id = param[0];
            value = param[1];

            try
            {
                DAL.DALUserAccount dal = new DAL.DALUserAccount(this._ConnectionString);
                switch (index)
                {
                    case 1:

                        dal.UpdAccountByColumn(Int32.Parse(id), "NickName", value);
                        break;
                    case 2:
                        dal.UpdAccountByColumn(Int32.Parse(id), "FirstName", value);
                        break;
                    case 3:
                        dal.UpdAccountByColumn(Int32.Parse(id), "LastName", value);
                        break;
                    case 4:
                        dal.UpdAccountByColumn(Int32.Parse(id), "MidName", value);
                        break;
                    case 5:
                        dal.UpdAccountByColumn(Int32.Parse(id), "Gender", value);
                        break;
                    case 6:
                        dal.UpdAccountByColumn(Int32.Parse(id), "AgeRange", value);
                        break;
                    case 7:
                        dal.UpdAccountByColumn(Int32.Parse(id), "Local", value);
                        break;
                    case 8:
                        dal.UpdAccountByColumn(Int32.Parse(id), "Email", value);
                        break;
                    case 9:
                        dal.UpdAccountByColumn(Int32.Parse(id), "Birthday", value);
                        break;
                    case 10:
                        dal.UpdAccountByColumn(Int32.Parse(id), "MobilePhone", value);
                        break;
                    case 11:
                        dal.UpdAccountByColumn(Int32.Parse(id), "PicUrl", value);
                        break;
                }
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public OAccount GetAccountInfo(int UserID)
        {
            try
            {
                DAL.DALUserAccount dal = new DAL.DALUserAccount(this._ConnectionString);
                OAccount acc = dal.GetAccountInfo(UserID);
                return acc;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public OAccount GetAccountInfo(string Source, string SourceID)
        {
            try
            {
                DAL.DALUserAccount dal = new DAL.DALUserAccount(this._ConnectionString);
                OAccount acc = dal.GetAccountInfo(Source, SourceID);
                return acc;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public OAccount GetAccountInfo(string Email)
        {
            try
            {
                DAL.DALUserAccount dal = new DAL.DALUserAccount(this._ConnectionString);
                OAccount acc = dal.GetAccountInfo(Email);

                return acc;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public string ChkEmailExist(string data, string token)
        {
            //string key = Common.AESCryptography.TokenAnalysis(token);
            string iv = "8417728284177282";
            Common.AESCryptography aes = new Common.AESCryptography(token, iv);
            string plantext = aes.Decrypt(data);

            OAccount acc = GetAccountInfo(plantext);

            if (acc == null)
                return null;
            else
                return acc.Source;
        }

        //public bool ChkAccountPW(string data, string token)
        //{
        //    //string key = Common.AESCryptography.TokenAnalysis(token);
        //    string iv = "8417728284177282";
        //    Common.AESCryptography aes = new Common.AESCryptography(token, iv);
        //    string plantext = aes.Decrypt(data);

        //    AccountPWStruct ps = JsonConvert.DeserializeObject<AccountPWStruct>(plantext);
        //    string Email = ps.acc;
        //    string Password = ps.pw;

        //    DAL.DALUserAccount dal = new DAL.DALUserAccount(this._ConnectionString);
        //    if (Common.MD5Cryptography.Encrypt(Password).Equals(dal.GetPassword(Email)))
        //        return true;
        //    else
        //        return false;
        //}

        public bool ChkAccountPW(string Email, string Password)
        {

            DAL.DALUserAccount dal = new DAL.DALUserAccount(this._ConnectionString);
            if (Common.MD5Cryptography.Encrypt(Password).Equals(dal.GetPassword(Email)))
                return true;
            else
                return false;
        }

        public OAccount GetAccountFromEmail(string data, string token)
        {
            //string key = Common.AESCryptography.TokenAnalysis(token);
            string iv = "8417728284177282";
            Common.AESCryptography aes = new Common.AESCryptography(token, iv);
            string plantext = aes.Decrypt(data);

            OAccount acc = GetAccountInfo(plantext);

            if (acc == null)
                return null;
            else
                return acc;
        }

        public OAccount GetAccountFromEmail(string Email)
        {
            OAccount acc = GetAccountInfo(Email);

            if (acc == null)
                return null;
            else
                return acc;
        }


        public OAccount GetAccountFromFB(string data, string token)
        {
            //string key = Common.AESCryptography.TokenAnalysis(token);
            string iv = "8417728284177282";
            Common.AESCryptography aes = new Common.AESCryptography(token, iv);
            string plantext = aes.Decrypt(data);
            string Source = plantext.Split(',')[0];
            string SourceID = plantext.Split(',')[1];

            OAccount acc = GetAccountInfo(Source, SourceID);

            if (acc == null)
                return null;
            else
                return acc;
        }

        public bool UpdAccountPW(int UserID, string oldPW, string newPW)
        {
            string oldPWMD5 = Common.MD5Cryptography.Encrypt(oldPW);
            string newPWMD5 = Common.MD5Cryptography.Encrypt(newPW);

            DAL.DALUserAccount dal = new DAL.DALUserAccount(this._ConnectionString);
            try
            {
                return dal.UpdPassword(UserID, oldPWMD5, newPWMD5);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public string RenewAccountRandomPW(int UserID, string Email)
        {
            OAccount oa = this.GetAccountInfo(UserID);
            if (oa.Email != Email)
                throw new Exception("使用者帳號不符合");

            string newPW = Callcar.Common.General.GetRandomString(8);
            string newPWMD5 = Callcar.Common.MD5Cryptography.Encrypt(newPW);

            DAL.DALUserAccount dal = new DAL.DALUserAccount(this._ConnectionString);
            if (!dal.UpdRandomPassword(oa.ID, newPWMD5))
                throw new Exception("更新密碼失敗");

            Callcar.Common.GmailSMTP smtp = new Common.GmailSMTP("support@ishareco.com", "share52439256b");
            string Subject;
            StringBuilder Body = new StringBuilder();

            Body.Append(oa.LName + oa.FName + " 您好： <br />");

            Body.Append("您在" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "  通知艾雪科技服務中心, 要求我們EMAIL您新的密碼 <p>");

            Body.Append("會員帳號: " + Email + "<br />");
            Body.Append("密碼: " + newPW + "<p>");

            Body.Append("請您務必盡快登入後，更改成您所慣用之密碼 <br /><p>");

            Body.Append("艾雪科技 服務中心<br />");
            Body.Append(@"網址: <a href='https://www.ishareco.com/callcar'> https://www.ishareco.com</a> <br />");
            Body.Append(@"Email: <a href='mailto:support@ishareco.com'>support@ishareco.com</a> <br />");
            Subject = "CallCar機場接駁 新密碼通知";
            List<string> MailAddress = new List<string>();
            MailAddress.Add(Email);
            try
            {
                string Result = smtp.Send(MailAddress, "艾雪科技客服通知", Subject, Body.ToString());
                if (Result != "Success")
                    throw new Exception("寄信失敗");

                return "Success";
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }

    /// <summary>
    /// 員工的BAL
    /// </summary>
    public class BALEmployee : CallcarBAL
    {
        public BALEmployee(string ConnectionString) : base(ConnectionString) { }

        public int AddNewEmployeeBackWeb(string data)
        {
            EmployeInfoStruct es = JsonConvert.DeserializeObject<EmployeInfoStruct>(data);

            //DAL.DALSerialNum dSerial = new DAL.DALSerialNum(this._ConnectionString);
            //int UID = 0;

            //try
            //{
            //    OSerialNum serial = dSerial.GetSerialNumObject("EmployeeID");
            //    UID = Int32.Parse(DateTime.Now.ToString(serial.Prefix) + dSerial.GetSeqNoString(serial));
            //}
            //catch (Exception ex)
            //{
            //    throw new Exception(ex.Message);
            //}

            DAL.DALEmployee dal = new DAL.DALEmployee(this._ConnectionString);
            try
            {
                return dal.AddAccount(es.pw, es.empname, es.cid, es.phone1, es.phone2, es.role, es.company, es.fleet, es.city, es.distinct, es.address, es.email);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// 司機的 帳密驗證
        /// </summary>
        /// <param name="data"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public bool ChkAccPWAPP(string Account, string Password)
        {

            // string iv = "8417728284177282";
            //Common.AESCryptography aes = new Common.AESCryptography(token, iv);
            //string plantext = aes.Decrypt(data);

            //string Account = ps.acc;
            //string Password = ps.pw;

            return _chkDriverPW(Account, Password);
        }

        //後台的帳密驗證
        public EmployeInfoStruct ChkAccPWandGetInfoBackWeb(string data)
        {
            try
            {
                AccountPWStruct ps = JsonConvert.DeserializeObject<AccountPWStruct>(data);
                string Account = ps.acc;
                string Password = ps.pw;
                int EmpID;
                DAL.DALEmployee dal = new DAL.DALEmployee(this._ConnectionString);
                EmpID = dal.GetEmployeeIDByEmail(Account);
                if (EmpID == 0)
                    throw new Exception("帳號密碼錯誤");

                if (_chkAccPW(EmpID, Password))
                {

                    OEmployee emp = dal.GetEmployeeInfoByID(EmpID);
                    EmployeInfoStruct eis = new EmployeInfoStruct();
                    if (emp.Active == "0")
                        return null;
                    else
                    {
                        eis.id = EmpID;
                        eis.role = emp.Role;
                        eis.Logint_tGroups_AutoID = 1;
                        eis.empname = emp.Name;
                        eis.company = emp.CompanyNo;
                        eis.fleet = emp.FleetID.ToString();
                        eis.active = "1";
                        eis.address = "";
                        eis.cid = "";
                        eis.city = "";
                        eis.distinct = "";
                        eis.phone1 = "";
                        eis.phone2 = "";
                        eis.pw = "";
                        return eis;
                    }
                }
                else
                    return null;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.StackTrace);
            }
        }

        public string SetEmployeeStatus(string data)
        {
            int EmpID = Int32.Parse(data.Split(',')[0]);
            string OldStatus = data.Split(',')[1];
            if (OldStatus == "Active")
                OldStatus = "1";
            else
                OldStatus = "0";


            string NewStatus = data.Split(',')[2];
            if (NewStatus == "Active")
                NewStatus = "1";
            else
                NewStatus = "0";

            DAL.DALEmployee dal = new DAL.DALEmployee(this._ConnectionString);
            try
            {
                string status = dal.SetEmployeStatus(EmpID, OldStatus, NewStatus);
                if (status == "1")
                    return "Active";
                else
                    return "Deactive";
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private bool _chkAccPW(int EmpID, string PW)
        {
            DAL.DALEmployee dal = new DAL.DALEmployee(this._ConnectionString);
            string DBPW = dal.GetPassword(EmpID);
            string OutPW = Common.MD5Cryptography.Encrypt(PW);
            if (OutPW.Equals(DBPW))
                return true;
            else
                return false;
        }

        private bool _chkDriverPW(string CitizenID, string PW)
        {
            DAL.DALEmployee dal = new DAL.DALEmployee(this._ConnectionString);
            if (dal.GetEmployeeIDByCitizenID(CitizenID) == 0)
                return false;

            if (CitizenID.Substring(5, 5).Equals(PW))
                return true;
            else
                return false;
        }

        public List<OEmployee> GetALLEmployee()
        {
            DAL.DALEmployee dal = new DAL.DALEmployee(this._ConnectionString);
            List<OEmployee> List = new List<OEmployee>();

            return dal.GetEmployeeInfo();

        }

        public DriverInfoStruct GetDriverInfo(int UserID)
        {
            DAL.DALEmployee dal = new DAL.DALEmployee(this._ConnectionString);
            OEmployee oe = dal.GetEmployeeInfoByID(UserID);
            if (oe != null)
            {
                DriverInfoStruct ds = new DriverInfoStruct();
                ds.id = oe.ID;
                ds.name = oe.Name;
                ds.fno = oe.FleetID;
                ds.fname = "測試車隊";
                return ds;
            }
            else
                return null;
        }

        public OEmployee GetEmployee(int EmpID)
        {
            DAL.DALEmployee dal = new DAL.DALEmployee(this._ConnectionString);
            try
            {
                return dal.GetEmployeeInfoByID(EmpID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<OEmployee> GetDriverByFleet(int FleetID)
        {
            DAL.DALGeneral dalg = new DAL.DALGeneral(this._ConnectionString);
            List<OFleet> fList = dalg.GetFleetList();
            OFleet f = fList.Find(x => x.FleetID == FleetID);
            if (f == null)
                return null;
            DAL.DALEmployee dal = new DAL.DALEmployee(this._ConnectionString);
            List<OEmployee> List = dal.GetEmployeeByCompany(f.Company);
            if (List != null)
                return List.FindAll(x => x.Role == "D" && x.FleetID == FleetID);
            else
                return null;
        }
    }

    /// <summary>
    /// 電子發票的BAL
    /// </summary>
    public class BALInvoice : CallcarBAL
    {

        public BALInvoice(string ConnectionString)
            : base(ConnectionString)
        {

        }

        public int CreateNewInvoice(string data, string token)
        {
            //string key = Common.AESCryptography.TokenAnalysis(token);
            string iv = "8417728284177282";
            Common.AESCryptography aes = new Common.AESCryptography(token, iv);
            string plantext = aes.Decrypt(data);
            APINewInvoice ni;
            try
            {
                ni = JsonConvert.DeserializeObject<APINewInvoice>(plantext);
                DAL.DALInvoice dal = new DAL.DALInvoice(this._ConnectionString);
                return dal.CreatenewInvoice(Int32.Parse(ni.uid), ni.ititle, ni.ein, ni.iaddress);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public bool RemoveInvoice(int InvoiceID, int UserID)
        {
            DAL.DALInvoice dal = new DAL.DALInvoice(this._ConnectionString);
            try
            {
                return dal.RemoveInvoice(UserID, InvoiceID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public OInvoice GetUserInvoice(string data, string token)
        {
            //string key = Common.AESCryptography.TokenAnalysis(token);
            string iv = "8417728284177282";
            Common.AESCryptography aes = new Common.AESCryptography(token, iv);
            string plantext = aes.Decrypt(data);
            int UserID = Int32.Parse(plantext);
            DAL.DALInvoice dal = new DAL.DALInvoice(this._ConnectionString);
            OInvoice invoice = dal.QryUserInvoice(UserID);

            if (invoice != null)
                return invoice;
            else
                return null;
        }
    }

    /// <summary>
    /// 信用卡的BAL
    /// </summary>
    public class BALCreditCard : CallcarBAL
    {
        public BALCreditCard(string ConnectionString)
            : base(ConnectionString)
        {

        }

        public int CreateNewCard(APINewCard ic, string Pay2GoUrl, string MerchantID, string Hash, string IV)
        {
            BALUserAccount bal = new BALUserAccount(this._ConnectionString);
            OAccount acc = bal.GetAccountInfo(Int32.Parse(ic.uid));
            string CreditOrderNo;
            DAL.DALSerialNum dSerial = new DAL.DALSerialNum(this._ConnectionString);
            try
            {
                OSerialNum serial = dSerial.GetSerialNumObject("CreditAuth");
                CreditOrderNo = DateTime.Now.ToString(serial.Prefix) + dSerial.GetSeqNoString(serial);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


            Callcar.Common.Pay2GoCardAuth auth = new Common.Pay2GoCardAuth(Pay2GoUrl, MerchantID, Hash, IV);
            auth.SetAuthData(CreditOrderNo, 1, "卡片驗證，不執行請款", acc.Email, ic.cno, ic.edate, ic.acode, acc.ID.ToString());
            if (auth.Authorize() != "Success")
                throw new Exception("信用卡驗證失敗");

            DAL.DALCreditCard dCard = new DAL.DALCreditCard(this._ConnectionString);
            try
            {
                ic.edate = "20" + ic.edate.Substring(2, 2) + ic.edate.Substring(0, 2);
                //DAL.DALUserAccount dalu = new DAL.DALUserAccount(this._ConnectionString);
                //string PW = dalu.GetAccountPW(acc.ID);
                //if (PW == "")
                //    throw new Exception("信用卡新增失敗" + Environment.NewLine + "信用卡加密失敗");
                string IDMD5;

                IDMD5 = Callcar.Common.MD5Cryptography.Encrypt(ic.uid);

                Callcar.Common.AESCryptography aes = new Common.AESCryptography(IDMD5.Substring(0, 16), IDMD5.Substring(16, 16));
                ic.cno = aes.Encrypt(ic.cno);
                return dCard.CreateNewCard(acc.ID, ic.cno, auth.GetToken(), auth.GetTokenLife());
            }
            catch (Exception ex)
            {
                throw new Exception("DB Insert 新增信用卡失敗" + Environment.NewLine + ex.StackTrace);
            }
        }

        public bool RemoveCard(int CardID, int UserID)
        {
            DAL.DALCreditCard dal = new DAL.DALCreditCard(this._ConnectionString);
            try
            {
                return dal.RemoveCard(UserID, CardID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public OCard GetUserActiveCard(int UserID)
        {

            DAL.DALCreditCard dal = new DAL.DALCreditCard(this._ConnectionString);


            OCard card = dal.QryUserCards(UserID);

            if (card != null)
            {
                Callcar.DAL.DALUserAccount dalu = new DAL.DALUserAccount(this._ConnectionString);
                //string PW = dalu.GetAccountPW(UserID);
                //if (PW == "")
                //    throw new Exception("信用卡查詢失敗" + Environment.NewLine + "信用卡解密失敗");
                string IDMD5;

                IDMD5 = Callcar.Common.MD5Cryptography.Encrypt(Convert.ToString(UserID));
                Callcar.Common.AESCryptography aes = new Common.AESCryptography(IDMD5.Substring(0, 16), IDMD5.Substring(16, 16));
                card.CNo = aes.Decrypt(card.CNo);

                return card;
            }
            else
                return null;



        }
    }

    /// <summary>
    /// 訂單的BAL
    /// </summary>
    public class BALReservation : CallcarBAL
    {
        public BALReservation(string ConnectionString) : base(ConnectionString) { }

        public string CreateNewReservation(APINewOrder os)//string data, string token)
        {
            string ReservationNo = string.Empty;

            if (os.pcartype == "1")
            {
                BALDispatch bald = new BALDispatch(this._ConnectionString);
                try
                {
                    if (bald.isCarFull(os.dno))
                    {
                        throw new Exception("此車次已額滿");
                    }

                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }

            OReservation o = new OReservation();
            o.BaggageCnt = Int32.Parse(os.bcnt);
            o.ChooseType = os.ctype;
            o.Coupon = os.coupon.Trim();

            o.PickCartype = os.pcartype;
            if (os.ctype == "F")
            {
                o.FlightTime = Common.General.ConvertToDateTime(os.fdate + os.ftime, 12);
                o.FlightDate = os.fdate;
                o.FlightNo = os.fno.Trim();
            }
            else
            {
                o.FlightNo = "";
                o.FlightDate = "";
            }
            OCard card = new OCard();
            DAL.DALCreditCard dalc = new DAL.DALCreditCard(this._ConnectionString);
            OCard oc = dalc.QryUserCards(Int32.Parse(os.uid));
            DAL.DALInvoice dali = new DAL.DALInvoice(this._ConnectionString);
            if (os.idata != "" && os.idata != "null")
            {
                OInvoice oi = dali.QryUserInvoice(Int32.Parse(os.uid));
                o.Invoice = JsonConvert.SerializeObject(oi);
            }
            else
                o.Invoice = "";

            o.Credit = JsonConvert.SerializeObject(oc);
            o.PassengerCnt = Int32.Parse(os.pcnt);
            o.Ternimal = os.terminal;
            o.Airport = os.airport;
            o.PickupCity = os.pcity;
            o.PickupDistinct = os.pdistinct;
            o.PickupAddress = os.paddress;
            o.PickupVillage = os.pvillage;
            o.PickupGPS = new OSpatialGPS(double.Parse(os.plat), double.Parse(os.plng));
            o.PreferTime = os.ptime;
            o.ProcessStage = "0";
            o.ServeDate = os.sdate;
            o.ServiceType = os.stype;
            o.TakeoffAddress = os.taddress;
            o.TakeoffCity = os.tcity;
            o.TakeoffDistinct = os.tdistinct;
            o.TakeoffVillage = os.tvillage;
            o.TakeoffGPS = new OSpatialGPS(double.Parse(os.tlat), double.Parse(os.tlng));
            o.UserID = Int32.Parse(os.uid);
            o.MaxFlag = os.max;
            OPrice op = new OPrice();
            BALGeneral balg = new BALGeneral(this._ConnectionString);
            if (os.pcartype == "1")
                op = balg.CalculateTrailFee(DateTime.Now.ToString("yyyyMMdd"), o.ServeDate, o.PreferTime, o.PassengerCnt, o.BaggageCnt, o.Coupon, os.dno, 0, true, o.PreferTime, o.UserID);
            else
                op = balg.CalculateTrailFee(DateTime.Now.ToString("yyyyMMdd"), o.ServeDate, o.PreferTime, o.PassengerCnt, o.BaggageCnt, o.Coupon, 0, 0, true, o.PreferTime, o.UserID);

            o.TrailPrice = op.Price;

            BALUserAccount balu = new BALUserAccount(this._ConnectionString);
            OAccount oa = balu.GetAccountInfo(o.UserID);

            if (os.pname == null || os.pname == "null" || os.pname == "")
                os.pname = oa.LName + oa.FName;
            if (os.pmobile == null || os.pmobile == "null" || os.pmobile == "")
                os.pmobile = oa.MobilePhone;

            o.PassengerName = os.pname;
            o.PassengerPhone = os.pmobile;

            DAL.DALSerialNum dSerial = new DAL.DALSerialNum(this._ConnectionString);
            try
            {
                OSerialNum serial = dSerial.GetSerialNumObject("Reservation");
                ReservationNo = DateTime.Now.ToString(serial.Prefix) + dSerial.GetSeqNoString(serial);

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            o.ReservationNo = ReservationNo;

            try
            {
                DAL.DALReservation dalReservation = new DAL.DALReservation(this._ConnectionString);
                if (dalReservation.CreateNewReservation(o, os.pcartype, os.dno, os.servetime, op))
                    return ReservationNo;
                else
                    return null;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public List<OReservation> GetReservationByDate(string date)
        {
            DAL.DALReservation dalr = new DAL.DALReservation(this._ConnectionString);
            List<OReservation> ListR;
            try
            {
                ListR = dalr.GetReservations(date, date);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            if (ListR == null)
                return null;
            else
                return ListR;
        }

        public List<OReservation> GetReservationByConditions(string data)
        {
            try
            {
                QOrderCondStruct qs = JsonConvert.DeserializeObject<QOrderCondStruct>(data);
                if (string.IsNullOrWhiteSpace(qs.QSDate) || string.IsNullOrWhiteSpace(qs.QEDate))
                {
                    throw new Exception("日期為必須條件");
                }

                if (qs.take < 0 || qs.skip < 0)
                {
                    throw new Exception("查詢範圍異常");
                }

                DAL.DALReservation dalr = new DAL.DALReservation(this._ConnectionString);

                return dalr.GetReservationsMultipleCondition(qs, qs.take, qs.skip);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public List<OReservation> GetReservationsByUser(int UserID)
        {
            DataTable dt;
            List<OReservation> List = new List<OReservation>();
            try
            {
                DAL.DALReservation dal = new DAL.DALReservation(this._ConnectionString);

                dt = dal.GetReservationsByUser(UserID);

                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        OReservation r = new OReservation();
                        r.BaggageCnt = Convert.ToInt32(dt.Rows[i]["BaggageCnt"]);
                        r.ChooseType = Convert.ToString(dt.Rows[i]["SelectionType"]);
                        r.Coupon = Convert.ToString(dt.Rows[i]["Coupon"]);
                        r.Credit = Convert.ToString(dt.Rows[i]["CreditData"]);
                        if (r.ChooseType == "F")
                        {
                            r.FlightDate = Convert.ToDateTime(dt.Rows[i]["ScheduleFlightTime"]).ToString("yyyyMMdd");
                            r.FlightNo = Convert.ToString(dt.Rows[i]["FlightNo"]);
                            r.FlightTime = Convert.ToDateTime(dt.Rows[i]["ScheduleFlightTime"]);
                        }
                        r.Airport = Convert.ToString(dt.Rows[i]["Airport"]);
                        r.Ternimal = Convert.ToString(dt.Rows[i]["Ternimal"]);
                        r.Invoice = Convert.ToString(dt.Rows[i]["InvoiceData"]);
                        r.MaxFlag = Convert.ToString(dt.Rows[i]["MaxFlag"]);
                        r.OrderTime = Convert.ToDateTime(dt.Rows[i]["CreateTime"]);
                        r.PassengerCnt = Convert.ToInt32(dt.Rows[i]["PassengerCnt"]);
                        r.PickCartype = Convert.ToString(dt.Rows[i]["PickCartype"]);
                        r.PickupAddress = Convert.ToString(dt.Rows[i]["PickupAddress"]);
                        r.PickupCity = Convert.ToString(dt.Rows[i]["PickupCity"]);
                        r.PickupDistinct = Convert.ToString(dt.Rows[i]["PickupDistinct"]);
                        r.PickupVillage = Convert.ToString(dt.Rows[i]["PickupVillage"]);
                        r.PreferTime = Convert.ToString(dt.Rows[i]["TimeSegment"]);
                        r.ProcessStage = Convert.ToString(dt.Rows[i]["ProcessStage"]);
                        r.ReservationNo = Convert.ToString(dt.Rows[i]["ReservationNo"]);
                        r.ServeDate = Convert.ToString(dt.Rows[i]["TakeDate"]);
                        r.ServiceType = Convert.ToString(dt.Rows[i]["ServiceType"]);
                        r.TakeoffAddress = Convert.ToString(dt.Rows[i]["TakeoffAddress"]);
                        r.TakeoffCity = Convert.ToString(dt.Rows[i]["TakeoffCity"]);
                        r.TakeoffDistinct = Convert.ToString(dt.Rows[i]["TakeoffDistinct"]);
                        r.TakeoffVillage = Convert.ToString(dt.Rows[i]["TakeoffVillage"]);
                        r.UserID = Convert.ToInt32(dt.Rows[i]["UserID"]);
                        r.EIN = Convert.ToString(dt.Rows[i]["InvoiceNum"]);
                        r.TakeoffGPS = new OSpatialGPS(Convert.ToDouble(dt.Rows[i]["tlat"]), Convert.ToDouble(dt.Rows[i]["tlng"]));
                        r.PickupGPS = new OSpatialGPS(Convert.ToDouble(dt.Rows[i]["plat"]), Convert.ToDouble(dt.Rows[i]["plng"]));
                        r.AddBagCnt = Convert.ToInt32(dt.Rows[i]["AddBaggageCnt"]);
                        r.PassengerName = Convert.ToString(dt.Rows[i]["PassengerName"]);
                        r.PassengerPhone = Convert.ToString(dt.Rows[i]["PassengerPhone"]);

                        List.Add(r);
                    }
                    return List;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public OReservation GetReservationByNo(string RNo)
        {
            DAL.DALReservation dalr = new DAL.DALReservation(this._ConnectionString);
            OReservation R;
            try
            {
                R = dalr.GetReservation(RNo);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            if (R == null)
                return null;
            else
                return R;
        }

        public bool UpdServicePosition(string data)
        {
            UOrderStruct uo = JsonConvert.DeserializeObject<UOrderStruct>(data);
            if (uo.uType != "ServicePosition")
                return false;

            List<string> City = new List<string>();
            List<string> Distinct = new List<string>();
            List<string> Village = new List<string>();
            List<string> Address = new List<string>();
            List<string> Lat = new List<string>();
            List<string> Lng = new List<string>();
            string[] Olddata = uo.OldData.Split(',');
            string[] Newdata = uo.NewData.Split(',');
            City.Add(Olddata[0]);
            City.Add(Newdata[1]);
            Distinct.Add(Olddata[0]);
            Distinct.Add(Olddata[1]);
            Village.Add(Olddata[0]);
            Village.Add(Olddata[1]);
            Address.Add(Olddata[0]);
            Address.Add(Olddata[1]);
            Lat.Add(Olddata[0]);
            Lat.Add(Olddata[1]);
            Lng.Add(Olddata[0]);
            Lng.Add(Olddata[1]);

            DAL.DALReservation dalr = new DAL.DALReservation(this._ConnectionString);
            OReservation r = dalr.GetReservation(uo.RNo);

            try
            {
                return dalr.UpdReservationServicePostion(uo.RNo, r.ServiceType, City, Distinct, Village, Address, Lat, Lng);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        /// <summary>
        /// 取消訂單
        /// </summary>
        /// <param name="Source">取消來源 U:使用者 B:後台</param>
        /// <param name="Rno">訂單編號</param>
        /// <returns></returns>
        public string CancelReservation(string Source, string Rno)
        {
            BALReservation balr = new BALReservation(this._ConnectionString);
            OReservation or;

            try
            {
                or = balr.GetReservationByNo(Rno);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            DAL.DALReservation dalr = new DAL.DALReservation(this._ConnectionString);

            DateTime NowTime = DateTime.Now;
            bool rtn = false;

            if (or == null)
                return "XXX";

            if (or.ProcessStage == "X" || or.ProcessStage == "B" || or.ProcessStage == "C" || or.ProcessStage == "Z")
            {
                return "999";// 目前狀態無法取消
            }
            else if (or.ProcessStage == "A")
            {
                if (Source == "U")
                {
                    DateTime TakeDatetime = Callcar.Common.General.ConvertToDateTime(or.ServeDate + or.PreferTime, 12);

                    double diffTime = new TimeSpan(TakeDatetime.Ticks - NowTime.Ticks).TotalHours;
                    if (diffTime < 24)
                        return "998";//24小時內無法取消
                }

                try
                {
                    rtn = dalr.CancelReservation(Rno, or.ProcessStage);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }


            }
            else if (or.ProcessStage == "0")
            {
                try
                {
                    rtn = dalr.CancelReservation(Rno, or.ProcessStage);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }

            }
            else
            {
                return "XXX";
            }

            if (rtn)
                return "Success";
            else
                return "Failure";
        }

        public Dictionary<DateTime, int> GetFloatingPriceByOrder(string Rno)
        {
            DAL.DALReservation dal = new DAL.DALReservation(this._ConnectionString);
            DataTable dt = dal.GetFloatingPriceByOrder(Rno);
            Dictionary<DateTime, int> FPrice = new Dictionary<DateTime, int>();
            int oldprice = 0;

            for (int i = 0; i < dt.Rows.Count; i++)
            {

                if (oldprice != Convert.ToInt32(dt.Rows[i]["price"]))
                    FPrice.Add(Convert.ToDateTime(dt.Rows[i]["CreateTime"]), Convert.ToInt32(dt.Rows[i]["price"]));

                oldprice = Convert.ToInt32(dt.Rows[i]["price"]);
            }
            return FPrice;
        }
    }

    /// <summary>
    /// 航班的BAL
    /// </summary>
    public class BALFlight : CallcarBAL
    {
        public BALFlight(string ConnectionString) : base(ConnectionString) { }

        /// <summary>
        ///  [舊版] 回傳航班資料 (未判斷出回國)
        /// </summary>
        /// <param name="FlightNo"></param>
        /// <param name="FlightDate"></param>
        /// <returns></returns>
        public APIFlightStruct GetFlightInfo(string FlightNo, string FlightDate)
        {
            DAL.DALFlight dal = new DAL.DALFlight(this._ConnectionString);
            try
            {
                //將CI0001轉為 CI1
                //取代掉 dash
                FlightNo = FlightNo.Replace("-", "");
                string AirlineID = FlightNo.Substring(0, 2);
                string FlightNumber = FlightNo.Substring(2, FlightNo.Length - 2);
                FlightNo = AirlineID + Int32.Parse(FlightNumber).ToString();
                DataTable dt = dal.GetFlightInfo(FlightDate, FlightNo);

                if (dt.Rows.Count == 0)
                    return null;



                if (dt.Rows.Count > 0)
                {
                    List<OScheduledFlight> FlightList = new List<OScheduledFlight>();
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        OScheduledFlight fs = new OScheduledFlight();
                        DateTime dtFlightDate = Common.General.ConvertToDateTime(FlightDate, 8);

                        string fly = Convert.ToString(dt.Rows[i]["FlyDay"]);
                        int dayWeek = (int)dtFlightDate.DayOfWeek;
                        if (dayWeek == 0)
                        {
                            if (fly.Substring(6, 1) != "1")
                                continue;
                        }
                        else
                        {
                            if (fly.Substring(dayWeek - 1, 1) == "0")
                                continue;
                        }

                        fs.AirlineID = Convert.ToString(dt.Rows[i]["AirlineID"]).Trim();
                        fs.FlightDate = dtFlightDate.ToString("yyyy/MM/dd");
                        fs.ArrivalAirportID = Convert.ToString(dt.Rows[i]["ArrivalAirportID"]).Trim();
                        fs.ArrivalTime = Convert.ToString(dt.Rows[i]["ArrivalTime"]).Trim();
                        fs.DepartureAirportID = Convert.ToString(dt.Rows[i]["DepartureAirportID"]).Trim();
                        fs.DepartureTime = Convert.ToString(dt.Rows[i]["DepartureTime"]).Trim();
                        fs.FlightNo = FlightNo;
                        if (Convert.ToString(dt.Rows[i]["FlightType"]).Trim() == "PA")
                            fs.FlightType = "到站";
                        else
                            fs.FlightType = "離站";

                        fs.Ternimal = Convert.ToString(dt.Rows[i]["Terminal"]).Trim();

                        FlightList.Add(fs);
                    }

                    if (FlightList.Count != 1)
                    {
                        APIFlightStruct afs = new APIFlightStruct();
                        afs.ft = FlightList[0].FlightType;
                        afs.fd = FlightList[0].FlightDate;
                        afs.aid = FlightList[0].AirlineID;
                        OAirline oa = GetAirlineInfo(afs.aid);
                        afs.alname = (oa == null) ? " " : oa.AirlineName;
                        afs.fno = FlightList[0].FlightNo;
                        afs.da = FlightList[0].DepartureAirportID;
                        OAirport oap = GetAirportInfo(afs.da);
                        afs.dpcity = (oap == null) ? " " : oap.CityName;
                        afs.dpname = (oap == null) ? " " : oap.AirportName;
                        afs.dt = FlightList[0].DepartureTime;
                        afs.aa = FlightList[0].ArrivalAirportID;
                        oap = GetAirportInfo(afs.aa);
                        afs.apcity = (oap == null) ? " " : oap.CityName;
                        afs.apname = (oap == null) ? " " : oap.AirportName;
                        afs.at = FlightList[0].ArrivalTime;
                        afs.ter = FlightList[0].Ternimal;

                        return afs;
                    }
                    else
                        return null;
                }
                else
                    return null;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// 回傳航班資料 (判斷出回國)
        /// </summary>
        /// <param name="ServiceType"></param>
        /// <param name="FlightNo"></param>
        /// <param name="FlightDate"></param>
        /// <returns></returns>
        public APIFlightStruct GetFlightInfo(string ServiceType, string FlightNo, string FlightDate)
        {
            DAL.DALFlight dal = new DAL.DALFlight(this._ConnectionString);
            try
            {
                //將CI0001轉為 CI1
                //取代掉 dash
                FlightNo = FlightNo.Replace("-", "");
                string AirlineID = FlightNo.Substring(0, 2);
                string FlightNumber = FlightNo.Substring(2, FlightNo.Length - 2);
                FlightNo = AirlineID + Int32.Parse(FlightNumber).ToString();
                DataTable dt = dal.GetFlightInfo(FlightDate, FlightNo);

                if (dt.Rows.Count == 0)
                    return null;



                if (dt.Rows.Count > 0)
                {
                    List<OScheduledFlight> FlightList = new List<OScheduledFlight>();
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        OScheduledFlight fs = new OScheduledFlight();
                        DateTime dtFlightDate = Common.General.ConvertToDateTime(FlightDate, 8);

                        string fly = Convert.ToString(dt.Rows[i]["FlyDay"]);
                        int dayWeek = (int)dtFlightDate.DayOfWeek;
                        if (dayWeek == 0)
                        {
                            if (fly.Substring(6, 1) != "1")
                                continue;
                        }
                        else
                        {
                            if (fly.Substring(dayWeek - 1, 1) == "0")
                                continue;
                        }

                        fs.AirlineID = Convert.ToString(dt.Rows[i]["AirlineID"]).Trim();
                        fs.FlightDate = dtFlightDate.ToString("yyyy/MM/dd");
                        fs.ArrivalAirportID = Convert.ToString(dt.Rows[i]["ArrivalAirportID"]).Trim();
                        fs.ArrivalTime = Convert.ToString(dt.Rows[i]["ArrivalTime"]).Trim();
                        fs.DepartureAirportID = Convert.ToString(dt.Rows[i]["DepartureAirportID"]).Trim();
                        fs.DepartureTime = Convert.ToString(dt.Rows[i]["DepartureTime"]).Trim();
                        fs.FlightNo = FlightNo;
                        if (Convert.ToString(dt.Rows[i]["FlightType"]).Trim() == "PA")
                            fs.FlightType = "到站";
                        else
                            fs.FlightType = "離站";

                        fs.Ternimal = Convert.ToString(dt.Rows[i]["Terminal"]).Trim();

                        if (fs.FlightType == "到站" && ServiceType == "O")
                            continue;
                        if (fs.FlightType == "離站" && ServiceType == "I")
                            continue;

                        FlightList.Add(fs);
                    }

                    if (FlightList.Count == 1)
                    {
                        APIFlightStruct afs = new APIFlightStruct();
                        afs.ft = FlightList[0].FlightType;
                        afs.fd = FlightList[0].FlightDate;
                        afs.aid = FlightList[0].AirlineID;
                        OAirline oa = GetAirlineInfo(afs.aid);
                        afs.alname = (oa == null) ? " " : oa.AirlineName;
                        afs.fno = FlightList[0].FlightNo;
                        afs.da = FlightList[0].DepartureAirportID;
                        OAirport oap = GetAirportInfo(afs.da);
                        afs.dpcity = (oap == null) ? " " : oap.CityName;
                        afs.dpname = (oap == null) ? " " : oap.AirportName;
                        afs.dt = FlightList[0].DepartureTime;
                        afs.aa = FlightList[0].ArrivalAirportID;
                        oap = GetAirportInfo(afs.aa);
                        afs.apcity = (oap == null) ? " " : oap.CityName;
                        afs.apname = (oap == null) ? " " : oap.AirportName;
                        afs.at = FlightList[0].ArrivalTime;
                        afs.ter = FlightList[0].Ternimal;

                        return afs;
                    }
                    else
                        return null;
                }
                else
                    return null;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        /// <summary>
        /// 回傳航班資料 (利用FlightStatus)
        /// </summary>
        /// <param name="ServiceType"></param>
        /// <param name="FlightNo"></param>
        /// <param name="FlightDate"></param>
        /// <returns></returns>
        public APIFlightStruct GetFlightInfoAPI(string appid, string appkey, string ServiceType, string FlightNo, string FlightDate)
        {
            DAL.DALFlight dal = new DAL.DALFlight(this._ConnectionString);
            FlightNo = FlightNo.Replace("-", "");
            string AirlineID = FlightNo.Substring(0, 2);
            string FlightNumber = FlightNo.Substring(2, FlightNo.Length - 2);
            FlightNo = Int32.Parse(FlightNumber).ToString();

            string APIURL = @"https://api.flightstats.com/flex/schedules/rest/v1/json/flight/{0}/{1}/{2}/{3}/{4}/{5}?appId={6}&appKey={7}";
            string FlightURL = string.Empty;
            string type;
            type = "departing";
            FlightURL = string.Format(APIURL, AirlineID, FlightNo, type, FlightDate.Substring(0, 4), FlightDate.Substring(4, 2), FlightDate.Substring(6, 2), appid, appkey);
            WebRequest request = WebRequest.Create(FlightURL);
            System.Net.ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            //X509Certificate2 cert1 = new X509Certificate2(System.AppDomain.CurrentDomain.BaseDirectory + @"SSL\sss.pfx", "qwerasdf!", X509KeyStorageFlags.PersistKeySet | X509KeyStorageFlags.MachineKeySet);
            //request.ClientCertificates.Add(cert1);
            //request.ImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;
            ////設定驗證回呼(總是同意)
            //ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(CheckValidationResult);

            //request.Credentials = CredentialCache.DefaultCredentials;
            WebResponse response = request.GetResponse();
            Console.WriteLine(((HttpWebResponse)response).StatusDescription);
            Stream dataStream = response.GetResponseStream();
            StreamReader reader = new StreamReader(dataStream);
            string responseFromServer = reader.ReadToEnd();
            reader.Close();
            response.Close();

            JObject jo = JsonConvert.DeserializeObject<JObject>(responseFromServer);

            string scheduledFlights = Convert.ToString(jo["scheduledFlights"]);
            if (scheduledFlights == "[]")
                return null;

            List<OFSScheduledFlight> ofs;
            try
            {
                ofs = JsonConvert.DeserializeObject<List<OFSScheduledFlight>>(scheduledFlights);
            }
            catch (Exception)
            {
                return null;
            }

            foreach (OFSScheduledFlight afs in ofs)
            {
                APIFlightStruct afs2 = new APIFlightStruct();
                if (ServiceType == "O")
                {
                    if (afs.departureAirportFsCode != "TPE")
                        continue;
                    afs2.ft = "離站";
                }
                else
                {
                    if (afs.arrivalAirportFsCode != "TPE")
                        continue;

                    afs2.ft = "到站";
                }

                afs2.fd = FlightDate;
                afs2.aid = AirlineID;
                OAirline oa = GetAirlineInfo(afs2.aid);
                afs2.alname = (oa == null) ? " " : oa.AirlineName;
                afs2.fno = AirlineID + FlightNo;
                afs2.da = afs.departureAirportFsCode;
                OAirport oap = GetAirportInfo(afs2.da);
                afs2.dpcity = (oap == null) ? " " : oap.CityName;
                afs2.dpname = (oap == null) ? " " : oap.AirportName;
                afs2.dt = afs.departureTime.ToString("HHmm");
                afs2.fulldt = afs.departureTime.ToString("yyyyMMddHHmm");
                afs2.aa = afs.arrivalAirportFsCode;
                oap = GetAirportInfo(afs2.aa);
                afs2.apcity = (oap == null) ? " " : oap.CityName;
                afs2.apname = (oap == null) ? " " : oap.AirportName;
                afs2.at = afs.arrivalTime.ToString("HHmm");
                afs2.fullat = afs.arrivalTime.ToString("yyyyMMddHHmm");
                if (ServiceType == "O")
                    afs2.ter = afs.departureTerminal;
                else
                    afs2.ter = afs.arrivalTerminal;

                return afs2;
            }

            return null;
        }
        private bool CheckValidationResult(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors errors) { return true; }

        public OAirport GetAirportInfo(string AirportCode)
        {
            DAL.DALFlight dal = new DAL.DALFlight(this._ConnectionString);
            OAirport airport = new OAirport();
            DataTable dt = dal.GetAirportInfo(AirportCode);
            if (dt.Rows.Count > 0)
            {
                airport.AirportCode = AirportCode;
                airport.AirportName = Convert.ToString(dt.Rows[0]["AirportName"]);
                airport.CityName = Convert.ToString(dt.Rows[0]["CityName"]);
                airport.CountryAbbrev = Convert.ToString(dt.Rows[0]["CountryAbbrev"]);
                airport.CountryName = Convert.ToString(dt.Rows[0]["CountryName"]);
            }
            else
                return null;

            return airport;
        }

        public OAirline GetAirlineInfo(string IATACode)
        {
            DAL.DALFlight dal = new DAL.DALFlight(this._ConnectionString);
            OAirline airline = new OAirline();
            DataTable dt = dal.GetAirlineInfo(IATACode);
            if (dt.Rows.Count > 0)
            {
                airline.IATA = IATACode;
                airline.ICAO = Convert.ToString(dt.Rows[0]["ICAO"]);
                airline.AirlineName = Convert.ToString(dt.Rows[0]["AirlineName"]);
                airline.CallSign = Convert.ToString(dt.Rows[0]["CallSign"]);
                airline.Country = Convert.ToString(dt.Rows[0]["Country"]);
            }
            else
                return null;

            return airline;
        }
    }


    /// <summary>
    /// 選車次的BAL
    /// </summary>
    public class BALPickCar : CallcarBAL
    {
        public BALPickCar(string ConnectionString) : base(ConnectionString) { }


        #region 整點制
        //public OPickCarTime GetPickCarTimeInfo(string ServiceType, string TakeDate, string FlightDate, string FlightTime, string SelectionType, int PassengerCnt, int BaggageCnt, double lat, double lng)
        //{
        //    DateTime dFlightTime;
        //    DateTime dTakeDate = Callcar.Common.General.ConvertToDateTime(TakeDate, 8);
        //    DateTime NowTime = DateTime.Now;

        //    if (dTakeDate.Date < NowTime.Date)
        //        throw new Exception("搭乘日期不可小於今日");

        //    if (SelectionType == "F")
        //    {
        //        dFlightTime = Callcar.Common.General.ConvertToDateTime(FlightDate + FlightTime, 12);
        //        if (FlightDate == null || FlightDate == "" || FlightTime == null || FlightTime == "")
        //            throw new Exception("搭乘日期時間不可為空");

        //        if (dFlightTime.Date < dTakeDate.Date && ServiceType == "O")
        //            throw new Exception("搭乘日期不可大於航班日期");
        //        else if (dFlightTime.Date > dTakeDate.Date && ServiceType == "I")
        //            throw new Exception("搭乘日期不可小於航班日期");
        //    }

        //    OPickCarTime Info = new OPickCarTime();
        //    List<string> QTime = GetTimeList(ServiceType, TakeDate, FlightDate, FlightTime, SelectionType);

        //    if (QTime == null)
        //        return Info;

        //    if (dTakeDate.Date <= NowTime.Date.AddDays(2).Date) //純撿車
        //    {
        //        foreach (string time in QTime)
        //        {
        //            List<OPickCarInfo> unit = GetCarsCanPick(TakeDate, time, ServiceType, PassengerCnt, BaggageCnt, lat, lng);
        //            if (unit != null)
        //                Info.PickCars.AddRange(unit);
        //        }
        //    }
        //    else //純預約
        //        Info.TimeRange = QTime;

        //    return Info;
        //}

        private List<string> GetTimeList(string ServiceType, string TakeDate, string FlightDate, string FlightTime, string SelectionType)
        {
            DateTime dFlightTime;
            List<string> List = new List<string>();
            int NowHour;

            DateTime NowTime = DateTime.Now;

            DateTime dTakeDate = Callcar.Common.General.ConvertToDateTime(TakeDate, 8);


            if (dTakeDate.Date == NowTime.Date)
                NowHour = DateTime.Now.Hour;
            else
                NowHour = 0;

            if (SelectionType == "F")
            {
                //用航班預約 取 出國航班前3小時 回國航班後90分鐘 之車次
                dFlightTime = Callcar.Common.General.ConvertToDateTime(FlightDate + FlightTime, 12);

                for (int i = NowHour; i < 24; i++)
                {
                    string Hour;
                    DateTime dTakeTime;

                    double TimeDiff;
                    if (ServiceType == "I")
                    {
                        Hour = i.ToString().PadLeft(2, '0') + FlightTime.Substring(2, 2);
                        dTakeTime = Callcar.Common.General.ConvertToDateTime(TakeDate + Hour, 12);
                        TimeDiff = new TimeSpan(dTakeTime.Ticks - dFlightTime.Ticks).TotalMinutes;

                        Hour = i.ToString().PadLeft(2, '0') + "00";
                        if (TimeDiff >= 90)
                            List.Add(Hour);
                    }
                    else if (ServiceType == "O")
                    {
                        Hour = i.ToString().PadLeft(2, '0') + "00";
                        dTakeTime = Callcar.Common.General.ConvertToDateTime(TakeDate + Hour, 12);
                        TimeDiff = new TimeSpan(dFlightTime.Ticks - dTakeTime.Ticks).TotalHours;
                        if (TimeDiff >= 3)
                            List.Add(Hour);
                    }
                    else
                        throw new Exception("ServiceType Data Error");
                }

            }
            else
            {
                //用航廈預約 直接讓User選時間
                if (Int32.Parse(TakeDate) > Int32.Parse(DateTime.Now.ToString("yyyyMMdd")))
                {
                    for (int i = 0; i < 24; i++)
                        List.Add(i.ToString().PadLeft(2, '0') + "00");
                }
                else
                {
                    if (NowHour + 3 >= 24 && ServiceType == "O")
                        return null;
                    else if (NowHour + 2 >= 24 && ServiceType == "I")
                        return null;

                    for (int i = NowHour + 1; i < 24; i++)
                    {
                        List.Add(i.ToString().PadLeft(2, '0') + "00");
                    }
                }

            }

            return List;
        }
        #endregion

        /// <summary>
        /// 本機端的揀車API
        /// </summary>
        /// <param name="TakeDate"></param>
        /// <param name="TimeSegment"></param>
        /// <param name="ServiceType"></param>
        /// <param name="pcnt"></param>
        /// <param name="bcnt"></param>
        /// <param name="lat"></param>
        /// <param name="lng"></param>
        /// <returns></returns>
        private List<OPickCarInfo> GetCarsCanPick(string TakeDate, string TimeSegment, string ServiceType, int pcnt, int bcnt, double lat, double lng)
        {
            DAL.DALPickCar dal = new DAL.DALPickCar(this._ConnectionString);
            List<OPickCarInfo> pci = dal.GetPickCarsList(TakeDate, TimeSegment, ServiceType);

            if (pci != null)
            {
                //比對資料是否選擇此車次
                foreach (OPickCarInfo car in pci)
                {
                    //Step1. 人數行李決定是顯示
                    int PCapacity = 0, BCapacity = 0;
                    if (car.CarType == "四人座")
                    {
                        PCapacity = 3;
                        BCapacity = 3;
                    }
                    else if (car.CarType == "七人座")
                    {
                        if (car.MaxFlag == "1")
                        {
                            PCapacity = 7;
                            BCapacity = 7;
                        }
                        else
                        {
                            PCapacity = 5;
                            BCapacity = 7;
                        }
                    }

                    if (car.RemainBCapacity + bcnt > BCapacity)
                        car.ShowFlag = false;
                    else if (car.RemainPCapacity + pcnt > PCapacity)
                        car.ShowFlag = false;
                    else
                    {
                        car.RemainPCapacity = PCapacity - car.RemainPCapacity;
                        car.RemainBCapacity = BCapacity - car.RemainBCapacity;
                    }

                    //Step2 地點距離決定是否顯示

                    OPickCarInfo.DispatchUnit last = car.Reservations.FindLast(x => x.SeqNo >= 0);
                    double distent = Callcar.Common.General.GetDistance(lat, lng, last.lat, last.lng);

                    //取最後一組客人 
                    //出國距離保守抓2KM                    
                    //回國距離抓5KM
                    if (ServiceType == "O")
                    {
                        if (distent > 2)
                            car.ShowFlag = false;
                    }
                    else
                    {
                        if (distent > 5)
                            car.ShowFlag = false;
                    }

                    //需要顯示的才計算時間
                    if (car.ShowFlag)
                    {
                        if (car.ServiceType == "I")
                        {
                            //回國一律統一上車時間
                            car.ShowServiceTime = last.ScheduleTime;
                        }
                        else
                        {
                            //回國抓最後一組預約時間十五分鐘後
                            car.ShowServiceTime = last.ScheduleTime.AddMinutes(15);
                        }
                    }
                }

                pci.RemoveAll(x => x.ShowFlag == false);
                //檢車開放24~48小時
                DateTime QueryTime = Common.General.ConvertToDateTime(TakeDate + TimeSegment, 12);
                if (DateTime.Now.AddHours(24) < QueryTime)
                    return pci;
                else
                    pci.RemoveAll(x => x.ShowFlag == false || x.ShowFlag == true);

                return pci;
            }
            else
            {
                return null;
            }
        }



        #region 15分鐘制
        public OPickCarTime GetPickCarTimeInfo2(string ServiceType, string TakeDate, string FlightDate, string FlightTime, string SelectionType, int PassengerCnt, int BaggageCnt, double Pickuplat, double Pickuplng, double Takeofflat, double Takeofflng, string PickAPIURL, int ServiceTimes)
        {
            DateTime dFlightTime;
            DateTime dTakeDate = Callcar.Common.General.ConvertToDateTime(TakeDate, 8);
            DateTime NowTime = DateTime.Now;

            if (dTakeDate.Date < NowTime.Date)
                throw new Exception("搭乘日期不可小於今日");

            if (SelectionType == "F")
            {
                dFlightTime = Callcar.Common.General.ConvertToDateTime(FlightDate + FlightTime, 12);
                if (FlightDate == null || FlightDate == "" || FlightTime == null || FlightTime == "")
                    throw new Exception("搭乘日期時間不可為空");

                if (dFlightTime.Date < dTakeDate.Date && ServiceType == "O")
                    throw new Exception("搭乘日期不可大於航班日期");
                else if (dFlightTime.Date > dTakeDate.Date && ServiceType == "I")
                    throw new Exception("搭乘日期不可小於航班日期");
            }


            OPickCarTime Info = new OPickCarTime();
            List<string> QTime = GetTimeList2(ServiceType, TakeDate, FlightDate, FlightTime, SelectionType);

            if (QTime == null || QTime.Count == 0)
                return Info;

            /* 呼叫司圖API
            if (dTakeDate.Date <= NowTime.Date.AddDays(2).Date) //純撿車
            {
                DateTime STime = Common.General.ConvertToDateTime(TakeDate + QTime[0], 12);
                DateTime ETime = Common.General.ConvertToDateTime(TakeDate + QTime[QTime.Count - 1], 12);

                List<OPickCarInfo> unit = GetCarsCanPick2(STime, ETime, ServiceType, PassengerCnt, BaggageCnt, Pickuplat, Pickuplng, Takeofflat, Takeofflng, PickAPIURL, ServiceTimes);

                if (unit != null)
                    Info.PickCars.AddRange(unit);
            }
            else //純預約
                Info.TimeRange = QTime;
             */

            DAL.DALDispatch dald = new DAL.DALDispatch(this._ConnectionString);

            if (dald.isMatchDateExist(TakeDate)) //純撿車
            {
                foreach (string time in QTime)
                {

                    List<OPickCarInfo> unit = new List<OPickCarInfo>();
                    if (ServiceType == "O")
                        unit = GetCarsCanPick(TakeDate, time, ServiceType, PassengerCnt, BaggageCnt, Pickuplat, Pickuplng);
                    else
                        unit = GetCarsCanPick(TakeDate, time, ServiceType, PassengerCnt, BaggageCnt, Takeofflat, Takeofflng);

                    if (unit != null)
                        Info.PickCars.AddRange(unit);
                }
            }
            else //純預約
                Info.TimeRange = QTime;

            return Info;
        }

        /// <summary>
        /// 回傳15分鐘制的 可預約時段區間
        /// </summary>
        /// <param name="ServiceType"></param>
        /// <param name="TakeDate"></param>
        /// <param name="FlightDate"></param>
        /// <param name="FlightTime"></param>
        /// <param name="SelectionType"></param>
        /// <returns></returns>
        private List<string> GetTimeList2(string ServiceType, string TakeDate, string FlightDate, string FlightTime, string SelectionType)
        {
            DateTime dFlightTime;
            List<string> List = new List<string>();

            DateTime dTakeTimeBegin, dTakeTimeEnd, dTakeTime;
            string tmpMin;
            int mm;

            if (SelectionType == "F")
            {
                //用航班預約 取 出國航班前3小時 回國航班後90分鐘 之車次
                //用航班預約 取 出國航班前2.5 hr 回國航班後1hr之車次 2017.11.14
                //回國航班改為1.5hr 2017.12.12
                dFlightTime = Callcar.Common.General.ConvertToDateTime(FlightDate + FlightTime, 12);
                if (ServiceType == "I")
                {
                    dTakeTimeBegin = dFlightTime.AddMinutes(75);
                    dTakeTimeEnd = dFlightTime.AddHours(3);

                    if (dTakeTimeBegin.ToString("yyyyMMdd") == TakeDate)
                    {
                        if (dTakeTimeEnd.ToString("yyyyMMdd") != TakeDate)
                            dTakeTimeEnd = Common.General.ConvertToDateTime(TakeDate + "2359", 12);
                    }
                    else
                    {
                        if (dTakeTimeEnd.ToString("yyyyMMdd") != TakeDate)
                            return List;
                        else
                            dTakeTimeBegin = Common.General.ConvertToDateTime(TakeDate + "0000", 12);
                    }
                }
                else if (ServiceType == "O")
                {
                    dTakeTimeEnd = dFlightTime.AddMinutes(-150);
                    dTakeTimeBegin = dFlightTime.AddHours(-6);

                    if (dTakeTimeBegin.ToString("yyyyMMdd") == TakeDate)
                    {
                        if (dTakeTimeEnd.ToString("yyyyMMdd") != TakeDate)
                            dTakeTimeEnd = Common.General.ConvertToDateTime(TakeDate + "2359", 12);
                    }
                    else
                    {
                        if (dTakeTimeEnd.ToString("yyyyMMdd") != TakeDate)
                            return List;
                        else
                            dTakeTimeBegin = Common.General.ConvertToDateTime(TakeDate + "0000", 12);
                    }
                }
                else
                    throw new Exception("ServiceType Data Error");


                mm = Convert.ToInt32(dTakeTimeBegin.ToString("mm"));
                if (mm > 45)
                    dTakeTimeBegin = Common.General.ConvertToDateTime(dTakeTimeBegin.AddHours(1).ToString("yyyyMMddHH") + "00", 12);
                else
                {
                    if (mm == 0)
                        tmpMin = "00";
                    else if (mm <= 15 && mm > 0)
                        tmpMin = "15";
                    else if (mm <= 30 && mm > 15)
                        tmpMin = "30";
                    else
                        tmpMin = "45";

                    dTakeTimeBegin = Common.General.ConvertToDateTime(dTakeTimeBegin.ToString("yyyyMMddHH") + tmpMin, 12);
                }

                dTakeTime = dTakeTimeBegin;
                while (dTakeTime <= dTakeTimeEnd)
                {
                    List.Add(dTakeTime.ToString("HHmm"));
                    dTakeTime = dTakeTime.AddMinutes(15);
                }

            }
            else
            {
                //用航廈預約 直接讓User選時間
                if (Int32.Parse(TakeDate) > Int32.Parse(DateTime.Now.ToString("yyyyMMdd")))
                {
                    //預約未來日子
                    dTakeTimeBegin = Common.General.ConvertToDateTime(TakeDate + "0000", 12);
                    dTakeTimeEnd = Common.General.ConvertToDateTime(TakeDate + "2359", 12);
                }
                else
                {
                    //今日
                    if (ServiceType == "O")
                    {
                        dTakeTimeBegin = DateTime.Now.AddHours(2);
                        dTakeTimeEnd = Common.General.ConvertToDateTime(TakeDate + "2359", 12);
                    }
                    else
                    {
                        dTakeTimeBegin = DateTime.Now.AddMinutes(75);
                        dTakeTimeEnd = Common.General.ConvertToDateTime(TakeDate + "2359", 12);
                    }

                    if (dTakeTimeBegin.ToString("yyyyMMdd") != TakeDate)
                        return List;
                    else
                    {
                        mm = Convert.ToInt32(dTakeTimeBegin.ToString("mm"));
                        if (mm >= 45)
                            dTakeTimeBegin = Common.General.ConvertToDateTime(dTakeTimeBegin.AddHours(1).ToString("yyyyMMddHH") + "00", 12);
                        else
                        {
                            if (mm < 15 && mm >= 0)
                                tmpMin = "15";
                            else if (mm < 30 && mm >= 15)
                                tmpMin = "30";
                            else
                                tmpMin = "45";

                            dTakeTimeBegin = Common.General.ConvertToDateTime(dTakeTimeBegin.ToString("yyyyMMddHH") + tmpMin, 12);
                        }
                    }
                }
                dTakeTime = dTakeTimeBegin;
                while (dTakeTime <= dTakeTimeEnd)
                {
                    List.Add(dTakeTime.ToString("HHmm"));
                    dTakeTime = dTakeTime.AddMinutes(15);
                }
            }

            return List;
        }


        /// <summary>
        /// 呼叫司圖揀車API (尚未使用)
        /// </summary>
        /// <param name="sTime"></param>
        /// <param name="eTime"></param>
        /// <param name="ServiceType"></param>
        /// <param name="pcnt"></param>
        /// <param name="bcnt"></param>
        /// <param name="Pickuplat"></param>
        /// <param name="Pickuplng"></param>
        /// <param name="Takeofflat"></param>
        /// <param name="Takeofflng"></param>
        /// <param name="APIURL"></param>
        /// <param name="ServiceTimes"></param>
        /// <returns></returns>
        private List<OPickCarInfo> GetCarsCanPick2(DateTime sTime, DateTime eTime, string ServiceType, int pcnt, int bcnt, double Pickuplat, double Pickuplng, double Takeofflat, double Takeofflng, string APIURL, int ServiceTimes)
        {
            DAL.DALPickCar dal = new DAL.DALPickCar(this._ConnectionString);
            List<OPickCarInfo> pci = dal.GetPickCarsListByTime(sTime, eTime, ServiceType);
            APIPickCarSource pcs = new APIPickCarSource(ServiceTimes);
            pcs.oBCnt = bcnt;
            pcs.oPCnt = pcnt;
            pcs.PickupLat = Pickuplat;
            pcs.PickupLng = Pickuplng;
            pcs.TakeoffLat = Takeofflat;
            pcs.TakeoffLng = Takeofflng;
            pcs.type = ServiceType;
            pcs.totalServiceTime = ServiceTimes;

            if (pci != null)
            {
                //比對資料是否選擇此車次
                foreach (OPickCarInfo car in pci)
                {
                    //Step1. 人數行李決定是顯示
                    int PCapacity = 0, BCapacity = 0;
                    if (car.CarType == "四人座")
                    {
                        PCapacity = 3;
                        BCapacity = 3;
                    }
                    else if (car.CarType == "七人座")
                    {
                        if (car.MaxFlag == "1")
                        {
                            PCapacity = 7;
                            BCapacity = 7;
                        }
                        else
                        {
                            PCapacity = 5;
                            BCapacity = 7;
                        }
                    }

                    if (car.RemainBCapacity + bcnt > BCapacity)
                        car.ShowFlag = false;
                    else if (car.RemainPCapacity + pcnt > PCapacity)
                        car.ShowFlag = false;
                    else
                    {
                        car.RemainPCapacity = PCapacity - car.RemainPCapacity;
                        car.RemainBCapacity = BCapacity - car.RemainBCapacity;
                    }

                    if (car.ShowFlag)
                    {
                        APIPickCarSource.PickCars c = new APIPickCarSource.PickCars();
                        c.dno = car.DispatchNo;
                        c.scnt = car.StopCnt;
                        c.pcnt = car.RemainPCapacity;
                        c.bcnt = car.RemainBCapacity;
                        foreach (OPickCarInfo.DispatchUnit u in car.Reservations)
                        {
                            APIPickCarSource.PickCarServices s = new APIPickCarSource.PickCarServices();
                            if (ServiceType == "O")
                            {
                                s.PickupLat = u.lat;
                                s.PickupLng = u.lng;
                                s.time = u.ScheduleTime.ToString("yyyy-MM-dd HH:mm");
                            }
                            else
                            {
                                s.TakeoffLat = u.lat;
                                s.TakeoffLng = u.lng;
                                s.time = u.ScheduleTime.ToString("yyyy-MM-dd HH:mm");
                            }
                        }
                        pcs.cars.Add(c);
                    }
                }

                string Json = JsonConvert.SerializeObject(pcs);

                //呼叫API
                HttpWebRequest request = HttpWebRequest.Create(APIURL) as HttpWebRequest;
                string result = null;
                request.Method = "POST";    // 方法
                request.KeepAlive = true; //是否保持連線
                request.ContentType = "application/json";

                byte[] bs = Encoding.ASCII.GetBytes(Json);

                using (Stream Stream = request.GetRequestStream())
                {
                    Stream.Write(bs, 0, bs.Length);
                }

                using (WebResponse response = request.GetResponse())
                {
                    StreamReader sr = new StreamReader(response.GetResponseStream());
                    result = sr.ReadToEnd();
                    sr.Close();
                }

                return pci;
            }
            else
            {
                return null;
            }
        }

        #endregion
    }


    public class BALDispatch : CallcarBAL
    {
        public BALDispatch(string ConnectionString) : base(ConnectionString) { }

        public List<ODispatchSheet> GetDispatchSheetByDriver(int DriverID)
        {

            DateTime StartTime = DateTime.Now.AddDays(-1);
            string StartDate = StartTime.ToString("yyyyMMdd");
            // string Stime = StartTime.ToString("HH") + "00";
            DateTime EndTime = DateTime.Now.AddDays(2);
            string EndDate = EndTime.ToString("yyyyMMdd");
            //string Etime = EndTime.ToString("HH") + "00";

            DAL.DALDispatch dal = new DAL.DALDispatch(this._ConnectionString);
            try
            {
                List<ODispatchSheet> ListD = dal.GetUnServeDispatchSheets(DriverID, StartDate, EndDate);//, Stime, Etime);
                if (ListD != null)
                    return ListD;
                else
                    return null;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<ODispatchSheet> GetServedDispatchSheetByDriver(int DriverID)
        {

            DateTime StartTime = DateTime.Now.AddHours(-24);
            string StartDate = StartTime.ToString("yyyyMMdd");
            //      string Stime = StartTime.ToString("HH") + "00";
            DateTime EndTime = DateTime.Now;
            string EndDate = EndTime.ToString("yyyyMMdd");
            //   string Etime = EndTime.ToString("HH") + "00";

            DAL.DALDispatch dal = new DAL.DALDispatch(this._ConnectionString);
            try
            {
                List<ODispatchSheet> ListD = dal.GetServedDispatchSheets(DriverID, StartDate, EndDate);//, Stime, Etime);
                if (ListD != null)
                    return ListD;
                else
                    return null;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<ODispatchSheet> GetDispatchSheetByDate(string data)
        {
            string StartDate = data.Split(',')[0];
            string EndDate = data.Split(',')[1];

            DAL.DALDispatch dal = new DAL.DALDispatch(this._ConnectionString);
            try
            {
                List<ODispatchSheet> ListD = dal.GetDispatchSheets(StartDate, EndDate);
                if (ListD != null)
                    return ListD;
                else
                    return null;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// 依單號取得完整派車單
        /// </summary>
        /// <param name="Dno">派車單號</param>
        /// <returns></returns>
        public ODispatchSheet GetDispatchSheetByNo(string Dno)
        {
            BALGeneral bal = new BAL.BALGeneral(this._ConnectionString);
            BALEmployee bale = new BALEmployee(this._ConnectionString);
            DAL.DALDispatch dal = new DAL.DALDispatch(this._ConnectionString);
            try
            {
                ODispatchSheet dispatch = dal.GetDispatchSheets(Dno);
                if (dispatch != null)
                {
                    if (dispatch.FleetID > 0)
                        dispatch.FleetName = bal.GetFleet(dispatch.FleetID).FleetName;
                    else
                        dispatch.FleetName = "";
                    if (dispatch.DriverID > 0)
                        dispatch.DriverName = bale.GetEmployee(dispatch.DriverID).Name;
                    else
                        dispatch.DriverName = "";

                    return dispatch;
                }
                else
                    return null;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public bool UpdateServiceStatusByDriver(ODriverServiceRecord odr)
        {

            try
            {
                DAL.DALDispatch dal = new DAL.DALDispatch(this._ConnectionString);

                if (dal.UpdateReservationServiceStatus(odr))
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public OMachSourceData GetMatchSourceByDate(string date)
        {
            DAL.DALReservation dalr = new DAL.DALReservation(this._ConnectionString);
            OMachSourceData Match = new OMachSourceData();
            DataTable dt = new DataTable();

            try
            {
                dt = dalr.GetReservationForMatch(date);
            }
            catch (Exception e)
            {
                throw e;
            }

            try
            {
                Match.MatchDate = date;

                if (dt.Rows.Count > 0)
                {

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        OMatchRData m = new OMatchRData();
                        m.ReservationNo = Convert.ToString(dt.Rows[i]["ReservationNo"]);
                        m.ServiceType = Convert.ToString(dt.Rows[i]["ServiceType"]);
                        m.TimeSegment = Convert.ToString(dt.Rows[i]["TimeSegment"]);
                        m.BaggageCnt = Convert.ToInt32(dt.Rows[i]["BaggageCnt"]);
                        m.PassengerCnt = Convert.ToInt32(dt.Rows[i]["PassengerCnt"]);
                        m.PickupLat = Convert.ToDouble(dt.Rows[i]["plng"]);
                        m.PickupLng = Convert.ToDouble(dt.Rows[i]["plat"]);
                        m.TakeoffLat = Convert.ToDouble(dt.Rows[i]["tlat"]);
                        m.TakeoffLng = Convert.ToDouble(dt.Rows[i]["tlng"]);
                        m.MaxFlag = Convert.ToString(dt.Rows[i]["MaxFlag"]);

                        if (m.ServiceType == "O")
                        {
                            if (m.TimeSegment.Substring(2, 2) == "15" || m.TimeSegment.Substring(2, 2) == "30" || m.TimeSegment.Substring(2, 2) == "00")
                                m.MatchRegion = m.TimeSegment.Substring(0, 2) + "00";
                            else if (m.TimeSegment.Substring(2, 2) == "45")
                            {
                                int hour;
                                hour = Convert.ToInt32(m.TimeSegment.Substring(0, 2)) + 1;
                                if (hour == 24)
                                    m.MatchRegion = m.TimeSegment.Substring(0, 2) + "00";
                                else
                                    m.MatchRegion = hour.ToString().PadLeft(2, '0') + "00";
                            }
                        }
                        else
                        {
                            if (m.TimeSegment.Substring(2, 2) == "15" || m.TimeSegment.Substring(2, 2) == "00")
                                m.MatchRegion = m.TimeSegment.Substring(0, 2) + "00";
                            else if (m.TimeSegment.Substring(2, 2) == "45" || m.TimeSegment.Substring(2, 2) == "30")
                            {
                                int hour;
                                hour = Convert.ToInt32(m.TimeSegment.Substring(0, 2)) + 1;
                                if (hour == 24)
                                    m.MatchRegion = m.TimeSegment.Substring(0, 2) + "00";
                                else
                                    m.MatchRegion = hour.ToString().PadLeft(2, '0') + "00";
                            }
                        }

                        Match.ReservationList.Add(m);
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            //DAL.DALDispatch dald = new DAL.DALDispatch(this._ConnectionString);

            ////暫時不取排班資料
            //List<OScheduleData> SList = new List<OScheduleData>();// dald.GetScheduleByDate(date);

            //foreach (OScheduleData s in SList)
            //{
            //    OMatchCData c = new OMatchCData();
            //    c.CarType = s.CarType;
            //    c.ScheduleID = s.ScheduleID;
            //    c.ShiftType = s.ShiftType;
            //    if (s.CarType == "四人座")
            //    {
            //        c.NCapacity = 3;
            //        c.MCapacity = 4;
            //    }
            //    else if (s.CarType == "七人座")
            //    {
            //        c.NCapacity = 5;
            //        c.MCapacity = 7;
            //    }

            //    Match.CarList.Add(c);
            //}

            return Match;
        }

        public bool SaveMatchRecord(string date, string Status, int ID, string MatchJson, string ErrMsg)
        {
            DAL.DALDispatch dald = new DAL.DALDispatch(this._ConnectionString);
            try
            {
                return dald.SaveMatchError(date, Status, ID, MatchJson, ErrMsg);
            }
            catch (Exception ex)

            { throw new Exception(ex.Message); }
        }

        /// <summary>
        /// 批次新增媒合結果
        /// </summary>
        /// <param name="data">媒合結果的JSON</param>
        /// <returns></returns>
        public int BatchAddDispatchSheet(string DispatchDate, string data)
        {
            MatchResultJsonStruct MatchResult = JsonConvert.DeserializeObject<MatchResultJsonStruct>(data);
            int sheetCnt = 0;


            DAL.DALDispatch dald = new DAL.DALDispatch(this._ConnectionString);
            DAL.DALEmployee dale = new DAL.DALEmployee(this._ConnectionString);
            DAL.DALReservation dalr = new DAL.DALReservation(this._ConnectionString);
            DAL.DALSerialNum dals = new DAL.DALSerialNum(this._ConnectionString);
            //將Json資料設定至派遣Object中
            foreach (DispatchInfo info in MatchResult.DispatchInfo)
            {
                ODispatchSheet od = new ODispatchSheet();

                string DispatchNo = string.Empty;
                try
                {
                    OSerialNum serial = dals.GetSerialNumObject("Dispatch");
                    DispatchNo = DateTime.Now.ToString(serial.Prefix) + dals.GetSeqNoString(serial);
                }
                catch (Exception ex)
                {
                    throw new Exception("取派車單號失敗" + ex.Message + Environment.NewLine + JsonConvert.SerializeObject(info));
                }

                sheetCnt++;
                od.DispatchNo = DispatchNo;

                int PCnt = 0, BCnt = 0;
                string TakeDate = string.Empty;
                List<DateTime> ServiceTime = new List<DateTime>();

                foreach (ServiceList s in info.Detail.ServiceList)
                {
                    ODpServiceUnit su = new ODpServiceUnit();
                    OReservation r;
                    try
                    {
                        r = dalr.GetReservation(s.ReservationNo);
                    }
                    catch (Exception ex)
                    {
                        //寫入媒合紀錄檔
                        JObject jo = JsonConvert.DeserializeObject<JObject>(data);
                        JArray arryDispatch = (JArray)jo["DispatchInfo"];
                        for (int i = 0; i < arryDispatch.Count; i++)
                        {
                            if (Convert.ToInt32(arryDispatch[i]["Index"].ToString()) == info.Index)
                            {
                                SaveMatchRecord(od.TakeDate, "E", info.Index, arryDispatch[i]["Detail"].ToString(), ex.Message);
                                break;
                            }
                        }
                        continue;
                    }

                    su.DispatchNo = DispatchNo;
                    su.ScheduleTime = Convert.ToDateTime(s.ServiceTime);
                    ServiceTime.Add(su.ScheduleTime);
                    ServiceTime.Sort((a, b) => a.CompareTo(b));
                    su.ReservationNo = s.ReservationNo;
                    su.ServiceRemark = "0";
                    if (r.ServiceType == "I")
                        su.ServiceGPS = r.TakeoffGPS;
                    else
                        su.ServiceGPS = r.PickupGPS;
                    PCnt += r.PassengerCnt;
                    BCnt += r.BaggageCnt;
                    TakeDate = r.ServeDate;

                    od.ServiceList.Add(su);
                }

                od.BaggageCnt = BCnt;
                od.PassengerCnt = PCnt;


                if (od.PassengerCnt > 3)
                    od.CarType = "七人座";
                else
                    od.CarType = "四人座";
                //先固定寫測試資料
                od.DriverID = 0;
                od.DriverName = "";
                od.FleetID = 0;
                od.CarNo = "";
                // }
                od.TimeSegment = info.Detail.TimeSegment;


                od.FisrtServiceTime = ServiceTime[0].ToString("HHmm");
                od.ServiceCnt = info.Detail.ServiceCnt;
                od.ServiceType = info.Detail.ServiceType;
                od.TakeDate = TakeDate;// info.Detail.ServiceDate.Replace("/", "");


                //儲存媒合資料
                try
                {
                    dald.addDispatchSheet(od);
                }
                catch (Exception ex)
                {
                    //寫入媒合紀錄檔
                    JObject jo = JsonConvert.DeserializeObject<JObject>(data);
                    JArray arryDispatch = (JArray)jo["DispatchInfo"];
                    for (int i = 0; i < arryDispatch.Count; i++)
                    {
                        if (Convert.ToInt32(arryDispatch[i]["Index"].ToString()) == info.Index)
                        {
                            SaveMatchRecord(od.TakeDate, "E", info.Index, arryDispatch[i]["Detail"].ToString(), ex.Message);
                            break;
                        }
                    }
                    continue;
                }

            }

            return sheetCnt;
        }


        public bool isCarFull(string Dno)
        {
            //撿車的 先撿查出車狀態
            DAL.DALDispatch dald = new DAL.DALDispatch(this._ConnectionString);
            ODispatchSheet od = dald.GetDispatchSheetBrief(Dno);
            if (od == null)
            {
                throw new Exception("派車單號異常");
            }
            else
            {
                if (od.ServiceCnt > 2)
                    return true;
                else
                    return false;
            }
        }

        public List<ODispatchSheet> GetDispatchByConditions(string data)
        {
            try
            {
                QDispatchCondStruct qs = JsonConvert.DeserializeObject<QDispatchCondStruct>(data);
                if (string.IsNullOrWhiteSpace(qs.QSDate) || string.IsNullOrWhiteSpace(qs.QEDate))
                {
                    throw new Exception("日期為必須條件");
                }
                if (qs.take < 0 || qs.skip < 0)
                {
                    throw new Exception("查詢範圍異常");
                }
                DAL.DALDispatch dald = new DAL.DALDispatch(this._ConnectionString);
                return dald.GetDispatchMultipleCondition(qs, qs.take, qs.skip);

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public bool UpdDispatchCarDriver(string data)
        {
            //data
            //dno,old fleet id,old car no,old employee id,new  fleet id,new car no,new employee id
            string Dno, NewCarNo, NewCarType;
            int NewEmpID, NewFleetID;
            string[] parm = data.Split(',');
            try
            {
                Dno = parm[0];
                //OldFleetID = parm[1];
                //OldCarNo = parm[2];
                //OldEmpID = Int32.Parse(parm[3]);
                NewFleetID = Int32.Parse(parm[1]);
                NewCarNo = parm[2];
                NewEmpID = Int32.Parse(parm[3]);
                NewCarType = parm[4];


            }
            catch (Exception)
            {
                throw new Exception("Data Error");
            }

            DAL.DALDispatch dald = new DAL.DALDispatch(this._ConnectionString);
            try
            {
                return dald.UpdDispatchCarDriver(Dno, NewFleetID, NewCarNo, NewEmpID, NewCarType);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DispatchLocationsStruct GetDispatchLocationByNo(string Dno)
        {
            try
            {
                DAL.DALDispatch dald = new DAL.DALDispatch(this._ConnectionString);

                ODispatchSheet ods = dald.GetDispatchSheetBrief(Dno);
                if (ods == null)
                    return null;

                DispatchLocationsStruct dl = new DispatchLocationsStruct();
                dl.dno = Dno;
                dl.cnt = ods.ServiceCnt;
                dl.Service = dald.GetDispatchService(Dno);

                return dl;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


    }
    /// <summary>
    /// 其他BAL
    /// </summary>
    public class BALGeneral : CallcarBAL
    {
        public BALGeneral(string ConnectionString) : base(ConnectionString) { }


        public List<OBullletin> GetShowBulletin(string Target, int FleetNo, int EmployeeID)
        {
            DAL.DALGeneral dal = new DAL.DALGeneral(this._ConnectionString);
            DataTable dt = dal.GetShowBulletin(Target, FleetNo, EmployeeID);
            List<OBullletin> ob = new List<OBullletin>();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                OBullletin b = new OBullletin();
                b.SeqNo = Convert.ToInt32(dt.Rows[i]["SeqNo"]);
                b.Target = Convert.ToString(dt.Rows[i]["TargetType"]);
                b.FleetNo = Convert.ToInt32(dt.Rows[i]["FleetNo"]);
                b.EmployeeID = Convert.ToInt32(dt.Rows[i]["EmployeeID"]);
                b.Display = Convert.ToString(dt.Rows[i]["DisplayFlag"]);
                b.Content = Convert.ToString(dt.Rows[i]["Content"]);
                b.CreateTime = Convert.ToDateTime(dt.Rows[i]["CreateTime"]);
                ob.Add(b);
            }

            return ob;
        }
        //public string GetAppVersion(string AppName, string OS)
        //{
        //    DAL.DALGeneral dal = new DAL.DALGeneral(this._ConnectionString);
        //    dmAppSetting app = dal.GetApp(AppName, OS);
        //    if (app != null)
        //        return app.LastVersion;
        //    else
        //        return "";
        //}

        public OHoliday GetHoliday(DateTime inDate)
        {
            try
            {
                DAL.DALGeneral dal = new DAL.DALGeneral(this._ConnectionString);
                return dal.GetHoliday(inDate.ToString("yyyyMMdd"));
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        ///  計算價格
        /// </summary>
        /// <param name="OrderDate">訂單日期</param>
        /// <param name="TakeDate">乘車日期</param>
        /// <param name="TimeSegment">乘車時段</param>
        /// <param name="Pcnt">訂單人數</param>
        /// <param name="Bcnt">訂單行李數</param>
        /// <param name="Coupon">優惠內容</param>
        /// <param name="CarPcnt">目前派車單人數</param>
        /// <returns></returns>
        public OPrice CalculateTrailFee(string OrderDate, string TakeDate, string TimeSegment, int Pcnt, int Bcnt, string Coupon, int CarPcnt, int addBag, bool newOrder, string ServeTime, int UserID)
        {
            return CalculatePrice(OrderDate, TakeDate, TimeSegment, Pcnt, Bcnt, Coupon, CarPcnt, addBag, newOrder, ServeTime, UserID);
        }

        /// <summary>
        /// 計算價格
        /// </summary>
        /// <param name="OrderDate">訂單日期</param>
        /// <param name="TakeDate">乘車日期</param>
        /// <param name="TimeSegment">乘車時段</param>
        /// <param name="Pcnt">訂單人數</param>
        /// <param name="Bcnt">訂單行李數</param>
        /// <param name="Coupon">優惠內容</param>
        /// <param name="Dno">派車單號(撿車時使用)</param>
        /// <returns></returns>
        public OPrice CalculateTrailFee(string OrderDate, string TakeDate, string TimeSegment, int Pcnt, int Bcnt, string Coupon, string Dno, int addBag, bool newOrder, string ServeTime, int UserID)
        {

            if (Dno != null && Dno != "null" && Dno != "")
            {
                DAL.DALDispatch dald = new DAL.DALDispatch(this._ConnectionString);
                ODispatchSheet dispatch = dald.GetDispatchSheetBrief(Dno);
                if (dispatch == null)
                    throw new Exception("派車單號錯誤");

                return CalculatePrice(OrderDate, TakeDate, TimeSegment, Pcnt, Bcnt, Coupon, dispatch.PassengerCnt, addBag, newOrder, ServeTime, UserID);
            }
            else
                return CalculatePrice(OrderDate, TakeDate, TimeSegment, Pcnt, Bcnt, Coupon, 0, addBag, newOrder, ServeTime, UserID);
        }

        private OPrice CalculatePrice(string OrderDate, string TakeDate, string TimeSegment, int Pcnt, int Bcnt, string Coupon, int CarPcnt, int addBag, bool newOrder, string ServeTime, int UserID)
        {
            OPrice op = new OPrice();
            try
            {
                int CarFee = 0, NightFee = 0, BagFee = 0, PromotFee = 0;
                List<OFare> FareTable = new List<OFare>();
                DAL.DALGeneral dal = new DAL.DALGeneral(this._ConnectionString);

                FareTable = dal.GetFareTable(OrderDate);
                if (FareTable.Count > 0)
                {
                    int index = -1;
                    //取得車資 ChargeType = 1
                    index = FareTable.FindIndex(x => x.ChargeType == "1" && x.MinQty == (Pcnt + CarPcnt));
                    if (index == -1)
                        throw new Exception("無此服務費率表");
                    else
                        CarFee = FareTable[index].Amount * Pcnt;

                    op.CarFee = CarFee;

                    //取得夜加  ChargeType = 2                    
                    if (ServeTime != null && ServeTime != "")
                    {
                        if ((Int32.Parse(TimeSegment) >= 2200 || Int32.Parse(TimeSegment) < 600) && (Int32.Parse(ServeTime) >= 2200 || Int32.Parse(ServeTime) < 600))
                        {
                            index = FareTable.FindIndex(x => x.ChargeType == "2");
                            if (index == -1)
                                throw new Exception("無此服務費率表");
                            else
                                NightFee = FareTable[index].Amount * Pcnt;
                        }
                        else
                            NightFee = 0;
                    }
                    else
                    {
                        if (Int32.Parse(TimeSegment) >= 2200 || Int32.Parse(TimeSegment) < 600)
                        {
                            index = FareTable.FindIndex(x => x.ChargeType == "2");
                            if (index == -1)
                                throw new Exception("無此服務費率表");
                            else
                                NightFee = FareTable[index].Amount * Pcnt;
                        }
                        else
                            NightFee = 0;
                    }
                    //if (UserID == 1496734211)
                        op.NightFee = NightFee;
                    //else
                    //{
                    //op.NightFee = 0;
                    //NightFee = 0;
                    //}

                    //行李加費  ChargeType = 3
                    if (Bcnt - Pcnt > 0)
                    {
                        int BagCnt = Bcnt - Pcnt;
                        index = FareTable.FindIndex(x => x.ChargeType == "3");
                        BagFee = FareTable[index].Amount * BagCnt;
                    }
                    else
                        BagFee = 0;

                    if (addBag > 0)
                    {
                        index = FareTable.FindIndex(x => x.ChargeType == "3");
                        BagFee += addBag * FareTable[index].Amount;
                    }

                    op.BaggageFee = BagFee;

                    //驗證優惠碼
                    OCoupon coupon = this.GetCoupon(Coupon, OrderDate, TakeDate, newOrder);
                    if (coupon != null)
                    {
                        if (coupon.CarFeeSub != -1)
                        {
                            if (coupon.CarFeeSub > 0)
                            {
                                //林氏璧專案 以人數折扣
                                if (coupon.CouponCode == "LINSHIBI")
                                    PromotFee += coupon.CarFeeSub * Pcnt;
                                else
                                    PromotFee += coupon.CarFeeSub;
                            }
                        }
                        //使用固定車資時 車資以單人計算 併計算優惠金額
                        if (coupon.FixCarFee != -1)
                        {


                            if (coupon.FixCarFee == 0)
                                PromotFee += CarFee;
                            else if (coupon.FixCarFee > 0)
                            {
                                int CarFeeSub = coupon.FixCarFee * Pcnt;
                                int temp = CarFee - CarFeeSub;
                                PromotFee += temp;
                            }
                        }

                        if (coupon.NightFeeSub != -1)
                        {
                            if (coupon.NightFeeSub == 0)
                                PromotFee += NightFee;
                            else if (coupon.NightFeeSub > 0)
                            {
                                if (NightFee <= coupon.NightFeeSub)
                                    PromotFee += NightFee;
                                else
                                    PromotFee += coupon.NightFeeSub;
                            }
                        }
                    }
                    op.PromoteAmt = PromotFee;

                    //計算廠商月結金額

                    if (UserID > 0)
                    {
                        if (UserID == 1522464810)//旅途中
                        {
                            if (Int32.Parse(OrderDate) >= 20180401 && Int32.Parse(OrderDate) <= 20180630)
                                op.CollectAmt = 279 * Pcnt;
                            else if (Int32.Parse(OrderDate) >= 20180701 && Int32.Parse(OrderDate) <= 20180930)
                                op.CollectAmt = 298 * Pcnt;
                            else if (Int32.Parse(OrderDate) >= 20181001 && Int32.Parse(OrderDate) <= 20181231)
                                op.CollectAmt = 330 * Pcnt;
                            else
                                op.CollectAmt = 389 * Pcnt;

                        }
                    }


                    op.TotalAmt = op.CarFee + op.NightFee + op.BaggageFee + op.OtherFee + op.OtherSaleFee; //原始總金額 = 各項費用加總
                    op.Price = op.TotalAmt - op.PromoteAmt; //自付金額 = 原始總金額 - 優惠金額
                    op.OfficialTotalAmt = op.CollectAmt + op.Price; //實收總金額 = 自付金額+月結金額


                    return op;// CarFee.ToString() + "_" + NightFee.ToString() + "_" + BagFee.ToString() + "_" + PromotFee.ToString();
                }
                else
                    throw new Exception("無此服務費率表");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public int GetMinServicePassenger(string CityName, string DistinctName, string VillageName)
        {
            DAL.DALGeneral dal = new DAL.DALGeneral(this._ConnectionString);
            string Status;
            try
            {
                Status = dal.GetDistinctServiceStatus(CityName, DistinctName);
            }
            catch (Exception ex)
            {
                throw new Exception
                (ex.Message);
            }
            if (Status != null)
            {
                if (Status == "1")
                    return 1;
                else if (Status == "X")
                    return -1;
            }
            else
                throw new Exception("無定義之區域");
            try
            {
                Status = dal.GetVillageServiceStatus(CityName, DistinctName, VillageName);
            }
            catch (Exception ex)
            {
                throw new Exception
                (ex.Message);
            }
            if (Status != null)
            {
                string VillStatus = Status.Split(',')[0];
                int Cnt = Int32.Parse(Status.Split(',')[1]);
                if (VillStatus == "1")
                    return 1;
                else if (VillStatus == "X")
                    return -1;
                else
                    return Cnt;
            }
            else
                throw new Exception("無定義之區域");
        }

        public List<OFleet> GetFleets()
        {
            DAL.DALGeneral dal = new DAL.DALGeneral(this._ConnectionString);
            try
            {
                return dal.GetFleetList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public OFleet GetFleet(int FleetID)
        {
            DAL.DALGeneral dal = new DAL.DALGeneral(this._ConnectionString);
            try
            {
                return dal.GetFleet(FleetID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<OCar> GetCars(string fid)
        {
            DAL.DALGeneral dal = new DAL.DALGeneral(this._ConnectionString);
            try
            {
                return dal.GetCarsByFleet(fid);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<OEmployee> GetDrivers(string fid)
        {
            DAL.DALGeneral dal = new DAL.DALGeneral(this._ConnectionString);
            try
            {
                List<OEmployee> DriverList = new List<OEmployee>();
                return DriverList;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// API端驗證 Coupon當下是否有效
        /// </summary>
        /// <param name="Code"></param>
        /// <param name="TakeDate"></param>
        /// <param name="chkDate"></param>
        /// <returns></returns>
        public bool VarifyCoupon(string Code, string TakeDate, string chkDate)
        {
            DAL.DALGeneral dal = new DAL.DALGeneral(this._ConnectionString);
            DataTable dt = dal.GetCoupon(Code);


            if (dt.Rows.Count == 0)
                return false;
            else
            {
                string SDate;
                string EDate;
                string TDateS;
                string TDateE;
                int UseCnt, LimitCnt;
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    SDate = Convert.ToString(dt.Rows[i]["StartDate"]);
                    EDate = Convert.ToString(dt.Rows[i]["EndDate"]);
                    TDateS = Convert.ToString(dt.Rows[i]["TakeDateS"]);
                    TDateE = Convert.ToString(dt.Rows[i]["TakeDateE"]);
                    UseCnt = Convert.ToInt32(dt.Rows[i]["UseCnt"]);
                    LimitCnt = Convert.ToInt32(dt.Rows[i]["LimitCnt"]);
                    if (Convert.ToInt32(chkDate) >= Convert.ToInt32(SDate) && Convert.ToInt32(chkDate) <= Convert.ToInt32(EDate))
                        if (Convert.ToInt32(TakeDate) >= Convert.ToInt32(TDateS) && Convert.ToInt32(TakeDate) <= Convert.ToInt32(TDateE))
                            if (UseCnt < LimitCnt)
                                return true;
                }
                return false;
            }
        }

        /// <summary>
        /// 取得Coupon資料
        /// </summary>
        /// <param name="Code"></param>
        /// <param name="chkDate"></param>
        /// <param name="TakeDate"></param>
        /// <param name="newOrder"></param>
        /// <returns></returns>
        public OCoupon GetCoupon(string Code, string chkDate, string TakeDate, bool newOrder)
        {
            DAL.DALGeneral dal = new DAL.DALGeneral(this._ConnectionString);
            DataTable dt = dal.GetCoupon(Code);
            //string OrderDate = DateTime.Now.ToString("yyyyMMdd");
            OCoupon coupun = new OCoupon();

            if (dt.Rows.Count == 0)
                return null;
            else
            {
                string SDate, EDate, TDateS, TDateE;
                int UseCnt, LimitCnt;
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    SDate = Convert.ToString(dt.Rows[i]["StartDate"]);
                    EDate = Convert.ToString(dt.Rows[i]["EndDate"]);
                    TDateS = Convert.ToString(dt.Rows[i]["TakeDateS"]);
                    TDateE = Convert.ToString(dt.Rows[i]["TakeDateE"]);
                    UseCnt = Convert.ToInt32(dt.Rows[i]["UseCnt"]);
                    LimitCnt = Convert.ToInt32(dt.Rows[i]["LimitCnt"]);
                    if (Convert.ToInt32(chkDate) >= Convert.ToInt32(SDate) && Convert.ToInt32(chkDate) <= Convert.ToInt32(EDate))
                        if (Convert.ToInt32(TakeDate) >= Convert.ToInt32(TDateS) && Convert.ToInt32(TakeDate) <= Convert.ToInt32(TDateE))
                            if ((UseCnt < LimitCnt && newOrder) || !newOrder)
                            {
                                coupun.CouponCode = Code;
                                coupun.CarFeeSub = Convert.ToInt32(dt.Rows[i]["CarFeeSub"]);
                                coupun.NightFeeSub = Convert.ToInt32(dt.Rows[i]["NightFeeSub"]);
                                coupun.EndDate = Convert.ToString(dt.Rows[i]["EndDate"]);
                                coupun.StartDate = Convert.ToString(dt.Rows[i]["StartDate"]);
                                coupun.Memo = Convert.ToString(dt.Rows[i]["Memo"]);
                                coupun.SeqNo = Convert.ToInt32(dt.Rows[i]["SeqNo"]);
                                coupun.FixCarFee = Convert.ToInt32(dt.Rows[i]["FixCarFee"]);
                                return coupun;
                            }
                }
                return null;
            }
        }
    }
}
