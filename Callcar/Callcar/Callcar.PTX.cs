﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;
using Newtonsoft.Json;
using Callcar.Struct;

namespace Callcar
{

    public class PtxRequest
    {
        private const string AppRoot = @"https://ptx.transportdata.tw/MOTC/";

        public PtxRequest()
        {
        }

        public PtxRequest(string Ticket)
        {
            this.Ticket = Ticket;
        }

        public void SetRequest(string PTXDomain, string PTXService, string PTXApp, string PTXiata)
        {
            this._version = "v2";
            this._domain = PTXDomain;
            this._service = PTXService;
            this._application = PTXApp;
            this._iata = PTXiata;
            this._parameter = new Dictionary<string, string>();
        }

        public void SetParameter(Dictionary<string, string> para)
        {
            this._parameter = para;
        }

        public static string GetTicket(string Account, string Password)
        {
            PtxRequest ptx = new PtxRequest();
            ptx.SetRequest("Account", "Login", "", "");
            Dictionary<string, string> dic = new Dictionary<string, string>();
            dic.Add("UserData.account", Account);
            dic.Add("UserData.password", Password);
            ptx.SetParameter(dic);
            LoginResult mr = JsonConvert.DeserializeObject<LoginResult>(ptx.ExecuteRequest());

            if (mr.Status == "True" || mr.Status == "true")
                return mr.Ticket;
            else
                return "Error";
        }

        public string ExecuteRequest()
        {
            if (this._service == null || this._service == "")
                return "Error";

            string RequestURL = "";
            string Option = "";
            RtnStruct mj = new RtnStruct();

            foreach (var item in this._parameter)
            {
                Option += item.Key + "=" + item.Value + "&";
            }

            if (Option.Length > 0)
                Option = Option.Substring(0, Option.Length - 1);

            RequestURL = AppRoot + this._version;

            if (this._domain != "" & this._domain != null)
                RequestURL += @"/" + this._domain;

            if (this._service != "" & this._service != null)
                RequestURL += @"/" + this._service;

            if (this._application != "" & this._application != null)
                RequestURL += @"/" + this._application;

            if (this._iata != "" & this._iata != null)
                RequestURL += @"/" + this._iata;

            RequestURL += "?" + Option;

            HttpWebRequest req = (HttpWebRequest)HttpWebRequest.Create(RequestURL);
            req.Method = "GET";
            try
            {
                using (HttpWebResponse wr = (HttpWebResponse)req.GetResponse())
                {
                    if (wr.StatusCode == HttpStatusCode.OK)
                    {
                        Stream stream = wr.GetResponseStream();
                        StreamReader streamReader = new StreamReader(stream);
                        char[] buffer = new char[256];
                        int data = 0;
                        string result = "";

                        while (true)
                        {
                            data = streamReader.Read(buffer, 0, buffer.Length);
                            string msg = new string(buffer, 0, data);
                            result = result + msg;
                            if (data == 0)
                                break;
                        }
                        return result;
                    }
                    else
                    {
                        return "Error";
                    }
                }
            }
            catch (WebException)
            {
                return "Error";
            }
        }
        private string _version { get; set; }
        private string _domain { get; set; }
        private string _service { get; set; }
        private string _application { get; set; }
        private string _iata { get; set; }
        private Dictionary<string, string> _parameter { get; set; }

        public string Ticket { get; set; }
    }

    public class LoginResult
    {
        public string Status { get; set; }
        public string Message { get; set; }
        public string Ticket { get; set; }
    }
}
