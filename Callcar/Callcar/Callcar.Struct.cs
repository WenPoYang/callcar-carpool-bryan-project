﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;


namespace Callcar.Struct
{
    public class RtnStruct
    {
        public RtnStruct() { }
        //porperty
        public string Status { get; set; }
        public object RtnObject { get; set; }
    }

    #region UserAPI
    public class APIInsAccAPP
    {
        public string fname { get; set; }
        public string lname { get; set; }
        public string email { get; set; }
        public string mobile { get; set; }
        public string pw { get; set; }

    }

    public class APIInsAccFB
    {
        public string source { get; set; }
        public string sid { get; set; }
        public string nname { get; set; }
        public string fname { get; set; }
        public string mname { get; set; }
        public string lname { get; set; }
        public string sex { get; set; }
        public string email { get; set; }
        public string mobile { get; set; }
        public string pw { get; set; }

    }

    public class APINewOrder
    {

        public string uid { get; set; }  //用戶序號
        public string stype { get; set; } //出國或回國
        public string sdate { get; set; } //乘車日期
        public string ptime { get; set; } //預約時段
        public string ctype { get; set; } //航班或航廈
        public string pcartype { get; set; } //預約或撿車
        public string fno { get; set; }  //航班編號
        public string fdate { get; set; } //航班日期
        public string ftime { get; set; } //航班時間
        public string pcity { get; set; } //上車地點
        public string pdistinct { get; set; } //上車地點
        public string pvillage { get; set; }
        public string paddress { get; set; } //上車地點
        public string plat { get; set; }
        public string plng { get; set; }
        public string tcity { get; set; } //上車地點
        public string tdistinct { get; set; } //上車地點
        public string tvillage { get; set; }
        public string taddress { get; set; } //上車地點
        public string tlat { get; set; }
        public string tlng { get; set; }
        public string pcnt { get; set; } //人數
        public string bcnt { get; set; } //行李數量
        public string max { get; set; } // 挑戰滿車
        public string idata { get; set; } //發票編號
        public string cdata { get; set; } //信用卡編號
        public string dno { get; set; } //派車單號
        public string servetime { get; set; } //檢車的服務時間
        public int price { get; set; }//金額
        public string airport { get; set; } //機場代碼
        public string terminal { get; set; } //航廈
        public string coupon { get; set; } //優惠碼
        public string pname { get; set; } //乘客姓名
        public string pmobile { get; set; }//乘客手機
    }

    public class APINewInvoice
    {

        public string uid { get; set; }
        public string itype { get; set; }
        public string ititle { get; set; }
        public string ein { get; set; }
        public string iaddress { get; set; }

    }

    public class APIInvoiceOutput
    {
        public int iid { get; set; }
        public int uid { get; set; }
        public string title { get; set; }
        public string ein { get; set; }
        public string address { get; set; }
    }
    public class APINewCard
    {


        public string uid { get; set; }
        public string ccom { get; set; }
        public string cno { get; set; }
        public string edate { get; set; }
        public string acode { get; set; }

    }

    public class Pay2goResponse
    {
        public Pay2goResponse() { }

        public string Status { get; set; }
        public string Message { get; set; }
        public Pay2goResponseResult Result { get; set; }


        public class Pay2goResponseResult
        {
            public Pay2goResponseResult() { }

            public string MerchantID { get; set; }
            public string Amt { get; set; }
            public string TradeNo { get; set; }
            public string MerchantOrderNo { get; set; }
            public string RespondCode { get; set; }
            public string Auth { get; set; }
            public string AuthDate { get; set; }
            public string AuthTime { get; set; }
            public string Card6No { get; set; }
            public string Card4No { get; set; }
            public string Exp { get; set; }
            public string ECI { get; set; }
            public string IP { get; set; }
            public string EscrowBank { get; set; }
            public string TokenLife { get; set; }
            public string TokenValue { get; set; }
            public string CheckCode { get; set; }
        }
    }

    public class APITrailFee
    {

        public string od { get; set; }
        public string td { get; set; }
        public string tt { get; set; }
        public int pc { get; set; }
        public int bc { get; set; }
        public string cp { get; set; }
        public string dno { get; set; }

    }

    public class APIConsumeFee
    {

        public string tp { get; set; }
        public string cf { get; set; }
        public string nf { get; set; }
        public string bf { get; set; }
        public string pf { get; set; }

    }

    public class APIFlightStruct
    {

        public string ft { get; set; }
        public string fd { get; set; }
        public string aid { get; set; }
        public string fno { get; set; }
        public string da { get; set; }
        public string dt { get; set; }
        public string fulldt { get; set; }
        public string aa { get; set; }
        public string at { get; set; }
        public string fullat { get; set; }
        public string ter { get; set; }
        public string alname { get; set; }
        public string apname { get; set; }
        public string apcity { get; set; }
        public string dpname { get; set; }
        public string dpcity { get; set; }

    }

    public class APIGetAccOuterStruct
    {

        public string sr { get; set; } //source
        public string sid { get; set; } //sourceid

    }

    public class APIVarifyAcc
    {

        public string uid { get; set; }
        public string oid { get; set; }
        public string ml { get; set; }

    }

    public class GenScheduleStrct
    {
        public GenScheduleStrct()
        {
            this.ScheduleID = 0;
            this.FlightType = "";
            this.AirlineID = "";
            this.FlightNumber = "";
            this.ScheduleStartDate = "";
            this.ScheduleEndDate = "";
            this.DepartureAirportID = "";
            this.DepartureTime = "";
            this.ArrivalAirportID = "";
            this.ArrivalTime = "";
            this.FlyDay = "0000000";
            this.Terminal = "";
            this.Monday = false;
            this.Tuesday = false;
            this.Wednesday = false;
            this.Thursday = false;
            this.Friday = false;
            this.Saturday = false;
            this.Sunday = false;

        }
        //Property
        public int ScheduleID { get; set; }
        public string FlightType { get; set; }
        public string AirlineID { get; set; }
        public string FlightNumber { get; set; }
        public string ScheduleStartDate { get; set; }
        public string ScheduleEndDate { get; set; }
        public string DepartureAirportID { get; set; }
        public string DepartureTime { get; set; }
        public string ArrivalAirportID { get; set; }
        public string ArrivalTime { get; set; }
        public string FlyDay { get; set; }
        public string Terminal { get; set; }
        public bool Monday { get; set; }
        public bool Tuesday { get; set; }
        public bool Wednesday { get; set; }
        public bool Thursday { get; set; }
        public bool Friday { get; set; }
        public bool Saturday { get; set; }
        public bool Sunday { get; set; }
    }

    public class DepartureFlightInfoStruct
    {

        public string FlightDate { get; set; }
        public string FlightNumber { get; set; }
        public string AirRouteType { get; set; } //航線種類 = ['1: 國際' or '2: 國內' or '3: 兩岸' or '4: 國際包機' or '5: 國內包機' or '6: 兩岸包機' or '-2: 特殊'],
        public string AirlineID { get; set; }
        public string DepartureAirportID { get; set; }
        public string ArrivalAirportID { get; set; }
        public string ScheduleDepartureTime { get; set; }
        public string ActualDepartureTime { get; set; }
        public string DepartureRemark { get; set; } //航班屬性狀態,為第三方觀點的狀態(資料來源:民航局),
        public string DepartureRemarkEn { get; set; }
        public string Terminal { get; set; }
        public string Gate { get; set; }
        public string UpdateTime { get; set; }
    }

    public class ArrivalFlightInfoStruct
    {


        public string FlightDate { get; set; }
        public string FlightNumber { get; set; }
        public string AirRouteType { get; set; } //航線種類 = ['1: 國際' or '2: 國內' or '3: 兩岸' or '4: 國際包機' or '5: 國內包機' or '6: 兩岸包機' or '-2: 特殊'],
        public string AirlineID { get; set; }
        public string ArrivalAirportID { get; set; }
        public string DepartureAirportID { get; set; }
        public string SchedulearrivalTime { get; set; }
        public string ActualDepartureTime { get; set; }
        public string DepartureRemark { get; set; } //航班屬性狀態,為第三方觀點的狀態(資料來源:民航局),
        public string DepartureRemarkEn { get; set; }
        public string Terminal { get; set; }
        public string Gate { get; set; }
        public string UpdateTime { get; set; }
    }

    public class APIPickCarSource
    {
        public APIPickCarSource(int ServiceTime) { this.cars = new List<Struct.APIPickCarSource.PickCars>(); this.oPCnt = 0; this.oBCnt = 0; this.totalServiceTime = ServiceTime; }

        public string type { get; set; }
        public int oPCnt { get; set; }
        public int oBCnt { get; set; }
        public double PickupLat { get; set; }
        public double PickupLng { get; set; }
        public double TakeoffLat { get; set; }
        public double TakeoffLng { get; set; }
        public int totalServiceTime { get; set; }
        public List<PickCars> cars { get; set; }

        public class PickCars
        {
            public PickCars() { this.services = new List<Struct.APIPickCarSource.PickCarServices>(); this.scnt = 0; }
            public string dno { get; set; }
            public int scnt { get; set; }
            public int pcnt { get; set; }
            public int bcnt { get; set; }
            public List<PickCarServices> services { get; set; }
        }

        public class PickCarServices
        {
            public double PickupLat { get; set; }
            public double PickupLng { get; set; }
            public double TakeoffLat { get; set; }
            public double TakeoffLng { get; set; }
            public string time { get; set; }
        }
    }

    #endregion

    #region DriverAPI
    public class DriverInfoStruct
    {
        public int id { get; set; }
        public string name { get; set; }
        public int fno { get; set; }
        public string fname { get; set; }
    }
    #endregion

    #region BackWebAPI

    public class DispatchBriefStruct
    {
        public string dno { get; set; }  //派遣單號
        public string td { get; set; }  //搭乘日期
        public string tt { get; set; } //服務時間
        public string st { get; set; }  //出回國 (I/O)
        public int pc { get; set; } //全車人數
        public int bc { get; set; }  //全車行李數
        public int sc { get; set; }  //全車訂單數
        public string cp { get; set; } //車隊
        public int fid { get; set; } //車隊ID
        public int did { get; set; } //司機ID
        public string dr { get; set; } //司機
        public string cno { get; set; } //車號
        public string ct { get; set; }//車型
    }

    public class EmployeInfoStruct
    {
        public int id { get; set; } //員工編號新增時不需傳入
        public string pw { get; set; } //帳號明碼
        public string cid { get; set; } //身分證字號
        public string empname { get; set; } //姓名
        public string email { get; set; } //Email
        public string phone1 { get; set; } //手機
        public string phone2 { get; set; } //市話
        public string role { get; set; } //角色
        public string company { get; set; } //公司
        public string fleet { get; set; } //車隊
        public string city { get; set; } //所在地 城市
        public string distinct { get; set; } //所在地 區名
        public string address { get; set; } //所在地 地址
        public string active { get; set; } //活動/不活動帳號 (1 / 0) //新增時不需傳入
        public int Logint_tGroups_AutoID { get; set; } //暫時使用和相專接網站 用
    }

    public class UpdOrderAddress
    {
        public string Rno { get; set; }
        public string TakeoffCity { get; set; }
        public string TakeoffDistinct { get; set; }
        public string TakeoffVillage { get; set; }
        public string TakeoffAddress { get; set; }
        public double Takeofflat { get; set; }
        public double Takeofflng { get; set; }

        public string PickupCity { get; set; }
        public string PickupDistinct { get; set; }
        public string PickupVillage { get; set; }
        public string PickupAddress { get; set; }
        public double Pickuplat { get; set; }
        public double Pickuplng { get; set; }

        public string TakeDate { get; set; }
        public string TakeTime { get; set; }
        public string FlightNo { get; set; }
        public string FlightDate { get; set; }
    }

    public class ReservationFullStruct
    {
        public string rno { get; set; }
        public int uid { get; set; }
        public string sertype { get; set; }
        public string tdate { get; set; }
        public string timeseg { get; set; }
        public string seltype { get; set; }
        public string pcartype { get; set; }
        public string fno { get; set; }
        public string fdatetime { get; set; }
        public string pcity { get; set; }
        public string pdistinct { get; set; }
        public string pvillage { get; set; }
        public string paddress { get; set; }
        public double plat { get; set; }
        public double plng { get; set; }
        public string tcity { get; set; }
        public string tdistinct { get; set; }
        public string tvillage { get; set; }
        public string taddress { get; set; }
        public double tlat { get; set; }
        public double tlng { get; set; }
        public int pcnt { get; set; }
        public int bcnt { get; set; }
        public string max { get; set; }
        public int trialprice { get; set; }
        public string coupun { get; set; }
        public string invoice { get; set; }
        public string credit { get; set; }
        public string stage { get; set; }
        public string ein { get; set; }
        public string cdatetime { get; set; }
    }

    public class QOrderCondStruct
    {
        public string QSDate { get; set; }
        public string QEDate { get; set; }
        public string OrderNo { get; set; }
        public int UserID { get; set; }
        public string MobilePhone { get; set; }
        public int take { get; set; }
        public int skip { get; set; }
    }

    public class UOrderStruct
    {
        public string RNo { get; set; }
        public string uType { get; set; }
        public string OldData { get; set; }
        public string NewData { get; set; }
    }

    public class QDispatchCondStruct
    {
        public string QSDate { get; set; }
        public string QEDate { get; set; }
        public string DispatchNo { get; set; }
        public string OrderNo { get; set; }
        public int DriverID { get; set; }
        public string CarNo { get; set; }
        public int skip { get; set; }
        public int take { get; set; }
    }

    public class UDispatchCarStruct
    {
        public string Dno { get; set; }
        public string OldData { get; set; } //fleet id,car no,employee id
        public string NewData { get; set; } //fleet id,car no,employee id
    }

    public class FleetInfoStruct
    {

        public string ID { get; set; }
        public string Name { get; set; }
        public bool ActiveFlag { get; set; }
        public DateTime CreateTime { get; set; }
        public DateTime UpdTime { get; set; }

    }

    public class FleetListStruct
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public bool ActiveFlag { get; set; }
    }

    public class CarListStruct
    {
        public string CarNo { get; set; }
        public string FleetName { get; set; }
        public string Type { get; set; }
        public string DriverName { get; set; }
        public string ScheduleFlag { get; set; }
    }

    public class DriverListStruct
    {
        public string ID { get; set; }
        public string FleetID { get; set; }
        public string Name { get; set; }
        public string ActiveFlag { get; set; }
    }

    public class DispatchLocationsStruct
    {
        public string dno { get; set; }
        public int cnt { get; set; }
        public List<CObject.ODpServiceUnit> Service { get; set; }
        public DispatchLocationsStruct()
        {
            this.cnt = 0;
            this.Service = new List<CObject.ODpServiceUnit>();
        }
    }



    #region 媒合結果回傳Json
    public class MatchResultJsonStruct
    {
        public List<DispatchInfo> DispatchInfo { get; set; }
    }

    public class DispatchInfo
    {
        public DispatchInfo()
        {
            this.Detail = new Detail();
        }
        public int Index { get; set; }
        public Detail Detail { get; set; }
    }

    public class Detail
    {
        public Detail()
        {
            this.ServiceList = new List<ServiceList>();
        }
        public string ServiceDate { get; set; }
        public string ServiceType { get; set; }
        public int ScheduleID { get; set; }
        public string TimeSegment { get; set; }
        public int ServiceCnt { get; set; }
        public int PassengerCnt { get; set; }
        public int BaggageCnt { get; set; }
        public string MaxFlag { get; set; }
        public List<ServiceList> ServiceList { get; set; }
    }

    public class ServiceList
    {
        public string ReservationNo { get; set; }
        public string ServiceTime { get; set; }
    }
    #endregion

    #endregion

    #region Common
    public class AccountPWStruct
    {
        public string acc { get; set; }
        public string pw { get; set; }
    }
    #endregion
}