﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Sybase.Data.AseClient;
using DbConn;
using Utility;

namespace CHMC.ER
{
    public class DALER
    {
        private string _ConnectionString;
        public DALER(string ConnectionString)
        {
            this._ConnectionString = ConnectionString;
        }

        public ERTagInfo GetTagERInfo(string TagID)
        {
            StringBuilder sb = new StringBuilder();
            List<AseParameter> parms = new List<AseParameter>();
            sb.Append(" Select  tag_id,patient_id,arrival_time,reader_id,reader_name,tag_voltage,tag_band,tag_button ");
            sb.Append(" From er_tag Where tag_id = @id and tag_band = '1' ");
            parms.Add(new AseParameter("@id", TagID));
            DataTable dt;

            using (dbConnectionSybase conn = new dbConnectionSybase(this._ConnectionString))
            {
                dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
            }

            if (dt.Rows.Count <= 0 || dt.Rows.Count > 1)
                return null;

            ERTagInfo t = new ERTagInfo();
            t.ArrivalTime = Common.ConvertToDateTime(Convert.ToString(dt.Rows[0]["arrival_time"]));
            t.PatientID = Convert.ToString(dt.Rows[0]["patient_id"]);
            t.TagID = Convert.ToString(dt.Rows[0]["tag_id"]);
            t.RxID = Convert.ToString(dt.Rows[0]["reader_id"]);
            t.RxDesc = "";// Common.CP850ToUnicode(Convert.ToString(dt.Rows[0]["reader_name"]));
            t.TagPower = Convert.ToString(dt.Rows[0]["tag_voltage"]);
            t.TagBand = Convert.ToString(dt.Rows[0]["tag_band"]);
            t.TagButton = Convert.ToString(dt.Rows[0]["tag_button"]);
            return t;
        }

        public bool addNewLog(ERTagInfoLog log)
        {
            StringBuilder sb = new StringBuilder();
            List<AseParameter> parms = new List<AseParameter>();
            sb.Append("Insert Into er_tag_log ( tag_id,patient_id,arrival_time,reader_id,reader_name,activator_id,activator_name,tag_voltage,tag_band,tag_button,append_op,append_dt,data_op,data_dt,patient_status) ");
            sb.Append(" Values ( @tag_id,@patient_id,@arrival_time,@reader_id,@reader_name,@activator_id,@activator_name,@tag_voltage,@tag_band,@tag_button,@append_op,@append_dt,@data_op,@data_dt,@patient_status) ");
            parms.Add(new AseParameter("@tag_id", log.TagID));
            parms.Add(new AseParameter("@patient_id", log.PatientID));
            parms.Add(new AseParameter("@arrival_time", log.ArrivalTime.ToString("yyyyMMddHHmmss")));
            parms.Add(new AseParameter("@reader_id", log.RxID));
            parms.Add(new AseParameter("@reader_name", ""));//Common.UnicodeToCP850(log.RxDesc)));
            parms.Add(new AseParameter("@activator_id", ""));
            parms.Add(new AseParameter("@activator_name", ""));
            parms.Add(new AseParameter("@tag_voltage", log.TagPower));
            parms.Add(new AseParameter("@tag_band", log.TagBand));
            parms.Add(new AseParameter("@tag_button", log.TagButton));
            parms.Add(new AseParameter("@append_op", "RFID"));
            parms.Add(new AseParameter("@append_dt", DateTime.Now.ToString("yyyyMMddHHmmss")));
            parms.Add(new AseParameter("@data_op", "RFID"));
            parms.Add(new AseParameter("@data_dt", DateTime.Now.ToString("yyyyMMddHHmmss")));
            parms.Add(new AseParameter("@patient_status", ""));

            using (dbConnectionSybase conn = new dbConnectionSybase(this._ConnectionString))
            {
                if (conn.executeInsertQuery(sb.ToString(), parms.ToArray()))
                {
                    sb.Length = 0;
                    parms.Clear();

                    sb.Append("Update er_tag  ");
                    sb.Append("Set reader_id =@reader_id,reader_name =@reader_name,tag_voltage=@tag_voltage,tag_button=@tag_button,append_op=@append_op,append_dt=@append_dt,data_op=@data_op,data_dt=@data_dt ");
                    sb.Append("Where  tag_id = @tag_id ");
                    parms.Add(new AseParameter("@tag_id", log.TagID));
                    parms.Add(new AseParameter("@reader_id", log.RxID));
                    parms.Add(new AseParameter("@reader_name", ""));//Common.UnicodeToCP850(log.RxDesc)));
                    parms.Add(new AseParameter("@tag_voltage", log.TagPower));
                    parms.Add(new AseParameter("@tag_button", log.TagButton));
                    parms.Add(new AseParameter("@append_op", "RFID"));
                    parms.Add(new AseParameter("@append_dt", DateTime.Now.ToString("yyyyMMddHHmmss")));
                    parms.Add(new AseParameter("@data_op", "RFID"));
                    parms.Add(new AseParameter("@data_dt", DateTime.Now.ToString("yyyyMMddHHmmss")));

                    if (conn.executeUpdateQuery(sb.ToString(), parms.ToArray()) > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }


        }

    }

    public class ERTagInfo
    {
        public string TagID { get; set; }
        public string PatientID { get; set; }
        public DateTime ArrivalTime { get; set; }
        public string RxID { get; set; }
        public string RxDesc { get; set; }
        public string TagPower { get; set; }
        public string TagBand { get; set; }
        public string TagButton { get; set; }
    }

    public class ERTagInfoLog
    {
        public string TagID { get; set; }
        public string PatientID { get; set; }
        public DateTime ArrivalTime { get; set; }
        public string RxID { get; set; }
        public string RxDesc { get; set; }
        public string TagPower { get; set; }
        public string TagBand { get; set; }
        public string TagButton { get; set; }
    }
}

