﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using Newtonsoft.Json;
using BeaconRx;
using System.Configuration;
using CHMC.ER;
using Utility;
using NLog;
using NLog.Config;


namespace ErTagTrans
{
    public partial class wMain : Form
    {
        private static Logger logger = NLog.LogManager.GetCurrentClassLogger();
        string CurrentDir = @"C:\inetpub\wwwroot\RxAPI";
        string ConnectionStringBeacon = ConfigurationManager.ConnectionStrings["BeaconRxDB"].ConnectionString;
        string ConnectionStringSybase = ConfigurationManager.ConnectionStrings["SybaseDB"].ConnectionString;
        public wMain()
        {
            InitializeComponent();


            timer1.Interval = 1000;
            timer1.Enabled = true;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {

            timer1.Enabled = false;



            DirectoryInfo di = new DirectoryInfo(CurrentDir + @"\Rx");

            foreach (var fi in di.GetFiles("*.txt").OrderBy(f => f.Name))
            {
                try
                {
                    string FileTime = fi.Name.Split('.')[0].Split('_')[0];
                    DateTime Receivetime = Common.ConvertToDateTime(FileTime);
                    string JsonData = File.ReadAllText(fi.FullName);

                    RxOutputData RxJson = JsonConvert.DeserializeObject<RxOutputData>(JsonData);
                    DALBeacon dalb = new DALBeacon(ConnectionStringBeacon);

                    Receiver Rx = dalb.GetReceiver(RxJson.RxMac, Receivetime);

                    if (Rx == null)
                    {
                        logger.Warn("Rx MacAddress : " + RxJson.RxMac + " 不存在");
                        File.Delete(fi.FullName);
                        continue;
                    }

                    foreach (TagOutputData td in RxJson.Tag)
                    {

                        //取出Pair資料
                        //1.從TagMac找到Pair
                        if (Convert.ToInt32(td.RSSI.Replace("db", "")) <= Rx.RSSIBoundary - 5)
                        {
                            if (td == RxJson.Tag.Last<TagOutputData>())
                                File.Delete(fi.FullName);
                            continue;
                        }

                        DALER dale = new DALER(ConnectionStringSybase);
                        Tag t = dalb.GetTag(td.TxMac, DateTime.Now);
                        if (t == null)
                        {
                            logger.Warn("TagMac = " + t.TagMac + "  不存在");
                            if (td == RxJson.Tag.Last<TagOutputData>())
                                File.Delete(fi.FullName);

                            continue;
                        }



                        ERTagInfo et = dale.GetTagERInfo(t.TagID);
                        TagPair p = dalb.GetPairByTagID(t.TagID, DateTime.Now);

                        if (p == null)
                        {
                            //檢傷查不到這個Tag正在被配帶
                            if (et == null)
                            {
                                logger.Warn("檢傷 ：TagID = " + t.TagID + "  不存在");
                                if (td == RxJson.Tag.Last<TagOutputData>())
                                    File.Delete(fi.FullName);

                                continue;
                            }

                            //剛入院 尚未產生TagPair
                            //從Sybase資料庫讀取
                            // 新增一筆Pair
                            TagPair pair = new TagPair();
                            pair.TagID = t.TagID;
                            pair.BeginTime = et.ArrivalTime;
                            pair.PairID = 0;
                            pair.ReleaseTime = Common.ConvertToDateTime("99991231235959");
                            pair.TargetID = et.PatientID;
                            pair.TargetType = "P";
                            pair.Status = "新增";

                            int PairID = 0;
                            try
                            {
                                PairID = dalb.addNewPair(pair);
                            }
                            catch (MySql.Data.MySqlClient.MySqlException ex)
                            {
                                logger.Error("新增 Pair 失敗 TagID " + t.TagID + Environment.NewLine + JsonConvert.SerializeObject(td) + Environment.NewLine + ex.Number + " " + ex.Message);
                                File.Delete(fi.FullName);
                                continue;
                            }
                            catch (Exception ex)
                            {
                                logger.Error("新增 Pair 失敗 TagID " + t.TagID + Environment.NewLine + JsonConvert.SerializeObject(td) + Environment.NewLine + ex.Message);
                                File.Delete(fi.FullName);
                                continue;
                            }
                            if (PairID == -1)
                            {
                                logger.Error("新增 Pair 失敗 TagID " + t.TagID + Environment.NewLine + JsonConvert.SerializeObject(td));
                                File.Delete(fi.FullName);
                                continue;
                            }
                            //取得TagPair
                            p = dalb.GetPairByTagID(t.TagID, DateTime.Now);
                        }
                        else
                        {
                            //有Pair資料 檢查急診端是否已經解除
                            //已經解除 則同步解除Local Pair
                            //否則繼續

                            if (et == null)
                            {
                                //急診已經release
                                //Local端也要Release
                                try
                                {
                                    if (dalb.releasePair(p.PairID))
                                        File.Delete(fi.FullName);
                                }
                                catch (Exception)
                                {
                                }

                                File.Delete(fi.FullName);
                                continue;
                            }
                        }


                        //新增足跡資料至LocalDB
                        TagTraceLog log = new TagTraceLog();
                        log.PairID = p.PairID;
                        //先改成抓local time
                        log.ReceiveTime = DateTime.Now;// Common.ConvertToDateTime(td.Rtime.Replace("-", ""));
                        log.RxMac = RxJson.RxMac;
                        if (td.Power == "100%")
                        {
                            try
                            {
                                log.Power = Convert.ToInt32(et.TagPower.Replace("%", ""));
                            }
                            catch (Exception)
                            {
                                log.Power = 99;
                            }
                        }
                        else
                        {
                            try
                            {
                                log.Power = Convert.ToInt32(td.Power.Replace("%", ""));
                            }
                            catch (Exception)
                            {
                                try
                                {
                                    log.Power = Convert.ToInt32(et.TagPower.Replace("%", ""));
                                }
                                catch (Exception)
                                {
                                    log.Power = 99;
                                }
                            }
                        }


                        log.RSSI = Convert.ToInt32(td.RSSI.Replace("db", ""));
                        log.Button = Convert.ToBoolean(Convert.ToInt32(td.Button));
                        log.GSensor = Convert.ToBoolean(Convert.ToInt32(td.GSensor));
                        int TraceID = dalb.addNewTraceLog(log);
                        if (TraceID == -1)
                        {
                            logger.Error("新增 Trace 失敗 PairID " + p.PairID);
                            continue;
                        }
                        else if (TraceID == 0)
                        {
                            File.Delete(fi.FullName);
                            continue;
                        }


                        //更新Pair 狀態
                        bool addER = true;
                        bool modPair = true;
                        string Status = "";

                        /*
                        if (p.RxMac != Rx.RxMac)
                        {
                            //轉移至新的位置                            
                            if (log.RSSI >= Rx.RSSIBoundary)
                            {
                                Status = "進入";
                                addER = true;
                                modPair = true;
                            }
                        }
                        else
                        {
                            //同一個Rx範圍
                            if (log.RSSI < Rx.RSSIBoundary)
                            {
                                if (p.Status != "離開")
                                {
                                    Status = "離開";
                                    modPair = true;
                                    addER = true;
                                }
                            }
                            else if (log.RSSI >= Rx.RSSIBoundary)
                            {
                                if (p.Status == "進入")
                                    Status = "停留";
                                else if (p.Status == "離開")
                                    Status = "進入";
                                else
                                    Status = "";

                                if (Status == "停留" || Status == "進入")
                                {
                                    addER = true;
                                    modPair = true;
                                }
                                else
                                {
                                    addER = false;
                                    modPair = false;
                                }
                            }
                        }

                         

                        //按下呼叫紐 or 取消呼叫紐 or 電力不足時寫入追蹤&急診
                        if (log.Button || log.Button != p.Button || log.Power <= 25)
                        {
                            addER = true;
                            modPair = true;
                        }
                        */
                        //更新配對現況檔
                        p.Status = Status;
                        p.Power = log.Power;
                        p.RxMac = Rx.RxMac;
                        p.Button = log.Button;

                        //追蹤資料
                        TagTrace trace = new TagTrace();
                        trace.PairID = log.PairID;
                        trace.ReceiveTime = log.ReceiveTime;
                        trace.RxMac = Rx.RxMac;
                        trace.Power = log.Power;
                        trace.Status = Status;
                        trace.Button = log.Button;

                        try
                        {
                            if (modPair)
                            {
                                dalb.updPair(p); //更新配對現況
                                dalb.addNewTrace(trace); //寫入配對追蹤紀錄
                            }
                        }
                        catch (Exception)
                        {
                        }



                        //到振興才開放 
                        //回寫振興資料庫
                        if (addER)
                        {

                            ERTagInfoLog ERLog = new ERTagInfoLog();
                            ERLog.TagID = t.TagID;
                            ERLog.PatientID = p.TargetID;
                            ERLog.ArrivalTime = p.BeginTime;
                            ERLog.RxID = Rx.RxID;
                            ERLog.RxDesc = "";// Rx.RxDesc;
                            ERLog.TagBand = "1";
                            ERLog.TagButton = td.Button;
                            ERLog.TagPower = td.Power;

                            if (!dale.addNewLog(ERLog))
                            {
                                if (!dale.addNewLog(ERLog))
                                {
                                    File.Delete(fi.FullName);
                                    continue;
                                }
                            }
                        }


                        File.Delete(fi.FullName);
                    }
                }
                catch (Exception ex)
                {
                    logger.Error("External Error : " + ex.Message);
                    File.Delete(fi.FullName);
                    continue;
                }
            }
            timer1.Enabled = true;
        }
    }

    public class RxOutputData
    {
        public RxOutputData()
        {
            Tag = new List<TagOutputData>();
        }
        public string RxMac { get; set; }
        // public DateTime Receivetime { get; set; }
        public List<TagOutputData> Tag { get; set; }
    }

    public class TagOutputData
    {
        public string TxMac { get; set; }
        public string Rtime { get; set; }
        public string Power { get; set; }
        public string RSSI { get; set; }
        public string Button { get; set; }
        public string GSensor { get; set; }
    }
}
