﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DbConn;
using MySql.Data.MySqlClient;
using System.Data;

namespace BeaconRx
{
    public class DALBeacon
    {
        private string _ConnectionString;
        public DALBeacon(string ConnectionString)
        {
            this._ConnectionString = ConnectionString;
        }

        public Tag GetTag(string MacAddress, DateTime QTime)
        {
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            sb.Append("SELECT SeqNo, TagID, StartTime, EndTime, MacAddress, TagDesc ");
            sb.Append("FROM tag_maintain where MacAddress = @mac and @time between StartTime and EndTime");
            parms.Add(new MySqlParameter("@mac", MacAddress));
            parms.Add(new MySqlParameter("@time", QTime));
            DataTable dt;
            using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
            {
                dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
            }

            if (dt.Rows.Count == 0 || dt.Rows.Count > 1)
                return null;

            Tag t = new Tag();
            t.SeqNo = Convert.ToInt32(dt.Rows[0]["SeqNo"]);
            t.TagID = Convert.ToString(dt.Rows[0]["TagID"]);
            t.TagMac = Convert.ToString(dt.Rows[0]["MacAddress"]);
            t.TagDesc = Convert.ToString(dt.Rows[0]["TagDesc"]);
            t.StartTime = Convert.ToDateTime(dt.Rows[0]["StartTime"]);
            t.EndTime = Convert.ToDateTime(dt.Rows[0]["EndTime"]);

            return t;
        }

        public Receiver GetReceiver(string MacAddress, DateTime QTime)
        {
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            sb.Append("SELECT SeqNo, RxID, StartTime, EndTime, MacAddress, RxDesc,Position,RSSIBoundary ");
            sb.Append("FROM rx_maintain 	where MacAddress = @mac and @time between StartTime and EndTime ");
            parms.Add(new MySqlParameter("@mac", MacAddress));
            parms.Add(new MySqlParameter("@time", QTime));
            DataTable dt;
            using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
            {
                dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
            }

            if (dt.Rows.Count == 0 || dt.Rows.Count > 1)
                return null;

            Receiver r = new Receiver();
            r.SeqNo = Convert.ToInt32(dt.Rows[0]["SeqNo"]);
            r.RxID = Convert.ToString(dt.Rows[0]["RxID"]);
            r.RxMac = Convert.ToString(dt.Rows[0]["MacAddress"]);
            r.RxDesc = Convert.ToString(dt.Rows[0]["RxDesc"]);
            r.StartTime = Convert.ToDateTime(dt.Rows[0]["StartTime"]);
            r.EndTime = Convert.ToDateTime(dt.Rows[0]["EndTime"]);
            r.Position = Convert.ToString(dt.Rows[0]["Position"]);
            r.RSSIBoundary = Convert.ToInt32(dt.Rows[0]["RSSIBoundary"]);

            return r;
        }

        public TagPair GetPairByTagID(string TagID, DateTime QTime)
        {
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            sb.Append("SELECT PairID, TagID, BeginTime, TargetType, TargetID, ReleaseTime,Power, Button, RxMac, Status ");
            sb.Append("FROM tag_pair_record 	where TagID = @tid and @time between BeginTime and  ReleaseTime");
            parms.Add(new MySqlParameter("@tid", TagID));
            parms.Add(new MySqlParameter("@time", QTime));
            DataTable dt;
            using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
            {
                dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
            }

            if (dt.Rows.Count == 0 || dt.Rows.Count > 1)
                return null;

            TagPair t = new TagPair();
            t.PairID = Convert.ToInt32(dt.Rows[0]["PairID"]);
            t.TagID = Convert.ToString(dt.Rows[0]["TagID"]);
            t.TargetType = Convert.ToString(dt.Rows[0]["TargetType"]);
            t.TargetID = Convert.ToString(dt.Rows[0]["TargetID"]);
            t.BeginTime = Convert.ToDateTime(dt.Rows[0]["BeginTime"]);
            t.ReleaseTime = Convert.ToDateTime(dt.Rows[0]["ReleaseTime"]);
            t.Power = Convert.ToInt32(dt.Rows[0]["Power"]);
            t.Button = Convert.ToBoolean(dt.Rows[0]["Button"]);
            t.RxMac = Convert.ToString(dt.Rows[0]["RxMac"]);
            t.Status = Convert.ToString(dt.Rows[0]["Status"]);

            return t;
        }

        public TagPair GetPairByTagMac(string TagMac, DateTime QTime)
        {
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            sb.Append("SELECT p.PairID, p.TagID, p.BeginTime, p.TargetType, p.TargetID, p.ReleaseTime,Power, Button, RxMac, Status ");
            sb.Append("FROM tag_pair_record p,tag_maintain t	 ");
            sb.Append("where p.TagID = t.TagID and  t.MacAddress = @mac and @time between p.BeginTime and  p.ReleaseTime ");
            sb.Append("and @time between t.StartTime and t.EndTime ");
            parms.Add(new MySqlParameter("@mac", TagMac));
            parms.Add(new MySqlParameter("@time", QTime));
            DataTable dt;
            using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
            {
                dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
            }

            if (dt.Rows.Count == 0 || dt.Rows.Count > 1)
                return null;

            TagPair t = new TagPair();
            t.PairID = Convert.ToInt32(dt.Rows[0]["PairID"]);
            t.TagID = Convert.ToString(dt.Rows[0]["TagID"]);
            t.TargetType = Convert.ToString(dt.Rows[0]["TargetType"]);
            t.TargetID = Convert.ToString(dt.Rows[0]["TargetID"]);
            t.BeginTime = Convert.ToDateTime(dt.Rows[0]["BeginTime"]);
            t.ReleaseTime = Convert.ToDateTime(dt.Rows[0]["ReleaseTime"]);
            t.Power = Convert.ToInt32(dt.Rows[0]["Power"]);
            t.Button = Convert.ToBoolean(dt.Rows[0]["Button"]);
            t.RxMac = Convert.ToString(dt.Rows[0]["RxMac"]);
            t.Status = Convert.ToString(dt.Rows[0]["Status"]);

            return t;
        }

        public int addNewPair(TagPair pair)
        {
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();

            sb.Append("INSERT INTO tag_pair_record (TagID, BeginTime, TargetType, TargetID, ReleaseTime,Power, Button, RxMac, Status) ");
            sb.Append(" VALUES (@TagID, @BeginTime, @TargetType, @TargetID, @ReleaseTime,@Power, @Button, @RxMac, @Status) ");

            parms.Add(new MySqlParameter("@TagID", pair.TagID));
            parms.Add(new MySqlParameter("@BeginTime", pair.BeginTime));
            parms.Add(new MySqlParameter("@TargetType", pair.TargetType));
            parms.Add(new MySqlParameter("@TargetID", pair.TargetID));
            parms.Add(new MySqlParameter("@ReleaseTime", pair.ReleaseTime));
            parms.Add(new MySqlParameter("@Power", pair.Power));
            parms.Add(new MySqlParameter("@Button", pair.Button));
            parms.Add(new MySqlParameter("@RxMac", pair.RxMac));
            parms.Add(new MySqlParameter("@Status", pair.Status));

            DataTable dt;
            bool rtn = false;
            using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
            {
                try
                {
                    rtn = conn.executeInsertQuery(sb.ToString(), parms.ToArray());
                }
                catch (MySqlException e)
                {
                    throw e;
                }
                catch (Exception e)
                {
                    throw e;
                }
                if (!rtn)
                    return -1;

                sb.Length = 0;
                parms.Clear();
                sb.Append("SELECT LAST_INSERT_ID()");
                dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());

                return Convert.ToInt32(dt.Rows[0][0]);
            }
        }

        public bool updPair(TagPair pair)
        {
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();

            sb.Append("UPDATE tag_pair_record SET Power=@Power ,Button=@Button ,RxMac=@RxMac ,Status=@Status  WHERE PairID = @PairID ");
            parms.Add(new MySqlParameter("@Power", pair.Power));
            parms.Add(new MySqlParameter("@Button", pair.Button));
            parms.Add(new MySqlParameter("@RxMac", pair.RxMac));
            parms.Add(new MySqlParameter("@Status", pair.Status));
            parms.Add(new MySqlParameter("@PairID", pair.PairID));
            using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
            {
                return conn.executeInsertQuery(sb.ToString(), parms.ToArray());
            }
        }

        public bool releasePair(int PairID)
        {
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            sb.Append("UPDATE tag_pair_record SET ReleaseTime=NOW() WHERE PairID = @PairID ");
            parms.Add(new MySqlParameter("@PairID", PairID));
            using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
            {
                return  conn.executeUpdateQuery(sb.ToString(), parms.ToArray());
            }

        }

        public bool addNewTrace(TagTrace trace)
        {
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();

            sb.Append("INSERT INTO tag_trace_record (PairID, ReceiveTime,Power, Button, RxMac, Status) ");
            sb.Append(" VALUES (@PairID, @ReceiveTime, @Power, @Button, @RxMac, @Status) ");

            parms.Add(new MySqlParameter("@PairID", trace.PairID));
            parms.Add(new MySqlParameter("@ReceiveTime", trace.ReceiveTime));
            parms.Add(new MySqlParameter("@Power", trace.Power));
            parms.Add(new MySqlParameter("@Button", trace.Button));
            parms.Add(new MySqlParameter("@RxMac", trace.RxMac));
            parms.Add(new MySqlParameter("@Status", trace.Status));


            using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
            {
                return conn.executeInsertQuery(sb.ToString(), parms.ToArray());
            }
        }

        public int addNewTraceLog(TagTraceLog trace)
        {
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();

            sb.Append("INSERT INTO tag_trace_record_log 	(PairID, ReceiveTime, RxMac, Power, RSSI, Button, GSensor) ");
            sb.Append("VALUES (@PairID, @ReceiveTime, @RxMac, @Power, @RSSI, @Button, @GSensor) ");

            parms.Add(new MySqlParameter("@PairID", trace.PairID));
            parms.Add(new MySqlParameter("@ReceiveTime", trace.ReceiveTime));
            parms.Add(new MySqlParameter("@RxMac", trace.RxMac));
            parms.Add(new MySqlParameter("@Power", trace.Power));
            parms.Add(new MySqlParameter("@RSSI", trace.RSSI));
            parms.Add(new MySqlParameter("@Button", trace.Button));
            parms.Add(new MySqlParameter("@GSensor", trace.GSensor));
            DataTable dt;
            bool rtn = false;
            using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
            {
                try
                {
                    rtn = conn.executeInsertQuery(sb.ToString(), parms.ToArray());
                }
                catch (MySqlException e)
                {
                    if (e.Number == 1062)
                        return 0;
                    else
                        throw new Exception(e.StackTrace);
                }
                if (!rtn)
                    return -1;

                sb.Length = 0;
                parms.Clear();
                sb.Append("SELECT LAST_INSERT_ID()");
                dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());

                sb.Length = 0;
                parms.Clear();

                sb.Append("UPDATE tag_pair_record SET LastReceiveTime=@time WHERE PairID = @PairID");
                parms.Add(new MySqlParameter("@PairID", trace.PairID));
                parms.Add(new MySqlParameter("@time", trace.ReceiveTime));

                try
                {
                    conn.executeUpdateQuery(sb.ToString(), parms.ToArray());
                }
                catch (MySqlException)
                {

                }
                return Convert.ToInt32(dt.Rows[0][0]);
            }
        }
    }

    public class Receiver
    {
        public Receiver()
        {

        }

        public int SeqNo { get; set; }
        public string RxID { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public string RxMac { get; set; }
        public string RxDesc { get; set; }
        public string Position { get; set; }
        public int RSSIBoundary { get; set; }
    }

    public class Tag
    {
        public Tag()
        {

        }
        public int SeqNo { get; set; }
        public string TagID { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public string TagMac { get; set; }
        public string TagDesc { get; set; }
    }

    public class TagPair
    {
        public TagPair()
        {

        }
        public int PairID { get; set; }
        public string TagID { get; set; }
        public DateTime BeginTime { get; set; }
        public string TargetType { get; set; }
        public string TargetID { get; set; }
        public DateTime ReleaseTime { get; set; }
        public int Power { get; set; }
        public bool Button { get; set; }
        public string RxMac { get; set; }
        public string Status { get; set; }
    }

    public class TagTrace
    {
        public TagTrace()
        {

        }
        public int PairID { get; set; }
        public string TagID { get; set; }
        public DateTime ReceiveTime { get; set; }
        public int Power { get; set; }
        public bool Button { get; set; }
        public string RxMac { get; set; }
        public string Status { get; set; }
    }

    public class TagTraceLog
    {
        public TagTraceLog() { }

        public int SeqNo { get; set; }
        public int PairID { get; set; }
        public DateTime ReceiveTime { get; set; }
        public string RxMac { get; set; }
        public int Power { get; set; }
        public int RSSI { get; set; }
        public bool Button { get; set; }
        public bool GSensor { get; set; }
    }
}

