﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Utility
{
    public class Common
    {
        public static DateTime ConvertToDateTime(string input)
        {
            int length;
            length = input.Length;

            if (length == 8)
                return DateTime.Parse(input.Substring(0, 4) + "/" + input.Substring(4, 2) + "/" + input.Substring(6, 2));
            else if (length == 12)
                return DateTime.Parse(input.Substring(0, 4) + "/" + input.Substring(4, 2) + "/" + input.Substring(6, 2) + " " + input.Substring(8, 2) + ":" + input.Substring(10, 2));
            else if (length == 14)
                return DateTime.Parse(input.Substring(0, 4) + "/" + input.Substring(4, 2) + "/" + input.Substring(6, 2) + " " + input.Substring(8, 2) + ":" + input.Substring(10, 2) + ":" + input.Substring(12, 2));
            else
                throw new Exception("Convert Length Error");

        }

        public static string UnicodeToCP850(string input)
        {
            Encoding utf8 = Encoding.GetEncoding("utf-8");
            Encoding CP850 = Encoding.GetEncoding("cp850");

            byte[] temp = utf8.GetBytes(input);
            byte[] temp1 = Encoding.Convert(utf8, CP850, temp);
            string result = CP850.GetString(temp1);
            return result;
        }

        public static string CP850ToUnicode(string input)
        {
            string strOutString = "";
            byte[] byteString;
            byteString = Encoding.GetEncoding("cp850").GetBytes(input);
            strOutString = Encoding.GetEncoding("utf-8").GetString(byteString);
            return strOutString;

            //Encoding utf8 = Encoding.GetEncoding("utf-8");
            //Encoding CP850 = Encoding.GetEncoding(850);

            //byte[] temp = CP850.GetBytes(input);
            //byte[] temp1 = Encoding.Convert(CP850, utf8, temp);
            //string result = utf8.GetString(temp1);
            //return result;
        }
    }
}
