﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.IO;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Text;

namespace B2B_API
{
    [ServiceContract(Namespace = "B2B_API")]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]



    public class Auth
    {
        public static string ConnectionStringB = ConfigurationManager.ConnectionStrings["B2B"].ConnectionString;
        public static string ConnectionString = ConfigurationManager.ConnectionStrings["CarpoolLab"].ConnectionString;

        [Description("取得授權")]
        [OperationContract, WebInvoke(UriTemplate = "TEST", Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]


        [Description("TEST")]
        [OperationContract, WebInvoke(UriTemplate = "TEST", Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public testclass Test()
        {
            testclass test = new testclass();
            return test;
        }

        public class testclass
        {
            public string test1 { get; set; }
            public int test2 { get; set; }
        }

    }
}
