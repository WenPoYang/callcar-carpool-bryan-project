﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MySql.Data.MySqlClient;
using System.Data;
using DbConn;
using System.Text;
using B2B_API.Share;
using Newtonsoft.Json;
using B2B_API.Company;
using CallCar.Data;
using CallCar.Tool;
using CallCar.Reservation;


namespace B2B_API.Reservations
{

    public class Reservation
    {

        public Reservation()
        {

        }

        /*
    * 使用客戶：WiFiHero 、 JourneyQ
    */
        public static OReservation CreateNewReservation(Order order, CompanyInfo company)
        {
            OReservation or = new OReservation();

            or.AddBagCnt = 0;
            or.Airport = order.airport;
            or.BaggageCnt = order.bcnt;

            if (company.CompanyEIN == "")
                or.Coupon = "WIFIHERO";
            else
                or.Coupon = "";

            or.Credit = "";
            or.EIN = "";

            if (order.flight_no == "")
            {
                or.ChooseType = "T";
                or.FlightNo = "";
            }
            else
            {
                or.ChooseType = "F";
                or.FlightNo = order.flight_no;
                or.FlightTime = DateTime.Parse(order.flight_time);
            }
            or.Invoice = "";
            or.MaxFlag = "0";

            or.PassengerCnt = order.pcnt;
            or.PassengerName = order.pname;
            or.PassengerPhone = order.pmobile;
            or.PickCartype = "0";

            //機場資料
            CallCarAddress caa = new CallCarAddress();

            if (order.airport == "TPE")
            {
                if (order.term == "T1")
                    caa.Address = "桃園國際機場 第一航廈";
                else if (order.term == "T2")
                    caa.Address = "桃園國際機場 第二航廈";
                else if (order.term == "T3")
                    caa.Address = "桃園國際機場 第三航廈";
            }
            else if (order.airport == "RMQ")
                caa.Address = "台中國際機場";
            else if (order.airport == "KHH")
                caa.Address = "高雄國際機場";
            else
                throw new Exception("ERR_NONE_AIRPORT");

            if (order.airport == "TPE")
            {
                caa.City = "桃園市";
                caa.Distinct = "大園區";
                if (order.service == "O" && order.term == "T2")
                {
                    caa.GPS = new OSpatialGPS(25.076838, 121.232336);
                    //caa.GPS.lat = 25.076838;
                    //caa.GPS.lng = 121.232336;
                }
                else if (order.service == "I" && order.term == "T2")
                {
                    caa.GPS = new OSpatialGPS(25.076838, 121.232336);
                }
                else if (order.service == "O" && order.term == "T1")
                {
                    caa.GPS = new OSpatialGPS(25.081056, 121.237585);
                }
                else if (order.service == "I" && order.term == "T1")
                {
                    caa.GPS = new OSpatialGPS(25.081056, 121.237585);
                }
            }
            else if (order.airport == "RMQ")
            {
                caa.City = "台中市";
                caa.Distinct = "沙鹿區";
                caa.GPS = new OSpatialGPS(24.264489, 120.623387);
            }
            else if (order.airport == "KHH")
            {
                caa.City = "高雄市";
                caa.Distinct = "小港區";
                caa.GPS = new OSpatialGPS(22.575861, 120.344654);
            }
            else
                throw new Exception("ERR_NONE_AIRPORT");

            //利用TGOS轉換傳入地址的資料
            CallCarAddress ca = new CallCarAddress();
            string outputstring;
            TGOS.TGOSJson tg;
            try
            {
                outputstring = TGOS.Query(order.address);
                tg = JsonConvert.DeserializeObject<TGOS.TGOSJson>(outputstring);
            }
            catch (Exception)
            {
                throw new Exception("ERR_ADD_TRANS");
            }

            if (tg.Info[0].IsSuccess != "True")
            {
                throw new Exception("ERR_ADD_TRANS");
            }
            if (tg.AddressList.Count != 1)
            {
                throw new Exception("ERR_ADD_TRANS");
            }

            ca.City = tg.AddressList[0].COUNTY;
            if (ca.City == "臺北市")
                ca.City = "台北市";

            ca.Distinct = tg.AddressList[0].TOWN;
            ca.Village = tg.AddressList[0].VILLAGE;
            ca.Address = tg.AddressList[0].FULL_ADDR;
            ca.GPS = new OSpatialGPS(tg.AddressList[0].Y, tg.AddressList[0].X);



            if (order.service == "O")
            {
                or.PickupAddress = ca.Address;
                or.PickupCity = ca.City;
                or.PickupDistinct = ca.Distinct;
                or.PickupVillage = ca.Village;
                or.PickupGPS = ca.GPS;

                or.TakeoffAddress = caa.Address;
                or.TakeoffCity = caa.City;
                or.TakeoffDistinct = caa.Distinct;
                or.TakeoffVillage = caa.Village;
                or.TakeoffGPS = caa.GPS;
            }
            else
            {
                or.TakeoffAddress = ca.Address;
                or.TakeoffCity = ca.City;
                or.TakeoffDistinct = ca.Distinct;
                or.TakeoffVillage = ca.Village;
                or.TakeoffGPS = ca.GPS;

                or.PickupAddress = caa.Address;
                or.PickupCity = caa.City;
                or.PickupDistinct = caa.Distinct;
                or.PickupVillage = caa.Village;
                or.PickupGPS = caa.GPS;
            }
            or.PreferTime = DateTime.Parse(order.take_time).ToString("HHmm");
            or.ProcessStage = "0";
            string ReservationNo;
            //BALSerialNum dSerial = new BALSerialNum(ShareParameter.ConnectionString);
            //try
            //{
            //    OSerialNum serial = dSerial.GetSerialNumObject("Reservation");
            //    ReservationNo = DateTime.Now.ToString(serial.Prefix) + dSerial.GetSeqNoString(serial);

            //}
            //catch (Exception)
            //{
            //    throw new Exception("ERR_SERIAL_WRONG");
            //}
            or.ReservationNo = "";
            or.ServeDate = DateTime.Parse(order.take_time).ToString("yyyyMMdd");
            or.ServiceType = order.service;
            or.Ternimal = order.term;
            or.UserID = company.BindingID;
            //企業戶採月結
            or.Price = 0;

            try
            {
                ReservationNo = insNewReservation(or);
                if (ReservationNo != null && ReservationNo != "")
                {
                    or.ReservationNo = ReservationNo;
                    return or;
                }
                else
                {
                    throw new Exception("ERR_GEN_ORDER");
                }
            }
            catch (Exception)
            {
                throw new Exception("ERR_GEN_ORDER");
            }
        }

        private static string insNewReservation(OReservation or)
        {
            BALReservation balr = new BALReservation(ShareParameter.ConnectionString);
            try
            {
                return balr.CreateNewReservation(or);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            //List<MySqlParameter> parms = new List<MySqlParameter>();
            //StringBuilder sb = new StringBuilder();
            //sb.Append("INSERT INTO reservation_sheet  ");
            //sb.Append("(ReservationNo,UserID, ServiceType, TakeDate, TimeSegment, SelectionType,PickCartype, FlightNo, ScheduleFlightTime,Airport,Ternimal,  ");
            //sb.Append("PickupCity, PickupDistinct,PickupVillage, PickupAddress,PickupGPS, TakeoffCity, TakeoffDistinct,TakeoffVillage, TakeoffAddress, TakeoffGPS, PassengerCnt,   ");
            //sb.Append("BaggageCnt,MaxFlag, Price, Coupon, InvoiceData,CreditData, ProcessStage,PassengerName, PassengerPhone)  ");
            //sb.Append("VALUES (@ReservationNo,@UserID, @ServiceType, @TakeDate, @TimeSegment, @SelectionType, @PickCartype,@FlightNo, @ScheduleFlightTime,@Airport,@Ternimal,  ");
            //sb.Append("@PickupCity, @PickupDistinct,@PickupVillage, @PickupAddress,GeomFromText(@PickupGPS), @TakeoffCity, @TakeoffDistinct, @TakeoffVillage, @TakeoffAddress, GeomFromText(@TakeoffGPS), @PassengerCnt,   ");
            //sb.Append("@BaggageCnt, @MaxFlag,@Price, @Coupon, @InvoiceData,@CreditData, @ProcessStage,@PassengerName, @PassengerPhone)  ");

            //using (dbConnectionMySQL conn = new dbConnectionMySQL(ShareParameter.ConnectionString))
            //{
            //    parms.Add(new MySqlParameter("@ReservationNo", or.ReservationNo));
            //    parms.Add(new MySqlParameter("@UserID", or.UserID));
            //    parms.Add(new MySqlParameter("@ServiceType", or.ServiceType));
            //    parms.Add(new MySqlParameter("@TakeDate", or.ServeDate));
            //    parms.Add(new MySqlParameter("@TimeSegment", or.PreferTime));
            //    parms.Add(new MySqlParameter("@SelectionType", or.ChooseType));

            //    if (or.ChooseType == "F")
            //    {
            //        parms.Add(new MySqlParameter("@FlightNo", or.FlightNo));
            //        parms.Add(new MySqlParameter("@ScheduleFlightTime", or.FlightTime));
            //    }
            //    else
            //    {
            //        parms.Add(new MySqlParameter("@FlightNo", ""));
            //        parms.Add(new MySqlParameter("@ScheduleFlightTime", null));
            //    }
            //    parms.Add(new MySqlParameter("@Airport", or.Airport));
            //    parms.Add(new MySqlParameter("@Ternimal", or.Ternimal));
            //    parms.Add(new MySqlParameter("@PickCartype", or.PickCartype));
            //    parms.Add(new MySqlParameter("@PickupCity", or.PickupCity));
            //    parms.Add(new MySqlParameter("@PickupDistinct", or.PickupDistinct));
            //    parms.Add(new MySqlParameter("@PickupVillage", or.PickupVillage));
            //    parms.Add(new MySqlParameter("@PickupAddress", or.PickupAddress));
            //    parms.Add(new MySqlParameter("@PickupGPS", "Point(" + or.PickupGPS.GetLng().ToString() + " " + or.PickupGPS.GetLat().ToString() + ")"));
            //    parms.Add(new MySqlParameter("@TakeoffCity", or.TakeoffCity));
            //    parms.Add(new MySqlParameter("@TakeoffDistinct", or.TakeoffDistinct));
            //    parms.Add(new MySqlParameter("@TakeoffVillage", or.TakeoffVillage));
            //    parms.Add(new MySqlParameter("@TakeoffAddress", or.TakeoffAddress));
            //    parms.Add(new MySqlParameter("@TakeoffGPS", "Point(" + or.TakeoffGPS.GetLng().ToString() + " " + or.TakeoffGPS.GetLat().ToString() + ")"));
            //    parms.Add(new MySqlParameter("@PassengerCnt", or.PassengerCnt));
            //    parms.Add(new MySqlParameter("@BaggageCnt", or.BaggageCnt));
            //    parms.Add(new MySqlParameter("@MaxFlag", or.MaxFlag));
            //    parms.Add(new MySqlParameter("@Coupon", or.Coupon));
            //    parms.Add(new MySqlParameter("@InvoiceData", or.Invoice));
            //    parms.Add(new MySqlParameter("@CreditData", or.Credit));
            //    parms.Add(new MySqlParameter("@ProcessStage", "0"));
            //    parms.Add(new MySqlParameter("@Price", or.Price));
            //    parms.Add(new MySqlParameter("@PassengerName", or.PassengerName));
            //    parms.Add(new MySqlParameter("@PassengerPhone", or.PassengerPhone));


            //try
            //{
            //    if (conn.executeInsertQuery(sb.ToString(), parms.ToArray()) != 1)
            //        return false;
            //    else
            //        return true;

            //}
            //catch (MySqlException ex)
            //{

            //    throw new Exception(ex.Message);
            //}
            //catch (Exception ex)
            //{

            //    throw new Exception(ex.Message);
            //}


        }

        public static bool CancelReservation(string ReservationNo, string UpdType)
        {
            List<MySqlParameter> parms = new List<MySqlParameter>();
            StringBuilder sb = new StringBuilder();
            sb.Append("Update reservation_sheet  Set ProcessStage = @type Where ReservationNo = @no");
            parms.Add(new MySqlParameter("@type", UpdType));
            parms.Add(new MySqlParameter("@no", ReservationNo));

            using (dbConnectionMySQL conn = new dbConnectionMySQL(ShareParameter.ConnectionString))
            {
                try
                {
                    if (conn.executeUpdateQuery(sb.ToString(), parms.ToArray()) != 1)
                        return false;
                    else
                        return true;

                }
                catch (MySqlException ex)
                {

                    throw new Exception(ex.Message);
                }
                catch (Exception ex)
                {

                    throw new Exception(ex.Message);
                }
            }
        }

        public static List<OReservation> QryCompanyReservation(string ReservationNo, string Date, string ServiceType,int CompanyID)
        {
            List<MySqlParameter> parms = new List<MySqlParameter>();
            StringBuilder sb = new StringBuilder();
            List<OReservation> or = new List<OReservation>();
            sb.Append("Select  ReservationNo,ServiceType,CarpoolFlag,TakeDate,TimeSegment,FlightNo,ScheduleFlightTime,Airport,Ternimal,PickupAddress,TakeoffAddress,PassengerCnt,BaggageCnt,PassengerName,PassengerPhone ");
            sb.Append("From reservation_sheet ");
            sb.Append("Where  UserID = @id ");
            parms.Add(new MySqlParameter("@id", CompanyID));

            if (Date == null || Date == "" || Date == string.Empty)
                sb.Append(" AND TakeDate >=  date_format(now(),'%Y%m%d') ");
            else
            {
                sb.Append(" AND TakeDate = @date");
                parms.Add(new MySqlParameter("@date", Date));
            }

            if (ReservationNo != null && ReservationNo != "" && ReservationNo != string.Empty)
            {
                sb.Append(" AND ReservationNo = @no");
                parms.Add(new MySqlParameter("@no", ReservationNo));
            }

            if (ServiceType != null && ServiceType != "" && ServiceType != string.Empty)
            {
                sb.Append(" AND ServiceType = @type");
                parms.Add(new MySqlParameter("@type", ServiceType));
            }
            DataTable dt;
            using (dbConnectionMySQL conn = new dbConnectionMySQL(ShareParameter.ConnectionString))
            {
                dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
            }

            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    OReservation o = new OReservation();
                    o.ReservationNo = Convert.ToString(dt.Rows[i]["ReservationNo"]);
                    o.ServiceType = Convert.ToString(dt.Rows[i]["ServiceType"]);
                    o.CarpoolFlag = Convert.ToString(dt.Rows[i]["CarpoolFlag"]);
                    o.ServeDate = Convert.ToString(dt.Rows[i]["TakeDate"]);
                    o.PreferTime = Convert.ToString(dt.Rows[i]["TakeDate"]);
                    o.FlightNo = Convert.ToString(dt.Rows[i]["TakeDate"]);
                    if (o.FlightNo != null && o.FlightNo != "")
                        o.FlightTime = Convert.ToDateTime(dt.Rows[i]["ScheduleFlightTime"]);

                    o.Airport = Convert.ToString(dt.Rows[i]["Airport"]);
                    o.Ternimal = Convert.ToString(dt.Rows[i]["Ternimal"]);
                    o.PickupAddress = Convert.ToString(dt.Rows[i]["PickupAddress"]);
                    o.TakeoffAddress = Convert.ToString(dt.Rows[i]["TakeoffAddress"]);
                    o.PassengerCnt = Convert.ToInt32(dt.Rows[i]["PassengerCnt"]);
                    o.BaggageCnt = Convert.ToInt32(dt.Rows[i]["BaggageCnt"]);
                    o.PassengerName = Convert.ToString(dt.Rows[i]["PassengerName"]);
                    o.PassengerPhone = Convert.ToString(dt.Rows[i]["PassengerPhone"]);
                    or.Add(o);
                }

            }
            return or;
        }
    }
}