﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.IO;
using System.Security.Cryptography;
using MySql.Data.MySqlClient;
using System.Data;
using DbConn;
using System.Net;
using System.Xml;
using B2B_API.Company;
using System.Configuration;
using System.Transactions;
using CallCar.Data;
using CallCar.Tool;

namespace B2B_API.Share
{
    public class ShareParameter
    {
        public static string ConnectionStringB = ConfigurationManager.ConnectionStrings["B2B"].ConnectionString;
        public static string ConnectionString = ConfigurationManager.ConnectionStrings["CarpoolLab"].ConnectionString;
    }
    #region 共用interface
    public class ShareFunc
    {
        /// <summary>
        ///  檢查Token是否正確
        /// </summary>
        /// <param name="token"></param>
        /// <returns>如果有效且正確 則回傳公司資料 否則回傳 null</returns>
        public static CompanyInfo VarifyToken(string token)
        {
            //先檢查token時效

            if (TokenInfo.isTokenAlive(token))
            {
                CompanyInfo company = CompanyInfo.GetCompanyByToken(token);
                return company;
            }
            else
                throw new Exception("ERR_TOKEN_EXPIRED");
        }
    }
    #endregion

    #region Structure
    public class ReturnMsg
    {
        public string Status { get; set; }
        public object Msg { get; set; }
    }

    /*
             {
                  "order": {
                    "no":"xxxxxxxxxx",
                    "service":"O",
                    "share":"1",
                    "take_time": "2017/12/31 17:30",
                    "flight_no":"CI4",
                    "flight_time":"2017/12/31 20:00",
                    "airport":"TPE",
                    "term":"T1",
                    "address":"台北市內湖區瑞光路76巷43號",
                    "pcnt":1,
                    "bcnt":1,
                    "pname":"XXX",
                    "pmobile":"09xxxxxxxx"
                  },
                  "token": "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"                  
            }
            */
    public class Order
    {
        public string no { get; set; }
        public string service { get; set; }
        public string share { get; set; }
        public string take_time { get; set; }
        public string flight_no { get; set; }
        public string flight_time { get; set; }
        public string airport { get; set; }
        public string term { get; set; }
        public string address { get; set; }
        public int pcnt { get; set; }
        public int bcnt { get; set; }
        public string pname { get; set; }
        public string pmobile { get; set; }
    }



    /// <summary>
    /// 取號物件
    /// </summary>


    public class CallCarAddress
    {
        public string City { get; set; }
        public string Distinct { get; set; }
        public string Village { get; set; }
        public string Address { get; set; }
        public OSpatialGPS GPS { get; set; }
    }

    public class TokenInfo
    {
        public static string RenewToken(string CompanyAccount)
        {
            Int32 unixTimestamp = (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
            string token = MD5Cryptography.Encrypt(unixTimestamp.ToString());
            List<MySqlParameter> parms = new List<MySqlParameter>();
            StringBuilder sb = new StringBuilder();

            sb.Append("INSERT INTO token_status (Token, BeginTime, EndTime) VALUES (@token, NOW(),DATE_ADD(NOW(),INTERVAL  10 MINUTE))");
            parms.Add(new MySqlParameter("@token", token));
            using (TransactionScope scope = new TransactionScope())
            {
                try
                {

                    using (dbConnectionMySQL conn = new dbConnectionMySQL(ShareParameter.ConnectionStringB))
                    {
                        if (conn.executeInsertQuery(sb.ToString(), parms.ToArray()) <= 0)
                            return "";
                        else
                        {
                            sb.Length = 0;
                            parms.Clear();
                            sb.Append("UPDATE business_info	SET TokenValue=@token	 WHERE CompanyEIN = @acc");
                            parms.Add(new MySqlParameter("@token", token));
                            parms.Add(new MySqlParameter("@acc", CompanyAccount));

                            if (conn.executeUpdateQuery(sb.ToString(), parms.ToArray()) == 1)
                            {
                                scope.Complete();
                                return token;
                            }
                            else
                                return "";
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
        public static bool isTokenAlive(string token)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("select * from token_status where token = @token and now() between  BeginTime and EndTime");
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("@token", token));

            using (dbConnectionMySQL conn = new dbConnectionMySQL(ShareParameter.ConnectionStringB))
            {
                if (conn.executeSelectQuery(sb.ToString(), parms.ToArray()).Rows.Count > 0)
                    return true;
                else
                    return false;
            }
        }

        public int SeqNo { get; set; }
        public string Token { get; set; }
        public DateTime BeginTime { get; set; }
        public DateTime EndTime { get; set; }
    }
    #region TGOS
    public class TGOS
    {
        public static string Query(string aAddress)
        {
            //所要查詢的門牌地址
            //string aAddress = "台北市內湖區瑞光路76巷43號";
            //坐標系統(SRS)EPSG:4326(WGS84)國際通用, EPSG:3825 (TWD97TM119) 澎湖及金馬適用,EPSG:3826 (TWD97TM121) 台灣地區適用,EPSG:3827 (TWD67TM119) 澎湖及金馬適用,EPSG:3828 (TWD67TM121) 台灣地區適用
            string aSRS = "EPSG:4326";
            //0:最近門牌號機制,1:單雙號機制,2:[最近門牌號機制]+[單雙號機制]
            int aFuzzyType = 0;
            //回傳的資料格式，允許傳入的代碼為：JSON、XML
            string aResultDataType = "JSON";
            //模糊比對回傳門牌號的許可誤差範圍，輸入格式為正整數，如輸入 0 則代表不限制誤差範圍
            int aFuzzyBuffer = 0;
            //是否只進行完全比對，允許傳入的值為：true、false，如輸入 true ，模糊比對機制將不被使用
            string aIsOnlyFullMatch = "false";
            //是否鎖定縣市，允許傳入的值為：true、false，如輸入 true ，則代表查詢結果中的 [縣市] 要與所輸入的門牌地址中的 [縣市] 完全相同
            string aIsLockCounty = "false";
            //是否鎖定鄉鎮市區，允許傳入的值為：true、false，如輸入 true ，則代表查詢結果中的 [鄉鎮市區] 要與所輸入的門牌地址中的 [鄉鎮市區] 完全相同
            string aIsLockTown = "false";
            //是否鎖定村里，允許傳入的值為：true、false，如輸入 true ，則代表查詢結果中的 [村里] 要與所輸入的門牌地址中的 [村里] 完全相同
            string aIsLockVillage = "false";
            //是否鎖定路段，允許傳入的值為：true、false，如輸入 true ，則代表查詢結果中的 [路段] 要與所輸入的門牌地址中的 [路段] 完全相同
            string aIsLockRoadSection = "false";
            //是否鎖定巷，允許傳入的值為：true、false，如輸入 true ，則代表查詢結果中的 [巷] 要與所輸入的門牌地址中的 [巷] 完全相同
            string aIsLockLane = "false";
            //是否鎖定弄，允許傳入的值為：true、false，如輸入 true ，則代表查詢結果中的 [弄] 要與所輸入的門牌地址中的 [弄] 完全相同
            string aIsLockAlley = "false";
            //是否鎖定地區，允許傳入的值為：true、fals，如輸入 true ，則代表查詢結果中的 [地區] 要與所輸入的門牌地址中的 [地區] 完全相同
            string aIsLockArea = "false";
            //號之、之號是否視為相同，允許傳入的值為：true、false
            string aIsSameNumber_SubNumber = "false";
            //TGOS_Query_Addr_Url WebService網址
            string aCanIgnoreVillage = "true";
            //找不時是否可忽略村里，允許傳入的值為：true、false
            string aCanIgnoreNeighborhood = "true";
            //找不時是否可忽略鄰，允許傳入的值為：true、false
            int aReturnMaxCount = 0;
            //如為多筆時，限制回傳最大筆數，輸入格式為正整數，如輸入 0 則代表不限制回傳筆數





            //使用string.Format給予參數設定
            string param = @"oAPPId={0}&oAPIKey={1}&oAddress={2}&oSRS={3}&oFuzzyType={4}
                         &oResultDataType={5}&oFuzzyBuffer={6}&oIsOnlyFullMatch={7}&oIsLockCounty={8}
                         &oIsLockTown={9}&oIsLockVillage={10}&oIsLockRoadSection={11}&oIsLockLane={12}
                         &oIsLockAlley={13}&oIsLockArea={14}&oIsSameNumber_SubNumber={15}&oCanIgnoreVillage={16}&oCanIgnoreNeighborhood={17}&oReturnMaxCount={18}";

            //給予需要的參數
            param = string.Format(param,

                  //應用程式識別碼(APPId) 實由使用者向TGOS申請
                  QueryAddr.QueryAddraAppId,
                  //應用程式介接驗證碼(APIKey)由使用者向TGOS申請
                  QueryAddr.QueryAddrAPIKey,
                  aAddress,
                  aSRS,
                  aFuzzyType,
                  aResultDataType,
                  aFuzzyBuffer,
                  aIsOnlyFullMatch,
                  aIsLockCounty,
                  aIsLockTown,
                  aIsLockVillage,
                  aIsLockRoadSection,
                  aIsLockLane,
                  aIsLockAlley,
                  aIsLockArea,
                  aIsSameNumber_SubNumber,
                  aCanIgnoreVillage,
                  aCanIgnoreNeighborhood,
                  aReturnMaxCount
              );
            string Outputstring = string.Empty;
            Outputstring = QueryAddr.QueryAddrResult(param, QueryAddr.Query_Addr_Url + "/QueryAddr?", Outputstring);
            return Outputstring;
        }

        public class TGOSJson
        {
            public List<Info> Info { get; set; }
            public List<AddressList> AddressList { get; set; }
            public string Status { get; set; }
        }


        public class Info
        {
            public string IsSuccess { get; set; }
            public string InAddress { get; set; }
            public string InSRS { get; set; }
            public string InFuzzyType { get; set; }
            public string InFuzzyBuffer { get; set; }
            public string InIsOnlyFullMatch { get; set; }
            public string InIsLockCounty { get; set; }
            public string InIsLockTown { get; set; }
            public string InIsLockVillage { get; set; }
            public string InIsLockRoadSection { get; set; }
            public string InIsLockLane { get; set; }
            public string InIsLockAlley { get; set; }
            public string InIsLockArea { get; set; }
            public string InIsSameNumber_SubNumber { get; set; }
            public string InCanIgnoreVillage { get; set; }
            public string InCanIgnoreNeighborhood { get; set; }
            public string InReturnMaxCount { get; set; }
            public string OutTotal { get; set; }
            public string OutMatchType { get; set; }
            public string OutMatchCode { get; set; }
            public string OutTraceInfo { get; set; }
        }

        public class AddressList
        {
            public string FULL_ADDR { get; set; }
            public string COUNTY { get; set; }
            public string TOWN { get; set; }
            public string VILLAGE { get; set; }
            public string NEIGHBORHOOD { get; set; }
            public string ROAD { get; set; }
            public string SECTION { get; set; }
            public string LANE { get; set; }
            public string ALLEY { get; set; }
            public string SUB_ALLEY { get; set; }
            public string TONG { get; set; }
            public string NUMBER { get; set; }
            public double X { get; set; }
            public double Y { get; set; }
        }


        public class QueryAddr
        {

            public QueryAddr()
            {
                //
                // TODO: Add constructor logic here
                //
            }

            //TGOSQueryAddrAppId
            public static string QueryAddraAppId
            {
                get
                {
                    try
                    {
                        //return System.Web.Configuration.WebConfigurationManager.AppSettings["TGOSQueryAddrAppId"];
                        return "XmPSzyEB0Dmv9dUWazaiYl8hucwGr4SEnIg574iGvlXW19O92mjXUg==";

                    }
                    catch
                    {
                        return "";
                    }

                }
            }

            //TGOSQueryAddrAPIKey
            public static string QueryAddrAPIKey
            {
                get
                {
                    try
                    {
                        //return System.Web.Configuration.WebConfigurationManager.AppSettings["TGOSQueryAddrAPIKey"];
                        return "cGEErDNy5yNr14zbsE/4GSfiGP5i3PuZY28uEWtsx7RQEyzkRQy69/qsYhLG120jRQbxVeuhh9bTMq6W2AVjAipPQK3MDTYMXYvmMZ4qX2gEF+5v8kWSwx4v9dPo7qMyL0splHzmDDWP4UTNZW2QnIQOFbKRELiwAmDgeCJ59E0zZSwCU56J4B+yAOLex27S1lGq8sosfm07uPve09my0CmRVpca1vKuMvxg8ePF1pZk7vhAOzMyqT6D6ZKEtKeB/vogdhEXlyachwqUJ+nvaZtyfiGPJ0ZfY7REwtunTZG9unbKzOIivfBweZYA7X5WDB6Po3aHPAxZyQSYcQOZncFX6uJ38SZCejrISmTfGL4=";
                    }
                    catch
                    {
                        return "";
                    }
                }
            }

            //TGOS_Query_Addr_Url
            public static string Query_Addr_Url
            {
                get
                {
                    try
                    {
                        //return System.Web.Configuration.WebConfigurationManager.AppSettings["TGOS_Query_Addr_Url"];
                        return "http://addr.tgos.nat.gov.tw/addrws/v30/QueryAddr.asmx";
                    }
                    catch
                    {
                        return "";
                    }
                }
            }

            #region 目前用不到    
            /*
            ////////////////////////////////////////////////////////////////////

            //TGOSQueryGeoAddrAppId
            public static string QueryGeoAddrAppId {
                get {
                    try {
                        return System.Web.Configuration.WebConfigurationManager.AppSettings["TGOSQueryGeoAddrAppId"];

                    } catch {
                        return "";
                    }

                }
            }

            //TGOSQueryGeoAddrAPIKey
            public static string QueryGeoAddrAPIKey {
                get {
                    try {
                        return System.Web.Configuration.WebConfigurationManager.AppSettings["TGOSQueryGeoAddrAPIKey"];

                    } catch {
                        return "";
                    }
                }
            }

            //TGOS_Query_GeoAddr_Url
            public static string Query_GeoAddr_Url {
                get {
                    try {
                        return System.Web.Configuration.WebConfigurationManager.AppSettings["TGOS_Query_GeoAddr_Url"];

                    } catch {
                        return "";
                    }
                }
            }

            ////////////////////////////////////////////////////////////////////


            //TGOSQueryAddrListAppId
            public static string QueryAddrListAppId {
                get {
                    try {
                        return System.Web.Configuration.WebConfigurationManager.AppSettings["TGOSQueryAddrListAppId"];

                    } catch {
                        return "";
                    }

                }
            }

            //TGOSQueryAddrListAPIKey
            public static string QueryAddrListAPIKey {
                get {
                    try {
                        return System.Web.Configuration.WebConfigurationManager.AppSettings["TGOSQueryAddrListAPIKey"];

                    } catch {
                        return "";
                    }
                }
            }

            //TGOS_Query_AddrList_Url
            public static string Query_AddrList_Url {
                get {
                    try {
                        return System.Web.Configuration.WebConfigurationManager.AppSettings["TGOS_Query_AddrList_Url"];

                    } catch {
                        return "";
                    }
                }
            }
            */
            #endregion
            ////////////////////////////////////////////////////////////////////

            public static string QueryAddrResult(string oParam, string oUrl, string oLblResult)
            {

                HttpWebRequest req = (HttpWebRequest)HttpWebRequest.Create(oUrl);
                oParam = oParam.Replace("+", "%2B");
                //參數以UTF8格式轉成Byte[]之後才能夠寫入資料流
                byte[] PostData = Encoding.UTF8.GetBytes(oParam);
                //取得或設定要求的方法
                req.Method = "POST";
                //取得或設定 Content-type HTTP 標頭的值。 輸入資料被編碼為名稱/值對 
                //當Method為get時候，瀏覽器用x-www-form-urlencoded的編碼方式把form數據轉換成一個字串（name1=value1&name2=value2...）
                //然後把這個字串append到url後面，用?分割，加載這個新的url。
                //當Method為post時候，瀏覽器把form數據封裝到http body中，
                //然後發送到server。如果沒有type=file的控件，用默認的application/x-www-form-urlencoded就可以了。
                req.ContentType = "application/x-www-form-urlencoded";

                //取得或設定 Content-length HTTP 標頭(資訊)
                req.ContentLength = PostData.Length;
                //client對sever請求提出取得資料流 
                //using清理非托管不受GC控制的資源
                using (Stream reqStream = req.GetRequestStream())
                {
                    //把參數資料寫入資料流
                    reqStream.Write(PostData, 0, PostData.Length);
                }
                //取server端資料使用 request.GetResponse() 方法時，
                //HttpWebRequest即會將資料傳送到指定的網址做處理，
                //處理完成後會依照Server的動作看是回傳資料或是重新指向他頁
                using (WebResponse wr = req.GetResponse())
                {
                    //取得sever端資料流放入myStream
                    //一般回傳'所需要的資料要用 HttpWebResponse類別 來接收Response的內容，
                    //取得Response後在使用 StreamReader類別 來讀取 response.GetResponseStream() 的資料流內容
                    using (Stream myStream = wr.GetResponseStream())
                    {
                        //new一個新的資料讀取器,從myStream取得資料
                        //清理非托管不受GC控制的資源
                        using (StreamReader myStreamReader = new StreamReader(myStream))
                        {

                            ////對讀取出來的JSON格式資料進行加密
                            //string Json = HttpUtility.HtmlEncode (myStreamReader.ReadToEnd ( ));
                            ////對讀取出來的JSON格式資料進行解密
                            //Json = HttpContext.Current.Server.HtmlDecode (Json);
                            ////XmlDocument讀取XML
                            //XmlDocument aXmlDocument = new XmlDocument ( );
                            //aXmlDocument.LoadXml (Json);

                            ////使用 StreamReader 來從標準文字檔讀取資料。
                            ////HttpUtility.HtmlEncode()將字串轉換為 HTML 編碼的字串。
                            //return oLblResult = aXmlDocument.DocumentElement.InnerText;

                            string responseFromServer = myStreamReader.ReadToEnd();
                            XmlDocument aXmlDocument = new XmlDocument();
                            aXmlDocument.LoadXml(responseFromServer);
                            return aXmlDocument.DocumentElement.InnerText;
                        }
                    }
                }

            }

            #region 目前用不到
            /*
            public static string GeoQueryAddrResult ( string oParam, string oUrl, string oLblResult ) {



                HttpWebRequest req = (HttpWebRequest)HttpWebRequest.Create (oUrl);
                oParam = oParam.Replace ("+", "%2B");
                byte[] PostData = Encoding.UTF8.GetBytes (oParam);

                req.Method = "POST";//取得或設定要求的方法
                req.ContentType = "application/x-www-form-urlencoded";


                req.ContentLength = PostData.Length;//取得或設定 Content-length HTTP 標頭(資訊)

                using ( Stream reqStream = req.GetRequestStream ( ) ) {
                    //client對sever請求提出取得資料流 
                    reqStream.Write (PostData, 0, PostData.Length);//把參數資料寫入資料流
                }//清理非托管不受GC控制的资源

                using ( WebResponse wr = req.GetResponse ( ) ) {

                    using ( Stream myStream = wr.GetResponseStream ( ) ) {

                        using ( StreamReader myStreamReader = new StreamReader (myStream) )//new一個新的資料讀取器,從myStream取得資料
                        {

                            //LblResultDataType.Text = aResultDataType;
                            //使用 StreamReader 來從標準文字檔讀取資料。
                            string Json = myStreamReader.ReadToEnd ( );
                            ////對讀取出來的JSON格式資料進行解密
                            Json = HttpUtility.HtmlDecode (Json);



                            return oLblResult = HttpUtility.HtmlEncode (Json);



                            //return oLblResult = HttpUtility.HtmlEncode(Json);//使用 StreamReader 來從標準文字檔讀取資料。
                            //HttpUtility.HtmlEncode()將字串轉換為 HTML 編碼的字串。

                        }
                    }
                }//清理非托管不受GC控制的资源

            }

            public static string GetAddrListResult ( string oparam, string oUrl ) {
                HttpWebRequest req = (HttpWebRequest)HttpWebRequest.Create (oUrl);
                oparam = oparam.Replace ("+", "%2B");
                byte[] PostData = Encoding.UTF8.GetBytes (oparam);

                req.Method = "POST";//取得或設定要求的方法
                req.ContentType = "application/x-www-form-urlencoded";


                req.ContentLength = PostData.Length;//取得或設定 Content-length HTTP 標頭(資訊)

                using ( Stream reqStream = req.GetRequestStream ( ) ) {
                    //client對sever請求提出取得資料流 
                    reqStream.Write (PostData, 0, PostData.Length);//把參數資料寫入資料流
                }//清理非托管不受GC控制的资源

                using ( WebResponse wr = req.GetResponse ( ) ) {

                    using ( Stream myStream = wr.GetResponseStream ( ) ) {

                        using ( StreamReader myStreamReader = new StreamReader (myStream) )//new一個新的資料讀取器,從myStream取得資料
                        {
                            //使用 StreamReader 來從標準文字檔讀取資料。
                            string Json = myStreamReader.ReadToEnd ( );
                            ////對讀取出來的JSON格式資料進行解密


                            ////XmlDocument讀取XML
                            XmlDocument aXmlDocument = new XmlDocument ( );
                            aXmlDocument.LoadXml (Json);

                            return aXmlDocument.InnerText;


                        }
                    }
                }//清理非托管不受GC控制的资源

            }
            */
            #endregion
        }
    }


    #endregion

    #region Google Map Structure
    /* 
     * Place
     */
    public class AddressComponent
    {
        public string long_name { get; set; }
        public string short_name { get; set; }
        public List<string> types { get; set; }
    }

    public class Location
    {
        public double lat { get; set; }
        public double lng { get; set; }
    }

    public class Northeast
    {
        public double lat { get; set; }
        public double lng { get; set; }
    }

    public class Southwest
    {
        public double lat { get; set; }
        public double lng { get; set; }
    }

    public class Viewport
    {
        public Northeast northeast { get; set; }
        public Southwest southwest { get; set; }
    }

    public class Geometry
    {
        public Location location { get; set; }
        public Viewport viewport { get; set; }
    }

    public class PlaceResult
    {
        public List<AddressComponent> address_components { get; set; }
        public string adr_address { get; set; }
        public string formatted_address { get; set; }
        public Geometry geometry { get; set; }
        public string icon { get; set; }
        public string id { get; set; }
        public string name { get; set; }
        public string place_id { get; set; }
        public string reference { get; set; }
        public string scope { get; set; }
        public List<string> types { get; set; }
        public string url { get; set; }
        public int utc_offset { get; set; }
    }

    public class GooglePlaceInfo
    {
        public List<object> html_attributions { get; set; }
        public PlaceResult result { get; set; }
        public string status { get; set; }
    }


    /*
     * Map
     */
    public class MapResult
    {
        public string formatted_address { get; set; }
        public Geometry geometry { get; set; }
        public string icon { get; set; }
        public string id { get; set; }
        public string name { get; set; }
        public string place_id { get; set; }
        public string reference { get; set; }
        public List<string> types { get; set; }
    }

    public class GoogleMapInfo
    {
        public List<object> html_attributions { get; set; }
        public List<MapResult> results { get; set; }
        public string status { get; set; }
    }

    #endregion
    #endregion
}