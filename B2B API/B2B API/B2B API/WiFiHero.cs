﻿using System;
using System.ComponentModel;
using System.IO;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using B2B_API.Share;
using B2B_API.Reservations;
using CallCar.Reservation;
using CallCar.Data;
using CallCar.Tool;
using System.Collections.Generic;

namespace B2B_API
{
    [ServiceContract(Namespace = "B2B_API")]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
    public class WiFiHero
    {

        [Description("新增訂單")]
        [OperationContract, WebInvoke(UriTemplate = "nOrder", Method = "POST", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        public ReturnMsg CreateOrder(Stream input)
        {

            string data = new StreamReader(input).ReadToEnd();
            ReturnMsg msg = new ReturnMsg();
            string Token, CryptData;
            Order order;

            try
            {
                JObject jo = JsonConvert.DeserializeObject<JObject>(data);
                Token = Convert.ToString(jo["token"]);
                CryptData = Convert.ToString(jo["data"]);
            }
            catch (Exception)
            {
                msg.Status = "ERROR";
                msg.Msg = "ERR_DATA_ERROR";
                return msg;
            }
            Company.CompanyInfo company = new Company.CompanyInfo();
            try
            {
                company = ShareFunc.VarifyToken(Token);
            }
            catch (Exception)
            {
                msg.Status = "ERROR";
                msg.Msg = "ERR_TOKEN_ERROR";
                return msg;
            }
            if (company != null)
            {
                AESCryptography aes = new AESCryptography(company.CompanyHash, company.CompanyIV);
                CryptData = aes.Decrypt(CryptData);
                order = JsonConvert.DeserializeObject<Order>(CryptData);

                try
                {
                    OReservation or = Reservation.CreateNewReservation(order, company);
                    msg.Status = "SUCCESS";
                    order.no = or.ReservationNo;
                    msg.Msg = aes.Encrypt(JsonConvert.SerializeObject(order));
                    return msg;
                }
                catch (Exception ex)
                {
                    msg.Status = "ERROR";
                    msg.Msg = ex.Message;
                    return msg;
                }
            }
            else
            {
                msg.Status = "ERROR";
                msg.Msg = "ERR_ERROR_TOKEN";
                return msg;
            }

        }

        [Description("取消訂單")]
        [OperationContract, WebInvoke(UriTemplate = "dOrder", Method = "POST", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        public ReturnMsg CancelOrder(Stream input)
        {
            string data = new StreamReader(input).ReadToEnd();
            ReturnMsg msg = new ReturnMsg();
            string Token, ReservationNo, CryptData, UpdType;

            try
            {
                JObject jo = JsonConvert.DeserializeObject<JObject>(data);
                Token = Convert.ToString(jo["token"]);
                CryptData = Convert.ToString(jo["data"]);
            }
            catch (Exception)
            {
                msg.Status = "ERROR";
                msg.Msg = "ERR_DATA_ERROR";
                return msg;
            }
            Company.CompanyInfo company = new Company.CompanyInfo();
            try
            {
                company = ShareFunc.VarifyToken(Token);
            }
            catch (Exception)
            {
                msg.Status = "ERROR";
                msg.Msg = "ERR_TOKEN_ERROR";
                return msg;
            }
            if (company != null)
            {
                AESCryptography aes = new AESCryptography(company.CompanyHash, company.CompanyIV);
                ReservationNo = aes.Decrypt(CryptData);

                BALReservation balr = new BALReservation(ShareParameter.ConnectionString);
                OReservation or = balr.GetReservation(ReservationNo);
                DateTime Now = DateTime.Now;

                if (DateTime.Compare(CallCar.Tool.General.ConvertToDateTime(or.ServeDate + or.PreferTime, 12), Now.AddHours(24)) < 0)
                {
                    msg.Status = "ERROR";
                    msg.Msg = "ERR_BEYOND_DEADLINE";
                    return msg;
                }
                else
                    UpdType = "X";

                try
                {
                    if (Reservation.CancelReservation(ReservationNo, UpdType))
                    {
                        msg.Status = "SUCCESS";
                        msg.Msg = "";
                        return msg;
                    }
                    else
                    {
                        msg.Status = "ERROR";
                        msg.Msg = "ERR_CANCEL_FAIL";
                        return msg;
                    }
                }
                catch (Exception)
                {
                    msg.Status = "ERROR";
                    msg.Msg = "ERR_CANCEL_FAIL";
                    return msg;
                }
            }
            else
            {
                msg.Status = "ERROR";
                msg.Msg = "ERR_TOKEN_ERROR";
                return msg;
            }
        }

        [Description("訂單查詢")]
        [OperationContract, WebInvoke(UriTemplate = "qOrder", Method = "POST", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        public ReturnMsg QueryOrder(Stream input)
        {
            string data = new StreamReader(input).ReadToEnd();
            ReturnMsg msg = new ReturnMsg();
            string Token, ReservationNo, QueryDate, ServiceType;
            JObject jo;
            try
            {
                jo = JsonConvert.DeserializeObject<JObject>(data);
                Token = Convert.ToString(jo["token"]);
                data = Convert.ToString(jo["data"]);
            }
            catch (Exception)
            {
                msg.Status = "ERROR";
                msg.Msg = "ERR_DATA_ERROR";
                return msg;
            }
            Company.CompanyInfo company = new Company.CompanyInfo();
            try
            {
                company = ShareFunc.VarifyToken(Token);
            }
            catch (Exception)
            {
                msg.Status = "ERROR";
                msg.Msg = "ERR_TOKEN_ERROR";
                return msg;
            }
            if (company != null)
            {
                //資料解密
                CallCar.Tool.AESCryptography aes = new CallCar.Tool.AESCryptography(company.CompanyHash, company.CompanyIV);
                string Querystring = aes.Decrypt(data);

                jo = JsonConvert.DeserializeObject<JObject>(Querystring);
                ReservationNo = Convert.ToString(jo["no"]);
                QueryDate = Convert.ToString(jo["date"]);
                ServiceType = Convert.ToString(jo["stype"]);


                try
                {
                    List<Order> Lists = new List<Order>();
                    List<OReservation> ors = Reservation.QryCompanyReservation(ReservationNo, QueryDate, ServiceType, company.BindingID);
                    foreach (OReservation o in ors)
                    {
                        Order or = new Order();
                        if (ServiceType == "O")
                            or.address = o.PickupAddress;
                        else
                            or.address = o.TakeoffAddress;

                        or.airport = o.Airport;
                        or.bcnt = o.BaggageCnt;
                        if (o.FlightNo != null && o.FlightNo != "")
                        {
                            or.flight_no = o.FlightNo;
                            or.flight_time = o.FlightTime.ToString("yyyy/MM/dd HH:mm");
                        }
                        else
                        {
                            or.flight_no = "";
                            or.flight_time = "";
                        }
                        or.no = o.ReservationNo;
                        or.pcnt = o.PassengerCnt;
                        or.pmobile = o.PassengerPhone;
                        or.pname = o.PassengerName;
                        or.service = o.ServiceType;
                        if (o.CarpoolFlag == "S")
                            or.share = "1";
                        else
                            or.share = "0";
                        or.take_time = CallCar.Tool.General.ConvertToDateTime(o.ServeDate + o.PreferTime, 12).ToString("yyyy/MM/dd HH:mm");
                        or.term = o.Ternimal;
                        Lists.Add(or);
                    }
                    msg.Status = "SUCCESS";
                    msg.Msg = aes.Encrypt(JsonConvert.SerializeObject(Lists));
                    return msg;
                }
                catch (Exception)
                {
                    msg.Status = "ERROR";
                    msg.Msg = "ERR_Query_Parameter";
                    return msg;
                }
            }
            else
            {
                msg.Status = "ERROR";
                msg.Msg = "ERR_TOKEN_ERROR";
                return msg;
            }
        }
    }
}