﻿using System;
using System.ServiceModel.Activation;
using System.Web;
using System.Web.Routing;


namespace B2B_API
{
    public class Global : HttpApplication
    {
        //protected void Application_BeginRequest(object sender, EventArgs e)
        //{
        //HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        //if (HttpContext.Current.Request.HttpMethod == "OPTIONS")
        //{
        //    HttpContext.Current.Response.AddHeader("Access-Control-Allow-Methods", "GET, POST");
        //    HttpContext.Current.Response.AddHeader("Access-Control-Allow-Headers", "Content-Type, Accept");
        //    HttpContext.Current.Response.AddHeader("Access-Control-Max-Age", "1728000");
        //    HttpContext.Current.Response.End();
        //}
        //}

        void Application_Start(object sender, EventArgs e)
        {
            RegisterRoutes();
        }

        private void RegisterRoutes()
        {
            // Edit the base address of Service1 by replacing the "Service1" string below
            RouteTable.Routes.Add(new ServiceRoute("Auth", new WebServiceHostFactory(), typeof(Auth)));
            RouteTable.Routes.Add(new ServiceRoute("JQ", new WebServiceHostFactory(), typeof(JourneyQ)));
            RouteTable.Routes.Add(new ServiceRoute("WiFiHero", new WebServiceHostFactory(), typeof(WiFiHero)));
            RouteTable.Routes.Add(new ServiceRoute("Addweup", new WebServiceHostFactory(), typeof(Addweup)));
            RouteTable.Routes.Add(new ServiceRoute("iShareco", new WebServiceHostFactory(), typeof(iShareco)));
        }
    }
}

