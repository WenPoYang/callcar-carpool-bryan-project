﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.IO;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using CallCar;
using CallCar.Account;
using CallCar.Data;
using B2B_API.Share;
using System.Data;
using MySql.Data.MySqlClient;
using DbConn;
using System.Transactions;
using CallCar.Dispatch;
using CallCar.Reservation;

namespace B2B_API
{
    [ServiceContract(Namespace = "B2B_API")]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]



    public class iShareco
    {

        [Description("取得待媒合清單")]
        [OperationContract, WebInvoke(UriTemplate = "getMOrder", Method = "POST", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        public ReturnMsg getMatchOrder(Stream input)
        {
            ReturnMsg msg = new ReturnMsg();
            string data = new StreamReader(input).ReadToEnd();

            string date;
            try
            {
                JObject jo = JsonConvert.DeserializeObject<JObject>(data);
                date = Convert.ToString(jo["d"]);
            }
            catch (Exception)
            {
                msg.Status = "ERROR";
                msg.Msg = "ERR_DATA_ERROR";
                return msg;
            }
            try
            {
                StringBuilder sb = new StringBuilder();
                List<MySqlParameter> parms = new List<MySqlParameter>();
                sb.Append("SELECT ReservationNo, UserID, ServiceType, TakeDate, TimeSegment, SelectionType, PickCartype, FlightNo, ScheduleFlightTime, PickupCity, ");
                sb.Append("PickupDistinct, PickupVillage, PickupAddress, X(PickupGPS) as plng, Y(PickupGPS) as plat, TakeoffCity, TakeoffDistinct, TakeoffVillage, TakeoffAddress, X(TakeoffGPS) as tlng ,Y(TakeoffGPS) as tlat , PassengerCnt, ");
                sb.Append("BaggageCnt, MaxFlag, Price, Coupon, InvoiceData, CreditData, ProcessStage, InvoiceNum, CreateTime, UpdTime ");
                sb.Append("FROM reservation_sheet where TakeDate = @sdate  AND ProcessStage = '0' order by TakeDate,TimeSegment asc ");

                parms.Add(new MySqlParameter("@sdate", date));
                DataTable dt = new DataTable();
                List<OReservation> List = new List<OReservation>();
                try
                {
                    using (dbConnectionMySQL conn = new dbConnectionMySQL(ShareParameter.ConnectionString))
                    {
                        dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                    }
                }
                catch (MySqlException ex)
                {
                    msg.Status = "ERROR";
                    msg.Msg = ex.Message;
                    return msg;
                }

                OMachSourceData Match = new OMachSourceData();
                Match.MatchDate = date;
                if (dt.Rows.Count > 0)
                {

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        OMatchRData m = new OMatchRData();
                        m.ReservationNo = Convert.ToString(dt.Rows[i]["ReservationNo"]);
                        m.ServiceType = Convert.ToString(dt.Rows[i]["ServiceType"]);
                        m.TimeSegment = Convert.ToString(dt.Rows[i]["TimeSegment"]);
                        m.BaggageCnt = Convert.ToInt32(dt.Rows[i]["BaggageCnt"]);
                        m.PassengerCnt = Convert.ToInt32(dt.Rows[i]["PassengerCnt"]);
                        m.PickupLat = Convert.ToDouble(dt.Rows[i]["plng"]);
                        m.PickupLng = Convert.ToDouble(dt.Rows[i]["plat"]);
                        m.TakeoffLat = Convert.ToDouble(dt.Rows[i]["tlat"]);
                        m.TakeoffLng = Convert.ToDouble(dt.Rows[i]["tlng"]);
                        m.MaxFlag = Convert.ToString(dt.Rows[i]["MaxFlag"]);
                        if (dt.Rows[i]["FlightNo"] != null && dt.Rows[i]["FlightNo"] != DBNull.Value)
                        {
                            m.FlightNo = Convert.ToString(dt.Rows[i]["FlightNo"]);

                            if (m.FlightNo != "")
                                m.FlightTime = Convert.ToDateTime(dt.Rows[i]["ScheduleFlightTime"]);
                        }
                        else
                        {
                            m.FlightNo = "";
                        }

                        DateTime MatchStartTime, MatchEndTime, PreferTime;
                        PreferTime = CallCar.Tool.General.ConvertToDateTime(date + m.TimeSegment, 12);

                        if (m.ServiceType == "O")
                        {
                            if (m.FlightNo != "" && m.FlightTime != null)
                            {
                                if (PreferTime.AddMinutes(15).CompareTo(m.FlightTime.AddMinutes(-150)) >= 0)
                                    MatchEndTime = m.FlightTime.AddMinutes(-150);
                                else
                                    MatchEndTime = PreferTime.AddMinutes(15);

                                MatchStartTime = PreferTime.AddMinutes(-30);
                            }
                            else
                            {
                                MatchEndTime = PreferTime.AddMinutes(15);
                                MatchStartTime = PreferTime.AddMinutes(-30);
                            }
                        }
                        else
                        {
                            MatchEndTime = PreferTime.AddMinutes(30);
                            MatchStartTime = PreferTime;
                        }

                        if (MatchStartTime.Date < PreferTime.Date)
                            m.mStartTime = "0000";
                        else
                            m.mStartTime = MatchStartTime.ToString("HHmm");

                        if (MatchEndTime.Date > PreferTime.Date)
                            m.mEndTime = "2359";
                        else
                            m.mEndTime = MatchEndTime.ToString("HHmm");

                        Match.ReservationList.Add(m);
                    }
                }

                msg.Status = "SUCCESS";
                msg.Msg = JsonConvert.SerializeObject(Match);
                return msg;
            }
            catch (Exception ex)
            {
                msg.Status = "Error";
                msg.Msg = ex.Message;
                return msg;
            }
        }

        //[Description("暫時取得正式資料庫待媒合清單")]
        //[OperationContract, WebInvoke(UriTemplate = "getMOrder2", Method = "POST", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        //public ReturnMsg getMatchOrder2(Stream input)
        //{
        //    ReturnMsg msg = new ReturnMsg();
        //    string data = new StreamReader(input).ReadToEnd();

        //    string date;
        //    try
        //    {
        //        JObject jo = JsonConvert.DeserializeObject<JObject>(data);
        //        date = Convert.ToString(jo["d"]);
        //    }
        //    catch (Exception)
        //    {
        //        msg.Status = "ERROR";
        //        msg.Msg = "ERR_DATA_ERROR";
        //        return msg;
        //    }
        //    try
        //    {
        //        StringBuilder sb = new StringBuilder();
        //        List<MySqlParameter> parms = new List<MySqlParameter>();
        //        sb.Append("SELECT ReservationNo, UserID, ServiceType, TakeDate, TimeSegment, SelectionType, PickCartype, FlightNo, ScheduleFlightTime, PickupCity, ");
        //        sb.Append("PickupDistinct, PickupVillage, PickupAddress, X(PickupGPS) as plng, Y(PickupGPS) as plat, TakeoffCity, TakeoffDistinct, TakeoffVillage, TakeoffAddress, X(TakeoffGPS) as tlng ,Y(TakeoffGPS) as tlat , PassengerCnt, ");
        //        sb.Append("BaggageCnt, MaxFlag, Price, Coupon, InvoiceData, CreditData, ProcessStage, InvoiceNum, CreateTime, UpdTime ");
        //        sb.Append("FROM reservation_sheet where TakeDate = @sdate  AND ProcessStage = '0' order by TakeDate,TimeSegment asc ");

        //        parms.Add(new MySqlParameter("@sdate", date));
        //        DataTable dt = new DataTable();
        //        List<OReservation> List = new List<OReservation>();
        //        try
        //        {
        //            using (dbConnectionMySQL conn = new dbConnectionMySQL("Server=10.0.2.20;Port=3306;Database=carpoollab;Uid=ishareco;Pwd=fu0!j0$su0^vup#;Allow User Variables=True"))
        //            {
        //                dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
        //            }
        //        }
        //        catch (MySqlException ex)
        //        {
        //            msg.Status = "ERROR";
        //            msg.Msg = ex.Message;
        //            return msg;
        //        }

        //        OMachSourceData Match = new OMachSourceData();
        //        Match.MatchDate = date;
        //        if (dt.Rows.Count > 0)
        //        {

        //            for (int i = 0; i < dt.Rows.Count; i++)
        //            {
        //                OMatchRData m = new OMatchRData();
        //                m.ReservationNo = Convert.ToString(dt.Rows[i]["ReservationNo"]);
        //                m.ServiceType = Convert.ToString(dt.Rows[i]["ServiceType"]);
        //                m.TimeSegment = Convert.ToString(dt.Rows[i]["TimeSegment"]);
        //                m.BaggageCnt = Convert.ToInt32(dt.Rows[i]["BaggageCnt"]);
        //                m.PassengerCnt = Convert.ToInt32(dt.Rows[i]["PassengerCnt"]);
        //                m.PickupLat = Convert.ToDouble(dt.Rows[i]["plng"]);
        //                m.PickupLng = Convert.ToDouble(dt.Rows[i]["plat"]);
        //                m.TakeoffLat = Convert.ToDouble(dt.Rows[i]["tlat"]);
        //                m.TakeoffLng = Convert.ToDouble(dt.Rows[i]["tlng"]);
        //                m.MaxFlag = Convert.ToString(dt.Rows[i]["MaxFlag"]);
        //                if (dt.Rows[i]["FlightNo"] != null && dt.Rows[i]["FlightNo"] != DBNull.Value)
        //                {
        //                    m.FlightNo = Convert.ToString(dt.Rows[i]["FlightNo"]);

        //                    if (m.FlightNo != "")
        //                        m.FlightTime = Convert.ToDateTime(dt.Rows[i]["ScheduleFlightTime"]);
        //                }
        //                else
        //                {
        //                    m.FlightNo = "";
        //                }

        //                DateTime MatchStartTime, MatchEndTime, PreferTime;
        //                PreferTime = CallCar.Tool.General.ConvertToDateTime(date + m.TimeSegment, 12);

        //                if (m.ServiceType == "O")
        //                {
        //                    if (m.FlightNo != "" && m.FlightTime != null)
        //                    {
        //                        if (PreferTime.AddMinutes(15).CompareTo(m.FlightTime.AddMinutes(-150)) >= 0)
        //                            MatchEndTime = m.FlightTime.AddMinutes(-150);
        //                        else
        //                            MatchEndTime = PreferTime.AddMinutes(15);

        //                        MatchStartTime = PreferTime.AddMinutes(-30);
        //                    }
        //                    else
        //                    {
        //                        MatchEndTime = PreferTime.AddMinutes(15);
        //                        MatchStartTime = PreferTime.AddMinutes(-30);
        //                    }
        //                }
        //                else
        //                {
        //                    MatchEndTime = PreferTime.AddMinutes(15);
        //                    MatchStartTime = PreferTime;
        //                }

        //                if (MatchStartTime.Date < PreferTime.Date)
        //                    m.mStartTime = "0000";
        //                else
        //                    m.mStartTime = MatchStartTime.ToString("HHmm");

        //                if (MatchEndTime.Date > PreferTime.Date)
        //                    m.mEndTime = "2359";
        //                else
        //                    m.mEndTime = MatchEndTime.ToString("HHmm");

        //                Match.ReservationList.Add(m);
        //            }
        //        }

        //        msg.Status = "SUCCESS";
        //        msg.Msg = JsonConvert.SerializeObject(Match);
        //        return msg;
        //    }
        //    catch (Exception ex)
        //    {
        //        msg.Status = "Error";
        //        msg.Msg = ex.Message;
        //        return msg;
        //    }
        //}

        [Description("變更訂單服務時間")]
        [OperationContract, WebInvoke(UriTemplate = "chgServeTime", Method = "POST", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        public ReturnMsg ModifyServiceTime(Stream input)
        {
            ReturnMsg msg = new ReturnMsg();
            string data = new StreamReader(input).ReadToEnd();

            string ReservationNo, DispatchNo;
            DateTime NewServiceTime;
            try
            {
                JObject jo = JsonConvert.DeserializeObject<JObject>(data);
                DispatchNo = Convert.ToString(jo["d"]);
                ReservationNo = Convert.ToString(jo["r"]);
                NewServiceTime = Convert.ToDateTime(jo["t"]);
            }
            catch (Exception)
            {
                msg.Status = "ERROR";
                msg.Msg = "ERR_DATA_ERROR";
                return msg;
            }

            using (TransactionScope scope = new TransactionScope())
            {
                StringBuilder sb = new StringBuilder();
                List<MySqlParameter> parms = new List<MySqlParameter>();

                try
                {
                    sb.Append("UPDATE dispatch_reservation SET ScheduleServeTime= @time WHERE DispatchNo = @dno and ReservationNo = @rno");
                    parms.Add(new MySqlParameter("@time", NewServiceTime));
                    parms.Add(new MySqlParameter("@dno", DispatchNo));
                    parms.Add(new MySqlParameter("@rno", ReservationNo));

                    using (dbConnectionMySQL conn = new dbConnectionMySQL(ShareParameter.ConnectionString))
                    {
                        conn.executeUpdateQuery(sb.ToString(), parms.ToArray());
                    }

                    MoidfySMSTime(ReservationNo, NewServiceTime);

                    AddDispatchRefresh("U", DispatchNo);

                    scope.Complete();

                    msg.Status = "SUCCESS";
                    msg.Msg = "";
                    return msg;
                }
                catch (Exception ex)
                {
                    msg.Status = "ERROR";
                    msg.Msg = ex.Message;
                    return msg;
                }
            }
        }

        [Description("派車拆單")]
        [OperationContract, WebInvoke(UriTemplate = "splitDispatch", Method = "POST", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        public ReturnMsg SplitDispatch(Stream input)
        {
            ReturnMsg msg = new ReturnMsg();
            string data = new StreamReader(input).ReadToEnd();

            string ReservationNo, OldDispatchNo, NewDispatchNo;

            try
            {
                JObject jo = JsonConvert.DeserializeObject<JObject>(data);
                OldDispatchNo = Convert.ToString(jo["d"]);
                ReservationNo = Convert.ToString(jo["r"]);
            }
            catch (Exception)
            {
                msg.Status = "ERROR";
                msg.Msg = "ERR_DATA_ERROR";
                return msg;
            }

            BALDispatch bald = new BALDispatch(ShareParameter.ConnectionString);
            ODispatchSheet OldSheet = bald.GetDispatchInfo(OldDispatchNo);
            if (OldSheet.ServiceCnt <= 1)
            {
                msg.Status = "ERROR";
                msg.Msg = "ERR_SERVICE_CNT";
                return msg;
            }

            BALReservation balr = new BALReservation(ShareParameter.ConnectionString);
            OReservation or = balr.GetReservation(ReservationNo);

            try
            {
                //取新派車序號
                CallCar.Tool.BALSerialNum serial = new CallCar.Tool.BALSerialNum(ShareParameter.ConnectionString);
                OSerialNum oserial = serial.GetSerialNumObject("Dispatch");
                NewDispatchNo = DateTime.Now.ToString(oserial.Prefix) + serial.GetSeqNoString(oserial);
            }
            catch (Exception)
            {
                msg.Status = "ERROR";
                msg.Msg = "ERR_GET_SERIAL";
                return msg;
            }

            try
            {
                StringBuilder sb = new StringBuilder();
                List<MySqlParameter> parms = new List<MySqlParameter>();

                using (TransactionScope scope = new TransactionScope())
                {
                    //先修改dispatch_reservation中的派車序號
                    sb.Append("UPDATE dispatch_reservation SET DispatchNo=@dno	WHERE DispatchNo=@odno and ReservationNo = @rno");
                    parms.Add(new MySqlParameter("@dno", NewDispatchNo));
                    parms.Add(new MySqlParameter("@odno", OldDispatchNo));
                    parms.Add(new MySqlParameter("@rno", ReservationNo));


                    using (dbConnectionMySQL conn = new dbConnectionMySQL(ShareParameter.ConnectionString))
                    {
                        conn.executeUpdateQuery(sb.ToString(), parms.ToArray());
                    }
                    sb.Length = 0;
                    parms.Clear();

                    //新增一筆dispatch_sheet
                    sb.Append("INSERT INTO dispatch_sheet ");
                    sb.Append("(DispatchNo, ServiceType, ServeDate, TimeSegment, DispatchTime, CarpoolFlag, CanPick, PassengerCnt, BaggageCnt, StopCnt, MaxFlag, FleetID, CarType, CarNo, DriverID, ServiceRemark, UpdFlag) ");
                    sb.Append("VALUES(@DispatchNo, @ServiceType, @ServeDate, @TimeSegment, @DispatchTime, @CarpoolFlag, @CanPick, @PassengerCnt, @BaggageCnt, @StopCnt, @MaxFlag, @FleetID, @CarType, @CarNo, @DriverID, @ServiceRemark, @UpdFlag)");
                    parms.Add(new MySqlParameter("@DispatchNo", NewDispatchNo));
                    parms.Add(new MySqlParameter("@ServiceType", OldSheet.ServiceType));
                    parms.Add(new MySqlParameter("@ServeDate", OldSheet.TakeDate));
                    parms.Add(new MySqlParameter("@TimeSegment", OldSheet.TimeSegment));
                    parms.Add(new MySqlParameter("@DispatchTime", or.PreferTime));
                    parms.Add(new MySqlParameter("@CarpoolFlag", OldSheet.CarpoolFlag));
                    parms.Add(new MySqlParameter("@CanPick", OldSheet.CanPick));
                    parms.Add(new MySqlParameter("@PassengerCnt", or.PassengerCnt));
                    parms.Add(new MySqlParameter("@BaggageCnt", or.BaggageCnt));
                    parms.Add(new MySqlParameter("@StopCnt", 1));
                    parms.Add(new MySqlParameter("@MaxFlag", "0"));
                    parms.Add(new MySqlParameter("@FleetID", Int32.Parse("0")));
                    if (or.PassengerCnt <= 3 && or.BaggageCnt <= 3)
                        parms.Add(new MySqlParameter("@CarType", "四人座"));
                    else
                        parms.Add(new MySqlParameter("@CarType", "七人座"));
                    parms.Add(new MySqlParameter("@CarNo", ""));
                    parms.Add(new MySqlParameter("@DriverID", Int32.Parse("0")));
                    parms.Add(new MySqlParameter("@ServiceRemark", OldSheet.ServiceRemark));
                    parms.Add(new MySqlParameter("@UpdFlag", "0"));

                    using (dbConnectionMySQL conn = new dbConnectionMySQL(ShareParameter.ConnectionString))
                    {
                        conn.executeInsertQuery(sb.ToString(), parms.ToArray());
                    }
                    sb.Length = 0;
                    parms.Clear();

                    //修改原本派車單內容
                    sb.Append("UPDATE dispatch_sheet SET PassengerCnt=@pcnt,BaggageCnt=@bcnt,StopCnt=@scnt,CarType = @ctype WHERE DispatchNo = @dno");
                    parms.Add(new MySqlParameter("@pcnt", OldSheet.PassengerCnt - or.PassengerCnt));
                    parms.Add(new MySqlParameter("@bcnt", OldSheet.BaggageCnt - or.BaggageCnt));
                    parms.Add(new MySqlParameter("@scnt", OldSheet.ServiceCnt - 1));
                    if (OldSheet.PassengerCnt - or.PassengerCnt <= 3 && OldSheet.BaggageCnt - or.BaggageCnt <= 3)
                        parms.Add(new MySqlParameter("@ctype", "四人座"));
                    else
                        parms.Add(new MySqlParameter("@ctype", "七人座"));
                    parms.Add(new MySqlParameter("@dno", OldDispatchNo));

                    using (dbConnectionMySQL conn = new dbConnectionMySQL(ShareParameter.ConnectionString))
                    {
                        conn.executeUpdateQuery(sb.ToString(), parms.ToArray());
                    }

                    //新增派單更新資料表
                    AddDispatchRefresh("U", NewDispatchNo);
                    AddDispatchRefresh("U", OldDispatchNo);

                    scope.Complete();

                }
            }
            catch (Exception ex)
            {
                msg.Status = "ERROR";
                msg.Msg = ex.Message;
                return msg;
            }
            msg.Status = "SUCCESS";
            msg.Msg = "";
            return msg;
        }

        [Description("派車併單")]
        [OperationContract, WebInvoke(UriTemplate = "CbDispatch", Method = "POST", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        public ReturnMsg CombineDispatch(Stream input)
        {
            ReturnMsg msg = new ReturnMsg();
            string data = new StreamReader(input).ReadToEnd();

            string ReservationNo, OldDispatchNo, NewDispatchNo;

            try
            {
                JObject jo = JsonConvert.DeserializeObject<JObject>(data);
                OldDispatchNo = Convert.ToString(jo["fromd"]);
                ReservationNo = Convert.ToString(jo["r"]);
                NewDispatchNo = Convert.ToString(jo["tod"]);
            }
            catch (Exception)
            {
                msg.Status = "ERROR";
                msg.Msg = "ERR_DATA_ERROR";
                return msg;
            }


            BALDispatch bald = new BALDispatch(ShareParameter.ConnectionString);
            ODispatchSheet OldSheet = bald.GetDispatchInfo(OldDispatchNo);
            ODispatchSheet NewSheet = bald.GetDispatchInfo(NewDispatchNo);

            BALReservation balr = new BALReservation(ShareParameter.ConnectionString);
            OReservation or = balr.GetReservation(ReservationNo);

            //try
            //{
            //    //取新派車序號
            //    CallCar.Tool.BALSerialNum serial = new CallCar.Tool.BALSerialNum(ShareParameter.ConnectionString);
            //    OSerialNum oserial = serial.GetSerialNumObject("Dispatch");
            //    NewDispatchNo = DateTime.Now.ToString(oserial.Prefix) + serial.GetSeqNoString(oserial);
            //}
            //catch (Exception)
            //{
            //    msg.Status = "ERROR";
            //    msg.Msg = "ERR_GET_SERIAL";
            //    return msg;
            //}

            try
            {
                StringBuilder sb = new StringBuilder();
                List<MySqlParameter> parms = new List<MySqlParameter>();

                using (TransactionScope scope = new TransactionScope())
                {
                    //先修改dispatch_reservation中的派車序號
                    sb.Append("UPDATE dispatch_reservation SET DispatchNo=@dno	WHERE DispatchNo=@odno and ReservationNo = @rno");
                    parms.Add(new MySqlParameter("@dno", NewDispatchNo));
                    parms.Add(new MySqlParameter("@odno", OldDispatchNo));
                    parms.Add(new MySqlParameter("@rno", ReservationNo));


                    using (dbConnectionMySQL conn = new dbConnectionMySQL(ShareParameter.ConnectionString))
                    {
                        conn.executeUpdateQuery(sb.ToString(), parms.ToArray());
                    }
                    sb.Length = 0;
                    parms.Clear();

                    //修改原派車單內容
                    sb.Append("UPDATE dispatch_sheet SET PassengerCnt=@pcnt,BaggageCnt=@bcnt,StopCnt=@scnt,CarType = @ctype WHERE DispatchNo = @dno");
                    parms.Add(new MySqlParameter("@pcnt", OldSheet.PassengerCnt - or.PassengerCnt));
                    parms.Add(new MySqlParameter("@bcnt", OldSheet.BaggageCnt - or.BaggageCnt));
                    parms.Add(new MySqlParameter("@scnt", OldSheet.ServiceCnt - 1));
                    if (OldSheet.PassengerCnt - or.PassengerCnt <= 3 && OldSheet.BaggageCnt - or.BaggageCnt <= 3)
                        parms.Add(new MySqlParameter("@ctype", "四人座"));
                    else
                        parms.Add(new MySqlParameter("@ctype", "七人座"));
                    parms.Add(new MySqlParameter("@dno", OldDispatchNo));

                    using (dbConnectionMySQL conn = new dbConnectionMySQL(ShareParameter.ConnectionString))
                    {
                        conn.executeUpdateQuery(sb.ToString(), parms.ToArray());
                    }

                    sb.Length = 0;
                    parms.Clear();

                    //修改新派車單內容
                    sb.Append("UPDATE dispatch_sheet SET PassengerCnt=@pcnt,BaggageCnt=@bcnt,StopCnt=@scnt,CarType = @ctype WHERE DispatchNo = @dno");
                    parms.Add(new MySqlParameter("@pcnt", NewSheet.PassengerCnt + or.PassengerCnt));
                    parms.Add(new MySqlParameter("@bcnt", NewSheet.BaggageCnt + or.BaggageCnt));
                    parms.Add(new MySqlParameter("@scnt", NewSheet.ServiceCnt + 1));
                    if (OldSheet.PassengerCnt + or.PassengerCnt <= 3 && OldSheet.BaggageCnt + or.BaggageCnt <= 3)
                        parms.Add(new MySqlParameter("@ctype", "四人座"));
                    else
                        parms.Add(new MySqlParameter("@ctype", "七人座"));
                    parms.Add(new MySqlParameter("@dno", NewDispatchNo));

                    using (dbConnectionMySQL conn = new dbConnectionMySQL(ShareParameter.ConnectionString))
                    {
                        conn.executeUpdateQuery(sb.ToString(), parms.ToArray());
                    }

                    //新增派單更新資料表
                    AddDispatchRefresh("U", NewDispatchNo);
                    AddDispatchRefresh("U", OldDispatchNo);

                    scope.Complete();

                }
            }
            catch (Exception ex)
            {
                msg.Status = "ERROR";
                msg.Msg = ex.Message;
                return msg;
            }
            msg.Status = "SUCCESS";
            msg.Msg = "";
            return msg;
        }

        [Description("共乘區域查詢")]
        [OperationContract, WebInvoke(UriTemplate = "SSAQ", Method = "POST", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        public ReturnMsg ShareServiceAreaQry(Stream input)
        {
            string data = new StreamReader(input).ReadToEnd();

            ReturnMsg r = new ReturnMsg();
            string token = string.Empty;

            try
            {
                string CityName = data.Split(',')[0];
                if (CityName == "台北市" || CityName == "臺北市")
                    CityName = "台北市";
                string DistinctName = data.Split(',')[1];
                string VillageName = data.Split(',')[2];

                string ServiceStatus = GetVillageServiceStatus(CityName, DistinctName, VillageName);
                r.Status = "Success";
                if (ServiceStatus == "1")
                    r.Msg = "S";
                else
                    r.Msg = "X";
                return r;
            }
            catch (Exception)
            {
                r.Status = "Error";
                r.Msg = "查詢服務區域發生錯誤";
                return r;
            }
        }

        private bool AddDispatchRefresh(string type, string DispatchNo)
        {
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();


            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(ShareParameter.ConnectionString))
                {

                    sb.Append("SELECT SeqNo FROM dispatch_refresh_record	where DispatchNo = @dno and RefreshType = @type and RefreshFlag = '0'");
                    parms.Add(new MySqlParameter("@dno", DispatchNo));
                    parms.Add(new MySqlParameter("@type", type));

                    try
                    {
                        DataTable dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                        if (dt.Rows.Count > 0)
                            return true;
                    }
                    catch (MySqlException ex)
                    {
                        throw ex;
                    }

                    sb.Length = 0;
                    parms.Clear();

                    sb.Append("INSERT INTO dispatch_refresh_record (RefreshType, DispatchNo, RefreshFlag) VALUES (@type, @dno, '0')");
                    parms.Add(new MySqlParameter("@type", type));
                    parms.Add(new MySqlParameter("@dno", DispatchNo));

                    try
                    {

                        conn.executeInsertQuery(sb.ToString(), parms.ToArray());

                        return true;
                    }
                    catch (MySqlException ex)
                    {
                        throw ex;
                    }
                }
            }
            catch (MySqlException ex)
            {
                throw ex;
            }
        }

        private bool MoidfySMSTime(string ReservationNo, DateTime NewServeTime)
        {
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();


            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(ShareParameter.ConnectionString))
                {
                    sb.Append("SELECT SeqNo FROM reservation_sms	where ReservationNo = @rno");
                    parms.Add(new MySqlParameter("@rno", ReservationNo));

                    try
                    {
                        DataTable dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                        if (dt.Rows.Count == 0)
                            return true;
                    }
                    catch (MySqlException ex)
                    {
                        throw ex;
                    }

                    sb.Length = 0;
                    parms.Clear();

                    sb.Append("UPDATE reservation_sms SET ServeTime=@time WHERE ReservationNo = @rno");
                    parms.Add(new MySqlParameter("@time", NewServeTime));
                    parms.Add(new MySqlParameter("@rno", ReservationNo));

                    try
                    {

                        conn.executeUpdateQuery(sb.ToString(), parms.ToArray());

                        return true;
                    }
                    catch (MySqlException ex)
                    {
                        throw ex;
                    }
                }
            }
            catch (MySqlException ex)
            {
                throw ex;
            }
        }

        private string GetVillageServiceStatus(string CityName, string DistinctName, string VillageName)
        {
            DataTable dt = new DataTable();
            StringBuilder sb = new StringBuilder();
            string DistinctStatus, VillageStatus;
            List<MySqlParameter> parms = new List<MySqlParameter>();
            sb.Append("select d.ServiceStatus as 'DistinctStatus', r.ServiceStatus as 'VillageStatus' from city_distinct_data d,service_range_data r ");
            sb.Append("where d.DistinctID = r.DistinctID and  d.CityName = @city and d.DistinctName = @dict and r.VillageName = @vil ");
            parms.Add(new MySqlParameter("@city", CityName));
            parms.Add(new MySqlParameter("@dict", DistinctName));
            parms.Add(new MySqlParameter("@vil", VillageName));

            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(ShareParameter.ConnectionString))
                {
                    dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                    if (dt.Rows.Count == 1)
                    {
                        DistinctStatus = Convert.ToString(dt.Rows[0]["DistinctStatus"]);
                        VillageStatus = Convert.ToString(dt.Rows[0]["VillageStatus"]);

                        if (DistinctStatus == "1")
                            return "1";
                        else if (DistinctStatus == "X")
                            return "0";
                        else
                        {
                            if (VillageStatus == "1")
                                return "1";
                            else
                                return "0";
                        }
                    }
                    else
                        return "0";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
