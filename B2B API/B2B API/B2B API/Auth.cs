﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.IO;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using DbConn;
using MySql.Data.MySqlClient;
using System.Data;
using B2B_API.Share;
using CallCar.Tool;
using B2B_API.Company;

namespace B2B_API
{
    [ServiceContract(Namespace = "B2B_API")]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]



    public class Auth
    {

        [Description("取得授權")]
        [OperationContract, WebInvoke(UriTemplate = "gAuth", Method = "POST", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        public ReturnMsg GetToken(Stream input)
        {
            ReturnMsg msg = new ReturnMsg();
            string data = new StreamReader(input).ReadToEnd();

            string Account;
            string Password;
            try
            {
                JObject jo = JsonConvert.DeserializeObject<JObject>(data);
                Account = Convert.ToString(jo["acc"]);
                Password = Convert.ToString(jo["pw"]);

            }
            catch (Exception)
            {
                msg.Status = "ERROR";
                msg.Msg = "ERR_DATA_ERROR";
                return msg;
            }

            Company.CompanyInfo company = new Company.CompanyInfo();
            try
            {
                company = CompanyInfo.GetCompanyByEIN(Account);
            }
            catch (Exception)
            {
                msg.Status = "ERROR";
                msg.Msg = "ERR_TOKEN_ERROR";
                return msg;
            }

            string md5pw = MD5Cryptography.Encrypt(Password);
            //string md5pw = Password;

            if (md5pw != company.ComConPW)
            {
                msg.Status = "ERROR";
                msg.Msg = "ERR_LOGIN_FAIL";
                return msg;
            }
            string token = "";
            try
            {
                token = TokenInfo.RenewToken(Account);
            }
            catch (Exception)
            {
                msg.Status = "ERROR";
                msg.Msg = "ERR_AUTH_FAIL";
                return msg;
            }

            if (token == "")
            {
                msg.Status = "ERROR";
                msg.Msg = "ERR_AUTH_FAIL";
                return msg;
            }

            msg.Status = "SUCCESS";
            AESCryptography aes = new AESCryptography(company.CompanyHash, company.CompanyIV);
            msg.Msg = aes.Encrypt(token);
            return msg;
        }
    }
}
