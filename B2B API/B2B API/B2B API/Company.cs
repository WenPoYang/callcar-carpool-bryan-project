﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using MySql.Data.MySqlClient;
using DbConn;
using System.Text;
using System.Configuration;
using B2B_API.Share;

namespace B2B_API.Company
{
    public class CompanyInfo
    {


        public CompanyInfo()
        {

        }

        public static CompanyInfo GetCompanyByToken(string token)
        {
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            sb.Append("SELECT CompanyEIN, CompanyName, ComConPW, BindingID, CompanyHash, CompanyIV, TokenValue ");
            sb.Append(" FROM business_info where TokenValue = @token ");
            parms.Add(new MySqlParameter("@token", token));
            DataTable dt = new DataTable();
            using (dbConnectionMySQL conn = new dbConnectionMySQL(ShareParameter.ConnectionStringB))
            {
                dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
            }

            if (dt.Rows.Count == 0)
            {
                return null;
            }
            else if (dt.Rows.Count > 1)
            {
                throw new Exception("ERR_ERROR_TOKEN");
            }
            else
            {
                CompanyInfo c = new CompanyInfo();
                c.Token = Convert.ToString(dt.Rows[0]["TokenValue"]);
                c.CompanyName = Convert.ToString(dt.Rows[0]["CompanyName"]);
                c.CompanyIV = Convert.ToString(dt.Rows[0]["CompanyIV"]);
                c.CompanyHash = Convert.ToString(dt.Rows[0]["CompanyHash"]);
                c.CompanyEIN = Convert.ToString(dt.Rows[0]["CompanyEIN"]);
                c.ComConPW = Convert.ToString(dt.Rows[0]["ComConPW"]);
                c.BindingID = Convert.ToInt32(dt.Rows[0]["BindingID"]);
                return c;
            }
        }

        public static CompanyInfo GetCompanyByEIN(string EIN)
        {
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            sb.Append("SELECT CompanyEIN, CompanyName, ComConPW, BindingID, CompanyHash, CompanyIV, TokenValue ");
            sb.Append(" FROM business_info where CompanyEIN = @ein ");
            parms.Add(new MySqlParameter("@ein", EIN));
            DataTable dt = new DataTable();
            using (dbConnectionMySQL conn = new dbConnectionMySQL(ShareParameter.ConnectionStringB))
            {
                dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
            }

            if (dt.Rows.Count == 0)
            {
                return null;
            }
            else if (dt.Rows.Count > 1)
            {
                throw new Exception("ERR_ERROR_TOKEN");
            }
            else
            {
                CompanyInfo c = new CompanyInfo();
                c.Token = Convert.ToString(dt.Rows[0]["TokenValue"]);
                c.CompanyName = Convert.ToString(dt.Rows[0]["CompanyName"]);
                c.CompanyIV = Convert.ToString(dt.Rows[0]["CompanyIV"]);
                c.CompanyHash = Convert.ToString(dt.Rows[0]["CompanyHash"]);
                c.CompanyEIN = Convert.ToString(dt.Rows[0]["CompanyEIN"]);
                c.ComConPW = Convert.ToString(dt.Rows[0]["ComConPW"]);
                c.BindingID = Convert.ToInt32(dt.Rows[0]["BindingID"]);
                return c;
            }
        }

        public string CompanyEIN { get; set; }
        public string CompanyName { get; set; }
        public string ComConPW { get; set; }
        public int BindingID { get; set; }
        public string CompanyHash { get; set; }
        public string CompanyIV { get; set; }
        public string Token { get; set; }
    }
}