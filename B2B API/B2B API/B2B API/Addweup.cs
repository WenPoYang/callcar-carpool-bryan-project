﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.IO;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using CallCar.Account;
using CallCar.Data;
using B2B_API.Share;
using CallCar.Tool;
using System.Net;
using DbConn;
using MySql.Data.MySqlClient;
using System.Data;

namespace B2B_API
{
    [ServiceContract(Namespace = "B2B_API")]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]



    public class Addweup
    {
        private string AddweupAPIURL = @"http://dev.api.uchange2.com/v1.0/callcar/order/valid/";
        private string AddweupToken = "MmMzYzYxZTliNzRmZWU5ZTE1YTFmNzUwZjE4MGU5YjU5NWQ3Y2ZlMGQwMWM0YmMzZjVmNTYwNmNhYmE1M2M5ZQ==";

        [Description("確認Email是否有註冊")]
        [OperationContract, WebInvoke(UriTemplate = "Check", Method = "POST", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        public ReturnMsg ChkEmail(Stream input)
        {
            ReturnMsg msg = new ReturnMsg();
            string data = new StreamReader(input).ReadToEnd();

            string Token, CryptData, Email;
            JObject jo;
            try
            {
                jo = JsonConvert.DeserializeObject<JObject>(data);
                Token = Convert.ToString(jo["token"]);
                CryptData = Convert.ToString(jo["data"]);
            }
            catch (Exception)
            {
                msg.Status = "ERROR";
                msg.Msg = "ERR_DATA_ERROR";
                return msg;
            }

            Company.CompanyInfo company = new Company.CompanyInfo();
            try
            {
                company = ShareFunc.VarifyToken(Token);
            }
            catch (Exception)
            {
                msg.Status = "ERROR";
                msg.Msg = "ERR_TOKEN_ERROR";
                return msg;
            }

            try
            {
                AESCryptography aes = new AESCryptography(company.CompanyHash, company.CompanyIV);
                try
                {
                    Email = aes.Decrypt(CryptData);
                }
                catch (Exception)
                {
                    msg.Status = "ERROR";
                    msg.Msg = "ERR_INCOME_DATA";
                    return msg;
                }
                BALAccount bala = new BALAccount(ShareParameter.ConnectionString);
                OAccount oa = bala.GetUserAccount(Email);

                if (oa != null && oa.ID != 0)
                {
                    msg.Status = "SUCCESS";
                    msg.Msg = aes.Encrypt(oa.ID.ToString());
                }
                else
                {
                    msg.Status = "SUCCESS";
                    msg.Msg = "UNREGISTERED";
                }
                return msg;
            }
            catch (Exception)
            {
                msg.Status = "ERROR";
                msg.Msg = "ERR_SYSTEM_ERROR";
                return msg;
            }
        }

        [Description("註冊新用戶")]
        [OperationContract, WebInvoke(UriTemplate = "Register", Method = "POST", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        public ReturnMsg NewAccountReg(Stream input)
        {

            string Email, Password;
            OAccount oa = new OAccount();
            string data = new StreamReader(input).ReadToEnd();
            ReturnMsg msg = new ReturnMsg();
            string Token, CryptData;
            JObject jo;
            try
            {
                jo = JsonConvert.DeserializeObject<JObject>(data);
                Token = Convert.ToString(jo["token"]);
                CryptData = Convert.ToString(jo["data"]);
            }
            catch (Exception)
            {
                msg.Status = "ERROR";
                msg.Msg = "ERR_DATA_ERROR";
                return msg;
            }
            Company.CompanyInfo company = new Company.CompanyInfo();
            try
            {
                company = ShareFunc.VarifyToken(Token);
            }
            catch (Exception)
            {
                msg.Status = "ERROR";
                msg.Msg = "ERR_TOKEN_ERROR";
                return msg;
            }
            if (company != null)
            {
                AESCryptography aes = new AESCryptography(company.CompanyHash, company.CompanyIV);
                try
                {

                    CryptData = aes.Decrypt(CryptData);
                }
                catch (Exception)
                {
                    msg.Status = "ERROR";
                    msg.Msg = "ERR_INCOME_DATA";
                    return msg;
                }
                jo = JsonConvert.DeserializeObject<JObject>(CryptData);
                Email = Convert.ToString(jo["email"]);
                Password = Convert.ToString(jo["pw"]);

                oa.AgeRange = "";
                oa.Birthday = "";
                oa.Email = Email;
                oa.Password = Password;
                oa.FName = "";
                oa.Gender = "2";
                oa.Introducer = 0;
                oa.LName = "";
                oa.Local = "";
                oa.MName = "";
                oa.MobilePhone = "";
                oa.NickName = "";
                oa.Source = "ADD";
                oa.SourceID = "";


                BALAccount bala = new BALAccount(ShareParameter.ConnectionString);
                try
                {
                    oa.ID = bala.CreateNewUserAccount(oa);
                }
                catch (Exception)
                {
                    msg.Status = "ERROR";
                    msg.Msg = "ERR_REG_ERROR";
                    return msg;
                }
                msg.Status = "SUCCESS";
                msg.Msg = aes.Encrypt(oa.ID.ToString());
                return msg;
            }
            else
            {
                msg.Status = "ERROR";
                msg.Msg = "ERR_ERROR_TOKEN";
                return msg;
            }
        }

        [Description("儲值")]
        [OperationContract, WebInvoke(UriTemplate = "Deposit", Method = "POST", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        public ReturnMsg DepositAmt(Stream input)
        {
            string data = new StreamReader(input).ReadToEnd();
            ReturnMsg msg = new ReturnMsg();
            string Token, CryptData;
            JObject jo;
            try
            {
                jo = JsonConvert.DeserializeObject<JObject>(data);
                Token = Convert.ToString(jo["token"]);
                CryptData = Convert.ToString(jo["data"]);
            }
            catch (Exception)
            {
                msg.Status = "ERROR";
                msg.Msg = "ERR_DATA_ERROR";
                return msg;
            }
            Company.CompanyInfo company = new Company.CompanyInfo();
            try
            {
                company = ShareFunc.VarifyToken(Token);

            }
            catch (Exception)
            {
                msg.Status = "ERROR";
                msg.Msg = "ERR_TOKEN_ERROR";
                return msg;
            }

            if (company != null)
            {
                try
                {
                    AESCryptography aes = new AESCryptography(company.CompanyHash, company.CompanyIV);
                    CryptData = aes.Decrypt(CryptData);
                }
                catch (Exception)
                {
                    msg.Status = "ERROR";
                    msg.Msg = "ERR_INCOME_DATA";
                    return msg;
                }
                jo = JsonConvert.DeserializeObject<JObject>(CryptData);

                int UserID = Convert.ToInt32(jo["id"]);
                int Amount = Convert.ToInt32(jo["amt"]);
                string Serialnum = Convert.ToString(jo["serial"]); //Addweup訂單編號
                string result = "";

                try
                {
                    HttpWebRequest request = HttpWebRequest.Create(AddweupAPIURL + Serialnum) as HttpWebRequest;

                    request.Method = "GET";    // 方法
                    request.KeepAlive = true; //是否保持連線
                    request.ContentType = "application/json";
                    request.Headers["X-Access-Token"] = AddweupToken;

                    using (WebResponse response = request.GetResponse())
                    {
                        StreamReader sr = new StreamReader(response.GetResponseStream());
                        result = sr.ReadToEnd();
                        sr.Close();
                    }
                }
                catch (Exception)
                {
                    msg.Status = "ERROR";
                    msg.Msg = "ERR_QRY_ADDWEUP";
                    return msg;
                }
                jo = JsonConvert.DeserializeObject<JObject>(result);

                if (!Convert.ToBoolean(jo["result"]))
                {
                    msg.Status = "ERROR";
                    msg.Msg = "ERR_ADD_ORDER";
                    return msg;
                }

                StringBuilder sb = new StringBuilder();
                List<MySqlParameter> parms = new List<MySqlParameter>();

                sb.Append("INSERT INTO addweup_serial_record (SerialNum, SerialJson, TransFlag) VALUES (@SerialNum, @SerialJson, '0')");
                parms.Add(new MySqlParameter("@SerialNum", Serialnum));
                parms.Add(new MySqlParameter("@SerialJson", CryptData));

                try
                {
                    using (dbConnectionMySQL conn = new dbConnectionMySQL(ShareParameter.ConnectionStringB))
                    {
                        conn.executeInsertQuery(sb.ToString(), parms.ToArray());
                    }
                }
                catch (MySqlException)
                {
                    msg.Status = "ERROR";
                    msg.Msg = "ERR_SYS_ERROR";
                    return msg;
                }


                msg.Status = "SUCCESS";
                msg.Msg = "";
                return msg;
            }
            else
            {
                msg.Status = "ERROR";
                msg.Msg = "ERR_ERROR_TOKEN";
                return msg;
            }
        }

        [Description("確認Addweup序號是否串入成功")]
        [OperationContract, WebInvoke(UriTemplate = "CheckS", Method = "POST", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        public ReturnMsg CheckAddSerial(Stream input)
        {
            string data = new StreamReader(input).ReadToEnd();
            ReturnMsg msg = new ReturnMsg();
            string Token, CryptData;
            JObject jo;
            try
            {
                jo = JsonConvert.DeserializeObject<JObject>(data);
                Token = Convert.ToString(jo["token"]);
                CryptData = Convert.ToString(jo["data"]);
            }
            catch (Exception)
            {
                msg.Status = "ERROR";
                msg.Msg = "ERR_DATA_ERROR";
                return msg;
            }
            Company.CompanyInfo company = new Company.CompanyInfo();
            try
            {
                company = ShareFunc.VarifyToken(Token);
            }
            catch (Exception)
            {
                msg.Status = "ERROR";
                msg.Msg = "ERR_TOKEN_ERROR";
                return msg;
            }
            if (company != null)
            {
                DataTable dt;
                AESCryptography aes = new AESCryptography(company.CompanyHash, company.CompanyIV);
                try
                {
                    CryptData = aes.Decrypt(CryptData);
                }
                catch (Exception)
                {
                    msg.Status = "ERROR";
                    msg.Msg = "ERR_INCOME_DATA";
                    return msg;
                }
                StringBuilder sb = new StringBuilder();
                List<MySqlParameter> parms = new List<MySqlParameter>();

                sb.Append("Select * From  addweup_serial_record WHERE SerialNum = @SerialNum ");
                parms.Add(new MySqlParameter("@SerialNum", CryptData));

                try
                {
                    using (dbConnectionMySQL conn = new dbConnectionMySQL(ShareParameter.ConnectionStringB))
                    {
                        dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                    }
                }
                catch (MySqlException)
                {
                    msg.Status = "ERROR";
                    msg.Msg = "ERR_SYS_ERROR";
                    return msg;
                }

                if (dt.Rows.Count == 1)
                {
                    msg.Status = "SUCCESS";
                    msg.Msg = "True";
                }
                else if (dt.Rows.Count == 0)
                {
                    msg.Status = "SUCCESS";
                    msg.Msg = "False";
                }
                else
                {
                    msg.Status = "ERROR";
                    msg.Msg = "ERR_MULTI_DEPOSIT";
                }
                return msg;

            }
            else
            {
                msg.Status = "ERROR";
                msg.Msg = "ERR_ERROR_TOKEN";
                return msg;
            }
        }
    }
}
