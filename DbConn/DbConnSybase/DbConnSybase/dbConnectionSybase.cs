﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Sybase.Data;
using Sybase.Data.AseClient;

namespace DbConn
{

    /// <summary>
    /// MySQL的DBConn層 無transaction
    /// </summary>
    public class dbConnectionSybase : IDisposable
    {

        private AseDataAdapter myAdapter;
        private AseConnection conn;
        private bool disposed = false;
        /// <constructor>
        /// Initialise Connection
        /// </constructor>
        public dbConnectionSybase(string ConnectionString)
        {
            myAdapter = new AseDataAdapter();
            conn = new AseConnection(ConnectionString);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;
            if (disposing)
            {
                this.conn.Close();
                this.conn.Dispose();
                this.myAdapter.Dispose();
            }
            disposed = true;
        }

        /// <method>
        /// Open Database Connection if Closed or Broken
        /// </method>
        private AseConnection openConnection()
        {
            if (conn.State == ConnectionState.Closed || conn.State ==
                        ConnectionState.Broken)
            {
                conn.Open();
            }
            return conn;
        }

        /// <method>
        /// Select Query
        /// </method>
        public DataTable executeSelectQuery(String _query, AseParameter[] AseParameter)
        {
            AseCommand myCommand = new AseCommand();
            DataTable dataTable = new DataTable();
            dataTable = null;
            DataSet ds = new DataSet();
            try
            {
                myCommand.Connection = openConnection();
                myCommand.CommandText = _query;
                myCommand.Parameters.AddRange(AseParameter);
                myCommand.ExecuteNonQuery();
                myAdapter.SelectCommand = myCommand;
                myAdapter.Fill(ds);
                dataTable = ds.Tables[0];
            }
            catch (AseException e)
            {
                throw new Exception("Error - Connection.executeSelectQuery - Query: " + _query + " \nException: " + e.StackTrace.ToString());
            }
            finally
            {

            }
            return dataTable;
        }

        /// <method>
        /// Insert Query
        /// </method>
        public bool executeInsertQuery(String _query, AseParameter[] AseParameter)
        {
            AseCommand myCommand = new AseCommand();
            try
            {
                myCommand.Connection = openConnection();
                myCommand.CommandText = _query;
                myCommand.Parameters.AddRange(AseParameter);
                myAdapter.InsertCommand = myCommand;
                myCommand.ExecuteNonQuery();
            }
            catch (AseException e)
            {
                throw new Exception("Error - Connection.executeInsertQuery - Query: " + _query + " \nException: \n" + e.StackTrace.ToString());
            }
            finally
            {
            }
            return true;
        }

        /// <method>
        /// Update Query
        /// </method>
        public int executeUpdateQuery(String _query, AseParameter[] AseParameter)
        {
            AseCommand myCommand = new AseCommand();
            int rtn;
            try
            {
                myCommand.Connection = openConnection();
                myCommand.CommandText = _query;
                myCommand.Parameters.AddRange(AseParameter);
                myAdapter.UpdateCommand = myCommand;
                rtn = myCommand.ExecuteNonQuery();
                if (rtn < 0)
                    throw new Exception("Error -  Connection.executeDeleteQuery - Query: " + _query + " \nException: Update No Data");
            }
            catch (AseException e)
            {
                throw e;// throw new Exception("Error - Connection.executeUpdateQuery - Query: " + _query + " \nException: " + e.StackTrace.ToString());
            }
            finally
            {
            }
            return rtn;
        }

        /// <method>
        /// Delete Query
        /// </method>
        public bool executeDeleteQuery(String _query, AseParameter[] AseParameter)
        {
            AseCommand myCommand = new AseCommand();
            try
            {
                myCommand.Connection = openConnection();
                myCommand.CommandText = _query;
                myCommand.Parameters.AddRange(AseParameter);
                myAdapter.UpdateCommand = myCommand;

                if (myCommand.ExecuteNonQuery() <= 0)
                    throw new Exception("Error -  Connection.executeDeleteQuery - Query: " + _query + " \nException: Delete No Data");
            }
            catch (AseException e)
            {
                throw e;// throw new Exception("Error - Connection.executeDeleteQuery - Query: " + _query + " \nException: " + e.StackTrace.ToString());
            }
            finally
            {
            }
            return true;
        }
    }

    /// <summary>
    /// MySQL的DBConn層 支援transaction
    /// </summary>
    public class dbTConnectionSybase : IDisposable
    {
        private AseDataAdapter myAdapter;
        private AseConnection conn;
        private AseTransaction trn;
        private bool disposed = false;
        /// <constructor>
        /// Initialise Connection
        /// </constructor>
        public dbTConnectionSybase(string ConnectionString)
        {
            this.myAdapter = new AseDataAdapter();
            this.conn = new AseConnection(ConnectionString);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;
            if (disposing)
            {
                trn.Dispose();
                this.conn.Close();
                this.conn.Dispose();
                this.myAdapter.Dispose();
            }
            disposed = true;
        }

        /// <method>
        /// Open Database Connection if Closed or Broken
        /// </method>
        private AseConnection openConnection()
        {
            if (this.conn.State == ConnectionState.Closed || this.conn.State == ConnectionState.Broken)
            {
                this.conn.Open();
                this.trn = conn.BeginTransaction();
            }
            return this.conn;
        }

        /// <method>
        /// Open Transaction if Connection is not Closed and Broken
        /// </method>
        private AseTransaction getTransaction()
        {

            if (this.conn.State == ConnectionState.Open)
                return this.trn;
            else
                this.openConnection();

            return this.trn;

        }

        public void Commit()
        {
            try
            {
                if (trn.Connection != null && conn.State != ConnectionState.Closed && conn.State != ConnectionState.Broken)
                    trn.Commit();
            }
            catch (AseException e)
            {
                throw new Exception("Error - Commit Exception: \n" + e.StackTrace.ToString());
            }
        }

        public void Rollback()
        {
            try
            {
                if (trn.Connection != null && conn.State != ConnectionState.Closed && conn.State != ConnectionState.Broken)
                    trn.Rollback();
            }
            catch (AseException e)
            {
                throw new Exception("Error - Rollback Exception: \n" + e.StackTrace.ToString());
            }
        }
        /// <method>
        /// Insert Query
        /// </method>
        public bool executeInsertQuery(String _query, AseParameter[] AseParameter)
        {
            AseCommand myCommand = new AseCommand();
            try
            {
                myCommand.Connection = this.openConnection();
                myCommand.Transaction = this.getTransaction();
                myCommand.CommandText = _query;
                myCommand.Parameters.AddRange(AseParameter);
                myAdapter.InsertCommand = myCommand;
                myCommand.ExecuteNonQuery();
            }
            catch (AseException e)
            {
                Rollback();
                throw new Exception("Error - Connection.executeInsertQuery - Query: " + _query + " \nException: \n" + e.StackTrace.ToString());
            }
            finally
            {
            }
            return true;
        }

        /// <method>
        /// Update Query
        /// </method>
        public bool executeUpdateQuery(String _query, AseParameter[] AseParameter)
        {
            AseCommand myCommand = new AseCommand();
            try
            {
                myCommand.Connection = openConnection();
                myCommand.Transaction = getTransaction();
                myCommand.CommandText = _query;
                myCommand.Parameters.AddRange(AseParameter);
                myAdapter.UpdateCommand = myCommand;
                myCommand.ExecuteNonQuery();
            }
            catch (AseException e)
            {
                Rollback();
                throw new Exception("Error - Connection.executeUpdateQuery - Query: " + _query + " \nException: " + e.StackTrace.ToString());
            }
            finally
            {
            }
            return true;
        }

        /// <method>
        /// Delete Query
        /// </method>
        public bool executeDeleteQuery(String _query, AseParameter[] AseParameter)
        {
            AseCommand myCommand = new AseCommand();
            try
            {
                myCommand.Connection = openConnection();
                myCommand.Transaction = getTransaction();
                myCommand.CommandText = _query;
                myCommand.Parameters.AddRange(AseParameter);
                myAdapter.UpdateCommand = myCommand;
                myCommand.ExecuteNonQuery();
            }
            catch (AseException e)
            {
                Rollback();
                throw new Exception("Error - Connection.executeDeleteQuery - Query: " + _query + " \nException: " + e.StackTrace.ToString());
            }
            finally
            {
            }
            return true;
        }
    }
}
