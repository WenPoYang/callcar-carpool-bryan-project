﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MySql.Data.MySqlClient;
using System.Data;

namespace DbConn
{
    /// <summary>
    /// MySQL的DBConn層 無transaction
    /// </summary>
    public class dbConnectionMySQL : IDisposable
    {
        private MySqlDataAdapter myAdapter;
        private MySqlConnection conn;
        private bool disposed = false;
        /// <constructor>
        /// Initialise Connection
        /// </constructor>
        public dbConnectionMySQL(string ConnectionString)
        {
            myAdapter = new MySqlDataAdapter();
            conn = new MySqlConnection(ConnectionString);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;
            if (disposing)
            {
                this.conn.Close();
                this.conn.Dispose();
                this.myAdapter.Dispose();
            }
            disposed = true;
        }

        /// <method>
        /// Open Database Connection if Closed or Broken
        /// </method>
        private MySqlConnection openConnection()
        {
            if (conn.State == ConnectionState.Closed || conn.State ==
                        ConnectionState.Broken)
            {
                conn.Open();
            }
            return conn;
        }

        /// <method>
        /// Select Query
        /// </method>
        public DataTable executeSelectQuery(String _query, MySqlParameter[] MySqlParameter)
        {
            MySqlCommand myCommand = new MySqlCommand();
            DataTable dataTable = new DataTable();
            dataTable = null;
            DataSet ds = new DataSet();
            try
            {
                myCommand.Connection = openConnection();
                myCommand.CommandText = _query;
                myCommand.Parameters.AddRange(MySqlParameter);
                myCommand.ExecuteNonQuery();
                myAdapter.SelectCommand = myCommand;
                myAdapter.Fill(ds);
                dataTable = ds.Tables[0];
            }
            catch (MySqlException e)
            {
                throw e;
                //("Error - Connection.executeSelectQuery - Query: " + _query + " \nException: " + e.StackTrace.ToString() + "\nErrorNumber:" + e.Number);
            }
            finally
            {

            }
            return dataTable;
        }

        /// <method>
        /// Insert Query
        /// </method>
        public int executeInsertQuery(String _query, MySqlParameter[] MySqlParameter)
        {
            MySqlCommand myCommand = new MySqlCommand();
            try
            {
                myCommand.Connection = openConnection();
                myCommand.CommandText = _query;
                myCommand.Parameters.AddRange(MySqlParameter);
                myAdapter.InsertCommand = myCommand;
                return myCommand.ExecuteNonQuery();

            }
            catch (MySqlException e)
            {
                throw e;
                //throw new Exception("Error - Connection.executeInsertQuery - Query: " + _query + " \nException: \n" + e.StackTrace.ToString() + "\nErrorNumber:" + e.Number);
            }
            finally
            {
            }
        }

        /// <method>
        /// Update Query
        /// </method>
        public int executeUpdateQuery(String _query, MySqlParameter[] MySqlParameter)
        {
            MySqlCommand myCommand = new MySqlCommand();
            try
            {
                myCommand.Connection = openConnection();
                myCommand.CommandText = _query;
                myCommand.Parameters.AddRange(MySqlParameter);
                myAdapter.UpdateCommand = myCommand;
                return myCommand.ExecuteNonQuery();
            }
            catch (MySqlException e)
            {
                throw e;
                //throw new Exception("Error - Connection.executeUpdateQuery - Query: " + _query + " \nException: " + e.StackTrace.ToString() + "\nErrorNumber:" + e.Number);
            }
            finally
            {
            }
        }

        /// <method>
        /// Delete Query
        /// </method>
        public int executeDeleteQuery(String _query, MySqlParameter[] MySqlParameter)
        {
            MySqlCommand myCommand = new MySqlCommand();
            try
            {
                myCommand.Connection = openConnection();
                myCommand.CommandText = _query;
                myCommand.Parameters.AddRange(MySqlParameter);
                myAdapter.UpdateCommand = myCommand;
                return myCommand.ExecuteNonQuery();
            }
            catch (MySqlException e)
            {
                throw e;
                //throw new Exception("Error - Connection.executeDeleteQuery - Query: " + _query + " \nException: " + e.StackTrace.ToString() + "\nErrorNumber:" + e.Number);
            }
            finally
            {
            }
        }
    }

    /// <summary>
    /// MySQL的DBConn層 支援transaction
    /// </summary>
    public class dbTConnectionMySQL : IDisposable
    {
        private MySqlDataAdapter myAdapter;
        private MySqlConnection conn;
        private MySqlTransaction trn;
        private bool disposed = false;
        /// <constructor>
        /// Initialise Connection
        /// </constructor>
        public dbTConnectionMySQL(string ConnectionString)
        {
            this.myAdapter = new MySqlDataAdapter();
            this.conn = new MySqlConnection(ConnectionString);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;
            if (disposing)
            {
                trn.Rollback();
                trn.Dispose();
                this.conn.Close();
                this.conn.Dispose();
                this.myAdapter.Dispose();
            }
            disposed = true;
        }

        /// <method>
        /// Open Database Connection if Closed or Broken
        /// </method>
        private MySqlConnection openConnection()
        {
            if (this.conn.State == ConnectionState.Closed || this.conn.State == ConnectionState.Broken)
            {
                this.conn.Open();
                this.trn = conn.BeginTransaction();
            }
            return this.conn;
        }

        /// <method>
        /// Open Transaction if Connection is not Closed and Broken
        /// </method>
        private MySqlTransaction getTransaction()
        {

            if (this.conn.State == ConnectionState.Open)
                return this.trn;
            else
                this.openConnection();

            return this.trn;

        }

        public void Commit()
        {
            try
            {
                if (this.trn.Connection != null && this.conn.State != ConnectionState.Closed && this.conn.State != ConnectionState.Broken)
                    this.trn.Commit();
            }
            catch (MySqlException e)
            {
                throw e;
                //throw new Exception("Error - Commit Exception: \n" + e.StackTrace.ToString());
            }
        }

        public void Rollback()
        {
            try
            {
                if (this.trn.Connection != null && this.conn.State != ConnectionState.Closed && this.conn.State != ConnectionState.Broken)
                    this.trn.Rollback();
            }
            catch (MySqlException e)
            {
                throw e;
                //throw new Exception("Error - Rollback Exception: \n" + e.StackTrace.ToString());
            }
        }

        /// <method>
        /// Select Query
        /// </method>
        public DataTable executeSelectQuery(String _query, MySqlParameter[] MySqlParameter)
        {
            MySqlCommand myCommand = new MySqlCommand();
            DataTable dataTable = new DataTable();
            dataTable = null;
            DataSet ds = new DataSet();
            try
            {
                myCommand.Connection = this.openConnection();
                //myCommand.Transaction = this.getTransaction();
                myCommand.CommandText = _query;
                myCommand.Parameters.AddRange(MySqlParameter);
                myCommand.ExecuteNonQuery();
                myAdapter.SelectCommand = myCommand;
                myAdapter.Fill(ds);
                dataTable = ds.Tables[0];
            }
            catch (MySqlException e)
            {
                throw e;
                //("Error - Connection.executeSelectQuery - Query: " + _query + " \nException: " + e.StackTrace.ToString() + "\nErrorNumber:" + e.Number);
            }
            finally
            {

            }
            return dataTable;
        }

        /// <method>
        /// Insert Query
        /// </method>
        public int executeInsertQuery(String _query, MySqlParameter[] MySqlParameter)
        {
            MySqlCommand myCommand = new MySqlCommand();
            try
            {
                myCommand.Connection = this.openConnection();
                myCommand.Transaction = this.getTransaction();
                myCommand.CommandText = _query;
                myCommand.Parameters.AddRange(MySqlParameter);
                myAdapter.InsertCommand = myCommand;
                return myCommand.ExecuteNonQuery();
            }
            catch (MySqlException e)
            {
                throw e;// throw new Exception("Error - Connection.executeInsertQuery - Query: " + _query + " \nException: \n" + e.StackTrace.ToString() + "\nErrorNumber:" + e.Number);
            }
            finally
            {
            }

        }

        /// <method>
        /// Update Query
        /// </method>
        public int executeUpdateQuery(String _query, MySqlParameter[] MySqlParameter)
        {
            MySqlCommand myCommand = new MySqlCommand();
            try
            {
                myCommand.Connection = openConnection();
                myCommand.Transaction = getTransaction();
                myCommand.CommandText = _query;
                myCommand.Parameters.AddRange(MySqlParameter);
                myAdapter.UpdateCommand = myCommand;
                return myCommand.ExecuteNonQuery();
            }
            catch (MySqlException e)
            {
                throw e; //throw new Exception("Error - Connection.executeUpdateQuery - Query: " + _query + " \nException: " + e.StackTrace.ToString() + "\nErrorNumber:" + e.Number);
            }
            finally
            {
            }

        }

        /// <method>
        /// Delete Query
        /// </method>
        public int executeDeleteQuery(String _query, MySqlParameter[] MySqlParameter)
        {
            MySqlCommand myCommand = new MySqlCommand();
            try
            {
                myCommand.Connection = openConnection();
                myCommand.Transaction = getTransaction();
                myCommand.CommandText = _query;
                myCommand.Parameters.AddRange(MySqlParameter);
                myAdapter.UpdateCommand = myCommand;
                return myCommand.ExecuteNonQuery();
            }
            catch (MySqlException e)
            {
                throw e; //throw new Exception("Error - Connection.executeDeleteQuery - Query: " + _query + " \nException: " + e.StackTrace.ToString() + "\nErrorNumber:" + e.Number);
            }
            finally
            {
            }
        }
    }
}
