﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.ComponentModel;
using System.IO;
using Dapper;
using MySql.Data.MySqlClient;
using System.Configuration;
using Newtonsoft.Json;
using Callcar.BAL;

namespace LineBOT_API
{

    [ServiceContract(Namespace = "LineBOT_API")]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]


    public class LineBOT
    {
        public static string ConnectionString = ConfigurationManager.ConnectionStrings["CarpoolLab"].ConnectionString;

        [Description("取得此使用者使用記錄")]
        [OperationContract, WebInvoke(UriTemplate = "GetLineStep", Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public string GetBOTStep(Stream input)
        {
            List<OFlowStep> List;
            strReturn rtn = new strReturn();

            string data = new StreamReader(input).ReadToEnd();
            string name = data.Split(',')[0];
            string id = data.Split(',')[1];
            string sql = "SELECT SeqNo, AppName, AppID, StepID, Language, CreateTime, UpdTime 	FROM bot_steps 	where AppName = @name and AppID = @ID";
            var parms = new { name = name, ID = id };

            try
            {
                using (var cn = new MySqlConnection(ConnectionString))
                {
                    cn.Open();
                    var step = cn.Query<OFlowStep>(sql, parms);
                    List = step.ToList<OFlowStep>();
                }
            }
            catch (Exception ex)
            {

                rtn.Status = "Error";
                rtn.Message = ex.Message;
                rtn.Obj = ex;

                return JsonConvert.SerializeObject(rtn);
            }

            strFlowStep str = new strFlowStep();
            if (List.Count > 0)
            {
                str.AppID = List[0].AppID;
                str.AppName = List[0].AppName;
                str.Step = List[0].StepID;
            }
            else
                str = null;

            rtn.Status = "Success";
            rtn.Message = "";
            rtn.Obj = str;

            return JsonConvert.SerializeObject(rtn);
        }

        [Description("新增一筆使用者BOT使用記錄")]
        [OperationContract, WebInvoke(UriTemplate = "NewStep", Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public string InsBOTStep(Stream input)
        {
            strReturn rtn = new strReturn();
            string data = new StreamReader(input).ReadToEnd();
            string name = data.Split(',')[0];
            string id = data.Split(',')[1];
            string step = data.Split(',')[2];
            string Lan = data.Split(',')[3];
            string sql = "INSERT INTO bot_steps (AppName, AppID, StepID,Language ) VALUES (@name, @id, @step,@lan)";
            var parms = new { name = name, id = id, step = step, lan = Lan };

            try
            {
                using (var cn = new MySqlConnection(ConnectionString))
                {
                    cn.Open();
                    cn.Execute(sql, parms);
                }
                rtn.Status = "Success";
                rtn.Message = "";
                rtn.Obj = null;
                return JsonConvert.SerializeObject(rtn);
            }
            catch (Exception ex)
            {
                rtn.Status = "Error";
                rtn.Message = ex.Message;
                rtn.Obj = ex;
                return JsonConvert.SerializeObject(rtn);
            }
        }

        [Description("更新使用者BOT Step")]
        [OperationContract, WebInvoke(UriTemplate = "UpdStep", Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public string UpdBOTStep(Stream input)
        {
            strReturn rtn = new strReturn();
            string data = new StreamReader(input).ReadToEnd();
            string name = data.Split(',')[0];
            string id = data.Split(',')[1];
            string step = data.Split(',')[2];
            string Lan = data.Split(',')[3];
            string sql = "Update bot_steps Set  StepID = @step , Language = @lan Where AppName = @name and  AppID = @id  ";
            var parms = new { name = name, id = id, step = step, lan = Lan };

            try
            {
                using (var cn = new MySqlConnection(ConnectionString))
                {
                    cn.Open();
                    cn.Execute(sql, parms);
                }
                rtn.Status = "Success";
                rtn.Message = "";
                rtn.Obj = null;
                return JsonConvert.SerializeObject(rtn);
            }
            catch (Exception ex)
            {
                rtn.Status = "Error";
                rtn.Message = ex.Message;
                rtn.Obj = ex;
                return JsonConvert.SerializeObject(rtn);
            }
        }

        [Description("檢查LineID是否已綁定")]
        [OperationContract, WebInvoke(UriTemplate = "chkBinding", Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public string isBinding(Stream input)
        {
            strReturn rtn = new strReturn();
            List<OBotIdendity> List;
            string data = new StreamReader(input).ReadToEnd();
            string sql = "SELECT BindingAPP, BindingID, UserID, DisplayName, PicURL	FROM account_binding 	where BindingAPP = @app and BindingID = @id";
            string name = data.Split(',')[0];
            string id = data.Split(',')[1];
            var parms = new { app = name, id = id };

            try
            {
                using (var cn = new MySqlConnection(ConnectionString))
                {
                    cn.Open();
                    var Identity = cn.Query<OBotIdendity>(sql, parms);
                    List = Identity.ToList();
                }
                if (List.Count > 0)
                    rtn.Obj = "True";
                else
                    rtn.Obj = "False";
                rtn.Message = null;
                rtn.Status = "Success";
            }
            catch (Exception ex)
            {
                rtn.Status = "Error";
                rtn.Message = ex.Message;
                rtn.Obj = ex;
            }
            return JsonConvert.SerializeObject(rtn);
        }

        [Description("將LineID綁定至既有帳號")]
        [OperationContract, WebInvoke(UriTemplate = "makBinding", Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public string MakBinding(Stream input)
        {
            strReturn rtn = new strReturn();
            string data = new StreamReader(input).ReadToEnd();
            string sql = "INSERT INTO account_binding (BindingAPP, BindingID, UserID, DisplayName, PicURL)	VALUES (@name, @id, @uid,@dname, @pic)";

            instrBotBinding identity = JsonConvert.DeserializeObject<instrBotBinding>(data);

            try
            {
                BALUserAccount balu = new Callcar.BAL.BALUserAccount(ConnectionString);
                Callcar.CObject.OAccount oa = balu.GetAccountFromEmail(identity.Email);

                var parms = new { name = identity.BindingAPP, id = identity.BindingID, uid = oa.ID, dname = identity.DisplayName, pic = identity.PicURL };

                using (var cn = new MySqlConnection(ConnectionString))
                {
                    cn.Open();
                    cn.Execute(sql, parms);
                }
                rtn.Message = null;
                rtn.Status = "Success";
                rtn.Obj = null;
            }
            catch (Exception ex)
            {
                rtn.Status = "Error";
                rtn.Message = ex.Message;
                rtn.Obj = ex;
            }
            return JsonConvert.SerializeObject(rtn);
        }

        [Description("檢查 Email 是否已在系統註冊")]
        [OperationContract, WebInvoke(UriTemplate = "chkEmailDup", Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public string isEmailDuplicated(Stream input)
        {
            strReturn rtn = new strReturn();
            string data = new StreamReader(input).ReadToEnd();

            try
            {
                BALUserAccount balu = new Callcar.BAL.BALUserAccount(ConnectionString);
                Callcar.CObject.OAccount oa = balu.GetAccountFromEmail(data);

                if (oa != null)
                    rtn.Obj = "True";
                else
                    rtn.Obj = "False";

                rtn.Message = null;
                rtn.Status = "Success";

            }
            catch (Exception ex)
            {
                rtn.Status = "Error";
                rtn.Message = ex.Message;
                rtn.Obj = ex;
            }
            return JsonConvert.SerializeObject(rtn);
        }

        [Description("檢查 Email 及密碼是否正確")]
        [OperationContract, WebInvoke(UriTemplate = "chkEP", Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public string chkEmailPW(Stream input)
        {
            strReturn rtn = new strReturn();
            string data = new StreamReader(input).ReadToEnd();
            strEmailPW str = JsonConvert.DeserializeObject<strEmailPW>(data);
            try
            {
                BALUserAccount bal = new BALUserAccount(ConnectionString);

                if (bal.ChkAccountPW(str.Email, str.PW))
                    rtn.Obj = "True";
                else
                    rtn.Obj = "False";

                rtn.Message = "";
                rtn.Status = "Success";
            }
            catch (Exception ex)
            {
                rtn.Status = "Error";
                rtn.Obj = ex;
                rtn.Message = ex.Message;
            }
            return JsonConvert.SerializeObject(rtn);
        }

        [Description("從Line新增ㄧ筆CallCar帳號")]
        [OperationContract, WebInvoke(UriTemplate = "addAcc", Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public string addLineAcc(Stream input)
        {
            strReturn rtn = new strReturn();
            string data = new StreamReader(input).ReadToEnd();
            Callcar.Struct.APIInsAccFB js = JsonConvert.DeserializeObject<Callcar.Struct.APIInsAccFB>(data);
            try
            {
                BALUserAccount bal = new BALUserAccount(ConnectionString);
                int UserID = bal.CreateAccFromFB(js);

                rtn.Status = "Success";
                rtn.Message = "";
                rtn.Obj = UserID;
            }
            catch (Exception ex)
            {
                rtn.Status = "Error";
                rtn.Obj = ex;
                rtn.Message = ex.Message;
            }
            return JsonConvert.SerializeObject(rtn);
        }
        #region JsonStruct
        public class strReturn
        {
            public string Status { get; set; }
            public string Message { get; set; }
            public object Obj { get; set; }
        }

        public class strFlowStep
        {
            public string AppName { get; set; }
            public string AppID { get; set; }
            public string Language { get; set; }
            public string Step { get; set; }
        }

        public class instrBotBinding
        {
            public string BindingAPP { get; set; }
            public string BindingID { get; set; }
            public string Email { get; set; }
            public string DisplayName { get; set; }
            public string PicURL { get; set; }
        }

        public class strEmailPW
        {
            public string Email { get; set; }
            public string PW { get; set; }
        }
        #endregion

        #region Object
        public class OFlowStep
        {
            public int SeqNo { get; set; }
            public string AppName { get; set; }
            public string AppID { get; set; }
            public string Language { get; set; }
            public string StepID { get; set; }
            public DateTime CreateTime { get; set; }
            public DateTime UpdTime { get; set; }
        }

        public class OBotIdendity
        {
            public string BindingAPP { get; set; }
            public string BindingID { get; set; }
            public int UserID { get; set; }
            public string DisplayName { get; set; }
            public string PicURL { get; set; }
        }
        #endregion
    }
}
