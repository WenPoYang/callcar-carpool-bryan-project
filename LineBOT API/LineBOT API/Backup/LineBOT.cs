﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Text;
using System.ComponentModel;
using System.IO;
using Dapper;
using MySql.Data.MySqlClient;
using MySql.Data.Types;
using System.Configuration;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace LineBOT_API
{

    [ServiceContract(Namespace = "LineBOT_API")]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]


    public class LineBOT
    {
        public static string ConnectionString = ConfigurationManager.ConnectionStrings["CarpoolLab"].ConnectionString;

        [Description("取得此使用者使用記錄")]
        [OperationContract, WebInvoke(UriTemplate = "GetLineStep", Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public string GetBOTStep(Stream input)
        {
            List<OFlowStep> List;
            strReturn rtn = new strReturn();

            string data = new StreamReader(input).ReadToEnd();
            string name = data.Split(',')[0];
            string id = data.Split(',')[1];
            string sql = "SELECT SeqNo, AppName, AppID, StepID, CreateTime, UpdTime 	FROM bot_steps 	where AppName = @name and AppID = @ID";
            var parms = new { name = name, ID = id };

            try
            {
                using (var cn = new MySqlConnection(ConnectionString))
                {
                    cn.Open();
                    var step = cn.Query<OFlowStep>(sql, parms);
                    List = step.ToList<OFlowStep>();
                }
            }
            catch (Exception ex)
            {

                rtn.Status = "Error";
                rtn.Message = ex.Message;
                rtn.Obj = ex;

                return JsonConvert.SerializeObject(rtn);
            }

            strFlowStep str = new strFlowStep();
            if (List.Count > 0)
            {
                str.AppID = List[0].AppID;
                str.AppName = List[0].AppName;
                str.Step = List[0].StepID;
            }
            else
                str = null;

            rtn.Status = "Success";
            rtn.Message = "";
            rtn.Obj = str;

            return JsonConvert.SerializeObject(rtn);
        }

        [Description("新增一筆使用者BOT使用記錄")]
        [OperationContract, WebInvoke(UriTemplate = "NewStep", Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public string InsBOTStep(Stream input)
        {
            strReturn rtn = new strReturn();
            string data = new StreamReader(input).ReadToEnd();
            string name = data.Split(',')[0];
            string id = data.Split(',')[1];
            string step = data.Split(',')[2];
            string sql = "INSERT INTO bot_steps (AppName, AppID, StepID) VALUES (@name, @id, @step)";
            var parms = new { name = name, id = id, step = step };

            try
            {
                using (var cn = new MySqlConnection(ConnectionString))
                {
                    cn.Open();
                    cn.Execute(sql, parms);
                }
                rtn.Status = "Success";
                rtn.Message = "";
                rtn.Obj = null;
                return JsonConvert.SerializeObject(rtn);
            }
            catch (Exception ex)
            {
                rtn.Status = "Error";
                rtn.Message = ex.Message;
                rtn.Obj = ex;
                return JsonConvert.SerializeObject(rtn);
            }
        }

        [Description("更新使用者BOT Step")]
        [OperationContract, WebInvoke(UriTemplate = "UpdStep", Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public string UpdBOTStep(Stream input)
        {
            strReturn rtn = new strReturn();
            string data = new StreamReader(input).ReadToEnd();
            string name = data.Split(',')[0];
            string id = data.Split(',')[1];
            string step = data.Split(',')[2];
            string sql = "Update bot_steps Set  StepID = @step Where AppName = @name and  AppID = @id  ";
            var parms = new { name = name, id = id, step = step };

            try
            {
                using (var cn = new MySqlConnection(ConnectionString))
                {
                    cn.Open();
                    cn.Execute(sql, parms);
                }
                rtn.Status = "Success";
                rtn.Message = "";
                rtn.Obj = null;
                return JsonConvert.SerializeObject(rtn);
            }
            catch (Exception ex)
            {
                rtn.Status = "Error";
                rtn.Message = ex.Message;
                rtn.Obj = ex;
                return JsonConvert.SerializeObject(rtn);
            }
        }

        #region JsonStruct
        public class strReturn
        {
            public string Status { get; set; }
            public string Message { get; set; }
            public object Obj { get; set; }
        }

        public class strFlowStep
        {
            public string AppName { get; set; }
            public string AppID { get; set; }
            public string Step { get; set; }
        }
        #endregion

        #region Object
        public class OFlowStep
        {
            public int SeqNo { get; set; }
            public string AppName { get; set; }
            public string AppID { get; set; }
            public string StepID { get; set; }
            public DateTime CreateTime { get; set; }
            public DateTime UpdTime { get; set; }
        }
        #endregion
    }
}
