﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Flurl.Http;
using Flurl;
using System.Runtime.Serialization.Json;
using System.Net.Http;
using System.Net.Http.Headers;
using FlightXMLClient;
using FlightAwareTest.FlightAware;

namespace FlightAwareTest
{
    public partial class Form1 : Form
    {
        //private string fxmlUrl = @"http://flightxml.flightaware.com/json/FlightXML3";
        // http://flightxml.flightaware.com/soap/FlightXML3 4832137a6182bba5328aa640097cf6a67f04b438
        private const string username = "ishareco";
        private const string apiKey = "4832137a6182bba5328aa640097cf6a67f04b438";

        public Form1()
        {
            InitializeComponent();
        }
        private void BtnGO_Click(object sender, EventArgs e)
        {
            FlightXML3 client = new FlightXML3();
            client.Credentials = new System.Net.NetworkCredential("ishareco", "4832137a6182bba5328aa640097cf6a67f04b438");


            ArrayOfAirlineFlightScheduleStruct afss = client.AirlineFlightSchedules(Int32.Parse(DateTimeToStamp("20171013000000").ToString()), Int32.Parse(DateTimeToStamp("20171013235900").ToString()), "", "", "CAL", "4", false, false, 1, false, 0, false);

            StringBuilder sb = new StringBuilder();
            AirlineInfoStruct ais = new AirlineInfoStruct();

            foreach (var flight in afss.flights)
            {
                DateTime dt = StampToDateTime(flight.departuretime.ToString());

                string temp = "CAL4@" + DateTimeToStamp(dt.ToString("yyyyMMddHHmmss")).ToString();
                ArrayOfFlightInfoStatusStruct res = client.FlightInfoStatus(temp, false, false, "", 1, false, 0, false);
                foreach (var f in res.flights)
                {
                    AirportDisplayStruct oairport = f.origin;
                    FlightAware.Timestamp fdt = f.filed_departure_time;
                    DateTime ddt = StampToDateTime(fdt.localtime.ToString());
                    AirportDisplayStruct tairport = f.destination;

                    sb.Append(String.Format("{0}  From 機場：{1}  時間： {2}  航廈：{3}  TO機場：{4}  時間：{5} ", flight.ident, flight.origin, StampToDateTime(f.filed_departure_time.localtime.ToString()).ToString("HH:mm"), f.terminal_orig, flight.destination, StampToDateTime(f.filed_arrival_time.localtime.ToString()).ToString("HH:mm")));
                    sb.Append(Environment.NewLine);
                }
            }
            txtResult.Text = sb.ToString();
        }

        private static async Task ProcessRepositories(string apiUrl, string username, string apiKey)
        {
            var serializer = new DataContractJsonSerializer(typeof(AirportInfoResult));
            var client = new HttpClient();
            var credentials = Encoding.ASCII.GetBytes(username + ":" + apiKey);
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(credentials));

            var streamTask = client.GetStreamAsync(apiUrl);
            var airportInfo = serializer.ReadObject(await streamTask) as AirportInfoResult;
            Console.WriteLine(airportInfo.AirportResult.Name);
            Console.WriteLine(airportInfo.AirportResult.Code);
            Console.ReadKey();
        }
        private DateTime StampToDateTime(string timeStamp)
        {
            DateTime dateTimeStart = TimeZone.CurrentTimeZone.ToLocalTime(new DateTime(1970, 1, 1));
            long lTime = long.Parse(timeStamp + "0000000");
            TimeSpan toNow = new TimeSpan(lTime);

            return dateTimeStart.Add(toNow);
        }

        private double DateTimeToStamp(string datetime)
        {
            DateTime dt;
            dt = DateTime.Parse(datetime.Substring(0, 4) + "-" + datetime.Substring(4, 2) + "-" + datetime.Substring(6, 2) + " " + datetime.Substring(8, 2) + ":" + datetime.Substring(10, 2) + ":" + datetime.Substring(12, 2));
            return (dt.AddHours(-8) - new DateTime(1970, 1, 1, 0, 0, 0)).TotalSeconds;
        }
    }
}
