﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace FlightXMLClient
{
    [DataContract(Name = "airport")]
    class AirlineFlightScheduleStruct2
    {
        [DataMember(Name = "actual_ident")]
        public string AtualIdent { get; set; }
        [DataMember(Name = "aircrafttype")]
        public string AircraftType { get; set; }
        [DataMember(Name = "arrivaltime")]
        public int ArrivalTime { get; set; }
        [DataMember(Name = "departuretime")]
        public int DepartureTime { get; set; }
        [DataMember(Name = "destination")]
        public string DestinationAirport { get; set; }
        [DataMember(Name = "fa_ident")]
        public string IdentID { get; set; }
        [DataMember(Name = "ident")]
        public string Ident { get; set; }
        [DataMember(Name = "meal_service")]
        public string MealService { get; set; }
        [DataMember(Name = "origin")]
        public string OriginalAirport { get; set; }
        [DataMember(Name = "seats_cabin_business")]
        public int BusinessSeats { get; set; }
        [DataMember(Name = "seats_cabin_coach")]
        public int CoachSeats { get; set; }
        [DataMember(Name = "seats_cabin_first")]
        public int FirstSeats { get; set; }
    }
}
