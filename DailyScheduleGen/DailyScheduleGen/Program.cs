﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DbConn;
using MySql.Data.MySqlClient;
using NLog.Config;
using NLog;

namespace DailyScheduleGen
{
    class Program
    {
        private const string ConnectionString = "Server=61.63.55.200;Port=3306;Database=carpoollab;Uid=apiuser;Pwd=7oPOh88I;Allow User Variables=True";
        //private const string ConnectionString = "Server=10.0.2.20;Port=3306;Database=carpoollab;Uid=ishareco;Pwd=fu0!j0$su0^vup#;Allow User Variables=True";
        private static Logger logger = NLog.LogManager.GetCurrentClassLogger();

        static void Main(string[] args)
        {
            string ScheduleDate;
            StringBuilder sb = new StringBuilder();
            DataTable dtCars = new DataTable();
            DataTable dt = new DataTable();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            List<int> ListVacation = new List<int>();
            ScheduleDate = DateTime.Now.AddDays(5).ToString("yyyyMMdd");

            //取得需排班的車
            sb.Append("select c.CarNo, c.FleetID, c.CarType, c.DriverID, c.ScheduleFlag,u.ShiftType from car_info c,user_info u where c.DriverID = u.UserID AND c.ScheduleFlag = '1'");
            using (dbConnectionMySQL conn = new dbConnectionMySQL(ConnectionString))
            {
                dtCars = conn.executeSelectQuery(sb.ToString(), parms.ToArray());

                sb.Length = 0;
                //取得休假名單
                sb.Append("select UserID from driver_vacation_sheet where VacationDate = @date");
                parms.Add(new MySqlParameter("@date", ScheduleDate));

                dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    ListVacation.Add(Convert.ToInt32(dt.Rows[i]["UserID"]));
                }

                for (int i = 0; i < dtCars.Rows.Count; i++)
                {
                    int UserID;
                    UserID = Convert.ToInt32(dtCars.Rows[i]["DriverID"]);
                    if (!ListVacation.Exists(x => x == UserID))
                    {
                        //未休假
                        string CarNo,  CarType, ShiftType;
                        uint FleetID;
                        CarNo = Convert.ToString(dtCars.Rows[i]["CarNo"]);
                        FleetID = Convert.ToUInt32(dtCars.Rows[i]["FleetID"]);
                        CarType = Convert.ToString(dtCars.Rows[i]["CarType"]);
                        ShiftType = Convert.ToString(dtCars.Rows[i]["ShiftType"]);

                        sb.Length = 0;
                        parms.Clear();
                        sb.Append("INSERT INTO daily_schedule	(ScheduleDate, ShiftType, CarNo, DriverID, FleetID, CarType) ");
                        sb.Append("VALUES (@ScheduleDate, @ShiftType,@CarNo , @DriverID, @FleetID, @CarType) ");

                        parms.Add(new MySqlParameter("@ScheduleDate", ScheduleDate));
                        parms.Add(new MySqlParameter("@ShiftType", ShiftType));
                        parms.Add(new MySqlParameter("@CarNo", CarNo));
                        parms.Add(new MySqlParameter("@DriverID", UserID));
                        parms.Add(new MySqlParameter("@FleetID", FleetID));
                        parms.Add(new MySqlParameter("@CarType", CarType));

                        try
                        {
                            conn.executeInsertQuery(sb.ToString(), parms.ToArray());
                        }
                        catch (Exception ex)
                        {
                            logger.Error(ex.Message);
                        }

                    }
                }
            }
        }
    }
}

