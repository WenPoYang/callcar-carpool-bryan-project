﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Net;



namespace FunctionTest
{

    public partial class Form1 : Form
    {
        private string EnUrl = @"http://apid.ishareco.com/DAPI/cryp?data={0}&key={1}";
        private string DeenUrl = @"http://apid.ishareco.com/DAPI/decryp?data={0}&key={1}";

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {

            try
            {
                string Url = string.Format(EnUrl, txtPlantext.Text, txtKey.Text);
                string CryPherText;
                WebRequest wr = WebRequest.Create(Url);
                StringBuilder sbJson = new StringBuilder();
                using (Stream stream = wr.GetResponse().GetResponseStream())
                {
                    StreamReader sr = new StreamReader(stream);

                    CryPherText = sr.ReadToEnd();
                    sr.Close();
                }
                txtCypherText.Text = CryPherText;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                string Url = string.Format(DeenUrl, txtCypherText.Text, txtKey.Text);
                string PlanText;
                WebRequest wr = WebRequest.Create(Url);
                StringBuilder sbJson = new StringBuilder();
                using (Stream stream = wr.GetResponse().GetResponseStream())
                {
                    StreamReader sr = new StreamReader(stream);

                    PlanText = sr.ReadToEnd();
                    sr.Close();
                }
                txtDecrypherText.Text = PlanText;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            txtCypherText.Text = Callcar.Common.MD5Cryptography.Encrypt(txtPlantext.Text);
        }
    }
}
